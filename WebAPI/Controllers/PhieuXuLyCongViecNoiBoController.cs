﻿using AutoMapper;
using Common.Enums;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Providers;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Web;
using System.Runtime.InteropServices;
using Common;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/phieuxuly")]
    [Authorize]
    public class PhieuXuLyCongViecNoiBoController : ApiControllerBase
    {
        private IPhieuXuLyCongViecNoiBoService _phieuXuLyCongViecNoiBoService;
        private IPhieuXuLyCongViecNoiBoUserService _phieuXuLyCongViecNoiBoUserService;
        private IDeleteService _deleteService;
        private IDuAnUserService _duAnUserService;
        private IDuAnService _duAnService;

        public PhieuXuLyCongViecNoiBoController(
            IErrorService errorService,
            IDeleteService deleteService,
            IPhieuXuLyCongViecNoiBoService phieuXuLyCongViecNoiBoService,
            IPhieuXuLyCongViecNoiBoUserService phieuXuLyCongViecNoiBoUserService,
            IDuAnUserService duAnUserService,
            IDuAnService duAnService
            ) : base(errorService)
        {
            this._phieuXuLyCongViecNoiBoService = phieuXuLyCongViecNoiBoService;
            this._phieuXuLyCongViecNoiBoUserService = phieuXuLyCongViecNoiBoUserService;
            this._deleteService = deleteService;
            _duAnUserService = duAnUserService;
            _duAnService = duAnService;
        }

        [Route("getallformobile")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetAllForMobile(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                string userID = User.Identity.GetUserId();
                var model = _phieuXuLyCongViecNoiBoService.GetAllForMobile(page, pageSize, userID, out totalRow, filter);
                PaginationSet<PhieuForMobiles> pagedSet = new PaginationSet<PhieuForMobiles>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getcountmobile")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetCountMobile(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string userID = User.Identity.GetUserId();
                var countData = _phieuXuLyCongViecNoiBoService.GetCountForMobile(userID);
                response = request.CreateResponse(HttpStatusCode.OK, countData);
                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                string userID = User.Identity.GetUserId();
                List<string> userIDNhanVien = new List<string>();
                if (User.IsInRole("Trưởng phòng"))
                {
                    int? idNhomDuAnTheoUser = null;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    var userDuAn = _duAnUserService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdDuanUser);
                    foreach (DuAnUser das in userDuAn)
                    {
                        if (das.AppUsers.IdNhomDuAnTheoUser == idNhomDuAnTheoUser)
                        {
                            var roleUser = AppUserManager.GetRoles(das.UserId);
                            if (roleUser.Contains("Nhân viên"))
                            {
                                if(!userIDNhanVien.Contains(das.UserId))
                                    userIDNhanVien.Add(das.UserId);
                            }
                                
                        }
                    }
                }
                var model = _phieuXuLyCongViecNoiBoService.GetByFilter(idDA, page, pageSize, userID,userIDNhanVien, out totalRow, filter );

                var modelVm = Mapper.Map<IEnumerable<PhieuXuLyCongViecNoiBo>, IEnumerable<PhieuXuLyCongViecNoiBoViewModels>>(model);

                PaginationSet<PhieuXuLyCongViecNoiBoViewModels> pagedSet = new PaginationSet<PhieuXuLyCongViecNoiBoViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getphieucha")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetByIdCha(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                string userID = User.Identity.GetUserId();
                var model = _phieuXuLyCongViecNoiBoService.GetPhieuCha(userID);
                var modelVm = Mapper.Map<IEnumerable<PhieuXuLyCongViecNoiBo>, IEnumerable<PhieuXuLyCongViecNoiBoViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage Create(HttpRequestMessage request, PhieuXuLyCongViecNoiBoAddViewModels newModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newPhieu = new PhieuXuLyCongViecNoiBo();
                    newPhieu.IdDuAn = newModel.IdDuAn;
                    newPhieu.TenCongViec = newModel.TenCongViec;
                    newPhieu.NoiDung = newModel.NoiDung;
                    newPhieu.NgayChuyen = DateTime.Now;
                    newPhieu.TrangThai = newModel.TrangThai;
                    newPhieu.IDPhieuCha = newModel.IDPhieuCha;
                    newPhieu.CreatedDate = DateTime.Now;
                    newPhieu.CreatedBy = User.Identity.GetUserId();
                    _phieuXuLyCongViecNoiBoService.Add(newPhieu);
                    _phieuXuLyCongViecNoiBoService.Save();
                    List<PhieuXuLyCongViecNoiBoUser> lstUserNew = new List<PhieuXuLyCongViecNoiBoUser>();
                    foreach(PhieuXuLyCongViecNoiBoUserAddViewModels phieuuser in newModel.Users)
                    {
                        if (phieuuser.Checked == true)
                        {
                            lstUserNew.Add(new PhieuXuLyCongViecNoiBoUser
                            {
                                IdPhieuXuLyCongViecNoiBo = newPhieu.IdPhieuXuLyCongViecNoiBo,
                                FullName = phieuuser.FullName,
                                UserXLCVNoiBoEnum = phieuuser.Loai,
                                UserId = phieuuser.UserId
                            });
                        }
                    }
                    _phieuXuLyCongViecNoiBoUserService.AddMulti(lstUserNew);
                    //var responseData = Mapper.Map<DanhMucNhaThau, DanhMucNhaThauViewModels>(newPhieu);
                    response = request.CreateResponse(HttpStatusCode.Created, newPhieu);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Create", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage Update(HttpRequestMessage request, PhieuXuLyCongViecNoiBoAddViewModels newModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newPhieu = _phieuXuLyCongViecNoiBoService.GetById(newModel.IdPhieuXuLyCongViecNoiBo);
                    newPhieu.IdDuAn = newModel.IdDuAn;
                    newPhieu.TenCongViec = newModel.TenCongViec;
                    newPhieu.NoiDung = newModel.NoiDung;
                    newPhieu.NgayChuyen = DateTime.Now;
                    newPhieu.TrangThai = newModel.TrangThai;
                    newPhieu.IDPhieuCha = newModel.IDPhieuCha;
                    newPhieu.CreatedDate = DateTime.Now;
                    newPhieu.CreatedBy = User.Identity.GetUserId();
                    _phieuXuLyCongViecNoiBoService.Update(newPhieu);
                    _phieuXuLyCongViecNoiBoService.Save();

                    _phieuXuLyCongViecNoiBoUserService.DeleteByPhieu(newModel.IdPhieuXuLyCongViecNoiBo);

                    List<PhieuXuLyCongViecNoiBoUser> lstUserNew = new List<PhieuXuLyCongViecNoiBoUser>();
                    foreach (PhieuXuLyCongViecNoiBoUserAddViewModels phieuuser in newModel.Users)
                    {
                        if (phieuuser.Checked == true)
                        {
                            lstUserNew.Add(new PhieuXuLyCongViecNoiBoUser
                            {
                                IdPhieuXuLyCongViecNoiBo = newPhieu.IdPhieuXuLyCongViecNoiBo,
                                FullName = phieuuser.FullName,
                                UserXLCVNoiBoEnum = phieuuser.Loai,
                                UserId = phieuuser.UserId
                            });
                        }
                    }
                    _phieuXuLyCongViecNoiBoUserService.AddMulti(lstUserNew);
                    response = request.CreateResponse(HttpStatusCode.Created, newPhieu);
                }

                return response;
            });
        }

        [Route("updatetrangthai")]
        [HttpPut]
        [Permission(Action = "Create", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, PhieuXuLyCongViecNoiBoAddViewModels newModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newPhieu = _phieuXuLyCongViecNoiBoService.GetById(newModel.IdPhieuXuLyCongViecNoiBo);
                    newPhieu.TrangThai = newModel.TrangThai;
                    newPhieu.UpdatedDate = DateTime.Now;
                    newPhieu.UpdatedBy = User.Identity.GetUserId();
                    _phieuXuLyCongViecNoiBoService.Update(newPhieu);
                    _phieuXuLyCongViecNoiBoService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, newPhieu);
                }

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _phieuXuLyCongViecNoiBoService.GetById(id);

                var responseData = Mapper.Map<PhieuXuLyCongViecNoiBo, PhieuXuLyCongViecNoiBoViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyidformobile/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage GetByIdForMobile(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _phieuXuLyCongViecNoiBoService.GetByIdForMobile(id);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [HttpDelete]
        [Route("deletemulti")]
        [Permission(Action = "Delete", Function = "PHIEUXULYCONGVIECNOIBO")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var tempt = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                foreach (var item in tempt)
                {
                    int id = Convert.ToInt16(item);
                    _phieuXuLyCongViecNoiBoService.Delete(id);
                    _phieuXuLyCongViecNoiBoService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }


        [Route("XuatFile")]
        [HttpGet]
        public HttpResponseMessage ExportXDCBNam(HttpRequestMessage request, int idDA, string filter = null)
        {

            string userID = User.Identity.GetUserId();
            List<string> userIDNhanVien = new List<string>();
            if (User.IsInRole("Trưởng phòng"))
            {
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
                var userDuAn = _duAnUserService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdDuanUser);
                foreach (DuAnUser das in userDuAn)
                {
                    if (das.AppUsers.IdNhomDuAnTheoUser == idNhomDuAnTheoUser)
                    {
                        var roleUser = AppUserManager.GetRoles(das.UserId);
                        if (roleUser.Contains("Nhân viên"))
                        {
                            if (!userIDNhanVien.Contains(das.UserId))
                                userIDNhanVien.Add(das.UserId);
                        }

                    }
                }
                _duAnUserService.Save();
            }
            var model = _phieuXuLyCongViecNoiBoService.GetForExport(idDA, userID, userIDNhanVien, filter);

            var duAn = _duAnService.GetByIdForPhieu(idDA);

            Microsoft.Office.Interop.Word.Application appWord = null;
            Microsoft.Office.Interop.Word.Document docWord = null;
            Microsoft.Office.Interop.Word.ContentControls contentControls = null;

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PhieuXuLyCongViecNoiBo.docx");
            string tempPathCopy = HttpContext.Current.Server.MapPath("~/Templates/PhieuXuLyCongViecNoiBo_Copy" + string.Format("{0:yyyyMMddhhmmsss}.docx", DateTime.Now));
            System.IO.File.Copy(templateDocument, tempPathCopy);
            string documentName = string.Format("PhieuXuLyCongViecNoiBo_{0:ddMMyyyyhhmmsss}.docx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);
            try
            {
                object _MissingValue = System.Reflection.Missing.Value;
                //Create an instance for word app  
                appWord = new Application();
                docWord = new Document();
                docWord = appWord.Documents.Open(tempPathCopy, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue,
                    _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue);
                docWord.Activate();
                contentControls = docWord.ContentControls;
                List<ContentControl> lstTemp = new List<ContentControl>();
                for (int i = 1; i <= contentControls.Count; i++)
                {
                    lstTemp.Add(contentControls[i]);
                }
                foreach (var contentControl in lstTemp)
                {
                    if (contentControl.Title == "txtChuDauTu")
                    {
                        contentControl.Range.Text = duAn.ChuDauTu?.TenChuDauTu;
                    }
                    if (contentControl.Title == "txtTenDuAn")
                    {
                        contentControl.Range.Text = duAn.TenDuAn;
                    }
                    if (contentControl.Title == "txtDiaDiem")
                    {
                        contentControl.Range.Text = duAn.DiaDiem;
                    }
                    Marshal.ReleaseComObject(contentControl);
                }
                int stt = 1;
                int indexRowTable = 3;
                string uID = User.Identity.GetUserId();
                var modelParent = model.Where(x => x.IDPhieuCha == null);
                foreach (var item in modelParent)
                {
                    var lstCBTrucTiep = item.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 0).Select(x => x.FullName);
                    string CBTrucTiep = lstCBTrucTiep.Count() > 0 ? string.Join(";", lstCBTrucTiep) : "";
                    var lstTPTrucTiep = item.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 1).Select(x => x.FullName);
                    string TPTrucTiep = lstTPTrucTiep.Count() > 0 ? string.Join(";", lstTPTrucTiep) : "";
                    var lstCBPhoiHop = item.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 2).Select(x => x.FullName);
                    string CBPhoiHop = lstCBPhoiHop.Count() > 0 ? string.Join(";", lstCBPhoiHop) : "";
                    var lstTPPhoiHop = item.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 3).Select(x => x.FullName);
                    string TPPhoiHop = lstTPPhoiHop.Count() > 0 ? string.Join(";", lstTPPhoiHop) : "";
                    //var lstLanhDao = item.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 4).Select(x => x.FullName);
                    //string LanhDao = lstLanhDao.Count() > 0 ? string.Join(";", lstLanhDao) : "";
                    string ngayNhan = "", ngayChuyen = "";
                    if(uID != item.CreatedBy)
                    {
                        ngayNhan = item.NgayNhan != null ? Convert.ToDateTime(item.NgayChuyen).ToString("dd/MM/yyyy hh:mm:ss") : "";
                    }
                    else
                    {
                        ngayChuyen = item.NgayNhan != null ? Convert.ToDateTime(item.NgayChuyen).ToString("dd/MM/yyyy hh:mm:ss") : "";
                    }

                    //do data vao dong dau tien
                    if (stt == 1)
                    {
                        docWord.Tables[2].Cell(3, 1).Range.FormattedText.Text = stt.ToString();
                        docWord.Tables[2].Cell(3, 2).Range.FormattedText.Text = ngayNhan;
                        docWord.Tables[2].Cell(3, 3).Range.FormattedText.Text = item.NoiDung;
                        docWord.Tables[2].Cell(3, 4).Range.FormattedText.Text = ngayChuyen;
                        docWord.Tables[2].Cell(3, 5).Range.FormattedText.Text = CBTrucTiep;
                        docWord.Tables[2].Cell(3, 6).Range.FormattedText.Text = TPTrucTiep;
                        docWord.Tables[2].Cell(3, 7).Range.FormattedText.Text = CBPhoiHop;
                        docWord.Tables[2].Cell(3, 8).Range.FormattedText.Text = TPPhoiHop;
                    }
                    else
                    {
                        docWord.Tables[2].Rows.Add();
                        docWord.Tables[2].Cell(indexRowTable, 1).Range.FormattedText.Text = stt.ToString();
                        docWord.Tables[2].Cell(indexRowTable, 2).Range.FormattedText.Text = ngayNhan;
                        docWord.Tables[2].Cell(indexRowTable, 3).Range.FormattedText.Text = item.NoiDung;
                        docWord.Tables[2].Cell(indexRowTable, 4).Range.FormattedText.Text = ngayChuyen;
                        docWord.Tables[2].Cell(indexRowTable, 5).Range.FormattedText.Text = CBTrucTiep;
                        docWord.Tables[2].Cell(indexRowTable, 6).Range.FormattedText.Text = TPTrucTiep;
                        docWord.Tables[2].Cell(indexRowTable, 7).Range.FormattedText.Text = CBPhoiHop;
                        docWord.Tables[2].Cell(indexRowTable, 8).Range.FormattedText.Text = TPPhoiHop;
                    }
                    var phieuChild = model.Where(x => x.IDPhieuCha == item.IdPhieuXuLyCongViecNoiBo);
                    if(phieuChild.Count() > 0)
                    {
                        var stt_child = 1;
                        foreach (var child in phieuChild)
                        {
                            indexRowTable++;
                            var lstCBTrucTiep_Child = child.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 0).Select(x => x.FullName);
                            string CBTrucTiep_Child = lstCBTrucTiep_Child.Count() > 0 ? string.Join(";", lstCBTrucTiep_Child) : "";
                            var lstTPTrucTiep_Child = child.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 1).Select(x => x.FullName);
                            string TPTrucTiep_Child = lstTPTrucTiep_Child.Count() > 0 ? string.Join(";", lstTPTrucTiep_Child) : "";
                            var lstCBPhoiHop_Child = child.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 2).Select(x => x.FullName);
                            string CBPhoiHop_Child = lstCBPhoiHop_Child.Count() > 0 ? string.Join(";", lstCBPhoiHop_Child) : "";
                            var lstTPPhoiHop_Child = child.PhieuXuLyCongViecNoiBoUsers.Where(x => x.UserXLCVNoiBoEnum == 3).Select(x => x.FullName);
                            string TPPhoiHop_Child = lstTPPhoiHop_Child.Count() > 0 ? string.Join(";", lstTPPhoiHop_Child) : "";

                            string ngayNhan_Child = "", ngayChuyen_Child = "";
                            if (uID != child.CreatedBy)
                            {
                                ngayNhan_Child = child.NgayNhan != null ? Convert.ToDateTime(child.NgayChuyen).ToString("dd/MM/yyyy hh:mm:ss") : "";
                            }
                            else
                            {
                                ngayChuyen_Child = child.NgayNhan != null ? Convert.ToDateTime(child.NgayChuyen).ToString("dd/MM/yyyy hh:mm:ss") : "";
                            }

                            docWord.Tables[2].Rows.Add();
                            docWord.Tables[2].Cell(indexRowTable, 1).Range.FormattedText.Text = stt.ToString() + "." + stt_child.ToString();
                            docWord.Tables[2].Cell(indexRowTable, 2).Range.FormattedText.Text = ngayNhan_Child;
                            docWord.Tables[2].Cell(indexRowTable, 3).Range.FormattedText.Text = child.NoiDung;
                            docWord.Tables[2].Cell(indexRowTable, 4).Range.FormattedText.Text = ngayChuyen_Child;
                            docWord.Tables[2].Cell(indexRowTable, 5).Range.FormattedText.Text = CBTrucTiep_Child;
                            docWord.Tables[2].Cell(indexRowTable, 6).Range.FormattedText.Text = TPTrucTiep_Child;
                            docWord.Tables[2].Cell(indexRowTable, 7).Range.FormattedText.Text = CBPhoiHop_Child;
                            docWord.Tables[2].Cell(indexRowTable, 8).Range.FormattedText.Text = TPPhoiHop_Child;
                            stt_child++;
                        }
                    }
                    indexRowTable++;
                    stt++;
                }
                
                docWord.SaveAs2(fullPath, ref _MissingValue, ref _MissingValue, ref _MissingValue,
                     ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue,
                      ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue);
            }
            catch (Exception ex)
            {
                            string Error = ex.ToString();
            }
            finally
            {
                docWord?.Close();
                appWord?.Quit();
                if (System.IO.File.Exists(tempPathCopy))
                {
                    System.IO.File.Delete(tempPathCopy);
                }
                if (contentControls != null) Marshal.ReleaseComObject(contentControls);
                if (docWord != null) Marshal.ReleaseComObject(docWord);
                if (appWord != null) Marshal.ReleaseComObject(appWord);
            }
            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
        }
    }
}
