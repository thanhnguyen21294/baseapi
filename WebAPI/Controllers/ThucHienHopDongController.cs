﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/thuchienhopdong")]
    [Authorize]
    public class ThucHienHopDongController : ApiControllerBase
    {
        private IThucHienHopDongService _ThucHienHopDongService;
        private IThanhToanHopDongService _ThanhToanHopDongService;
        public ThucHienHopDongController(IErrorService errorService, IThucHienHopDongService thucHienHopDongService, IThanhToanHopDongService thanhToanHopDongService) : base(errorService)
        {
            this._ThucHienHopDongService = thucHienHopDongService;
            _ThanhToanHopDongService = thanhToanHopDongService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThucHienHopDongService.GetAll().OrderByDescending(x => x.IdThucHien);

                var modelVm = Mapper.Map<IEnumerable<ThucHienHopDong>, IEnumerable<ThucHienHopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _ThucHienHopDongService.GetByFilterNew(idDuAn, page, pageSize, out totalRow, filter).ToList();
                var modelVm = Mapper.Map<IEnumerable<ThucHienHopDong>, IEnumerable<ThucHienHopDongViewModels>>(model).ToList();
                var grpHD = modelVm.Where(x => x.IdHopDong != null).GroupBy(x => new { x.IdHopDong, x.HopDong.TenHopDong }).Select(x => new GroupThucHienHopDongViewModel
                {
                    Ten = x.Key.TenHopDong,
                    ThucHienHopDongs = x.Select(y => y)
                });
                var grpCP = modelVm.Where(x => x.IdHopDong == null && x.IdLoaiChiPhi != null).GroupBy(x => new { x.IdLoaiChiPhi, x.LoaiChiPhi.TenLoaiChiPhi }).Select(x => new GroupThucHienHopDongViewModel
                {
                    Ten = x.Key.TenLoaiChiPhi,
                    ThucHienHopDongs = x.Select(y => y)
                });
                grpHD = grpHD.Union(grpCP);
                PaginationSet<GroupThucHienHopDongViewModel> pagedSet = new PaginationSet<GroupThucHienHopDongViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = grpHD
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getbyidhopdong")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idHopDong)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThucHienHopDongService.GetByIdHopDong(idHopDong);
                var modelVm = Mapper.Map<IEnumerable<ThucHienHopDong>, IEnumerable<ThucHienHopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ThucHienHopDongService.GetById(id);

                var responseData = Mapper.Map<ThucHienHopDong, ThucHienHopDongViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, ThucHienHopDongViewModels vmThucHienHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThucHienHopDong = new ThucHienHopDong();
                    newThucHienHopDong.IdHopDong = vmThucHienHopDongViewModels.IdHopDong;
                    newThucHienHopDong.IdDuAn = vmThucHienHopDongViewModels.IdDuAn;
                    newThucHienHopDong.NoiDung = vmThucHienHopDongViewModels.NoiDung;
                    newThucHienHopDong.IdLoaiChiPhi = vmThucHienHopDongViewModels.IdLoaiChiPhi;
                    if (vmThucHienHopDongViewModels.ThoiDiemBaoCao != "" && vmThucHienHopDongViewModels.ThoiDiemBaoCao != null)
                        newThucHienHopDong.ThoiDiemBaoCao = DateTime.ParseExact(vmThucHienHopDongViewModels.ThoiDiemBaoCao, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmThucHienHopDongViewModels.UocDenNgay != "" && vmThucHienHopDongViewModels.UocDenNgay != null)
                        newThucHienHopDong.UocDenNgay = DateTime.ParseExact(vmThucHienHopDongViewModels.UocDenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newThucHienHopDong.KhoiLuong = vmThucHienHopDongViewModels.KhoiLuong;
                    newThucHienHopDong.UocThucHien = vmThucHienHopDongViewModels.UocThucHien;
                    if (vmThucHienHopDongViewModels.NgayNghiemThu != "" && vmThucHienHopDongViewModels.NgayNghiemThu != null)
                        newThucHienHopDong.NgayNghiemThu = DateTime.ParseExact(vmThucHienHopDongViewModels.NgayNghiemThu, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newThucHienHopDong.GiaTriNghiemThu = vmThucHienHopDongViewModels.GiaTriNghiemThu;
                    newThucHienHopDong.GhiChu = vmThucHienHopDongViewModels.GhiChu;
                    newThucHienHopDong.CreatedDate = DateTime.Now;
                    newThucHienHopDong.CreatedBy = User.Identity.GetUserId();
                    newThucHienHopDong.Status = true;

                    _ThucHienHopDongService.Add(newThucHienHopDong);
                    _ThucHienHopDongService.Save();

                    var responseData = Mapper.Map<ThucHienHopDong, ThucHienHopDongViewModels>(newThucHienHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThucHienHopDongViewModels vmThucHienHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThucHienHopDong = _ThucHienHopDongService.GetById(vmThucHienHopDongViewModels.IdThucHien);
                    updateThucHienHopDong.IdHopDong = vmThucHienHopDongViewModels.IdHopDong;
                    updateThucHienHopDong.IdDuAn = vmThucHienHopDongViewModels.IdDuAn;
                    updateThucHienHopDong.NoiDung = vmThucHienHopDongViewModels.NoiDung;
                    updateThucHienHopDong.IdLoaiChiPhi = vmThucHienHopDongViewModels.IdLoaiChiPhi;
                    if (vmThucHienHopDongViewModels.ThoiDiemBaoCao != "" && vmThucHienHopDongViewModels.ThoiDiemBaoCao != null)
                    {
                        updateThucHienHopDong.ThoiDiemBaoCao = DateTime.ParseExact(vmThucHienHopDongViewModels.ThoiDiemBaoCao, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateThucHienHopDong.ThoiDiemBaoCao = null;
                    }
                        
                    if (vmThucHienHopDongViewModels.UocDenNgay != "" && vmThucHienHopDongViewModels.UocDenNgay != null)
                    {
                        updateThucHienHopDong.UocDenNgay = DateTime.ParseExact(vmThucHienHopDongViewModels.UocDenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateThucHienHopDong.UocDenNgay = null;
                    }
                        
                    updateThucHienHopDong.KhoiLuong = vmThucHienHopDongViewModels.KhoiLuong;
                    updateThucHienHopDong.UocThucHien = vmThucHienHopDongViewModels.UocThucHien;
                    if (vmThucHienHopDongViewModels.NgayNghiemThu != "" && vmThucHienHopDongViewModels.NgayNghiemThu != null)
                    {
                        updateThucHienHopDong.NgayNghiemThu = DateTime.ParseExact(vmThucHienHopDongViewModels.NgayNghiemThu, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateThucHienHopDong.NgayNghiemThu = null;
                    }
                        
                    updateThucHienHopDong.GiaTriNghiemThu = vmThucHienHopDongViewModels.GiaTriNghiemThu;
                    updateThucHienHopDong.GhiChu = vmThucHienHopDongViewModels.GhiChu;
                    updateThucHienHopDong.UpdatedDate = DateTime.Now;
                    updateThucHienHopDong.UpdatedBy = User.Identity.GetUserId();

                    _ThucHienHopDongService.Update(updateThucHienHopDong);
                    _ThucHienHopDongService.Save();

                    var responseData = Mapper.Map<ThucHienHopDong, ThucHienHopDongViewModels>(updateThucHienHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });
        }


        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThucHienHopDong = _ThucHienHopDongService.GetById(vnTrangThaiUp.ID);
                    updateThucHienHopDong.TrangThaiGui = 1;
                    updateThucHienHopDong.UpdatedDate = DateTime.Now;
                    updateThucHienHopDong.UpdatedBy = User.Identity.GetUserId();
                    _ThucHienHopDongService.Update(updateThucHienHopDong);
                    _ThucHienHopDongService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _ThucHienHopDongService.Delete(id);
                    _ThucHienHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THUCHIENHOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDThucHien = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDThucHien)
                    {
                        var lstThanhToanHopDong = _ThanhToanHopDongService.GetByIdThucHien(item);
                        if (lstThanhToanHopDong.Count() > 0) {
                            foreach(var itemTT in lstThanhToanHopDong)
                            {
                                itemTT.IdThucHien = null;
                            }
                        }
                        _ThucHienHopDongService.Delete(item);
                    }

                    _ThucHienHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listIDThucHien.Count);
                }

                return response;
            });
        }
    }
}
