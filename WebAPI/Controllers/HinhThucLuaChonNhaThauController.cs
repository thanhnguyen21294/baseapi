﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hinhthucluachonnhathau")]
    [Authorize]
    public class HinhThucLuaChonNhaThauController : ApiControllerBase
    {
        private IHinhThucLuaChonNhaThauService _hinhThucLuaChonNhaThauService;
        public HinhThucLuaChonNhaThauController(IErrorService errorService, IHinhThucLuaChonNhaThauService hinhThucLuaChonNhaThauService) : base(errorService)
        {
            this._hinhThucLuaChonNhaThauService = hinhThucLuaChonNhaThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hinhThucLuaChonNhaThauService.GetAll().OrderByDescending(x => x.IdHinhThucLuaChon);

                var modelVm = Mapper.Map<IEnumerable<HinhThucLuaChonNhaThau>, IEnumerable<HinhThucLuaChonNhaThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hinhThucLuaChonNhaThauService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<HinhThucLuaChonNhaThau>, IEnumerable<HinhThucLuaChonNhaThauViewModels>>(model);

                PaginationSet<HinhThucLuaChonNhaThauViewModels> pagedSet = new PaginationSet<HinhThucLuaChonNhaThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hinhThucLuaChonNhaThauService.GetById(id);

                var responseData = Mapper.Map<HinhThucLuaChonNhaThau, HinhThucLuaChonNhaThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, HinhThucLuaChonNhaThauViewModels hinhThucLuaChonNhaThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHinhThucLuaChonNhaThau = new HinhThucLuaChonNhaThau();
                    newHinhThucLuaChonNhaThau.UpdateHinhThucLuaChonNhaThau(hinhThucLuaChonNhaThauViewModels);
                    newHinhThucLuaChonNhaThau.CreatedDate = DateTime.Now;
                    newHinhThucLuaChonNhaThau.CreatedBy = User.Identity.GetUserId();
                    _hinhThucLuaChonNhaThauService.Add(newHinhThucLuaChonNhaThau);
                    _hinhThucLuaChonNhaThauService.Save();

                    var responseData = Mapper.Map<HinhThucLuaChonNhaThau, HinhThucLuaChonNhaThauViewModels>(newHinhThucLuaChonNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, HinhThucLuaChonNhaThauViewModels hinhThucLuaChonNhaThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbHinhThucLuaChonNhaThau = _hinhThucLuaChonNhaThauService.GetById(hinhThucLuaChonNhaThauViewModels.IdHinhThucLuaChon);

                    dbHinhThucLuaChonNhaThau.UpdateHinhThucLuaChonNhaThau(hinhThucLuaChonNhaThauViewModels);
                    dbHinhThucLuaChonNhaThau.UpdatedDate = DateTime.Now;
                    dbHinhThucLuaChonNhaThau.UpdatedBy = User.Identity.GetUserId();
                    _hinhThucLuaChonNhaThauService.Update(dbHinhThucLuaChonNhaThau);
                    _hinhThucLuaChonNhaThauService.Save();

                    var responseData = Mapper.Map<HinhThucLuaChonNhaThau, HinhThucLuaChonNhaThauViewModels>(dbHinhThucLuaChonNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hinhThucLuaChonNhaThauService.Delete(id);
                    _hinhThucLuaChonNhaThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCLUACHONNHATHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listHinhThucLuaChonNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listHinhThucLuaChonNhaThau)
                    {
                        _hinhThucLuaChonNhaThauService.Delete(item);
                    }

                    _hinhThucLuaChonNhaThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listHinhThucLuaChonNhaThau.Count);
                }

                return response;
            });
        }
    }
}
