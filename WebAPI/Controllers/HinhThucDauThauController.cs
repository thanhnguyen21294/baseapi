﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hinhthucdauthau")]
    [Authorize]
    public class HinhThucDauThauController : ApiControllerBase
    {
        private IHinhThucDauThauService _hinhThucDauThauService;
        public HinhThucDauThauController(IErrorService errorService, IHinhThucDauThauService hinhThucDauThauService) : base(errorService)
        {
            this._hinhThucDauThauService = hinhThucDauThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hinhThucDauThauService.GetAll().OrderByDescending(x => x.IdHinhThucDauThau);

                var modelVm = Mapper.Map<IEnumerable<HinhThucDauThau>, IEnumerable<HinhThucDauThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hinhThucDauThauService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<HinhThucDauThau>, IEnumerable<HinhThucDauThauViewModels>>(model);

                PaginationSet<HinhThucDauThauViewModels> pagedSet = new PaginationSet<HinhThucDauThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hinhThucDauThauService.GetById(id);

                var responseData = Mapper.Map<HinhThucDauThau, HinhThucDauThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, HinhThucDauThauViewModels hinhThucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHinhThucDauThau = new HinhThucDauThau();
                    newHinhThucDauThau.UpdateHinhThucDauThau(hinhThucDauThauViewModels);
                    newHinhThucDauThau.CreatedDate = DateTime.Now;
                    newHinhThucDauThau.CreatedBy = User.Identity.GetUserId();
                    _hinhThucDauThauService.Add(newHinhThucDauThau);
                    _hinhThucDauThauService.Save();

                    var responseData = Mapper.Map<HinhThucDauThau, HinhThucDauThauViewModels>(newHinhThucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, HinhThucDauThauViewModels hinhThucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbHinhThucDauThau = _hinhThucDauThauService.GetById(hinhThucDauThauViewModels.IdHinhThucDauThau);

                    dbHinhThucDauThau.UpdateHinhThucDauThau(hinhThucDauThauViewModels);
                    dbHinhThucDauThau.UpdatedDate = DateTime.Now;
                    dbHinhThucDauThau.UpdatedBy = User.Identity.GetUserId();
                    _hinhThucDauThauService.Update(dbHinhThucDauThau);
                    _hinhThucDauThauService.Save();

                    var responseData = Mapper.Map<HinhThucDauThau, HinhThucDauThauViewModels>(dbHinhThucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hinhThucDauThauService.Delete(id);
                    _hinhThucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCDAUTHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listHinhThucDauThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listHinhThucDauThau)
                    {
                        _hinhThucDauThauService.Delete(item);
                    }

                    _hinhThucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listHinhThucDauThau.Count);
                }

                return response;
            });
        }
    }
}
