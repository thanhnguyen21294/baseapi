﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tiendothuchienhopdong")]
    public class TienDoThucHienHopDongController : ApiControllerBase
    {
        public IDuAnService _duAnService;
        public TienDoThucHienHopDongController(IDuAnService duAnService, IErrorService errorService):base(errorService)
        {
            _duAnService = duAnService;
        }
        [HttpGet]
        [Route("getall")]
        public HttpResponseMessage GetAllTienDoThucHienHopDong(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }


                var tiendothuchienhopdong = _duAnService.GetAllTienDoThucHienHopDong(idNhomDuAnTheoUser).Select(x =>
                new
                {
                    x.TenDuAn,
                    HopDongs = x.HopDongs.Select(hopdong => new
                    {
                        hopdong.TenHopDong,
                        hopdong.NgayBatDau,
                        hopdong.ThoiGianThucHien,
                        TienDoThucHiens = hopdong.TienDoThucHiens.Select(tiendothuchien => new {
                            tiendothuchien.TenCongViec,
                            tiendothuchien.TuNgay,
                            tiendothuchien.DenNgay,
                            TienDoChiTiets= tiendothuchien.TienDoChiTiets.Select(tiendochitiet => new {
                                tiendochitiet.NoiDung,
                                tiendochitiet.TonTaiVuongMac,
                                tiendochitiet.NgayThucHien,
                            })
                        })
                    })
                        
                });
                response = request.CreateResponse(HttpStatusCode.OK, tiendothuchienhopdong);
                return response;

            });
        }

    }
}
