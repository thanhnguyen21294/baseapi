﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/loaihopdong")]
    [Authorize]
    public class LoaiHopDongController : ApiControllerBase
    {
        private ILoaiHopDongService _loaiHopDongService;
        public LoaiHopDongController(IErrorService errorService, ILoaiHopDongService loaiHopDongService) : base(errorService)
        {
            this._loaiHopDongService = loaiHopDongService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _loaiHopDongService.GetAll().OrderByDescending(x => x.IdLoaiHopDong);

                var modelVm = Mapper.Map<IEnumerable<LoaiHopDong>, IEnumerable<LoaiHopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAIHOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _loaiHopDongService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LoaiHopDong>, IEnumerable<LoaiHopDongViewModels>>(model);

                PaginationSet<LoaiHopDongViewModels> pagedSet = new PaginationSet<LoaiHopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAIHOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _loaiHopDongService.GetById(id);

                var responseData = Mapper.Map<LoaiHopDong, LoaiHopDongViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LOAIHOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, LoaiHopDongViewModels loaiHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLoaiHopDong = new LoaiHopDong();
                    newLoaiHopDong.UpdateLoaiHopDong(loaiHopDongViewModels);
                    newLoaiHopDong.CreatedDate = DateTime.Now;
                    newLoaiHopDong.CreatedBy = User.Identity.GetUserId();
                    _loaiHopDongService.Add(newLoaiHopDong);
                    _loaiHopDongService.Save();

                    var responseData = Mapper.Map<LoaiHopDong, LoaiHopDongViewModels>(newLoaiHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LOAIHOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, LoaiHopDongViewModels loaiHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLoaiHopDong = _loaiHopDongService.GetById(loaiHopDongViewModels.IdLoaiHopDong);

                    dbLoaiHopDong.UpdateLoaiHopDong(loaiHopDongViewModels);
                    dbLoaiHopDong.UpdatedDate = DateTime.Now;
                    dbLoaiHopDong.UpdatedBy = User.Identity.GetUserId();
                    _loaiHopDongService.Update(dbLoaiHopDong);
                    _loaiHopDongService.Save();

                    var responseData = Mapper.Map<LoaiHopDong, LoaiHopDongViewModels>(dbLoaiHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAIHOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _loaiHopDongService.Delete(id);
                    _loaiHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAIHOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLoaiHopDong = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLoaiHopDong)
                    {
                        _loaiHopDongService.Delete(item);
                    }

                    _loaiHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLoaiHopDong.Count);
                }

                return response;
            });
        }
    }
}
