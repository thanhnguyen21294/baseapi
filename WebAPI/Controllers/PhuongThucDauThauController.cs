﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/phuongthucdauthau")]
    [Authorize]
    public class PhuongThucDauThauController : ApiControllerBase
    {
        private IPhuongThucDauThauService _phuongThucDauThauService;
        public PhuongThucDauThauController(IErrorService errorService, IPhuongThucDauThauService phuongThucDauThauService) : base(errorService)
        {
            this._phuongThucDauThauService = phuongThucDauThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _phuongThucDauThauService.GetAll().OrderByDescending(x => x.IdPhuongThucDauThau);

                var modelVm = Mapper.Map<IEnumerable<PhuongThucDauThau>, IEnumerable<PhuongThucDauThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _phuongThucDauThauService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<PhuongThucDauThau>, IEnumerable<PhuongThucDauThauViewModels>>(model);

                PaginationSet<PhuongThucDauThauViewModels> pagedSet = new PaginationSet<PhuongThucDauThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _phuongThucDauThauService.GetById(id);

                var responseData = Mapper.Map<PhuongThucDauThau, PhuongThucDauThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, PhuongThucDauThauViewModels phuongThucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newPhuongThucDauThau = new PhuongThucDauThau();
                    newPhuongThucDauThau.UpdatePhuongThucDauThau(phuongThucDauThauViewModels);
                    newPhuongThucDauThau.CreatedDate = DateTime.Now;
                    newPhuongThucDauThau.CreatedBy = User.Identity.GetUserId();
                    _phuongThucDauThauService.Add(newPhuongThucDauThau);
                    _phuongThucDauThauService.Save();

                    var responseData = Mapper.Map<PhuongThucDauThau, PhuongThucDauThauViewModels>(newPhuongThucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, PhuongThucDauThauViewModels phuongThucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbPhuongThucDauThau = _phuongThucDauThauService.GetById(phuongThucDauThauViewModels.IdPhuongThucDauThau);

                    dbPhuongThucDauThau.UpdatePhuongThucDauThau(phuongThucDauThauViewModels);
                    dbPhuongThucDauThau.UpdatedDate = DateTime.Now;
                    dbPhuongThucDauThau.UpdatedBy = User.Identity.GetUserId();
                    _phuongThucDauThauService.Update(dbPhuongThucDauThau);
                    _phuongThucDauThauService.Save();

                    var responseData = Mapper.Map<PhuongThucDauThau, PhuongThucDauThauViewModels>(dbPhuongThucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phuongThucDauThauService.Delete(id);
                    _phuongThucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "PHUONGTHUCDAUTHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listPhuongThucDauThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listPhuongThucDauThau)
                    {
                        _phuongThucDauThauService.Delete(item);
                    }

                    _phuongThucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listPhuongThucDauThau.Count);
                }

                return response;
            });
        }
    }
}
