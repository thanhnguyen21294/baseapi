﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/coquanpheduyet")]
    [Authorize]
    public class CoQuanPheDuyetController : ApiControllerBase
    {
        private ICoQuanPheDuyetService _coQuanPheDuyetService;
        public CoQuanPheDuyetController(IErrorService errorService, ICoQuanPheDuyetService coQuanPheDuyetService) : base(errorService)
        {
            this._coQuanPheDuyetService = coQuanPheDuyetService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _coQuanPheDuyetService.GetAll().OrderByDescending(x => x.IdCoQuanPheDuyet);

                var modelVm = Mapper.Map<IEnumerable<CoQuanPheDuyet>, IEnumerable<CoQuanPheDuyetViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _coQuanPheDuyetService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<CoQuanPheDuyet>, IEnumerable<CoQuanPheDuyetViewModels>>(model);

                PaginationSet<CoQuanPheDuyetViewModels> pagedSet = new PaginationSet<CoQuanPheDuyetViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _coQuanPheDuyetService.GetById(id);

                var responseData = Mapper.Map<CoQuanPheDuyet, CoQuanPheDuyetViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage Create(HttpRequestMessage request, CoQuanPheDuyetViewModels coQuanPheDuyetViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newCoQuanPheDuyet = new CoQuanPheDuyet();
                    newCoQuanPheDuyet.UpdateCoQuanPheDuyet(coQuanPheDuyetViewModels);
                    newCoQuanPheDuyet.CreatedDate = DateTime.Now;
                    newCoQuanPheDuyet.CreatedBy = User.Identity.GetUserId();
                    _coQuanPheDuyetService.Add(newCoQuanPheDuyet);
                    _coQuanPheDuyetService.Save();

                    var responseData = Mapper.Map<CoQuanPheDuyet, CoQuanPheDuyetViewModels>(newCoQuanPheDuyet);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage Update(HttpRequestMessage request, CoQuanPheDuyetViewModels coQuanPheDuyetViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbCoQuanPheDuyet = _coQuanPheDuyetService.GetById(coQuanPheDuyetViewModels.IdCoQuanPheDuyet);

                    dbCoQuanPheDuyet.UpdateCoQuanPheDuyet(coQuanPheDuyetViewModels);
                    dbCoQuanPheDuyet.UpdatedDate = DateTime.Now;
                    dbCoQuanPheDuyet.UpdatedBy = User.Identity.GetUserId();
                    _coQuanPheDuyetService.Update(dbCoQuanPheDuyet);
                    _coQuanPheDuyetService.Save();

                    var responseData = Mapper.Map<CoQuanPheDuyet, CoQuanPheDuyetViewModels>(dbCoQuanPheDuyet);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _coQuanPheDuyetService.Delete(id);
                    _coQuanPheDuyetService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "COQUANPHEDUYET")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listCoQuanPheDuyet = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listCoQuanPheDuyet)
                    {
                        _coQuanPheDuyetService.Delete(item);
                    }

                    _coQuanPheDuyetService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listCoQuanPheDuyet.Count);
                }

                return response;
            });
        }
    }
}
