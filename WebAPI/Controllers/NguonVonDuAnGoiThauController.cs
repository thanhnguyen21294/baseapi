﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/nguonvonduangoithau")]
    [Authorize]
    public class NguonVonDuAnGoiThauController : ApiControllerBase
    {
        private INguonVonDuAnGoiThauService _NguonVonDuAnGoiThauService;
        public NguonVonDuAnGoiThauController(IErrorService errorService, INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService) : base(errorService)
        {
            _NguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
        }
        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _NguonVonDuAnGoiThauService.GetAll().OrderByDescending(x => x.IdNguonVonDuAn);

                var modelVm = Mapper.Map<IEnumerable<NguonVonDuAnGoiThau>, IEnumerable<NguonVonDuAnGoiThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _NguonVonDuAnGoiThauService.GetById(id);

                var responseData = Mapper.Map<NguonVonDuAnGoiThau, NguonVonDuAnGoiThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyidgoithau")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idGoiThau)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _NguonVonDuAnGoiThauService.GetListByIdGoiThau(idGoiThau).OrderByDescending(x => x.IdNguonVonDuAnGoiThau);
                var modelVm = Mapper.Map<IEnumerable<NguonVonDuAnGoiThau>, IEnumerable<NguonVonDuAnGoiThauViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonDuAnGoiThauService.Delete(id);
                    _NguonVonDuAnGoiThauService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listNguonVonDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listNguonVonDuAn)
                    {
                        _NguonVonDuAnGoiThauService.Delete(item);
                    }

                    _NguonVonDuAnGoiThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listNguonVonDuAn.Count);
                }
                return response;
            });
        }
    }
}
