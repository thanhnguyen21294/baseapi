﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/loaicapcongtrinh")]
    [Authorize]
    public class LoaiCapCongTrinhController : ApiControllerBase
    {
        private ILoaiCapCongTrinhService _loaiCapCongTrinhService;
        public LoaiCapCongTrinhController(IErrorService errorService, ILoaiCapCongTrinhService loaiCapCongTrinhService) : base(errorService)
        {
            this._loaiCapCongTrinhService = loaiCapCongTrinhService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _loaiCapCongTrinhService.GetAll().OrderByDescending(x => x.IdLoaiCapCongTrinh);

                var modelVm = Mapper.Map<IEnumerable<LoaiCapCongTrinh>, IEnumerable<LoaiCapCongTrinhViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _loaiCapCongTrinhService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LoaiCapCongTrinh>, IEnumerable<LoaiCapCongTrinhViewModels>>(model);

                PaginationSet<LoaiCapCongTrinhViewModels> pagedSet = new PaginationSet<LoaiCapCongTrinhViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _loaiCapCongTrinhService.GetById(id);

                var responseData = Mapper.Map<LoaiCapCongTrinh, LoaiCapCongTrinhViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage Create(HttpRequestMessage request, LoaiCapCongTrinhViewModels loaiCapCongTrinhtVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLoaiCapCongTrinh = new LoaiCapCongTrinh();
                    newLoaiCapCongTrinh.UpdateLoaiCapCongTrinh(loaiCapCongTrinhtVm);
                    newLoaiCapCongTrinh.CreatedDate = DateTime.Now;
                    newLoaiCapCongTrinh.CreatedBy = User.Identity.GetUserId();
                    _loaiCapCongTrinhService.Add(newLoaiCapCongTrinh);
                    _loaiCapCongTrinhService.Save();

                    var responseData = Mapper.Map<LoaiCapCongTrinh, LoaiCapCongTrinhViewModels>(newLoaiCapCongTrinh);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage Update(HttpRequestMessage request, LoaiCapCongTrinhViewModels loaiCapCongTrinhVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLoaiCapCongTrinh = _loaiCapCongTrinhService.GetById(loaiCapCongTrinhVm.IdLoaiCapCongTrinh);

                    dbLoaiCapCongTrinh.UpdateLoaiCapCongTrinh(loaiCapCongTrinhVm);
                    dbLoaiCapCongTrinh.UpdatedDate = DateTime.Now;
                    dbLoaiCapCongTrinh.UpdatedBy = User.Identity.GetUserId();
                    _loaiCapCongTrinhService.Update(dbLoaiCapCongTrinh);
                    _loaiCapCongTrinhService.Save();

                    var responseData = Mapper.Map<LoaiCapCongTrinh, LoaiCapCongTrinhViewModels>(dbLoaiCapCongTrinh);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _loaiCapCongTrinhService.Delete(id);
                    _loaiCapCongTrinhService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAICAPCONGTRINH")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLoaiCapCongTrinh = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLoaiCapCongTrinh)
                    {
                        _loaiCapCongTrinhService.Delete(item);
                    }

                    _loaiCapCongTrinhService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLoaiCapCongTrinh.Count);
                }

                return response;
            });
        }
    }
}
