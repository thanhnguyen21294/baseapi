﻿using AutoMapper;
using Common;
using Microsoft.AspNet.Identity;
using Model.Models;
using Newtonsoft.Json;
using Service;
using Service.GPMBService;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.System;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/actionhistory")]
    public class ActionHistoryController : ApiControllerBase
    {
        public IActionHistoryService _historyService;
        public IDuAnService _duAnService;
        public IKeHoachVonService _keHoachVonService;
        public IKeHoachLuaChonNhaThauService _keHoachLuaChonNhaThauService;
        public IHopDongService _hopDongService;
        public ILapQuanLyDauTuService _lapQuanLyDauTuService;
        public ITienDoThucHienService _tienDoThucHienService;
        public IFunctionService _functionService;
        public IChuDauTuService _chuDauTuService;
        public IQLTD_KeHoachService _qltd_kehoachService;
        public IQLTD_CTCV_ChuTruongDauTuService _qltd_ctcv_ctdtService;
        public IQLTD_CTCV_ChuanBiDauTuService _qltd_ctcv_cbdtService;
        public IQLTD_CTCV_ChuanBiThucHienService _qltd_ctcv_cbthService;
        public IQLTD_CTCV_ThucHienDuAnService _qltd_ctcv_thService;
        public IQLTD_CTCV_QuyetToanDuAnService _qltd_ctcv_qtService;
        public IQLTD_TienDoThucHienService _qltd_tienDoThucHienService;
        public IQLTD_TDTH_ChuanBiDauTuService _qltd_tdth_chuanBiDauTuService;
        public IQLTD_TDTH_ChuanBiThucHienService _qltd_tdth_chuanBiThucHienService;
        public IQLTD_TDTH_ThucHienDuAnService _qltd_tdth_thucHienDuAnService;
        public IQLTD_TDTH_QuyetToanDuAnService _qltd_tdth_quyetToanDuAnService;

        public IGPMB_KeHoachService _gpmb_kehoachService;
        public IGPMB_FileService _gpmb_fileService;

        public ActionHistoryController(IErrorService errorService, IActionHistoryService historyService,
            IDuAnService duAnService, IKeHoachVonService keHoachVonService,
            IKeHoachLuaChonNhaThauService keHoachLuaChonNhaThauService, IHopDongService hopDongService,
            ILapQuanLyDauTuService lapQuanLyDauTuService, ITienDoThucHienService tienDoThucHienService,
            IFunctionService functionService, IChuDauTuService chuDauTuService,
            IQLTD_KeHoachService qltd_kehoachService,
            IQLTD_CTCV_ChuTruongDauTuService qltd_ctcv_ctdtService,
            IQLTD_CTCV_ChuanBiDauTuService qltd_ctcv_cbdtService,
            IQLTD_CTCV_ChuanBiThucHienService qltd_ctcv_cbthService,
            IQLTD_CTCV_ThucHienDuAnService qltd_ctcv_thService,
            IQLTD_CTCV_QuyetToanDuAnService qltd_ctcv_qtService,
            IQLTD_TienDoThucHienService qltd_tienDoThucHienService,
            IQLTD_TDTH_ChuanBiDauTuService qltd_tdth_chuanBiDauTuService,
            IQLTD_TDTH_ChuanBiThucHienService qltd_tdth_chuanBiThucHienService,
            IQLTD_TDTH_ThucHienDuAnService qltd_tdth_thucHienDuAnService,
            IQLTD_TDTH_QuyetToanDuAnService qltd_tdth_quyetToanDuAnService,
            IGPMB_KeHoachService gpmb_kehoachService,
            IGPMB_FileService gpmb_fileService
            ) : base(errorService)
        {
            this._historyService = historyService;
            this._duAnService = duAnService;
            this._keHoachVonService = keHoachVonService;
            this._keHoachLuaChonNhaThauService = keHoachLuaChonNhaThauService;
            this._hopDongService = hopDongService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
            this._tienDoThucHienService = tienDoThucHienService;
            this._functionService = functionService;
            this._chuDauTuService = chuDauTuService;
            this._qltd_kehoachService = qltd_kehoachService;
            this._qltd_ctcv_ctdtService = qltd_ctcv_ctdtService;
            this._qltd_ctcv_cbdtService = qltd_ctcv_cbdtService;
            this._qltd_ctcv_cbthService = qltd_ctcv_cbthService;
            this._qltd_ctcv_thService = qltd_ctcv_thService;
            this._qltd_ctcv_qtService = qltd_ctcv_qtService;
            this._qltd_tienDoThucHienService = qltd_tienDoThucHienService;
            this._qltd_tdth_chuanBiDauTuService = qltd_tdth_chuanBiDauTuService;
            this._qltd_tdth_chuanBiThucHienService = qltd_tdth_chuanBiThucHienService;
            this._qltd_tdth_thucHienDuAnService = qltd_tdth_thucHienDuAnService;
            this._qltd_tdth_quyetToanDuAnService = qltd_tdth_quyetToanDuAnService;
            _gpmb_kehoachService = gpmb_kehoachService;
            _gpmb_fileService = gpmb_fileService;
            //this._canceltoken = canceltoken;
        }

        //[Authorize]
        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int page, int pageSize, string filter = null, int loaitimkiem = 0, DateTime? begindate = null, DateTime? enddate = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _historyService.GetAll(page, pageSize, out totalRow, filter, loaitimkiem, begindate, enddate).OrderByDescending(x => x.IdHistory);

                var modelVm = Mapper.Map<IEnumerable<ActionHistory>, IEnumerable<ActionHistoryViewModel>>(model);

                PaginationSet<ActionHistoryViewModel> pagedSet = new PaginationSet<ActionHistoryViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Authorize]
        [Route("getbyiduser")]
        [HttpGet]
        public HttpResponseMessage GetByIdUser(HttpRequestMessage request, string username, int page, int pageSize, string filter = null, int loaitimkiem = 0, DateTime? begindate = null, DateTime? enddate = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _historyService.GetByIdUser(username, page, pageSize, out totalRow, filter, loaitimkiem, begindate, enddate).OrderByDescending(x => x.IdHistory);

                var modelVm = Mapper.Map<IEnumerable<ActionHistory>, IEnumerable<ActionHistoryViewModel>>(model);

                PaginationSet<ActionHistoryViewModel> pagedSet = new PaginationSet<ActionHistoryViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Authorize]
        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, ActionHistoryViewModel historyViewModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newhistory = new ActionHistory();
                    newhistory.UpDateHistory(historyViewModel);
                    newhistory.CreateDate = DateTime.Now;
                    try
                    {
                        //Dùng chung
                        //chủ đầu tư
                        if (newhistory.Function == "/api/chudautu/add" || newhistory.Function == "/api/chudautu/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Chủ đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/chudautu";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenChuDauTu : ") + 15);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/chudautu/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Chủ đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/chudautu";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Cơ quan phê duyệt
                        if (newhistory.Function == "/api/coquanpheduyet/add" || newhistory.Function == "/api/coquanpheduyet/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Cơ quan phê duyệt";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/coquanpheduyet";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenCoQuanPheDuyet : ") + 21);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/coquanpheduyet/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Cơ quan phê duyệt";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/coquanpheduyet";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Giai đoạn dự án
                        if (newhistory.Function == "/api/giaidoanduan/add" || newhistory.Function == "/api/giaidoanduan/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Giai đoạn dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/giaidoanduan";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenGiaiDoanDuAn : ") + 19);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/giaidoanduan/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Giai đoạn dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/giaidoanduan";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Hình thức đấu thầu
                        if (newhistory.Function == "/api/hinhthucdauthau/add" || newhistory.Function == "/api/hinhthucdauthau/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucdauthau";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenHinhThucDauThau : ") + 22);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hinhthucdauthau/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucdauthau";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Hình thức đầu tư
                        if (newhistory.Function == "/api/hinhthucdautu/add" || newhistory.Function == "/api/hinhthucdautu/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucdautu";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenHinhThucDauTu : ") + 20);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hinhthucdautu/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucdautu";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Hình thức lựa chọn nhà thầu
                        if (newhistory.Function == "/api/hinhthucluachonnhathau/add" || newhistory.Function == "/api/hinhthucluachonnhathau/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức lựa chọn nhà thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucluachonnhathau";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenHinhThucLuaChon : ") + 22);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hinhthucluachonnhathau/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức lựa chọn nhà thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucluachonnhathau";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Hình thức quản lý dự án
                        if (newhistory.Function == "/api/hinhthucquanly/add" || newhistory.Function == "/api/hinhthucquanly/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức quản lý dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucquanly";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenHinhThucQuanLy : ") + 21);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hinhthucquanly/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Hình thức quản lý dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/hinhthucquanly";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Lĩnh vực đấu thầu
                        if (newhistory.Function == "/api/linhvucdauthau/add" || newhistory.Function == "/api/linhvucdauthau/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Lĩnh vực đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/linhvucdauthau";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenLinhVucDauThau : ") + 21);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/linhvucdauthau/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Lĩnh vực đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/linhvucdauthau";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Lĩnh vực ngành nghề
                        if (newhistory.Function == "/api/linhvucnganhnghe/add" || newhistory.Function == "/api/linhvucnganhnghe/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Lĩnh vực ngành nghề";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/linhvucnganhnghe";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenLinhVucNganhNghe : ") + 23);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/linhvucnganhnghe/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Lĩnh vực ngành nghề";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/linhvucnganhnghe";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Loại cấp công trình
                        if (newhistory.Function == "/api/loaicapcongtrinh/add" || newhistory.Function == "/api/loaicapcongtrinh/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Loại cấp công trình";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/loaicapcongtrinh";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenLoaiCapCongTrinh : ") + 23);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/loaicapcongtrinh/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Loại cấp công trình";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/loaicapcongtrinh";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Loại hợp đồng
                        if (newhistory.Function == "/api/loaihopdong/add" || newhistory.Function == "/api/loaihopdong/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Loại hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/loaihopdong";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenLoaiHopDong : ") + 18);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/loaihopdong/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Loại hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/loaihopdong";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Nguồn vốn
                        if (newhistory.Function == "/api/nguonvon/add" || newhistory.Function == "/api/nguonvon/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Nguồn vốn";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/nguonvon";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenNguonVon : ") + 15);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/nguonvon/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Nguồn vốn";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/nguonvon";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Nhóm dự án
                        if (newhistory.Function == "/api/nhomduan/add" || newhistory.Function == "/api/nhomduan/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Nhóm dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/nhomduan";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenNhomDuAn : ") + 15);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/nhomduan/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Nhóm dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/nhomduan";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Phương thức đấu thầu
                        if (newhistory.Function == "/api/phuongthucdauthau/add" || newhistory.Function == "/api/phuongthucdauthau/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Phương thức đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/phuongthucdauthau";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenPhuongThucDauThau : ") + 24);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/phuongthucdauthau/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Phương thức đấu thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/phuongthucdauthau";
                            newhistory.FunctionID = "DMDC";
                        }
                        //Tình trạng dự án
                        if (newhistory.Function == "/api/tinhtrangduan/add" || newhistory.Function == "/api/tinhtrangduan/update")
                        {
                            newhistory.Function = "Danh mục dùng chung / Tình trạng dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/tinhtrangduan";
                            newhistory.FunctionID = "DMDC";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenTinhTrangDuAn : ") + 20);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tinhtrangduan/deletemulti")
                        {
                            newhistory.Function = "Danh mục dùng chung / Tình trạng dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/dungchung/tinhtrangduan";
                            newhistory.FunctionID = "DMDC";
                        }

                        // QUẢN LÝ DỰ ÁN
                        //Danh sách dự án
                        if (newhistory.Function == "/api/duan/add" || newhistory.Function == "/api/duan/update")
                        {
                            newhistory.Function = "Dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/duan";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duancha = newhistory.Content;
                            if (duancha.IndexOf(" IdDuAnCha :") != -1)
                            {
                                duancha = duancha.Substring(duancha.IndexOf(" IdDuAnCha :") + 12);
                                if (duancha.IndexOf(",") != -1)
                                {
                                    duancha = duancha.Substring(0, duancha.IndexOf(","));
                                    if (duancha.Length > 0 && duancha != " " && duancha != "  " && duancha != "null")
                                    {
                                        var idduancha = Int32.Parse(duancha);
                                        if (idduancha > 0)
                                        {
                                            var tenduancha = _duAnService.GetById(idduancha).TenDuAn;
                                            newhistory.ParentFunction = tenduancha;
                                        }
                                    }
                                }
                            }

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenDuAn : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }

                            var a = newhistory.Content;
                        }
                        if (newhistory.Function == "/api/duan/deletemulti")
                        {
                            newhistory.Function = "Dự án";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/duan";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Kế hoạch vốn
                        if (newhistory.Function == "/api/kehoachvon/add" || newhistory.Function == "/api/kehoachvon/update")
                        {
                            newhistory.Function = "Kế hoạch vốn";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/kehoachvon";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var kehoachvondc = newhistory.Content;
                            var duan = newhistory.Content;
                            if (kehoachvondc.IndexOf("IdKeHoachVonDieuChinh :") != -1)
                            {
                                kehoachvondc = kehoachvondc.Substring(kehoachvondc.IndexOf("IdKeHoachVonDieuChinh :") + 23);
                                if (kehoachvondc.IndexOf(",") != -1)
                                {
                                    kehoachvondc = kehoachvondc.Substring(0, kehoachvondc.IndexOf(","));
                                    if (kehoachvondc != "null")
                                    {
                                        if (kehoachvondc.Length > 0 && kehoachvondc != " " && kehoachvondc != "  ")
                                        {
                                            kehoachvondc = _historyService.RemoveSpace(kehoachvondc);
                                            var idkhvdc = Int32.Parse(kehoachvondc);
                                            if (idkhvdc > 0)
                                            {
                                                var tenkhvdc = _keHoachVonService.GetById(idkhvdc).SoQuyetDinh;
                                                newhistory.Function = "Kế hoạch vốn " + " ( " + tenkhvdc + " )" + " / Kế hoạch vốn điều chỉnh";
                                            }
                                        }
                                    }
                                }
                            }
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.IndexOf(",") != -1)
                            {
                                duan = duan.Substring(0, duan.IndexOf(","));

                                if (duan.Length > 0 && duan != " " && duan != "  ")
                                {
                                    duan = _historyService.RemoveSpace(duan);
                                    var idduan = Int32.Parse(duan);
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                }
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" SoQuyetDinh : ") + 15);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/kehoachvon/delete")
                        {
                            newhistory.Function = "Kế hoạch vốn";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/kehoachvon";
                            newhistory.FunctionID = "QLDA";
                        }
                        if (newhistory.Function == "/api/kehoachvon/deletemulti")
                        {
                            newhistory.Function = "Kế hoạch vốn / Kế hoạch vốn điều chỉnh";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/kehoachvon";
                            newhistory.FunctionID = "QLDA";
                        }

                        #region Quản lý tiến độ
                        //Kế hoạch
                        if (newhistory.Function == "/api/qltd_kehoach/add" || newhistory.Function == "/api/qltd_kehoach/update"
                        || newhistory.Function == "/api/qltd_kehoach/pheduyet")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var giaidoan = newhistory.Content;
                            var duan = newhistory.Content;
                            var noidung = newhistory.Content;

                            if (newhistory.Function == "/api/qltd_kehoach/pheduyet")
                            {
                                if (giaidoan.IndexOf("idGiaiDoan :") != -1)
                                {
                                    giaidoan = giaidoan.Substring(giaidoan.IndexOf("idGiaiDoan :") + 12);
                                    switch (giaidoan)
                                    {
                                        case "2":
                                            newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư / Phê duyệt";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "3":
                                            newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án / Phê duyệt";
                                            newhistory.ParentFunction = " ";

                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "4":
                                            newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án / Phê duyệt";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "5":
                                            newhistory.Function = "Quản lý tiến độ / Thực hiện dự án / Phê duyệt";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        default:
                                            newhistory.Function = "Quản lý tiến độ / Quyết toán dự án / Phê duyệt";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                if (giaidoan.IndexOf("idGiaiDoan :") != -1)
                                {
                                    giaidoan = giaidoan.Substring(giaidoan.IndexOf("idGiaiDoan :") + 12);
                                    switch (giaidoan)
                                    {
                                        case "2":
                                            newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư / Kế hoạch";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "3":
                                            newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án / Kế hoạch";
                                            newhistory.ParentFunction = " ";

                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "4":
                                            newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án / Kế hoạch";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "5":
                                            newhistory.Function = "Quản lý tiến độ / Thực hiện dự án / Kế hoạch";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        default:
                                            newhistory.Function = "Quản lý tiến độ / Quyết toán dự án / Kế hoạch";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                    }
                                }
                            }
                            if (noidung.IndexOf("NoiDung : ") != -1)
                            {
                                noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                if (noidung.IndexOf(",") != -1)
                                {
                                    noidung = noidung.Substring(0, noidung.IndexOf(","));
                                    newhistory.Content = noidung;
                                }
                            }

                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.IndexOf(",") != -1)
                            {
                                duan = duan.Substring(0, duan.IndexOf(","));

                                if (duan.Length > 0 && duan != " " && duan != "  ")
                                {
                                    duan = _historyService.RemoveSpace(duan);
                                    var idduan = Int32.Parse(duan);
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                }
                            }
                        }
                        //if (newhistory.Function == "/api/qltd_kehoach/delete")
                        //{
                        //    newhistory.Function = "Quản lý dự án / Quản lý tiến độ / Chủ trương đầu tư";
                        //    //newhistory.ParentFunction = " ";
                        //    newhistory.FunctionUrl = "/main/quanlyduan/kehoachvon";
                        //    newhistory.FunctionID = "QLDA";

                        //}

                        //Công việc
                        if (newhistory.Function == "/api/qltd_ctcv_chutruongdautu/update"
                || newhistory.Function == "/api/qltd_ctcv_chuanbidautu/update"
                || newhistory.Function == "/api/qltd_ctcv_chuanbithuchien/update"
                || newhistory.Function == "/api/qltd_ctcv_thuchienduan/update"
                || newhistory.Function == "/api/qltd_ctcv_quyettoanduan/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var kehoach = newhistory.Content;
                            var congviec = newhistory.Content;
                            var giaidoan = newhistory.Content;

                            if (giaidoan.IndexOf("IdGiaiDoan :") != -1)
                            {
                                giaidoan = giaidoan.Substring(giaidoan.IndexOf("IdGiaiDoan :") + 12);
                                if (giaidoan.IndexOf(",") != -1)
                                {
                                    giaidoan = giaidoan.Substring(0, giaidoan.IndexOf(","));
                                    switch (giaidoan)
                                    {
                                        case "2":
                                            newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "3":
                                            newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "4":
                                            newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        case "5":
                                            newhistory.Function = "Quản lý tiến độ / Thực hiện dự án";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                        default:
                                            newhistory.Function = "Quản lý tiến độ / Quyết toán dự án";
                                            newhistory.ParentFunction = " ";
                                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                                            newhistory.FunctionID = "QLDA";
                                            break;
                                    }
                                }
                            }

                            if (kehoach.IndexOf("IdKeHoach :") != -1)
                            {
                                kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoach :") + 11);
                                if (kehoach.IndexOf(",") != -1)
                                {
                                    kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                    if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                    {
                                        var idkehoach = Int32.Parse(kehoach);
                                        var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                        var idduan = kehoachs.IdDuAn;
                                        var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                        newhistory.ParentFunction = tenduan;

                                        var tenkehoach = kehoachs.NoiDung;

                                        if (congviec.IndexOf("TenCongViec : ") != -1)
                                        {
                                            congviec = congviec.Substring(congviec.IndexOf("TenCongViec : ") + 14);
                                            if (congviec.IndexOf(",") != -1)
                                            {
                                                congviec = congviec.Substring(0, congviec.IndexOf(","));
                                                newhistory.Content = "Kế hoạch: " + tenkehoach + " . " + "Công việc: " + congviec;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //Khó khăn vướng mắc
                        if (newhistory.Function == "/api/qltd_khokhanvuongmac/add" || newhistory.Function == "/api/qltd_khokhanvuongmac/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var kehoach = newhistory.Content;
                            var nguyenhan = newhistory.Content;
                            var giaiphap = newhistory.Content;
                            var xuly = newhistory.Content;

                            if (kehoach.IndexOf("IdKeHoach :") != -1)
                            {
                                int idkehoach = 0;
                                kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoach :") + 11);

                                if (newhistory.Function == "/api/qltd_khokhanvuongmac/add")
                                {
                                    idkehoach = Convert.ToInt32(kehoach);
                                }
                                else
                                {
                                    if (kehoach.IndexOf(",") != -1)
                                    {
                                        kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                        if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                        {
                                            idkehoach = Int32.Parse(kehoach);
                                        }
                                    }
                                }

                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idGiaiDoan = kehoachs.IdGiaiDoan;
                                switch (idGiaiDoan)
                                {
                                    case 3:
                                        newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                                        newhistory.FunctionID = "QLDA";
                                        break;
                                    case 4:
                                        newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                                        newhistory.FunctionID = "QLDA";
                                        break;
                                    case 5:
                                        newhistory.Function = "Quản lý tiến độ / Thực hiện dự án";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                                        newhistory.FunctionID = "QLDA";
                                        break;
                                    case 6:
                                        newhistory.Function = "Quản lý tiến độ / Quyết toán dự án";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                                        newhistory.FunctionID = "QLDA";
                                        break;
                                    default:
                                        newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                                        newhistory.FunctionID = "QLDA";
                                        break;
                                }

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;

                                var tenkehoach = kehoachs.NoiDung;
                                newhistory.Content = tenkehoach;

                                if (nguyenhan.IndexOf("NguyenNhanLyDo : ") != -1)
                                {
                                    nguyenhan = nguyenhan.Substring(nguyenhan.IndexOf("NguyenNhanLyDo : ") + 16);
                                    if (nguyenhan.IndexOf(",") != -1)
                                    {
                                        nguyenhan = nguyenhan.Substring(0, nguyenhan.IndexOf(","));
                                    }
                                    newhistory.Content = newhistory.Content + "," + nguyenhan;
                                }

                                if (giaiphap.IndexOf("GiaiPhapTrienKhai : ") != -1)
                                {
                                    giaiphap = giaiphap.Substring(giaiphap.IndexOf("GiaiPhapTrienKhai : ") + 19);
                                    if (giaiphap.IndexOf(",") != -1)
                                    {
                                        giaiphap = giaiphap.Substring(0, giaiphap.IndexOf(","));
                                    }
                                    newhistory.Content = newhistory.Content + "," + giaiphap;
                                }

                                if (xuly.IndexOf("DaGiaiQuyet :") != -1)
                                {
                                    xuly = xuly.Substring(xuly.IndexOf("DaGiaiQuyet :") + 13);
                                    if (xuly.IndexOf(",") != -1)
                                    {
                                        xuly = xuly.Substring(0, xuly.IndexOf(","));
                                        if (xuly == "true")
                                        {
                                            xuly = "Đã xử lý";
                                        }
                                        else
                                        {
                                            xuly = "Chưa xử lý";
                                        }
                                    }
                                    newhistory.Content = newhistory.Content + "," + xuly;
                                }
                            }


                            //if (kehoach.IndexOf("IdKeHoach :") != -1)
                            //{
                            //    kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoach :") + 11);

                            //    if (kehoach.IndexOf(",") != -1)
                            //    {
                            //        kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                            //        if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                            //        {
                            //            var idkehoach = Int32.Parse(kehoach);

                            //        }
                            //    }
                            //}
                        }

                        //Tiến độ chung
                        if (newhistory.Function == "/api/qltd_kehoachtiendochung/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var kehoach = newhistory.Content;
                            var noidung = newhistory.Content;

                            if (kehoach.IndexOf("IdKeHoach :") != -1)
                            {
                                kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoach :") + 11);
                                if (kehoach.IndexOf(",") != -1)
                                {
                                    kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                    if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                    {
                                        var idkehoach = Int32.Parse(kehoach);
                                        var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                        var idGiaiDoan = kehoachs.IdGiaiDoan;
                                        switch (idGiaiDoan)
                                        {
                                            case 3:
                                                newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án";
                                                newhistory.ParentFunction = " ";
                                                newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                                                newhistory.FunctionID = "QLDA";
                                                break;
                                            case 4:
                                                newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án";
                                                newhistory.ParentFunction = " ";
                                                newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                                                newhistory.FunctionID = "QLDA";
                                                break;
                                            case 5:
                                                newhistory.Function = "Quản lý tiến độ / Thực hiện dự án";
                                                newhistory.ParentFunction = " ";
                                                newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                                                newhistory.FunctionID = "QLDA";
                                                break;
                                            case 6:
                                                newhistory.Function = "Quản lý tiến độ / Quyết toán dự án";
                                                newhistory.ParentFunction = " ";
                                                newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                                                newhistory.FunctionID = "QLDA";
                                                break;
                                            default:
                                                newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư";
                                                newhistory.ParentFunction = " ";
                                                newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                                                newhistory.FunctionID = "QLDA";
                                                break;
                                        }

                                        var idduan = kehoachs.IdDuAn;
                                        var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                        newhistory.ParentFunction = tenduan;
                                    }
                                }
                            }

                            if (noidung.IndexOf("NoiDung : ") != -1)
                            {
                                noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                newhistory.Content = noidung;
                            }

                        }

                        //Tiến độ thực hiện chi tiết
                        ////Chủ trương dầu tư
                        if (newhistory.Function == "/api/qltd_tdth_chutruongdautu/add" || newhistory.Function == "/api/qltd_tdth_chutruongdautu/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                            newhistory.FunctionID = "QLDA";

                            var chitietcongviec = newhistory.Content;
                            var noidung = newhistory.Content;
                            if (chitietcongviec.IndexOf("IdChiTietCongViec :") != -1)
                            {
                                int idchitietcongviec = 0;
                                chitietcongviec = chitietcongviec.Substring(chitietcongviec.IndexOf("IdChiTietCongViec :") + 19);

                                if (newhistory.Function == "/api/qltd_tdth_chutruongdautu/add")
                                {
                                    idchitietcongviec = Convert.ToInt32(chitietcongviec);
                                }
                                else
                                {
                                    if (chitietcongviec.IndexOf(",") != -1)
                                    {
                                        chitietcongviec = chitietcongviec.Substring(0, chitietcongviec.IndexOf(","));

                                        if (chitietcongviec.Length > 0 && chitietcongviec != " " && chitietcongviec != "  ")
                                        {
                                            idchitietcongviec = Int32.Parse(chitietcongviec);
                                        }
                                    }
                                }

                                var ctcv_ctdt = _qltd_ctcv_ctdtService.GetByID(idchitietcongviec);

                                var idkehoach = ctcv_ctdt.IdKeHoach;
                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                                var tenkehoach = kehoachs.NoiDung;


                                if (noidung.IndexOf("NoiDung : ") != -1)
                                {
                                    noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                    if (noidung.IndexOf(",") != -1)
                                    {
                                        noidung = noidung.Substring(0, noidung.IndexOf(","));
                                        newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung: " + noidung;
                                    }
                                }
                            }


                        }

                        ////Phê duyệt dự án
                        if (newhistory.Function == "/api/qltd_tdth_chuanbidautu/add" || newhistory.Function == "/api/qltd_tdth_chuanbidautu/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                            newhistory.FunctionID = "QLDA";

                            var chitietcongviec = newhistory.Content;
                            var noidung = newhistory.Content;
                            if (chitietcongviec.IndexOf("IdChiTietCongViec :") != -1)
                            {
                                int idchitietcongviec = 0;
                                chitietcongviec = chitietcongviec.Substring(chitietcongviec.IndexOf("IdChiTietCongViec :") + 19);

                                if (newhistory.Function == "/api/qltd_tdth_chuanbidautu/add")
                                {
                                    idchitietcongviec = Convert.ToInt32(chitietcongviec);
                                }
                                else
                                {
                                    if (chitietcongviec.IndexOf(",") != -1)
                                    {
                                        chitietcongviec = chitietcongviec.Substring(0, chitietcongviec.IndexOf(","));

                                        if (chitietcongviec.Length > 0 && chitietcongviec != " " && chitietcongviec != "  ")
                                        {
                                            idchitietcongviec = Int32.Parse(chitietcongviec);
                                        }
                                    }
                                }

                                //var ctcv_ctdt = _qltd_ctcv_ctdtService.GetByID(idchitietcongviec);
                                var ctcv_cbdt = _qltd_ctcv_cbdtService.GetByID(idchitietcongviec);

                                var idkehoach = ctcv_cbdt.IdKeHoach;
                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                                var tenkehoach = kehoachs.NoiDung;


                                if (noidung.IndexOf("NoiDung : ") != -1)
                                {
                                    noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                    if (noidung.IndexOf(",") != -1)
                                    {
                                        noidung = noidung.Substring(0, noidung.IndexOf(","));
                                        newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung: " + noidung;
                                    }
                                }
                            }


                        }

                        ////Chuẩn bị thực hiện
                        if (newhistory.Function == "/api/qltd_tdth_chuanbithuchien/add" || newhistory.Function == "/api/qltd_tdth_chuanbithuchien/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                            newhistory.FunctionID = "QLDA";

                            var chitietcongviec = newhistory.Content;
                            var noidung = newhistory.Content;
                            if (chitietcongviec.IndexOf("IdChiTietCongViec :") != -1)
                            {
                                int idchitietcongviec = 0;
                                chitietcongviec = chitietcongviec.Substring(chitietcongviec.IndexOf("IdChiTietCongViec :") + 19);

                                if (newhistory.Function == "/api/qltd_tdth_chuanbithuchien/add")

                                {
                                    idchitietcongviec = Convert.ToInt32(chitietcongviec);
                                }
                                else
                                {
                                    if (chitietcongviec.IndexOf(",") != -1)
                                    {
                                        chitietcongviec = chitietcongviec.Substring(0, chitietcongviec.IndexOf(","));

                                        if (chitietcongviec.Length > 0 && chitietcongviec != " " && chitietcongviec != "  ")
                                        {
                                            idchitietcongviec = Int32.Parse(chitietcongviec);
                                        }
                                    }
                                }

                                //var ctcv_ctdt = _qltd_ctcv_ctdtService.GetByID(idchitietcongviec);
                                //var ctcv_cbdt = _qltd_ctcv_cbdtService.GetByID(idchitietcongviec);
                                var ctcv_cbth = _qltd_ctcv_cbthService.GetByID(idchitietcongviec);

                                var idkehoach = ctcv_cbth.IdKeHoach;
                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                                var tenkehoach = kehoachs.NoiDung;


                                if (noidung.IndexOf("NoiDung : ") != -1)
                                {
                                    noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                    if (noidung.IndexOf(",") != -1)
                                    {
                                        noidung = noidung.Substring(0, noidung.IndexOf(","));
                                        newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung: " + noidung;
                                    }
                                }
                            }


                        }

                        ////Thực hiện dự án
                        if (newhistory.Function == "/api/qltd_tdth_thuchienduan/add" || newhistory.Function == "/api/qltd_tdth_thuchienduan/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Thực hiện dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                            newhistory.FunctionID = "QLDA";

                            var chitietcongviec = newhistory.Content;
                            var noidung = newhistory.Content;
                            if (chitietcongviec.IndexOf("IdChiTietCongViec :") != -1)
                            {
                                int idchitietcongviec = 0;
                                chitietcongviec = chitietcongviec.Substring(chitietcongviec.IndexOf("IdChiTietCongViec :") + 19);

                                if (newhistory.Function == "/api/qltd_tdth_thuchienduan/add")
                                {
                                    idchitietcongviec = Convert.ToInt32(chitietcongviec);
                                }
                                else
                                {
                                    if (chitietcongviec.IndexOf(",") != -1)
                                    {
                                        chitietcongviec = chitietcongviec.Substring(0, chitietcongviec.IndexOf(","));

                                        if (chitietcongviec.Length > 0 && chitietcongviec != " " && chitietcongviec != "  ")
                                        {
                                            idchitietcongviec = Int32.Parse(chitietcongviec);
                                        }
                                    }
                                }

                                var ctcv_th = _qltd_ctcv_thService.GetByID(idchitietcongviec);

                                var idkehoach = ctcv_th.IdKeHoach;
                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                                var tenkehoach = kehoachs.NoiDung;


                                if (noidung.IndexOf("NoiDung : ") != -1)
                                {
                                    noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                    if (noidung.IndexOf(",") != -1)
                                    {
                                        noidung = noidung.Substring(0, noidung.IndexOf(","));
                                        newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung: " + noidung;
                                    }
                                }
                            }


                        }

                        ////Quyết toán dự án
                        if (newhistory.Function == "/api/qltd_tdth_quyettoanduan/add" || newhistory.Function == "/api/qltd_tdth_quyettoanduan/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Quyết toán dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                            newhistory.FunctionID = "QLDA";

                            var chitietcongviec = newhistory.Content;
                            var noidung = newhistory.Content;
                            if (chitietcongviec.IndexOf("IdChiTietCongViec :") != -1)
                            {
                                int idchitietcongviec = 0;
                                chitietcongviec = chitietcongviec.Substring(chitietcongviec.IndexOf("IdChiTietCongViec :") + 19);

                                if (newhistory.Function == "/api/qltd_tdth_quyettoanduan/add")
                                {
                                    idchitietcongviec = Convert.ToInt32(chitietcongviec);
                                }
                                else
                                {
                                    if (chitietcongviec.IndexOf(",") != -1)
                                    {
                                        chitietcongviec = chitietcongviec.Substring(0, chitietcongviec.IndexOf(","));

                                        if (chitietcongviec.Length > 0 && chitietcongviec != " " && chitietcongviec != "  ")
                                        {
                                            idchitietcongviec = Int32.Parse(chitietcongviec);
                                        }
                                    }
                                }

                                var ctcv_qt = _qltd_ctcv_qtService.GetByID(idchitietcongviec);

                                var idkehoach = ctcv_qt.IdKeHoach;
                                var kehoachs = _qltd_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                                var tenkehoach = kehoachs.NoiDung;


                                if (noidung.IndexOf("NoiDung : ") != -1)
                                {
                                    noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                    if (noidung.IndexOf(",") != -1)
                                    {
                                        noidung = noidung.Substring(0, noidung.IndexOf(","));
                                        newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung: " + noidung;
                                    }
                                }
                            }


                        }

                        //Thực hiện chi tiết
                        ////Chủ trương đầu tư
                        if (newhistory.Function == "/api/qltd_thct_chutruongdautu/add")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Chủ trương đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chutruongdautu";
                            newhistory.FunctionID = "QLDA";

                            var TienDoThucHien = newhistory.Content;
                            var NoiDung = newhistory.Content;
                            var ThayDoi = newhistory.Content;
                            if (TienDoThucHien.IndexOf("IdTienDoThucHien :") != -1)
                            {
                                int idTienDoCongViec = 0;
                                TienDoThucHien = TienDoThucHien.Substring(TienDoThucHien.IndexOf("IdTienDoThucHien :") + 18);
                                if (TienDoThucHien.IndexOf(",") != -1)
                                {
                                    TienDoThucHien = TienDoThucHien.Substring(0, TienDoThucHien.IndexOf(","));

                                    if (TienDoThucHien.Length > 0 && TienDoThucHien != " " && TienDoThucHien != "  ")
                                    {
                                        idTienDoCongViec = Int32.Parse(TienDoThucHien);
                                    }

                                    var tienDoThucHien = _qltd_tienDoThucHienService.GetById(idTienDoCongViec);
                                    var noiDungTienDo = tienDoThucHien.NoiDung;
                                    var idChiTietCongViec = tienDoThucHien.IdChiTietCongViec;
                                    var chiTietCongViec = _qltd_ctcv_ctdtService.GetByID(idChiTietCongViec);
                                    var idKeHoach = chiTietCongViec.IdKeHoach;

                                    var kehoachs = _qltd_kehoachService.GetById(idKeHoach);
                                    var idduan = kehoachs.IdDuAn;
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                    var tenkehoach = kehoachs.NoiDung;

                                    newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung tiến độ: " + noiDungTienDo + ".";

                                    NoiDung = NoiDung.Substring(NoiDung.IndexOf("NoiDung : ") + 10);
                                    if (NoiDung.IndexOf(",") != -1)
                                    {
                                        NoiDung = NoiDung.Substring(0, NoiDung.IndexOf(","));
                                        newhistory.Content = newhistory.Content + " Nội dung tiến độ chi tiết: " + NoiDung + ".";
                                    }

                                    ThayDoi = ThayDoi.Substring(ThayDoi.IndexOf("ThayDoi :") + 9);
                                    if (ThayDoi == "true")
                                    {
                                        newhistory.Content = newhistory.Content + " Có thay đổi";
                                    }
                                    else
                                    {
                                        newhistory.Content = newhistory.Content + " Không thay đổi";
                                    }
                                }
                            }
                        }

                        ////Phê duyệt dự án
                        if (newhistory.Function == "/api/qltd_thct_chuanbidautu/add")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Phê duyệt dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbidautu";
                            newhistory.FunctionID = "QLDA";

                            var TienDoThucHien = newhistory.Content;
                            var NoiDung = newhistory.Content;
                            var ThayDoi = newhistory.Content;
                            if (TienDoThucHien.IndexOf("IdTienDoThucHien :") != -1)
                            {
                                int idTienDoCongViec = 0;
                                TienDoThucHien = TienDoThucHien.Substring(TienDoThucHien.IndexOf("IdTienDoThucHien :") + 18);
                                if (TienDoThucHien.IndexOf(",") != -1)
                                {
                                    TienDoThucHien = TienDoThucHien.Substring(0, TienDoThucHien.IndexOf(","));

                                    if (TienDoThucHien.Length > 0 && TienDoThucHien != " " && TienDoThucHien != "  ")
                                    {
                                        idTienDoCongViec = Int32.Parse(TienDoThucHien);
                                    }

                                    var tienDoThucHien = _qltd_tdth_chuanBiDauTuService.GetById(idTienDoCongViec);
                                    var noiDungTienDo = tienDoThucHien.NoiDung;
                                    var idChiTietCongViec = tienDoThucHien.IdChiTietCongViec;
                                    var chiTietCongViec = _qltd_ctcv_cbdtService.GetByID(idChiTietCongViec);
                                    var idKeHoach = chiTietCongViec.IdKeHoach;

                                    var kehoachs = _qltd_kehoachService.GetById(idKeHoach);
                                    var idduan = kehoachs.IdDuAn;
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                    var tenkehoach = kehoachs.NoiDung;

                                    newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung tiến độ: " + noiDungTienDo + ".";

                                    NoiDung = NoiDung.Substring(NoiDung.IndexOf("NoiDung : ") + 10);
                                    if (NoiDung.IndexOf(",") != -1)
                                    {
                                        NoiDung = NoiDung.Substring(0, NoiDung.IndexOf(","));
                                        newhistory.Content = newhistory.Content + " Nội dung tiến độ chi tiết: " + NoiDung + ".";
                                    }

                                    ThayDoi = ThayDoi.Substring(ThayDoi.IndexOf("ThayDoi :") + 9);
                                    if (ThayDoi == "true")
                                    {
                                        newhistory.Content = newhistory.Content + " Có thay đổi";
                                    }
                                    else
                                    {
                                        newhistory.Content = newhistory.Content + " Không thay đổi";
                                    }
                                }
                            }
                        }

                        ////Chuẩn bị thực hiện
                        if (newhistory.Function == "/api/qltd_thct_chuanbithuchien/add")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Chuẩn bị thực hiện dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/chuanbithuchien";
                            newhistory.FunctionID = "QLDA";

                            var TienDoThucHien = newhistory.Content;
                            var NoiDung = newhistory.Content;
                            var ThayDoi = newhistory.Content;
                            if (TienDoThucHien.IndexOf("IdTienDoThucHien :") != -1)
                            {
                                int idTienDoCongViec = 0;
                                TienDoThucHien = TienDoThucHien.Substring(TienDoThucHien.IndexOf("IdTienDoThucHien :") + 18);
                                if (TienDoThucHien.IndexOf(",") != -1)
                                {
                                    TienDoThucHien = TienDoThucHien.Substring(0, TienDoThucHien.IndexOf(","));

                                    if (TienDoThucHien.Length > 0 && TienDoThucHien != " " && TienDoThucHien != "  ")
                                    {
                                        idTienDoCongViec = Int32.Parse(TienDoThucHien);
                                    }

                                    var tienDoThucHien = _qltd_tdth_chuanBiThucHienService.GetById(idTienDoCongViec);
                                    var noiDungTienDo = tienDoThucHien.NoiDung;
                                    var idChiTietCongViec = tienDoThucHien.IdChiTietCongViec;
                                    var chiTietCongViec = _qltd_ctcv_cbthService.GetByID(idChiTietCongViec);
                                    var idKeHoach = chiTietCongViec.IdKeHoach;

                                    var kehoachs = _qltd_kehoachService.GetById(idKeHoach);
                                    var idduan = kehoachs.IdDuAn;
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                    var tenkehoach = kehoachs.NoiDung;

                                    newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung tiến độ: " + noiDungTienDo + ".";

                                    NoiDung = NoiDung.Substring(NoiDung.IndexOf("NoiDung : ") + 10);
                                    if (NoiDung.IndexOf(",") != -1)
                                    {
                                        NoiDung = NoiDung.Substring(0, NoiDung.IndexOf(","));
                                        newhistory.Content = newhistory.Content + " Nội dung tiến độ chi tiết: " + NoiDung + ".";
                                    }

                                    ThayDoi = ThayDoi.Substring(ThayDoi.IndexOf("ThayDoi :") + 9);
                                    if (ThayDoi == "true")
                                    {
                                        newhistory.Content = newhistory.Content + " Có thay đổi";
                                    }
                                    else
                                    {
                                        newhistory.Content = newhistory.Content + " Không thay đổi";
                                    }
                                }
                            }
                        }

                        //Thực hiện dự án
                        if (newhistory.Function == "/api/qltd_thct_thuchienduan/add")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Thực hiện dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/thuchienduan";
                            newhistory.FunctionID = "QLDA";

                            var TienDoThucHien = newhistory.Content;
                            var NoiDung = newhistory.Content;
                            var ThayDoi = newhistory.Content;
                            if (TienDoThucHien.IndexOf("IdTienDoThucHien :") != -1)
                            {
                                int idTienDoCongViec = 0;
                                TienDoThucHien = TienDoThucHien.Substring(TienDoThucHien.IndexOf("IdTienDoThucHien :") + 18);
                                if (TienDoThucHien.IndexOf(",") != -1)
                                {
                                    TienDoThucHien = TienDoThucHien.Substring(0, TienDoThucHien.IndexOf(","));

                                    if (TienDoThucHien.Length > 0 && TienDoThucHien != " " && TienDoThucHien != "  ")
                                    {
                                        idTienDoCongViec = Int32.Parse(TienDoThucHien);
                                    }

                                    var tienDoThucHien = _qltd_tdth_thucHienDuAnService.GetById(idTienDoCongViec);
                                    var noiDungTienDo = tienDoThucHien.NoiDung;
                                    var idChiTietCongViec = tienDoThucHien.IdChiTietCongViec;
                                    var chiTietCongViec = _qltd_ctcv_thService.GetByID(idChiTietCongViec);
                                    var idKeHoach = chiTietCongViec.IdKeHoach;

                                    var kehoachs = _qltd_kehoachService.GetById(idKeHoach);
                                    var idduan = kehoachs.IdDuAn;
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                    var tenkehoach = kehoachs.NoiDung;

                                    newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung tiến độ: " + noiDungTienDo + ".";

                                    NoiDung = NoiDung.Substring(NoiDung.IndexOf("NoiDung : ") + 10);
                                    if (NoiDung.IndexOf(",") != -1)
                                    {
                                        NoiDung = NoiDung.Substring(0, NoiDung.IndexOf(","));
                                        newhistory.Content = newhistory.Content + " Nội dung tiến độ chi tiết: " + NoiDung + ".";
                                    }

                                    ThayDoi = ThayDoi.Substring(ThayDoi.IndexOf("ThayDoi :") + 9);
                                    if (ThayDoi == "true")
                                    {
                                        newhistory.Content = newhistory.Content + " Có thay đổi";
                                    }
                                    else
                                    {
                                        newhistory.Content = newhistory.Content + " Không thay đổi";
                                    }
                                }
                            }
                        }

                        ////Quyết toán dự án
                        if (newhistory.Function == "/api/qltd_thct_quyettoanduan/add")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            newhistory.Function = "Quản lý tiến độ / Quyết toán dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/quyettoanduan";
                            newhistory.FunctionID = "QLDA";

                            var TienDoThucHien = newhistory.Content;
                            var NoiDung = newhistory.Content;
                            var ThayDoi = newhistory.Content;
                            if (TienDoThucHien.IndexOf("IdTienDoThucHien :") != -1)
                            {
                                int idTienDoCongViec = 0;
                                TienDoThucHien = TienDoThucHien.Substring(TienDoThucHien.IndexOf("IdTienDoThucHien :") + 18);
                                if (TienDoThucHien.IndexOf(",") != -1)
                                {
                                    TienDoThucHien = TienDoThucHien.Substring(0, TienDoThucHien.IndexOf(","));

                                    if (TienDoThucHien.Length > 0 && TienDoThucHien != " " && TienDoThucHien != "  ")
                                    {
                                        idTienDoCongViec = Int32.Parse(TienDoThucHien);
                                    }

                                    var tienDoThucHien = _qltd_tdth_quyetToanDuAnService.GetById(idTienDoCongViec);
                                    var noiDungTienDo = tienDoThucHien.NoiDung;
                                    var idChiTietCongViec = tienDoThucHien.IdChiTietCongViec;
                                    var chiTietCongViec = _qltd_ctcv_qtService.GetByID(idChiTietCongViec);
                                    var idKeHoach = chiTietCongViec.IdKeHoach;

                                    var kehoachs = _qltd_kehoachService.GetById(idKeHoach);
                                    var idduan = kehoachs.IdDuAn;
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                    var tenkehoach = kehoachs.NoiDung;

                                    newhistory.Content = "Kế hoạch: " + tenkehoach + ". Nội dung tiến độ: " + noiDungTienDo + ".";

                                    NoiDung = NoiDung.Substring(NoiDung.IndexOf("NoiDung : ") + 10);
                                    if (NoiDung.IndexOf(",") != -1)
                                    {
                                        NoiDung = NoiDung.Substring(0, NoiDung.IndexOf(","));
                                        newhistory.Content = newhistory.Content + " Nội dung tiến độ chi tiết: " + NoiDung + ".";
                                    }

                                    ThayDoi = ThayDoi.Substring(ThayDoi.IndexOf("ThayDoi :") + 9);
                                    if (ThayDoi == "true")
                                    {
                                        newhistory.Content = newhistory.Content + " Có thay đổi";
                                    }
                                    else
                                    {
                                        newhistory.Content = newhistory.Content + " Không thay đổi";
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Giải phóng mặt bằng
                        #region Kế hoạch tiến độ
                        if (newhistory.Function == "/api/gpmb_kehoach/add" || newhistory.Function == "/api/gpmb_kehoach/update")
                        {
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var kehoachvondc = newhistory.Content;
                            var duan = newhistory.Content;

                            if (func == "/api/gpmb_kehoach/add")
                            {
                                duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            }
                            else
                            {
                                duan = duan.Substring(duan.IndexOf("IdDuAn : ") + 9);
                                duan = duan.Substring(0, duan.IndexOf(" , "));
                            }


                            var idduan = Int32.Parse(duan);
                            var tenduan = _duAnService.GetById(idduan).TenDuAn;
                            newhistory.ParentFunction = tenduan;

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = "Kế hoạch: " + newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }

                        if (newhistory.Function == "/api/gpmb_kehoach/pheduyet" || newhistory.Function == "/api/gpmb_kehoach/khongpheduyet")
                        {
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var kehoachvondc = newhistory.Content;
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.IndexOf(",") != -1)
                            {
                                duan = duan.Substring(0, duan.IndexOf(","));

                                if (duan.Length > 0 && duan != " " && duan != "  ")
                                {
                                    duan = _historyService.RemoveSpace(duan);
                                    var idduan = Int32.Parse(duan);
                                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                    newhistory.ParentFunction = tenduan;
                                }
                            }
                            var tinhtrang = newhistory.Content.Substring(newhistory.Content.IndexOf(" TinhTrang :") + 12);
                            if (tinhtrang.IndexOf(" , ") != -1)
                            {
                                tinhtrang = tinhtrang.Substring(0, tinhtrang.IndexOf(", "));
                            }

                            var trangthai = newhistory.Content.Substring(newhistory.Content.IndexOf(" TrangThai :") + 12);
                            if (trangthai.IndexOf(" , ") != -1)
                            {
                                trangthai = trangthai.Substring(0, trangthai.IndexOf(", "));
                            }

                            string szNoiDung = "";
                            var noidung = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (noidung.IndexOf(" , ") != -1)
                            {
                                szNoiDung = noidung.Substring(0, noidung.IndexOf(" , "));
                            }

                            if (func == "/api/gpmb_kehoach/pheduyet")
                            {
                                if ((tinhtrang == "null" && trangthai == "-1") || (tinhtrang == "1" && trangthai == "0") || (tinhtrang == "2" && trangthai == "0"))
                                {
                                    newhistory.Content = "Gửi kế hoạch " + szNoiDung;
                                }
                                else if (tinhtrang == "1" && trangthai == "1")
                                {
                                    newhistory.Content = "Chấp thuận kế hoạch " + szNoiDung;
                                }
                                else if (tinhtrang == "2" && trangthai == "2")
                                {
                                    newhistory.Content = "Phê duyệt kế hoạch " + szNoiDung;
                                }
                            }
                            else
                            {
                                if (tinhtrang == "1" && trangthai == "0")
                                {
                                    newhistory.Content = "Không chấp thuận kế hoạch " + szNoiDung;
                                }
                                else if (tinhtrang == "2" && trangthai == "0")
                                {
                                    newhistory.Content = "Không phê duyệt kế hoạch " + szNoiDung;
                                }
                            }

                        }

                        if (newhistory.Function == "/api/gpmb_kehoach/delete")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                            newhistory.FunctionID = "QLDA";
                        }

                        if (newhistory.Function == "/api/gpmb_ctcv/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var kehoach = newhistory.Content;
                            var congviec = newhistory.Content;

                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                            newhistory.FunctionID = "QLDA";

                            if (kehoach.IndexOf(" IdKeHoachGPMB :") != -1)
                            {
                                kehoach = kehoach.Substring(kehoach.IndexOf(" IdKeHoachGPMB :") + 16);
                                if (kehoach.IndexOf(",") != -1)
                                {
                                    kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                    if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                    {
                                        var idkehoach = Int32.Parse(kehoach);
                                        var kehoachs = _gpmb_kehoachService.GetById(idkehoach);

                                        var idduan = kehoachs.IdDuAn;
                                        var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                        newhistory.ParentFunction = tenduan;

                                        var tenkehoach = kehoachs.NoiDung;

                                        if (congviec.IndexOf("TenCongViec : ") != -1)
                                        {
                                            congviec = congviec.Substring(congviec.IndexOf("TenCongViec : ") + 14);
                                            if (congviec.IndexOf(",") != -1)
                                            {
                                                congviec = congviec.Substring(0, congviec.IndexOf(","));
                                                newhistory.Content = "Kế hoạch: " + tenkehoach + " . " + "Công việc: " + congviec;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (newhistory.Function == "/api/gpmb_kehoachtiendochung/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var kehoach = newhistory.Content;
                            var noidung = newhistory.Content;
                            string tenkehoach = "";
                            if (kehoach.IndexOf("IdKeHoachGPMB :") != -1)
                            {
                                kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoachGPMB :") + 15);
                                if (kehoach.IndexOf(",") != -1)
                                {
                                    kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                    if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                    {
                                        var idkehoach = Int32.Parse(kehoach);
                                        var kehoachs = _gpmb_kehoachService.GetById(idkehoach);

                                        newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                                        newhistory.ParentFunction = " ";
                                        newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                                        newhistory.FunctionID = "QLDA";

                                        var idduan = kehoachs.IdDuAn;
                                        tenkehoach = kehoachs.NoiDung;
                                        var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                        newhistory.ParentFunction = tenduan;
                                    }
                                }
                            }

                            if (noidung.IndexOf("NoiDung : ") != -1)
                            {
                                noidung = noidung.Substring(noidung.IndexOf("NoiDung : ") + 10);
                                newhistory.Content = "Tiến độ chung Kế hoạch: " + tenkehoach + "." + "Nội dung: " + noidung;
                            }

                        }

                        if (newhistory.Function == "/api/gpmb_khokhanvuongmac/add" || newhistory.Function == "/api/gpmb_khokhanvuongmac/update")
                        {
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kế hoạch tiến độ";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/tiendo";
                            newhistory.FunctionID = "QLDA";

                            var kehoach = newhistory.Content;
                            var nguyenhan = newhistory.Content;
                            var giaiphap = newhistory.Content;
                            var xuly = newhistory.Content;

                            if (kehoach.IndexOf("IdKeHoachGPMB :") != -1)
                            {
                                int idkehoach = 0;
                                kehoach = kehoach.Substring(kehoach.IndexOf("IdKeHoachGPMB :") + 15);

                                if (func == "/api/gpmb_khokhanvuongmac/add")
                                {
                                    idkehoach = Convert.ToInt32(kehoach);
                                }
                                else
                                {
                                    if (kehoach.IndexOf(",") != -1)
                                    {
                                        kehoach = kehoach.Substring(0, kehoach.IndexOf(","));

                                        if (kehoach.Length > 0 && kehoach != " " && kehoach != "  ")
                                        {
                                            idkehoach = Int32.Parse(kehoach);
                                        }
                                    }
                                }

                                var kehoachs = _gpmb_kehoachService.GetById(idkehoach);

                                var idduan = kehoachs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;

                                var tenkehoach = kehoachs.NoiDung;
                                newhistory.Content = "Khó khăn vướng mắc: Kế hoạch: " + tenkehoach;

                                if (nguyenhan.IndexOf("NguyenNhanLyDo : ") != -1)
                                {
                                    nguyenhan = nguyenhan.Substring(nguyenhan.IndexOf("NguyenNhanLyDo : ") + 16);
                                    if (nguyenhan.IndexOf(",") != -1)
                                    {
                                        nguyenhan = nguyenhan.Substring(0, nguyenhan.IndexOf(","));
                                    }
                                    newhistory.Content += ", Nguyên nhân: " + nguyenhan;
                                }

                                if (giaiphap.IndexOf("GiaiPhapTrienKhai : ") != -1)
                                {
                                    giaiphap = giaiphap.Substring(giaiphap.IndexOf("GiaiPhapTrienKhai : ") + 19);
                                    if (giaiphap.IndexOf(",") != -1)
                                    {
                                        giaiphap = giaiphap.Substring(0, giaiphap.IndexOf(","));
                                    }
                                    newhistory.Content += ", Giải pháp: " + giaiphap;
                                }

                                if (func == "/api/gpmb_khokhanvuongmac/add")
                                {
                                    if (xuly.IndexOf("DaGiaiQuyet :") == -1)
                                    {
                                        newhistory.Content += ", Xử lý: Chưa xử lý";
                                    }
                                }

                                if (xuly.IndexOf("DaGiaiQuyet :") != -1)
                                {
                                    xuly = xuly.Substring(xuly.IndexOf("DaGiaiQuyet :") + 13);
                                    if (xuly.IndexOf(",") != -1)
                                    {
                                        xuly = xuly.Substring(0, xuly.IndexOf(","));
                                        if (xuly == "true")
                                        {
                                            xuly = "Đã xử lý";
                                        }
                                        else
                                        {
                                            xuly = "Chưa xử lý";
                                        }
                                    }
                                    newhistory.Content += ", Xử lý: " + xuly;
                                }
                            }
                        }
                        #endregion

                        #region Kết quả thực hiện
                        if (newhistory.Function == "/api/gpmb_tongquanketqua/add" || newhistory.Function == "/api/gpmb_tongquanketqua/update")
                        {
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kết quả thực hiện";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/ketqua";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var kehoachvondc = newhistory.Content;
                            var duan = newhistory.Content;

                            newhistory.Content = "Tổng quan nhiệm vụ GPMB";

                            if (duan.IndexOf(" IdDuAn :") != -1)
                            {
                                //var a = duan.IndexOf(" IdDuAn :") + 9;
                                //duan = duan.Substring(duan.IndexOf(" IdDuAn :") + 9, duan.IndexOf(","));
                                //duan = duan.IndexOf("")
                                duan = duan.Substring(duan.IndexOf(" IdDuAn :"));

                                if (func == "/api/gpmb_tongquanketqua/update")
                                {
                                    duan = duan.Substring(9, duan.IndexOf(", ") - 9);
                                }
                                else
                                {
                                    if (duan.IndexOf(", ") > -1)
                                    {
                                        duan = duan.Substring(0, duan.IndexOf(", "));
                                    }
                                    duan = duan.Substring(9);
                                }
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }

                            //if (duan.IndexOf(", IdDuAn :") != -1)
                            //{
                            //    if (func == "/api/gpmb_tongquanketqua/add")
                            //    {
                            //        duan = duan.Substring(duan.IndexOf(", IdDuAn :") + 10);
                            //    }
                            //    else
                            //    {
                            //        duan = duan.Substring(duan.IndexOf(", IdDuAn : ") + 11);
                            //    }
                            //    if (duan.IndexOf(",") != -1)
                            //    {
                            //        duan = duan.Substring(0, duan.IndexOf(","));

                            //        if (duan.Length > 0 && duan != " " && duan != "  ")
                            //        {
                            //            var idduan = Int32.Parse(duan);
                            //            var tenduan = _duAnService.GetById(idduan).TenDuAn;
                            //            newhistory.ParentFunction = tenduan;
                            //        }
                            //    }
                            //}

                            //if (func == "/api/gpmb_tongquanketqua/add")
                            //{
                            //    duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            //}
                            //else
                            //{
                            //    duan = duan.Substring(duan.IndexOf("IdDuAn : ") + 9);
                            //    duan = duan.Substring(0, duan.IndexOf(" , "));
                            //}




                        }

                        if (newhistory.Function == "/api/gpmb_tongquanketqua/delete")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kết quả thực hiện";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/ketqua";
                            newhistory.FunctionID = "QLDA";
                        }

                        if (newhistory.Function == "/api/gpmb_ketqua/update")
                        {
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Kết quả thực hiện";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/ketqua";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var kehoachvondc = newhistory.Content;
                            var duan = newhistory.Content;

                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            duan = duan.Substring(0, duan.IndexOf(", "));

                            var idduan = Int32.Parse(duan);
                            var tenduan = _duAnService.GetById(idduan).TenDuAn;
                            newhistory.ParentFunction = tenduan;

                            newhistory.Content = "Kết quả thực hiện GPMB";
                        }
                        #endregion

                        #region Đăng kí kế hoạch
                        if (newhistory.Function == "/api/gpmb/file/add" || newhistory.Function == "/api/gpmb/file/update")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Đăng ký kế hoạch";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/dangky";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            //var duan = newhistory.Content;
                            //duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            //var idduan = Int32.Parse(duan);
                            //var tenduan = _duAnService.GetById(idduan).TenDuAn;
                            //newhistory.ParentFunction = tenduan;

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }

                        if (newhistory.Function == "/api/gpmb/file/pheduyet")
                        {
                            var func = newhistory.Function;
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Đăng ký kế hoạch";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/dangky";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var kehoachvondc = newhistory.Content;

                            var tinhtrang = newhistory.Content.Substring(newhistory.Content.IndexOf(" TinhTrang :") + 12);
                            if (tinhtrang.IndexOf(" , ") != -1)
                            {
                                tinhtrang = tinhtrang.Substring(0, tinhtrang.IndexOf(", "));
                            }

                            var trangthai = newhistory.Content.Substring(newhistory.Content.IndexOf(" TrangThai :") + 12);
                            if (trangthai.IndexOf(" , ") != -1)
                            {
                                trangthai = trangthai.Substring(0, trangthai.IndexOf(", "));
                            }

                            string szNoiDung = "";
                            var noidung = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (noidung.IndexOf(" , ") != -1)
                            {
                                szNoiDung = noidung.Substring(0, noidung.IndexOf(" , "));
                            }

                            if (trangthai == "-1")
                            {
                                newhistory.Content = "Gửi " + szNoiDung;
                            }
                            else if (trangthai == "1")
                            {
                                newhistory.Content = "Chấp thuận " + szNoiDung;
                            }
                            else if (trangthai == "2")
                            {
                                newhistory.Content = "Phê duyệt " + szNoiDung;
                            }
                            else if (trangthai == "0")
                            {
                                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                if (roleTemp.Contains("Trưởng phòng"))
                                {
                                    newhistory.Content = "Không chấp thuận " + szNoiDung;
                                }
                                else
                                {
                                    newhistory.Content = "Không phê duyệt " + szNoiDung;
                                }
                            }
                        }

                        if (newhistory.Function == "/api/gpmb/file/delete")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Đăng ký kế hoạch";
                            //newhistory.ParentFunction = " ";
                            newhistory.Content = newhistory.Content;
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/dangky";
                            newhistory.FunctionID = "QLDA";
                        }

                        if (newhistory.Function == "/api/gpmb/file/comment/add")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Đăng ký kế hoạch / Thảo luận";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/dangky";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var file = newhistory.Content;
                            string tenfile = "";
                            if (file.IndexOf(" IdFile :") != -1)
                            {
                                //var a = duan.IndexOf(" IdDuAn :") + 9;
                                //duan = duan.Substring(duan.IndexOf(" IdDuAn :") + 9, duan.IndexOf(","));
                                //duan = duan.IndexOf("")
                                file = file.Substring(file.IndexOf(" IdFile :"));

                                //if (func == "/api/gpmb_tongquanketqua/update")
                                //{
                                //    duan = duan.Substring(9, duan.IndexOf(", ") - 9);
                                //}
                                //else
                                //{
                                if (file.IndexOf(", ") > -1)
                                {
                                    file = file.Substring(0, file.IndexOf(", "));
                                }
                                file = file.Substring(9);
                                //}
                                var idfile = Int32.Parse(file);
                                tenfile = _gpmb_fileService.GetById(idfile).NoiDung;
                                newhistory.ParentFunction = tenfile;
                            }

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("NoiDung : ") + 10);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }

                        if (newhistory.Function == "/api/gpmb/file/comment/delete")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Đăng ký kế hoạch / Thảo luận";
                            //newhistory.ParentFunction = " ";
                            newhistory.Content = newhistory.Content;
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/dangky";
                            newhistory.FunctionID = "QLDA";
                        }
                        #endregion

                        #region Thảo luận
                        if (newhistory.Function == "/api/gpmb_comment/add")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Thảo luận";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/ketqua";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');

                            var duan = newhistory.Content;
                            if (duan.IndexOf(" IdDuAn :") != -1)
                            {
                                //var a = duan.IndexOf(" IdDuAn :") + 9;
                                //duan = duan.Substring(duan.IndexOf(" IdDuAn :") + 9, duan.IndexOf(","));
                                //duan = duan.IndexOf("")
                                duan = duan.Substring(duan.IndexOf(" IdDuAn :"));

                                //if (func == "/api/gpmb_tongquanketqua/update")
                                //{
                                //    duan = duan.Substring(9, duan.IndexOf(", ") - 9);
                                //}
                                //else
                                //{
                                if (duan.IndexOf(", ") > -1)
                                {
                                    duan = duan.Substring(0, duan.IndexOf(", "));
                                }
                                duan = duan.Substring(9);
                                //}
                                var idduan = Int32.Parse(duan);
                                newhistory.ParentFunction = _duAnService.GetById(idduan).TenDuAn;
                            }

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("NoiDung : ") + 10);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }

                        if (newhistory.Function == "/api/gpmb_comment/delete")
                        {
                            newhistory.Function = "Quản lý tiến độ / Giải phóng mặt bằng / Thảo luận";
                            //newhistory.ParentFunction = " ";
                            newhistory.Content = newhistory.Content;
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlytiendo/gpmb/ketqua";
                            newhistory.FunctionID = "QLDA";
                        }
                        #endregion

                        #endregion

                        //Kế hoạch lựa chọn nhà thầu
                        if (newhistory.Function == "/api/kehoachluachonnhathau/add" || newhistory.Function == "/api/kehoachluachonnhathau/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kế hoạch lựa chọn nhà thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/kehoachluachonnhathau";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var kehoachlcnt = newhistory.Content;
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/kehoachluachonnhathau/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kế hoạch lựa chọn nhà thầu";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/kehoachluachonnhathau";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Gói thầu
                        if (newhistory.Function == "/api/goithau/add" || newhistory.Function == "/api/goithau/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kế hoạch lựa chọn nhà thầu / Gói thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/kehoachluachonnhathau";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var kehoachlcnt = newhistory.Content;
                            var duan = newhistory.Content;
                            kehoachlcnt = kehoachlcnt.Substring(kehoachlcnt.IndexOf("IdKeHoachLuaChonNhaThau :") + 25);
                            if (kehoachlcnt.Length > 0 && kehoachlcnt != " " && kehoachlcnt != "  ")
                            {
                                kehoachlcnt = _historyService.RemoveSpace(kehoachlcnt);
                                var idkehoachlcnt = Int32.Parse(kehoachlcnt);
                                var tenkehoachlcnt = _keHoachLuaChonNhaThauService.GetById(idkehoachlcnt).NoiDung;
                                newhistory.Function = "Quản lý đấu thầu / Kế hoạch lựa chọn nhà thầu " + "( " + tenkehoachlcnt + " )" + " / Gói thầu";
                            }
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenGoiThau : ") + 14);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/goithau/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kế hoạch lựa chọn nhà thầu / Gói thầu";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/kehoachluachonnhathau";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Hồ sơ mời thầu
                        if (newhistory.Function == "/api/hosomoithau/add" || newhistory.Function == "/api/hosomoithau/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hồ sơ mời thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/hosomoithau";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hosomoithau/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hồ sơ mời thầu";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/hosomoithau";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Kết quả lựa chọn nhà thầu
                        if (newhistory.Function == "/api/ketquadauthau/add" || newhistory.Function == "/api/ketquadauthau/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kết quả lựa chọn nhà thầu";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/ketquadauthau";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/ketquadauthau/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Kết quả lựa chọn nhà thầu";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/ketquadauthau";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Hợp đồng
                        if (newhistory.Function == "/api/hopdong/add" || newhistory.Function == "/api/hopdong/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/thongtinhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("TenHopDong : ") + 13);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/hopdong/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hợp đồng";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/thongtinhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Phụ lục hợp đồng
                        if (newhistory.Function == "/api/phuluchopdong/add" || newhistory.Function == "/api/phuluchopdong/update")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hợp đồng / Phụ lục hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/thongtinhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var hopdong = newhistory.Content;
                            hopdong = hopdong.Substring(hopdong.IndexOf("IdHopDong :") + 11);
                            if (hopdong.Length > 0 && hopdong != " " && hopdong != "  ")
                            {
                                hopdong = _historyService.RemoveSpace(hopdong);
                                var idhopdong = Int32.Parse(hopdong);
                                newhistory.Function = "Quản lý đấu thầu / Hợp đồng " + "( " + _hopDongService.GetById(idhopdong).TenHopDong + " )" + " / Phụ lục hợp đồng";
                                var idduan = _hopDongService.GetById(idhopdong).IdDuAn;
                                newhistory.ParentFunction = _duAnService.GetById(idduan).TenDuAn;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("TenPhuLuc : ") + 12);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/phuluchopdong/deletemulti")
                        {
                            newhistory.Function = "Quản lý đấu thầu / Hợp đồng / Phụ lục hợp đồng";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydauthau/thongtinhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Chủ trương đầu tư
                        if (newhistory.Function == "/api/chutruongdautu/add" || newhistory.Function == "/api/chutruongdautu/update")
                        {
                            newhistory.Function = "Quản lý đầu tư / Chủ trương đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/chutruongdautu";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" SoVanBan : ") + 12);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/chutruongdautu/deletemulti")
                        {
                            newhistory.Function = "Quản lý đầu tư / Chủ trương đầu tư";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/chutruongdautu";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Lập dự án đầu tư
                        if (newhistory.Function == "/api/lapquanlydautu/add" || newhistory.Function == "/api/lapquanlydautu/update")
                        {
                            newhistory.Function = "Quản lý đầu tư / Lập dự án đầu tư";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            if (newhistory.Content.IndexOf("LoaiQuanLyDauTu :1") != -1)
                            {
                                newhistory.Function = "Quản lý đầu tư / Lập dự án đầu tư";
                                newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                            }
                            if (newhistory.Content.IndexOf("LoaiQuanLyDauTu :2") != -1)
                            {
                                newhistory.Function = "Quản lý đầu tư / Lập TKKT - TDT";
                                newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/laptktctdt";
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/lapquanlydautu/delete/")
                        {
                            //newhistory.Function = "Quản lý đấu tư / Chủ trương đầu tư";
                            //newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Thẩm tra, thẩm định lập dự án đầu tư
                        if (newhistory.Function == "/api/thamdinhlapquanlydautu/add" || newhistory.Function == "/api/thamdinhlapquanlydautu/update")
                        {
                            //newhistory.Function = "Quản lý đấu thầu / Lập dự án đầu tư / Thẩm tra, thẩm định lập dự án đầu tư";
                            newhistory.ParentFunction = " ";
                            //newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var lapqldt = newhistory.Content;
                            lapqldt = lapqldt.Substring(lapqldt.IndexOf("IdLapQuanLyDauTu :") + 18);
                            if (lapqldt.Length > 0 && lapqldt != " " && lapqldt != "  ")
                            {
                                lapqldt = _historyService.RemoveSpace(lapqldt);
                                var idlqldt = Int32.Parse(lapqldt);
                                var lql = _lapQuanLyDauTuService.GetById(idlqldt);
                                if (lql.LoaiQuanLyDauTu == 1)
                                {
                                    newhistory.Function = "Quản lý đầu tư / Lập dự án đầu tư " + "( " + lql.NoiDung + " )" + " / Thẩm tra, thẩm định lập dự án đầu tư";
                                    newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                                }
                                if (lql.LoaiQuanLyDauTu == 2)
                                {
                                    newhistory.Function = "Quản lý đầu tư / Lập TKKT - TDT " + "( " + lql.NoiDung + " )" + " / Thẩm tra, thẩm định lập TKKT - TDT ";
                                    newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                                }
                                var idduan = lql.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/thamdinhlapquanlydautu/deletemulti")
                        {
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlydautu/lapduandautu";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Thực hiện hợp đồng
                        if (newhistory.Function == "/api/thuchienhopdong/add" || newhistory.Function == "/api/thuchienhopdong/update")
                        {
                            newhistory.Function = "Quản lý xây dựng / Thực hiện hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/thuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/thuchienhopdong/deletemulti")
                        {
                            newhistory.Function = "Quản lý xây dựng / Thực hiện hợp đồng";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/thuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Tiến độ thực hiện hợp đồng
                        if (newhistory.Function == "/api/tiendothuchien/add" || newhistory.Function == "/api/tiendothuchien/update")
                        {
                            newhistory.Function = "Quản lý xây dựng / Tiến độ thực hiện hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/tiendothuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenCongViec : ") + 15);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tiendothuchien/deletemulti")
                        {
                            newhistory.Function = "Quản lý xây dựng / Tiến độ thực hiện hợp đồng";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/tiendothuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Tiến độ thực hiện chi tiết
                        if (newhistory.Function == "/api/tiendochitiet/add" || newhistory.Function == "/api/tiendochitiet/update")
                        {
                            newhistory.Function = "Quản lý xây dựng / Tiến độ thực hiện hợp đồng / Tiến độ thực hiện chi tiết";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/tiendothuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var tiendo = newhistory.Content;
                            tiendo = tiendo.Substring(tiendo.IndexOf("IdTienDo :") + 10);
                            tiendo = _historyService.RemoveSpace(tiendo);
                            var idtiendo = Int32.Parse(tiendo);
                            var tiendoth = _tienDoThucHienService.GetById(idtiendo);
                            newhistory.Function = "Quản lý xây dựng / Tiến độ thực hiện hợp đồng " + "( " + tiendoth.TenCongViec + " )" + "/ Tiến độ thực hiện chi tiết";
                            var idduan = tiendoth.IdDuAn;
                            var tenduan = _duAnService.GetById(idduan).TenDuAn;
                            newhistory.ParentFunction = tenduan;

                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tiendochitiet/deletemulti")
                        {
                            newhistory.Function = "Quản lý xây dựng / Tiến độ thực hiện hợp đồng / Tiến độ thực hiện chi tiết";
                            newhistory.FunctionUrl = "/main/quanlyduan/quanlyxaydung/tiendothuchienhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Thanh toán chi phí khác
                        if (newhistory.Function == "/api/thanhtoanchiphikhac/add" || newhistory.Function == "/api/thanhtoanchiphikhac/update")
                        {
                            newhistory.Function = "Tài chính / Thanh toán chi phí khác";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanchiphikhac";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/thanhtoanchiphikhac/deletemulti")
                        {
                            newhistory.Function = "Tài chính / Thanh toán chi phí khác";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanchiphikhac";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Quyết toán dự án
                        if (newhistory.Function == "/api/quyettoanduan/add" || newhistory.Function == "/api/quyettoanduan/update")
                        {
                            newhistory.Function = "Tài chính / Quyết toán dự án";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/quyettoanduan";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/quyettoanduan/deletemulti")
                        {
                            newhistory.Function = "Tài chính / Quyết toán dự án";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/quyettoanduan";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Tạm ứng hợp đồng
                        if (newhistory.Function == "/api/tamunghopdong/add" || newhistory.Function == "/api/tamunghopdong/update")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Tạm ứng hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var hopdong = newhistory.Content;
                            hopdong = hopdong.Substring(hopdong.IndexOf("IdHopDong :") + 11);
                            if (hopdong.Length > 0 && hopdong != " " && hopdong != "  ")
                            {
                                hopdong = _historyService.RemoveSpace(hopdong);
                                var idhopdong = Int32.Parse(hopdong);
                                var hopdongs = _hopDongService.GetById(idhopdong);
                                var idduan = hopdongs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án " + "( " + hopdongs.TenHopDong + " )" + "/ Tạm ứng hợp đồng";
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tamunghopdong/deletemulti")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Tạm ứng hợp đồng";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Thanh toán hợp đồng - Quyết toán hợp đồng
                        if (newhistory.Function == "/api/thanhtoanhopdong/add" || newhistory.Function == "/api/thanhtoanhopdong/update")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Thanh toán hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var hopdong = newhistory.Content;
                            hopdong = hopdong.Substring(hopdong.IndexOf("IdHopDong :") + 11);
                            if (hopdong.Length > 0 && hopdong != " " && hopdong != "  ")
                            {
                                hopdong = _historyService.RemoveSpace(hopdong);
                                var idhopdong = Int32.Parse(hopdong);
                                var hopdongs = _hopDongService.GetById(idhopdong);
                                var idduan = hopdongs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án " + "( " + hopdongs.TenHopDong + " )" + "/ Thanh toán hợp đồng";
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/thanhtoanhopdong/deletemulti")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Thanh toán hợp đồng";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Quyết toán hợp đồng
                        if (newhistory.Function == "/api/quyettoanhopdong/add" || newhistory.Function == "/api/quyettoanhopdong/update")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Quyết toán hợp đồng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var hopdong = newhistory.Content;
                            hopdong = hopdong.Substring(hopdong.IndexOf("IdHopDong :") + 11);
                            if (hopdong.Length > 0 && hopdong != " " && hopdong != "  ")
                            {
                                hopdong = _historyService.RemoveSpace(hopdong);
                                var idhopdong = Int32.Parse(hopdong);
                                var hopdongs = _hopDongService.GetById(idhopdong);
                                var idduan = hopdongs.IdDuAn;
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án " + "( " + hopdongs.TenHopDong + " )" + "/ Quyết toán hợp đồng";
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" NoiDung : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/quyettoanhopdong/deletemulti")
                        {
                            newhistory.Function = "Tài chính / Thanh toán hợp đồng dự án / Quyết toán hợp đồng";
                            newhistory.FunctionUrl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Văn bản dự án / thư mục
                        if (newhistory.Function == "/api/tm/add" || newhistory.Function == "/api/tm/update" || newhistory.Function == "/api/tm/edit")
                        {
                            newhistory.Function = "Văn bản dự án / Thư mục";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/vanbanduan";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenThuMuc : ") + 13);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tm/delete")
                        {
                            newhistory.Function = "Văn bản dự án / Thư mục";
                            newhistory.FunctionUrl = "/main/quanlyduan/vanbanduan";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Văn bản dự án / danh sách văn bản
                        if (newhistory.Function == "/api/vbda/add" || newhistory.Function == "/api/vbda/update")
                        {
                            newhistory.Function = "Văn bản dự án / Thư mục / Danh sách văn bản";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/quanlyduan/vanbanduan";
                            newhistory.FunctionID = "QLDA";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            var duan = newhistory.Content;
                            duan = duan.Substring(duan.IndexOf("IdDuAn :") + 8);
                            if (duan.Length > 0 && duan != " " && duan != "  ")
                            {
                                duan = _historyService.RemoveSpace(duan);
                                var idduan = Int32.Parse(duan);
                                var tenduan = _duAnService.GetById(idduan).TenDuAn;
                                newhistory.ParentFunction = tenduan;
                            }
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf(" TenVanBan : ") + 13);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/vbda/delete")
                        {
                            newhistory.Function = "Văn bản dự án / Danh sách văn bản";
                            newhistory.FunctionUrl = "/main/quanlyduan/vanbanduan";
                            newhistory.FunctionID = "QLDA";
                        }

                        //HỆ THỐNG
                        //lấy ra tất cả chức năng cha
                        var parentfunction = _functionService.GetAllForHistory().Where(x => x.ParentId == null);
                        //Chức năng
                        if (newhistory.Function == "/api/function/add" || newhistory.Function == "/api/function/update")
                        {
                            newhistory.Function = "Hệ thống / Chức năng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/hethong/function";
                            newhistory.FunctionID = "HETHONG";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            //var parentnewhistory = newhistory.Content;
                            //parentnewhistory = parentnewhistory.Substring(parentnewhistory.IndexOf("ParetnId :"));
                            //if(parentnewhistory.Length >0 && parentnewhistory != " " && parentnewhistory != "  ")
                            //{
                            //    parentnewhistory = _historyService.RemoveSpace(parentnewhistory);

                            //}
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("Name : ") + 7);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/function/delete")
                        {
                            newhistory.Function = "Hệ thống / Chức năng";
                            newhistory.FunctionUrl = "/main/hethong/function";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Nhóm người dùng
                        if (newhistory.Function == "/api/appRole/add" || newhistory.Function == "/api/appRole/update")
                        {
                            newhistory.Function = "Hệ thống / Nhóm người dùng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/hethong/role";
                            newhistory.FunctionID = "HETHONG";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("Name : ") + 7);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/appRole/delete")
                        {
                            newhistory.Function = "Hệ thống / Nhóm người dùng";
                            newhistory.FunctionUrl = "/main/hethong/role";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Người dùng
                        if (newhistory.Function == "/api/appUser/add" || newhistory.Function == "/api/appUser/update")
                        {
                            newhistory.Function = "Hệ thống / Người dùng";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/hethong/user";
                            newhistory.FunctionID = "HETHONG";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("UserName : ") + 11);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/appUser/delete")
                        {
                            newhistory.Function = "Hệ thống / Người dùng";
                            newhistory.FunctionUrl = "/main/hethong/user";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Danh sách phòng ban
                        if (newhistory.Function == "/api/NhomDuAnTheoUser/add" || newhistory.Function == "/api/NhomDuAnTheoUser/update")
                        {
                            newhistory.Function = "Hệ thống / Danh sách phòng ban";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/hethong/phongban";
                            newhistory.FunctionID = "HETHONG";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("TenNhomDuAn : ") + 14);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/NhomDuAnTheoUser/deletemulti")
                        {
                            newhistory.Function = "Hệ thống / Danh sách phòng ban";
                            newhistory.FunctionUrl = "/main/hethong/phongban";
                            newhistory.FunctionID = "QLDA";
                        }
                        //Thư mục
                        if (newhistory.Function == "/api/tmmd/add" || newhistory.Function == "/api/tmmd/update")
                        {
                            newhistory.Function = "Hệ thống / Thư mục";
                            newhistory.ParentFunction = " ";
                            newhistory.FunctionUrl = "/main/hethong/thumucmacdinh";
                            newhistory.FunctionID = "HETHONG";
                            newhistory.Content = newhistory.Content.Substring(1, newhistory.Content.Length - 2);
                            newhistory.Content = newhistory.Content.Replace('"', ' ');
                            newhistory.Content = newhistory.Content.Substring(newhistory.Content.IndexOf("TenThuMuc : ") + 12);
                            if (newhistory.Content.IndexOf(" , ") != -1)
                            {
                                newhistory.Content = newhistory.Content.Substring(0, newhistory.Content.IndexOf(" , "));
                            }
                        }
                        if (newhistory.Function == "/api/tmmd/delete")
                        {
                            newhistory.Function = "Hệ thống / Thư mục";
                            newhistory.FunctionUrl = "/main/hethong/thumucmacdinh";
                            newhistory.FunctionID = "QLDA";
                        }


                        if (newhistory.Content == null)
                        {
                            newhistory.Content = " ";
                        }
                        if (newhistory.Function == null)
                        {
                            newhistory.Function = " ";
                        }
                        if (newhistory.ParentFunction == null)
                        {
                            newhistory.ParentFunction = " ";
                        }

                        _historyService.Add(newhistory);
                        _historyService.Save();

                        var responseData = Mapper.Map<ActionHistory, ActionHistoryViewModel>(newhistory);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                    catch (Exception)
                    {
                        newhistory.Content = " ";
                        newhistory.ParentFunction = " ";
                        _historyService.Add(newhistory);
                        _historyService.Save();
                        var responseData = Mapper.Map<ActionHistory, ActionHistoryViewModel>(newhistory);
                        response = request.CreateResponse(HttpStatusCode.Created, responseData);
                    }
                }
                return response;
            });
        }

        [Route("setupdelete")]
        [HttpPost]
        public HttpResponseMessage SetupDelete(HttpRequestMessage request, int kieuxoa, int chuky, int thang, int thu, int ngay, int gio, int phut, DateTime? ngayxoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var folderReport = "/History";

                string filePath = HttpContext.Current.Server.MapPath(folderReport);
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string str = "setup.log";

                var path2 = Path.Combine(filePath, str);

                string str2 = "auto.log";
                var path3 = Path.Combine(filePath, str2);
                int checkauto = 0;
                int checkcontinue = 0;
                if (File.Exists(path3))
                {
                    string jsonString1 = File.ReadAllText(path3);

                    var sponsors1 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString1);
                    if (kieuxoa == 1)
                    {
                        AutoDeleteHistory auto = new AutoDeleteHistory();
                        auto.Id = 0;
                        auto.MoTa = "0 cancel >0 auto";
                        File.WriteAllText(path3, JsonConvert.SerializeObject(auto));
                        checkcontinue = 0;
                    }
                    else
                    {
                        if (sponsors1.Id == 0)
                        {
                            checkauto = 1;
                            AutoDeleteHistory auto = new AutoDeleteHistory();
                            auto.Id = 1;
                            auto.MoTa = "0 cancel >0 auto";
                            File.WriteAllText(path3, JsonConvert.SerializeObject(auto));
                            checkcontinue = 1;
                        }
                        else
                        {
                            checkauto = sponsors1.Id + 1;
                            AutoDeleteHistory auto = new AutoDeleteHistory();
                            auto.Id = checkauto;
                            auto.MoTa = "0 cancel >0 auto";
                            File.WriteAllText(path3, JsonConvert.SerializeObject(auto));
                            checkcontinue = checkauto;
                        }
                    }
                }
                else
                {
                    AutoDeleteHistory auto = new AutoDeleteHistory();
                    auto.Id = 0;
                    auto.MoTa = "0 cancel >0 auto";
                    using (StreamWriter file = File.CreateText(path3))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, auto);
                    }
                    File.WriteAllText(path3, JsonConvert.SerializeObject(auto));
                }

                CaiDatXoaLichSu cd = new CaiDatXoaLichSu();
                cd.KieuXoa = kieuxoa;
                cd.ChuKy = chuky;
                cd.Thang = thang;
                cd.Thu = thu;
                cd.Ngay = ngay;
                cd.Gio = gio;
                cd.Phut = phut;
                cd.NgayXoa = ngayxoa;
                // serialize JSON directly to a file
                if (File.Exists(path2))
                {
                    File.WriteAllText(path2, JsonConvert.SerializeObject(cd));
                }
                else
                {
                    cd.KieuXoa = 1;
                    cd.ChuKy = 0;
                    cd.Thang = 1;
                    cd.Thu = 2;
                    cd.Ngay = 1;
                    cd.Gio = 0;
                    cd.Phut = 0;
                    using (StreamWriter file = File.CreateText(path2))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, cd);
                    }
                    File.WriteAllText(path2, JsonConvert.SerializeObject(cd));
                }

                if (checkcontinue > 0)
                {
                    _historyService.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, 0, filePath, checkcontinue, checkauto);
                }

                string jsonString = File.ReadAllText(path2);

                var sponsors = JsonConvert.DeserializeObject<CaiDatXoaLichSu>(jsonString);

                response = request.CreateResponse(HttpStatusCode.OK, sponsors);

                return response;
            });
        }

        [Authorize]
        [Route("getcaidat")]
        [HttpGet]
        public HttpResponseMessage GetCaiDat(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var folderReport = "/History";

                string filePath = HttpContext.Current.Server.MapPath(folderReport);
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                string str = "setup.log";

                var path2 = Path.Combine(filePath, str);
                if (!File.Exists(path2))
                {
                    var sponsors = "empty";
                    response = request.CreateResponse(HttpStatusCode.OK, sponsors);

                    return response;
                }
                else
                {
                    string jsonString = File.ReadAllText(path2);

                    var sponsors = JsonConvert.DeserializeObject<CaiDatXoaLichSu>(jsonString);

                    response = request.CreateResponse(HttpStatusCode.OK, sponsors);

                    return response;
                }
            });
        }

        [Authorize]
        [Route("getfile")]
        [HttpGet]
        public HttpResponseMessage GetFile(HttpRequestMessage request, string str)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var folderReport = "/History";
                string filePath = HttpContext.Current.Server.MapPath(folderReport);
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                var path2 = Path.Combine(filePath, str);
                string jsonString = File.ReadAllText(path2);
                var sponsors = JsonConvert.DeserializeObject<IList<ActionHistory>>(jsonString);
                response = request.CreateResponse(HttpStatusCode.OK, sponsors);
                return response;
            });
        }

        [Authorize]
        [Route("gettenfile")]
        [HttpGet]
        public HttpResponseMessage GetTenFile(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var folderReport = "/History";
                string filePath = HttpContext.Current.Server.MapPath(folderReport);
                DirectoryInfo d = new DirectoryInfo(filePath);
                FileInfo[] Files = d.GetFiles("*.log");
                List<string> str = new List<string>();
                foreach (FileInfo file in Files)
                {
                    str.Add(file.Name);
                }
                response = request.CreateResponse(HttpStatusCode.OK, str);
                return response;
            });
        }

        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        [Route("stopdelete")]
        [HttpPost]
        public void StopDelete(HttpRequestMessage request)
        {
            //EventArgs e = new EventArgs();
            cancellationTokenSource.Cancel();
            cancellationTokenSource = null;
            //_historyService.Cancel1();
        }

        [Authorize]
        [Route("delete")]
        [HttpPost]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                _historyService.DeleteByID(id);
                _historyService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, id);
                return response;
            });
        }

        [Authorize]
        [Route("deletefile")]
        [HttpPost]
        public HttpResponseMessage DeleteFile(HttpRequestMessage request, string item)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var folderReport = "/History";
                string filePath = HttpContext.Current.Server.MapPath(folderReport);
                var path3 = Path.Combine(filePath, item);
                if (File.Exists(path3))
                {
                    File.Delete(path3);
                }
                response = request.CreateResponse(HttpStatusCode.OK, item);
                return response;
            });
        }

        [Route("startautodeletehistory")]
        [HttpGet]
        public string StartAutoDeleteHistory()
        {
            var folderReport = "/History";
            string filePath = System.Web.HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string str = "setup.log";
            var path2 = Path.Combine(filePath, str);
            string str2 = "auto.log";
            var path3 = Path.Combine(filePath, str2);
            if (System.IO.File.Exists(path2) && System.IO.File.Exists(path3))
            {
                string jsonString = System.IO.File.ReadAllText(path2);
                var sponsors = JsonConvert.DeserializeObject<CaiDatXoaLichSu>(jsonString);
                using (var wb = new WebClient())
                {
                    string baseApi = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                    if (baseApi.IndexOf("/api/") != -1)
                    {
                        baseApi = baseApi.Substring(0, baseApi.IndexOf("/api/"));
                    }
                    var data = new NameValueCollection();
                    var url = baseApi + "/api/actionhistory/setupdelete?kieuxoa=" + sponsors.KieuXoa + "&chuky=" + sponsors.ChuKy + "&thang=" + sponsors.Thang + "&thu=" + sponsors.Thu + "&ngay=" + sponsors.Ngay + "&gio=" + sponsors.Gio + "&phut=" + sponsors.Phut + "&ngayxoa=" + null;
                    var response = wb.UploadValues(url, "POST", data);
                    string responseInString = Encoding.UTF8.GetString(response);
                }
            }
            return "Delete History Automatic";
        }
    }
}