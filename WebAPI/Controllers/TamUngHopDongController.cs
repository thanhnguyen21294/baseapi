﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tamunghopdong")]
    [Authorize]
    public class TamUngHopDongController : ApiControllerBase
    {
        private ITamUngHopDongService _TamUngHopDongService;
        private INguonVonGoiThauTamUngService _NguonVonGoiThauTamUngService;
        public INguonVonDuAnGoiThauService _NguonVonDuAnGoiThauService;
        public INguonVonDuAnService _NguonVonDuAnService;
        public INguonVonService _NguonVonService;
        public TamUngHopDongController(IErrorService errorService, ITamUngHopDongService tamUngHopDongService, INguonVonGoiThauTamUngService nguonVonGoiThauTamUngService,
             INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService, INguonVonDuAnService nguonVonDuAnService, INguonVonService nguonVonService) : base(errorService)
        {
            this._TamUngHopDongService = tamUngHopDongService;
            _NguonVonGoiThauTamUngService = nguonVonGoiThauTamUngService;
            _NguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            _NguonVonDuAnService = nguonVonDuAnService;
            _NguonVonService = nguonVonService;
          
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var nguonVonGoiThaus = _NguonVonGoiThauTamUngService.GetAll();
                var model = _TamUngHopDongService.GetAll().OrderByDescending(x => x.IdTamUng);
                 
             
                 IEnumerable< TamUngHopDongViewModels> modelVm= model.GroupJoin(nguonVonGoiThaus, x => x.IdTamUng, y => y.IdTamUng, (x, grp) => new TamUngHopDongViewModels
                {
                    IdTamUng = x.IdTamUng,
                    IdHopDong = x.IdHopDong,
                    GhiChu = x.GhiChu,
                    NgayTamUng = x.NgayTamUng.ToString(),
                    NoiDung = x.NoiDung,
                    TongGiaTriTamUng = grp.Select(y => y.GiaTri).Sum(),
                    NguonVonGoiThauTamUngs = x.NguonVonGoiThauTamUngs,
                    TenNguonVons = grp.Join(_NguonVonDuAnGoiThauService.GetAll(), a => a.IdNguonVonDuAnGoiThau, b => b.IdNguonVonDuAnGoiThau, (a, b) => new {
                         IdNguonVonDuAn = b.IdNguonVonDuAn
                     }).Join(_NguonVonDuAnService.GetAll(), a => a.IdNguonVonDuAn, b => b.IdNguonVonDuAn, (a, b) => new {
                         IdNguonVon = b.IdNguonVon
                     }).Join(_NguonVonService.GetAll(), a => a.IdNguonVon, b => b.IdNguonVon, (a, b) => new {
                         TenNguonVon = b.TenNguonVon
                     }).Select(c => c.TenNguonVon),

                 });
                

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
       

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _TamUngHopDongService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getbyidhopdong")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idHopDong)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _TamUngHopDongService.GetByIdHopDong(idHopDong);
                List<TamUngHopDongViewModels> modelVm = new List<TamUngHopDongViewModels>();
                foreach (var item in model)
                {
                    TamUngHopDongViewModels itemVm = new TamUngHopDongViewModels();
                    itemVm.IdHopDong = item.IdHopDong;
                    itemVm.IdTamUng = item.IdTamUng;
                    itemVm.NgayTamUng = (item.NgayTamUng!=null)?item.NgayTamUng.ToString():null;
                    itemVm.NoiDung = item.NoiDung;
                    itemVm.GhiChu = item.GhiChu;
                    itemVm.NguonVonGoiThauTamUngs = item.NguonVonGoiThauTamUngs;
                    itemVm.TongGiaTriTamUng=(item.NguonVonGoiThauTamUngs!=null)?(item.NguonVonGoiThauTamUngs.Select(x=>x.GiaTri).Sum()):0;
                    modelVm.Add(itemVm);

                }

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _TamUngHopDongService.GetById(id);
                TamUngHopDongViewModels modelVm = new TamUngHopDongViewModels();
                modelVm.IdTamUng = model.IdTamUng;
                modelVm.IdHopDong = model.IdHopDong;
                modelVm.NgayTamUng =(model.NgayTamUng!=null)?model.NgayTamUng.ToString():null;
                modelVm.NguonVonGoiThauTamUngs = model.NguonVonGoiThauTamUngs;
                modelVm.GhiChu = model.GhiChu;
                modelVm.NoiDung = model.NoiDung;
                modelVm.TongGiaTriTamUng =(model.NguonVonGoiThauTamUngs.Count()>0)? model.NguonVonGoiThauTamUngs.Select(x => x.GiaTri).Sum():0;
               

                var response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallnguonvongoithautamung")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetAllNguonVonGoiThauTamUng(HttpRequestMessage request, string year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _TamUngHopDongService.GetAllNguonVonGoiThauTamUngByYear(year);
                //var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                //PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                //{
                //    Items = modelVm
                //};
                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, TamUngHopDongViewModels vmTamUngHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newTamUngHopDong = new TamUngHopDong();
                    newTamUngHopDong.IdHopDong = vmTamUngHopDongViewModels.IdHopDong;
                    newTamUngHopDong.NoiDung = vmTamUngHopDongViewModels.NoiDung;
                    if (vmTamUngHopDongViewModels.NgayTamUng != "" && vmTamUngHopDongViewModels.NgayTamUng != null)
                        newTamUngHopDong.NgayTamUng = DateTime.ParseExact(vmTamUngHopDongViewModels.NgayTamUng, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newTamUngHopDong.GhiChu = vmTamUngHopDongViewModels.GhiChu;
                    newTamUngHopDong.CreatedDate = DateTime.Now;
                    newTamUngHopDong.CreatedBy = User.Identity.GetUserId();
                    newTamUngHopDong.Status = true;

                    foreach (var item in vmTamUngHopDongViewModels.NguonVonGoiThauTamUngs)
                    {
                        _NguonVonGoiThauTamUngService.Add(new NguonVonGoiThauTamUng
                        {
                            IdNguonVonDuAnGoiThau = item.IdNguonVonDuAnGoiThau,
                            GiaTri = item.GiaTri,
                            Status = true
                        });
                    }

                    _TamUngHopDongService.Add(newTamUngHopDong);
                    _TamUngHopDongService.Save();

                   
                    
                    vmTamUngHopDongViewModels.IdTamUng = newTamUngHopDong.IdTamUng;
                    vmTamUngHopDongViewModels.NguonVonGoiThauTamUngs = newTamUngHopDong.NguonVonGoiThauTamUngs;
                    vmTamUngHopDongViewModels.TongGiaTriTamUng = (newTamUngHopDong.NguonVonGoiThauTamUngs!=null) ? newTamUngHopDong.NguonVonGoiThauTamUngs.Select(x => x.GiaTri).Sum():0;
                    response = request.CreateResponse(HttpStatusCode.Created, vmTamUngHopDongViewModels);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, TamUngHopDongViewModels vmTamUngHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateTamUngHopDong = _TamUngHopDongService.GetById(vmTamUngHopDongViewModels.IdTamUng);
                    updateTamUngHopDong.IdHopDong = vmTamUngHopDongViewModels.IdHopDong;
                    updateTamUngHopDong.NoiDung = vmTamUngHopDongViewModels.NoiDung;
                    if (vmTamUngHopDongViewModels.NgayTamUng != "" && vmTamUngHopDongViewModels.NgayTamUng != null)
                    {
                        updateTamUngHopDong.NgayTamUng = DateTime.ParseExact(vmTamUngHopDongViewModels.NgayTamUng, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateTamUngHopDong.NgayTamUng = null;
                    }
                        
                    updateTamUngHopDong.GhiChu = vmTamUngHopDongViewModels.GhiChu;
                    updateTamUngHopDong.GhiChu = vmTamUngHopDongViewModels.GhiChu;
                    updateTamUngHopDong.UpdatedDate = DateTime.Now;
                    updateTamUngHopDong.UpdatedBy = User.Identity.GetUserId();
                    try
                    {
                        _NguonVonGoiThauTamUngService.DeleteByIdTamUngHopDong(vmTamUngHopDongViewModels.IdTamUng);
                        foreach (var item in vmTamUngHopDongViewModels.NguonVonGoiThauTamUngs)
                        {
                            _NguonVonGoiThauTamUngService.Add(new NguonVonGoiThauTamUng
                            {
                                IdNguonVonDuAnGoiThau = item.IdNguonVonDuAnGoiThau,
                                IdTamUng = vmTamUngHopDongViewModels.IdTamUng,
                                GiaTri = item.GiaTri,
                                Status = true
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _TamUngHopDongService.Update(updateTamUngHopDong);
                    _TamUngHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonGoiThauTamUngService.DeleteByIdTamUngHopDong(id);
                    _TamUngHopDongService.Delete(id);
                    _TamUngHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDGoiThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDGoiThau)
                    {
                        _NguonVonGoiThauTamUngService.DeleteByIdTamUngHopDong(item);
                        _TamUngHopDongService.Delete(item);
                    }

                    _TamUngHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listIDGoiThau.Count);
                }

                return response;
            });
        }
    }
}
