﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/chudautu")]
    //[Authorize]
    public class ChuDauTuController : ApiControllerBase
    {
        private IChuDauTuService _chuDauTuService;
        public ChuDauTuController(IErrorService errorService, IChuDauTuService chuDauTuService) : base(errorService)
        {
            this._chuDauTuService = chuDauTuService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _chuDauTuService.GetAll().OrderByDescending(x => x.IdChuDauTu);

                var modelVm = Mapper.Map<IEnumerable<ChuDauTu>, IEnumerable<ChuDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallname")]
        [HttpGet]
        public HttpResponseMessage GetAllName(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _chuDauTuService.GetAll().OrderByDescending(x => x.IdChuDauTu).Select(x => x.TenChuDauTu);
                                
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CHUDAUTU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _chuDauTuService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<ChuDauTu>, IEnumerable<ChuDauTuViewModels>>(model);

                PaginationSet<ChuDauTuViewModels> pagedSet = new PaginationSet<ChuDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CHUDAUTU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _chuDauTuService.GetById(id);

                var responseData = Mapper.Map<ChuDauTu, ChuDauTuViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "CHUDAUTU")]
        public HttpResponseMessage Create(HttpRequestMessage request, ChuDauTuViewModels chuDauTuViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newChuDauTu = new ChuDauTu();
                    newChuDauTu.UpdateChuDauTu(chuDauTuViewModels);
                    newChuDauTu.CreatedDate = DateTime.Now;
                    newChuDauTu.CreatedBy = User.Identity.GetUserId();
                    _chuDauTuService.Add(newChuDauTu);
                    _chuDauTuService.Save();

                    var responseData = Mapper.Map<ChuDauTu, ChuDauTuViewModels>(newChuDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "CHUDAUTU")]
        public HttpResponseMessage Update(HttpRequestMessage request, ChuDauTuViewModels chuDauTuViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbChuDauTu = _chuDauTuService.GetById(chuDauTuViewModels.IdChuDauTu);

                    dbChuDauTu.UpdateChuDauTu(chuDauTuViewModels);
                    dbChuDauTu.UpdatedDate = DateTime.Now;
                    dbChuDauTu.UpdatedBy = User.Identity.GetUserId();
                    _chuDauTuService.Update(dbChuDauTu);
                    _chuDauTuService.Save();

                    var responseData = Mapper.Map<ChuDauTu, ChuDauTuViewModels>(dbChuDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CHUDAUTU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuDauTuService.Delete(id);
                    _chuDauTuService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CHUDAUTU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listChuDauTu = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listChuDauTu)
                    {
                        _chuDauTuService.Delete(item);
                    }

                    _chuDauTuService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listChuDauTu.Count);
                }

                return response;
            });
        }
    }
}
