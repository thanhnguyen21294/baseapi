﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/linhvucnganhnghe")]
    [Authorize]
    public class LinhVucNganhNgheController : ApiControllerBase
    {
        private ILinhVucNganhNgheService _linhVucNganhNgheService;
        public LinhVucNganhNgheController(IErrorService errorService, ILinhVucNganhNgheService linhVucNganhNgheService) : base(errorService)
        {
            this._linhVucNganhNgheService = linhVucNganhNgheService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _linhVucNganhNgheService.GetAll().OrderByDescending(x => x.IdLinhVucNganhNghe);

                var modelVm = Mapper.Map<IEnumerable<LinhVucNganhNghe>, IEnumerable<LinhVucNganhNgheViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _linhVucNganhNgheService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LinhVucNganhNghe>, IEnumerable<LinhVucNganhNgheViewModels>>(model);

                PaginationSet<LinhVucNganhNgheViewModels> pagedSet = new PaginationSet<LinhVucNganhNgheViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _linhVucNganhNgheService.GetById(id);

                var responseData = Mapper.Map<LinhVucNganhNghe, LinhVucNganhNgheViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage Create(HttpRequestMessage request, LinhVucNganhNgheViewModels linhVucNganhNgheViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLinhVucNganhNghe = new LinhVucNganhNghe();
                    newLinhVucNganhNghe.UpdateLinhVucNganhNghe(linhVucNganhNgheViewModels);
                    newLinhVucNganhNghe.CreatedDate = DateTime.Now;
                    newLinhVucNganhNghe.CreatedBy = User.Identity.GetUserId();
                    _linhVucNganhNgheService.Add(newLinhVucNganhNghe);
                    _linhVucNganhNgheService.Save();

                    var responseData = Mapper.Map<LinhVucNganhNghe, LinhVucNganhNgheViewModels>(newLinhVucNganhNghe);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage Update(HttpRequestMessage request, LinhVucNganhNgheViewModels linhVucNganhNgheViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLinhVucNganhNghe = _linhVucNganhNgheService.GetById(linhVucNganhNgheViewModels.IdLinhVucNganhNghe);

                    dbLinhVucNganhNghe.UpdateLinhVucNganhNghe(linhVucNganhNgheViewModels);
                    dbLinhVucNganhNghe.UpdatedDate = DateTime.Now;
                    dbLinhVucNganhNghe.UpdatedBy = User.Identity.GetUserId();
                    _linhVucNganhNgheService.Update(dbLinhVucNganhNghe);
                    _linhVucNganhNgheService.Save();

                    var responseData = Mapper.Map<LinhVucNganhNghe, LinhVucNganhNgheViewModels>(dbLinhVucNganhNghe);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _linhVucNganhNgheService.Delete(id);
                    _linhVucNganhNgheService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LINHVUCNGANHNGHE")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLinhVucNganhNghe = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLinhVucNganhNghe)
                    {
                        _linhVucNganhNgheService.Delete(item);
                    }

                    _linhVucNganhNgheService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLinhVucNganhNghe.Count);
                }

                return response;
            });
        }

        [Route("showDABeforeDelete")]
        [HttpGet]
        public HttpResponseMessage showDABeforeDelete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var model = _linhVucNganhNgheService.GetDuAnById(id);

                    var modelVm = Mapper.Map<IEnumerable<LinhVucNganhNghe>, IEnumerable<LinhVucNganhNgheViewModels>>(model);

                    response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                }
                return response;

            });
        }
    }
}
