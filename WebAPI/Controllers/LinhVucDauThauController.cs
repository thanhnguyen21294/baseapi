﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/linhvucdauthau")]
    [Authorize]
    public class LinhVucDauThauController : ApiControllerBase
    {
        private ILinhVucDauThauService _linhVucDauThauService;
        public LinhVucDauThauController(IErrorService errorService, ILinhVucDauThauService linhVucDauThauService) : base(errorService)
        {
            this._linhVucDauThauService = linhVucDauThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _linhVucDauThauService.GetAll().OrderByDescending(x => x.IdLinhVucDauThau);

                var modelVm = Mapper.Map<IEnumerable<LinhVucDauThau>, IEnumerable<LinhVucDauThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _linhVucDauThauService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LinhVucDauThau>, IEnumerable<LinhVucDauThauViewModels>>(model);

                PaginationSet<LinhVucDauThauViewModels> pagedSet = new PaginationSet<LinhVucDauThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _linhVucDauThauService.GetById(id);

                var responseData = Mapper.Map<LinhVucDauThau, LinhVucDauThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, LinhVucDauThauViewModels linhVucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLinhVucDauThau = new LinhVucDauThau();
                    newLinhVucDauThau.UpdateLinhVucDauThau(linhVucDauThauViewModels);
                    newLinhVucDauThau.CreatedDate = DateTime.Now;
                    newLinhVucDauThau.CreatedBy = User.Identity.GetUserId();
                    _linhVucDauThauService.Add(newLinhVucDauThau);
                    _linhVucDauThauService.Save();

                    var responseData = Mapper.Map<LinhVucDauThau, LinhVucDauThauViewModels>(newLinhVucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, LinhVucDauThauViewModels linhVucDauThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLinhVucDauThau = _linhVucDauThauService.GetById(linhVucDauThauViewModels.IdLinhVucDauThau);

                    dbLinhVucDauThau.UpdateLinhVucDauThau(linhVucDauThauViewModels);
                    dbLinhVucDauThau.UpdatedDate = DateTime.Now;
                    dbLinhVucDauThau.UpdatedBy = User.Identity.GetUserId();
                    _linhVucDauThauService.Update(dbLinhVucDauThau);
                    _linhVucDauThauService.Save();

                    var responseData = Mapper.Map<LinhVucDauThau, LinhVucDauThauViewModels>(dbLinhVucDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _linhVucDauThauService.Delete(id);
                    _linhVucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LINHVUCDAUTHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLinhVucDauThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLinhVucDauThau)
                    {
                        _linhVucDauThauService.Delete(item);
                    }

                    _linhVucDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLinhVucDauThau.Count);
                }

                return response;
            });
        }
    }
}
