﻿using AutoMapper;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.DuAn;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/kehoachvoncacduan")]
    public class KeHoachVonCacDuAnController : ApiControllerBase
    {
        public IDuAnService _duanService;
        public INguonVonService _nguonvonService;
        public IGiaiDoanDuAnService _giaidoanService;
        public ITinhTrangDuAnService _tinhtrangDAService;
        public INhomDuAnService _nhomDAService;
        public IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        public IErrorService _errrorService;
        public KeHoachVonCacDuAnController(IDuAnService duanService,  INguonVonService nguonvonService,
            
            IGiaiDoanDuAnService giaidoanService, ITinhTrangDuAnService tinhtrangDAService, INhomDuAnService nhomDAService, 
            IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, 
           IErrorService errorService) : base(errorService)
        {
            _duanService = duanService;
            _nguonvonService = nguonvonService;
            _giaidoanService = giaidoanService;
            _tinhtrangDAService = tinhtrangDAService;
            _nhomDAService = nhomDAService;
            _thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;

        }
       
        [HttpGet]
        [Route("getKHV")]
        public HttpResponseMessage GetKHV(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
               var lstDuAn = _duanService.GetAllKHV().Select(x=>new { x.IdDuAn,x.TenDuAn,x.ThoiGianThucHien,x.IdNhomDuAn,x.HopDongs,x.IdTinhTrangDuAn,
                    x.IdGiaiDoanDuAn,x.NguonVonDuAns,x.KeHoachVons,x.LapQuanLyDauTus
               });
                var modelVm = new
                {
                    DuAns = (filter == null) ? lstDuAn : lstDuAn.Where(x => x.TenDuAn.Contains(filter)),
                    NhomDuAns = _nhomDAService.GetAll().Select(x=> new { x.IdNhomDuAn,x.TenNhomDuAn}),
                    NguonVons = _nguonvonService.GetAll().Select(x => new { x.IdNguonVon, x.TenNguonVon }),
                    GiaiDoans = _giaidoanService.GetAll().Select(x => new { x.IdGiaiDoanDuAn, x.TenGiaiDoanDuAn }),
                    TinhTrangDuAns = _tinhtrangDAService.GetAll().Select(x => new { x.IdTinhTrangDuAn,x.TenTinhTrangDuAn }),
                    ThanhToanChiPhiKhacs = _thanhToanChiPhiKhacService.GetThanhToan_NguonVonThanhToanCPK().Select(x => new { x.IdDuAn, x.NguonVonThanhToanChiPhiKhacs }),
                };
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
          
        }
    }
}
