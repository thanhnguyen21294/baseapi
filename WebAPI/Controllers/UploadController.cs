﻿using Common;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using OfficeOpenXml;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Providers;

//using Xbim.CobieLiteUk;
//using Xbim.Ifc;
//using Xbim.ModelGeometry.Scene;
//using XbimExchanger.IfcToCOBieLiteUK;

namespace WebAPI.Controllers
{

    [RoutePrefix("api/upload")]
    [Authorize]
    public class UploadController : ApiControllerBase
    {
        IErrorService err;
        IVonCapNhatKLNTService _vonCapNhatKLNTService;
        IDuAnService _duAnService;
        ICapNhatKLNTService _capNhatKLNTService;
        public UploadController(IVonCapNhatKLNTService vonCapNhatKLNTService, IErrorService errorService, ICapNhatKLNTService capNhatKLNTService, IDuAnService duAnService) : base(errorService)
        {
            this.err = errorService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._duAnService = duAnService;
            this._capNhatKLNTService = capNhatKLNTService;
        }

        [HttpPost]
        [Route("saveFile/{folder}")]
        public HttpResponseMessage SaveMultipeFile(string folder)
        {
            var index = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string[] url = new string[httpRequest.Files.Count];
                string path = "";
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 10240 * 10240 * 1; //Size = 1 MB

                        if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string directory = string.Empty;
                            directory = "/" + folder + "/";
                            string fileName = postedFile.FileName;
                            string pathFolder;
                            switch (folder)
                            {
                                case "GiaiPhongMatBang":
                                    {
                                        pathFolder = "/UploadedFiles/DuAn/" + folder;
                                        break;
                                    }
                                default:
                                    {
                                        pathFolder = "/UploadedFiles/DuAn/VanBanDuAn/" + folder;
                                        break;
                                    }
                            }
                            if (!Directory.Exists(HttpContext.Current.Server.MapPath(pathFolder)))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(pathFolder));

                            }
                            path = Path.Combine(HttpContext.Current.Server.MapPath(pathFolder + "/"), fileName);
                            postedFile.SaveAs(path);

                            if (url == null)
                                url[0] = Path.Combine(directory, fileName);

                            else
                                url[index] = Path.Combine(directory, fileName);
                            index++;
                        }
                    }
                }
                switch (folder)
                {
                    case "GiaiPhongMatBang":
                        {
                            return Request.CreateResponse(HttpStatusCode.Created, "/UploadedFiles/DuAn" + url[0]); ;
                        }
                    default:
                        {
                            return Request.CreateResponse(HttpStatusCode.Created, url);
                        }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex); ;
            }
        }


        [HttpPost]
        [Route("saveFileVanBanDen/{folder}")]
        public HttpResponseMessage SaveMultipeFileVanBanDen(string folder)
        {
            var index = 0;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                string[] url = new string[httpRequest.Files.Count];
                string path = "";
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 10240 * 10240 * 1; //Size = 1 MB

                        if (postedFile.ContentLength > MaxContentLength)
                        {
                            var message = string.Format("Please Upload a file upto 1 mb.");
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string directory = string.Empty;
                            directory = "/" + folder + "/";
                            string fileName = postedFile.FileName;
                            string pathFolder = "/UploadedFiles/VanBanDen/" + folder;
                            if (!Directory.Exists(HttpContext.Current.Server.MapPath(pathFolder)))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(pathFolder));

                            }
                            path = Path.Combine(HttpContext.Current.Server.MapPath(pathFolder + "/"), fileName);
                            postedFile.SaveAs(path);

                            if (url == null)
                                url[0] = Path.Combine(directory, fileName);

                            else
                                url[index] = Path.Combine(directory, fileName);
                            index++;
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Created, url);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex); ;
            }
        }


        [HttpDelete]
        [Route("delete/")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string urlFile)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string[] arrayUrl = urlFile.Split('|');
                foreach (var item in arrayUrl)
                {

                    string path = HttpContext.Current.Server.MapPath("/UploadedFiles/DuAn/VanBanDuAn" + item);

                    File.Delete(path);

                }
                response = request.CreateResponse(HttpStatusCode.OK, "success");
                return response;
            });
        }

        [HttpPost]
        [Route("saveImage")]
        //[Permission(Action = "Create", Function = "USER")]
        public HttpResponseMessage SaveImage(string type = "")
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Mời bạn Upload File dưới định dạng .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Bạn chỉ có thể Upload File lớn nhất là 1MB.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            string directory = string.Empty;
                            if (type == "avatar")
                            {
                                directory = "/UploadedFiles/Avatars/";
                            }
                            else
                            {
                                directory = "/UploadedFiles/";
                            }
                            if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory)))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory));
                            }

                            string path = Path.Combine(HttpContext.Current.Server.MapPath(directory), postedFile.FileName);
                            //Userimage myfolder name where i want to save my image
                            postedFile.SaveAs(path);
                            return Request.CreateResponse(HttpStatusCode.OK, Path.Combine(directory, postedFile.FileName));
                        }
                    }

                    var message1 = string.Format("Cập nhật ảnh thành công.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Hãy Upload lên 1 ảnh.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex); ;

            }
        }

        [HttpPost]
        [Route("saveFileMobile")]
        public async Task<HttpResponseMessage> SaveFileMobile(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;
            Stream requestStream = await request.Content.ReadAsStreamAsync();
            string userId = User.Identity.GetUserId();
            string filename = request.Headers.GetValues("File-Name").First().ToString();

            try
            {
                if (requestStream != null)
                {
                    string path = HttpContext.Current.Server.MapPath("~/UploadedFiles/DuAn/GiaiPhongMatBang" + filename);
                    using (FileStream fileStream = File.Create(path, (int)requestStream.Length))
                    {
                        byte[] bytesInStream = new byte[requestStream.Length];
                        requestStream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                    response = Request.CreateResponse(HttpStatusCode.Created, "/UploadedFiles/DuAn/GiaiPhongMatBang/" + filename);
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadGateway, "Not eble to upload the File ");
                }
            }
            catch (HttpException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, "Error upload file! " + ex.ToString());
            }
        }

        [HttpPost]
        [Route("saveImageMobile")]
        public async Task<HttpResponseMessage> SaveImageMobile(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;
            Stream requestStream = await request.Content.ReadAsStreamAsync();
            string userId = User.Identity.GetUserId();
            string filename = userId + ".png";
            try
            {
                if (requestStream != null)
                {
                    string path = HttpContext.Current.Server.MapPath("~/UploadedFiles/Avatars/" + filename);
                    using (FileStream fileStream = File.Create(path, (int)requestStream.Length))
                    {
                        byte[] bytesInStream = new byte[requestStream.Length];
                        requestStream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                    //resize image
                    string folderThumb = HttpContext.Current.Server.MapPath("~/UploadedFiles/Avatars/thumbs/");
                    if (!Directory.Exists(folderThumb))
                    {
                        Directory.CreateDirectory(folderThumb);
                    }
                    string pathFile = HttpContext.Current.Server.MapPath("~/UploadedFiles/Avatars/thumbs/" + filename);
                    if (File.Exists(pathFile))
                    {
                        File.Delete(pathFile);
                    }
                    Image image = Image.FromFile(path);
                    float aspectRatio = (float)image.Size.Width / (float)image.Size.Height;
                    int newHeight = 150;
                    int newWidth = Convert.ToInt32(aspectRatio * newHeight);
                    Bitmap thumbBitmap = new Bitmap(newWidth, newHeight);
                    Graphics thumbGraph = Graphics.FromImage(thumbBitmap);
                    thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                    thumbGraph.DrawImage(image, imageRectangle);
                    thumbBitmap.Save(pathFile, System.Drawing.Imaging.ImageFormat.Png);
                    response = Request.CreateResponse(HttpStatusCode.Created, "/UploadedFiles/Avatars/thumbs/" + filename);
                    image.Dispose();
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadGateway, "Not eble to upload the File ");
                }
            }
            catch (HttpException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, "Error upload file! " + ex.ToString());
            }
        }

        [HttpPost]
        [Route("saveFileChatMobile")]
        public async Task<HttpResponseMessage> SaveFileChatMobile(HttpRequestMessage request)
        {
            HttpResponseMessage response = null;
            Stream requestStream = await request.Content.ReadAsStreamAsync();
            string userId = User.Identity.GetUserId();
            string filename = request.Headers.GetValues("File-Name").First().ToString();
            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
            try
            {
                if (requestStream != null)
                {
                    string folderPath = HttpContext.Current.Server.MapPath("~/UploadedFiles/ChatFiles/");
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string path = HttpContext.Current.Server.MapPath("~/UploadedFiles/ChatFiles/" + filename);
                    using (FileStream fileStream = File.Create(path, (int)requestStream.Length))
                    {
                        byte[] bytesInStream = new byte[requestStream.Length];
                        requestStream.Read(bytesInStream, 0, (int)bytesInStream.Length);
                        fileStream.Write(bytesInStream, 0, bytesInStream.Length);
                    }
                    response = Request.CreateResponse(HttpStatusCode.Created, "/UploadedFiles/ChatFiles/" + filename);

                    //resize image
                    if (AllowedFileExtensions.Contains(filename.Substring(filename.LastIndexOf('.')).ToLower()))
                    {
                        string folderThumb = HttpContext.Current.Server.MapPath("~/UploadedFiles/ChatFiles/thumbs/");
                        if (!Directory.Exists(folderThumb))
                        {
                            Directory.CreateDirectory(folderThumb);
                        }
                        string pathFile = HttpContext.Current.Server.MapPath("~/UploadedFiles/ChatFiles/thumbs/" + filename);
                        if (File.Exists(pathFile))
                        {
                            File.Delete(pathFile);
                        }
                        Image image = Image.FromFile(path);
                        float aspectRatio = (float)image.Size.Width / (float)image.Size.Height;
                        int newHeight = 150;
                        int newWidth = Convert.ToInt32(aspectRatio * newHeight);
                        Bitmap thumbBitmap = new Bitmap(newWidth, newHeight);
                        Graphics thumbGraph = Graphics.FromImage(thumbBitmap);
                        thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imageRectangle);
                        thumbBitmap.Save(pathFile, System.Drawing.Imaging.ImageFormat.Png);
                        response = Request.CreateResponse(HttpStatusCode.Created, "/UploadedFiles/ChatFiles/thumbs/" + filename);
                        image.Dispose();

                    }
                    return response;
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadGateway, "Not eble to upload the File ");
                }
            }
            catch (HttpException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadGateway, "Error upload file! " + ex.ToString());
            }
        }

        [HttpPost]
        [Route("saveExcelFile")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage SaveExcelFile(HttpRequestMessage request)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            //try
            //{

            var httpRequest = HttpContext.Current.Request;

            foreach (string file in httpRequest.Files)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                var postedFile = httpRequest.Files[file];
                if (postedFile != null && postedFile.ContentLength > 0)
                {
                    IList<string> AllowedFileExtensions = new List<string> { ".xlsx" };
                    var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                    var extension = ext.ToLower();
                    if (!AllowedFileExtensions.Contains(extension))
                    {
                        return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Hệ thống chỉ hỗ trợ định dạng file xlsx.");
                    }
                    else
                    {
                        string directory = string.Empty;

                        directory = "/UploadedFiles/CapNhatKLNTGiaiNgan/";

                        if (!Directory.Exists(HttpContext.Current.Server.MapPath(directory)))
                        {
                            Directory.CreateDirectory(HttpContext.Current.Server.MapPath(directory));

                        }

                        var url = directory + postedFile.FileName;


                        string path = Path.Combine(HttpContext.Current.Server.MapPath(directory), postedFile.FileName);

                        if (_capNhatKLNTService.GetAll().Where(x => x.FileUrl.Contains(url)).Count() > 0)
                        {
                            return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "File đã tồn tại. Vui lòng kiểm tra lại.");
                        }

                        postedFile.SaveAs(path);

                        FileInfo existingFile = new FileInfo(path);
                        using (ExcelPackage package = new ExcelPackage(existingFile))
                        {
                            //get the first worksheet in the workbook
                            //ExcelWorksheet sheetVonHuyen = package.Workbook.Worksheets[1];
                            //ExcelWorksheet sheetVonTP = package.Workbook.Worksheets[2];

                            ExcelWorksheet sheetVonHuyen = package.Workbook.Worksheets["BC.VH"];
                            ExcelWorksheet sheetVonTP = package.Workbook.Worksheets["BC.VTP"];

                            try
                            {
                                if (sheetVonHuyen.ToString() == "BC.VH" && sheetVonTP.ToString() == "BC.VTP")
                                {
                                    int totalsheetVonHuyencolCount = sheetVonHuyen.Dimension.End.Column;  //get Column Count
                                    int totalsheetVonHuyenrowCount = sheetVonHuyen.Dimension.End.Row;     //get row count

                                    int totalsheetVonTPcolCount = sheetVonTP.Dimension.End.Column;  //get Column Count
                                    int totalsheetVonTProwCount = sheetVonTP.Dimension.End.Row;     //get row count

                                    int i = 0;
                                    int fromCol = 1;
                                    List<VonCapNhatKLNT> list = new List<VonCapNhatKLNT>();
                                    DateTime timeNow = DateTime.Now;
                                    #region Add file info to CapNhatKLNT
                                    var datenow = DateTime.Now;
                                    string day;
                                    string mont;
                                    if (datenow.Day < 10)
                                    {
                                        day = "0" + datenow.Day.ToString();
                                    }
                                    else
                                    {
                                        day = datenow.Day.ToString();
                                    }

                                    if (datenow.Month < 10)
                                    {
                                        mont = "0" + datenow.Month.ToString();
                                    }
                                    else
                                    {
                                        mont = datenow.Month.ToString();
                                    }

                                    var szFileName = postedFile.FileName.Substring(0, postedFile.FileName.Length - 5).Trim();

                                    CapNhatKLNT capNhatKLNT = new CapNhatKLNT
                                    {
                                        FileUrl = url,
                                        FileName = szFileName/* + "_" +  $"{DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year}"*/,
                                        CreatedDate = DateTime.ParseExact((day + "/" + mont + "/" + datenow.Year).ToString(), "dd/MM/yyyy", new CultureInfo("vi-VN")),
                                        CreatedBy = User.Identity.Name,
                                        Status = true

                                    };
                                    var capNhatKLNTAdded = _capNhatKLNTService.Add(capNhatKLNT);
                                    _capNhatKLNTService.Save();
                                    #endregion

                                    try
                                    {

                                        for (i = 13; i <= totalsheetVonHuyenrowCount; i++)
                                        {
                                            if (sheetVonHuyen.Cells["J" + i].Value != null)
                                            {
                                                var excelRow = sheetVonHuyen.Cells[i, fromCol, i, totalsheetVonHuyencolCount];

                                                if (string.IsNullOrEmpty(excelRow[$"B{i}"].Text.Trim()))
                                                {
                                                    break;
                                                }

                                                string klntvh = "";
                                                if (excelRow[$"AK{i}"].Text.Trim().Length > 0)
                                                {
                                                    klntvh = excelRow[$"AK{i}"].Text.Trim();
                                                    if (klntvh == "-")
                                                    {
                                                        klntvh = "0";
                                                    }
                                                }


                                                string tdgnvh = "";
                                                if (excelRow[$"AL{i}"].Text.Trim().Length > 0)
                                                {
                                                    tdgnvh = excelRow[$"AL{i}"].Text.Trim();
                                                    if (tdgnvh == "-")
                                                    {
                                                        tdgnvh = "0";
                                                    }
                                                }

                                                string szMaDAVonHuyen = sheetVonHuyen.Cells["J" + i].Text.Trim();

                                                if (szMaDAVonHuyen.IndexOf(',') > 0)
                                                {
                                                    szMaDAVonHuyen = szMaDAVonHuyen.Replace(",", "");
                                                }

                                                if (szMaDAVonHuyen.IndexOf('.') > 0)
                                                {
                                                    szMaDAVonHuyen = szMaDAVonHuyen.Substring(0, szMaDAVonHuyen.IndexOf('.'));
                                                }

                                                var vonCapNhatKLNT = new VonCapNhatKLNT()
                                                {
                                                    CreateDateTime = timeNow,
                                                    KLNTVonHuyen = klntvh,
                                                    TDGNVonHuyen = tdgnvh,
                                                    KLNTVonTP = "",
                                                    TDGNVonTP = "",
                                                    isVonHuyen = true,
                                                    isVonTP = false,
                                                    MaDuAn = szMaDAVonHuyen
                                                };

                                                list.Add(vonCapNhatKLNT);
                                                //vonCapNhatKLNT = _vonCapNhatKLNTService.Add(vonCapNhatKLNT);
                                                //_vonCapNhatKLNTService.Save();
                                            }
                                        }

                                        List<VonCapNhatKLNT> list1 = new List<VonCapNhatKLNT>();

                                        for (i = 12; i <= totalsheetVonTProwCount; i++)
                                        {
                                            if (sheetVonTP.Cells["J" + i].Value != null)
                                            {
                                                //try
                                                //{
                                                var excelRow = sheetVonTP.Cells[i, fromCol, i, totalsheetVonTPcolCount];

                                                if (string.IsNullOrEmpty(excelRow[$"B{i}"].Text.Trim()))
                                                {
                                                    break;
                                                }
                                                string klntvtp = "";
                                                if (excelRow[$"Z{i}"].Text.Trim().Length > 0)
                                                {
                                                    klntvtp = excelRow[$"Z{i}"].Text.Trim();
                                                    if (klntvtp == "-")
                                                    {
                                                        klntvtp = "0";
                                                    }
                                                }

                                                string tdgnvtp = "";
                                                if (excelRow[$"AA{i}"].Text.Trim().Length > 0)
                                                {
                                                    tdgnvtp = excelRow[$"AA{i}"].Text.Trim();
                                                    if (tdgnvtp == "-")
                                                    {
                                                        tdgnvtp = "0";
                                                    }

                                                }

                                                string szMaDAVonTP = sheetVonTP.Cells["J" + i].Text.Trim();

                                                if (szMaDAVonTP.IndexOf(',') > 0)
                                                {
                                                    szMaDAVonTP.Replace(",", "");
                                                }
                                                if (szMaDAVonTP.IndexOf('.') > 0)
                                                {
                                                    szMaDAVonTP.Substring(0, szMaDAVonTP.IndexOf('.'));
                                                }

                                                var list2 = list.Where(x => x.MaDuAn == szMaDAVonTP).FirstOrDefault();
                                                if (list2 != null)
                                                {
                                                    list2.KLNTVonTP = klntvtp;
                                                    list2.TDGNVonTP = tdgnvtp;
                                                    list2.isVonTP = true;
                                                }
                                                else
                                                {
                                                    var vonCapNhatKLNT = new VonCapNhatKLNT()
                                                    {
                                                        CreateDateTime = timeNow,
                                                        KLNTVonHuyen = "",
                                                        TDGNVonHuyen = "",
                                                        KLNTVonTP = klntvtp,
                                                        TDGNVonTP = tdgnvtp,
                                                        MaDuAn = szMaDAVonTP,
                                                        isVonHuyen = false,
                                                        isVonTP = true
                                                    };
                                                    list.Add(vonCapNhatKLNT);
                                                }
                                                //}
                                                //catch(Exception ex)
                                                //{

                                                //}
                                            }
                                        }

                                        foreach (var item in list)
                                        {

                                            int idDA = _duAnService.GetAll().Where(x => x.Ma.ToString() == item.MaDuAn).Select(x => x.IdDuAn).FirstOrDefault();
                                            if (idDA > 0)
                                            {
                                                item.IdDuAn = idDA;
                                                item.IdFile = capNhatKLNTAdded.IdFile;

                                                _vonCapNhatKLNTService.Add(item);
                                            }

                                        }
                                        _vonCapNhatKLNTService.Save();

                                    }
                                    catch (Exception ex)
                                    {
                                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                                    }
                                }
                            }
                            catch
                            {
                                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Không đúng định dạng File Import.");
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, Path.Combine(directory, postedFile.FileName));
                    }
                }
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Vui lòng chọn File để upload.");
                //var message1 = string.Format("Upload File thành công.");
                //return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
            }
            return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Vui lòng chọn File để upload.");
            //var res = string.Format("Hãy Upload File.");
            //dict.Add("error", res);
            //return Request.CreateResponse(HttpStatusCode.NotFound, dict);

            //}
            //catch (Exception ex)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);

            //}
        }
    }
}
