﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.Common;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tiendodauthau")]
    public class TienDoDauThauController : ApiControllerBase
    {
        public IDuAnService _duAnService;
        public IDanhMucNhaThauService _danhMucNhaThauService;
        public TienDoDauThauController(IDuAnService duAnService,IErrorService errorService,IDanhMucNhaThauService danhMucNhaThauService):base(errorService)
        {
            _duAnService = duAnService;
            _danhMucNhaThauService = danhMucNhaThauService;


        }
        [HttpGet]
        [Route("getall")]
        public HttpResponseMessage GetAll(HttpRequestMessage request )
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var duan = _duAnService.GetAllTienDoDauThau().ToList();
              
                response = request.CreateResponse(HttpStatusCode.OK, duan);
                return response;

            });
        }
    }
}
