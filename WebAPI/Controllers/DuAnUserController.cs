﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Providers;


namespace WebAPI.Controllers
{
    [RoutePrefix("api/duanuser")]
    [Authorize]
    public class DuAnUserController : ApiControllerBase
    {
        private IDuAnUserService _duAnUserService;
        private IDeleteService _deleteService;

        public DuAnUserController(
            IErrorService errorService,
            IDeleteService deleteService,
            IDuAnUserService duAnUserService) : base(errorService)
        {
            this._duAnUserService = duAnUserService;
            this._deleteService = deleteService;
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _duAnUserService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdDuanUser);

                var modelVm = Mapper.Map<IEnumerable<DuAnUser>, IEnumerable<DuAnUserViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getduantenuser")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetDuAnTenUser(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _duAnUserService.GetDuAnTenUser().OrderByDescending(x => x.IdDuanUser).GroupBy(x => x.IdDuAn)
                .Select(x => new { 
                    IDDuAn = x.Key,
                    TenUser = string.Join("; ",x.Select(y => y.AppUsers.FullName))
                });
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getbyidduanphieu")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdDuAnPhieu(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                var model = _duAnUserService.GetByIdDuAnPhieu(idDA).OrderByDescending(x => x.IdDuanUser).Select(x => new {
                    IDDuAn = x.IdDuAn,
                    TenNhom = x.AppUsers.NhomDuAnTheoUser.TenNhomDuAn,
                    UserId = x.UserId,
                    x.AppUsers.FullName,
                    x.AppUsers.IdNhomDuAnTheoUser,
                    role = AppUserManager.GetRoles(x.UserId)[0].ToString()
                }).Distinct();
                var Users = model.Select(x => new
                {
                    x.UserId,
                    x.FullName
                });
                var PhongXuLyTrucTiep = model.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).GroupBy(x => new { x.IdNhomDuAnTheoUser, x.TenNhom }).Select(x => new { 
                    x.Key.IdNhomDuAnTheoUser,
                    x.Key.TenNhom,
                    ChucVu = x.GroupBy(y => y.role).Select(y => new {
                        y.Key,
                        Users = y.Select(z => new {
                            UserId = z.UserId,
                            z.FullName,
                            z.role
                        })
                    }).OrderByDescending(o => o.Key)
                }).FirstOrDefault();
                var PhongPhoiHop = model.Where(x => x.IdNhomDuAnTheoUser != idNhomDuAnTheoUser && x.IdNhomDuAnTheoUser > 6).GroupBy(x => new { x.IdNhomDuAnTheoUser, x.TenNhom }).Select(y => new {
                    y.Key.IdNhomDuAnTheoUser,
                    y.Key.TenNhom,
                    ChucVu = y.GroupBy(yy => yy.role).Select(z => new {
                        z.Key,
                        Users = z.Select(zz => new {
                            UserId = zz.UserId,
                            zz.FullName,
                            zz.role
                        })
                    }).OrderByDescending(o => o.Key)
                });
                var PhongGiamDoc = model.Where(x => x.IdNhomDuAnTheoUser == 5).Select(y => new {
                            y.UserId,
                            y.FullName,
                            y.role
                });
                var PhongPhoGiamDoc = model.Where(x => x.IdNhomDuAnTheoUser == 6).Select(y => new {
                    y.UserId,
                    y.FullName,
                    y.role
                });
                response = request.CreateResponse(HttpStatusCode.OK, new { 
                    PhongXuLyTrucTiep, 
                    PhongPhoiHop,
                    PhongGiamDoc,
                    PhongPhoGiamDoc,
                    Users
                });

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage update(HttpRequestMessage request, UpdateDuAnUserViewModels listIdUser)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    // lấy ID phòng cảu tài khoản đang đăng nhập
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
                if (User.IsInRole("Trưởng phòng"))
                {
                    // lấy danh sách dự án user theo IDDuAn
                    var model = _duAnUserService.GetByIdDuAn(listIdUser.IdDuAn).OrderByDescending(x => x.IdDuanUser);
                    foreach (DuAnUser das in model)
                    {
                        // nếu user thuộc phòng đang xét
                        if (das.AppUsers.IdNhomDuAnTheoUser == idNhomDuAnTheoUser)
                        {
                            var roleUser = AppUserManager.GetRoles(das.UserId);
                            if (roleUser.Contains("Nhân viên"))
                                _duAnUserService.Delete(das.IdDuanUser);
                        }
                    }
                    _duAnUserService.Save();
                }
                if (User.IsInRole("Giám đốc"))
                {
                    // lấy danh sách dự án user theo IDDuAn
                    var model = _duAnUserService.GetByIdDuAn(listIdUser.IdDuAn).OrderByDescending(x => x.IdDuanUser);
                    foreach (DuAnUser das in model)
                    {
                        // nếu user khác user đang xét
                        if (das.AppUsers.Id != User.Identity.GetUserId())
                        {
                           _duAnUserService.Delete(das.IdDuanUser);
                        }
                    }
                    _duAnUserService.Save();
                }

                //var listIdUserNew = new JavaScriptSerializer().Deserialize<List<string>>(listIdUser);
                foreach (string uNew in listIdUser.lstIdUser)
                {
                    var newDuanUser = new DuAnUser();
                    newDuanUser.UserId = uNew;
                    newDuanUser.IdDuAn = listIdUser.IdDuAn;
                    newDuanUser.CreatedDate = DateTime.Now;
                    newDuanUser.CreatedBy = User.Identity.GetUserId();
                    newDuanUser.Status = true;
                    _duAnUserService.Add(newDuanUser);
                }
                _duAnUserService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, "OK");
                return response;
            });
        }

    }
}
