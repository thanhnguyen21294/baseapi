﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/nhomduan")]
    [Authorize]
    public class NhomDuAnController : ApiControllerBase
    {
        private INhomDuAnService _nhomDuAnService;
        public NhomDuAnController(IErrorService errorService, INhomDuAnService nhomDuAnService) : base(errorService)
        {
            this._nhomDuAnService = nhomDuAnService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nhomDuAnService.GetAll().OrderByDescending(x => x.IdNhomDuAn);

                var modelVm = Mapper.Map<IEnumerable<NhomDuAn>, IEnumerable<NhomDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NHOMDUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _nhomDuAnService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<NhomDuAn>, IEnumerable<NhomDuAnViewModels>>(model);

                PaginationSet<NhomDuAnViewModels> pagedSet = new PaginationSet<NhomDuAnViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NHOMDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nhomDuAnService.GetById(id);

                var responseData = Mapper.Map<NhomDuAn, NhomDuAnViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "NHOMDUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, NhomDuAnViewModels nhomDuAnVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newNhomDuAn = new NhomDuAn();
                    newNhomDuAn.UpdateNhomDuAn(nhomDuAnVm);
                    newNhomDuAn.CreatedDate = DateTime.Now;
                    newNhomDuAn.CreatedBy = User.Identity.GetUserId();
                    _nhomDuAnService.Add(newNhomDuAn);
                    _nhomDuAnService.Save();

                    var responseData = Mapper.Map<NhomDuAn, NhomDuAnViewModels>(newNhomDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "NHOMDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, NhomDuAnViewModels nhomDuAnVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbNhomDuAn = _nhomDuAnService.GetById(nhomDuAnVm.IdNhomDuAn);

                    dbNhomDuAn.UpdateNhomDuAn(nhomDuAnVm);
                    dbNhomDuAn.UpdatedDate = DateTime.Now;
                    dbNhomDuAn.UpdatedBy = User.Identity.GetUserId();
                    _nhomDuAnService.Update(dbNhomDuAn);
                    _nhomDuAnService.Save();

                    var responseData = Mapper.Map<NhomDuAn, NhomDuAnViewModels>(dbNhomDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NHOMDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhomDuAnService.Delete(id);
                    _nhomDuAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NHOMDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listNhomDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listNhomDuAn)
                    {
                        _nhomDuAnService.Delete(item);
                    }

                    _nhomDuAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listNhomDuAn.Count);
                }

                return response;
            });
        }
    }
}
