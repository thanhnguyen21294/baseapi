﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/thanhtoanhopdong")]
    [Authorize]
    public class ThanhToanHopDongController : ApiControllerBase
    {
        private IThanhToanHopDongService _ThanhToanHopDongService;
        private INguonVonGoiThauThanhToanService _NguonVonGoiThauThanhToanService;
        private INguonVonDuAnGoiThauService _NguonVonDuAnGoiThauService;
        private INguonVonDuAnService _NguonVonDuAnService;
        private INguonVonService _NguonVonService;

      
        public ThanhToanHopDongController(IErrorService errorService, IThanhToanHopDongService thanhToanHopDongService, INguonVonGoiThauThanhToanService nguonVonGoiThauThanhToanService,
             INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService, INguonVonDuAnService nguonVonDuAnService, INguonVonService nguonVonService) : base(errorService)
        {
            this._ThanhToanHopDongService = thanhToanHopDongService;
            _NguonVonGoiThauThanhToanService = nguonVonGoiThauThanhToanService;
            _NguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            _NguonVonDuAnService = nguonVonDuAnService;
            _NguonVonService = nguonVonService;


        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var nguonVonGoiThauThanhToans = _NguonVonGoiThauThanhToanService.GetAll();
                var model = _ThanhToanHopDongService.GetAll().OrderByDescending(x => x.IdThanhToanHopDong);

                IEnumerable<ThanhToanHopDongViewModels> modelVm = model.GroupJoin(nguonVonGoiThauThanhToans, x => x.IdThanhToanHopDong, y => y.IdThanhToanHopDong, (x, grp) => new ThanhToanHopDongViewModels
                {
                    IdThanhToanHopDong = x.IdThanhToanHopDong,
                    IdHopDong = x.IdHopDong,
                    GhiChu = x.GhiChu,
                    ThoiDiemThanhToan = x.ThoiDiemThanhToan.ToString(),
                    NoiDung = x.NoiDung,
                    TrangThaiGui = x.TrangThaiGui,
                    TongGiaTriThanhToan = grp.Select(y => y.GiaTri).Sum(),
                    TongGiaTriThuHoiUng = grp.Where(y => y.GiaTriTabmis != null).Select(y => Convert.ToDouble(y.GiaTriTabmis)).Sum(),
                    DeNghiThanhToan = x.DeNghiThanhToan,
                    IdThucHien = x.IdThucHien,
                    ThucHienHopDong = x.ThucHienHopDong,
                    TenNguonVons = grp.Join(_NguonVonDuAnGoiThauService.GetAll(), a => a.IdNguonVonDuAnGoiThau, b => b.IdNguonVonDuAnGoiThau, (a, b) => new {
                        IdNguonVonDuAn = b.IdNguonVonDuAn
                    }).Join(_NguonVonDuAnService.GetAll(), a => a.IdNguonVonDuAn, b => b.IdNguonVonDuAn, (a, b) => new {
                        IdNguonVon = b.IdNguonVon
                    }).Join(_NguonVonService.GetAll(), a => a.IdNguonVon, b => b.IdNguonVon, (a, b) => new{
                        TenNguonVon=b.TenNguonVon
                    }).Select(c=>c.TenNguonVon),
                   
                });


                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _ThanhToanHopDongService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getbyidhopdong")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idHopDong)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanHopDongService.GetByIdHopDong(idHopDong);
                var modelVm = new List<ThanhToanHopDongViewModels>();
                 foreach(var item in model)
                {
                   ThanhToanHopDongViewModels itemVm = new ThanhToanHopDongViewModels();
                    itemVm.IdThanhToanHopDong =item.IdThanhToanHopDong;
                    itemVm.IdHopDong = item.IdHopDong;
                    itemVm.GhiChu = item.GhiChu;
                    itemVm.ThoiDiemThanhToan = (item.ThoiDiemThanhToan != null) ? item.ThoiDiemThanhToan.ToString() : null;
                    itemVm.NoiDung = item.NoiDung;
                    itemVm.TongGiaTriThanhToan = (item.NguonVonGoiThauThanhToans != null) ? item.NguonVonGoiThauThanhToans.Select(y => y.GiaTri).Sum() : 0;
                    itemVm.NguonVonGoiThauThanhToans = item.NguonVonGoiThauThanhToans;
                    itemVm.DeNghiThanhToan = item.DeNghiThanhToan;
                    itemVm.IdThucHien = item.IdThucHien;
                    itemVm.ThucHienHopDong = item.ThucHienHopDong;
                    modelVm.Add(itemVm);
                }
               
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var modelVm = new ThanhToanHopDongViewModels();
                var model = _ThanhToanHopDongService.GetById(id);
                modelVm.IdThanhToanHopDong = model.IdThanhToanHopDong;
                modelVm.IdHopDong = model.IdHopDong;
                modelVm.GhiChu = model.GhiChu;
                modelVm.ThoiDiemThanhToan =(model.ThoiDiemThanhToan!=null)? model.ThoiDiemThanhToan.ToString():null;
                modelVm.NoiDung = model.NoiDung;
                modelVm.TongGiaTriThanhToan =(model.NguonVonGoiThauThanhToans!=null)? model.NguonVonGoiThauThanhToans.Select(y => y.GiaTri).Sum():0;
                modelVm.NguonVonGoiThauThanhToans = model.NguonVonGoiThauThanhToans;
                modelVm.DeNghiThanhToan = model.DeNghiThanhToan;
                modelVm.IdThucHien = model.IdThucHien;
                modelVm.ThucHienHopDong = model.ThucHienHopDong;
               

                var response = request.CreateResponse(HttpStatusCode.OK,modelVm);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, ThanhToanHopDongViewModels vmThanhToanHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThanhToanHopDong = new ThanhToanHopDong();
                    newThanhToanHopDong.IdHopDong = vmThanhToanHopDongViewModels.IdHopDong;
                    newThanhToanHopDong.NoiDung = vmThanhToanHopDongViewModels.NoiDung;
                    newThanhToanHopDong.IdThucHien = vmThanhToanHopDongViewModels.IdThucHien;
                    newThanhToanHopDong.DeNghiThanhToan = vmThanhToanHopDongViewModels.DeNghiThanhToan;
                    if (vmThanhToanHopDongViewModels.ThoiDiemThanhToan != "" && vmThanhToanHopDongViewModels.ThoiDiemThanhToan != null)
                        newThanhToanHopDong.ThoiDiemThanhToan = DateTime.ParseExact(vmThanhToanHopDongViewModels.ThoiDiemThanhToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newThanhToanHopDong.GhiChu = vmThanhToanHopDongViewModels.GhiChu;
                    newThanhToanHopDong.CreatedDate = DateTime.Now;
                    newThanhToanHopDong.CreatedBy = User.Identity.GetUserId();
                    newThanhToanHopDong.Status = true;

                    foreach (var item in vmThanhToanHopDongViewModels.NguonVonGoiThauThanhToans)
                    {
                        var a = item;
                        _NguonVonGoiThauThanhToanService.Add(new NguonVonGoiThauThanhToan
                        {
                            IdNguonVonDuAnGoiThau = item.IdNguonVonDuAnGoiThau,
                            GiaTri = item.GiaTri,
                            GiaTriTabmis = item.GiaTriTabmis,
                            //TamUng = item.TamUng,
                            Status = true
                        });
                    }

                    _ThanhToanHopDongService.Add(newThanhToanHopDong);
                    _ThanhToanHopDongService.Save();

                    vmThanhToanHopDongViewModels.NguonVonGoiThauThanhToans = newThanhToanHopDong.NguonVonGoiThauThanhToans;
                    vmThanhToanHopDongViewModels.TongGiaTriThanhToan = (newThanhToanHopDong.NguonVonGoiThauThanhToans != null) ? newThanhToanHopDong.NguonVonGoiThauThanhToans.Select(x => x.GiaTri).Sum() : 0;
                    vmThanhToanHopDongViewModels.IdThanhToanHopDong = newThanhToanHopDong.IdThanhToanHopDong;
                    response = request.CreateResponse(HttpStatusCode.Created, vmThanhToanHopDongViewModels);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThanhToanHopDongViewModels vmThanhToanHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThanhToanHopDong = _ThanhToanHopDongService.GetById(vmThanhToanHopDongViewModels.IdThanhToanHopDong);
                    updateThanhToanHopDong.IdHopDong = vmThanhToanHopDongViewModels.IdHopDong;
                    updateThanhToanHopDong.NoiDung = vmThanhToanHopDongViewModels.NoiDung;
                    updateThanhToanHopDong.IdThucHien = vmThanhToanHopDongViewModels.IdThucHien;
                    updateThanhToanHopDong.DeNghiThanhToan = vmThanhToanHopDongViewModels.DeNghiThanhToan;
                    if (vmThanhToanHopDongViewModels.ThoiDiemThanhToan != "" && vmThanhToanHopDongViewModels.ThoiDiemThanhToan != null)
                    {
                        updateThanhToanHopDong.ThoiDiemThanhToan = DateTime.ParseExact(vmThanhToanHopDongViewModels.ThoiDiemThanhToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateThanhToanHopDong.ThoiDiemThanhToan = null;
                    }
                        
                    updateThanhToanHopDong.GhiChu = vmThanhToanHopDongViewModels.GhiChu;
                    updateThanhToanHopDong.GhiChu = vmThanhToanHopDongViewModels.GhiChu;
                    updateThanhToanHopDong.UpdatedDate = DateTime.Now;
                    updateThanhToanHopDong.UpdatedBy = User.Identity.GetUserId();
                    try
                    {
                        _NguonVonGoiThauThanhToanService.DeleteByIdThanhToanHopDong(vmThanhToanHopDongViewModels.IdThanhToanHopDong);
                        foreach (var item in vmThanhToanHopDongViewModels.NguonVonGoiThauThanhToans)
                        {
                            _NguonVonGoiThauThanhToanService.Add(new NguonVonGoiThauThanhToan
                            {
                                IdNguonVonDuAnGoiThau = item.IdNguonVonDuAnGoiThau,
                                IdThanhToanHopDong = vmThanhToanHopDongViewModels.IdThanhToanHopDong,
                                GiaTri = item.GiaTri,
                                //TamUng = item.TamUng,
                                GiaTriTabmis = item.GiaTriTabmis,
                                Status = true
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _ThanhToanHopDongService.Update(updateThanhToanHopDong);
                    _ThanhToanHopDongService.Save();

                  
                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThanhToanHopDong = _ThanhToanHopDongService.GetById(vnTrangThaiUp.ID);
                    updateThanhToanHopDong.TrangThaiGui = 1;
                    updateThanhToanHopDong.UpdatedDate = DateTime.Now;
                    updateThanhToanHopDong.UpdatedBy = User.Identity.GetUserId();
                    _ThanhToanHopDongService.Update(updateThanhToanHopDong);
                    _ThanhToanHopDongService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonGoiThauThanhToanService.DeleteByIdThanhToanHopDong(id);
                    _ThanhToanHopDongService.Delete(id);
                    _ThanhToanHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THANHTOANHOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDGoiThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDGoiThau)
                    {
                        _NguonVonGoiThauThanhToanService.DeleteByIdThanhToanHopDong(item);
                        _ThanhToanHopDongService.Delete(item);
                    }

                    _ThanhToanHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listIDGoiThau.Count);
                }

                return response;
            });
        }

        [Route("getbyyearduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetByYearDuAn(HttpRequestMessage request, string year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanHopDongService.GetByYearDuAn(year);
                var modelVm = new List<ThanhToanHopDongViewModels>();
                foreach (var item in model)
                {
                    ThanhToanHopDongViewModels itemVm = new ThanhToanHopDongViewModels();
                    itemVm.IdThanhToanHopDong = item.IdThanhToanHopDong;
                    itemVm.IdHopDong = item.IdHopDong;
                    itemVm.GhiChu = item.GhiChu;
                    itemVm.ThoiDiemThanhToan = (item.ThoiDiemThanhToan != null) ? item.ThoiDiemThanhToan.ToString() : null;
                    itemVm.NoiDung = item.NoiDung;
                    itemVm.TongGiaTriThanhToan = (item.NguonVonGoiThauThanhToans != null) ? item.NguonVonGoiThauThanhToans.Select(y => y.GiaTri).Sum() : 0;
                    itemVm.NguonVonGoiThauThanhToans = item.NguonVonGoiThauThanhToans;
                    itemVm.DeNghiThanhToan = item.DeNghiThanhToan;
                    itemVm.IdThucHien = item.IdThucHien;
                    itemVm.ThucHienHopDong = item.ThucHienHopDong;
                    itemVm.TongGiaTriThanhToan = 0;
                    modelVm.Add(itemVm);
                }

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallnguonvongoithauthanhtoanbyyear")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetAllNguonVonGoiThauThanhToanByYear(HttpRequestMessage request, string year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanHopDongService.GetAllNguonVonGoiThauThanhToanByYear(year);

                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }
    }
}
