﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using Model.Models.KeToanModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/TheoDoiHoSoQuyetToan")]
    public class TheoDoiHoSoQuyetToanController : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IQLTD_CongViecService _qLTDCongViecService;
        private IDuAnHoSoQuyetToanService _duAnHoSoQuyetToanService;
        private IDanhMucHoSoQuyetToanService _danhMucHoSoQuyetToanService;
        private IBaoCaoTableService _baoCaoTableService;
        public TheoDoiHoSoQuyetToanController(IErrorService errorService,
            IDuAnService duAnService,
            IQLTD_CongViecService qLTDCongViecService,
            IDuAnHoSoQuyetToanService duAnHoSoQuyetToanService,
            IBaoCaoTableService baoCaoTableService,
            IDanhMucHoSoQuyetToanService danhMucHoSoQuyetToanService
           ) : base(errorService)
        {
            _duAnService = duAnService;
            _qLTDCongViecService = qLTDCongViecService;
            _duAnHoSoQuyetToanService = duAnHoSoQuyetToanService;
            _baoCaoTableService = baoCaoTableService;
            _danhMucHoSoQuyetToanService  = danhMucHoSoQuyetToanService;
        }

        [Route("View")]
        [HttpGet]
        public HttpResponseMessage View(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                // coong vieecj chuyen giao
                List<int> IDCVKetThuc = _qLTDCongViecService.GetCVKetThuc();
                // ho so quyet toan
                var lstHSQT = _duAnHoSoQuyetToanService.GetForView().ToList();
                var lstHSQTThieu = lstHSQT.Where(x => x.KeToan == 2).Select(x => x.IdDuAn).Distinct().ToList();

                var lstHSQTDu = lstHSQT.GroupBy(x => x.IdDuAn).Select(x => new
                {
                    IDDuAn = x.Key,
                    KeToan = x.Select(y => new
                    {
                        NhanDu = y.KeToan
                    }).Skip(1).All(i => i.NhanDu == 1)
                }).ToList().Where(z => z.KeToan).Select(z => z.IDDuAn).ToList();

                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }
                var lstDuAn = _duAnService.FilterForTheoDoiHoSoQT(filter, idUser);
                var lstReturn = lstDuAn.Select(y => new
                {
                    y.IdDuAn,
                    y.TenDuAn,
                    KetThuc = IDCVKetThuc.Contains(y.IdDuAn) ? 1 : 0,
                    HSQTThieu = lstHSQTThieu.Contains(y.IdDuAn) ? 1 : 0,
                    HSQTDu = lstHSQTDu.Contains(y.IdDuAn) ? 1 : 0
                }).ToList();
                var response = request.CreateResponse(HttpStatusCode.OK, lstReturn);
                return response;
            });
        }

        [Route("xuatbaocao")]
        [HttpGet]
        public HttpResponseMessage XuatBaoCao(HttpRequestMessage request, string filter)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = BaoCaoExcel(filter);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
        }

        private string BaoCaoExcel(string filter)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TheoDoiHoSoQuyetToan.xlsx");
            string documentName = string.Format("TheoDoiHoSoQuyetToan" + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    

                    // coong vieecj chuyen giao
                    List<int> IDCVKetThuc = _qLTDCongViecService.GetCVKetThuc();
                    // ho so quyet toan
                    var lstDanhMucHSQTRoot = _danhMucHoSoQuyetToanService.GetAll().Where(x => x.IdParent == null).ToDictionary(x => x.IdDanhMucHoSoQuyetToan, x => x.TenDanhMucHoSoQuyetToan);
                    var lstHSQT = _duAnHoSoQuyetToanService.GetForView().ToList();
                    var lstHSQTThieu = lstHSQT.Where(x => x.KeToan == 2).Select(x => x.IdDuAn).Distinct().ToList();

                    var lstHSQTHSThieu = lstHSQT.Where(x => x.KeToan == 2).GroupBy(x => x.IdDuAn).Select(x => new
                    {
                        x.Key,
                        DSHS = x.Select(y => new
                        {
                            y.DanhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan,
                            y.DanhMucHoSoQuyetToan.IdParent
                        }).GroupBy(y => y.IdParent).Select(z => new DSHoSoThieuTheoGiaiDoan
                        {
                            TenGiaiDoan = lstDanhMucHSQTRoot.ContainsKey(z.Key.Value) ? lstDanhMucHSQTRoot[Convert.ToInt32(z.Key.Value)] : "",
                            TenDSHoSo = string.Join("\n", z.Select(zz => " + " + zz.TenDanhMucHoSoQuyetToan)),
                            Count = z.Count()
                        })
                    }).ToDictionary(x =>x.Key, x=> x.DSHS.ToList());

                    var lstHSQTDu = lstHSQT.GroupBy(x => x.IdDuAn).Select(x => new
                    {
                        IDDuAn = x.Key,
                        KeToan = x.Select(y => new
                        {
                            NhanDu = y.KeToan
                        }).Skip(1).All(i => i.NhanDu == 1)
                    }).ToList().Where(z => z.KeToan).Select(z => z.IDDuAn).ToList();

                    string idUser = "";
                    if (!User.IsInRole("Admin"))
                    {
                        idUser = User.Identity.GetUserId();
                    }
                    var lstDuAn = _duAnService.FilterForTheoDoiHoSoQT(filter, idUser);
                    var lstReturn = lstDuAn.Select(y => new ExcelTheoDoiHoSoQT
                    {
                        IdDuAn = y.IdDuAn,
                        TenDuAn = y.TenDuAn,
                        KetThuc = IDCVKetThuc.Contains(y.IdDuAn) ? 1 : 0,
                        HSQTThieu = lstHSQTThieu.Contains(y.IdDuAn) ? 1 : 0,
                        HSQTDu = lstHSQTDu.Contains(y.IdDuAn) ? 1 : 0,
                        DSHSThieu = lstHSQTHSThieu.ContainsKey(y.IdDuAn) ? lstHSQTHSThieu[y.IdDuAn] : new List<DSHoSoThieuTheoGiaiDoan>(),
                    }).ToList();

                    int rowIndex1 = 7;
                    int count1 = 0;
                  
                    ExcelWorksheet sheetHT = pck.Workbook.Worksheets["Dự án hoàn thành TH đầu tư"];
                    // tổng cộng
                    
                    var lstHTThucHienDT = new List<ExcelTheoDoiHoSoQT>(lstReturn);
                    lstHTThucHienDT.RemoveAll(x => x.KetThuc == 0);
                    int totalHT = lstHTThucHienDT.Sum(x => x.KetThuc);
                    sheetHT.Cells[6, 3].Value = totalHT;
                    
                    // insert các dự án vào excel
                    foreach (var kq in lstHTThucHienDT)
                    {
                        count1++;
                        sheetHT.Cells[rowIndex1, 1].Value = count1;
                        sheetHT.Cells[rowIndex1, 2].Value = kq.TenDuAn;
                        sheetHT.Cells[rowIndex1, 3].Value = kq.KetThuc;
                        sheetHT.Cells[rowIndex1, 4].Value = "";
                        rowIndex1++;
                    }
                    sheetHT.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetHT.Column(4).Width = 18;
                    sheetHT.Column(1).Style.WrapText = true;
                    sheetHT.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetHT.Column(4).Style.WrapText = true;
                    sheetHT.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    if (rowIndex1 > 7)
                    {
                        using (ExcelRange rng = sheetHT.Cells["A7:D" + (rowIndex1 - 1)])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.Font.Size = 11;
                        }
                    }

                    ExcelWorksheet sheetHSDu = pck.Workbook.Worksheets["Dự án đủ hồ sơ"];
                    int rowIndex2 = 7;
                    int count2 = 0;
                    var lstHSDu = new List<ExcelTheoDoiHoSoQT>(lstReturn); ;
                    lstHSDu.RemoveAll(x => x.HSQTDu == 0);
                    // tổng cộng
                    int totalDu = lstHSDu.Sum(x => x.HSQTDu);
                    sheetHSDu.Cells[6, 3].Value = totalDu;

                    // insert các dự án vào excel
                    foreach (var kq in lstHSDu)
                    {
                        count2++;
                        sheetHSDu.Cells[rowIndex2, 1].Value = count2;
                        sheetHSDu.Cells[rowIndex2, 2].Value = kq.TenDuAn;
                        sheetHSDu.Cells[rowIndex2, 3].Value = kq.HSQTDu;
                        sheetHSDu.Cells[rowIndex2, 4].Value = "";
                        rowIndex2++;
                    }
                    sheetHSDu.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetHSDu.Column(1).Width = 18;
                    sheetHSDu.Column(1).Style.WrapText = true;
                    sheetHSDu.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetHSDu.Column(4).Style.WrapText = true;
                    sheetHSDu.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    if (rowIndex2 > 7)
                    {
                        using (ExcelRange rng = sheetHSDu.Cells["A7:D" + (rowIndex2 - 1)])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.Font.Size = 11;
                        }
                    }

                    ExcelWorksheet sheetHSThieu = pck.Workbook.Worksheets["Dự án thiếu hồ sơ"];
                    int rowIndex3 = 7;
                    int count3 = 0;
                    var lstHSThieu = new List<ExcelTheoDoiHoSoQT>(lstReturn); ;
                    lstHSThieu.RemoveAll(x => x.HSQTThieu == 0);
                    // tổng cộng
                    int totalThieu = 0;
                    // insert các dự án vào excel
                    foreach (var kq in lstHSThieu)
                    {
                        count3++;
                        int rowDA = rowIndex3;
                        sheetHSThieu.Cells[rowIndex3, 1].Value = count3;
                        sheetHSThieu.Cells[rowIndex3, 2].Value = kq.TenDuAn;
                        sheetHSThieu.Cells[rowIndex3, 3].Value = "";
                        sheetHSThieu.Cells[rowIndex3, 4].Value = "";
                        sheetHSThieu.Cells[rowIndex3, 5].Value = "";
                        sheetHSThieu.Cells[rowIndex3, 6].Value = "";
                        foreach (var hs in kq.DSHSThieu)
                        {
                            rowIndex3++;
                            sheetHSThieu.Cells[rowIndex3, 1].Value = "";
                            sheetHSThieu.Cells[rowIndex3, 2].Value = "";
                            sheetHSThieu.Cells[rowIndex3, 3].Value = "";
                            sheetHSThieu.Cells[rowIndex3, 4].Value = hs.TenGiaiDoan;
                            sheetHSThieu.Cells[rowIndex3, 5].Value = hs.TenDSHoSo;
                            sheetHSThieu.Cells[rowIndex3, 6].Value = "";
                            totalThieu = totalThieu + hs.Count;
                        }
                        sheetHSThieu.Cells[rowDA, 3].Value = totalThieu;
                        rowIndex3++;
                    }
                    sheetHSThieu.Cells[6, 3].Value = totalThieu;
                    sheetHSThieu.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheetHSThieu.Column(1).Style.WrapText = true;
                    sheetHSThieu.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetHSThieu.Column(4).Style.WrapText = true;
                    sheetHSThieu.Column(4).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheetHSThieu.Column(5).Style.WrapText = true;
                    sheetHSThieu.Column(5).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    if (rowIndex3 > 7)
                    {
                        using (ExcelRange rng = sheetHSThieu.Cells["A7:F" + (rowIndex3 - 1)])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.Font.Size = 11;
                        }
                    }
                    pck.SaveAs(new FileInfo(fullPath));
                    //Thêm báo cáo vào database
                    _baoCaoTableService.AddBaoCao(documentName, "BC_GPMB_DKKHTDCT", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }

        }

        private void In_Style(ExcelWorksheet sheet, int row, int col)
        {
            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet.Cells[row, col].Style.Numberformat.Format = "#,###.##";
        }

        private string GetString(string[] lst)
        {
            string sz = "";
            foreach (string s in lst)
            {
                if (s != "" && s != null)
                {
                    sz += s + "; ";
                }
            }
            if (sz != "")
                sz = sz.Substring(0, sz.Length - 2);
            return sz;
        }

    }
    public class ExcelTheoDoiHoSoQT
    {
        public int IdDuAn { set; get; }
        public string TenDuAn { set; get; }
        public int KetThuc { set; get; }
        public int HSQTThieu { set; get; }
        public int HSQTDu { set; get; }
        public List<DSHoSoThieuTheoGiaiDoan > DSHSThieu { set; get; }
    }
    public class DSHoSoThieuTheoGiaiDoan
    {
        public string TenGiaiDoan { set; get; }
        public string TenDSHoSo { set; get; }
        public int Count { set; get; }
    }
}
