﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using Model.Models.KeToanModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/TheoDoiTinhHinhGiaiNgan")]
    public class TheoDoiTinhHinhGiaiNganController : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IThucHienHopDongService _thucHienHopDongService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private IKeHoachVonService _keHoachVonService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        private ILoaiChiPhiService _loaiChiPhiService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        private IBaoLanhTHHDService _baoLanhTHHDService;
        public TheoDoiTinhHinhGiaiNganController(IErrorService errorService,
            IDuAnService duAnService, 
            IThucHienHopDongService thucHienHopDongService,
            IThanhToanHopDongService thanhToanHopDongService,
            IThanhToanChiPhiKhacService thanhToanChiPhiKhacService,
            IKeHoachVonService keHoachVonService,
            ILoaiChiPhiService loaiChiPhiService,
            IBaoCaoTableService baoCaoTableService,
            IBaoLanhTHHDService baoLanhTHHDService,
            IQLTD_KeHoachService qLTD_KeHoachService) : base(errorService)
        {
            this._duAnService = duAnService;
            _thucHienHopDongService  = thucHienHopDongService;
            _thanhToanHopDongService = thanhToanHopDongService;
            _keHoachVonService = keHoachVonService;
            _thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            _loaiChiPhiService = loaiChiPhiService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
            _baoLanhTHHDService = baoLanhTHHDService;
        }

        [Route("View")]
        [HttpGet]
        public HttpResponseMessage View(HttpRequestMessage request, string filter, int nam)
        {
            return CreateHttpResponse(request, () =>
            {
                // lấy giá trị nghiệm thu
                var nghiemThus = _thucHienHopDongService.GetForKeToanViewTheoDoi().Select(x => new
                {
                    x.IdHopDong,
                    x.HopDong?.IdGoiThau,
                    x.IdThucHien,
                    x.NgayNghiemThu,
                    x.GiaTriNghiemThu,
                    x.IdLoaiChiPhi,
                    x.LoaiChiPhi?.TenLoaiChiPhi,
                    x.IdDuAn
                });
                // nghiệm thu theo hợp đồng
                var nghiemThuTheoHDs = nghiemThus.Where(x => x.IdHopDong != null).Select(x => new
                {
                    x.IdHopDong,
                    x.IdGoiThau,
                    x.IdThucHien,
                    x.NgayNghiemThu,
                    x.GiaTriNghiemThu
                });
                var nghiemthuN1 = nghiemThuTheoHDs.Where(x => Convert.ToDateTime(x.NgayNghiemThu).Year < nam).GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.Sum(ss => ss.GiaTriNghiemThu)
                }).ToDictionary(t => t.Key, t => t.GiaTri);
                var nghiemthuN = nghiemThuTheoHDs.Where(x => Convert.ToDateTime(x.NgayNghiemThu).Year == nam).GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.Sum(ss => ss.GiaTriNghiemThu)
                }).ToDictionary(t => t.Key, t => t.GiaTri);
                var nghiemthuAll = nghiemThuTheoHDs.GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.Sum(ss => ss.GiaTriNghiemThu)
                }).ToDictionary(t => t.Key, t => t.GiaTri);

                // nghiệm thu theo chi phí
                var nghiemThuTheoCPs = nghiemThus.Where(x => x.IdHopDong == null).Select(x => new
                {
                    x.IdLoaiChiPhi,
                    x.TenLoaiChiPhi,
                    x.IdThucHien,
                    x.NgayNghiemThu,
                    x.GiaTriNghiemThu,
                    x.IdDuAn
                }).ToList();
                var nghiemthu_CP_N1 = nghiemThuTheoCPs.Where(x => Convert.ToDateTime(x.NgayNghiemThu).Year < nam).GroupBy(x => x.IdDuAn).Select(x => new {
                    x.Key,
                    ChiPhis = x.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        GiaTri = gg.Sum(ggg => ggg.GiaTriNghiemThu)
                    }).ToDictionary(d => d.IdLoaiChiPhi, d => d.GiaTri)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);

                var nghiemthu_CP_N = nghiemThuTheoCPs.Where(x => Convert.ToDateTime(x.NgayNghiemThu).Year == nam).GroupBy(x => x.IdDuAn).Select(x => new {
                    x.Key,
                    ChiPhis = x.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        GiaTri = gg.Sum(ggg => ggg.GiaTriNghiemThu)
                    }).ToDictionary(d => d.IdLoaiChiPhi, d => d.GiaTri)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);

                var nghiemthu_CP = nghiemThuTheoCPs.GroupBy(x => x.IdDuAn).Select(x => new {
                    x.Key,
                    ChiPhis = x.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        GiaTri = gg.Sum(ggg => ggg.GiaTriNghiemThu)
                    }).ToDictionary(d => d.IdLoaiChiPhi, d => d.GiaTri)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);

                // thanh toán hợp đông
                var thanhtoanHDs = _thanhToanHopDongService.GetForKeToanView().Select(x => new
                {
                    x.IdHopDong,
                    x.HopDong.IdGoiThau,
                    x.IdThucHien,
                    x.ThoiDiemThanhToan,
                    nguonvons = x.NguonVonGoiThauThanhToans.Select(y => new {
                        y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.Ma,
                        y.GiaTri,
                        y.GiaTriTabmis
                    })
                });
                var thanhtoanN1 = thanhtoanHDs.Where(x => Convert.ToDateTime(x.ThoiDiemThanhToan).Year < nam).GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.Sum(ss => ss.nguonvons.Sum(sss => sss.GiaTri))
                }).ToDictionary(t => t.Key, t => t?.GiaTri);
                var thanhtoanN = thanhtoanHDs.Where(x => Convert.ToDateTime(x.ThoiDiemThanhToan).Year == nam).GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.SelectMany(ss => ss.nguonvons.Select(sss => new NguonVonKHV {
                        Ma = sss.Ma,
                        GiaTri = sss.GiaTri,
                        GiaTriTabmis = sss.GiaTriTabmis
                    }))
                }).ToDictionary(t => t.Key, t => t.GiaTri);
                var thanhtoanAll = thanhtoanHDs.GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTri = s.Sum(ss => ss.nguonvons.Sum(sss => sss.GiaTri)),
                }).ToDictionary(t => t.Key, t => t?.GiaTri);

                var thanhtoanAllTabmis = thanhtoanHDs.GroupBy(g => g.IdGoiThau).Select(s => new
                {
                    s.Key,
                    GiaTriTabmis = s.Sum(ss => ss.nguonvons.Sum(sss => sss.GiaTriTabmis))
                }).ToDictionary(t => t.Key, t => t?.GiaTriTabmis);

                // kế hoạch vốn
                var keHoachVons = _keHoachVonService.GetKHVKeToanTheoDoi(nam);

                // thanh toán chi phí khác
                var thanhtoanCPK = _thanhToanChiPhiKhacService.GetForViewKeToan(nam);
                var thanhtoanCPKDuan_N1 = thanhtoanCPK.Where(x => x.NgayThanhToan.Value.Year < nam).GroupBy(g => g.IdDuAn).Select(g => new
                {
                    g.Key,
                    ChiPhis = g.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        GiaTri = gg.Sum(ggg => ggg.NguonVonThanhToanChiPhiKhacs.Sum(n => n.GiaTri))
                    }).ToDictionary(d => d.IdLoaiChiPhi, d => d.GiaTri)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);

                var thanhtoanCPKDuan_N = thanhtoanCPK.Where(x => x.NgayThanhToan.Value.Year == nam).GroupBy(g => g.IdDuAn).Select(g => new
                {
                    g.Key,
                    ChiPhis = g.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        NguonVons = gg.SelectMany(ggg => ggg.NguonVonThanhToanChiPhiKhacs.Select(n => new NguonVonKHV
                        {
                            Ma = n.NguonVonDuAn.NguonVon.Ma,
                            GiaTri = n.GiaTri,
                            GiaTriTabmis = n.GiaTriTabmis
                        }))
                    }).ToDictionary(d => d.IdLoaiChiPhi, d => d.NguonVons)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);

                var thanhtoanCPKDuan = thanhtoanCPK.GroupBy(g => g.IdDuAn).Select(g => new
                {
                    g.Key,
                    ChiPhis = g.GroupBy(gg => gg.IdLoaiChiPhi).Select(gg => new
                    {
                        IdLoaiChiPhi = Convert.ToInt32(gg.Key),
                        GiaTri = gg.Sum(ggg => ggg.NguonVonThanhToanChiPhiKhacs.Sum(n => n.GiaTri))
                    }).ToDictionary(dd => dd.IdLoaiChiPhi, dd => dd.GiaTri)
                }).ToDictionary(d => d.Key, d => d.ChiPhis);


                // bảo lãnh THHD
                var baolanhs = _baoLanhTHHDService.GetAll().GroupBy(x => x.IdHopDong).Select(x => new
                {
                    IdHopDong = x.Key,
                    BL = new BaoLanhViewModelInClasss
                    {
                        BLTH = x.Where(z => z.LoaiBaoLanh == 1).Select(zz => new BaoLanhViewModelInClass
                        {
                            NgayHetHan = zz.NgayHetHan,
                            SoTien = zz.SoTien
                        }).FirstOrDefault(),
                        BLTU = x.Where(z => z.LoaiBaoLanh == 2).Select(zz => new BaoLanhViewModelInClass
                        {
                            NgayHetHan = zz.NgayHetHan,
                            SoTien = zz.SoTien
                        }).FirstOrDefault()
                    }
                }).ToDictionary(z => z.IdHopDong, z => z.BL);

                var pheduyets = _qLTD_KeHoachService.GetKPPheDuyet().ToDictionary(x => x.IdDuAn, x => new { x.CTDT, x.CBDT, x.CBTH });

                // cây chi phí QLDA-GPMB-Khác
                var loaiCP = _loaiChiPhiService.GetAll();
                
                // xử lý cây của view
                var lstDuAn = _duAnService.FilterByNameDuAn(filter, nam);
                var lstReturn = lstDuAn.GroupBy(x => x.GiaiDoanDuAn).Select(x => new
                {
                    x.Key.TenGiaiDoanDuAn,
                    duans = x.Select(y => new
                    {
                        y.IdDuAn,
                        y.TenDuAn,
                        y.GhiChuKeToan,
                        PheDuyetDuAn = pheduyets.ContainsKey(y.IdDuAn) ? pheduyets[y.IdDuAn] : null,
                        NhomA = y.NhomDuAn?.TenNhomDuAn == "Nhóm A" ? "1" : "",
                        NhomB = y.NhomDuAn?.TenNhomDuAn == "Nhóm B" ? "1" : "",
                        NhomC = y.NhomDuAn?.TenNhomDuAn == "Nhóm C" ? "1" : "",
                        y.DiaDiem,
                        y.QuyMoNangLuc,
                        y.ThoiGianThucHien,
                        y.Ma,
                        y.ChuDauTu?.TenChuDauTu,
                        XD = y.GoiThaus.Where(g => g.IdLinhVucDauThau == 4).Select(g => GetDataGoiThau(g, nghiemthuN1, nghiemthuN, nghiemthuAll, thanhtoanN1, thanhtoanN, thanhtoanAll, thanhtoanAllTabmis, baolanhs)),
                        TB = y.GoiThaus.Where(g => g.IdLinhVucDauThau == 3).Select(g => GetDataGoiThau(g, nghiemthuN1, nghiemthuN, nghiemthuAll, thanhtoanN1, thanhtoanN, thanhtoanAll, thanhtoanAllTabmis, baolanhs)),
                        TV = y.GoiThaus.Where(g => g.IdLinhVucDauThau == 1 || g.IdLinhVucDauThau == 2).Select(g => GetDataGoiThau(g, nghiemthuN1, nghiemthuN, nghiemthuAll, thanhtoanN1, thanhtoanN, thanhtoanAll, thanhtoanAllTabmis, baolanhs)),
                        NguonVonKHV = keHoachVons.ContainsKey(y.IdDuAn) ? keHoachVons[y.IdDuAn] : null,
                        ChiPhiThanhToan = loaiCP.Select(c => new
                        {
                            c.IdLoaiChiPhi,
                            c.TenLoaiChiPhi,
                            c.IdLoaiChiPhiCha,
                            N1 = thanhtoanCPKDuan_N1.ContainsKey(y.IdDuAn) ? (thanhtoanCPKDuan_N1[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? thanhtoanCPKDuan_N1[y.IdDuAn][c.IdLoaiChiPhi] : 0) : 0,
                            NguonVonN = thanhtoanCPKDuan_N.ContainsKey(y.IdDuAn) ? (thanhtoanCPKDuan_N[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? thanhtoanCPKDuan_N[y.IdDuAn][c.IdLoaiChiPhi] : null) : null,
                            All = thanhtoanCPKDuan.ContainsKey(y.IdDuAn) ? (thanhtoanCPKDuan[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? thanhtoanCPKDuan[y.IdDuAn][c.IdLoaiChiPhi] : 0) : 0,
                            NghiemThuN1 = nghiemthu_CP_N1.ContainsKey(y.IdDuAn) ? (nghiemthu_CP_N1[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? nghiemthu_CP_N1[y.IdDuAn][c.IdLoaiChiPhi] : 0) : 0,
                            NghiemThuN = nghiemthu_CP_N.ContainsKey(y.IdDuAn) ? (nghiemthu_CP_N[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? nghiemthu_CP_N[y.IdDuAn][c.IdLoaiChiPhi] : 0) : 0,
                            NghiemThu = nghiemthu_CP.ContainsKey(y.IdDuAn) ? (nghiemthu_CP[y.IdDuAn].ContainsKey(c.IdLoaiChiPhi) ? nghiemthu_CP[y.IdDuAn][c.IdLoaiChiPhi] : 0) : 0,
                        })
                    })
                }).ToList();

                var response = request.CreateResponse(HttpStatusCode.OK, lstReturn);

                return response;
            });
        }

        public object GetDataGoiThau(GoiThau g, Dictionary<int?,double?> nghiemthuN1, Dictionary<int?, double?> nghiemthuN, Dictionary<int?, double?> nghiemthuAll,
            Dictionary<int, double?> thanhtoanN1, Dictionary<int, IEnumerable<NguonVonKHV>> thanhtoanN, Dictionary<int, double?> thanhtoanAll, Dictionary<int, double?> thanhtoanAllTabmis, Dictionary<int, BaoLanhViewModelInClasss> baolanh)
        {
            return new
            {
                g.IdGoiThau,
                g.TenGoiThau,
                g.GiaGoiThau,
                NTn1 = nghiemthuN1.ContainsKey(g.IdGoiThau) ? nghiemthuN1[g.IdGoiThau] : null,
                NTn = nghiemthuN.ContainsKey(g.IdGoiThau) ? nghiemthuN[g.IdGoiThau] : null,
                NTAll = nghiemthuAll.ContainsKey(g.IdGoiThau) ? nghiemthuAll[g.IdGoiThau] : null,
                TTn1 = thanhtoanN1.ContainsKey(g.IdGoiThau) ? thanhtoanN1[g.IdGoiThau] : null,
                TTn = thanhtoanN.ContainsKey(g.IdGoiThau) ? thanhtoanN[g.IdGoiThau] : null,
                TTAll = thanhtoanAll.ContainsKey(g.IdGoiThau) ? thanhtoanAll[g.IdGoiThau] : null,
                TTALLTabmis = thanhtoanAllTabmis.ContainsKey(g.IdGoiThau) ? thanhtoanAllTabmis[g.IdGoiThau] : null,
                HopDong = g.HopDongs.Select(gg => new
                {
                    gg.SoHopDong,
                    gg.DanhMucNhaThau.TenNhaThau,
                    gg.GiaHopDong,
                    gg.ThoiGianThucHien,
                    gg.NgayBatDau,
                    gg.NgayHoanThanh,
                    BL = baolanh.ContainsKey(gg.IdHopDong) ? baolanh[gg.IdHopDong] : null
                }).FirstOrDefault()
            };
        }

    }
    public class NguonVonKHV
    {
        public string Ma { get; set; }
        public double GiaTri { get; set; }
        public double? GiaTriTabmis { get; set; }
    }

    public class BaoLanhViewModelInClasss
    {
        public BaoLanhViewModelInClass BLTH { get; set; }
        public BaoLanhViewModelInClass BLTU { get; set; }
    }
    public class BaoLanhViewModelInClass
    {
        public DateTime? NgayHetHan { get; set; }
        public double? SoTien { get; set; }
    }
}
