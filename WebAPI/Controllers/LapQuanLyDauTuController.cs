﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Providers;
using System.Web.Script.Serialization;
using AutoMapper;
using Model.Models;
using WebAPI.Models.DuAn;
using Microsoft.AspNet.Identity;
using System.Globalization;
using System.Security.Claims;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/lapquanlydautu")]
    public class LapQuanLyDauTuController : ApiControllerBase
    {
        private ILapQuanLyDauTuService _LapQuanLyDauTuService;
        private INguonVonDuAnService _NguonVonDuAnService;
        private IDuAnService _duAnService;
        private IDeleteService _deleteService;
        public LapQuanLyDauTuController(IErrorService errorService, ILapQuanLyDauTuService lapquanlydautuService, INguonVonDuAnService nguonvonduanService, IDuAnService duAnService, IDeleteService deleteService) : base(errorService)
        {
            this._LapQuanLyDauTuService = lapquanlydautuService;
            this._NguonVonDuAnService = nguonvonduanService;
            this._duAnService = duAnService;
            this._deleteService = deleteService;
        }
        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int iLoai, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _LapQuanLyDauTuService.GetAll(filter, iLoai).OrderByDescending(x => x.IdLapQuanLyDauTu);

                var modelVm = Mapper.Map<IEnumerable<LapQuanLyDauTu>, IEnumerable<LapQuanLyDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallbyidduan")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdDuAn(HttpRequestMessage request, int iLoai, int idDuan)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _LapQuanLyDauTuService.GetAllByIdDuAn(iLoai, idDuan);

                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getallfothistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _LapQuanLyDauTuService.GetAll().OrderByDescending(x => x.IdLapQuanLyDauTu);

                var modelVm = Mapper.Map<IEnumerable<LapQuanLyDauTu>, IEnumerable<LapQuanLyDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("lapduandautugetoneofone")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPTINHHINHPHEDUYET")]
        public HttpResponseMessage LapDuAnDauTuGetOneOfOne(HttpRequestMessage request)
        {
            
            return CreateHttpResponse(request, () =>
            {
                List<LapQuanLyDauTu> lapQuanLyDauTu = new List<LapQuanLyDauTu>();
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }
                
                var duans = _duAnService.GetAll();

                if (idNhomDuAnTheoUser == null)
                {
                    duans = duans.ToList();
                }
                else
                {
                    duans = duans.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).ToList();
                }
                
                foreach (var DuAn in duans)
                {

                    var model = _LapQuanLyDauTuService.GetAll(1).Where(x => x.IdDuAn == DuAn.IdDuAn && (x.NgayPheDuyet != null || x.NgayTrinh != null));
                    if (model.Count() > 0)
                    {
                        var b = model.OrderByDescending(x => x.NgayPheDuyet).ThenByDescending(y => y.NgayTrinh).ThenByDescending(z => z.IdLapQuanLyDauTu);
                        var c = b.FirstOrDefault();
                        lapQuanLyDauTu.Add(c);
                    }

                }
                var modelVm = Mapper.Map<IEnumerable<LapQuanLyDauTu>, IEnumerable<LapQuanLyDauTuViewModels>>(lapQuanLyDauTu);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);


                return response;

            });
        }

        [Route("laptkkttdtgetoneofone")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "TONGHOPTINHHINHPHEDUYET")]
        public HttpResponseMessage LapTKKTTDTGetOneOfOne(HttpRequestMessage request)
        {

            return CreateHttpResponse(request, () =>
            {
                List<LapQuanLyDauTu> lapQuanLyDauTu = new List<LapQuanLyDauTu>();
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }
                
                var duans = _duAnService.GetAll();

                if (idNhomDuAnTheoUser == null)
                {
                    duans = duans.ToList();
                }
                else
                {
                    duans = duans.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).ToList();
                }
                var lqldt = _LapQuanLyDauTuService.GetAll(2);
                foreach (var DuAn in duans)
                {

                    var model = lqldt.Where(x => x.IdDuAn == DuAn.IdDuAn && (x.NgayPheDuyet != null || x.NgayTrinh != null));
                    if (model.Count() > 0)
                    {
                        var b = model.OrderByDescending(x => x.NgayPheDuyet).ThenByDescending(y => y.NgayTrinh).ThenByDescending(z => z.IdLapQuanLyDauTu);
                        var c = b.FirstOrDefault();
                        lapQuanLyDauTu.Add(c);
                    }

                }
                var modelVm = Mapper.Map<IEnumerable<LapQuanLyDauTu>, IEnumerable<LapQuanLyDauTuViewModels>>(lapQuanLyDauTu);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);


                return response;
            });

        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUANLYDAUTU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDA,  int iLoai, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _LapQuanLyDauTuService.GetByFilter(idDA, iLoai, page, pageSize, "", out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LapQuanLyDauTu>, IEnumerable<LapQuanLyDauTuViewModels>>(model);

                PaginationSet<LapQuanLyDauTuViewModels> pagedSet = new PaginationSet<LapQuanLyDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUANLYDAUTU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _LapQuanLyDauTuService.GetById(id);

                var responseData = Mapper.Map<LapQuanLyDauTu, LapQuanLyDauTuViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Create(HttpRequestMessage request, LapQuanLyDauTuViewModels vmLapQuanLyDauTu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLapQuanLyDauTu = new LapQuanLyDauTu();
                    newLapQuanLyDauTu.SoQuyetDinh = vmLapQuanLyDauTu.SoQuyetDinh;
                    newLapQuanLyDauTu.IdDuAn = vmLapQuanLyDauTu.IdDuAn;
                    newLapQuanLyDauTu.LoaiQuanLyDauTu = vmLapQuanLyDauTu.LoaiQuanLyDauTu;
                    newLapQuanLyDauTu.NoiDung = vmLapQuanLyDauTu.NoiDung;
                    if (vmLapQuanLyDauTu.NgayTrinh != "" && vmLapQuanLyDauTu.NgayTrinh != null)
                    {
                        newLapQuanLyDauTu.NgayTrinh = DateTime.ParseExact(vmLapQuanLyDauTu.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        newLapQuanLyDauTu.NgayTrinh = null;
                    }
                        
                    newLapQuanLyDauTu.SoToTrinh = vmLapQuanLyDauTu.SoToTrinh;
                    newLapQuanLyDauTu.GiaTriTrinh = vmLapQuanLyDauTu.GiaTriTrinh;
                    if (vmLapQuanLyDauTu.NgayPheDuyet != "" && vmLapQuanLyDauTu.NgayPheDuyet != null)
                    {
                        newLapQuanLyDauTu.NgayPheDuyet = DateTime.ParseExact(vmLapQuanLyDauTu.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        newLapQuanLyDauTu.NgayPheDuyet = null;
                    }
                        
                    newLapQuanLyDauTu.NguoiPheDuyet = vmLapQuanLyDauTu.NguoiPheDuyet;
                    newLapQuanLyDauTu.LoaiDieuChinh = vmLapQuanLyDauTu.LoaiDieuChinh;
                    newLapQuanLyDauTu.TongGiaTri = vmLapQuanLyDauTu.TongGiaTri;
                    newLapQuanLyDauTu.XayLap = vmLapQuanLyDauTu.XayLap;
                    newLapQuanLyDauTu.ThietBi = vmLapQuanLyDauTu.ThietBi;
                    newLapQuanLyDauTu.GiaiPhongMatBang = vmLapQuanLyDauTu.GiaiPhongMatBang;
                    newLapQuanLyDauTu.TuVan = vmLapQuanLyDauTu.TuVan;
                    newLapQuanLyDauTu.QuanLyDuAn = vmLapQuanLyDauTu.QuanLyDuAn;
                    newLapQuanLyDauTu.Khac = vmLapQuanLyDauTu.Khac;
                    newLapQuanLyDauTu.DuPhong = vmLapQuanLyDauTu.DuPhong;
                    newLapQuanLyDauTu.GhiChu = vmLapQuanLyDauTu.GhiChu;
                    newLapQuanLyDauTu.IdCoQuanPheDuyet = vmLapQuanLyDauTu.IdCoQuanPheDuyet;
                    newLapQuanLyDauTu.CreatedDate = DateTime.Now;
                    newLapQuanLyDauTu.CreatedBy = User.Identity.GetUserId();
                    newLapQuanLyDauTu.Status = true;

                    var model = _NguonVonDuAnService.GetAll().OrderByDescending(x => x.IdNguonVonDuAn);
                    var allNguonVonDuAn = Mapper.Map<IEnumerable<NguonVonDuAn>, IEnumerable<NguonVonDuAnViewModels>>(model);

          
          
                    _LapQuanLyDauTuService.Add(newLapQuanLyDauTu);
                    _LapQuanLyDauTuService.Save();
                    
                    
                    foreach (var item in vmLapQuanLyDauTu.NguonVonDuAns)
                    {
                       var tempt= _NguonVonDuAnService.GetAll().Where(x => x.IdDuAn == item.IdDuAn && x.IdNguonVon == item.IdNguonVon).ToList();
                       if(tempt.Count()==0)
                        {
                            var newNguonVonDuAn = new NguonVonDuAn()
                            {
                                Status = true,
                                IdLapQuanLyDauTu = newLapQuanLyDauTu.IdLapQuanLyDauTu,
                                IdNguonVon = item.IdNguonVon,
                                IdDuAn = vmLapQuanLyDauTu.IdDuAn// vmLapQuanLyDauTu.IdDuAn
                            };

                            _NguonVonDuAnService.Add(newNguonVonDuAn);
                            _NguonVonDuAnService.Save();

                        }

                    }
                    var responseData = Mapper.Map<LapQuanLyDauTu, LapQuanLyDauTuViewModels>(newLapQuanLyDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Update(HttpRequestMessage request, LapQuanLyDauTuViewModels vmLapQuanLyDauTu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateLapQuanLyDauTu = _LapQuanLyDauTuService.GetById(vmLapQuanLyDauTu.IdLapQuanLyDauTu);
                    updateLapQuanLyDauTu.SoQuyetDinh = vmLapQuanLyDauTu.SoQuyetDinh;
                    updateLapQuanLyDauTu.IdDuAn = vmLapQuanLyDauTu.IdDuAn;
                    updateLapQuanLyDauTu.LoaiQuanLyDauTu = vmLapQuanLyDauTu.LoaiQuanLyDauTu;
                    updateLapQuanLyDauTu.NoiDung = vmLapQuanLyDauTu.NoiDung;
                    if (vmLapQuanLyDauTu.NgayTrinh != "" && vmLapQuanLyDauTu.NgayTrinh != null)
                    {
                        updateLapQuanLyDauTu.NgayTrinh = DateTime.ParseExact(vmLapQuanLyDauTu.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateLapQuanLyDauTu.NgayTrinh = null;
                    }
                        
                    updateLapQuanLyDauTu.SoToTrinh = vmLapQuanLyDauTu.SoToTrinh;
                    updateLapQuanLyDauTu.GiaTriTrinh = vmLapQuanLyDauTu.GiaTriTrinh;
                    if (vmLapQuanLyDauTu.NgayPheDuyet != "" && vmLapQuanLyDauTu.NgayPheDuyet != null)
                    {
                        updateLapQuanLyDauTu.NgayPheDuyet = DateTime.ParseExact(vmLapQuanLyDauTu.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateLapQuanLyDauTu.NgayPheDuyet = null;
                    }
                        
                    updateLapQuanLyDauTu.NguoiPheDuyet = vmLapQuanLyDauTu.NguoiPheDuyet;
                    updateLapQuanLyDauTu.LoaiDieuChinh = vmLapQuanLyDauTu.LoaiDieuChinh;
                    updateLapQuanLyDauTu.TongGiaTri = vmLapQuanLyDauTu.TongGiaTri;
                    updateLapQuanLyDauTu.XayLap = vmLapQuanLyDauTu.XayLap;
                    updateLapQuanLyDauTu.ThietBi = vmLapQuanLyDauTu.ThietBi;
                    updateLapQuanLyDauTu.GiaiPhongMatBang = vmLapQuanLyDauTu.GiaiPhongMatBang;
                    updateLapQuanLyDauTu.TuVan = vmLapQuanLyDauTu.TuVan;
                    updateLapQuanLyDauTu.QuanLyDuAn = vmLapQuanLyDauTu.QuanLyDuAn;
                    updateLapQuanLyDauTu.Khac = vmLapQuanLyDauTu.Khac;
                    updateLapQuanLyDauTu.DuPhong = vmLapQuanLyDauTu.DuPhong;
                    updateLapQuanLyDauTu.GhiChu = vmLapQuanLyDauTu.GhiChu;
                    updateLapQuanLyDauTu.IdCoQuanPheDuyet = vmLapQuanLyDauTu.IdCoQuanPheDuyet;
                    updateLapQuanLyDauTu.UpdatedDate = DateTime.Now;
                    updateLapQuanLyDauTu.UpdatedBy = User.Identity.GetUserId();
                 
                    try
                    {
                        List<int> formNguonVonDuAn = vmLapQuanLyDauTu.NguonVonDuAns.Select(x => x.IdNguonVon).ToList();
                        List<int> oldNguonVonDuAn = _NguonVonDuAnService.GetByIdLapQuanLyDauTu(vmLapQuanLyDauTu.IdLapQuanLyDauTu).Select(x => x.IdNguonVon).ToList();
                        foreach (int idNV in formNguonVonDuAn)
                        {
                            if (!oldNguonVonDuAn.Contains(idNV))
                            {
                                _NguonVonDuAnService.Add(new NguonVonDuAn
                                {
                                    IdNguonVon = idNV,
                                    IdLapQuanLyDauTu = vmLapQuanLyDauTu.IdLapQuanLyDauTu,
                                    IdDuAn = vmLapQuanLyDauTu.IdDuAn,
                                    Status = true
                                });
                                _NguonVonDuAnService.Save();
                            }
                        }
                        foreach (int idNV in oldNguonVonDuAn)
                        {
                            if (!formNguonVonDuAn.Contains(idNV))
                            {
                                _NguonVonDuAnService.DeleteByIdLapQuanLyDauTu(vmLapQuanLyDauTu.IdLapQuanLyDauTu, idNV);
                                _NguonVonDuAnService.Save();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _LapQuanLyDauTuService.Update(updateLapQuanLyDauTu);
                    _LapQuanLyDauTuService.Save();

                    var responseData = Mapper.Map<LapQuanLyDauTu, LapQuanLyDauTuViewModels>(updateLapQuanLyDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _deleteService.DeleteLapQuanLyDauTu(id);
                    _deleteService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

      
    }
}
