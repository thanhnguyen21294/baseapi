﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tiendochitiet")]
    [Authorize]
    public class TienDoChiTietController : ApiControllerBase
    {
        private ITienDoChiTietService _TienDoChiTietService;
        public TienDoChiTietController(IErrorService errorService, ITienDoChiTietService tienDoChiTietService) : base(errorService)
        {
            this._TienDoChiTietService = tienDoChiTietService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _TienDoChiTietService.GetAll().OrderByDescending(x => x.IdTienDoChiTiet);

                var modelVm = Mapper.Map<IEnumerable<TienDoChiTiet>, IEnumerable<TienDoChiTietViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idTDTH, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _TienDoChiTietService.GetByFilter(idTDTH,page,pageSize,"",out totalRow,filter);

                var modelVm = Mapper.Map<IEnumerable<TienDoChiTiet>, IEnumerable<TienDoChiTietViewModels>>(model);

                PaginationSet<TienDoChiTietViewModels> pagedSet = new PaginationSet<TienDoChiTietViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }
        [Route("getbyidtiendothuchien")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetbyIdTienDoThucHien(HttpRequestMessage request, int idTDTH, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _TienDoChiTietService.GetbyIdTienDoThucHien(idTDTH, page, pageSize, "", out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<TienDoChiTiet>, IEnumerable<TienDoChiTietViewModels>>(model);

                PaginationSet<TienDoChiTietViewModels> pagedSet = new PaginationSet<TienDoChiTietViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }
        
        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _TienDoChiTietService.GetById(id);

                var responseData = Mapper.Map<TienDoChiTiet, TienDoChiTietViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, TienDoChiTietViewModels vmTienDoChiTietViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newTienDoChiTiet = new TienDoChiTiet();
                    newTienDoChiTiet.IdTienDo = vmTienDoChiTietViewModels.IdTienDo;
                    newTienDoChiTiet.NoiDung = vmTienDoChiTietViewModels.NoiDung;
                    if (vmTienDoChiTietViewModels.NgayThucHien != "" && vmTienDoChiTietViewModels.NgayThucHien != null)
                        newTienDoChiTiet.NgayThucHien = DateTime.ParseExact(vmTienDoChiTietViewModels.NgayThucHien, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newTienDoChiTiet.TonTaiVuongMac = vmTienDoChiTietViewModels.TonTaiVuongMac;
                    newTienDoChiTiet.GhiChu = vmTienDoChiTietViewModels.GhiChu;
                    newTienDoChiTiet.CreatedDate = DateTime.Now;
                    newTienDoChiTiet.CreatedBy = User.Identity.GetUserId();
                    _TienDoChiTietService.Add(newTienDoChiTiet);
                    _TienDoChiTietService.Save();

                    var responseData = Mapper.Map<TienDoChiTiet, TienDoChiTietViewModels>(newTienDoChiTiet);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, TienDoChiTietViewModels vmTienDoChiTietViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateTienDoChiTiet = _TienDoChiTietService.GetById(vmTienDoChiTietViewModels.IdTienDoChiTiet);
                    updateTienDoChiTiet.IdTienDo = vmTienDoChiTietViewModels.IdTienDo;
                    updateTienDoChiTiet.NoiDung = vmTienDoChiTietViewModels.NoiDung;
                    if (vmTienDoChiTietViewModels.NgayThucHien != "" && vmTienDoChiTietViewModels.NgayThucHien != null)
                    {
                        updateTienDoChiTiet.NgayThucHien = DateTime.ParseExact(vmTienDoChiTietViewModels.NgayThucHien, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateTienDoChiTiet.NgayThucHien = null;
                    }
                        
                    updateTienDoChiTiet.TonTaiVuongMac = vmTienDoChiTietViewModels.TonTaiVuongMac;
                    updateTienDoChiTiet.UpdatedDate = DateTime.Now;
                    updateTienDoChiTiet.UpdatedBy = User.Identity.GetUserId();
                    _TienDoChiTietService.Update(updateTienDoChiTiet);
                    _TienDoChiTietService.Save();

                    var responseData = Mapper.Map<TienDoChiTiet, TienDoChiTietViewModels>(updateTienDoChiTiet);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _TienDoChiTietService.Delete(id);
                    _TienDoChiTietService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listCoQuanPheDuyet = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listCoQuanPheDuyet)
                    {
                        _TienDoChiTietService.Delete(item);
                    }

                    _TienDoChiTietService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listCoQuanPheDuyet.Count);
                }

                return response;
            });
        }
    }
}
