﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/danhmucnhathau")]
    [Authorize]
    public class DanhMucNhaThauController : ApiControllerBase
    {
        private IDanhMucNhaThauService _danhMucNhaThauService;
        public DanhMucNhaThauController(IErrorService errorService, IDanhMucNhaThauService danhMucNhaThauService) : base(errorService)
        {
            this._danhMucNhaThauService = danhMucNhaThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _danhMucNhaThauService.GetAll(idDuAn);

                var modelVm = Mapper.Map<IEnumerable<DanhMucNhaThau>, IEnumerable<DanhMucNhaThauViewModels>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallforlocduan")]
        [HttpGet]
        public HttpResponseMessage GetAllForLocDuAn(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _danhMucNhaThauService.GetAllForLocDuAn();
                var modelVm = Mapper.Map<IEnumerable<DanhMucNhaThau>, IEnumerable<DanhMucNhaThauViewModels>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getdmnts")]
        [HttpGet]
        public HttpResponseMessage GetDMNT(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _danhMucNhaThauService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<DanhMucNhaThau>, IEnumerable<DanhMucNhaThauViewModels>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("getbyduan")]
        [HttpGet]
        public HttpResponseMessage GetByDuAn(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _danhMucNhaThauService.GetAll(idDuAn);
                var modelVm = Mapper.Map<IEnumerable<DanhMucNhaThau>, IEnumerable<DanhMucNhaThauViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow;

                var model = _danhMucNhaThauService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<DanhMucNhaThau>, IEnumerable<DanhMucNhaThauViewModels>>(model);

                PaginationSet<DanhMucNhaThauViewModels> pagedSet = new PaginationSet<DanhMucNhaThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _danhMucNhaThauService.GetById(id);

                var responseData = Mapper.Map<DanhMucNhaThau, DanhMucNhaThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, DanhMucNhaThauViewModels danhMucNhaThautVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newDanhMucNhaThau = new DanhMucNhaThau();
                    newDanhMucNhaThau.UpdateDanhMucNhaThau(danhMucNhaThautVm);
                    newDanhMucNhaThau.CreatedDate = DateTime.Now;
                    newDanhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                    _danhMucNhaThauService.Add(newDanhMucNhaThau);
                    _danhMucNhaThauService.Save();

                    var responseData = Mapper.Map<DanhMucNhaThau, DanhMucNhaThauViewModels>(newDanhMucNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, DanhMucNhaThauViewModels danhMucNhaThauVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbDanhMucNhaThau = _danhMucNhaThauService.GetById(danhMucNhaThauVm.IdNhaThau);

                    dbDanhMucNhaThau.UpdateDanhMucNhaThau(danhMucNhaThauVm);
                    dbDanhMucNhaThau.UpdatedDate = DateTime.Now;
                    dbDanhMucNhaThau.UpdatedBy = User.Identity.GetUserId();
                    _danhMucNhaThauService.Update(dbDanhMucNhaThau);
                    _danhMucNhaThauService.Save();

                    var responseData = Mapper.Map<DanhMucNhaThau, DanhMucNhaThauViewModels>(dbDanhMucNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _danhMucNhaThauService.Delete(id);
                    _danhMucNhaThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DANHMUCNHATHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listDanhMucNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listDanhMucNhaThau)
                    {
                        _danhMucNhaThauService.Delete(item);
                    }

                    _danhMucNhaThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listDanhMucNhaThau.Count);
                }

                return response;
            });
        }
    }
}
