﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/kehoachluachonnhathau")]
    [Authorize]
    public class KeHoachLuaChonNhaThauController : ApiControllerBase
    {
        private IKeHoachLuaChonNhaThauService _keHoachLuaChonNhaThauService;
        private IGoiThauService _goiThauService;
        private INguonVonDuAnGoiThauService _nguonVonDuAnGoiThauService;
        private IDeleteService _deleteService;
        public KeHoachLuaChonNhaThauController(IErrorService errorService, IKeHoachLuaChonNhaThauService keHoachLuaChonNhaThauService, IDeleteService deleteService,
            IGoiThauService goiThauService, INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService) : base(errorService)
        {
            this._keHoachLuaChonNhaThauService = keHoachLuaChonNhaThauService;
            this._goiThauService = goiThauService;
            this._nguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _keHoachLuaChonNhaThauService.GetAll(idDuAn);

                var modelVm = Mapper.Map<IEnumerable<KeHoachLuaChonNhaThau>, IEnumerable<KeHoachLuaChonNhaThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _keHoachLuaChonNhaThauService.GetAllForHistory();

                var modelVm = Mapper.Map<IEnumerable<KeHoachLuaChonNhaThau>, IEnumerable<KeHoachLuaChonNhaThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int totalRowfull = 0;

                var model = _keHoachLuaChonNhaThauService.GetByFilter(idDuAn, page, pageSize, out totalRow, out totalRowfull, filter);

                var modelVm = Mapper.Map<IEnumerable<KeHoachLuaChonNhaThau>, IEnumerable<KeHoachLuaChonNhaThauViewModels>>(model);

                PaginationSet<KeHoachLuaChonNhaThauViewModels> pagedSet = new PaginationSet<KeHoachLuaChonNhaThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    TotalFull = totalRowfull,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _keHoachLuaChonNhaThauService.GetById(id);

                var responseData = Mapper.Map<KeHoachLuaChonNhaThau, KeHoachLuaChonNhaThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, KeHoachLuaChonNhaThauViewModels keHoachLuaChonNhaThautVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newKeHoachLuaChonNhaThau = new KeHoachLuaChonNhaThau();
                    newKeHoachLuaChonNhaThau.UpdateKeHoachLuaChonNhaThau(keHoachLuaChonNhaThautVm);
                    newKeHoachLuaChonNhaThau.CreatedDate = DateTime.Now;
                    newKeHoachLuaChonNhaThau.CreatedBy = User.Identity.GetUserId();
                    //try
                    //{
                    //    List<GoiThau> listGoiThaus = new List<GoiThau>();
                    //    foreach (var item in keHoachLuaChonNhaThautVm.GoiThaus)
                    //    {
                    //        List<NguonVonDuAnGoiThau> listNguonVonDuAnGoiThaus = new List<NguonVonDuAnGoiThau>();
                    //        foreach (var nguonVonDuAnGoiThau in item.NguonVonDuAnGoiThaus)
                    //        {
                    //            listNguonVonDuAnGoiThaus.Add(new NguonVonDuAnGoiThau()
                    //            {
                    //                IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn
                    //            });
                    //        }
                    //        listGoiThaus.Add(new GoiThau()
                    //        {
                    //            TenGoiThau = item.TenGoiThau,
                    //            IdDuAn = keHoachLuaChonNhaThautVm.IdDuAn,
                    //            IdKeHoachLuaChonNhaThau = item.IdKeHoachLuaChonNhaThau,
                    //            GiaGoiThau = item.GiaGoiThau,
                    //            LoaiGoiThau = item.LoaiGoiThau,
                    //            IdLinhVucDauThau = item.IdLinhVucDauThau,
                    //            IdHinhThucLuaChon = item.IdHinhThucLuaChon,
                    //            IdPhuongThucDauThau = item.IdPhuongThucDauThau,
                    //            HinhThucDauThau = item.HinhThucDauThau,
                    //            LoaiDauThau = item.LoaiDauThau,
                    //            IdLoaiHopDong = item.IdLoaiHopDong,
                    //            ThoiGianThucHienHopDong = item.ThoiGianThucHienHopDong,
                    //            GhiChu = item.GhiChu,
                    //            NguonVonDuAnGoiThaus = listNguonVonDuAnGoiThaus
                    //        });
                    //    }

                    //    newKeHoachLuaChonNhaThau.GoiThaus = listGoiThaus;
                    //}
                    //catch (Exception e)
                    //{
                    //    return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    //}

                    _keHoachLuaChonNhaThauService.Add(newKeHoachLuaChonNhaThau);
                    _keHoachLuaChonNhaThauService.Save();

                    var responseData = Mapper.Map<KeHoachLuaChonNhaThau, KeHoachLuaChonNhaThauViewModels>(newKeHoachLuaChonNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, KeHoachLuaChonNhaThauViewModels keHoachLuaChonNhaThauVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbKeHoachLuaChonNhaThau = _keHoachLuaChonNhaThauService.GetById(keHoachLuaChonNhaThauVm.IdKeHoachLuaChonNhaThau);

                    dbKeHoachLuaChonNhaThau.UpdateKeHoachLuaChonNhaThau(keHoachLuaChonNhaThauVm);
                    dbKeHoachLuaChonNhaThau.UpdatedDate = DateTime.Now;
                    dbKeHoachLuaChonNhaThau.UpdatedBy = User.Identity.GetUserId();
                    //try
                    //{
                    //    var listGoiThauOld = _goiThauService.GetListByIdKeHoach(keHoachLuaChonNhaThauVm.IdKeHoachLuaChonNhaThau);
                    //    var listDelete = listGoiThauOld.Where(x =>
                    //        !keHoachLuaChonNhaThauVm.GoiThaus.Select(y => y.IdGoiThau).Contains(x.IdGoiThau));

                    //    foreach (var item in listDelete)
                    //    {
                    //        _goiThauService.Delete(item.IdGoiThau);
                    //    }

                    //    foreach (var item in keHoachLuaChonNhaThauVm.GoiThaus)
                    //    {

                    //        if (listGoiThauOld.Where(x=>x.IdGoiThau == item.IdGoiThau).Count() == 0)
                    //        {
                    //            List<NguonVonDuAnGoiThau> listNguonVonDuAnGoiThaus = new List<NguonVonDuAnGoiThau>();
                    //            foreach (var nguonVonDuAnGoiThau in item.NguonVonDuAnGoiThaus)
                    //            {
                    //                listNguonVonDuAnGoiThaus.Add(new NguonVonDuAnGoiThau()
                    //                {
                    //                    IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn
                    //                });
                    //            }
                    //            GoiThau goiThau = new GoiThau();
                    //            goiThau.UpdateGoiThau(item);
                    //            goiThau.IdDuAn = keHoachLuaChonNhaThauVm.IdDuAn;
                    //            goiThau.IdKeHoachLuaChonNhaThau = keHoachLuaChonNhaThauVm.IdKeHoachLuaChonNhaThau;
                    //            goiThau.NguonVonDuAnGoiThaus = listNguonVonDuAnGoiThaus;
                    //            _goiThauService.Add(goiThau);
                    //        }
                    //        else
                    //        {
                    //            var listNguonVonDuAnGoiThauOld =
                    //                _nguonVonDuAnGoiThauService.GetListByIdGoiThau(item.IdGoiThau);
                    //            var listDeleteNguonVon = listNguonVonDuAnGoiThauOld.Where(x =>
                    //                !item.NguonVonDuAnGoiThaus.Select(y => y.IdNguonVonDuAnGoiThau)
                    //                    .Contains(x.IdNguonVonDuAnGoiThau));
                    //            foreach (var nv in listDeleteNguonVon)
                    //            {
                    //                _nguonVonDuAnGoiThauService.Delete(nv.IdNguonVonDuAnGoiThau);
                    //            }
                    //            foreach (var nguonVonDuAnGoiThau in item.NguonVonDuAnGoiThaus)
                    //            {
                    //                if (listNguonVonDuAnGoiThauOld.Where(x=>x.IdNguonVonDuAnGoiThau == nguonVonDuAnGoiThau.IdNguonVonDuAnGoiThau).Count()== 0)
                    //                {
                    //                    _nguonVonDuAnGoiThauService.Add(new NguonVonDuAnGoiThau()
                    //                    {
                    //                        IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn,
                    //                        IdGoiThau = item.IdGoiThau
                    //                    });
                    //                }
                    //                else
                    //                {
                    //                    var editNguonVonDuAnGoiThau = _nguonVonDuAnGoiThauService.GetById(nguonVonDuAnGoiThau.IdNguonVonDuAnGoiThau);
                    //                    editNguonVonDuAnGoiThau.IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn;
                    //                }
                    //            }
                    //            var goiThau = _goiThauService.GetById(item.IdGoiThau);
                    //            goiThau.UpdateGoiThau(item);
                    //        }
                    //    }
                    //}
                    //catch (Exception e)
                    //{
                    //    return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    //}

                    _keHoachLuaChonNhaThauService.Update(dbKeHoachLuaChonNhaThau);
                    _keHoachLuaChonNhaThauService.Save();

                    var responseData = Mapper.Map<KeHoachLuaChonNhaThau, KeHoachLuaChonNhaThauViewModels>(dbKeHoachLuaChonNhaThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var query1 = _goiThauService.GetAll().Where(x => x.IdKeHoachLuaChonNhaThau == id);
                    if (query1.Count() > 0)
                    {
                        foreach (var item in query1)
                        {
                            var query2 = _nguonVonDuAnGoiThauService.GetAll().Where(y => y.IdGoiThau == item.IdGoiThau);
                            if (query2.Count() > 0)
                            {
                                foreach (var item1 in query2)
                                {
                                    _nguonVonDuAnGoiThauService.Delete(item1.IdNguonVonDuAnGoiThau);
                                }
                            }
                            _goiThauService.Delete(item.IdGoiThau);
                        }
                    }

                    _keHoachLuaChonNhaThauService.Delete(id);
                    _keHoachLuaChonNhaThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listKeHoachLuaChonNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listKeHoachLuaChonNhaThau)
                    {
                        _deleteService.DeleteKeHoachLuaChonNhaThau(item);
                    }
                    _deleteService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listKeHoachLuaChonNhaThau.Count);
                }

                return response;
            });
        }
    }
}
