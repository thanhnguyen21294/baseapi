﻿using AutoMapper;
using Data;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Mobile;
using Model.Models.QLTD;
using Newtonsoft.Json;
using Service;
using Service.Mobile;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models;
using WebAPI.Models.Mobile;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_kehoach")]
    [Authorize]
    public class QLTD_KeHoachController : ApiControllerBase
    {
        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IQLTD_KeHoachCongViecService _QLTD_KeHoachCongViecService;
        private IQLTD_KhoKhanVuongMacService _QLTD_KhoKhanVuongMacService;
        private IQLTD_CTCV_ChuTruongDauTuService _QLTD_CTCV_ChuTruongDauTuService;
        private IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;
        private IQLTD_CTCV_ChuanBiThucHienService _QLTD_CTCV_ChuanBiThucHienService;
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;
        private IQLTD_CTCV_QuyetToanDuAnService _QLTD_CTCV_QuyetToanDuAnService;
        private IQLTD_KeHoachTienDoChungService _QLTD_KeHoachTienDoChungService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;

        private IQLTD_TienDoThucHienService _QLTD_TienDoThucHienService;
        private IQLTD_TDTH_ChuanBiDauTuService _QLTD_TDTH_ChuanBiDauTuService;
        private IQLTD_TDTH_ChuanBiThucHienService _QLTD_TDTH_ChuanBiThucHienService;
        private IQLTD_TDTH_QuyetToanDuAnService _QLTD_TDTH_QuyetToanDuAnService;
        private IQLTD_TDTH_ThucHienDuAnService _QLTD_TDTH_ThucHienDuAnService;

        private IQLTD_THCT_ChuTruongDauTuService _QLTD_THCT_ChuTruongDauTuService;
        private IQLTD_THCT_ChuanBiDauTuService _QLTD_THCT_ChuanBiDauTuService;
        private IQLTD_THCT_ThucHienDuAnService _QLTD_THCT_ThucHienDuAnService;
        private IQLTD_THCT_ChuanBiThucHienService _QLTD_THCT_ChuanBiThucHienService;
        private IQLTD_THCT_QuyetToanDuAnService _QLTD_THCT_QuyetToanDuAnService;

        private INotificationService _NotificationService;
        public QLTD_KeHoachController(IErrorService errorService,
            IQLTD_KeHoachService qLTD_KeHoachService,
            IQLTD_KeHoachCongViecService qLTD_KeHoachCongViecService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IQLTD_CTCV_ChuTruongDauTuService qLTD_CTCV_ChuTruongDauTuService,
            IQLTD_CTCV_ChuanBiDauTuService qLTD_CTCV_ChuanBiDauTuService,
            IQLTD_CTCV_ChuanBiThucHienService qLTD_CTCV_ChuanBiThucHienService,
            IQLTD_CTCV_ThucHienDuAnService qLTD_CTCV_ThucHienDuAnService,
            IQLTD_CTCV_QuyetToanDuAnService qLTD_CTCV_QuyetToanDuAnService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IQLTD_TienDoThucHienService qLTD_TienDoThucHienService,
            IQLTD_TDTH_ChuanBiDauTuService qLTD_TDTH_ChuanBiDauTuService,
            IQLTD_TDTH_ChuanBiThucHienService qLTD_TDTH_ChuanBiThucHienService,
            IQLTD_TDTH_QuyetToanDuAnService qLTD_TDTH_QuyetToanDuAnService,
            IQLTD_TDTH_ThucHienDuAnService qLTD_TDTH_ThucHienDuAnService,
            
            IQLTD_THCT_ChuTruongDauTuService qLTD_THCT_ChuTruongDauTuService,
            IQLTD_THCT_ChuanBiDauTuService qLTD_THCT_ChuanBiDauTuService,
            IQLTD_THCT_ThucHienDuAnService qLTD_THCT_ThucHienDuAnService,
            IQLTD_THCT_ChuanBiThucHienService qLTD_THCT_ChuanBiThucHienService,
            IQLTD_THCT_QuyetToanDuAnService qLTD_THCT_QuyetToanDuAnService,
        IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_KeHoachService = qLTD_KeHoachService;
            this._QLTD_KeHoachCongViecService = qLTD_KeHoachCongViecService;
            this._QLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            _QLTD_CTCV_ChuTruongDauTuService = qLTD_CTCV_ChuTruongDauTuService;
            _QLTD_CTCV_ChuanBiDauTuService = qLTD_CTCV_ChuanBiDauTuService;
            _QLTD_CTCV_ChuanBiThucHienService = qLTD_CTCV_ChuanBiThucHienService;
            _QLTD_CTCV_ThucHienDuAnService = qLTD_CTCV_ThucHienDuAnService;
            _QLTD_CTCV_QuyetToanDuAnService = qLTD_CTCV_QuyetToanDuAnService;
            _QLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            _QLTD_TienDoThucHienService = qLTD_TienDoThucHienService;
            _QLTD_TDTH_ChuanBiDauTuService = qLTD_TDTH_ChuanBiDauTuService;
            _QLTD_TDTH_ChuanBiThucHienService = qLTD_TDTH_ChuanBiThucHienService;
            _QLTD_TDTH_QuyetToanDuAnService = qLTD_TDTH_QuyetToanDuAnService;
            _QLTD_TDTH_ThucHienDuAnService = qLTD_TDTH_ThucHienDuAnService;
            _QLTD_THCT_ChuTruongDauTuService = qLTD_THCT_ChuTruongDauTuService;
            _QLTD_THCT_ChuanBiDauTuService = qLTD_THCT_ChuanBiDauTuService;
            _QLTD_THCT_ThucHienDuAnService = qLTD_THCT_ThucHienDuAnService;
            _QLTD_THCT_ChuanBiThucHienService = qLTD_THCT_ChuanBiThucHienService;
            _QLTD_THCT_QuyetToanDuAnService = qLTD_THCT_QuyetToanDuAnService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
        }

        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn, int idGiaiDoan, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                //string idNhanVien = null;
                //List<string> listUser = new List<string>();
                //if (!User.IsInRole("Admin"))
                //{
                //    var permissions = JsonConvert.DeserializeObject<List<PermissionViewModel>>(((ClaimsIdentity)User.Identity).FindFirst("permissions").Value);
                //    if (!(permissions.Exists(x =>x.CanCreate) || permissions.Exists(x => x.FunctionId == "KEHOACHLAPDAT" && x.CanCreate)))
                //    {
                //        if (permissions.Exists(x => x.CanCreate))
                //        {
                //            listUser = AppUserManager.Users.Where(x =>
                //                x. ==
                //                int.Parse(((ClaimsIdentity)User.Identity).FindFirst("NhomNhanVienId").Value)).Select(y => y.Id).ToList();
                //        }
                //        else
                //        {
                //            idNhanVien = User.Identity.GetUserId();
                //        }
                //    }
                //}
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    if (User.IsInRole("Nhân viên"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                }

                var model = _QLTD_KeHoachService.GetAll(idDuAn, idNhomDuAnTheoUser, idGiaiDoan, filter);

                if (!User.IsInRole("Admin"))
                {
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (!userRoles.Contains("Nhân viên"))
                    {
                        model = model.Where(x => x.TrangThai > -1);
                    }
                }
                var modelVm = Mapper.Map<IEnumerable<QLTD_KeHoach>, IEnumerable<QLTD_KeHoachViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int idGiaiDoan, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                var model = _QLTD_KeHoachService.GetByFilter(idDuAn, idNhomDuAnTheoUser, idGiaiDoan, page, pageSize, "", out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_KeHoach>, IEnumerable<QLTD_KeHoachViewModels>>(model);

                PaginationSet<QLTD_KeHoachViewModels> pagedSet = new PaginationSet<QLTD_KeHoachViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QLTD_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int? idNhomDuAnTheoUser = null;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                    var newKeHoach = new QLTD_KeHoach();
                    newKeHoach.IdKeHoachDC = vmKeHoach.IdKeHoachDC;
                    newKeHoach.IdDuAn = vmKeHoach.IdDuAn;
                    newKeHoach.IdNhomDuAnTheoUser = idNhomDuAnTheoUser;
                    newKeHoach.IdGiaiDoan = vmKeHoach.IdGiaiDoan;
                    newKeHoach.NoiDung = vmKeHoach.NoiDung;
                    newKeHoach.GhiChu = vmKeHoach.GhiChu;
                    newKeHoach.TrangThai = -1;
                    var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (roleTemp.Contains("Trưởng phòng"))
                    {
                        newKeHoach.TrangThai = 1;
                        newKeHoach.TinhTrang = 1;
                        newKeHoach.NgayTinhTrang = DateTime.Now;
                    }
                    if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                        newKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newKeHoach.CreatedDate = DateTime.Now;
                    newKeHoach.CreatedBy = User.Identity.GetUserId();
                    newKeHoach.Status = true;

                    List<QLTD_KeHoachCongViec> lstKeHoachCongViec = new List<QLTD_KeHoachCongViec>();
                    foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                    {
                        QLTD_KeHoachCongViec newDt = new QLTD_KeHoachCongViec();
                        newDt.IdKeHoach = item.IdKeHoach;
                        newDt.IdCongViec = item.IdCongViec;
                        newDt.TuNgay = null;
                        if (item.TuNgay != "" && item.TuNgay != null)
                            newDt.TuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newDt.DenNgay = null;
                        if (item.DenNgay != "" && item.DenNgay != null)
                            newDt.DenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newDt.Status = true;
                        lstKeHoachCongViec.Add(newDt);
                    }
                    newKeHoach.QLTD_KeHoachCongViecs = lstKeHoachCongViec;

                    _QLTD_KeHoachService.Add(newKeHoach);
                    _QLTD_KeHoachService.Save();

                    if (vmKeHoach.IdKeHoachDC != null)
                    {
                        int idKHDC = Convert.ToInt32(vmKeHoach.IdKeHoachDC);

                        //QLTD_KeHoachTienDoChung newTDTHChung = _QLTD_KeHoachTienDoChungService.GetByIdKeHoach(idKHDC);
                        //newTDTHChung.IdKeHoach = newKeHoach.IdKeHoach;
                        //_QLTD_KeHoachTienDoChungService.Add(newTDTHChung);
                        //_QLTD_KeHoachTienDoChungService.Save();

                        IEnumerable<QLTD_KhoKhanVuongMac> lstKK = _QLTD_KhoKhanVuongMacService.GetByIdKeHoach(idKHDC).ToList();
                        foreach(QLTD_KhoKhanVuongMac kk in lstKK)
                        {
                            kk.IdKeHoach = newKeHoach.IdKeHoach;
                            _QLTD_KhoKhanVuongMacService.Add(kk);
                        }
                        _QLTD_KhoKhanVuongMacService.Save();


                        foreach (var item in newKeHoach.QLTD_KeHoachCongViecs)
                        {
                            switch (Convert.ToInt32(newKeHoach.IdGiaiDoan))
                            {
                                case 2:
                                    {
                                        var lstCTCV = _QLTD_CTCV_ChuTruongDauTuService.GetAllByIDKH(idKHDC).ToList();
                                        var oldItem = lstCTCV.Where(x => x.IdCongViec == item.IdCongViec).FirstOrDefault();
                                        var oldChiTietCV = oldItem.IdChiTietCongViec;
                                        int idTemp = item.IdKeHoachCongViec;
                                        QLTD_CTCV_ChuTruongDauTu newCTCV = oldItem;
                                        newCTCV.IdKeHoach = Convert.ToInt32(newKeHoach.IdKeHoach);
                                        newCTCV.IdKeHoachCongViec = idTemp;
                                        newCTCV.CreatedDate = DateTime.Now;
                                        newCTCV.CreatedBy = User.Identity.GetUserId();
                                        newCTCV.HoanThanh = 0;
                                        newCTCV.NgayHoanThanh = null;
                                        newCTCV.Status = true;
                                        _QLTD_CTCV_ChuTruongDauTuService.Add(newCTCV);
                                        _QLTD_CTCV_ChuTruongDauTuService.Save();

                                        var lstTDTH = _QLTD_TienDoThucHienService.GetByIDChiTietCongViec(oldChiTietCV).ToList();
                                        foreach(QLTD_TDTH_ChuTruongDauTu tdth in lstTDTH)
                                        {
                                            QLTD_TDTH_ChuTruongDauTu newTDTH = tdth;
                                            newTDTH.IdChiTietCongViec = newCTCV.IdChiTietCongViec;
                                            newTDTH.NgayHoanThanh = null;
                                            _QLTD_TienDoThucHienService.Add(newTDTH);

                                            var lstTHCT = _QLTD_THCT_ChuTruongDauTuService.GetByIDTienDoThucHien(tdth.IdTienDoThucHien).ToList();
                                            foreach (QLTD_THCT_ChuTruongDauTu thct in lstTHCT)
                                            {
                                                QLTD_THCT_ChuTruongDauTu newTHCT = thct;
                                                newTHCT.IdTienDoThucHien = newTDTH.IdTienDoThucHien;
                                                _QLTD_THCT_ChuTruongDauTuService.Add(newTHCT);
                                            }
                                            _QLTD_THCT_ChuTruongDauTuService.Save();
                                        }
                                        _QLTD_TienDoThucHienService.Save();
                                        break;
                                    }
                                case 3:
                                    {
                                        var lstCTCV = _QLTD_CTCV_ChuanBiDauTuService.GetAllByIDKH(idKHDC).ToList();
                                        var oldItem = lstCTCV.Where(x => x.IdCongViec == item.IdCongViec).FirstOrDefault();
                                        var oldChiTietCV = oldItem.IdChiTietCongViec;
                                        int idTemp = item.IdKeHoachCongViec;
                                        var newCTCV = oldItem;
                                        newCTCV.IdKeHoach = Convert.ToInt32(newKeHoach.IdKeHoach);
                                        newCTCV.IdKeHoachCongViec = idTemp;
                                        newCTCV.CreatedDate = DateTime.Now;
                                        newCTCV.CreatedBy = User.Identity.GetUserId();
                                        newCTCV.HoanThanh = 0;
                                        newCTCV.NgayHoanThanh = null;
                                        newCTCV.Status = true;
                                        _QLTD_CTCV_ChuanBiDauTuService.Add(newCTCV);
                                        _QLTD_CTCV_ChuanBiDauTuService.Save();

                                        var lstTDTH = _QLTD_TDTH_ChuanBiDauTuService.GetByIDChiTietCongViec(oldChiTietCV).ToList();
                                        foreach (QLTD_TDTH_ChuanBiDauTu tdth in lstTDTH)
                                        {
                                            QLTD_TDTH_ChuanBiDauTu newTDTH = tdth;
                                            newTDTH.IdChiTietCongViec = newCTCV.IdChiTietCongViec;
                                            newTDTH.NgayHoanThanh = null;
                                            _QLTD_TDTH_ChuanBiDauTuService.Add(newTDTH);

                                            var lstTHCT = _QLTD_THCT_ChuanBiDauTuService.GetByIDTienDoThucHien(tdth.IdTienDoThucHien).ToList();
                                            foreach (QLTD_THCT_ChuanBiDauTu thct in lstTHCT)
                                            {
                                                QLTD_THCT_ChuanBiDauTu newTHCT = thct;
                                                newTHCT.IdTienDoThucHien = newTDTH.IdTienDoThucHien;
                                                _QLTD_THCT_ChuanBiDauTuService.Add(newTHCT);
                                            }
                                            _QLTD_THCT_ChuanBiDauTuService.Save();
                                        }
                                        _QLTD_TDTH_ChuanBiDauTuService.Save();
                                        break;
                                    }
                                case 4:
                                    {
                                        var lstCTCV = _QLTD_CTCV_ChuanBiThucHienService.GetAllByIDKH(idKHDC).ToList();
                                        var oldItem = lstCTCV.Where(x => x.IdCongViec == item.IdCongViec).FirstOrDefault();
                                        var oldChiTietCV = oldItem.IdChiTietCongViec;
                                        int idTemp = item.IdKeHoachCongViec;
                                        var newCTCV = oldItem;
                                        newCTCV.IdKeHoach = Convert.ToInt32(newKeHoach.IdKeHoach);
                                        newCTCV.IdKeHoachCongViec = idTemp;
                                        newCTCV.CreatedDate = DateTime.Now;
                                        newCTCV.CreatedBy = User.Identity.GetUserId();
                                        newCTCV.Status = true;
                                        newCTCV.HoanThanh = 0;
                                        newCTCV.NgayHoanThanh = null;
                                        _QLTD_CTCV_ChuanBiThucHienService.Add(newCTCV);
                                        _QLTD_CTCV_ChuanBiThucHienService.Save();

                                        var lstTDTH = _QLTD_TDTH_ChuanBiThucHienService.GetByIDChiTietCongViec(oldChiTietCV).ToList();
                                        foreach (QLTD_TDTH_ChuanBiThucHien tdth in lstTDTH)
                                        {
                                            QLTD_TDTH_ChuanBiThucHien newTDTH = tdth;
                                            newTDTH.IdChiTietCongViec = newCTCV.IdChiTietCongViec;
                                            newTDTH.NgayHoanThanh = null;
                                            _QLTD_TDTH_ChuanBiThucHienService.Add(newTDTH);

                                            var lstTHCT = _QLTD_THCT_ChuanBiThucHienService.GetByIDTienDoThucHien(tdth.IdTienDoThucHien).ToList();
                                            foreach (QLTD_THCT_ChuanBiThucHien thct in lstTHCT)
                                            {
                                                QLTD_THCT_ChuanBiThucHien newTHCT = thct;
                                                newTHCT.IdTienDoThucHien = newTDTH.IdTienDoThucHien;
                                                _QLTD_THCT_ChuanBiThucHienService.Add(newTHCT);
                                            }
                                            _QLTD_THCT_ChuanBiThucHienService.Save();
                                        }
                                        _QLTD_TDTH_ChuanBiThucHienService.Save();
                                        break;
                                    }
                                case 5:
                                    {
                                        var lstCTCV = _QLTD_CTCV_ThucHienDuAnService.GetAllByIDKH(idKHDC).ToList();
                                        var oldItem = lstCTCV.Where(x => x.IdCongViec == item.IdCongViec).FirstOrDefault();
                                        var oldChiTietCV = oldItem.IdChiTietCongViec;
                                        int idTemp = item.IdKeHoachCongViec;
                                        var newCTCV = oldItem;
                                        newCTCV.IdKeHoach = Convert.ToInt32(newKeHoach.IdKeHoach);
                                        newCTCV.IdKeHoachCongViec = idTemp;
                                        newCTCV.CreatedDate = DateTime.Now;
                                        newCTCV.CreatedBy = User.Identity.GetUserId();
                                        newCTCV.Status = true;
                                        newCTCV.HoanThanh = 0;
                                        newCTCV.NgayHoanThanh = null;
                                        _QLTD_CTCV_ThucHienDuAnService.Add(newCTCV);
                                        _QLTD_CTCV_ThucHienDuAnService.Save();

                                        var lstTDTH = _QLTD_TDTH_ThucHienDuAnService.GetByIDChiTietCongViec(oldChiTietCV).ToList();
                                        foreach (QLTD_TDTH_ThucHienDuAn tdth in lstTDTH)
                                        {
                                            QLTD_TDTH_ThucHienDuAn newTDTH = tdth;
                                            newTDTH.IdChiTietCongViec = newCTCV.IdChiTietCongViec;
                                            newTDTH.NgayHoanThanh = null;
                                            _QLTD_TDTH_ThucHienDuAnService.Add(newTDTH);

                                            var lstTHCT = _QLTD_THCT_ThucHienDuAnService.GetByIDTienDoThucHien(tdth.IdTienDoThucHien).ToList();
                                            foreach (QLTD_THCT_ThucHienDuAn thct in lstTHCT)
                                            {
                                                QLTD_THCT_ThucHienDuAn newTHCT = thct;
                                                newTHCT.IdTienDoThucHien = newTDTH.IdTienDoThucHien;
                                                _QLTD_THCT_ThucHienDuAnService.Add(newTHCT);
                                            }
                                            _QLTD_THCT_ThucHienDuAnService.Save();
                                        }
                                        _QLTD_TDTH_ThucHienDuAnService.Save();
                                        break;
                                    }
                                case 6:
                                    {
                                        var lstCTCV = _QLTD_CTCV_QuyetToanDuAnService.GetAllByIDKH(idKHDC).ToList();
                                        var oldItem = lstCTCV.Where(x => x.IdCongViec == item.IdCongViec).FirstOrDefault();
                                        var oldChiTietCV = oldItem.IdChiTietCongViec;
                                        int idTemp = item.IdKeHoachCongViec;
                                        var newCTCV = oldItem;
                                        newCTCV.IdKeHoach = Convert.ToInt32(newKeHoach.IdKeHoach);
                                        newCTCV.IdKeHoachCongViec = idTemp;
                                        newCTCV.CreatedDate = DateTime.Now;
                                        newCTCV.CreatedBy = User.Identity.GetUserId();
                                        newCTCV.Status = true;
                                        newCTCV.HoanThanh = 0;
                                        newCTCV.NgayHoanThanh = null;
                                        _QLTD_CTCV_QuyetToanDuAnService.Add(newCTCV);
                                        _QLTD_CTCV_QuyetToanDuAnService.Save();

                                        var lstTDTH = _QLTD_TDTH_QuyetToanDuAnService.GetByIDChiTietCongViec(oldChiTietCV).ToList();
                                        foreach (QLTD_TDTH_QuyetToanDuAn tdth in lstTDTH)
                                        {
                                            QLTD_TDTH_QuyetToanDuAn newTDTH = tdth;
                                            newTDTH.IdChiTietCongViec = newCTCV.IdChiTietCongViec;
                                            newTDTH.NgayHoanThanh = null;
                                            _QLTD_TDTH_QuyetToanDuAnService.Add(newTDTH);

                                            var lstTHCT = _QLTD_THCT_QuyetToanDuAnService.GetByIDTienDoThucHien(tdth.IdTienDoThucHien).ToList();
                                            foreach (QLTD_THCT_QuyetToanDuAn thct in lstTHCT)
                                            {
                                                QLTD_THCT_QuyetToanDuAn newTHCT = thct;
                                                newTHCT.IdTienDoThucHien = newTDTH.IdTienDoThucHien;
                                                _QLTD_THCT_QuyetToanDuAnService.Add(newTHCT);
                                            }
                                            _QLTD_THCT_QuyetToanDuAnService.Save();
                                        }
                                        _QLTD_TDTH_QuyetToanDuAnService.Save();
                                        break;
                                    }
                            }
                        }
                    }

                    var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _QLTD_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _QLTD_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _QLTD_KeHoachCongViecService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    var updateKeHoach = _QLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);
                    updateKeHoach.IdDuAn = vmKeHoach.IdDuAn;
                    updateKeHoach.IdKeHoachDC = vmKeHoach.IdKeHoachDC;
                    updateKeHoach.IdGiaiDoan = vmKeHoach.IdGiaiDoan;
                    updateKeHoach.NoiDung = vmKeHoach.NoiDung;
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                        updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KeHoachService.Update(updateKeHoach);
                    _QLTD_KeHoachService.Save();

                    var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("updatetrangthai")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, QLTD_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _QLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);
                    updateKeHoach.TrangThai = vmKeHoach.TrangThai;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KeHoachService.Update(updateKeHoach);
                    _QLTD_KeHoachService.Save();

                    var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        _QLTD_KeHoachService.Delete(id);
                        _QLTD_KeHoachService.Save();
                    }
                    catch(Exception ex)
                    {

                    }

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_KeHoachService.GetById(id);
                var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("pheduyet")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage PheDuyet(HttpRequestMessage request, QLTD_KeHoachViewModels vmKeHoach)
        {
            string title = "Dự án " + _DuAnService.GetById(vmKeHoach.IdDuAn).TenDuAn;
            string body = "Kế hoạch " + vmKeHoach.NoiDung;
            List<string> listUserId = new List<string>();
            var userIdCreatedDA = vmKeHoach.CreatedBy;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _QLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);
                    updateKeHoach.PheDuyet = true;
                    int trangthaiUpdate = -1;
                    int tinhtrangUpdate = 0;
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    switch (vmKeHoach.TrangThai)
                    {
                        case -1:// cán bộ đang soạn
                            {
                                trangthaiUpdate = 1;
                                tinhtrangUpdate = 1;
                                body += " đang chờ phê duyệt";
                                listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                break;
                            }
                        case 0:// Kế hoạch không được phê duyệt
                            {
                                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                if (roleTemp.Contains("Trưởng phòng"))
                                {
                                    trangthaiUpdate = 2;
                                    tinhtrangUpdate = 2;
                                    listUserId.Add(GetPhoGiamDocIdByDuAnId(vmKeHoach.IdDuAn));
                                }
                                else
                                {
                                    listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                    trangthaiUpdate = 1;
                                    tinhtrangUpdate = 1;
                                }
                                body += " đang chờ phê duyệt";
                                break;
                            }
                        case 1:// CB đã gửi lên cho TP
                            {
                                if (idNhomDuAnTheoUser == 13)
                                {
                                    trangthaiUpdate = 3;
                                    tinhtrangUpdate = 3;
                                }
                                else
                                {
                                    trangthaiUpdate = 2;
                                    tinhtrangUpdate = 2;
                                }
                                body += " đã được phê duyệt bởi trưởng phòng";
                                var phoGiamDocId = GetPhoGiamDocIdByDuAnId(vmKeHoach.IdDuAn);
                                listUserId.Add(phoGiamDocId);
                                break;
                            }
                        case 2:// Trưởng phòng đã chấp thuận
                            {
                                trangthaiUpdate = 3;
                                tinhtrangUpdate = 3;
                                body += " đã được phê duyệt bởi phó giám đốc";
                                var giamDocId = GetGiamDocIdByDuAnId(vmKeHoach.IdDuAn);
                                listUserId.Add(giamDocId);
                                break;
                            }
                        case 3:// Phó giám đốc đã chấp thuận
                            {
                                trangthaiUpdate = 4;
                                tinhtrangUpdate = 4;
                                body += vmKeHoach.NoiDung + " đã được phê duyệt bởi giám đốc";
                                listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                listUserId.Add(GetPhoGiamDocIdByDuAnId(vmKeHoach.IdDuAn));
                                break;
                            }
                    }

                    //try
                    //{
                    if (vmKeHoach.IdKeHoachDC == null)
                    {
                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _QLTD_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _QLTD_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _QLTD_KeHoachCongViecService.Save();
                    }
                    //}
                    //catch (Exception e)
                    //{
                    //    return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    //}
                    if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                        updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    updateKeHoach.TrangThai = trangthaiUpdate;
                    updateKeHoach.TinhTrang = tinhtrangUpdate;
                    updateKeHoach.NgayTinhTrang = DateTime.Now;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KeHoachService.Update(updateKeHoach);
                    _QLTD_KeHoachService.Save();

                    if (vmKeHoach.IdKeHoachDC == null)
                    {
                        if (trangthaiUpdate == 4)
                        {
                            QLTD_KeHoachTienDoChung newTDTHChung = new QLTD_KeHoachTienDoChung();
                            newTDTHChung.IdKeHoach = vmKeHoach.IdKeHoach;
                            _QLTD_KeHoachTienDoChungService.Add(newTDTHChung);
                            _QLTD_KeHoachTienDoChungService.Save();
                            switch (Convert.ToInt32(vmKeHoach.IdGiaiDoan))
                            {
                                case 2:
                                    {
                                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuTruongDauTu();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuTruongDauTuService.Add(newCTCV);
                                        }
                                        _QLTD_CTCV_ChuTruongDauTuService.Save();
                                        break;
                                    }
                                case 3:
                                    {
                                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuanBiDauTu();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuanBiDauTuService.Add(newCTCV);
                                        }
                                        _QLTD_CTCV_ChuanBiDauTuService.Save();
                                        break;
                                    }
                                case 4:
                                    {
                                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuanBiThucHien();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuanBiThucHienService.Add(newCTCV);
                                        }
                                        _QLTD_CTCV_ChuanBiThucHienService.Save();
                                        break;
                                    }
                                case 5:
                                    {
                                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                                        {
                                            var newCTCV = new QLTD_CTCV_ThucHienDuAn();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ThucHienDuAnService.Add(newCTCV);
                                        }
                                        _QLTD_CTCV_ThucHienDuAnService.Save();
                                        break;
                                    }
                                case 6:
                                    {
                                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                                        {
                                            var newCTCV = new QLTD_CTCV_QuyetToanDuAn();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_QuyetToanDuAnService.Add(newCTCV);
                                        }
                                        _QLTD_CTCV_QuyetToanDuAnService.Save();
                                        break;
                                    }
                            }
                        }
                    }
                    var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("khongpheduyet")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage KhongPheDuyet(HttpRequestMessage request, QLTD_KeHoachViewModels vmKeHoach)
        {
            string title = "Dự án " + _DuAnService.GetById(vmKeHoach.IdDuAn).TenDuAn;
            string body = "Kế hoạch " + vmKeHoach.NoiDung;
            List<string> listUserId = new List<string>();
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        foreach (var item in vmKeHoach.QLTD_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _QLTD_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _QLTD_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _QLTD_KeHoachCongViecService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    var updateKeHoach = _QLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);

                    if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                        updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    updateKeHoach.PheDuyet = false;
                    updateKeHoach.TrangThai = vmKeHoach.TrangThai;
                    updateKeHoach.NgayTinhTrang = DateTime.Now;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KeHoachService.Update(updateKeHoach);
                    _QLTD_KeHoachService.Save();

                    var responseData = Mapper.Map<QLTD_KeHoach, QLTD_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                string nameRole;
                if (!roleTemp.Contains("Trưởng phòng"))
                {
                    listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                }
                if (vmKeHoach.CreatedBy != User.Identity.GetUserId())
                {
                    listUserId.Add(vmKeHoach.CreatedBy);
                }
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    nameRole = "Trưởng phòng";
                }
                else if (roleTemp.Contains("Phó giám đốc"))
                {
                    nameRole = "Phó giám đốc";
                }
                else
                {
                    nameRole = "Giám đốc";
                }
                body += " đã không được phê duyệt bởi " + nameRole;
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetListNhanVienId()
        {
            List<string> listUserId = new List<string>();
            var users = AppUserManager.Users;
            foreach (AppUser user in users)
            {
                var roleTemp = AppUserManager.GetRoles(user.Id);
                if (roleTemp.Contains("Nhân viên"))
                {
                    listUserId.Add(user.Id);
                }
            }
            return listUserId;
        }

        private List<string> GetListTruongPhongId()
        {
            List<string> listUserId = new List<string>();
            var users = AppUserManager.Users;
            foreach (AppUser user in users)
            {
                var roleTemp = AppUserManager.GetRoles(user.Id);
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    listUserId.Add(user.Id);
                }
            }
            return listUserId;
        }

        private List<string> GetListPhoGDOrGDId()
        {
            List<string> listUserId = new List<string>();
            var users = AppUserManager.Users;
            foreach (AppUser user in users)
            {
                var roleTemp = AppUserManager.GetRoles(user.Id);
                if (roleTemp.Contains("Giám đốc") || roleTemp.Contains("Phó giám đốc"))
                {
                    listUserId.Add(user.Id);
                }
            }
            return listUserId;
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
