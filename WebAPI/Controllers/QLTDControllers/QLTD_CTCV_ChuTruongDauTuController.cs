﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.Mobile;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_ctcv_chutruongdautu")]
    [Authorize]
    public class QLTD_CTCV_ChuTruongDauTuController : ApiControllerBase
    {
        private IQLTD_CTCV_ChuTruongDauTuService _QLTD_CTCV_ChuTruongDauTuService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        public QLTD_CTCV_ChuTruongDauTuController(
            IErrorService errorService,
            IQLTD_CTCV_ChuTruongDauTuService qLTD_CTCV_ChuTruongDauTuService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CTCV_ChuTruongDauTuService = qLTD_CTCV_ChuTruongDauTuService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuTruongDauTuService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuTruongDauTu>, IEnumerable<QLTD_CTCV_ChuTruongDauTuViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuTruongDauTuService.GetAllByIDKH(idKHCV, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuTruongDauTu>, IEnumerable<QLTD_CTCV_ChuTruongDauTuViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<QLTD_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new QLTD_CTCV_ChuTruongDauTu();
                        newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                        newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _QLTD_CTCV_ChuTruongDauTuService.Add(newCTCV);
                        _QLTD_CTCV_ChuTruongDauTuService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_ChuTruongDauTu, QLTD_CTCV_ChuTruongDauTuViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuTruongDauTuService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuTruongDauTu, QLTD_CTCV_ChuTruongDauTuViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuTruongDauTuService.GetByID(id);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuTruongDauTu, QLTD_CTCV_ChuTruongDauTuViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_CTCV_ChuTruongDauTuViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_CTCV_ChuTruongDauTuService.GetByID(vmData.IdChiTietCongViec);
                    updateData.CTDD_CoSanDiaDiem = vmData.CTDD_CoSanDiaDiem;
                    updateData.CTDD_SoVB = vmData.CTDD_SoVB;
                    updateData.CTDD_NgayPD = null;
                    if (vmData.CTDD_NgayPD != "" && vmData.CTDD_NgayPD != null)
                    {
                        updateData.CTDD_NgayPD = DateTime.ParseExact(vmData.CTDD_NgayPD, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.LCTDT_TienDoThucHien = vmData.LCTDT_TienDoThucHien;
                    updateData.LCTDT_KhoKhanVuongMac = vmData.LCTDT_KhoKhanVuongMac;
                    updateData.LCTDT_KienNghiDeXuat = vmData.LCTDT_KienNghiDeXuat;
                    updateData.LCTDT_NgayHT = null;
                    if (vmData.LCTDT_NgayHT != "" && vmData.LCTDT_NgayHT != null)
                    {
                        updateData.LCTDT_NgayHT = DateTime.ParseExact(vmData.LCTDT_NgayHT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.THSPDDA_NgayTrinhThucTe = null;
                    if (vmData.THSPDDA_NgayTrinhThucTe != "" && vmData.THSPDDA_NgayTrinhThucTe != null)
                    {
                        updateData.THSPDDA_NgayTrinhThucTe = DateTime.ParseExact(vmData.THSPDDA_NgayTrinhThucTe, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PQCTDT_SoQD = vmData.PQCTDT_SoQD;
                    updateData.PQCTDT_NgayLaySo = null;
                    if (vmData.PQCTDT_NgayLaySo != "" && vmData.PQCTDT_NgayLaySo != null)
                    {
                        updateData.PQCTDT_NgayLaySo = DateTime.ParseExact(vmData.PQCTDT_NgayLaySo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PQCTDT_GiaTri = vmData.PQCTDT_GiaTri;
                    updateData.PQCTDT_NgayPheDuyetTT = null;
                    if (vmData.PQCTDT_NgayPheDuyetTT != "" && vmData.PQCTDT_NgayPheDuyetTT != null)
                    {
                        updateData.PQCTDT_NgayPheDuyetTT = DateTime.ParseExact(vmData.PQCTDT_NgayPheDuyetTT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PQCTDT_CoQuanPD = vmData.PQCTDT_CoQuanPD;

                    updateData.BTGNV_SoToTrinh = vmData.BTGNV_SoToTrinh;
                    updateData.BTGNV_SoBaoCao = vmData.BTGNV_SoBaoCao;
                    updateData.BTGNV_NgayTrinh = null;
                    if (vmData.BTGNV_NgayTrinh != "" && vmData.BTGNV_NgayTrinh != null)
                    {
                        updateData.BTGNV_NgayTrinh = DateTime.ParseExact(vmData.BTGNV_NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }


                    updateData.HGNV_SoToTrinh = vmData.HGNV_SoToTrinh;
                    updateData.HGNV_SoBaoCao = vmData.HGNV_SoBaoCao;
                    updateData.HGNV_NgayTrinh = null;
                    if (vmData.HGNV_NgayTrinh != "" && vmData.HGNV_NgayTrinh != null)
                    {
                        updateData.HGNV_NgayTrinh = DateTime.ParseExact(vmData.HGNV_NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.HGNV_SoVBYKien = vmData.HGNV_SoVBYKien;

                    updateData.HTPPDGNV_SoVanBan = vmData.HTPPDGNV_SoVanBan;
                    updateData.HTPPDGNV_NgayNhan = null;
                    if (vmData.HTPPDGNV_NgayNhan != "" && vmData.HTPPDGNV_NgayNhan != null)
                    {
                        updateData.HTPPDGNV_NgayNhan = DateTime.ParseExact(vmData.HTPPDGNV_NgayNhan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }


                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_CTCV_ChuTruongDauTuService.Update(updateData);
                    _QLTD_CTCV_ChuTruongDauTuService.Save();

                    var responseData = Mapper.Map<QLTD_CTCV_ChuTruongDauTu, QLTD_CTCV_ChuTruongDauTuViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoach);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _QLTD_CTCV_ChuTruongDauTuService.GetByID(vmData.IdChiTietCongViec).QLTD_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
