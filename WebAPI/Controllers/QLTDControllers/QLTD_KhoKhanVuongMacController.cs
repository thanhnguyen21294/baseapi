﻿using AutoMapper;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.Mobile;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Controllers.BCControllers;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_khokhanvuongmac")]
    [Authorize]
    public class QLTD_KhoKhanVuongMacController : ApiControllerBase
    {
        private IQLTD_KhoKhanVuongMacService _QLTD_KhoKhanVuongMacService;
        private IAppRoleRepository _AppRoleRepository;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        private IDeleteService _deleteService;
        public QLTD_KhoKhanVuongMacController(
            IErrorService errorService,
            IQLTD_KeHoachService qLTD_KeHoachService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IAppRoleRepository AppRoleRepository,
            IDeleteService deleteService) : base(errorService)
        {
            _qLTD_KeHoachService = qLTD_KeHoachService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _AppRoleRepository = AppRoleRepository;
            _DuAnService = duAnService;
            this._QLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            this._deleteService = deleteService;
        }
        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QLTD_KhoKhanVuongMacViewModels vmKeHoach)
        {
            var keHoach = _qLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);
            List<string> listUserId = new List<string>();
            string title = "";
            string body = "";
            if (keHoach != null)
            {
                var idDuAn = keHoach.IdDuAn;
                title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                body = "Kế hoạch " + keHoach.NoiDung + " ,Khó khăn: " + vmKeHoach.NguyenNhanLyDo;
                listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                body = "Kế hoạch " + keHoach.NoiDung + " có khó khăn là " + vmKeHoach.NguyenNhanLyDo + " Giải pháp: " + vmKeHoach.GiaiPhapTrienKhai;
                listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
            }
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newKeHoach = new QLTD_KhoKhanVuongMac();
                    newKeHoach.IdKeHoach = vmKeHoach.IdKeHoach;
                    newKeHoach.NguyenNhanLyDo = vmKeHoach.NguyenNhanLyDo;
                    newKeHoach.GiaiPhapTrienKhai = vmKeHoach.GiaiPhapTrienKhai;
                    newKeHoach.DaGiaiQuyet = vmKeHoach.DaGiaiQuyet;
                    newKeHoach.CreatedDate = DateTime.Now;
                    newKeHoach.CreatedBy = User.Identity.GetUserId();
                    newKeHoach.Status = true;
                    _QLTD_KhoKhanVuongMacService.Add(newKeHoach);
                    _QLTD_KhoKhanVuongMacService.Save();

                    var responseData = Mapper.Map<QLTD_KhoKhanVuongMac, QLTD_KhoKhanVuongMacViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId, title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_KhoKhanVuongMacViewModels vmKeHoach)
        {
            List<string> listUserId = new List<string>();
            string title = "";
            string body = "";
            if (vmKeHoach.DaGiaiQuyet)
            {
                var keHoach = _qLTD_KeHoachService.GetById(vmKeHoach.IdKeHoach);
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Khó khăn: " + vmKeHoach.NguyenNhanLyDo + " đã được xử lý";
                    listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                    listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                }
            }
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _QLTD_KhoKhanVuongMacService.GetById(vmKeHoach.IdKhoKhanVuongMac);
                    updateKeHoach.NguyenNhanLyDo = vmKeHoach.NguyenNhanLyDo;
                    updateKeHoach.GiaiPhapTrienKhai = vmKeHoach.GiaiPhapTrienKhai;
                    updateKeHoach.DaGiaiQuyet = vmKeHoach.DaGiaiQuyet;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KhoKhanVuongMacService.Update(updateKeHoach);
                    _QLTD_KhoKhanVuongMacService.Save();

                    var responseData = Mapper.Map<QLTD_KhoKhanVuongMac, QLTD_KhoKhanVuongMacViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId, title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _QLTD_KhoKhanVuongMacService.Delete(id);
                    _QLTD_KhoKhanVuongMacService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_KhoKhanVuongMacService.GetById(id);
                var responseData = Mapper.Map<QLTD_KhoKhanVuongMac, QLTD_KhoKhanVuongMacViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getbyidkehoach")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_KhoKhanVuongMacService.GetByIdKeHoach(idKH);
                var responseData = Mapper.Map<IEnumerable<QLTD_KhoKhanVuongMac>, IEnumerable<QLTD_KhoKhanVuongMacViewModels>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getkhokhanvuongmacdieuhanh")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetKhoKhanVuongMacDieuHanh(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var model = _QLTD_KhoKhanVuongMacService.GetKhoKhanVuongMacDieuHanh(idUser, idNhomDuAnTheoUser);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getcountkhokhanvuongmacdieuhanh")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCountKhoKhanVuongMacDieuHanh(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var model = _QLTD_KhoKhanVuongMacService.GetCountKhoKhanVuongMacDieuHanh(idUser, idNhomDuAnTheoUser);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getpagekhokhanvuongmacdieuhanh")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetPageKhoKhanVuongMacDieuHanh(HttpRequestMessage request, int idKH, int Numberpage, int page, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                dicUserNhanVien.Clear();
                var users = AppUserManager.Users;
                foreach (AppUser user in users)
                {
                    var roleTemp = AppUserManager.GetRoles(user.Id);
                    if (roleTemp.Contains("Nhân viên"))
                    {
                        dicUserNhanVien.Add(user.Id, user.FullName);
                    }
                }

                var model = _QLTD_KhoKhanVuongMacService.GetPageKhoKhanVuongMacDieuHanh(idUser, idNhomDuAnTheoUser, Numberpage, page, filter, dicUserNhanVien);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }
        [Route("getPageTienDoChung")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage getPageTienDoChung(HttpRequestMessage request, int idKH, int Numberpage, int page, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                dicUserNhanVien.Clear();
                dicUserNhanVien = _AppRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}

                var model = _QLTD_KhoKhanVuongMacService.GetGroupAllKeHoachByDuAn(idUser, Numberpage, page, filter, dicUserNhanVien);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getCountKeHoachByDuAn")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCountKeHoachByDuAn(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var model = _QLTD_KhoKhanVuongMacService.GetCountKeHoachByDuAn(idUser);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }
        [Route("bckhokhandieuhanh")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BCKhoKhanDieuHanh(HttpRequestMessage request, int idKH)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = BaoCaoGPMBTongHopKetQua();

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
        }

        private string BaoCaoGPMBTongHopKetQua()
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BC_KhoKhanVuongMac.xlsx");
            string documentName = string.Format("KhoKhanDieuHanh-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);
            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        string idUser = "";
                        int idNhomDuAnTheoUser = 0;
                        if (!User.IsInRole("Admin"))
                        {
                            idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                            idUser = User.Identity.GetUserId();
                        }

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }

                        var lstData = _QLTD_KhoKhanVuongMacService.GetBCKhoKhanDieuHanh(idUser, idNhomDuAnTheoUser, dicUserNhanVien);

                        int rowIndex = 4;
                        int count = 0;

                        int rowEnd = XuatBaoCao(sheet, lstData, count, rowIndex);

                        sheet.Cells[2, 1].Value = "Ngày " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                        sheet.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Column(2).Style.WrapText = true;
                        sheet.Column(3).Style.WrapText = true;
                        sheet.Column(4).Style.WrapText = true;
                        sheet.Column(5).Style.WrapText = true;
                        if (rowEnd > 4)
                        {
                            using (ExcelRange rng = sheet.Cells["B5:E" + rowEnd])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                            }
                            using (ExcelRange rng = sheet.Cells["A5:A" + rowEnd])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                            }
                        }

                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<BC_KhoKhanVuongMac> lstData, int count, int rowIndex)
        {
            foreach (var p in lstData)
            {
                rowIndex++;
                count++;
                sheet.Cells[rowIndex, 1].Value = BaoCaoDungChung.ToRoman(count);
                var idPhong = p.IDPhong;
                string szTenPhong = "";
                if (idPhong != null)
                {
                    if (idPhong == 10)
                    {
                        szTenPhong = "Phòng ĐHDA";
                    }
                    else if (idPhong == 11)
                    {
                        szTenPhong = "Phòng QLKTHT&GPMB";
                    }
                    else if (idPhong == 12)
                    {
                        szTenPhong = "Phòng KHTH";
                    }
                    else
                    {
                        szTenPhong = "Phòng Kế toán - Tài vụ";
                    }
                }

                sheet.Cells[rowIndex, 2].Value = szTenPhong;
                sheet.Row(rowIndex).Style.Font.Bold = true;
                int stt = 0;
                foreach (var da in p.grpDA)
                {
                    rowIndex++;
                    stt++;
                    sheet.Cells[rowIndex, 1].Value = stt;
                    sheet.Cells[rowIndex, 2].Value = da.TenDuAn;

                    var lstUser = da.Users.ToList();
                    string szUser = "";

                    foreach (var item in lstUser)
                    {
                        string sz = item.Name;
                        szUser += sz + ",";
                    }
                    if (szUser.Length > 1)
                    {
                        szUser = szUser.Substring(0, szUser.Length - 1);
                    }
                    sheet.Cells[rowIndex, 5].Value = szUser;
                    foreach (var gd in da.grpGiaiDoan)
                    {
                        rowIndex++;
                        sheet.Cells[rowIndex, 1].Value = "-";
                        sheet.Cells[rowIndex, 2].Value = gd.TenGiaiDoan;
                        foreach (var kh in gd.grpKH)
                        {
                            var idkh = kh.IDKH;
                            var query = _QLTD_KhoKhanVuongMacService.Get_NoiDung_KK_GP_For_BaoCao(idkh);

                            if (query.Count() > 0)
                            {
                                string sz1 = "";
                                string sz2 = "";
                                var str = query.Select(x => x.KK).FirstOrDefault();
                                if (str != null)
                                {
                                    foreach (var item in str)
                                    {
                                        sz1 += item + "; ";
                                    }
                                    sheet.Cells[rowIndex, 3].Value = sz1;
                                }

                                var str2 = query.Select(x => x.GP).FirstOrDefault();
                                if (str2 != null)
                                {
                                    foreach (var item2 in str2)
                                    {
                                        sz2 += item2 + "; ";
                                    }
                                    sheet.Cells[rowIndex, 4].Value = sz2;
                                }
                            }
                        }
                    }
                }
            }
            return rowIndex;
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
