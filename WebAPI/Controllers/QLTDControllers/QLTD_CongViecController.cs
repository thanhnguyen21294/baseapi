﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_congviec")]
    [Authorize]
    public class QLTD_CongViecController : ApiControllerBase
    {
        private IQLTD_CongViecService _QLTD_CongViecService;
        private IDeleteService _deleteService;
        public QLTD_CongViecController(
            IErrorService errorService,
            IQLTD_CongViecService qLTD_CongViecService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CongViecService = qLTD_CongViecService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idGiaiDoan)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CongViecService.GetAll(idGiaiDoan);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CongViec>, IEnumerable<QLTD_CongViecViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
    }
}
