﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_kehoachcongviec")]
    [Authorize]
    public class QLTD_KeHoachCongViecController : ApiControllerBase
    {
        private IQLTD_KeHoachCongViecService _QLTD_KeHoachCongViecService;
        private IDeleteService _deleteService;
        public QLTD_KeHoachCongViecController(
            IErrorService errorService,
            IQLTD_KeHoachCongViecService qLTD_KeHoachCongViecService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_KeHoachCongViecService = qLTD_KeHoachCongViecService;
            this._deleteService = deleteService;
        }

        [Route("getbykehoach")]
        [HttpGet]
        public HttpResponseMessage GetByKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_KeHoachCongViecService.GetListByIdKeHoach(idKH);
                var modelVm = Mapper.Map<IEnumerable<QLTD_KeHoachCongViec>, IEnumerable<QLTD_KeHoachCongViecViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
    }
    
}
