﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.Mobile;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_ctcv_chuanbidautu")]
    [Authorize]
    public class QLTD_CTCV_ChuanBiDauTuController : ApiControllerBase
    {
        private IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        //public QLTD_CTCV_ChuanBiDauTuController(IErrorService errorService) : base(errorService) { }
        public QLTD_CTCV_ChuanBiDauTuController(
            IErrorService errorService,
            IQLTD_CTCV_ChuanBiDauTuService qLTD_CTCV_ChuanBiDauTuService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CTCV_ChuanBiDauTuService = qLTD_CTCV_ChuanBiDauTuService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiDauTuService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuanBiDauTu>, IEnumerable<QLTD_CTCV_ChuanBiDauTuViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiDauTuService.GetAllByIDKH(idKHCV, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuanBiDauTu>, IEnumerable<QLTD_CTCV_ChuanBiDauTuViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<QLTD_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new QLTD_CTCV_ChuanBiDauTu();
                        newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                        newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _QLTD_CTCV_ChuanBiDauTuService.Add(newCTCV);
                        _QLTD_CTCV_ChuanBiDauTuService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_ChuanBiDauTu, QLTD_CTCV_ChuanBiDauTuViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiDauTuService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuanBiDauTu, QLTD_CTCV_ChuanBiDauTuViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiDauTuService.GetByID(id);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuanBiDauTu, QLTD_CTCV_ChuanBiDauTuViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_CTCV_ChuanBiDauTuViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_CTCV_ChuanBiDauTuService.GetByID(vmData.IdChiTietCongViec);
                    updateData.BCPATK_NoiDung = vmData.BCPATK_NoiDung;
                    updateData.PDDT_SoQD = vmData.PDDT_SoQD;
                    updateData.PDDT_NgayPQ = null;
                    if (vmData.PDDT_NgayPQ != "" && vmData.PDDT_NgayPQ != null)
                    {
                        updateData.PDDT_NgayPQ = DateTime.ParseExact(vmData.PDDT_NgayPQ, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDDT_GiaTri = vmData.PDDT_GiaTri;
                    updateData.PDKHLCNT_SoQD = vmData.PDKHLCNT_SoQD;
                    updateData.PDKHLCNT_NgayPD = null;
                    if (vmData.PDKHLCNT_NgayPD != "" && vmData.PDKHLCNT_NgayPD != null)
                    {
                        updateData.PDKHLCNT_NgayPD = DateTime.ParseExact(vmData.PDKHLCNT_NgayPD, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDKHLCNT_GiaTri = vmData.PDKHLCNT_GiaTri;
                    updateData.KSHT_NoiDung = vmData.KSHT_NoiDung;
                    updateData.CGDD_NoiDung = vmData.CGDD_NoiDung;
                    updateData.SLHTKT_NoiDung = vmData.SLHTKT_NoiDung;
                    updateData.PDQH_KhongPheDuyet = vmData.PDQH_KhongPheDuyet;
                    updateData.PDQH_SoQD_QHCT = vmData.PDQH_SoQD_QHCT;
                    updateData.PDQH_NgayPD_QHCT = null;
                    if (vmData.PDQH_NgayPD_QHCT != "" && vmData.PDQH_NgayPD_QHCT != null)
                    {
                        updateData.PDQH_NgayPD_QHCT = DateTime.ParseExact(vmData.PDQH_NgayPD_QHCT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDQH_SoQD_QHTMB = vmData.PDQH_SoQD_QHTMB;
                    updateData.PDQH_NgayPD_QHTMB = null;
                    if (vmData.PDQH_NgayPD_QHTMB != "" && vmData.PDQH_NgayPD_QHTMB != null)
                    {
                        updateData.PDQH_NgayPD_QHTMB = DateTime.ParseExact(vmData.PDQH_NgayPD_QHTMB, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.KSDCDH_NoiDung = vmData.KSDCDH_NoiDung;
                    updateData.TTTDPCCC_NoiDung = vmData.TTTDPCCC_NoiDung;
                    updateData.TTK_NoiDung = vmData.TTK_NoiDung;
                    updateData.LHSCBDT_KhoKhanVuongMac = vmData.LHSCBDT_KhoKhanVuongMac;
                    updateData.LHSCBDT_KienNghiDeXuat = vmData.LHSCBDT_KienNghiDeXuat;
                    updateData.LHSCBDT_NgayHoanThanh = null;
                    if (vmData.LHSCBDT_NgayHoanThanh != "" && vmData.LHSCBDT_NgayHoanThanh != null)
                    {
                        updateData.LHSCBDT_NgayHoanThanh = DateTime.ParseExact(vmData.LHSCBDT_NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.LHSCBDT_SoQD = vmData.LHSCBDT_SoQD;
                    updateData.LHSCBDT_NgayLaySo = null;
                    if (vmData.LHSCBDT_NgayLaySo != "" && vmData.LHSCBDT_NgayLaySo != null)
                    {
                        updateData.LHSCBDT_NgayLaySo = DateTime.ParseExact(vmData.LHSCBDT_NgayLaySo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.THSCBDT_SoToTrinh = vmData.THSCBDT_SoToTrinh;
                    updateData.THSCBDT_NgayTrinh = null;
                    if (vmData.THSCBDT_NgayTrinh != "" && vmData.THSCBDT_NgayTrinh != null)
                    {
                        updateData.THSCBDT_NgayTrinh = DateTime.ParseExact(vmData.THSCBDT_NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDDA_BCKTKT_SoQD = vmData.PDDA_BCKTKT_SoQD;
                    updateData.PDDA_BCKTKT_NgayPD = null;
                    if (vmData.PDDA_BCKTKT_NgayPD != "" && vmData.PDDA_BCKTKT_NgayPD != null)
                    {
                        updateData.PDDA_BCKTKT_NgayPD = DateTime.ParseExact(vmData.PDDA_BCKTKT_NgayPD, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDDA_BCKTKT_GiaTri = vmData.PDDA_BCKTKT_GiaTri;
                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_CTCV_ChuanBiDauTuService.Update(updateData);
                    _QLTD_CTCV_ChuanBiDauTuService.Save();

                    var responseData = Mapper.Map<QLTD_CTCV_ChuanBiDauTu, QLTD_CTCV_ChuanBiDauTuViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoach);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _QLTD_CTCV_ChuanBiDauTuService.GetByID(vmData.IdChiTietCongViec).QLTD_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }

}
