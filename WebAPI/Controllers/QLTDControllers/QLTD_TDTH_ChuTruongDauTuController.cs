﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_tdth_chutruongdautu")]
    [Authorize]
    public class QLTD_TDTH_ChuTruongDauTuController : ApiControllerBase
    {
        private IQLTD_TienDoThucHienService _QLTD_TienDoThucHienService;
        private IDeleteService _deleteService;
        public QLTD_TDTH_ChuTruongDauTuController(IErrorService errorService,
            IQLTD_TienDoThucHienService qLTD_TienDoThucHienService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_TienDoThucHienService = qLTD_TienDoThucHienService;
            this._deleteService = deleteService;
        }

        [Route("getbychitietcv")]
        [HttpGet]
        public HttpResponseMessage GetByChiTietCongViec(HttpRequestMessage request, int idCTCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_TienDoThucHienService.GetByIDChiTietCongViec(idCTCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_TDTH_ChuTruongDauTu>, IEnumerable<QLTD_TDTH_ChuTruongDauTuViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idCTCV, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _QLTD_TienDoThucHienService.GetByFilter(idCTCV, page, pageSize, "", out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_TDTH_ChuTruongDauTu>, IEnumerable<QLTD_TDTH_ChuTruongDauTuViewModels>>(model);

                PaginationSet<QLTD_TDTH_ChuTruongDauTuViewModels> pagedSet = new PaginationSet<QLTD_TDTH_ChuTruongDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }


        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QLTD_TDTH_ChuTruongDauTuViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    var newData = new QLTD_TDTH_ChuTruongDauTu();
                    newData.IdChiTietCongViec = Convert.ToInt32(vmData.IdChiTietCongViec);
                    newData.NoiDung = vmData.NoiDung;
                    newData.TenDonViThucHien = vmData.TenDonViThucHien;
                    newData.DonViThamDinh = vmData.DonViThamDinh;
                    newData.DonViPhoiHop = vmData.DonViPhoiHop;
                    newData.CoQuanPheDuyet = vmData.CoQuanPheDuyet;
                    if (vmData.NgayBatDau != "" && vmData.NgayBatDau != null)
                        newData.NgayBatDau = DateTime.ParseExact(vmData.NgayBatDau, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmData.NgayHoanThanh != "" && vmData.NgayHoanThanh != null)
                        newData.NgayHoanThanh = DateTime.ParseExact(vmData.NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newData.HoanThanh = vmData.HoanThanh;
                    newData.GhiChu = vmData.GhiChu;
                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;
                    
                    _QLTD_TienDoThucHienService.Add(newData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_TDTH_ChuTruongDauTu, QLTD_TDTH_ChuTruongDauTuViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("clone")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Clone(HttpRequestMessage request, QLTD_TDTH_ChuTruongDauTuViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var cloneData = _QLTD_TienDoThucHienService.GetByIDChiTietCongViec(vmData.IdChiTietCongViec).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    cloneData.CreatedDate = DateTime.Now;
                    cloneData.CreatedBy = User.Identity.GetUserId();
                    cloneData.Status = true;

                    _QLTD_TienDoThucHienService.Add(cloneData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_TDTH_ChuTruongDauTu, QLTD_TDTH_ChuTruongDauTuViewModels>(cloneData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_TDTH_ChuTruongDauTuViewModels vmUpdate)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_TienDoThucHienService.GetById(vmUpdate.IdTienDoThucHien);
                    updateData.NoiDung = vmUpdate.NoiDung;
                    updateData.TenDonViThucHien = vmUpdate.TenDonViThucHien;
                    updateData.DonViThamDinh = vmUpdate.DonViThamDinh;
                    updateData.DonViPhoiHop = vmUpdate.DonViPhoiHop;
                    updateData.CoQuanPheDuyet = vmUpdate.CoQuanPheDuyet;
                    if (vmUpdate.NgayBatDau != "" && vmUpdate.NgayBatDau != null)
                        updateData.NgayBatDau = DateTime.ParseExact(vmUpdate.NgayBatDau, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmUpdate.NgayHoanThanh != "" && vmUpdate.NgayHoanThanh != null)
                        updateData.NgayHoanThanh = DateTime.ParseExact(vmUpdate.NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateData.HoanThanh = vmUpdate.HoanThanh;
                    updateData.GhiChu = vmUpdate.GhiChu;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_TienDoThucHienService.Update(updateData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_TDTH_ChuTruongDauTu, QLTD_TDTH_ChuTruongDauTuViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_TienDoThucHienService.GetById(id);
                var responseData = Mapper.Map<QLTD_TDTH_ChuTruongDauTu, QLTD_TDTH_ChuTruongDauTuViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _QLTD_TienDoThucHienService.Delete(id);
                    _QLTD_TienDoThucHienService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
