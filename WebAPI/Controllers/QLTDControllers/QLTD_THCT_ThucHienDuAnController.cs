﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_thct_thuchienduan")]
    [Authorize]
    public class QLTD_THCT_ThucHienDuAnController : ApiControllerBase
    {
        private IQLTD_THCT_ThucHienDuAnService _QLTD_TienDoThucHienService;
        private IDeleteService _deleteService;
        public QLTD_THCT_ThucHienDuAnController(IErrorService errorService,
            IQLTD_THCT_ThucHienDuAnService qLTD_TienDoThucHienService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_TienDoThucHienService = qLTD_TienDoThucHienService;
            this._deleteService = deleteService;
        }

        [Route("getbychitietcv")]
        [HttpGet]
        public HttpResponseMessage GetByChiTietCongViec(HttpRequestMessage request, int idTHCT)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_TienDoThucHienService.GetByIDTienDoThucHien(idTHCT);
                var modelVm = Mapper.Map<IEnumerable<QLTD_THCT_ThucHienDuAn>, IEnumerable<QLTD_THCT_ThucHienDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idTDTH, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _QLTD_TienDoThucHienService.GetByFilter(idTDTH, page, pageSize, "", out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_THCT_ThucHienDuAn>, IEnumerable<QLTD_THCT_ThucHienDuAnViewModels>>(model);

                PaginationSet<QLTD_THCT_ThucHienDuAnViewModels> pagedSet = new PaginationSet<QLTD_THCT_ThucHienDuAnViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getcongviechangngay")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCongViecHangNgay(HttpRequestMessage request, int idCTCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                DateTime dateNow = DateTime.Now;
                DateTime now = DateTime.Today;
                DateTime dateCheck = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0);

                int isCheck = 0;
                if (dateNow > dateCheck)
                {
                    isCheck = 1;
                }
                var model = _QLTD_TienDoThucHienService.GetCongViecHangNgay(idCTCV);
                CongViecHangNgay<QLTD_THCT_ThucHienDuAn> cvhn = new CongViecHangNgay<QLTD_THCT_ThucHienDuAn>
                {
                    dateNow = dateNow,
                    isCheck = isCheck,
                    items = model
                };
                response = request.CreateResponse(HttpStatusCode.OK, cvhn);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QLTD_THCT_ThucHienDuAnViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var newData = new QLTD_THCT_ThucHienDuAn();
                    newData.IdTienDoThucHien = Convert.ToInt32(vmData.IdTienDoThucHien);
                    newData.NoiDung = vmData.NoiDung;
                    newData.ThayDoi = vmData.ThayDoi;
                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;

                    _QLTD_TienDoThucHienService.Add(newData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_THCT_ThucHienDuAn, QLTD_THCT_ThucHienDuAnViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("clone")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Clone(HttpRequestMessage request, QLTD_THCT_ThucHienDuAnViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var cloneData = _QLTD_TienDoThucHienService.GetByIDTienDoThucHien(vmData.IdTienDoThucHien).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                    cloneData.CreatedDate = DateTime.Now;
                    cloneData.CreatedBy = User.Identity.GetUserId();
                    cloneData.Status = true;

                    _QLTD_TienDoThucHienService.Add(cloneData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_THCT_ThucHienDuAn, QLTD_THCT_ThucHienDuAnViewModels>(cloneData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_THCT_ThucHienDuAnViewModels vmUpdate)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_TienDoThucHienService.GetById(vmUpdate.IdThucHienChiTiet);
                    updateData.NoiDung = vmUpdate.NoiDung;
                    updateData.ThayDoi = vmUpdate.ThayDoi;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_TienDoThucHienService.Update(updateData);
                    _QLTD_TienDoThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_THCT_ThucHienDuAn, QLTD_THCT_ThucHienDuAnViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_TienDoThucHienService.GetById(id);
                var responseData = Mapper.Map<QLTD_THCT_ThucHienDuAn, QLTD_THCT_ThucHienDuAnViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _QLTD_TienDoThucHienService.Delete(id);
                    _QLTD_TienDoThucHienService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
