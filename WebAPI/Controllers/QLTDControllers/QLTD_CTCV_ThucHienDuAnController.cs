﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.Mobile;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_ctcv_thuchienduan")]
    [Authorize]
    public class QLTD_CTCV_ThucHienDuAnController : ApiControllerBase
    {
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        public QLTD_CTCV_ThucHienDuAnController(
            IErrorService errorService,
            IQLTD_CTCV_ThucHienDuAnService qLTD_CTCV_ThucHienDuAnService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CTCV_ThucHienDuAnService = qLTD_CTCV_ThucHienDuAnService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ThucHienDuAnService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ThucHienDuAn>, IEnumerable<QLTD_CTCV_ThucHienDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ThucHienDuAnService.GetAllByIDKH(idKHCV, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ThucHienDuAn>, IEnumerable<QLTD_CTCV_ThucHienDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<QLTD_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new QLTD_CTCV_ThucHienDuAn();
                        newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                        newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _QLTD_CTCV_ThucHienDuAnService.Add(newCTCV);
                        _QLTD_CTCV_ThucHienDuAnService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_ThucHienDuAn, QLTD_CTCV_ThucHienDuAnViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ThucHienDuAnService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<QLTD_CTCV_ThucHienDuAn, QLTD_CTCV_ThucHienDuAnViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ThucHienDuAnService.GetByID(id);
                var modelVm = Mapper.Map<QLTD_CTCV_ThucHienDuAn, QLTD_CTCV_ThucHienDuAnViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("thongkehoancong")]
        [HttpGet]
        public HttpResponseMessage ThongKeHoanCong(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ThucHienDuAnService.GetThongKe().Select(x => new { 
                    IdDuAn = x.QLTD_KeHoachCongViec.QLTD_KeHoach.IdDuAn,
                    TenDuAn = x.QLTD_KeHoachCongViec.QLTD_KeHoach.DuAn.TenDuAn,
                    IdKeHoach = x.QLTD_KeHoachCongViec.QLTD_KeHoach.IdKeHoach,
                    NgayTinhTrang = x.QLTD_KeHoachCongViec.QLTD_KeHoach.NgayTinhTrang,
                    NgayHoanThanh = x.NgayHoanThanh,
                }).ToList();
                


                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_CTCV_ThucHienDuAnViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_CTCV_ThucHienDuAnService.GetByID(vmData.IdChiTietCongViec);
                    updateData.GPMB_DamBaoTienDo = vmData.GPMB_DamBaoTienDo;
                    updateData.GPMB_KhoKhanVuongMac = vmData.GPMB_KhoKhanVuongMac;
                    updateData.GPMB_DeXuatGiaiPhap = vmData.GPMB_DeXuatGiaiPhap;
                    updateData.CTHLCNT_DangLapTKBVTC = vmData.CTHLCNT_DangLapTKBVTC;
                    updateData.CTHLCNT_DangTrinhTKBVTC = vmData.CTHLCNT_DangTrinhTKBVTC;
                    updateData.CTHLCNT_SoNgayQDPD_KHLCNT = vmData.CTHLCNT_SoNgayQDPD_KHLCNT;
                    updateData.CTHLCNT_SoQDKHLCNT = vmData.CTHLCNT_SoQDKHLCNT;
                    if (vmData.CTHLCNT_NgayPheDuyetKHLCNT != "" && vmData.CTHLCNT_NgayPheDuyetKHLCNT != null)
                    {
                        updateData.CTHLCNT_NgayPheDuyetKHLCNT = DateTime.ParseExact(vmData.CTHLCNT_NgayPheDuyetKHLCNT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.CTHLCNT_KhoKhan = vmData.CTHLCNT_KhoKhan;
                    updateData.DLCNT_SoQDKQLCNT = vmData.DLCNT_SoQDKQLCNT;
                    updateData.DLCNT_NgayPheDuyet = null;
                    if (vmData.DLCNT_NgayPheDuyet != "" && vmData.DLCNT_NgayPheDuyet != null)
                    {
                        updateData.DLCNT_NgayPheDuyet = DateTime.ParseExact(vmData.DLCNT_NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.DLCNT_GiaTri = vmData.DLCNT_GiaTri;
                    if (vmData.DLCNT_NgayPD_KQLCNT != "" && vmData.DLCNT_NgayPD_KQLCNT != null)
                    {
                        updateData.DLCNT_NgayPD_KQLCNT = DateTime.ParseExact(vmData.DLCNT_NgayPD_KQLCNT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.DTCXD_NgayBanGiaoMB = null;
                    if (vmData.DTCXD_NgayBanGiaoMB != "" && vmData.DTCXD_NgayBanGiaoMB != null)
                    {
                        updateData.DTCXD_NgayBanGiaoMB = DateTime.ParseExact(vmData.DTCXD_NgayBanGiaoMB, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.DTCXD_ThoiGianKhoiCong = vmData.DTCXD_ThoiGianKhoiCong;
                    updateData.DTCXD_KhoKhanVuongMac = vmData.DTCXD_KhoKhanVuongMac;
                    if (vmData.DTCXD_NgayBBNT_HoanThanhCT != "" && vmData.DTCXD_NgayBBNT_HoanThanhCT != null)
                    {
                        updateData.DTCXD_NgayBBNT_HoanThanhCT = DateTime.ParseExact(vmData.DTCXD_NgayBBNT_HoanThanhCT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.HTTC_NgayHoanThanh = null;
                    if (vmData.HTTC_NgayHoanThanh != "" && vmData.HTTC_NgayHoanThanh != null)
                    {
                        updateData.HTTC_NgayHoanThanh = DateTime.ParseExact(vmData.HTTC_NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_CTCV_ThucHienDuAnService.Update(updateData);
                    _QLTD_CTCV_ThucHienDuAnService.Save();

                    var responseData = Mapper.Map<QLTD_CTCV_ThucHienDuAn, QLTD_CTCV_ThucHienDuAnViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoach);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _QLTD_CTCV_ThucHienDuAnService.GetByID(vmData.IdChiTietCongViec).QLTD_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
