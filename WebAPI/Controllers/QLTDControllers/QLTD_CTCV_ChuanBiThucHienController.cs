﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.Mobile;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_ctcv_chuanbithuchien")]
    [Authorize]
    public class QLTD_CTCV_ChuanBiThucHienController : ApiControllerBase
    {
        private IQLTD_CTCV_ChuanBiThucHienService _QLTD_CTCV_ChuanBiThucHienService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;

        public QLTD_CTCV_ChuanBiThucHienController(
            IErrorService errorService,
            IQLTD_CTCV_ChuanBiThucHienService qLTD_CTCV_ChuanBiThucHienService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CTCV_ChuanBiThucHienService = qLTD_CTCV_ChuanBiThucHienService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiThucHienService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuanBiThucHien>, IEnumerable<QLTD_CTCV_ChuanBiThucHienViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiThucHienService.GetAllByIDKH(idKHCV, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_ChuanBiThucHien>, IEnumerable<QLTD_CTCV_ChuanBiThucHienViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<QLTD_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new QLTD_CTCV_ChuanBiThucHien();
                        newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                        newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _QLTD_CTCV_ChuanBiThucHienService.Add(newCTCV);
                        _QLTD_CTCV_ChuanBiThucHienService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_ChuanBiThucHien, QLTD_CTCV_ChuanBiThucHienViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiThucHienService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuanBiThucHien, QLTD_CTCV_ChuanBiThucHienViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_ChuanBiThucHienService.GetByID(id);
                var modelVm = Mapper.Map<QLTD_CTCV_ChuanBiThucHien, QLTD_CTCV_ChuanBiThucHienViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_CTCV_ChuanBiThucHienViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_CTCV_ChuanBiThucHienService.GetByID(vmData.IdChiTietCongViec);
                    updateData.PDDT_SoQuyetDinh = vmData.PDDT_SoQuyetDinh;
                    updateData.PDDT_NgayPheDuyet = null;
                    if (vmData.PDDT_NgayPheDuyet != "" && vmData.PDDT_NgayPheDuyet != null)
                    {
                        updateData.PDDT_NgayPheDuyet = DateTime.ParseExact(vmData.PDDT_NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDDT_GiaTri = vmData.PDDT_GiaTri;

                    updateData.PDKHLCNT_SoQuyetDinh = vmData.PDKHLCNT_SoQuyetDinh;
                    updateData.PDKHLCNT_NgayPheDuyet = null;
                    if (vmData.PDKHLCNT_NgayPheDuyet != "" && vmData.PDKHLCNT_NgayPheDuyet != null)
                    {
                        updateData.PDKHLCNT_NgayPheDuyet = DateTime.ParseExact(vmData.PDKHLCNT_NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDKHLCNT_GiaTri = vmData.PDKHLCNT_GiaTri;

                    updateData.LHS_KhoKhanVuongMac = vmData.LHS_KhoKhanVuongMac;
                    updateData.LHS_KienNghiDeXuat = vmData.LHS_KienNghiDeXuat;
                    updateData.LHS_NgayHoanThanh = null;
                    if (vmData.LHS_NgayHoanThanh != "" && vmData.LHS_NgayHoanThanh != null)
                    {
                        updateData.LHS_NgayHoanThanh = DateTime.ParseExact(vmData.LHS_NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.THS_SoToTrinh = vmData.THS_SoToTrinh;
                    updateData.LHS_NgayTrinh = null;
                    if (vmData.LHS_NgayTrinh != "" && vmData.LHS_NgayTrinh != null)
                    {
                        updateData.LHS_NgayTrinh = DateTime.ParseExact(vmData.LHS_NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.PDDA_SoQuyetDinh = vmData.PDDA_SoQuyetDinh;
                    updateData.PDDA_NgayPheDuyet = null;
                    if (vmData.PDDA_NgayPheDuyet != "" && vmData.PDDA_NgayPheDuyet != null)
                    {
                        updateData.PDDA_NgayPheDuyet = DateTime.ParseExact(vmData.PDDA_NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.PDDA_GiaTri = vmData.PDDA_GiaTri;
                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_CTCV_ChuanBiThucHienService.Update(updateData);
                    _QLTD_CTCV_ChuanBiThucHienService.Save();

                    var responseData = Mapper.Map<QLTD_CTCV_ChuanBiThucHien, QLTD_CTCV_ChuanBiThucHienViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoach);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _QLTD_CTCV_ChuanBiThucHienService.GetByID(vmData.IdChiTietCongViec).QLTD_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
