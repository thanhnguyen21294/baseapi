﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.QLTD;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_kehoachtiendochung")]
    [Authorize]
    public class QLTD_KeHoachTienDoChungController : ApiControllerBase
    {
        private IQLTD_KeHoachTienDoChungService _QLTD_KeHoachTienDoChungService;
        private IDeleteService _deleteService;
        public QLTD_KeHoachTienDoChungController(IErrorService errorService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            this._deleteService = deleteService;
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QLTD_KeHoachTienDoChungViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newData = new QLTD_KeHoachTienDoChung();
                    newData.IdKeHoach = vmKeHoach.IdKeHoach;
                    newData.NoiDung = vmKeHoach.NoiDung;
                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;
                    _QLTD_KeHoachTienDoChungService.Add(newData);
                    _QLTD_KeHoachTienDoChungService.Save();

                    var responseData = Mapper.Map<QLTD_KeHoachTienDoChung, QLTD_KeHoachTienDoChungViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_KeHoachTienDoChungViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_KeHoachTienDoChungService.GetById(vmKeHoach.IdKeHoachTienDoChung);
                    updateData.NoiDung = vmKeHoach.NoiDung;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();
                    _QLTD_KeHoachTienDoChungService.Update(updateData);
                    _QLTD_KeHoachTienDoChungService.Save();

                    var responseData = Mapper.Map<QLTD_KeHoachTienDoChung, QLTD_KeHoachTienDoChungViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _QLTD_KeHoachTienDoChungService.Delete(id);
                    _QLTD_KeHoachTienDoChungService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_KeHoachTienDoChungService.GetById(id);
                var responseData = Mapper.Map<QLTD_KeHoachTienDoChung, QLTD_KeHoachTienDoChungViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
        [Route("getbyidkehoach")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QLTD_KeHoachTienDoChungService.GetByIdKeHoach(idKH);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }
    }
}
