﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using Service.Mobile;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/qltd_ctcv_quyettoanduan")]
    [Authorize]
    public class QLTD_CTCV_QuyetToanDuAnController : ApiControllerBase
    {
        private IQLTD_CTCV_QuyetToanDuAnService _QLTD_CTCV_QuyetToanDuAnService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        public QLTD_CTCV_QuyetToanDuAnController(
            IErrorService errorService,
            IQLTD_CTCV_QuyetToanDuAnService qLTD_CTCV_QuyetToanDuAnService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._QLTD_CTCV_QuyetToanDuAnService = qLTD_CTCV_QuyetToanDuAnService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_QuyetToanDuAnService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_QuyetToanDuAn>, IEnumerable<QLTD_CTCV_QuyetToanDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_QuyetToanDuAnService.GetAllByIDKH(idKHCV, filter);
                var modelVm = Mapper.Map<IEnumerable<QLTD_CTCV_QuyetToanDuAn>, IEnumerable<QLTD_CTCV_QuyetToanDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<QLTD_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new QLTD_CTCV_QuyetToanDuAn();
                        newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                        newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _QLTD_CTCV_QuyetToanDuAnService.Add(newCTCV);
                        _QLTD_CTCV_QuyetToanDuAnService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_QuyetToanDuAn, QLTD_CTCV_QuyetToanDuAnViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_QuyetToanDuAnService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<QLTD_CTCV_QuyetToanDuAn, QLTD_CTCV_QuyetToanDuAnViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QLTD_CTCV_QuyetToanDuAnService.GetByID(id);
                var modelVm = Mapper.Map<QLTD_CTCV_QuyetToanDuAn, QLTD_CTCV_QuyetToanDuAnViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QLTD_CTCV_QuyetToanDuAnViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _QLTD_CTCV_QuyetToanDuAnService.GetByID(vmData.IdChiTietCongViec);
                    updateData.HSQLCL_DaHoanThanh = vmData.HSQLCL_DaHoanThanh;
                    updateData.TTQT_DangThamTra = vmData.TTQT_DangThamTra;
                    updateData.TTQT_SoThamTra = vmData.TTQT_SoThamTra;
                    updateData.TTQT_NgayThamTra = null;
                    if (vmData.TTQT_NgayThamTra != "" && vmData.TTQT_NgayThamTra != null)
                    {
                        updateData.TTQT_NgayThamTra = DateTime.ParseExact(vmData.TTQT_NgayThamTra, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.TTQT_GiaTri = vmData.TTQT_GiaTri;


                    updateData.KTDL_KhongKiemToan = vmData.KTDL_KhongKiemToan;
                    updateData.KTDL_DangKiemToan = vmData.KTDL_DangKiemToan;
                    updateData.KTDL_GiaTriSauKiemToan = vmData.KTDL_GiaTriSauKiemToan;
                    updateData.HTHSQT_GiaTriQuyetToan = vmData.HTHSQT_GiaTriQuyetToan;
                    updateData.HTQT_SoQD = vmData.HTQT_SoQD;
                    updateData.HTQT_NgayPD = null;
                    if (vmData.HTQT_NgayPD != "" && vmData.HTQT_NgayPD != null)
                    {
                        updateData.HTQT_NgayPD = DateTime.ParseExact(vmData.HTQT_NgayPD, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.HTQT_GiaTri = vmData.HTQT_GiaTri;
                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;

                    updateData.HTQT_CongNoThu = vmData.HTQT_CongNoThu;
                    updateData.HTQT_CongNoTra = vmData.HTQT_CongNoTra;
                    updateData.HTQT_BoTriTraCongNo = vmData.HTQT_BoTriTraCongNo;

                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _QLTD_CTCV_QuyetToanDuAnService.Update(updateData);
                    _QLTD_CTCV_QuyetToanDuAnService.Save();

                    var responseData = Mapper.Map<QLTD_CTCV_QuyetToanDuAn, QLTD_CTCV_QuyetToanDuAnViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoach);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _QLTD_CTCV_QuyetToanDuAnService.GetByID(vmData.IdChiTietCongViec).QLTD_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}