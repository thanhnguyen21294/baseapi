﻿using AutoMapper;
using Model.Models.QLTD;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.QLTD;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/vonCapNhatKLNT")]
    [Authorize]
    public class VonCapNhatKLNTController : ApiControllerBase
    {
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IDuAnService _duanService;
        //private IDeleteService _deleteService;
        public VonCapNhatKLNTController(
            IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duanService,
            IDeleteService deleteService) : base(errorService)
        {
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._duanService = duanService;
            //this._deleteService = deleteService;
        }
        [HttpGet]
        [Route("getbyidparent")]
        public HttpResponseMessage GetByIdParent(HttpRequestMessage request, int idFile, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _vonCapNhatKLNTService.GetAll().Where(x => x.IdFile == idFile);

                if(filter != null)
                {
                    model = model.Where(x => x.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()));
                }
                
                var modelVm = Mapper.Map<IEnumerable<VonCapNhatKLNT>, IEnumerable<VonCapNhatKLNTViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }


    }
}
