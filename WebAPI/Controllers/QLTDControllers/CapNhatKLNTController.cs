﻿using AutoMapper;
using Model.Models.QLTD;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.QLTDControllers
{
    [RoutePrefix("api/capNhatKLNT")]
    [Authorize]
    public class CapNhatKLNTController : ApiControllerBase
    {
        private ICapNhatKLNTService _CapNhatKLNTService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        //private IDeleteService _deleteService;
        public CapNhatKLNTController(
            IErrorService errorService,
            ICapNhatKLNTService capNhatKLNTService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
        IDeleteService deleteService) : base(errorService)
        {
            this._CapNhatKLNTService = capNhatKLNTService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            //this._deleteService = deleteService;
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Add(HttpRequestMessage request, CapNhatKLNTViewModels capNhatVM)
        {
            return CreateHttpResponse(request, () =>
            {
                var datenow = DateTime.Now;
                HttpResponseMessage response = null;
                CapNhatKLNT capNhat = new CapNhatKLNT();
                capNhat.UpdateCapNhatKLNT(capNhatVM);
                //if (!string.IsNullOrEmpty(duAnViewModels.NgayKhoiCongKeHoach))
                //{
                //    DateTime dateTime = DateTime.ParseExact(duAnViewModels.NgayKhoiCongKeHoach, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                //    duAn.NgayKhoiCongKeHoach = dateTime;
                //}
                string day;
                string mont;
                if(datenow.Day < 10)
                {
                    day = "0" + datenow.Day.ToString();
                }
                else
                {
                    day = datenow.Day.ToString();
                }

                if(datenow.Month < 10)
                {
                    mont = "0" + datenow.Month.ToString();
                }
                else
                {
                    mont = datenow.Month.ToString();
                }


                var a = (day + "/" + mont + "/" + datenow.Year).ToString();
                capNhat.CreatedDate = DateTime.ParseExact((day + "/" + mont + "/" + datenow.Year).ToString(), "dd/MM/yyyy", new CultureInfo("vi-VN"));
                capNhat.Status = true;

                capNhat = _CapNhatKLNTService.Add(capNhat);
                _CapNhatKLNTService.Save();
                capNhatVM = Mapper.Map<CapNhatKLNT, CapNhatKLNTViewModels>(capNhat);
                response = request.CreateResponse(HttpStatusCode.Created, capNhatVM);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetFileCapNhatKLNTByListPaging(HttpRequestMessage request,  int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {

                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _CapNhatKLNTService.GetAll().OrderByDescending(x => x.IdFile);

                var modelVm = Mapper.Map<IEnumerable<CapNhatKLNT>, IEnumerable<CapNhatKLNTViewModels>>(model);

                PaginationSet<CapNhatKLNTViewModels> pagedSet = new PaginationSet<CapNhatKLNTViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };


                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;

                ////int totalRow = 0;
                //var capNhatKLNTs = _CapNhatKLNTService.GetAll();
                //var modelData = new
                //{
                //    Items = capNhatKLNTs.Skip(pageSize * (page - 1)).Take(pageSize),
                //    Total = capNhatKLNTs.Count(),
                //};
                ////var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(model);

                //var response = request.CreateResponse(HttpStatusCode.OK, modelData);

                //return response;


            });
        }

        [Route("delete")]
        [HttpPost]
        //[Permission(Action = "Delete", Function = "BAOCAOTABLE")]
        public HttpResponseMessage Delete(HttpRequestMessage request, CapNhatKLNT capNhatKLNT)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lst = _vonCapNhatKLNTService.ListIntFile(capNhatKLNT.IdFile);
                    foreach(var item in lst)
                    {
                        _vonCapNhatKLNTService.Delete(item);
                    }
                    _vonCapNhatKLNTService.Save();
                    _CapNhatKLNTService.Delete(capNhatKLNT.IdFile);
                    _CapNhatKLNTService.Save();
                    var folderReport = "/UploadedFiles/CapNhatKLNTGiaiNgan/";
                    string filePath = HttpContext.Current.Server.MapPath(folderReport);
                    string fullPath = filePath + capNhatKLNT.FileName;
                    File.Delete(fullPath);

                    response = request.CreateResponse(HttpStatusCode.Created, capNhatKLNT.IdFile);
                }

                return response;
            });
        }


        

    }
}
