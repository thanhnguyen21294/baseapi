﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.DieuHanhModel;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service;
using Service.BCService;
using Service.GPMBService;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.QLTDService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Models.QLTD;
using WebAPI.Providers;
using Service.Mobile;
using Data.Repositories;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/dieuhanh")]
    public class DieuHanhController : ApiControllerBase
    {
        public IDuAnService _duAnService;
        public INguonVonService _nguonvonService;
        public IGiaiDoanDuAnService _giaidoanService;
        public ITinhTrangDuAnService _tinhtrangDAService;
        public INhomDuAnService _nhomDAService;
        public IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        public INguonVonDuAnService _nguonVonDuAnService;
        public IKeHoachVonService _keHoachVonService;
        public ILapQuanLyDauTuService _lapQuanLyDauTuService;
        public IBaoCaoTienDoService _baoCaoTienDoService;
        public IQLTD_TienDoThucHienService _QLTD_TDTH_ChuTruongDauTuService;
        public IQLTD_THCT_ChuTruongDauTuService _QLTD_THCT_ChuTruongDauTuService;
        public IQLTD_TDTH_ChuanBiDauTuService _QLTD_TDTH_ChuanBiDauTuService;
        public IQLTD_THCT_ChuanBiDauTuService _QLTD_THCT_ChuanBiDauTuService;
        public IQLTD_TDTH_ChuanBiThucHienService _QLTD_TDTH_ChuanBiThucHienService;
        public IQLTD_THCT_ChuanBiThucHienService _QLTD_THCT_ChuanBiThucHienService;
        public IQLTD_TDTH_ThucHienDuAnService _QLTD_TDTH_ThucHienDuAnService;
        public IQLTD_THCT_ThucHienDuAnService _QLTD_THCT_ThucHienDuAnService;
        public IQLTD_TDTH_QuyetToanDuAnService _QLTD_TDTH_QuyetToanDuAnService;
        public IQLTD_THCT_QuyetToanDuAnService _QLTD_THCT_QuyetToanDuAnService;

        public IQLTD_CTCV_ChuTruongDauTuService _QLTD_CTCV_ChuTruongDauTuService;
        public IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;
        public IQLTD_CTCV_ChuanBiThucHienService _QLTD_CTCV_ChuanBiThucHienService;
        public IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;
        public IQLTD_CTCV_QuyetToanDuAnService _QLTD_CTCV_QuyetToanDuAnService;
        public IQLTD_KeHoachService _QLTD_KeHoachService;
        public IQLTD_KeHoachCongViecService _QLTD_KeHoachCongViecService;
        private IQLTD_KeHoachTienDoChungService _QLTD_KeHoachTienDoChungService;

        private IGPMB_TongQuanKetQuaService _GPMB_TongQuanKetQuaService;
        private IGPMB_KetQuaService _GPMB_KetQuaService;
        private IGPMB_KetQua_ChiTietService _GPMB_KetQua_ChiTietService;

        private IGPMB_KeHoachService _GPMB_KeHoachService;
        private IGPMB_KeHoachTienDoChungService _GPMB_KeHoachTienDoChungService;
        private IGPMB_CTCVService _GPMB_CTCVService;
        private IGPMB_FileService _GPMB_FileService;

        private IDuAnUserService _DuAnUserService;
        private INotificationService _NotificationService;
        private readonly IAppRoleRepository _appRoleRepository;
        private readonly IAppUserRepository _appUserRepository;

        public DieuHanhController(IDuAnService duanService, INguonVonService nguonvonService,
            IKeHoachVonService keHoachVonService, ILapQuanLyDauTuService lapQuanLyDauTuService,
            IGiaiDoanDuAnService giaidoanService, ITinhTrangDuAnService tinhtrangDAService, INhomDuAnService nhomDAService,
            IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, INguonVonDuAnService nguonVonDuAnService, IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_TienDoThucHienService qltd_TDTH_ChuTruongDauTuService,
            IQLTD_THCT_ChuTruongDauTuService qltd_THCT_ChuTruongDauTuService,
            IQLTD_TDTH_ChuanBiDauTuService qltd_TDTH_ChuanBiDauTuService,
            IQLTD_THCT_ChuanBiDauTuService qltd_THCT_ChuanBiDauTuService,
            IQLTD_TDTH_ChuanBiThucHienService qltd_TDTH_ChuanBiThucHienService,
            IQLTD_THCT_ChuanBiThucHienService qltd_THCT_ChuanBiThucHienService,
            IQLTD_TDTH_ThucHienDuAnService qltd_TDTH_ThucHienDuAnService,
            IQLTD_THCT_ThucHienDuAnService qltd_THCT_ThucHienDuAnService,
            IQLTD_TDTH_QuyetToanDuAnService qltd_TDTH_QuyetToanDuAnService,
            IQLTD_THCT_QuyetToanDuAnService qltd_THCT_QuyetToanDuAnService,
            IQLTD_CTCV_ChuTruongDauTuService qltd_CTCV_ChuTruongDauTuService,
            IQLTD_CTCV_ChuanBiDauTuService qltd_CTCV_ChuanBiDauTuService,
            IQLTD_CTCV_ChuanBiThucHienService qltd_CTCV_ChuanBiThucHienService,
            IQLTD_CTCV_ThucHienDuAnService qltd_CTCV_ThucHienDuAnService,
            IQLTD_CTCV_QuyetToanDuAnService qltd_CTCV_QuyetToanDuAnService,
            IQLTD_KeHoachService qltd_KeHoachService,
            IQLTD_KeHoachCongViecService qltd_KeHoachCongViecService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IGPMB_TongQuanKetQuaService GPMB_TongQuanKetQuaService,
            IGPMB_KetQuaService GPMB_KetQuaService,
            IGPMB_KeHoachService GPMB_KeHoachService,
            IGPMB_FileService GPMB_FileService,
            IGPMB_KetQua_ChiTietService GPMB_KetQua_ChiTietService,
            IGPMB_KeHoachTienDoChungService GPMB_KeHoachTienDoChungService,
            IGPMB_CTCVService GPMB_CTCVService,
            IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IAppRoleRepository roleRepository,
            IAppUserRepository appUserRepository,
        IErrorService errorService) : base(errorService)
        {
            _baoCaoTienDoService = baoCaoTienDoService;
            _duAnService = duanService;
            _nguonvonService = nguonvonService;
            _giaidoanService = giaidoanService;
            _tinhtrangDAService = tinhtrangDAService;
            _nhomDAService = nhomDAService;
            _thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            _nguonVonDuAnService = nguonVonDuAnService;
            _keHoachVonService = keHoachVonService;
            _lapQuanLyDauTuService = lapQuanLyDauTuService;
            _QLTD_TDTH_ChuTruongDauTuService = qltd_TDTH_ChuTruongDauTuService;
            _QLTD_THCT_ChuTruongDauTuService = qltd_THCT_ChuTruongDauTuService;
            _QLTD_TDTH_ChuanBiDauTuService = qltd_TDTH_ChuanBiDauTuService;
            _QLTD_THCT_ChuanBiDauTuService = qltd_THCT_ChuanBiDauTuService;
            _QLTD_TDTH_ChuanBiThucHienService = qltd_TDTH_ChuanBiThucHienService;
            _QLTD_THCT_ChuanBiThucHienService = qltd_THCT_ChuanBiThucHienService;
            _QLTD_TDTH_ThucHienDuAnService = qltd_TDTH_ThucHienDuAnService;
            _QLTD_THCT_ThucHienDuAnService = qltd_THCT_ThucHienDuAnService;
            _QLTD_TDTH_QuyetToanDuAnService = qltd_TDTH_QuyetToanDuAnService;
            _QLTD_THCT_QuyetToanDuAnService = qltd_THCT_QuyetToanDuAnService;
            _QLTD_CTCV_ChuTruongDauTuService = qltd_CTCV_ChuTruongDauTuService;
            _QLTD_CTCV_ChuanBiDauTuService = qltd_CTCV_ChuanBiDauTuService;
            _QLTD_CTCV_ChuanBiThucHienService = qltd_CTCV_ChuanBiThucHienService;
            _QLTD_CTCV_ThucHienDuAnService = qltd_CTCV_ThucHienDuAnService;
            _QLTD_CTCV_QuyetToanDuAnService = qltd_CTCV_QuyetToanDuAnService;
            _QLTD_KeHoachService = qltd_KeHoachService;
            _QLTD_KeHoachCongViecService = qltd_KeHoachCongViecService;
            _QLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;

            _GPMB_TongQuanKetQuaService = GPMB_TongQuanKetQuaService;
            _GPMB_KetQuaService = GPMB_KetQuaService;
            _GPMB_KeHoachService = GPMB_KeHoachService;
            _GPMB_FileService = GPMB_FileService;
            _GPMB_KetQua_ChiTietService = GPMB_KetQua_ChiTietService;
            _GPMB_KeHoachTienDoChungService = GPMB_KeHoachTienDoChungService;
            _GPMB_CTCVService = GPMB_CTCVService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _appRoleRepository = roleRepository;
            _appUserRepository = appUserRepository;
            //qltd_CTCV_ChuTruongDauTuService, qltd_CTCV_ChuanBiDauTuService, qltd_CTCV_ChuanBiThucHienService, qltd_CTCV_ThucHienDuAnService, qltd_CTCV_QuyetToanDuAnService
        }

        [HttpGet]
        [Route("getKHV")]
        [Permission(Action = "Read", Function = "KEHOACHVONCACDUAN")]
        public HttpResponseMessage GetKHV(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {
                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                }

                var nguonvons = _nguonvonService.GetAll();
                var kehoachvons = _keHoachVonService.GetAllForDieuHanhKHV();
                var lapquanlydautus = _lapQuanLyDauTuService.GetAll();

                var lstDuAn = _duAnService.GetAllKHV().Select(x => new
                {
                    x.IdNhomDuAnTheoUser,
                    x.IdDuAn,
                    x.TenDuAn,
                    x.ThoiGianThucHien,
                    x.IdDuAnCha,
                    //TenNhomDuAn = (x.IdNhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",
                    x.IdNhomDuAn,
                    TenNhomDuAn = "",
                    x.HopDongs,
                    x.IdTinhTrangDuAn,
                    x.IdGiaiDoanDuAn,
                    //chưa sửa load chậm do include
                    //ListTenNguonVon = x.NguonVonDuAns.Select(nvda => nvda.NguonVon.TenNguonVon),
                    //KeHoachVons = x.KeHoachVons.Where(khv => khv.TongGiaTri != null && khv.NgayPheDuyet != null).Select(khv => new { khv.TongGiaTri, khv.NienDo, khv.NgayPheDuyet }),
                    //LapQuanLyDauTus = x.LapQuanLyDauTus.Where(lqldt => lqldt.NgayPheDuyet != null && lqldt.SoQuyetDinh != null).Select(lqldt => new { lqldt.SoQuyetDinh, lqldt.NgayPheDuyet, lqldt.TongGiaTri, lqldt.LoaiDieuChinh, lqldt.LoaiQuanLyDauTu }),
                    //Sửa lại load chậm
                    ListTenNguonVon = "",
                    KeHoachVons = kehoachvons.Where(khv => khv.TongGiaTri != null && khv.IdDuAn == x.IdDuAn && khv.NgayPheDuyet != null).Select(khv => new { khv.TongGiaTri, khv.NienDo, khv.NgayPheDuyet }),
                    LapQuanLyDauTus = lapquanlydautus.Where(lqldt => lqldt.NgayPheDuyet != null && lqldt.IdDuAn == x.IdDuAn && lqldt.SoQuyetDinh != null).Select(lqldt => new { lqldt.SoQuyetDinh, lqldt.NgayPheDuyet, lqldt.TongGiaTri, lqldt.LoaiDieuChinh, lqldt.LoaiQuanLyDauTu }),
                });

                if (idNhomDuAnTheoUser == null)
                {
                    lstDuAn = lstDuAn.ToList();
                }
                else
                {
                    lstDuAn = lstDuAn.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).ToList();
                }
                var giaidoans = _giaidoanService.GetAll();
                var tinhtrangduans = _tinhtrangDAService.GetAll();
                var thanhtoanchiphikhacs = _thanhToanChiPhiKhacService.GetThanhToan_NguonVonThanhToanCPK();
                var modelVm = new
                {
                    DuAns = (filter == null) ? lstDuAn : lstDuAn.Where(x => x.TenDuAn.Contains(filter)),
                    NguonVons = nguonvons.Select(x => new { x.IdNguonVon, x.TenNguonVon }),
                    GiaiDoans = giaidoans.Select(x => new { x.IdGiaiDoanDuAn, x.TenGiaiDoanDuAn }).OrderByDescending(x => x.IdGiaiDoanDuAn),
                    TinhTrangDuAns = tinhtrangduans.Select(x => new { x.IdTinhTrangDuAn, x.TenTinhTrangDuAn }).OrderByDescending(x => x.IdTinhTrangDuAn),
                    ThanhToanChiPhiKhacs = thanhtoanchiphikhacs.Where(x => x.NgayThanhToan != null).Select(x => new { x.IdDuAn, x.NguonVonThanhToanChiPhiKhacs, x.NgayThanhToan }),
                };
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [HttpGet]
        [Route("gettiendothuchien")]
        [Permission(Action = "Read", Function = "TIENDOTHUCHIENHOPDONG")]
        public HttpResponseMessage GetAllTienDoThucHienHopDong(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }

                var tiendothuchienhopdong = _duAnService.GetAllTienDoThucHienHopDong(idNhomDuAnTheoUser).Select(x =>
                new
                {
                    x.TenDuAn,
                    HopDongs = x.HopDongs.Select(hopdong => new
                    {
                        hopdong.TenHopDong,
                        hopdong.NgayBatDau,
                        hopdong.ThoiGianThucHien,
                        TienDoThucHiens = hopdong.TienDoThucHiens.Select(tiendothuchien => new
                        {
                            tiendothuchien.TenCongViec,
                            tiendothuchien.TuNgay,
                            tiendothuchien.DenNgay,
                            TienDoChiTiets = tiendothuchien.TienDoChiTiets.Select(tiendochitiet => new
                            {
                                tiendochitiet.NoiDung,
                                tiendochitiet.TonTaiVuongMac,
                                tiendochitiet.NgayThucHien,
                            })
                        })
                    })

                });

                response = request.CreateResponse(HttpStatusCode.OK, tiendothuchienhopdong);
                return response;

            });
        }
        [HttpGet]
        [Route("gettiendodauthau")]
        [Permission(Action = "Read", Function = "TIENDODAUTHAU")]
        public HttpResponseMessage GetTienDoDauThau(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }

                var duan = _duAnService.GetAllTienDoDauThau();

                if (idNhomDuAnTheoUser == null)
                {
                    duan = duan.ToList();
                }
                else
                {
                    duan = duan.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).ToList();
                }

                response = request.CreateResponse(HttpStatusCode.OK, duan);
                return response;

            });
        }
        [HttpGet]
        [Route("getcacduancham")]
        [Permission(Action = "Read", Function = "CACDUANCHAMQUYETTOAN")]
        public HttpResponseMessage GetCacDuAnCham(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }

                var duans = _duAnService.GetAllDuAnCham(idNhomDuAnTheoUser).Select(x => new
                {
                    x.TenDuAn,
                    QuyetToanDuAn = x.QuyetToanDuAns.OrderByDescending(quettoanduan => quettoanduan.IdQuyetToanDuAn).Select(quyettoanduan => new
                    {
                        quyettoanduan.NgayKyBienBan,
                        quyettoanduan.NgayTrinh,
                        quyettoanduan.NgayThamTra,
                        quyettoanduan.NgayPheDuyetQuyetToan,
                    }).FirstOrDefault(),
                    TenNhomDuAn = (x.NhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",

                });

                List<object> l1 = duans.ToList<object>();

                response = request.CreateResponse(HttpStatusCode.OK, duans);
                return response;

            });
        }
        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
        #region Tiến độ dự án
        #region Dự án chậm
        [HttpGet]
        [Route("getTDDDACham")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetTienDoCTDT(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                List<object> lst = new List<object>();
                List<object> CTDT = new List<object>();
                List<object> CBDT = new List<object>();
                List<object> CBTH = new List<object>();
                List<object> TH = new List<object>();
                List<object> QT = new List<object>();

                dicUserNhanVien.Clear();
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);

                if (idNhomDuAnTheoUser != 13)
                {
                    CTDT = _baoCaoTienDoService.GetList_DH_TDDA_CTDT_CDA(idUser, filter);
                    CBDT = _baoCaoTienDoService.GetList_DH_TDDA_CBDT_CDA(idUser, filter);
                    CBTH = _baoCaoTienDoService.GetList_DH_TDDA_CBTH_CDA(idUser, filter);
                    TH = _baoCaoTienDoService.GetList_DH_TDDA_TH_CDA(idUser, filter);
                }
                QT = _baoCaoTienDoService.GetList_DH_TDDA_QT_CDA(idUser, filter);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstCTDT = new List<object>();
                List<object> lstCBDT = new List<object>();
                List<object> lstCBTH = new List<object>();
                List<object> lstTH = new List<object>();
                List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstCTDT = GetListObject(CTDT, KHMN);
                    lstCBDT = GetListObject(CBDT, KHMN);
                    lstCBTH = GetListObject(CBTH, KHMN);
                    lstTH = GetListObject(TH, KHMN);
                }
                lstQT = GetListObject(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstCTDT.Count() > 0)
                    {
                        lst.Add(lstCTDT);
                    }
                    if (lstCBDT.Count() > 0)
                    {
                        lst.Add(lstCBDT);
                    }
                    if (lstCBTH.Count() > 0)
                    {
                        lst.Add(lstCBTH);
                    }
                    if (lstTH.Count() > 0)
                    {
                        lst.Add(lstTH);
                    }
                }
                if (lstQT.Count() > 0)
                {
                    lst.Add(lstQT);
                }
                response = request.CreateResponse(HttpStatusCode.OK, lst);
                return response;
            });
        }
        #endregion
        [HttpGet]
        [Route("m_getTDDDACham")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage M_GetTienDoCTDT(HttpRequestMessage request, int Numberpage, int page, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                List<object> lst = new List<object>();
                List<object> CTDT = new List<object>();
                List<object> CBDT = new List<object>();
                List<object> CBTH = new List<object>();
                List<object> TH = new List<object>();
                List<object> QT = new List<object>();

                dicUserNhanVien.Clear();
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);

                if (idNhomDuAnTheoUser != 13)
                {
                    CTDT = _baoCaoTienDoService.GetList_DH_TDDA_CTDT_CDA(idUser, filter);
                    CBDT = _baoCaoTienDoService.GetList_DH_TDDA_CBDT_CDA(idUser, filter);
                    CBTH = _baoCaoTienDoService.GetList_DH_TDDA_CBTH_CDA(idUser, filter);
                    TH = _baoCaoTienDoService.GetList_DH_TDDA_TH_CDA(idUser, filter);
                }
                QT = _baoCaoTienDoService.GetList_DH_TDDA_QT_CDA(idUser, filter);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstCTDT = new List<object>();
                List<object> lstCBDT = new List<object>();
                List<object> lstCBTH = new List<object>();
                List<object> lstTH = new List<object>();
                List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstCTDT = GetListObject(CTDT, KHMN);
                    lstCBDT = GetListObject(CBDT, KHMN);
                    lstCBTH = GetListObject(CBTH, KHMN);
                    lstTH = GetListObject(TH, KHMN);
                }
                lstQT = GetListObject(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstCTDT.Count() > 0)
                    {
                        lst.Add(lstCTDT);
                    }
                    if (lstCBDT.Count() > 0)
                    {
                        lst.Add(lstCBDT);
                    }
                    if (lstCBTH.Count() > 0)
                    {
                        lst.Add(lstCBTH);
                    }
                    if (lstTH.Count() > 0)
                    {
                        lst.Add(lstTH);
                    }
                }
                if (lstQT.Count() > 0)
                {
                    lst.Add(lstQT);
                }
                int total = lst.Count();
                lst = lst.Skip((page - 1) * Numberpage).Take(Numberpage).ToList();
                response = request.CreateResponse(HttpStatusCode.OK, new { 
                    lst,
                    total
                });
                return response;
            });
        }
        #region test
        [HttpGet]
        [Route("gettiendodauthau")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetABC(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {

                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                }

                var duan = _duAnService.GetAllTienDoDauThau();

                if (idNhomDuAnTheoUser == null)
                {
                    duan = duan.ToList();
                }
                else
                {
                    duan = duan.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser).ToList();
                }

                response = request.CreateResponse(HttpStatusCode.OK, duan);
                return response;

            });
        }
        #endregion
        #endregion


        #region KHMN
        [HttpGet]
        [Route("getkehoachmoinhat")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage Getkhmn(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                int nTrangThai = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                    //Trưởng phòng: 1, Phó Giám đốc: 2, Giám đốc: 3
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (userRoles.Contains("Trưởng phòng"))
                    {
                        nTrangThai = 1;
                    }
                    else if (userRoles.Contains("Phó giám đốc"))
                    {
                        nTrangThai = 2;
                    }
                    else if (userRoles.Contains("Giám đốc"))
                    {
                        nTrangThai = 3;
                    }
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x=> x.FullName);
                var lst = _QLTD_KeHoachService.Get_KeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(idNhomDuAnTheoUser, idUser, nTrangThai, filter, dicUserNhanVien);
                var list = lst.GroupBy(x => new { x.IDDA, x.DuAn })
                .Select(x => new GiaiDoanKeHoachMoiNhat()
                {
                    IDDA = x.Key.IDDA,
                    DA = x.Key.DuAn,
                    grpKH = x.Select(y => new KHMoiNhat()
                    {
                        IDGiaiDoan = y.IDGiaiDoan,
                        IDKH = y.IDKH,
                        KH = y.KH,
                        TinhTrang = y.TinhTrang,
                        TrangThai = y.TrangThai,
                        qltd_kehoachcongviec = y.qltd_kehoachcongviec
                    })
                });
                response = request.CreateResponse(HttpStatusCode.OK, list);
                return response;

            });
        }
        #endregion

        #region Công việc chưa hoàn thành
        [HttpGet]
        [Route("getcongviecchuahoanthanh")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage Getcvmn(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                List<object> lst = new List<object>();
                List<object> CTDT = new List<object>();
                List<object> CBDT = new List<object>();
                List<object> CBTH = new List<object>();
                List<object> TH = new List<object>();
                List<object> QT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    CTDT = _baoCaoTienDoService.GetLst_CV_CHT_CTDT(idUser);
                    CBDT = _baoCaoTienDoService.GetLst_CV_CHT_CBDT(idUser);
                    CBTH = _baoCaoTienDoService.GetLst_CV_CHT_CBTH(idUser);
                    TH = _baoCaoTienDoService.GetLst_CV_CHT_TH(idUser);
                }
                QT = _baoCaoTienDoService.GetLst_CV_CHT_QT(idUser);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstCTDT = new List<object>();
                List<object> lstCBDT = new List<object>();
                List<object> lstCBTH = new List<object>();
                List<object> lstTH = new List<object>();
                List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstCTDT = GetListObject1(CTDT, KHMN);
                    lstCBDT = GetListObject1(CBDT, KHMN);
                    lstCBTH = GetListObject1(CBTH, KHMN);
                    lstTH = GetListObject1(TH, KHMN);
                }
                lstQT = GetListObject1(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstCTDT.Count() > 0)
                    {
                        lst.Add(lstCTDT[0]);
                    }
                    if (lstCBDT.Count() > 0)
                    {
                        lst.Add(lstCBDT[0]);
                    }
                    if (lstCBTH.Count() > 0)
                    {
                        lst.Add(lstCBTH[0]);
                    }
                    if (lstTH.Count() > 0)
                    {
                        lst.Add(lstTH[0]);
                    }
                }
                if (lstQT.Count() > 0)
                {
                    lst.Add(lstQT[0]);
                }

                response = request.CreateResponse(HttpStatusCode.OK, lst);
                return response;
            });

        }
        #endregion

        #region Dùng chung
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            if (typeMethod.GetProperty(property) != null)
                return typeMethod.GetProperty(property).GetValue(objMethod, null);
            return "";
        }

        public List<object> GetListObject(List<object> l1, List<object> l2)
        {
            DateTime now = DateTime.Today;
            var l = from item in l1
                    join item1 in l2
                    on GetValueObject(item, "IDKH") equals GetValueObject(item1, "IDKeHoach")
                    select new
                    {
                        IDGiaiDoan = GetValueObject(item, "IDGiaiDoan"),
                        GiaiDoan = GetValueObject(item, "GiaiDoan"),
                        IDDA = GetValueObject(item, "IDDA"),
                        TenDuAn = GetValueObject(item, "TenDuAn"),
                        IdNhomDuAnTheoUser = GetValueObject(item, "IdNhomDuAnTheoUser"),
                        OrderDuAn = GetValueObject(item, "OrderDuAn"),
                        IDKH = GetValueObject(item, "IDKH"),
                        IDKHCV = GetValueObject(item, "IDKHCV"),
                        TuNgay = GetValueObject(item, "TuNgay"),
                        DenNgay = GetValueObject(item, "DenNgay"),
                        CV = GetValueObject(item, "CV"),
                        HoanThanh = GetValueObject(item, "HoanThanh"),
                        IDCTCV = GetValueObject(item, "IDCTCV"),
                        DuAnUsers = GetValueObject(item, "DuAnUsers")
                    };


            var lstBaoCao = (from item in l
                             group item by new { item.IDGiaiDoan, item.GiaiDoan }
                        into item1
                             select new
                             {
                                 IDGiaiDoan = item1.Key.IDGiaiDoan,
                                 GiaiDoan = item1.Key.GiaiDoan,

                                 grpDuAn = item1.GroupBy(x => new { x.IDDA, x.TenDuAn }).Select(x => new
                                 {
                                     IDDA = x.Key.IDDA,
                                     TenDuAn = x.Key.TenDuAn,
                                     IdNhomDuAnTheoUser = x.Select(oda => oda.IdNhomDuAnTheoUser).FirstOrDefault(),
                                     OrderDuAn = x.Select(oda => oda.OrderDuAn).FirstOrDefault(),
                                     DuAnUsers = string.Join("; ", ((List<string>)x.Select(x1 => x1.DuAnUsers).FirstOrDefault()).Where(y => dicUserNhanVien.ContainsKey(y)).Select(z => dicUserNhanVien[z])),
                                     grpCV = x.Select(z => new
                                     {
                                         IDKH = z.IDKH,
                                         IDKHCV = z.IDKHCV,
                                         CV = z.CV,
                                         HoanThanh = z.HoanThanh,
                                         TuNgay = z.TuNgay,
                                         DenNgay = z.DenNgay,
                                         SubDate = z.DenNgay != null ? (now.Date - Convert.ToDateTime(z.DenNgay)).Days : 0,
                                         IDCTCV = z.IDCTCV,
                                         IDGD = z.IDGiaiDoan
                                     }).ToList<object>()
                                 }).ToList<object>()
                             }).ToList<object>();

            return lstBaoCao;
        }

        public List<object> GetListObject1(List<object> l1, List<object> l2)
        {
            DateTime now = DateTime.Today;
            var l = from item in l1
                    join item1 in l2
                    on GetValueObject(item, "IDKH") equals GetValueObject(item1, "IDKeHoach")
                    select new
                    {
                        IDGiaiDoan = GetValueObject(item, "IDGiaiDoan"),
                        GiaiDoan = GetValueObject(item, "GiaiDoan"),
                        IDDA = GetValueObject(item, "IDDA"),
                        TenDuAn = GetValueObject(item, "TenDuAn"),
                        IdNhomDuAnTheoUser = GetValueObject(item, "IdNhomDuAnTheoUser"),
                        OrderDuAn = GetValueObject(item, "OrderDuAn"),
                        IDKH = GetValueObject(item, "IDKH"),
                        IDKHCV = GetValueObject(item, "IDKHCV"),
                        CV = GetValueObject(item, "CV"),
                        HoanThanh = GetValueObject(item, "HoanThanh"),
                        IDCTCV = GetValueObject(item, "IDCTCV"),
                        DuAnUsers = GetValueObject(item, "DuAnUsers")
                    };


            var lstBaoCao = (from item in l
                             group item by new { item.IDGiaiDoan, item.GiaiDoan }
                        into item1
                             select new
                             {
                                 IDGiaiDoan = item1.Key.IDGiaiDoan,
                                 GiaiDoan = item1.Key.GiaiDoan,

                                 grpDuAn = item1.GroupBy(x => new { x.IDDA, x.TenDuAn }).Select(x => new
                                 {
                                     IDDA = x.Key.IDDA,
                                     TenDuAn = x.Key.TenDuAn,
                                     IdNhomDuAnTheoUser = x.Select(oda => oda.IdNhomDuAnTheoUser).FirstOrDefault(),
                                     OrderDuAn = x.Select(oda => oda.OrderDuAn).FirstOrDefault(),
                                     DuAnUsers = string.Join("; ", ((List<string>)x.Select(x1 => x1.DuAnUsers).FirstOrDefault()).Where(y => dicUserNhanVien.ContainsKey(y)).Select(z => dicUserNhanVien[z])),
                                     grpCV = x.Select(z => new
                                     {
                                         IDKH = z.IDKH,
                                         IDKHCV = z.IDKHCV,
                                         CV = z.CV,
                                         HoanThanh = z.HoanThanh,
                                         IDCTCV = z.IDCTCV,
                                         IDGD = z.IDGiaiDoan
                                     }).ToList<object>()
                                 }).ToList<object>()
                             }).ToList<object>();

            return lstBaoCao;
        }
        #endregion

        #region Công việc chưa hoàn thành
        [HttpGet]
        [Route("getcongviecchuyenquyettoan")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCVChuyenQT(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                List<object> lst = new List<object>();
                List<object> TH = new List<object>();
                //List<object> QT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    TH = _baoCaoTienDoService.GetLst_CVChuyenQT(idUser);
                }
                //QT = _baoCaoTienDoService.GetLst_CV_CHT_QT(idUser);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstTH = new List<object>();
                //List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstTH = GetListObject(TH, KHMN);
                }
                //lstQT = GetListObject(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstTH.Count() > 0)
                    {
                        lst.Add(lstTH[0]);
                    }
                }
                //if (lstQT.Count() > 0)
                //{
                //    lst.Add(lstQT[0]);
                //}
                response = request.CreateResponse(HttpStatusCode.OK, lst);
                return response;
            });

        }
        #endregion

        [HttpGet]
        [Route("getallcongviecthaydoi")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetAllCongViecThayDoi(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int idNhomDuAnTheoUser = 0;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                var lstCBDT = _QLTD_TDTH_ChuanBiDauTuService.GetAllCTCVChuaHT(idUser, idNhomDuAnTheoUser);

                response = request.CreateResponse(HttpStatusCode.OK, lstCBDT);
                return response;

            });
        }

        [Route("khongthaydois")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage KhongThayDois(HttpRequestMessage request, List<UpdateKhongThayDoiViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (UpdateKhongThayDoiViewModels item in vmData)
                    {
                        switch (item.IdGiaiDoan)
                        {
                            case 2:
                                {
                                    var newData = new QLTD_THCT_ChuTruongDauTu();
                                    newData.IdTienDoThucHien = Convert.ToInt32(item.IdTienDoThucHien);
                                    newData.NoiDung = "Không thay đổi";
                                    newData.ThayDoi = false;
                                    newData.CreatedDate = DateTime.Now;
                                    newData.CreatedBy = User.Identity.GetUserId();
                                    newData.Status = true;

                                    _QLTD_THCT_ChuTruongDauTuService.Add(newData);
                                    _QLTD_THCT_ChuTruongDauTuService.Save();
                                    break;
                                }
                            case 3:
                                {
                                    var newData = new QLTD_THCT_ChuanBiDauTu();
                                    newData.IdTienDoThucHien = Convert.ToInt32(item.IdTienDoThucHien);
                                    newData.NoiDung = "Không thay đổi";
                                    newData.ThayDoi = false;
                                    newData.CreatedDate = DateTime.Now;
                                    newData.CreatedBy = User.Identity.GetUserId();
                                    newData.Status = true;

                                    _QLTD_THCT_ChuanBiDauTuService.Add(newData);
                                    _QLTD_THCT_ChuanBiDauTuService.Save();
                                    break;
                                }
                            case 4:
                                {
                                    var newData = new QLTD_THCT_ChuanBiThucHien();
                                    newData.IdTienDoThucHien = Convert.ToInt32(item.IdTienDoThucHien);
                                    newData.NoiDung = "Không thay đổi";
                                    newData.ThayDoi = false;
                                    newData.CreatedDate = DateTime.Now;
                                    newData.CreatedBy = User.Identity.GetUserId();
                                    newData.Status = true;

                                    _QLTD_THCT_ChuanBiThucHienService.Add(newData);
                                    _QLTD_THCT_ChuanBiThucHienService.Save();
                                    break;
                                }
                            case 5:
                                {
                                    var newData = new QLTD_THCT_ThucHienDuAn();
                                    newData.IdTienDoThucHien = Convert.ToInt32(item.IdTienDoThucHien);
                                    newData.NoiDung = "Không thay đổi";
                                    newData.ThayDoi = false;
                                    newData.CreatedDate = DateTime.Now;
                                    newData.CreatedBy = User.Identity.GetUserId();
                                    newData.Status = true;

                                    _QLTD_THCT_ThucHienDuAnService.Add(newData);
                                    _QLTD_THCT_ThucHienDuAnService.Save();
                                    break;
                                }
                            case 6:
                                {
                                    var newData = new QLTD_THCT_QuyetToanDuAn();
                                    newData.IdTienDoThucHien = Convert.ToInt32(item.IdTienDoThucHien);
                                    newData.NoiDung = "Không thay đổi";
                                    newData.ThayDoi = false;
                                    newData.CreatedDate = DateTime.Now;
                                    newData.CreatedBy = User.Identity.GetUserId();
                                    newData.Status = true;

                                    _QLTD_THCT_QuyetToanDuAnService.Add(newData);
                                    _QLTD_THCT_ThucHienDuAnService.Save();
                                    break;
                                }

                        }

                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [Route("congviechoanthanhs")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage CongViecHoanThanhs(HttpRequestMessage request, List<DieuHanhCongViecHoanThanhViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (DieuHanhCongViecHoanThanhViewModels item in vmData)
                    {
                        switch (item.IDGD)
                        {
                            case 2:
                                {
                                    var updateData = _QLTD_CTCV_ChuTruongDauTuService.GetByID(item.IDCTCV);
                                    updateData.HoanThanh = 2;
                                    updateData.NgayHoanThanh = DateTime.Now;
                                    _QLTD_CTCV_ChuTruongDauTuService.Update(updateData);
                                    _QLTD_CTCV_ChuTruongDauTuService.Save();
                                    break;
                                }
                            case 3:
                                {
                                    var updateData = _QLTD_CTCV_ChuanBiDauTuService.GetByID(item.IDCTCV);
                                    updateData.HoanThanh = 2;
                                    updateData.NgayHoanThanh = DateTime.Now;
                                    _QLTD_CTCV_ChuanBiDauTuService.Update(updateData);
                                    _QLTD_CTCV_ChuanBiDauTuService.Save();
                                    break;
                                }
                            case 4:
                                {
                                    var updateData = _QLTD_CTCV_ChuanBiThucHienService.GetByID(item.IDCTCV);
                                    updateData.HoanThanh = 2;
                                    updateData.NgayHoanThanh = DateTime.Now;
                                    _QLTD_CTCV_ChuanBiThucHienService.Update(updateData);
                                    _QLTD_CTCV_ChuanBiThucHienService.Save();
                                    break;
                                }
                            case 5:
                                {
                                    var updateData = _QLTD_CTCV_ThucHienDuAnService.GetByID(item.IDCTCV);
                                    updateData.HoanThanh = 2;
                                    updateData.NgayHoanThanh = DateTime.Now;
                                    _QLTD_CTCV_ThucHienDuAnService.Update(updateData);
                                    _QLTD_CTCV_ThucHienDuAnService.Save();
                                    break;
                                }
                            case 6:
                                {
                                    var updateData = _QLTD_CTCV_QuyetToanDuAnService.GetByID(item.IDCTCV);
                                    updateData.HoanThanh = 2;
                                    updateData.NgayHoanThanh = DateTime.Now;
                                    _QLTD_CTCV_QuyetToanDuAnService.Update(updateData);
                                    _QLTD_CTCV_QuyetToanDuAnService.Save();
                                    break;
                                }
                        }
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [HttpGet]
        [Route("getngaykhongnhap")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetNgayKhongNhap(HttpRequestMessage request, string tungay, string denngay)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstCBDT = _QLTD_KeHoachCongViecService.GetAllKHCVTheoNgay(dicUserNhanVien, tungay, denngay);

                response = request.CreateResponse(HttpStatusCode.OK, lstCBDT);
                return response;

            });
        }

        [Route("pheduyetnhieukehoach")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage PheDuyetNhieuKeHoach(HttpRequestMessage request, List<DieuHanhKeHoachPheDuyetViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    foreach (var kh in vmData)
                    {
                        var updateKeHoach = _QLTD_KeHoachService.GetById(kh.IDKH);
                        updateKeHoach.PheDuyet = true;
                        int trangthaiUpdate = -1;
                        int tinhtrangUpdate = 0;
                        string title = "Dự án " + _QLTD_KeHoachService.GetById(kh.IDKH).DuAn.TenDuAn;
                        string body = "Kế hoạch " + updateKeHoach.NoiDung;
                        List<string> listUserId = new List<string>();

                        switch (kh.TrangThai)
                        {
                            case 1:// CB đã gửi lên cho TP
                                {
                                    if (idNhomDuAnTheoUser == 13)
                                    {
                                        trangthaiUpdate = 3;
                                        tinhtrangUpdate = 3;
                                    }
                                    else
                                    {
                                        trangthaiUpdate = 2;
                                        tinhtrangUpdate = 2;
                                    }
                                    body += " đã được phê duyệt bởi trưởng phòng";
                                    var phoGiamDocId = GetPhoGiamDocIdByDuAnId(updateKeHoach.IdDuAn);
                                    listUserId.Add(phoGiamDocId);
                                    break;
                                }
                            case 2:// Trưởng phòng đã chấp thuận
                                {

                                    trangthaiUpdate = 3;
                                    tinhtrangUpdate = 3;
                                    body += " đã được phê duyệt bởi phó giám đốc";
                                    var giamDocId = GetGiamDocIdByDuAnId(updateKeHoach.IdDuAn);
                                    listUserId.Add(giamDocId);
                                    break;
                                }
                            case 3:// Phó giám đốc đã chấp thuận
                                {
                                    trangthaiUpdate = 4;
                                    tinhtrangUpdate = 4;
                                    listUserId = GetTruongPhongIdByDuAnId(updateKeHoach.IdDuAn);
                                    listUserId.Add(GetPhoGiamDocIdByDuAnId(updateKeHoach.IdDuAn));
                                    break;
                                }
                        }
                        updateKeHoach.TrangThai = trangthaiUpdate;
                        updateKeHoach.TinhTrang = tinhtrangUpdate;
                        updateKeHoach.NgayTinhTrang = DateTime.Now;
                        updateKeHoach.UpdatedDate = DateTime.Now;
                        updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                        _QLTD_KeHoachService.Update(updateKeHoach);
                        _QLTD_KeHoachService.Save();

                        if (trangthaiUpdate == 4)
                        {
                            QLTD_KeHoachTienDoChung newTDTHChung = new QLTD_KeHoachTienDoChung();
                            newTDTHChung.IdKeHoach = kh.IDKH;
                            _QLTD_KeHoachTienDoChungService.Add(newTDTHChung);
                            _QLTD_KeHoachTienDoChungService.Save();

                            foreach (var item in kh.qltd_kehoachcongviec)
                            {
                                switch (Convert.ToInt32(kh.IdGiaiDoan))
                                {
                                    case 2:
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuTruongDauTu();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuTruongDauTuService.Add(newCTCV);
                                            _QLTD_CTCV_ChuTruongDauTuService.Save();
                                            break;
                                        }
                                    case 3:
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuanBiDauTu();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuanBiDauTuService.Add(newCTCV);
                                            _QLTD_CTCV_ChuanBiDauTuService.Save();
                                            break;
                                        }
                                    case 4:
                                        {
                                            var newCTCV = new QLTD_CTCV_ChuanBiThucHien();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ChuanBiThucHienService.Add(newCTCV);
                                            _QLTD_CTCV_ChuanBiThucHienService.Save();
                                            break;
                                        }
                                    case 5:
                                        {
                                            var newCTCV = new QLTD_CTCV_ThucHienDuAn();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_ThucHienDuAnService.Add(newCTCV);
                                            _QLTD_CTCV_ThucHienDuAnService.Save();
                                            break;
                                        }
                                    case 6:
                                        {
                                            var newCTCV = new QLTD_CTCV_QuyetToanDuAn();
                                            newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                            newCTCV.IdKeHoachCongViec = Convert.ToInt32(item.IdKeHoachCongViec);
                                            newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                            newCTCV.CreatedDate = DateTime.Now;
                                            newCTCV.CreatedBy = User.Identity.GetUserId();
                                            newCTCV.Status = true;
                                            _QLTD_CTCV_QuyetToanDuAnService.Add(newCTCV);
                                            _QLTD_CTCV_QuyetToanDuAnService.Save();
                                            break;
                                        }
                                }
                            }
                        }

                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [Route("khongpheduyetnhieukehoach")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage KhongPheDuyetNhieuKeHoach(HttpRequestMessage request, List<DieuHanhKeHoachPheDuyetViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    foreach (var kh in vmData)
                    {
                        var updateKeHoach = _QLTD_KeHoachService.GetById(kh.IDKH);
                        updateKeHoach.PheDuyet = false;
                        int tinhtrangUpdate = kh.TrangThai;
                        int trangthaiUpdate = 0;

                        string title = "Dự án " + _duAnService.GetById(updateKeHoach.IdDuAn).TenDuAn;
                        string body = "Kế hoạch " + updateKeHoach.NoiDung;
                        List<string> listUserId = new List<string>();

                        var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                        string nameRole;
                        if (!roleTemp.Contains("Trưởng phòng"))
                        {
                            listUserId = GetTruongPhongIdByDuAnId(updateKeHoach.IdDuAn);
                        }
                        if (updateKeHoach.CreatedBy != User.Identity.GetUserId())
                        {
                            listUserId.Add(updateKeHoach.CreatedBy);
                        }
                        if (roleTemp.Contains("Trưởng phòng"))
                        {
                            nameRole = "Trưởng phòng";
                        }
                        else if (roleTemp.Contains("Phó giám đốc"))
                        {
                            nameRole = "Phó giám đốc";
                        }
                        else
                        {
                            nameRole = "Giám đốc";
                        }
                        body += " đã không được phê duyệt bởi " + nameRole;
                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }

                        updateKeHoach.TrangThai = trangthaiUpdate;
                        updateKeHoach.TinhTrang = tinhtrangUpdate;
                        updateKeHoach.NgayTinhTrang = DateTime.Now;
                        updateKeHoach.UpdatedDate = DateTime.Now;
                        updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                        _QLTD_KeHoachService.Update(updateKeHoach);
                        _QLTD_KeHoachService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }


                return response;
            });
        }

        [HttpGet]
        [Route("getcvdathuchienngaydenngay")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCVDaThucHienNgayDenNgay(HttpRequestMessage request, string tungay, string denngay)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstData = _QLTD_KeHoachCongViecService.GetCVDaThucHienNgayDenNgay(dicUserNhanVien, tungay, denngay);

                response = request.CreateResponse(HttpStatusCode.OK, lstData);
                return response;

            });
        }

        [HttpGet]
        [Route("getcvhoanthanhchamngaydenngay")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCVHoanThanhChamNgayDenNgay(HttpRequestMessage request, string tungay, string denngay)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstData = _QLTD_KeHoachCongViecService.GetCVHoanThanhChamNgayDenNgay(dicUserNhanVien, tungay, denngay);

                response = request.CreateResponse(HttpStatusCode.OK, lstData);
                return response;

            });
        }

        [HttpGet]
        [Route("getcvchamchuahoanthanh")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCVChamChuaHoanThanh(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstData = _QLTD_KeHoachCongViecService.GetCVChamChuaHoanThanhNgayDenNgay(dicUserNhanVien);

                response = request.CreateResponse(HttpStatusCode.OK, lstData);
                return response;

            });
        }

        [HttpGet]
        [Route("banlamviec_gpmb_tonghopketqua")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_GPMB_TongHopKetQua(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                //Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}

                var lstData = _GPMB_KetQuaService.GetGPMB_BanLamViec(filter);

                response = request.CreateResponse(HttpStatusCode.OK, lstData);
                return response;

            });
        }

        [HttpGet]
        [Route("banlamviec_gpmb_congviecdangthuchien")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_GPMB_CongViecDangThucHien(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var lstData = _GPMB_KetQuaService.GetBC_GPMB_CongViecDangThucHien(idUser);
                response = request.CreateResponse(HttpStatusCode.OK, lstData);
                return response;

            });
        }

        [Route("khongthaydoicongviecgpmbs")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage KhongThayDoiCongViecGPMBs(HttpRequestMessage request, List<UpdateKhongThayDoiCongViecGPMBViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (UpdateKhongThayDoiCongViecGPMBViewModels item in vmData)
                    {
                        var newData = new GPMB_KetQua_ChiTiet();
                        newData.IdGPMBKetQua = Convert.ToInt32(item.IDCV);
                        newData.NoiDung = "Không thay đổi";
                        newData.CreatedDate = DateTime.Now;
                        newData.CreatedBy = User.Identity.GetUserId();
                        newData.Status = true;

                        _GPMB_KetQua_ChiTietService.Add(newData);
                        _GPMB_KetQua_ChiTietService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [HttpGet]
        [Route("banlamviec_gpmb_pheduyetkehoachtiendo")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_GPMB_PheDuyetKeHoachTienDo(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                int nTrangThai = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                    //Trưởng phòng: 1, Phó Giám đốc: 2, Giám đốc: 3
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (userRoles.Contains("Trưởng phòng"))
                    {
                        nTrangThai = 1;
                    }
                    else if (userRoles.Contains("Phó giám đốc"))
                    {
                        nTrangThai = 2;
                    }
                    //else if (userRoles.Contains("Giám đốc"))
                    //{
                    //    nTrangThai = 3;
                    //}
                }

                var lst = _GPMB_KeHoachService.Get_KeHoachMoiNhat_DuoiMucPhoGiamDocPheDuyet_DieuHanh(idNhomDuAnTheoUser, idUser, nTrangThai, filter);

                var list = lst.GroupBy(x => new { x.IDDA, x.DuAn })
                .Select(x => new DuAnKHMoiNhat()
                {
                    IDDA = x.Key.IDDA,
                    DA = x.Key.DuAn,
                    grpKHPD = x.Select(y => new KHPDMoiNhat()
                    {
                        IDKH = y.IDKH,
                        KH = y.KH,
                        TinhTrang = y.TinhTrang,
                        TrangThai = y.TrangThai,
                        gpmb_kehoachcongviec = y.gpmb_kehoachcongviec
                    })
                });

                response = request.CreateResponse(HttpStatusCode.OK, list);
                return response;

            });
        }

        [Route("pheduyetnhieukehoachtiendo")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage PheDuyetNhieuKeHoachTienDo(HttpRequestMessage request, List<DieuHanh_KeHoachTienDoPheDuyet_ViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    foreach (var kh in vmData)
                    {
                        var updateKeHoach = _GPMB_KeHoachService.GetById(kh.IDKH);
                        updateKeHoach.PheDuyet = true;
                        int trangthaiUpdate = -1;
                        int tinhtrangUpdate = 0;

                        string title = "Dự án " + _duAnService.GetById(kh.IDKH).TenDuAn;
                        string body = "Kế hoạch " + updateKeHoach.NoiDung;
                        List<string> listUserId = new List<string>();

                        switch (kh.TrangThai)
                        {
                            case -1:// cán bộ đang soạn
                                {
                                    trangthaiUpdate = 1;
                                    tinhtrangUpdate = 1;
                                    body += " đang chờ phê duyệt";
                                    listUserId = GetTruongPhongIdByDuAnId(updateKeHoach.IdDuAn);
                                    break;
                                }
                            case 0:// Kế hoạch không được phê duyệt
                                {
                                    var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                    if (roleTemp.Contains("Trưởng phòng"))
                                    {
                                        trangthaiUpdate = 2;
                                        tinhtrangUpdate = 2;
                                        listUserId.Add(GetPhoGiamDocIdByDuAnId(updateKeHoach.IdDuAn));
                                    }
                                    else
                                    {
                                        trangthaiUpdate = 1;
                                        tinhtrangUpdate = 1;
                                        listUserId = GetTruongPhongIdByDuAnId(updateKeHoach.IdDuAn);
                                    }
                                    body += " đang chờ phê duyệt";
                                    break;
                                }
                            case 1:// CB đã gửi lên cho TP
                                {
                                    if (idNhomDuAnTheoUser == 13)
                                    {
                                        trangthaiUpdate = 3;
                                        tinhtrangUpdate = 3;
                                    }
                                    else
                                    {
                                        trangthaiUpdate = 2;
                                        tinhtrangUpdate = 2;
                                    }
                                    body += " đã được phê duyệt bởi trưởng phòng";
                                    var phoGiamDocId = GetPhoGiamDocIdByDuAnId(updateKeHoach.IdDuAn);
                                    listUserId.Add(phoGiamDocId);
                                    break;
                                }
                            case 2:// Trưởng phòng đã chấp thuận
                                {

                                    trangthaiUpdate = 3;
                                    tinhtrangUpdate = 3;
                                    body += " đã được phê duyệt bởi phó giám đốc";
                                    var giamDocId = GetGiamDocIdByDuAnId(updateKeHoach.IdDuAn);
                                    listUserId.Add(giamDocId);
                                    break;
                                }

                                //case 3:// Phó giám đốc đã chấp thuận
                                //    {
                                //        trangthaiUpdate = 4;
                                //        tinhtrangUpdate = 4;
                                //        break;
                                //    }
                        }

                        updateKeHoach.TrangThai = trangthaiUpdate;
                        updateKeHoach.TinhTrang = tinhtrangUpdate;
                        updateKeHoach.NgayTinhTrang = DateTime.Now;
                        updateKeHoach.UpdatedDate = DateTime.Now;
                        updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                        _GPMB_KeHoachService.Update(updateKeHoach);
                        _GPMB_KeHoachService.Save();

                        if (trangthaiUpdate == 3)
                        {
                            GPMB_KeHoachTienDoChung newTDTHChung = new GPMB_KeHoachTienDoChung();
                            newTDTHChung.IdKeHoachGPMB = kh.IDKH;
                            _GPMB_KeHoachTienDoChungService.Add(newTDTHChung);
                            _GPMB_KeHoachTienDoChungService.Save();

                            foreach (var item in kh.gpmb_kehoachcongviec)
                            {
                                var newCTCV = new GPMB_CTCV();
                                //newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                                newCTCV.IdKeHoachCongViecGPMB = Convert.ToInt32(item.IdKeHoachCongViec);
                                //newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                                newCTCV.CreatedDate = DateTime.Now;
                                newCTCV.CreatedBy = User.Identity.GetUserId();
                                newCTCV.Status = true;
                                _GPMB_CTCVService.Add(newCTCV);
                                _GPMB_CTCVService.Save();
                            }
                        }
                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [Route("khongpheduyetnhieukehoachtiendo")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage KhongPheDuyetNhieuKeHoachTienDo(HttpRequestMessage request, List<DieuHanh_KeHoachTienDoPheDuyet_ViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    foreach (var kh in vmData)
                    {
                        var updateKeHoach = _GPMB_KeHoachService.GetById(kh.IDKH);
                        updateKeHoach.PheDuyet = false;
                        int tinhtrangUpdate = kh.TrangThai;
                        int trangthaiUpdate = 0;

                        updateKeHoach.TrangThai = trangthaiUpdate;
                        updateKeHoach.TinhTrang = tinhtrangUpdate;
                        updateKeHoach.NgayTinhTrang = DateTime.Now;
                        updateKeHoach.UpdatedDate = DateTime.Now;
                        updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                        _GPMB_KeHoachService.Update(updateKeHoach);
                        _GPMB_KeHoachService.Save();

                        string title = "Dự án " + _duAnService.GetById(updateKeHoach.IdDuAn).TenDuAn;
                        string body = "Kế hoạch " + updateKeHoach.NoiDung;
                        List<string> listUserId = new List<string>();

                        var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                        string nameRole;
                        if (!roleTemp.Contains("Trưởng phòng"))
                        {
                            listUserId = GetTruongPhongIdByDuAnId(updateKeHoach.IdDuAn);
                        }
                        if (updateKeHoach.CreatedBy != User.Identity.GetUserId())
                        {
                            listUserId.Add(updateKeHoach.CreatedBy);
                        }
                        if (roleTemp.Contains("Trưởng phòng"))
                        {
                            nameRole = "Trưởng phòng";
                        }
                        else if (roleTemp.Contains("Phó giám đốc"))
                        {
                            nameRole = "Phó giám đốc";
                        }
                        else
                        {
                            nameRole = "Giám đốc";
                        }
                        body += " đã không được phê duyệt bởi " + nameRole;

                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [HttpGet]
        [Route("banlamviec_gpmb_pheduyetdangkikehoach")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_GPMB_PheDuyetDangKiKeHoach(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                int nTrangThai = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                    //Trưởng phòng: 1, Phó Giám đốc: 2, Giám đốc: 3
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (userRoles.Contains("Trưởng phòng"))
                    {
                        nTrangThai = 1;
                    }
                    else if (userRoles.Contains("Phó giám đốc"))
                    {
                        nTrangThai = 2;
                    }
                    //else if (userRoles.Contains("Giám đốc"))
                    //{
                    //    nTrangThai = 3;
                    //}
                }

                var lst = _GPMB_FileService.Get_GPMB_File_ChuaPheDuyet(nTrangThai, filter);

                response = request.CreateResponse(HttpStatusCode.OK, lst);
                return response;

            });
        }

        [Route("pheduyetnhieudangkykehoach")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage PheDuyetNhieuDangKyKeHoach(HttpRequestMessage request, List<DieuHanh_DangKyKeHoachPheDuyet_ViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                    foreach (var kh in vmData)
                    {
                        var updateEntity = _GPMB_FileService.GetById(kh.IDFile);
                        int trangthaiUpdate = -1;
                        int tinhtrangUpdate = 0;

                        string title = "Dự án " + _duAnService.GetById(kh.IDFile).TenDuAn;
                        string body = "Kế hoạch " + kh.NoiDung;
                        List<string> listUserId = new List<string>();

                        switch (kh.TrangThai)
                        {
                            case -1:// cán bộ đang soạn
                                {
                                    trangthaiUpdate = 1;
                                    tinhtrangUpdate = 1;
                                    body += " đang chờ phê duyệt";
                                    listUserId = GetTruongPhongIdByDuAnId(kh.IDFile);
                                    break;
                                }
                            case 0: // File không được phê duyệt
                                {
                                    var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                    if (roleTemp.Contains("Trưởng phòng"))
                                    {
                                        tinhtrangUpdate = 1;
                                        listUserId.Add(GetPhoGiamDocIdByDuAnId(kh.IDFile));
                                    }
                                    else if (roleTemp.Contains("Phó giám đốc"))
                                    {
                                        tinhtrangUpdate = 2;
                                        listUserId = GetTruongPhongIdByDuAnId(kh.IDFile);
                                    }
                                    body += " đang chờ phê duyệt";
                                    break;
                                }
                            case 1:// CB đã gửi lên cho TP
                                {
                                    {
                                        trangthaiUpdate = 2;
                                        tinhtrangUpdate = 2;
                                    }
                                    body += " đã được phê duyệt bởi trưởng phòng";
                                    var phoGiamDocId = GetPhoGiamDocIdByDuAnId(kh.IDFile);
                                    listUserId.Add(phoGiamDocId);
                                    break;
                                }
                            case 2:// Trưởng phòng đã chấp thuận
                                {

                                    trangthaiUpdate = 3;
                                    tinhtrangUpdate = 3;
                                    body += " đã được phê duyệt bởi phó giám đốc";
                                    var giamDocId = GetGiamDocIdByDuAnId(kh.IDFile);
                                    listUserId.Add(giamDocId);
                                    break;
                                }
                        }
                        updateEntity.TrangThai = trangthaiUpdate;
                        updateEntity.TinhTrang = tinhtrangUpdate;
                        updateEntity.UpdatedDate = updateEntity.NgayTinhTrang = DateTime.Now;
                        updateEntity.UpdatedBy = User.Identity.GetUserId();
                        _GPMB_FileService.Update(updateEntity);
                        _GPMB_FileService.Save();

                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }
                    }

                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        [Route("khongpheduyetnhieudangkykehoach")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage KhongPheDuyetNhieuDangKyKeHoach(HttpRequestMessage request, List<DieuHanh_DangKyKeHoachPheDuyet_ViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    foreach (var kh in vmData)
                    {
                        var updateKeHoach = _GPMB_FileService.GetById(kh.IDFile);
                        updateKeHoach.PheDuyet = false;
                        int tinhtrangUpdate = kh.TrangThai;
                        int trangthaiUpdate = -1;

                        updateKeHoach.TrangThai = trangthaiUpdate;
                        updateKeHoach.TinhTrang = tinhtrangUpdate;
                        updateKeHoach.NgayTinhTrang = DateTime.Now;
                        updateKeHoach.UpdatedDate = DateTime.Now;
                        updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                        _GPMB_FileService.Update(updateKeHoach);
                        _GPMB_FileService.Save();

                        string title = "Dự án " + _duAnService.GetById(kh.IDFile).TenDuAn;
                        string body = "Kế hoạch " + updateKeHoach.NoiDung;
                        List<string> listUserId = new List<string>();

                        var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                        string nameRole;
                        if (!roleTemp.Contains("Trưởng phòng"))
                        {
                            listUserId = GetTruongPhongIdByDuAnId(kh.IDFile);
                        }
                        if (updateKeHoach.CreatedBy != User.Identity.GetUserId())
                        {
                            listUserId.Add(updateKeHoach.CreatedBy);
                        }
                        if (roleTemp.Contains("Trưởng phòng"))
                        {
                            nameRole = "Trưởng phòng";
                        }
                        else if (roleTemp.Contains("Phó giám đốc"))
                        {
                            nameRole = "Phó giám đốc";
                        }
                        else
                        {
                            nameRole = "Giám đốc";
                        }
                        body += " đã không được phê duyệt bởi " + nameRole;
                        if (listUserId.Count() > 0)
                        {
                            _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                        }
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }

        // Duy
        [HttpGet]
        [Route("getcountkehoachmoinhat")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCountkhmn(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                int nTrangThai = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                    //Trưởng phòng: 1, Phó Giám đốc: 2, Giám đốc: 3
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (userRoles.Contains("Trưởng phòng"))
                    {
                        nTrangThai = 1;
                    }
                    else if (userRoles.Contains("Phó giám đốc"))
                    {
                        nTrangThai = 2;
                    }
                    else if (userRoles.Contains("Giám đốc"))
                    {
                        nTrangThai = 3;
                    }
                }

                var lst = _QLTD_KeHoachService.Get_CountKeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(idNhomDuAnTheoUser, idUser, nTrangThai, filter);
                var count = lst.Count();
                response = request.CreateResponse(HttpStatusCode.OK, count);
                return response;

            });
        }
        [HttpGet]
        [Route("getcountallcongviecthaydoi")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCountAllCongViecThayDoi(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int idNhomDuAnTheoUser = 0;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                var lstCBDT = _QLTD_TDTH_ChuanBiDauTuService.GetCountAllCTCVChuaHT(idUser, idNhomDuAnTheoUser);

                response = request.CreateResponse(HttpStatusCode.OK, lstCBDT);
                return response;
            });
        }

        #region Công việc chưa hoàn thành
        [HttpGet]
        [Route("getcuontcongviecchuahoanthanh")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCuontcvmn(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var countGrpDuan = 0;
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users.Where(x => x.IdNhomDuAnTheoUser != null && x.IdNhomDuAnTheoUser != 14).ToList();

                var users = _appUserRepository.GetAllUserOther14().ToList();
                //var roles = AppRoleManager.Roles.ToList();
                //foreach (AppUser user in users)
                //{
                    //var roleTemp = AppUserManager.GetRoles(user.Id).ToList();
                    //if (roleTemp.Contains("Nhân viên"))
                    //{
                    //    dicUserNhanVien.Add(user.Id, user.FullName);
                    //}
                    //if(_appRoleRepository.CheckRole(user.Id, "Nhân viên"))
                    //{
                    //    dicUserNhanVien.Add(user.Id, user.FullName);
                    //}
                //}
                List<object> lst = new List<object>();
                List<object> CTDT = new List<object>();
                List<object> CBDT = new List<object>();
                List<object> CBTH = new List<object>();
                List<object> TH = new List<object>();
                List<object> QT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    CTDT = _baoCaoTienDoService.GetLst_CV_CHT_CTDT(idUser);
                    CBDT = _baoCaoTienDoService.GetLst_CV_CHT_CBDT(idUser);
                    CBTH = _baoCaoTienDoService.GetLst_CV_CHT_CBTH(idUser);
                    TH = _baoCaoTienDoService.GetLst_CV_CHT_TH(idUser);
                }
                QT = _baoCaoTienDoService.GetLst_CV_CHT_QT(idUser);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstCTDT = new List<object>();
                List<object> lstCBDT = new List<object>();
                List<object> lstCBTH = new List<object>();
                List<object> lstTH = new List<object>();
                List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstCTDT = GetListObject1(CTDT, KHMN);
                    lstCBDT = GetListObject1(CBDT, KHMN);
                    lstCBTH = GetListObject1(CBTH, KHMN);
                    lstTH = GetListObject1(TH, KHMN);
                }
                lstQT = GetListObject(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstCTDT.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCTDT[0].GetType().GetProperty("grpDuAn").GetValue(lstCTDT[0])).Cast<object>().ToList().Count();
                    }
                    if (lstCBDT.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCBDT[0].GetType().GetProperty("grpDuAn").GetValue(lstCBDT[0])).Cast<object>().ToList().Count();
                    }
                    if (lstCBTH.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCBTH[0].GetType().GetProperty("grpDuAn").GetValue(lstCBTH[0])).Cast<object>().ToList().Count();
                    }
                    if (lstTH.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstTH[0].GetType().GetProperty("grpDuAn").GetValue(lstTH[0])).Cast<object>().ToList().Count();
                    }
                }
                if (lstQT.Count() > 0)
                {
                    countGrpDuan += ((IEnumerable)lstQT[0].GetType().GetProperty("grpDuAn").GetValue(lstQT[0])).Cast<object>().ToList().Count();
                }
                response = request.CreateResponse(HttpStatusCode.OK, countGrpDuan);

                return response;
            });

        }
        #endregion
        #region Dự án chậm
        [HttpGet]
        [Route("getCountTDDDACham")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetCountTienDoCTDT(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var countGrpDuan = 0;
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                List<object> lst = new List<object>();
                List<object> CTDT = new List<object>();
                List<object> CBDT = new List<object>();
                List<object> CBTH = new List<object>();
                List<object> TH = new List<object>();
                List<object> QT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    CTDT = _baoCaoTienDoService.GetList_DH_TDDA_CTDT_CDA(idUser, filter);
                    CBDT = _baoCaoTienDoService.GetList_DH_TDDA_CBDT_CDA(idUser, filter);
                    CBTH = _baoCaoTienDoService.GetList_DH_TDDA_CBTH_CDA(idUser, filter);
                    TH = _baoCaoTienDoService.GetList_DH_TDDA_TH_CDA(idUser, filter);
                }
                QT = _baoCaoTienDoService.GetList_DH_TDDA_QT_CDA(idUser, filter);

                List<object> KHMN = _baoCaoTienDoService.GetList_DH_TDDA_KHMN(idUser);
                List<object> lstCTDT = new List<object>();
                List<object> lstCBDT = new List<object>();
                List<object> lstCBTH = new List<object>();
                List<object> lstTH = new List<object>();
                List<object> lstQT = new List<object>();
                if (idNhomDuAnTheoUser != 13)
                {
                    lstCTDT = GetListObject(CTDT, KHMN);
                    lstCBDT = GetListObject(CBDT, KHMN);
                    lstCBTH = GetListObject(CBTH, KHMN);
                    lstTH = GetListObject(TH, KHMN);
                }
                lstQT = GetListObject(QT, KHMN);
                if (idNhomDuAnTheoUser != 13)
                {
                    if (lstCTDT.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCTDT[0].GetType().GetProperty("grpDuAn").GetValue(lstCTDT[0])).Cast<object>().ToList().Count();
                    }
                    if (lstCBDT.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCBDT[0].GetType().GetProperty("grpDuAn").GetValue(lstCBDT[0])).Cast<object>().ToList().Count();
                    }
                    if (lstCBTH.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstCBTH[0].GetType().GetProperty("grpDuAn").GetValue(lstCBTH[0])).Cast<object>().ToList().Count();
                    }
                    if (lstTH.Count() > 0)
                    {
                        countGrpDuan += ((IEnumerable)lstTH[0].GetType().GetProperty("grpDuAn").GetValue(lstTH[0])).Cast<object>().ToList().Count();
                    }
                }
                if (lstQT.Count() > 0)
                {
                    countGrpDuan += ((IEnumerable)lstQT[0].GetType().GetProperty("grpDuAn").GetValue(lstQT[0])).Cast<object>().ToList().Count();
                }
                response = request.CreateResponse(HttpStatusCode.OK, countGrpDuan);
                return response;
            });
        }
        #endregion
        [HttpGet]
        [Route("getpagecongviecthaydoi")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetPageCongViecThayDoi(HttpRequestMessage request, int Numberpage, int page, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int idNhomDuAnTheoUser = 0;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstCBDT = _QLTD_TDTH_ChuanBiDauTuService.GetPageCTCVChuaHT(idUser, idNhomDuAnTheoUser, Numberpage, page, filter, dicUserNhanVien);
                response = request.CreateResponse(HttpStatusCode.OK, lstCBDT);
                return response;

            });
        }
        [HttpGet]
        [Route("getpagekehoachmoinhat")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GePagetkhmn(HttpRequestMessage request, string filter, int Numberpage, int page)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                int nTrangThai = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (userRoles.Contains("Trưởng phòng"))
                    {
                        nTrangThai = 1;
                    }
                    else if (userRoles.Contains("Phó giám đốc"))
                    {
                        nTrangThai = 2;
                    }
                    else if (userRoles.Contains("Giám đốc"))
                    {
                        nTrangThai = 3;
                    }
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lst = _QLTD_KeHoachService.Get_KeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(idNhomDuAnTheoUser, idUser, nTrangThai, filter, dicUserNhanVien);
                var list = lst.GroupBy(x => new { x.IDDA, x.DuAn, x.IdNhomDuAnTheoUser, x.DuAnUsers })
                .Select(x => new GiaiDoanKeHoachMoiNhat()
                {
                    IDDA = x.Key.IDDA,
                    DA = x.Key.DuAn,
                    IdNhomDuAnTheoUser = x.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = x.Key.DuAnUsers,
                    grpKH = x.Select(y => new KHMoiNhat()
                    {
                        IDGiaiDoan = y.IDGiaiDoan,
                        IDKH = y.IDKH,
                        KH = y.KH,
                        TinhTrang = y.TinhTrang,
                        TrangThai = y.TrangThai,
                        qltd_kehoachcongviec = y.qltd_kehoachcongviec
                    })
                });
                var results = (list.OrderBy(x => x.IDDA)
             .Skip((page - 1) * Numberpage)
             .Take(Numberpage));
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", list.Count() % Numberpage > 0 ? ((list.Count() / Numberpage) + 1) : (list.Count() / Numberpage));
                dictionary2.Add("TotalItems", list.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", results);
                dictionary.Add("Page", dictionary2);
                //return dictionary;
                response = request.CreateResponse(HttpStatusCode.OK, dictionary);
                return response;

            });
        }

        [HttpGet]
        [Route("banlamviec_count_gpmb_congviecdangthuchien")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_Count_GPMB_CongViecDangThucHien(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var lstData = _GPMB_KetQuaService.GetBC_GPMB_CongViecDangThucHien(idUser);
                var count = lstData.Count();
                response = request.CreateResponse(HttpStatusCode.OK, count);
                return response;

            });
        }

        [HttpGet]
        [Route("banlamviec_page_gpmb_congviecdangthuchien")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_Page_GPMB_CongViecDangThucHien(HttpRequestMessage request, int Numberpage, int page, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }
                dicUserNhanVien.Clear();
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstData = _GPMB_KetQuaService.Page_GetBC_GPMB_CongViecDangThucHien(idUser, filter, dicUserNhanVien);
                var results = (lstData.OrderBy(x => x.IDDA)
              .Skip((page - 1) * Numberpage)
              .Take(Numberpage));
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", lstData.Count() % Numberpage > 0 ? ((lstData.Count() / Numberpage) + 1) : (lstData.Count() / Numberpage));
                dictionary2.Add("TotalItems", lstData.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", results);
                dictionary.Add("Page", dictionary2);
                response = request.CreateResponse(HttpStatusCode.OK, dictionary);
                return response;

            });
        }

        [HttpGet]
        [Route("banlamvie_congviecchoduyet")]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage BanLamViec_CongViecChoDuyet(HttpRequestMessage request, int Numberpage, int page, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }
                dicUserNhanVien.Clear();
                dicUserNhanVien = _appRoleRepository.GetUserRoleNhanVien().ToDictionary(x => x.UserId, x => x.FullName);
                var lstData = _GPMB_KetQuaService.Page_GetBC_GPMB_CongViecDangThucHien(idUser, filter, dicUserNhanVien);
                var results = (lstData.OrderBy(x => x.IDDA)
                .Skip((page - 1) * Numberpage)
                .Take(Numberpage));
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", lstData.Count() % Numberpage > 0 ? ((lstData.Count() / Numberpage) + 1) : (lstData.Count() / Numberpage));
                dictionary2.Add("TotalItems", lstData.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", results);
                dictionary.Add("Page", dictionary2);
                response = request.CreateResponse(HttpStatusCode.OK, dictionary);
                return response;

            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }

    }
}
