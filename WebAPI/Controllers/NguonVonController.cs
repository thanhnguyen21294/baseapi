﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/nguonvon")]
    [Authorize]
    public class NguonVonController : ApiControllerBase
    {
        private INguonVonService _nguonVonService;
        private IDeleteService _deleteService;
        public NguonVonController(IErrorService errorService, INguonVonService nguonVonService, IDeleteService deleteService) : base(errorService)
        {
            this._nguonVonService = nguonVonService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nguonVonService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<NguonVon>, IEnumerable<NguonVonViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NGUONVON")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _nguonVonService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<NguonVon>, IEnumerable<NguonVonViewModels>>(model);

                PaginationSet<NguonVonViewModels> pagedSet = new PaginationSet<NguonVonViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NGUONVON")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nguonVonService.GetById(id);

                var responseData = Mapper.Map<NguonVon, NguonVonViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "NGUONVON")]
        public HttpResponseMessage Create(HttpRequestMessage request, NguonVonViewModels nguonVonViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newNguonVon = new NguonVon();
                    newNguonVon.UpdateNguonVon(nguonVonViewModels);
                    newNguonVon.CreatedDate = DateTime.Now;
                    newNguonVon.CreatedBy = User.Identity.GetUserId();
                    _nguonVonService.Add(newNguonVon);
                    _nguonVonService.Save();

                    var responseData = Mapper.Map<NguonVon, NguonVonViewModels>(newNguonVon);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "NGUONVON")]
        public HttpResponseMessage Update(HttpRequestMessage request, NguonVonViewModels nguonVonViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbNguonVon = _nguonVonService.GetById(nguonVonViewModels.IdNguonVon);

                    dbNguonVon.UpdateNguonVon(nguonVonViewModels);
                    dbNguonVon.UpdatedDate = DateTime.Now;
                    dbNguonVon.UpdatedBy = User.Identity.GetUserId();
                    _nguonVonService.Update(dbNguonVon);
                    _nguonVonService.Save();

                    var responseData = Mapper.Map<NguonVon, NguonVonViewModels>(dbNguonVon);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NGUONVON")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nguonVonService.Delete(id);
                    _nguonVonService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NGUONVON")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listNguonVon = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listNguonVon)
                    {
                        _deleteService.DeleteNguonVon(item);
                    }

                    _deleteService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listNguonVon.Count);
                }

                return response;
            });
        }
    }
}
