﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.DuAn;
using WebAPI.Providers;
namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/tiendothuchien")]
    public class TienDoThucHienController : ApiControllerBase
    {
        private ITienDoThucHienService _TienDoThucHienService;
        private ITienDoChiTietService _TienDoChiTietService;
        public TienDoThucHienController(IErrorService errorService, ITienDoThucHienService tienDoThucHienService, ITienDoChiTietService tienDoChiTietService) : base(errorService)
        {
            _TienDoThucHienService = tienDoThucHienService;
            _TienDoChiTietService = tienDoChiTietService;
        }
        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _TienDoThucHienService.GetAll(filter).OrderByDescending(x => x.IdTienDo);

                var modelVm = Mapper.Map<IEnumerable<TienDoThucHien>, IEnumerable<TienDoThucHienViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _TienDoThucHienService.GetAll().OrderByDescending(x => x.IdTienDo);

                var modelVm = Mapper.Map<IEnumerable<TienDoThucHien>, IEnumerable<TienDoThucHienViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _TienDoThucHienService.GetByFilter(idDA, page, pageSize, "", out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);

                PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _TienDoThucHienService.GetById(id);

                var responseData = Mapper.Map<TienDoThucHien, TienDoThucHienViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, TienDoThucHienViewModels vmTienDoThucHien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newTienDoThucHien = new TienDoThucHien();
                    newTienDoThucHien.IdDuAn = vmTienDoThucHien.IdDuAn;
                    newTienDoThucHien.IdGoiThau = vmTienDoThucHien.IdGoiThau;
                    if (vmTienDoThucHien.TuNgay != "" && vmTienDoThucHien.TuNgay != null)
                        newTienDoThucHien.TuNgay = DateTime.ParseExact(vmTienDoThucHien.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmTienDoThucHien.DenNgay != "" && vmTienDoThucHien.DenNgay != null)
                        newTienDoThucHien.DenNgay = DateTime.ParseExact(vmTienDoThucHien.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newTienDoThucHien.IdHopDong = vmTienDoThucHien.IdHopDong;
                    newTienDoThucHien.TenCongViec = vmTienDoThucHien.TenCongViec;
                    newTienDoThucHien.GhiChu = vmTienDoThucHien.GhiChu;
                    newTienDoThucHien.UpdatedDate = DateTime.Now;
                    newTienDoThucHien.UpdatedBy = User.Identity.GetUserId();
                    newTienDoThucHien.Status = true;
                    newTienDoThucHien.CreatedDate = DateTime.Now;
                    newTienDoThucHien.CreatedBy = User.Identity.GetUserId();
                    //try
                    //{
                    //    List<TienDoChiTiet> lstTienDoChiTiets = new List<TienDoChiTiet>();
                    //    foreach (var item in vmTienDoThucHien.TienDoChiTiets)
                    //    {
                    //        TienDoChiTiet addTDCT = new TienDoChiTiet();
                    //        addTDCT.NoiDung = item.NoiDung;
                    //        if (item.NgayThucHien != "" && item.NgayThucHien != null)
                    //            addTDCT.NgayThucHien = Convert.ToDateTime(item.NgayThucHien);
                    //        addTDCT.IdTienDo = vmTienDoThucHien.IdTienDo;
                    //        addTDCT.TonTaiVuongMac = item.TonTaiVuongMac;
                    //        addTDCT.GhiChu = item.GhiChu;

                    //        lstTienDoChiTiets.Add(addTDCT);
                    //    }

                    //    newTienDoThucHien.TienDoChiTiets = lstTienDoChiTiets;
                    //}
                    //catch (Exception e)
                    //{
                    //    return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    //}
                    _TienDoThucHienService.Add(newTienDoThucHien);
                    _TienDoThucHienService.Save();

                    var responseData = Mapper.Map<TienDoThucHien, TienDoThucHienViewModels>(newTienDoThucHien);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, TienDoThucHienViewModels vmTienDoThucHien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateTienDoThucHien = _TienDoThucHienService.GetById(vmTienDoThucHien.IdTienDo);
                    updateTienDoThucHien.IdDuAn = vmTienDoThucHien.IdDuAn;
                    updateTienDoThucHien.IdGoiThau = vmTienDoThucHien.IdGoiThau;
                    if (vmTienDoThucHien.TuNgay != "" && vmTienDoThucHien.TuNgay != null)
                        updateTienDoThucHien.TuNgay = DateTime.ParseExact(vmTienDoThucHien.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmTienDoThucHien.DenNgay != "" && vmTienDoThucHien.DenNgay != null)
                        updateTienDoThucHien.DenNgay = DateTime.ParseExact(vmTienDoThucHien.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateTienDoThucHien.IdHopDong = vmTienDoThucHien.IdHopDong;
                    updateTienDoThucHien.TenCongViec = vmTienDoThucHien.TenCongViec;
                    updateTienDoThucHien.GhiChu = vmTienDoThucHien.GhiChu;
                    updateTienDoThucHien.UpdatedDate = DateTime.Now;
                    updateTienDoThucHien.UpdatedBy = User.Identity.GetUserId();

                    //try
                    //{
                    //    List<int> curentTienDoChiTiets = _TienDoChiTietService.GetbyIdTienDoThucHien(vmTienDoThucHien.IdTienDo).Select(x => x.IdTienDoChiTiet).ToList();
                    //    List<int> newTienDoChiTiets = vmTienDoThucHien.TienDoChiTiets.Select(x => x.IdTienDoChiTiet).ToList(); ;

                    //    List<int> lstIDUpdate = curentTienDoChiTiets.Intersect(newTienDoChiTiets).ToList();
                    //    if(lstIDUpdate.Count > 0)
                    //    {
                    //        foreach (int id in lstIDUpdate)
                    //        {
                    //            var newDataUpdate = vmTienDoThucHien.TienDoChiTiets.Where(x => x.IdTienDoChiTiet == id).FirstOrDefault();
                    //            var itemUpdate = _TienDoChiTietService.GetById(id);
                    //            itemUpdate.NoiDung = newDataUpdate.NoiDung;
                    //            if (newDataUpdate.NgayThucHien != "" && newDataUpdate.NgayThucHien != null)
                    //                itemUpdate.NgayThucHien = Convert.ToDateTime(newDataUpdate.NgayThucHien);
                    //            itemUpdate.IdTienDo = vmTienDoThucHien.IdTienDo;
                    //            itemUpdate.TonTaiVuongMac = newDataUpdate.TonTaiVuongMac;
                    //            itemUpdate.GhiChu = newDataUpdate.GhiChu;
                    //            _TienDoChiTietService.Update(itemUpdate);
                    //        }
                    //        _TienDoChiTietService.Save();
                    //    }
                    //    List<int> lstIDDel = curentTienDoChiTiets.Except(newTienDoChiTiets).ToList();
                    //    if (lstIDDel.Count > 0)
                    //    {
                    //        foreach (int id in lstIDDel)
                    //        {
                    //            _TienDoChiTietService.Delete(id);
                    //        }
                    //        _TienDoChiTietService.Save();
                    //    }
                    //    List<int> lstIDAdd = newTienDoChiTiets.Except(curentTienDoChiTiets).ToList();
                    //    if (lstIDAdd.Count > 0)
                    //    {
                    //        foreach (int id in lstIDAdd)
                    //        {
                    //            TienDoChiTiet addTDCT = new TienDoChiTiet();
                    //            var itemAdd = vmTienDoThucHien.TienDoChiTiets.Where(x => x.IdTienDoChiTiet == id).FirstOrDefault();
                    //            addTDCT.NoiDung = itemAdd.NoiDung;
                    //            if (itemAdd.NgayThucHien != "" && itemAdd.NgayThucHien != null)
                    //                addTDCT.NgayThucHien = Convert.ToDateTime(itemAdd.NgayThucHien);
                    //            addTDCT.IdTienDo = vmTienDoThucHien.IdTienDo;
                    //            addTDCT.TonTaiVuongMac = itemAdd.TonTaiVuongMac;
                    //            addTDCT.GhiChu = itemAdd.GhiChu;

                    //            _TienDoChiTietService.Add(addTDCT);
                    //        }
                    //        _TienDoChiTietService.Save();
                    //    }
                        
                    //}
                    //catch (Exception e)
                    //{
                    //    return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    //}


                    _TienDoThucHienService.Update(updateTienDoThucHien);
                    _TienDoThucHienService.Save();

                    var responseData = Mapper.Map<TienDoThucHien, TienDoThucHienViewModels>(updateTienDoThucHien);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _TienDoThucHienService.Delete(id);
                    _TienDoThucHienService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TIENDOTHUHIENHOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lstKeHoachVon = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in lstKeHoachVon)
                    {
                        _TienDoChiTietService.DeleteByIdTienDoThucHien(item);
                        _TienDoThucHienService.Delete(item);
                    }
                    _TienDoThucHienService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, lstKeHoachVon.Count);
                }

                return response;
            });
        }
    }
}
