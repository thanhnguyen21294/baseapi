﻿using Model.Models;
using Model.Models.QLTD;
using Service;
using Service.GPMBService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/convertfile")]
    public class ConvertFileToHTMLController : ApiControllerBase
    {
        private IConvertFileToHTMLService _ConvertFileToHTMLService;
        private IVanBanDuAnService _vbdaService;
        private IVanBanDenService _vanBanDenService;
        private IBaoCaoTableService _baoCaoTableService;
        private ICapNhatKLNTService _capNhatKLNTService;
        private IGPMB_FileService _gpmb_FileService;

        public ConvertFileToHTMLController(IErrorService errorService,
            IConvertFileToHTMLService ConvertFileToHTMLService,
            IVanBanDuAnService vbdaService,
            IBaoCaoTableService baoCaoTableService,
            ICapNhatKLNTService capNhatKLNTService,
            IVanBanDenService vanBanDenService,
            IGPMB_FileService gpmb_FileService) : base(errorService)
        {
            this._ConvertFileToHTMLService = ConvertFileToHTMLService;
            this._vbdaService = vbdaService;
            this._baoCaoTableService = baoCaoTableService;
            this._capNhatKLNTService = capNhatKLNTService;
            _vanBanDenService = vanBanDenService;
            _gpmb_FileService = gpmb_FileService;

        }

        [Route("convert/{vbdaId}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "CONVERTFILE")]
        public HttpResponseMessage ConvertFile(HttpRequestMessage request, int vbdaId)
        {
            VanBanDuAn vbda = this._vbdaService.GetById(vbdaId);

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(vbda.FileUrl));

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                object model = _ConvertFileToHTMLService.ConvertFile(path, vbda.FileName);
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });

        }

        [Route("convertvanbanden/{vbdenId}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "CONVERTFILE")]
        public HttpResponseMessage ConvertFileVanBanDen(HttpRequestMessage request, int vbdenId)
        {
            VanBanDen vbda = this._vanBanDenService.GetById(vbdenId);
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(vbda.FileUrl));
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                object model = _ConvertFileToHTMLService.ConvertFile(path, vbda.FileName);
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });

        }
        [Route("convertBC/{baocaoId}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "CONVERTFILE")]
        public HttpResponseMessage ConvertFileBaoCao(HttpRequestMessage request, int baocaoId)
        {
            BaoCaoTable baocao = this._baoCaoTableService.GetById(baocaoId);

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(baocao.URL));

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                object model = _ConvertFileToHTMLService.ConvertFile(path, baocao.Ten);
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });

        }

        [Route("convertKLNT/{KLNTId}")]
        [HttpGet]
        public HttpResponseMessage ConvertFileKLNT(HttpRequestMessage request, int KLNTId)
        {
            CapNhatKLNT capNhat = this._capNhatKLNTService.GetById(KLNTId);

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(capNhat.FileUrl));

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                object model = _ConvertFileToHTMLService.ConvertFile(path, capNhat.FileName);
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });

        }

        [Route("gpmb/{Id}")]
        [HttpGet]
        public HttpResponseMessage ConvertGPMBFile(HttpRequestMessage request, int Id)
        {
            string fileURL = _gpmb_FileService.GetById(Id).FileURL;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~" + fileURL));

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                object model = _ConvertFileToHTMLService.ConvertFile(path, fileURL.Substring(fileURL.LastIndexOf("/") + 1));
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

    }
}
