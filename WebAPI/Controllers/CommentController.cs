﻿using AutoMapper;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models;
using Model.Models;
using WebAPI.Providers;
using Microsoft.AspNet.Identity;
using WebAPI.SignalR;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/comment")]
    [Authorize]
    public class CommentController : ApiControllerBase
    {
        private ICommentService _commentService;

        public CommentController(IErrorService errorService, ICommentService commentService) : base(errorService)
        {
            this._commentService = commentService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _commentService.GetAll().OrderBy(x => x.CreatedDate);

                var modelVm = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COMMENT")]
        public HttpResponseMessage GetAllName(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _commentService.GetByIdDuAn(idDuAn).OrderBy(x => x.CreatedDate);
                var modelVm = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COMMENT")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _commentService.GetById(id);
                var responseData = Mapper.Map<Comment, CommentViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, CommentViewModels vmModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newModel = new Comment();
                    newModel.IdDuAn = vmModel.IdDuAn;
                    newModel.UserId = User.Identity.GetUserId();
                    newModel.NoiDung = vmModel.NoiDung;
                    newModel.Status = true;
                    newModel.CreatedDate = DateTime.Now;
                    newModel.CreatedBy = User.Identity.GetUserId();
                    _commentService.Add(newModel);
                    _commentService.Save();

                    //SignalR
                    //push notification
                    var modelHub = _commentService.GetById(newModel.IdComment);
                    QLDAHub.PushCommentToAllUsers(Mapper.Map<Comment, CommentViewModels>(modelHub), null);
                    // end.

                    var responseData = Mapper.Map<Comment, CommentViewModels>(newModel);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "COMMENT")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var modelHubDelete = _commentService.GetById(id);
                    var modelHub = new Comment();
                    modelHub.IdDuAn = modelHubDelete.IdDuAn;

                    _commentService.Delete(id);
                    _commentService.Save();
                    
                    QLDAHub.PushCommentToAllUsers(Mapper.Map<Comment, CommentViewModels>(modelHub), null);
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }
    }
}
