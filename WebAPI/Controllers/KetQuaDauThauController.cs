﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/ketquadauthau")]
    [Authorize]
    public class KetQuaDauThauController : ApiControllerBase
    {
        private IKetQuaDauThauService _ketQuaDauThauService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        public KetQuaDauThauController(IErrorService errorService, IKetQuaDauThauService ketQuaDauThauService, IDanhMucNhaThauService danhMucNhaThauService) : base(errorService)
        {
            this._ketQuaDauThauService = ketQuaDauThauService;
            this._danhMucNhaThauService = danhMucNhaThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ketQuaDauThauService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<KetQuaDauThau>, IEnumerable<KetQuaDauThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _ketQuaDauThauService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);

                PaginationSet<GoiThauViewModels> pagedSet = new PaginationSet<GoiThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ketQuaDauThauService.GetById(id);

                var responseData = Mapper.Map<KetQuaDauThau, KetQuaDauThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, KetQuaDauThauViewModels ketQuaDauThautVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newKetQuaDauThau = new KetQuaDauThau();
                    newKetQuaDauThau.UpdateKetQuaDauThau(ketQuaDauThautVm);
                    newKetQuaDauThau.CreatedDate = DateTime.Now;
                    newKetQuaDauThau.CreatedBy = User.Identity.GetUserId();
                    try
                    {
                        var query = _danhMucNhaThauService.GetAll(ketQuaDauThautVm.IdDuAn).Where(x =>
                            x.TenNhaThau.Contains(ketQuaDauThautVm.DanhMucNhaThau.TenNhaThau));
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = ketQuaDauThautVm.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = ketQuaDauThautVm.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = ketQuaDauThautVm.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            newKetQuaDauThau.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhmuc = query.SingleOrDefault();
                            newKetQuaDauThau.IdNhaThau = danhmuc.IdNhaThau;
                            danhmuc.DiaChiNhaThau = ketQuaDauThautVm.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch(Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    
                    _ketQuaDauThauService.Add(newKetQuaDauThau);
                    _ketQuaDauThauService.Save();
                    var responseData = Mapper.Map<KetQuaDauThau, KetQuaDauThauViewModels>(newKetQuaDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, KetQuaDauThauViewModels ketQuaDauThauVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbKetQuaDauThau = _ketQuaDauThauService.GetById(ketQuaDauThauVm.IdKetQuaLuaChonNhaThau);

                    dbKetQuaDauThau.UpdateKetQuaDauThau(ketQuaDauThauVm);
                    dbKetQuaDauThau.UpdatedDate = DateTime.Now;
                    dbKetQuaDauThau.UpdatedBy = User.Identity.GetUserId();
                    _ketQuaDauThauService.Update(dbKetQuaDauThau);
                    try
                    {
                        var query = _danhMucNhaThauService.GetAll(ketQuaDauThauVm.IdDuAn)
                            .Where(x => x.TenNhaThau == ketQuaDauThauVm.DanhMucNhaThau.TenNhaThau);
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = ketQuaDauThauVm.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = ketQuaDauThauVm.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = ketQuaDauThauVm.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            dbKetQuaDauThau.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhMuc = query.SingleOrDefault();
                            dbKetQuaDauThau.IdNhaThau = danhMuc.IdNhaThau;
                            danhMuc.DiaChiNhaThau = ketQuaDauThauVm.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _ketQuaDauThauService.Save();

                    var responseData = Mapper.Map<KetQuaDauThau, KetQuaDauThauViewModels>(dbKetQuaDauThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _ketQuaDauThauService.Delete(id);
                    _ketQuaDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KETQUADAUTHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listKetQuaDauThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listKetQuaDauThau)
                    {
                        _ketQuaDauThauService.Delete(item);
                    }

                    _ketQuaDauThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listKetQuaDauThau.Count);
                }

                return response;
            });
        }
    }
}
