﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/goithau")]
    [Authorize]
    public class GoiThauController : ApiControllerBase
    {
        private IGoiThauService _goiThauRepository;
        private INguonVonDuAnGoiThauService _nguonVonDuAnGoiThauService;
        private INguonVonDuAnGoiThauService _nguonVonDuAnGoiThauRepository;
        private IHoSoMoiThauService _hoSoMoiThauService;
        private IHopDongService _hopDongService;
        private IDeleteService _deleteService;
        public GoiThauController(IErrorService errorService, IDeleteService deleteService, IHopDongService hopDongService, IGoiThauService goiThauService, INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService, IHoSoMoiThauService hoSoMoiThauService, INguonVonDuAnGoiThauService nguonVonDuAnGoiThauRepository) : base(errorService)
        {
            this._goiThauRepository = goiThauService;
            this._nguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            this._nguonVonDuAnGoiThauRepository = nguonVonDuAnGoiThauRepository;
            this._hoSoMoiThauService = hoSoMoiThauService;
            this._hopDongService = hopDongService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _goiThauRepository.GetAll().OrderByDescending(x => x.IdGoiThau);

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallid")]
        [HttpGet]
        public HttpResponseMessage GetAllArrayID(HttpRequestMessage request, string idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var listid = new JavaScriptSerializer().Deserialize<List<int>>(idKH);
                List<GoiThau> gt = new List<GoiThau>();
                foreach(var item in listid)
                {
                    var qr = _goiThauRepository.GetAllID(item);
                    if (qr.Count() > 0)
                    {
                        foreach(var item2 in qr)
                        {
                            gt.Add(item2);
                        }
                    }
                }

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(gt);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }


        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,string idKeHoach, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var listid = new JavaScriptSerializer().Deserialize<List<int>>(idKeHoach);
                List<GoiThau> gt = new List<GoiThau>();
                foreach (var item in listid)
                {
                    var qr = _goiThauRepository.GetAllID(item);
                    if (qr.Count() > 0)
                    {
                        foreach (var item2 in qr)
                        {
                            gt.Add(item2);
                        }
                    }
                }
                
                
                if (filter != null)
                {
                    gt = gt.Where(x => x.TenGoiThau.Contains(filter)).ToList();
                }
                totalRow = gt.Count();
                gt = gt.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                
                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(gt);

                PaginationSet<GoiThauViewModels> pagedSet = new PaginationSet<GoiThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _goiThauRepository.GetByIdDuAn(idDA);
                
                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getByHoSoMoiThau")]
        [HttpGet]
        public HttpResponseMessage GetByHoSoMoiThau(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _goiThauRepository.GetByHoSoMoiThau(idDuAn);

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _goiThauRepository.GetById(id);

                var responseData = Mapper.Map<GoiThau, GoiThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, GoiThauViewModels vmgoiThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newGoiThau = new GoiThau();
                    newGoiThau.UpdateGoiThau(vmgoiThauViewModels);
                    newGoiThau.CreatedDate = DateTime.Now;
                    newGoiThau.CreatedBy = User.Identity.GetUserId();
                    try
                    {
                        List<NguonVonDuAnGoiThau> listNguonVonDuAnGoiThaus = new List<NguonVonDuAnGoiThau>();
                        foreach (var item in vmgoiThauViewModels.NguonVonDuAnGoiThaus)
                        {
                            listNguonVonDuAnGoiThaus.Add(new NguonVonDuAnGoiThau()
                            {
                                IdNguonVonDuAn = item.IdNguonVonDuAn
                            });
                        }
                        newGoiThau.NguonVonDuAnGoiThaus = listNguonVonDuAnGoiThaus;
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _goiThauRepository.Add(newGoiThau);
                    _goiThauRepository.Save();

                    var responseData = Mapper.Map<GoiThau, GoiThauViewModels>(newGoiThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, GoiThauViewModels vmGoiThauViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateGoiThau = _goiThauRepository.GetById(vmGoiThauViewModels.IdGoiThau);

                    updateGoiThau.UpdateGoiThau(vmGoiThauViewModels);
                    updateGoiThau.UpdatedDate = DateTime.Now;
                    updateGoiThau.UpdatedBy = User.Identity.GetUserId();
                    try
                    {
                        var listNguonVonDuAnGoiThauOld =
                                        _nguonVonDuAnGoiThauService.GetListByIdGoiThau(vmGoiThauViewModels.IdGoiThau);
                        var listDeleteNguonVon = listNguonVonDuAnGoiThauOld.Where(x =>
                            !vmGoiThauViewModels.NguonVonDuAnGoiThaus.Select(y => y.IdNguonVonDuAnGoiThau)
                                .Contains(x.IdNguonVonDuAnGoiThau));
                        foreach (var nv in listDeleteNguonVon)
                        {
                            _nguonVonDuAnGoiThauService.Delete(nv.IdNguonVonDuAnGoiThau);
                        }
                        foreach (var nguonVonDuAnGoiThau in vmGoiThauViewModels.NguonVonDuAnGoiThaus)
                        {
                            if (listNguonVonDuAnGoiThauOld.Where(x => x.IdNguonVonDuAnGoiThau == nguonVonDuAnGoiThau.IdNguonVonDuAnGoiThau).Count() == 0)
                            {
                                _nguonVonDuAnGoiThauService.Add(new NguonVonDuAnGoiThau()
                                {
                                    IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn,
                                    IdGoiThau = vmGoiThauViewModels.IdGoiThau
                                });
                            }
                            else
                            {
                                var editNguonVonDuAnGoiThau = _nguonVonDuAnGoiThauService.GetById(nguonVonDuAnGoiThau.IdNguonVonDuAnGoiThau);
                                editNguonVonDuAnGoiThau.IdNguonVonDuAn = nguonVonDuAnGoiThau.IdNguonVonDuAn;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _goiThauRepository.Save();

                    var responseData = Mapper.Map<GoiThau, GoiThauViewModels>(updateGoiThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _goiThauRepository.Delete(id);
                    _goiThauRepository.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHLUACHONNHATHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                //var b1 = _nguonVonDuAnGoiThauService.GetById(1);
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDGoiThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDGoiThau)
                    {
                      _deleteService.DeleteGoiThau(item);
                    }
                    _deleteService.Save();
                    //var response1 = _goiThauRepository.GetAll();
                    response = request.CreateResponse(HttpStatusCode.Created, listIDGoiThau);
                }

                return response;
            });
        }
    }
}
