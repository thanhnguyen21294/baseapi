﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/duanhosoquyettoan")]
    [Authorize]
    public class DuAnHoSoQuyetToanController : ApiControllerBase
    {
        private IDuAnHoSoQuyetToanService _DuAnHoSoQuyetToanService;
        public DuAnHoSoQuyetToanController(IErrorService errorService, IDuAnHoSoQuyetToanService DuAnHoSoQuyetToanService) : base(errorService)
        {
            this._DuAnHoSoQuyetToanService = DuAnHoSoQuyetToanService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _DuAnHoSoQuyetToanService.GetAll().OrderByDescending(x => x.IdDuAnHoSoQuyetToan);

                var modelVm = Mapper.Map<IEnumerable<DuAnHoSoQuyetToan>, IEnumerable<DuAnHoSoQuyetToanViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyidda/{idDA}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUANHOSOQUYETTOAN")]
        public HttpResponseMessage GetByIdDA(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _DuAnHoSoQuyetToanService.GetByIdDA(idDA);

                var gpr = model.Where(x => x.DanhMucHoSoQuyetToan.IdParent == null).Select(x => new
                {
                    x.IdDanhMucHoSoQuyetToan,
                    x.DanhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan,
                    x.DanhMucHoSoQuyetToan.Order,
                    x.KyThuat,
                    x.KeToan,
                    x.HoanThien,
                    DanhMucCha = model.Where(y => y.DanhMucHoSoQuyetToan.IdParent == x.IdDanhMucHoSoQuyetToan).Select(y => new
                    {
                        y.IdDanhMucHoSoQuyetToan,
                        y.DanhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan,
                        y.DanhMucHoSoQuyetToan.Order,
                        y.KyThuat,
                        y.KeToan,
                        y.HoanThien,
                        DanhMucCon = model.Where(z => z.DanhMucHoSoQuyetToan.IdParent == y.IdDanhMucHoSoQuyetToan).Select(z => new
                        {
                            z.IdDanhMucHoSoQuyetToan,
                            z.DanhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan,
                            z.DanhMucHoSoQuyetToan.Order,
                            z.KyThuat,
                            z.KeToan,
                            z.HoanThien,
                            DanhMucCon = model.Where(zz => zz.DanhMucHoSoQuyetToan.IdParent == z.IdDanhMucHoSoQuyetToan).Select(zz => new
                            {
                                zz.IdDanhMucHoSoQuyetToan,
                                zz.DanhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan,
                                zz.DanhMucHoSoQuyetToan.Order,
                                zz.KyThuat,
                                zz.KeToan,
                                zz.HoanThien,
                            }).OrderBy(zz => zz.Order)
                        }).OrderBy(z => z.Order)
                    }).OrderBy(y => y.Order)
                });
                var response = request.CreateResponse(HttpStatusCode.OK, gpr);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUANHOSOQUYETTOAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _DuAnHoSoQuyetToanService.GetById(id);

                var responseData = Mapper.Map<DuAnHoSoQuyetToan, DuAnHoSoQuyetToanViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUANHOSOQUYETTOAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, List<DuAnHoSoQuyetToanViewModels> lstData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var lst = lstData;// new JavaScriptSerializer().Deserialize<List<DuAnHoSoQuyetToanViewModels>>(lstData);
                    if(lst.Count > 0)
                    {
                        //kiểm tra xem đã thêm các hồ sơ vào dự án chưa
                        bool check = _DuAnHoSoQuyetToanService.CheckExistedDuAn(lst[0].IdDuAn);
                        if (check)
                        {
                            foreach (var item in lst)
                            {
                                var updateData = _DuAnHoSoQuyetToanService.GetByIdTwoKey(item.IdDuAn, item.IdDanhMucHoSoQuyetToan);
                                updateData.KyThuat = item.KyThuat != null ? (int)item.KyThuat : 0;
                                updateData.KeToan = item.KeToan != null ? (int)item.KeToan : 0;
                                if (item.HoanThien != "" && item.HoanThien != null)
                                {
                                    updateData.HoanThien = DateTime.ParseExact(item.HoanThien, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                                }
                                updateData.UpdatedDate = DateTime.Now;
                                updateData.UpdatedBy = User.Identity.GetUserId();
                                _DuAnHoSoQuyetToanService.Update(updateData);
                            }
                            _DuAnHoSoQuyetToanService.Save();
                        }
                        else
                        {
                            foreach(var item in lst)
                            {
                                var newDuAnHoSoQuyetToan = new DuAnHoSoQuyetToan();
                                newDuAnHoSoQuyetToan.IdDanhMucHoSoQuyetToan = item.IdDanhMucHoSoQuyetToan;
                                newDuAnHoSoQuyetToan.IdDuAn = item.IdDuAn;
                                newDuAnHoSoQuyetToan.KyThuat = item.KyThuat != null ? (int)item.KyThuat : 0;
                                newDuAnHoSoQuyetToan.KeToan = item.KeToan != null ? (int)item.KeToan : 0;
                                if (item.HoanThien != "" && item.HoanThien != null)
                                {
                                    newDuAnHoSoQuyetToan.HoanThien = DateTime.ParseExact(item.HoanThien, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                                }
                                newDuAnHoSoQuyetToan.CreatedDate = DateTime.Now;
                                newDuAnHoSoQuyetToan.CreatedBy = User.Identity.GetUserId();
                                _DuAnHoSoQuyetToanService.Add(newDuAnHoSoQuyetToan);
                            }
                            _DuAnHoSoQuyetToanService.Save();
                        }
                    }
                   
                    
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }

                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUANHOSOQUYETTOAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _DuAnHoSoQuyetToanService.Delete(id);
                    _DuAnHoSoQuyetToanService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUANHOSOQUYETTOAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listDuAnHoSoQuyetToan = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listDuAnHoSoQuyetToan)
                    {
                        _DuAnHoSoQuyetToanService.Delete(item);
                    }

                    _DuAnHoSoQuyetToanService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listDuAnHoSoQuyetToan.Count);
                }

                return response;
            });
        }
    }
}
