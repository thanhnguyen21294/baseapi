﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb_dangkykehoach")]
    [Authorize]
    public class GPMB_DangKyKeHoachController : ApiControllerBase
    {
        private IGPMB_DangKyKeHoachService _GPMB_DangKyKeHoachService;
        private IDeleteService _deleteService;
        public GPMB_DangKyKeHoachController(
            IErrorService errorService,
            IGPMB_DangKyKeHoachService qLTD_CTCV_ChuanBiDauTuService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_DangKyKeHoachService = qLTD_CTCV_ChuanBiDauTuService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_DangKyKeHoachService.GetAll(idDuAn);
                var modelVm = Mapper.Map<IEnumerable<GPMB_DangKyKeHoach>, IEnumerable<GPMB_DangKyKeHoachViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_DangKyKeHoachService.GetById(id);
                var modelVm = Mapper.Map<GPMB_DangKyKeHoach, GPMB_DangKyKeHoachViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_DangKyKeHoachViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   

                    var newData = new GPMB_DangKyKeHoach();
                    newData.IdDuAn = vmData.IdDuAn;
                    newData.QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh = vmData.QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh;
                    newData.QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh = vmData.QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh;
                    newData.HopHD_HopDUXMoRong_NgayHoanThanh = vmData.HopHD_HopDUXMoRong_NgayHoanThanh;
                    newData.QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh = vmData.QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh;
                    newData.TBThuHoiDat_NgayHoanThanh = vmData.TBThuHoiDat_NgayHoanThanh;

                    newData.HopDanPhatToKhai_NgayHoanThanh = vmData.HopDanPhatToKhai_NgayHoanThanh;
                    newData.DieuTraKeKhaiLapBBGPMB_NgayHoanThanh = vmData.DieuTraKeKhaiLapBBGPMB_NgayHoanThanh;
                    newData.CuongCheKDBB_NgayHoanThanh = vmData.CuongCheKDBB_NgayHoanThanh;
                    newData.LapHSGPMB_NgayHoanThanh = vmData.LapHSGPMB_NgayHoanThanh;
                    newData.LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh = vmData.LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh;
                    newData.CongKhaiKTCongKhaiDTPA_NgayHoanThanh = vmData.CongKhaiKTCongKhaiDTPA_NgayHoanThanh;
                    newData.TrinhHDTDHoanThienPACT_NgayHoanThanh = vmData.TrinhHDTDHoanThienPACT_NgayHoanThanh;
                    newData.QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh = vmData.QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh;
                    newData.ChiTraTien_NhanBGD_NgayHoanThanh = vmData.ChiTraTien_NhanBGD_NgayHoanThanh;
                    newData.CuongCheThuHoiDat_NgayHoanThanh = vmData.CuongCheThuHoiDat_NgayHoanThanh;

                    newData.QuyetToanKPTHGPMB_NgayHoanThanh = vmData.QuyetToanKPTHGPMB_NgayHoanThanh;
                    newData.LapHSDNTPGiaoDat_NgayHoanThanh = vmData.LapHSDNTPGiaoDat_NgayHoanThanh;

                    newData.GhiChu = vmData.GhiChu;

                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;

                    _GPMB_DangKyKeHoachService.Add(newData);
                    _GPMB_DangKyKeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_DangKyKeHoach, GPMB_DangKyKeHoachViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_DangKyKeHoachViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_DangKyKeHoachService.GetById(vmData.IdDangKyKeHoach);
                    updateData.QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh = vmData.QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh;
                    updateData.QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh = vmData.QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh;
                    updateData.HopHD_HopDUXMoRong_NgayHoanThanh = vmData.HopHD_HopDUXMoRong_NgayHoanThanh;
                    updateData.QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh = vmData.QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh;
                    updateData.TBThuHoiDat_NgayHoanThanh = vmData.TBThuHoiDat_NgayHoanThanh;

                    updateData.HopDanPhatToKhai_NgayHoanThanh = vmData.HopDanPhatToKhai_NgayHoanThanh;
                    updateData.DieuTraKeKhaiLapBBGPMB_NgayHoanThanh = vmData.DieuTraKeKhaiLapBBGPMB_NgayHoanThanh;
                    updateData.CuongCheKDBB_NgayHoanThanh = vmData.CuongCheKDBB_NgayHoanThanh;
                    updateData.LapHSGPMB_NgayHoanThanh = vmData.LapHSGPMB_NgayHoanThanh;
                    updateData.LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh = vmData.LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh;
                    updateData.CongKhaiKTCongKhaiDTPA_NgayHoanThanh = vmData.CongKhaiKTCongKhaiDTPA_NgayHoanThanh;
                    updateData.TrinhHDTDHoanThienPACT_NgayHoanThanh = vmData.TrinhHDTDHoanThienPACT_NgayHoanThanh;
                    updateData.QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh = vmData.QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh;
                    updateData.ChiTraTien_NhanBGD_NgayHoanThanh = vmData.ChiTraTien_NhanBGD_NgayHoanThanh;
                    updateData.CuongCheThuHoiDat_NgayHoanThanh = vmData.CuongCheThuHoiDat_NgayHoanThanh;

                    updateData.QuyetToanKPTHGPMB_NgayHoanThanh = vmData.QuyetToanKPTHGPMB_NgayHoanThanh;
                    updateData.LapHSDNTPGiaoDat_NgayHoanThanh = vmData.LapHSDNTPGiaoDat_NgayHoanThanh;

                    updateData.GhiChu = vmData.GhiChu;

                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _GPMB_DangKyKeHoachService.Update(updateData);
                    _GPMB_DangKyKeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_DangKyKeHoach, GPMB_DangKyKeHoachViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_DangKyKeHoachService.Delete(id);
                    _GPMB_DangKyKeHoachService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}