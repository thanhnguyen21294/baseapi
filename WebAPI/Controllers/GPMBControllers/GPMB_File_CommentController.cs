﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB;
using WebAPI.Providers;
using WebAPI.SignalR;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb/file/comment")]
    [Authorize]
    public class GPMB_File_CommentController : ApiControllerBase
    {
        private IGPMB_File_CommentService _GPMB_File_CommentService;

        public GPMB_File_CommentController(IErrorService errorService, IGPMB_File_CommentService GPMB_File_CommentService) : base(errorService)
        {
            this._GPMB_File_CommentService = GPMB_File_CommentService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_File_CommentService.GetAll().OrderBy(x => x.CreatedDate);

                var modelVm = Mapper.Map<IEnumerable<GPMP_File_Comment>, IEnumerable<GPMB_File_CommentViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyidgpmbfile")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMB_FILE_COMMENT")]
        public HttpResponseMessage GetAllName(HttpRequestMessage request, int idFile)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_File_CommentService.GetByIdGPMBFileComment(idFile).OrderBy(x => x.CreatedDate);
                var modelVm = Mapper.Map<IEnumerable<GPMP_File_Comment>, IEnumerable<GPMB_File_CommentViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMB_FILE_COMMENT")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_File_CommentService.GetById(id);
                var responseData = Mapper.Map<GPMP_File_Comment, GPMB_File_CommentViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_File_CommentViewModels vmModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newModel = new GPMP_File_Comment();
                    newModel.IdFile = vmModel.IdFile;
                    newModel.UserId = User.Identity.GetUserId();
                    newModel.NoiDung = vmModel.NoiDung;
                    newModel.Status = true;
                    newModel.CreatedDate = DateTime.Now;
                    newModel.CreatedBy = User.Identity.GetUserId();
                    _GPMB_File_CommentService.Add(newModel);
                    _GPMB_File_CommentService.Save();

                    var modelHub = _GPMB_File_CommentService.GetById(newModel.Id);
                    QLDAHub.PushGPMB_File_CommentToAllUsers(Mapper.Map<GPMP_File_Comment, GPMB_File_CommentViewModels>(modelHub), null);

                    var responseData = Mapper.Map<GPMP_File_Comment, GPMB_File_CommentViewModels>(newModel);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMB_FILE_COMMENT")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var modelHubDelete = _GPMB_File_CommentService.GetById(id);
                    var modelHub = new GPMP_File_Comment();
                    modelHub.IdFile = modelHubDelete.IdFile;

                    _GPMB_File_CommentService.Delete(id);
                    _GPMB_File_CommentService.Save();

                    QLDAHub.PushGPMB_File_CommentToAllUsers(Mapper.Map<GPMP_File_Comment, GPMB_File_CommentViewModels>(modelHub), null);
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }
    }
}