﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB;
using WebAPI.Providers;
using WebAPI.SignalR;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb_comment")]
    public class GPMB_CommentController : ApiControllerBase
    {
        private IGPMB_CommentService _GPMB_commentService;
        public GPMB_CommentController(IErrorService errorService, IGPMB_CommentService GPMB_commentService) : base(errorService)
        {
            this._GPMB_commentService = GPMB_commentService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_commentService.GetAll().OrderBy(x => x.CreatedDate);

                var modelVm = Mapper.Map<IEnumerable<GPMB_Comment>, IEnumerable<GPMB_Comment_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COMMENT")]
        public HttpResponseMessage GetAllName(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_commentService.GetByIdDuAn(idDuAn).OrderBy(x => x.CreatedDate);
                var modelVm = Mapper.Map<IEnumerable<GPMB_Comment>, IEnumerable<GPMB_Comment_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "COMMENT")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_commentService.GetById(id);
                var responseData = Mapper.Map<GPMB_Comment, GPMB_Comment_ViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_Comment_ViewModels vmModel)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newModel = new GPMB_Comment();
                    newModel.IdDuAn = vmModel.IdDuAn;
                    newModel.UserId = User.Identity.GetUserId();
                    newModel.NoiDung = vmModel.NoiDung;
                    newModel.Status = true;
                    newModel.CreatedDate = DateTime.Now;
                    newModel.CreatedBy = User.Identity.GetUserId();
                    _GPMB_commentService.Add(newModel);
                    _GPMB_commentService.Save();

                    //SignalR
                    //push notification
                    var modelHub = _GPMB_commentService.GetById(newModel.IdComment);
                    QLDAHub.PushGPMB_CommentToAllUsers(Mapper.Map<GPMB_Comment, GPMB_Comment_ViewModels>(modelHub), null);
                    // end.

                    var responseData = Mapper.Map<GPMB_Comment, GPMB_Comment_ViewModels>(newModel);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "COMMENT")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var modelHubDelete = _GPMB_commentService.GetById(id);
                    var modelHub = new GPMB_Comment();
                    modelHub.IdDuAn = modelHubDelete.IdDuAn;

                    _GPMB_commentService.Delete(id);
                    _GPMB_commentService.Save();

                    QLDAHub.PushGPMB_CommentToAllUsers(Mapper.Map<GPMB_Comment, GPMB_Comment_ViewModels>(modelHub), null);
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }
    }

   
}
