﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.GPMB;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb_ketqua_chitiet")]
    [Authorize]
    public class GPMB_KetQua_ChiTietController : ApiControllerBase
    {
        private IGPMB_KetQua_ChiTietService _GPMB_KetQua_ChiTietService;
        public GPMB_KetQua_ChiTietController(
            IErrorService errorService,
            IGPMB_KetQua_ChiTietService GPMB_KetQua_ChiTietService,
            IDeleteService deleteService) : base(errorService)
        {
            _GPMB_KetQua_ChiTietService = GPMB_KetQua_ChiTietService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_KetQua_ChiTietService.GetAll(idDuAn);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_KetQua_ChiTietService.GetById(id);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "GPMBKETQUA")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_KetQua_ChiTietViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newData = new GPMB_KetQua_ChiTiet();
                    newData.UpdateGPMBKetQuaChiTiet(vmData);
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.CreatedDate = DateTime.Now;
                    newData.Status = true;
                    _GPMB_KetQua_ChiTietService.Add(newData);
                    _GPMB_KetQua_ChiTietService.Save();

                    var responseData = Mapper.Map<GPMB_KetQua_ChiTiet, GPMB_KetQua_ChiTietViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idGPMBKetQua, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _GPMB_KetQua_ChiTietService.GetByFilter(idGPMBKetQua, page, pageSize, out totalRow);
                var modelVm = Mapper.Map<IEnumerable<GPMB_KetQua_ChiTiet>, IEnumerable<GPMB_KetQua_ChiTietViewModels>>(model);

                PaginationSet<GPMB_KetQua_ChiTietViewModels> pagedSet = new PaginationSet<GPMB_KetQua_ChiTietViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKETQUA")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_TongQuanKetQuaViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    //var updateData = _GPMB_TongQuanKetQuaService.GetById(vmData.IdGPMBTongQuanKetQua);
                    //updateData.UpdateGPMBTongQuanKetQua(vmData);
                    //updateData.UpdatedDate = DateTime.Now;
                    //updateData.UpdatedBy = User.Identity.GetUserId();

                    //_GPMB_TongQuanKetQuaService.Update(updateData);
                    //_GPMB_TongQuanKetQuaService.Save();

                    //var responseData = Mapper.Map<GPMB_TongQuanKetQua, GPMB_TongQuanKetQuaViewModels>(updateData);
                    //response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMBKETQUA")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    //_GPMB_TongQuanKetQuaService.Delete(id);
                    //_GPMB_TongQuanKetQuaService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
