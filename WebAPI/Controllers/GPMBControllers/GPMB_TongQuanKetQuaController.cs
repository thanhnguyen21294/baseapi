﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.GPMB;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb_tongquanketqua")]
    [Authorize]
    public class GPMB_TongQuanKetQuaController : ApiControllerBase
    {
        private IGPMB_TongQuanKetQuaService _GPMB_TongQuanKetQuaService;
        private IGPMB_KetQuaService _GPMB_KetQuaService;
        private IDeleteService _deleteService;
        public GPMB_TongQuanKetQuaController(
            IErrorService errorService,
            IGPMB_TongQuanKetQuaService gpmb_TongQuanKetQuaService,
            IGPMB_KetQuaService GPMB_KetQuaService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_TongQuanKetQuaService = gpmb_TongQuanKetQuaService;
            this._GPMB_KetQuaService = GPMB_KetQuaService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_TongQuanKetQuaService.GetAll(idDuAn);
                var modelVm = Mapper.Map<IEnumerable<GPMB_TongQuanKetQua>, IEnumerable<GPMB_TongQuanKetQuaViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_TongQuanKetQuaService.GetAllForHistory().OrderByDescending(x => x.IdGPMBTongQuanKetQua);
                var a = model.ToList();
                var modelVm = Mapper.Map<IEnumerable<GPMB_TongQuanKetQua>, IEnumerable<GPMB_TongQuanKetQuaViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }


        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_TongQuanKetQuaService.GetById(id);
                var modelVm = Mapper.Map<GPMB_TongQuanKetQua, GPMB_TongQuanKetQuaViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }




        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "GPMBKETQUA")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_TongQuanKetQuaViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newData = new GPMB_TongQuanKetQua();
                    newData.UpdateGPMBTongQuanKetQua(vmData);
                    newData.IdDuAn = vmData.IdDuAn;
                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;
                    //var capNhatKLNTAdded = _capNhatKLNTService.Add(capNhatKLNT);
                    var TongQuanKetQua = _GPMB_TongQuanKetQuaService.Add(newData);
                    _GPMB_TongQuanKetQuaService.Save();

                    var newGPMB_KetQua = new GPMB_KetQua();
                    newGPMB_KetQua.IDTongQuanKetQua = TongQuanKetQua.IdGPMBTongQuanKetQua;
                    newGPMB_KetQua.DHT_DT = null;
                    newGPMB_KetQua.DHT_SoHo = null;
                    newGPMB_KetQua.DHT_TongKinhPhi = null;

                    newGPMB_KetQua.DHT_DatNNCaThe_DT = null;
                    newGPMB_KetQua.DHT_DatNNCaThe_SoHo = null;

                    newGPMB_KetQua.DHT_DatNNCong_DT = null;
                    newGPMB_KetQua.DHT_DatNNCong_SoHo = null;

                    newGPMB_KetQua.DHT_DatKhac_DT = null;
                    newGPMB_KetQua.DHT_DatKhac_SoHo = null;

                    newGPMB_KetQua.DHT_MoMa = null;

                    newGPMB_KetQua.CTHX_TongDT = null;
                    newGPMB_KetQua.CTHX_TongSoHo = null;

                    newGPMB_KetQua.CTHX_LapTrinhHoiDong_DuThao_DT = null;
                    newGPMB_KetQua.CTHX_LapTrinhHoiDong_DuThao_SoHo = null;

                    newGPMB_KetQua.CTHX_CongKhai_DuThao_DT = null;
                    newGPMB_KetQua.CTHX_CongKhai_DuThao_SoHo = null;

                    newGPMB_KetQua.CTHX_TrinhHoiDong_ChinhThuc_DT = null;
                    newGPMB_KetQua.CTHX_TrinhHoiDong_ChinhThuc_SoHo = null;

                    newGPMB_KetQua.CTHX_QuyetDinhThuHoi_DT = null;
                    newGPMB_KetQua.CTHX_QuyetDinhThuHoi_SoHo = null;

                    newGPMB_KetQua.CTHX_ChiTraTien_DT = null;
                    newGPMB_KetQua.CTHX_ChiTraTien_SoHo = null;

                    newGPMB_KetQua.KKVM_DT = null;
                    newGPMB_KetQua.KKVM_SoHo = null;
                    newGPMB_KetQua.KKVM_DaPheChuaNhanTien = null;
                    newGPMB_KetQua.KKVM_KhongHopTac = null;
                    newGPMB_KetQua.KKVM_ChuaDieuTra = null;

                    newGPMB_KetQua.GhiChu = null;
                    newGPMB_KetQua.Status = true;   
                    //newGPMB_KetQua.IdDuAn = vmData.IdDuAn;

                    //newGPMB_KetQua.LapTrinhHDKTHoanThienDTPA_DT = null;
                    //newGPMB_KetQua.LapTrinhHDKTHoanThienDTPA_SoHo = null;

                    //newGPMB_KetQua.LapTrinhHDKTHoanThienDTPA_GiaTri = null;
                    //newGPMB_KetQua.CongKhaiKTCongKhaiDTPA_DT = null;
                    //newGPMB_KetQua.CongKhaiKTCongKhaiDTPA_SoHo = null;
                    //newGPMB_KetQua.CongKhaiKTCongKhaiDTPA_GiaTri = null;
                    //newGPMB_KetQua.TrinhHDTDHoanThienPACT_DT = null;
                    //newGPMB_KetQua.TrinhHDTDHoanThienPACT_SoHo = null;
                    //newGPMB_KetQua.TrinhHDTDHoanThienPACT_GiaTri = null;
                    //newGPMB_KetQua.QDThuHoiDatQuyetDinhPDPA_DT = null;
                    //newGPMB_KetQua.QDThuHoiDatQuyetDinhPDPA_SoHo = null;
                    //newGPMB_KetQua.QDThuHoiDatQuyetDinhPDPA_GiaTri = null;
                    //newGPMB_KetQua.ChiTraTienNhanBanGiaoDat_DT = null;
                    //newGPMB_KetQua.ChiTraTienNhanBanGiaoDat_SoHo = null;
                    //newGPMB_KetQua.ChiTraTienNhanBanGiaoDat_GiaTri = null;

                    //newGPMB_KetQua.DatNNCaTheTuSDOD_DT = null;
                    //newGPMB_KetQua.DatNNCaTheTuSDOD_SoHo = null;
                    //newGPMB_KetQua.DatNNCaTheTuSDOD_GiaTri = null;
                    //newGPMB_KetQua.DatNNCong_DT = null;
                    //newGPMB_KetQua.DatNNCong_SoHo = null;
                    //newGPMB_KetQua.DatNNCong_GiaTri = null;
                    //newGPMB_KetQua.DatKhac_DT = null;
                    //newGPMB_KetQua.DatKhac_SoHo = null;
                    //newGPMB_KetQua.DatKhac_GiaTri = null;
                    //newGPMB_KetQua.MoMa = null;
                    //newGPMB_KetQua.MoMa_GiaTri = null;

                    //newGPMB_KetQua.DaPheNhungChuaNhanTien = null;
                    //newGPMB_KetQua.KhongHopTacDieuTraKiemDem = null;
                    //newGPMB_KetQua.ChuaDieuTraKiemDem = null;

                    //newGPMB_KetQua.Tong_DT = null;
                    //newGPMB_KetQua.Tong_SoHo = null;
                    //newGPMB_KetQua.Tong_GiaTri = null;
                    _GPMB_KetQuaService.Add(newGPMB_KetQua);
                    _GPMB_KetQuaService.Save();

                    var responseData = Mapper.Map<GPMB_TongQuanKetQua, GPMB_TongQuanKetQuaViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKETQUA")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_TongQuanKetQuaViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_TongQuanKetQuaService.GetById(vmData.IdGPMBTongQuanKetQua);
                    updateData.UpdateGPMBTongQuanKetQua(vmData);
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _GPMB_TongQuanKetQuaService.Update(updateData);
                    _GPMB_TongQuanKetQuaService.Save();

                    var responseData = Mapper.Map<GPMB_TongQuanKetQua, GPMB_TongQuanKetQuaViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMBKETQUA")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_TongQuanKetQuaService.Delete(id);
                    _GPMB_TongQuanKetQuaService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
