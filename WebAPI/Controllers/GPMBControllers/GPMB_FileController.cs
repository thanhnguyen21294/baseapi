﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Mobile;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService;
using Service.Mobile;
using WebAPI.Controllers.Mobile;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb/file")]
    [Authorize]
    public class GPMB_FileController : ApiControllerBase
    {
        private IGPMB_FileService _GPMB_FileService;
        private INotificationService _InotificationService;
        private INotification_UserService _Inotification_UserService;
        private INotificationService _NotificationService;
        public GPMB_FileController(IErrorService errorService, IGPMB_FileService gpmb_FileSerivce, INotificationService notificationService, INotification_UserService notification_UserService)
            : base(errorService)
        {
            _GPMB_FileService = gpmb_FileSerivce;
            _InotificationService = notificationService;
            _Inotification_UserService = notification_UserService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAllAndSearch(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_FileService.GetAllAndSearch(filter);
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                if (roleTemp.Contains("Trưởng phòng") || roleTemp.Contains("Phó giám đốc"))
                {
                    model = model.Where(x => x.TinhTrang >= 1);
                }
                var modelVM = Mapper.Map<IEnumerable<GPMB_File>, IEnumerable<GPMB_FileViewModel>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVM);
                return response;
            });
        }

        [Route("getCount")]
        [HttpGet]
        public HttpResponseMessage GetCount(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_FileService.GetAll();
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                if (roleTemp.Contains("Trưởng phòng") || roleTemp.Contains("Phó giám đốc"))
                {
                    model = model.Where(x => x.TinhTrang >= 1);
                }
                response = request.CreateResponse(HttpStatusCode.OK, model.Count());
                return response;
            });
        }

        [Route("getallparent")]
        [HttpGet]
        public HttpResponseMessage GetAllParent(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_FileService.GetAll().Where(x => x.ParentId == null && x.TrangThai == 3);

                var modelVM = Mapper.Map<IEnumerable<GPMB_File>, IEnumerable<GPMB_FileViewModel>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVM);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_FileService.GetById(id);
                var modelVM = Mapper.Map<GPMB_File, GPMB_FileViewModel>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVM);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int pageIndex, int pageSize, string filter = "", bool isWeekChecked = true, bool isMonthChecked = true)
        {
            int totalRow = 0;
            bool isEmployee = true;
            int role = 1;
            var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
            if (roleTemp.Contains("Trưởng phòng"))
            {
                isEmployee = false;
                role = 1;
            }
            if (roleTemp.Contains("Phó giám đốc"))
            {
                isEmployee = false;
                role = 2;
            }
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_FileService.GetListPaging(filter, out totalRow, pageIndex, pageSize, isWeekChecked, isMonthChecked, isEmployee, role);

                var modelVM = Mapper.Map<IEnumerable<GPMB_File>, IEnumerable<GPMB_FileViewModel>>(model);
                PaginationSet<GPMB_FileViewModel> pagedSet = new PaginationSet<GPMB_FileViewModel>()
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalRows = totalRow,
                    Items = modelVM,
                };
                response = request.CreateResponse(HttpStatusCode.OK, new { Results = pagedSet });
                return response; ;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "GPMBDKKEHOACH")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_FileViewModel gpmb_File)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                GPMB_File newEntity = new GPMB_File();
                newEntity.ParentId = gpmb_File.ParentId;
                newEntity.NoiDung = gpmb_File.NoiDung;
                newEntity.Mota = gpmb_File.Mota;
                newEntity.FileURL = gpmb_File.FileURL;
                newEntity.TrangThai = -1;
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    newEntity.IsBossCreated = true;
                    newEntity.TrangThai = 1;
                    newEntity.TinhTrang = 1;
                }
                newEntity.NgayTinhTrang = newEntity.CreatedDate = DateTime.Now;
                newEntity.CreatedBy = User.Identity.GetUserId();
                newEntity.Status = true;
                _GPMB_FileService.Add(newEntity);
                _GPMB_FileService.Save();
                var responseData = Mapper.Map<GPMB_File, GPMB_FileViewModel>(newEntity);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBDKKEHOACH")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_FileViewModel gpmb_File)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                GPMB_File entityUpdate = _GPMB_FileService.GetById(gpmb_File.Id);
                entityUpdate.ParentId = gpmb_File.ParentId;
                entityUpdate.Mota = gpmb_File.Mota;
                entityUpdate.NoiDung = gpmb_File.NoiDung;
                entityUpdate.FileURL = gpmb_File.FileURL;
                entityUpdate.NgayTinhTrang = DateTime.Now;
                entityUpdate.UpdatedDate = DateTime.Now;
                entityUpdate.UpdatedBy = User.Identity.GetUserId();
                entityUpdate.Status = true;
                _GPMB_FileService.Update(entityUpdate);
                _GPMB_FileService.Save();
                var responseData = Mapper.Map<GPMB_File, GPMB_FileViewModel>(entityUpdate);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMBDKKEHOACH")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                var deleteEntity = _GPMB_FileService.GetById(id);
                if (deleteEntity.ParentId == null)
                {
                    var listFileWeek = _GPMB_FileService.GetAll().Where(x => x.ParentId == id);
                    if (listFileWeek.Count() > 0)
                    {
                        foreach (var item in listFileWeek)
                        {
                            _GPMB_FileService.Delete(item.Id);
                            _GPMB_FileService.Save();
                        }
                    }
                }
                _GPMB_FileService.Delete(id);
                _GPMB_FileService.Save();
                response = request.CreateResponse(HttpStatusCode.Created, id);
                return response;
            });
        }

        [Route("pheduyet")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBDKKEHOACH")]
        public HttpResponseMessage PheDuyet(HttpRequestMessage request, GPMB_FileViewModel gpmb_File)
        {
            string title = "Thông báo kế hoạch giải phóng mặt bằng";
            string body = "Kế hoạch " + gpmb_File.Ten;
            List<string> listUserId = new List<string>();
            var userIdCreated = gpmb_File.AppUser.Id;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateEntity = _GPMB_FileService.GetById(gpmb_File.Id);
                    int trangthaiUpdate = -1;
                    int tinhtrangUpdate = 0;
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    switch (gpmb_File.TrangThai)
                    {
                        case -1:// cán bộ đang soạn
                            {
                                trangthaiUpdate = 1;
                                tinhtrangUpdate = 1;
                                body += " đang chờ phê duyệt";
                                listUserId = GetUserListUserIdRoleByIdUser(userIdCreated, "Trưởng phòng");
                                break;
                            }
                        case 0: // File không được phê duyệt
                            {
                                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                string nameRole = "";
                                if (roleTemp.Contains("Trưởng phòng"))
                                {
                                    tinhtrangUpdate = 1;
                                    nameRole = "Trưởng phòng";
                                }
                                else if (roleTemp.Contains("Phó giám đốc"))
                                {
                                    tinhtrangUpdate = 2;
                                    nameRole = "Phó giám đốc";
                                }
                                body += " đã không được phê duyệt bởi " + nameRole;
                                listUserId.Add(userIdCreated);
                                break;
                            }
                        case 1:// CB đã gửi lên cho TP
                            {
                                {
                                    trangthaiUpdate = 2;
                                    tinhtrangUpdate = 2;
                                }
                                listUserId = GetUserListUserIdRoleByIdUser(userIdCreated, "Phó giám đốc");
                                break;
                            }
                        case 2:// Trưởng phòng đã chấp thuận
                            {

                                trangthaiUpdate = 3;
                                tinhtrangUpdate = 3;
                                body += " đã không được phê duyệt bởi Phó giám đốc";
                                listUserId = GetUserListUserIdRoleByIdUser(userIdCreated, "Trưởng phòng");
                                listUserId.Add(userIdCreated);
                                break;
                            }
                    }
                    updateEntity.Mota = gpmb_File.Mota;
                    updateEntity.NoiDung = gpmb_File.NoiDung;
                    updateEntity.TrangThai = trangthaiUpdate;
                    updateEntity.TinhTrang = tinhtrangUpdate;
                    updateEntity.FileURL = gpmb_File.FileURL;
                    updateEntity.PheDuyet = true;
                    updateEntity.UpdatedDate = updateEntity.NgayTinhTrang = DateTime.Now;
                    updateEntity.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_FileService.Update(updateEntity);
                    _GPMB_FileService.Save();
                    var responseData = Mapper.Map<GPMB_File, GPMB_FileViewModel>(updateEntity);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        /// Duy
        [Route("getgpmbDangKy")]
        [HttpGet]
        public HttpResponseMessage GetDangKyAndSearch(HttpRequestMessage request, string filter, int Numberpage, int page)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    int TinhTrang = 1;
                    var model = _GPMB_FileService.GetMobileAllAndSearch(filter, TinhTrang, Numberpage, page);
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                }
                if (roleTemp.Contains("Phó giám đốc"))
                {
                    int TinhTrang = 2;
                    var model = _GPMB_FileService.GetMobileAllAndSearch(filter, TinhTrang, Numberpage, page);
                    response = request.CreateResponse(HttpStatusCode.OK, model);
                }
                return response;
            });
        }

        [Route("getCountgpmbDangKy")]
        [HttpGet]
        public HttpResponseMessage GetCountDangKyAndSearch(HttpRequestMessage request, string filter)
        {
            return CreateHttpResponse(request, () =>
            {
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    int TinhTrang = 1;
                    var count = _GPMB_FileService.GetCountAllAndSearch(filter, TinhTrang);

                    return request.CreateResponse(HttpStatusCode.OK, count);
                }
                if (roleTemp.Contains("Phó giám đốc") || roleTemp.Contains("Giám đốc"))
                {
                    int TinhTrang = 2;
                    var count = _GPMB_FileService.GetCountAllAndSearch(filter, TinhTrang);
                    return request.CreateResponse(HttpStatusCode.OK, count);
                }
                return request.CreateResponse(HttpStatusCode.OK, 0); 
            });
        }


        private List<string> GetUserListUserIdRoleByIdUser(string idUser, string roleName)
        {
            var listUserId = AppUserManager.Users.Where(x => x.IdNhomDuAnTheoUser == 11).Select(x => x.Id);
            var listUserIdRole = new List<string>();
            foreach (var item in listUserId)
            {
                if (AppUserManager.GetRoles(item).Contains(roleName))
                {
                    listUserIdRole.Add(item);
                }
            }
            return listUserIdRole;

        }
    }
}
