﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.Mobile;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_khokhanvuongmac")]
    [Authorize]
    public class GPMB_KhoKhanVuongMacController : ApiControllerBase
    {
        private IGPMB_KhoKhanVuongMacService _GPMB_KhoKhanVuongMacService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        public GPMB_KhoKhanVuongMacController(IErrorService errorService,
            IGPMB_KhoKhanVuongMacService GPMB_KhoKhanVuongMacService,
            IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
              IQLTD_KeHoachService qLTD_KeHoachService,
            IDeleteService deleteService) : base(errorService)
        {
            _qLTD_KeHoachService = qLTD_KeHoachService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            this._GPMB_KhoKhanVuongMacService = GPMB_KhoKhanVuongMacService;
            this._deleteService = deleteService;
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_KhoKhanVuongMacViewModels vmKeHoach)
        {
            var keHoach = _qLTD_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);
            List<string> listUserId = new List<string>();
            string title = "";
            string body = "";
            if (keHoach != null)
            {
                var idDuAn = keHoach.IdDuAn;
                title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                body = "Kế hoạch " + keHoach.NoiDung + " ;Khó khăn: " + vmKeHoach.NguyenNhanLyDo;
                listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                body = "Kế hoạch " + keHoach.NoiDung + " có khó khăn là " + vmKeHoach.NguyenNhanLyDo + " Giải pháp: " + vmKeHoach.GiaiPhapTrienKhai;
                listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
            }
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newKeHoach = new GPMB_KhoKhanVuongMac();
                    newKeHoach.IdKeHoachGPMB = vmKeHoach.IdKeHoachGPMB;
                    newKeHoach.UpdateGPMB_KhoKhanVuongMac(vmKeHoach);
                    newKeHoach.CreatedDate = DateTime.Now;
                    newKeHoach.CreatedBy = User.Identity.GetUserId();
                    newKeHoach.Status = true;
                    _GPMB_KhoKhanVuongMacService.Add(newKeHoach);
                    _GPMB_KhoKhanVuongMacService.Save();

                    var responseData = Mapper.Map<GPMB_KhoKhanVuongMac, GPMB_KhoKhanVuongMacViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId, title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_KhoKhanVuongMacViewModels vmKeHoach)
        {
            List<string> listUserId = new List<string>();
            string title = "";
            string body = "";
            if (vmKeHoach.DaGiaiQuyet)
            {
                var keHoach = _qLTD_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Khó khăn: " + vmKeHoach.NguyenNhanLyDo + " đã được xử lý";
                    listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                    listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                }
            }
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _GPMB_KhoKhanVuongMacService.GetById(vmKeHoach.IdKhoKhanVuongMac);
                    updateKeHoach.UpdateGPMB_KhoKhanVuongMac(vmKeHoach);
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KhoKhanVuongMacService.Update(updateKeHoach);
                    _GPMB_KhoKhanVuongMacService.Save();

                    var responseData = Mapper.Map<GPMB_KhoKhanVuongMac, GPMB_KhoKhanVuongMacViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0 && vmKeHoach.DaGiaiQuyet)
                {
                    _NotificationService.SendNotiToListUser(listUserId, title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_KhoKhanVuongMacService.Delete(id);
                    _GPMB_KhoKhanVuongMacService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_KhoKhanVuongMacService.GetById(id);
                var responseData = Mapper.Map<GPMB_KhoKhanVuongMac, GPMB_KhoKhanVuongMacViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getbyidkehoach")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_KhoKhanVuongMacService.GetByIdKeHoach(idKH);
                var responseData = Mapper.Map<IEnumerable<GPMB_KhoKhanVuongMac>, IEnumerable<GPMB_KhoKhanVuongMacViewModels>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getkhokhanvuongmacdieuhanhgpmb")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetKhoKhanVuongMacDieuHanh(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                string idUser = "";
                int idNhomDuAnTheoUser = 0;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    idUser = User.Identity.GetUserId();
                }

                var model = _GPMB_KhoKhanVuongMacService.GetKhoKhanVuongMacDieuHanh(idUser, idNhomDuAnTheoUser);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }
}
