﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_kehoachtiendochung")]
    [Authorize]
    public class GPMB_KeHoachTienDoChungController : ApiControllerBase
    {
        private IGPMB_KeHoachTienDoChungService _GPMB_KeHoachTienDoChungService;
        private IDeleteService _deleteService;
        public GPMB_KeHoachTienDoChungController(IErrorService errorService,
            IGPMB_KeHoachTienDoChungService GPMB_KeHoachTienDoChungService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_KeHoachTienDoChungService = GPMB_KeHoachTienDoChungService;
            this._deleteService = deleteService;
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_KeHoachTienDoChungViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newData = new GPMB_KeHoachTienDoChung();
                    newData.IdKeHoachGPMB = vmKeHoach.IdKeHoachGPMB;
                    newData.NoiDung = vmKeHoach.NoiDung;
                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;
                    _GPMB_KeHoachTienDoChungService.Add(newData);
                    _GPMB_KeHoachTienDoChungService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoachTienDoChung, GPMB_KeHoachTienDoChungViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_KeHoachTienDoChungViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_KeHoachTienDoChungService.GetById(vmKeHoach.IdKeHoachTienDoChung);
                    updateData.NoiDung = vmKeHoach.NoiDung;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KeHoachTienDoChungService.Update(updateData);
                    _GPMB_KeHoachTienDoChungService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoachTienDoChung, GPMB_KeHoachTienDoChungViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_KeHoachTienDoChungService.Delete(id);
                    _GPMB_KeHoachTienDoChungService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_KeHoachTienDoChungService.GetById(id);
                var responseData = Mapper.Map<GPMB_KeHoachTienDoChung, GPMB_KeHoachTienDoChungViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
        [Route("getbyidkehoach")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_KeHoachTienDoChungService.GetByIdKeHoach(idKH);
                var responseData = Mapper.Map<GPMB_KeHoachTienDoChung, GPMB_KeHoachTienDoChungViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
    }
}
