﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.Mobile;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.Mobile;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Models.Mobile;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_kehoach")]
    [Authorize]
    public class GPMB_KeHoachController : ApiControllerBase
    {
        private IGPMB_KeHoachService _GPMB_KeHoachService;
        private IGPMB_KeHoachCongViecService _GPMB_KeHoachCongViecService;
        private IGPMB_KeHoachTienDoChungService _GPMB_KeHoachTienDoChungService;
        private IGPMB_CTCVService _GPMB_CTCVService;
        private IDuAnService _DuAnService;
        private IDuAnUserService _DuAnUserService;
        private INotificationService _NotificationService;
        private IDeleteService _deleteService;

        public GPMB_KeHoachController(IErrorService errorService,
            IGPMB_KeHoachService GPMB_KeHoachService,
            IGPMB_KeHoachCongViecService GPMB_KeHoachCongViecService,
            IGPMB_CTCVService GPMB_CTCVService,
            IGPMB_KeHoachTienDoChungService GPMB_KeHoachTienDoChungService,
            IDuAnService DuAnService,
            IDuAnUserService duAnUserService,
            IDevice_TokenService device_TokenService,
            INotification_UserService notificationUserService,
            INotificationService notificationService,

            IDeleteService deleteService) : base(errorService)
        {
            _GPMB_KeHoachService = GPMB_KeHoachService;
            _GPMB_KeHoachCongViecService = GPMB_KeHoachCongViecService;
            _GPMB_CTCVService = GPMB_CTCVService;
            _GPMB_KeHoachTienDoChungService = GPMB_KeHoachTienDoChungService;
            _deleteService = deleteService;
            _DuAnService = DuAnService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
        }

        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMBKEHOACH")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idDuAn, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                //string idNhanVien = null;
                //List<string> listUser = new List<string>();
                //if (!User.IsInRole("Admin"))
                //{
                //    var permissions = JsonConvert.DeserializeObject<List<PermissionViewModel>>(((ClaimsIdentity)User.Identity).FindFirst("permissions").Value);
                //    if (!(permissions.Exists(x =>x.CanCreate) || permissions.Exists(x => x.FunctionId == "KEHOACHLAPDAT" && x.CanCreate)))
                //    {
                //        if (permissions.Exists(x => x.CanCreate))
                //        {
                //            listUser = AppUserManager.Users.Where(x =>
                //                x. ==
                //                int.Parse(((ClaimsIdentity)User.Identity).FindFirst("NhomNhanVienId").Value)).Select(y => y.Id).ToList();
                //        }
                //        else
                //        {
                //            idNhanVien = User.Identity.GetUserId();
                //        }
                //    }
                //}
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    if (User.IsInRole("Nhân viên"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                }

                var model = _GPMB_KeHoachService.GetAll(idDuAn, idNhomDuAnTheoUser, filter);

                if (!User.IsInRole("Admin"))
                {
                    var userRoles = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (!userRoles.Contains("Nhân viên"))
                    {
                        model = model.Where(x => x.TrangThai > -1);
                    }
                }

                var modelVm = Mapper.Map<IEnumerable<GPMB_KeHoach>, IEnumerable<GPMB_KeHoachViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallforhistory")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMBKEHOACH")]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _GPMB_KeHoachService.GetAllForHistory();
                var modelVm = Mapper.Map<IEnumerable<GPMB_KeHoach>, IEnumerable<GPMB_KeHoachViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMBKEHOACH")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int? idNhomDuAnTheoUser = null;
                if (!User.IsInRole("Admin"))
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }

                var model = _GPMB_KeHoachService.GetByFilter(idDuAn, idNhomDuAnTheoUser, page, pageSize, "", out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<GPMB_KeHoach>, IEnumerable<GPMB_KeHoachViewModels>>(model);

                PaginationSet<GPMB_KeHoachViewModels> pagedSet = new PaginationSet<GPMB_KeHoachViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "GPMBKEHOACH")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    int? idNhomDuAnTheoUser = null;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }

                    var newKeHoach = new GPMB_KeHoach();
                    newKeHoach.IdDuAn = vmKeHoach.IdDuAn;
                    newKeHoach.IdNhomDuAnTheoUser = idNhomDuAnTheoUser;
                    newKeHoach.NoiDung = vmKeHoach.NoiDung;
                    newKeHoach.GhiChu = vmKeHoach.GhiChu;
                    newKeHoach.TrangThai = -1;
                    var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                    if (roleTemp.Contains("Trưởng phòng"))
                    {
                        newKeHoach.TrangThai = 1;
                        newKeHoach.TinhTrang = 1;
                        newKeHoach.NgayTinhTrang = DateTime.Now;
                    }
                    //if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                    //    newKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newKeHoach.CreatedDate = DateTime.Now;
                    newKeHoach.CreatedBy = User.Identity.GetUserId();
                    newKeHoach.Status = true;

                    List<GPMB_KeHoachCongViec> lstKeHoachCongViec = new List<GPMB_KeHoachCongViec>();
                    foreach (var item in vmKeHoach.GPMB_KeHoachCongViecs)
                    {
                        GPMB_KeHoachCongViec newDt = new GPMB_KeHoachCongViec();
                        newDt.IdKeHoachGPMB = item.IdKeHoachGPMB;
                        newDt.IdCongViecGPMB = item.IdCongViecGPMB;
                        newDt.TuNgay = null;
                        if (item.TuNgay != "" && item.TuNgay != null)
                            newDt.TuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newDt.DenNgay = null;
                        if (item.DenNgay != "" && item.DenNgay != null)
                            newDt.DenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newDt.Status = true;
                        lstKeHoachCongViec.Add(newDt);
                    }
                    newKeHoach.GPMB_KeHoachCongViecs = lstKeHoachCongViec;

                    _GPMB_KeHoachService.Add(newKeHoach);
                    _GPMB_KeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKEHOACH")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        foreach (var item in vmKeHoach.GPMB_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _GPMB_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _GPMB_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _GPMB_KeHoachCongViecService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    var updateKeHoach = _GPMB_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);
                    updateKeHoach.IdDuAn = vmKeHoach.IdDuAn;
                    updateKeHoach.NoiDung = vmKeHoach.NoiDung;
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    //if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                    //    updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KeHoachService.Update(updateKeHoach);
                    _GPMB_KeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("updatetrangthai")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKEHOACH")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, GPMB_KeHoachViewModels vmKeHoach)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _GPMB_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);
                    updateKeHoach.TrangThai = vmKeHoach.TrangThai;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KeHoachService.Update(updateKeHoach);
                    _GPMB_KeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMBKEHOACH")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_KeHoachService.Delete(id);
                    _GPMB_KeHoachService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "GPMBKEHOACH")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _GPMB_KeHoachService.GetById(id);
                var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("pheduyet")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKEHOACH")]
        public HttpResponseMessage PheDuyet(HttpRequestMessage request, GPMB_KeHoachViewModels vmKeHoach)
        {
            string title = "Dự án " + _DuAnService.GetById(vmKeHoach.IdDuAn).TenDuAn;
            string body = "Kế hoạch " + vmKeHoach.NoiDung;
            List<string> listUserId = new List<string>();
            var userIdCreatedDA = vmKeHoach.CreatedBy;
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoach = _GPMB_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);
                    updateKeHoach.PheDuyet = true;
                    int trangthaiUpdate = -1;
                    int tinhtrangUpdate = 0;
                    int idNhomDuAnTheoUser = 0;
                    if (!User.IsInRole("Admin"))
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                    switch (vmKeHoach.TrangThai)
                    {
                        case -1:// cán bộ đang soạn
                            {
                                trangthaiUpdate = 1;
                                tinhtrangUpdate = 1;

                                body += " đang chờ phê duyệt";
                                listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                break;
                            }
                        case 0:// Kế hoạch không được phê duyệt
                            {
                                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                                body += " đang chờ phê duyệt";
                                if (roleTemp.Contains("Trưởng phòng"))
                                {
                                    trangthaiUpdate = 2;
                                    tinhtrangUpdate = 2;
                                    listUserId.Add(GetPhoGiamDocIdByDuAnId(vmKeHoach.IdDuAn));
                                }
                                else
                                {
                                    listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                    trangthaiUpdate = 1;
                                    tinhtrangUpdate = 1;
                                }
                                break;
                            }
                        case 1:// CB đã gửi lên cho TP
                            {
                                if (idNhomDuAnTheoUser == 13)
                                {
                                    trangthaiUpdate = 3;
                                    tinhtrangUpdate = 3;
                                }
                                else
                                {
                                    trangthaiUpdate = 2;
                                    tinhtrangUpdate = 2;
                                }
                                body += " đã được phê duyệt bởi trưởng phòng";
                                var phoGiamDocId = GetPhoGiamDocIdByDuAnId(vmKeHoach.IdDuAn);
                                listUserId.Add(phoGiamDocId);
                                break;
                            }
                        case 2:// Trưởng phòng đã chấp thuận
                            {
                                trangthaiUpdate = 3;
                                tinhtrangUpdate = 3;
                                body += " đã được phê duyệt bởi phó giám đốc";
                                listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                                break;
                            }
                            //case 3:// Phó giám đốc đã chấp thuận
                            //    {
                            //        trangthaiUpdate = 4;
                            //        tinhtrangUpdate = 4;
                            //        break;
                            //    }
                    }

                    try
                    {
                        foreach (var item in vmKeHoach.GPMB_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _GPMB_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _GPMB_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _GPMB_KeHoachCongViecService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    //if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                    //    updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    updateKeHoach.TrangThai = trangthaiUpdate;
                    updateKeHoach.TinhTrang = tinhtrangUpdate;
                    updateKeHoach.NgayTinhTrang = DateTime.Now;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KeHoachService.Update(updateKeHoach);
                    _GPMB_KeHoachService.Save();

                    if (trangthaiUpdate == 3)
                    {
                        GPMB_KeHoachTienDoChung newTDTHChung = new GPMB_KeHoachTienDoChung();
                        newTDTHChung.IdKeHoachGPMB = vmKeHoach.IdKeHoachGPMB;
                        _GPMB_KeHoachTienDoChungService.Add(newTDTHChung);
                        _GPMB_KeHoachTienDoChungService.Save();

                        foreach (var item in vmKeHoach.GPMB_KeHoachCongViecs)
                        {
                            var newCTCV = new GPMB_CTCV();
                            //newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                            newCTCV.IdKeHoachCongViecGPMB = Convert.ToInt32(item.IdKeHoachCongViec);
                            //newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                            newCTCV.CreatedDate = DateTime.Now;
                            newCTCV.CreatedBy = User.Identity.GetUserId();
                            newCTCV.Status = true;
                            _GPMB_CTCVService.Add(newCTCV);
                            _GPMB_CTCVService.Save();
                        }
                    }

                    var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        [Route("khongpheduyet")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKEHOACH")]
        public HttpResponseMessage KhongPheDuyet(HttpRequestMessage request, GPMB_KeHoachViewModels vmKeHoach)
        {
            string title = "Dự án " + _DuAnService.GetById(vmKeHoach.IdDuAn).TenDuAn;
            string body = "Kế hoạch " + vmKeHoach.NoiDung;
            List<string> listUserId = new List<string>();
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    try
                    {
                        foreach (var item in vmKeHoach.GPMB_KeHoachCongViecs)
                        {
                            DateTime dateTuNgay = new DateTime();
                            if (item.TuNgay != "" && item.TuNgay != null)
                                dateTuNgay = DateTime.ParseExact(item.TuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            DateTime dateDenNgay = new DateTime();
                            if (item.DenNgay != "" && item.DenNgay != null)
                                dateDenNgay = DateTime.ParseExact(item.DenNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                            var updateKeHoachCongViec = _GPMB_KeHoachCongViecService.GetById(item.IdKeHoachCongViec);
                            updateKeHoachCongViec.TuNgay = dateTuNgay;
                            updateKeHoachCongViec.DenNgay = dateDenNgay;
                            _GPMB_KeHoachCongViecService.Update(updateKeHoachCongViec);
                        }
                        _GPMB_KeHoachCongViecService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    var updateKeHoach = _GPMB_KeHoachService.GetById(vmKeHoach.IdKeHoachGPMB);

                    //if (vmKeHoach.NgayGiaoLapChuTruong != "" && vmKeHoach.NgayGiaoLapChuTruong != null)
                    //    updateKeHoach.NgayGiaoLapChuTruong = DateTime.ParseExact(vmKeHoach.NgayGiaoLapChuTruong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    updateKeHoach.GhiChu = vmKeHoach.GhiChu;
                    updateKeHoach.PheDuyet = false;
                    updateKeHoach.TrangThai = vmKeHoach.TrangThai;
                    updateKeHoach.NgayTinhTrang = DateTime.Now;
                    updateKeHoach.UpdatedDate = DateTime.Now;
                    updateKeHoach.UpdatedBy = User.Identity.GetUserId();
                    _GPMB_KeHoachService.Update(updateKeHoach);
                    _GPMB_KeHoachService.Save();

                    var responseData = Mapper.Map<GPMB_KeHoach, GPMB_KeHoachViewModels>(updateKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var roleTemp = AppUserManager.GetRoles(User.Identity.GetUserId());
                string nameRole;
                if (!roleTemp.Contains("Trưởng phòng"))
                {
                    listUserId = GetTruongPhongIdByDuAnId(vmKeHoach.IdDuAn);
                }
                if (vmKeHoach.CreatedBy != User.Identity.GetUserId())
                {
                    listUserId.Add(vmKeHoach.CreatedBy);
                }
                if (roleTemp.Contains("Trưởng phòng"))
                {
                    nameRole = "Trưởng phòng";
                }
                else if (roleTemp.Contains("Phó giám đốc"))
                {
                    nameRole = "Phó giám đốc";
                }
                else
                {
                    nameRole = "Giám đốc";
                }
                body += " đã không được phê duyệt bởi " + nameRole;
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }

        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }
    }

}
