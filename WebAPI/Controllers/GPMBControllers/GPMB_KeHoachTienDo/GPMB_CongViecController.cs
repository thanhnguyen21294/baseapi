﻿using AutoMapper;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_congviec")]
    [Authorize]
    public class GPMB_CongViecController : ApiControllerBase
    {
        private IGPMB_CongViecService _GPMB_CongViecService;
        private IDeleteService _deleteService;
        public GPMB_CongViecController(
            IErrorService errorService,
            IGPMB_CongViecService GPMB_CongViecService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_CongViecService = GPMB_CongViecService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CongViecService.GetAll();
                var modelVm = Mapper.Map<IEnumerable<GPMB_CongViec>, IEnumerable<GPMB_CongViecViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
    }
}
