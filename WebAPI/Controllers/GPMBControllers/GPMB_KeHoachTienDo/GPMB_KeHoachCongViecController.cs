﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.QLTDService;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Models.QLTD;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_kehoachcongviec")]
    [Authorize]
    public class GPMB_KeHoachCongViecController : ApiControllerBase
    {
        private IGPMB_KeHoachCongViecService _GPMB_KeHoachCongViecService;
        private IDeleteService _deleteService;
        public GPMB_KeHoachCongViecController(
            IErrorService errorService,
            IGPMB_KeHoachCongViecService GPMB_KeHoachCongViecService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_KeHoachCongViecService = GPMB_KeHoachCongViecService;
            this._deleteService = deleteService;
        }

        [Route("getbykehoach")]
        [HttpGet]
        public HttpResponseMessage GetByKeHoach(HttpRequestMessage request, int idKH)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_KeHoachCongViecService.GetListByIdKeHoach(idKH);
                var modelVm = Mapper.Map<IEnumerable<GPMB_KeHoachCongViec>, IEnumerable<GPMB_KeHoachCongViecViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }
    }
}
