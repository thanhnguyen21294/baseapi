﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/GPMB_CTCV_Dot_dot")]
    [Authorize]
    public class GPMB_CTCV_DotController : ApiControllerBase
    {
        private IGPMB_CTCV_DotService _GPMB_CTCV_DotService;
        private IDeleteService _deleteService;
        public GPMB_CTCV_DotController(
            IErrorService errorService,
            IGPMB_CTCV_DotService GPMB_CTCV_DotService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_CTCV_DotService = GPMB_CTCV_DotService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idCVCT)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCV_DotService.GetAll(idCVCT);
                var modelVm = Mapper.Map<IEnumerable<GPMB_CTCV_Dot>, IEnumerable<GPMB_CTCV_Dot_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallbyidcv")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdCongViec(HttpRequestMessage request, int idCVCT, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCV_DotService.GetAllByIDCVCT(idCVCT, filter);
                var a = model.ToList();
                var modelVm = Mapper.Map<IEnumerable<GPMB_CTCV_Dot>, IEnumerable<GPMB_CTCV_Dot_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, GPMB_CTCV_Dot_ViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newCTCV = new GPMB_CTCV_Dot();
                    newCTCV.NoiDung = vmData.NoiDung;
                    newCTCV.IdChiTietCongViec = vmData.IdChiTietCongViec;
                    if (vmData.TBTHD_Ngay != "" && vmData.TBTHD_Ngay != null)
                    {
                        newCTCV.TBTHD_Ngay = DateTime.ParseExact(vmData.TBTHD_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    newCTCV.TBTHD_SoDoiTuong = vmData.TBTHD_SoDoiTuong;

                    if (vmData.HDPTK_Ngay != "" && vmData.HDPTK_Ngay != null)
                    {
                        newCTCV.HDPTK_Ngay = DateTime.ParseExact(vmData.HDPTK_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    newCTCV.DTKT_SoDoiTuong = vmData.DTKT_SoDoiTuong;
                    newCTCV.CCKDBB_Chon = vmData.CCKDBB_Chon;
                    newCTCV.LHSGPMB_SoDoiTuong = vmData.LHSGPMB_SoDoiTuong;
                    newCTCV.LTHDTT_SoDoiTuong = vmData.LTHDTT_SoDoiTuong;
                    newCTCV.CKKT_SoDoiTuong = vmData.CKKT_SoDoiTuong;
                    newCTCV.THDTD_SoDoiTuong = vmData.THDTD_SoDoiTuong;
                    newCTCV.QDPDPA_SoDoiTuong = vmData.QDPDPA_SoDoiTuong;
                    newCTCV.QDPDPA_GiaTri = vmData.QDPDPA_GiaTri;
                    newCTCV.CTNBG_SoDoiTuong = vmData.CTNBG_SoDoiTuong;
                    newCTCV.CTNBG_GiaTri = vmData.CTNBG_GiaTri;
                    newCTCV.CCTHD_Chon = vmData.CCTHD_Chon;
                    newCTCV.UpdatedDate = DateTime.Now;
                    newCTCV.UpdatedBy = User.Identity.GetUserId();

                    newCTCV.CreatedDate = DateTime.Now;
                    newCTCV.CreatedBy = User.Identity.GetUserId();
                    newCTCV.Status = true;
                    _GPMB_CTCV_DotService.Add(newCTCV);
                    _GPMB_CTCV_DotService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCV_DotService.GetByID(id);
                var modelVm = Mapper.Map<GPMB_CTCV_Dot, GPMB_CTCV_Dot_ViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_CTCV_Dot_ViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_CTCV_DotService.GetByID(vmData.IdChiTietCongViec_Dot);
                    updateData.NoiDung = vmData.NoiDung;
                    if (vmData.TBTHD_Ngay != "" && vmData.TBTHD_Ngay != null)
                    {
                        updateData.TBTHD_Ngay = DateTime.ParseExact(vmData.TBTHD_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.TBTHD_SoDoiTuong = vmData.TBTHD_SoDoiTuong;

                    if (vmData.HDPTK_Ngay != "" && vmData.HDPTK_Ngay != null)
                    {
                        updateData.HDPTK_Ngay = DateTime.ParseExact(vmData.HDPTK_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.DTKT_SoDoiTuong = vmData.DTKT_SoDoiTuong;
                    updateData.CCKDBB_Chon = vmData.CCKDBB_Chon;
                    updateData.LHSGPMB_SoDoiTuong = vmData.LHSGPMB_SoDoiTuong;
                    updateData.LTHDTT_SoDoiTuong = vmData.LTHDTT_SoDoiTuong;
                    updateData.CKKT_SoDoiTuong = vmData.CKKT_SoDoiTuong;
                    updateData.THDTD_SoDoiTuong = vmData.THDTD_SoDoiTuong;

                    updateData.QDPDPA_SoDoiTuong = vmData.QDPDPA_SoDoiTuong;
                    updateData.QDPDPA_GiaTri = vmData.QDPDPA_GiaTri;
                    updateData.CTNBG_SoDoiTuong = vmData.CTNBG_SoDoiTuong;
                    updateData.CTNBG_GiaTri = vmData.CTNBG_GiaTri;

                    updateData.CCTHD_Chon = vmData.CCTHD_Chon;
                    
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _GPMB_CTCV_DotService.Update(updateData);
                    _GPMB_CTCV_DotService.Save();

                    var responseData = Mapper.Map<GPMB_CTCV_Dot, GPMB_CTCV_Dot_ViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_CTCV_DotService.Delete(id);
                    _GPMB_CTCV_DotService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
