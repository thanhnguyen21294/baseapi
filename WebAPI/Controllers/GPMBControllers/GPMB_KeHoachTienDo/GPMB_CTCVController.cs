﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD.GPMB;
using Service;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.Mobile;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers.GPMB_KeHoachTienDo
{
    [RoutePrefix("api/gpmb_ctcv")]
    [Authorize]
    public class GPMB_CTCVController : ApiControllerBase
    {
        private IGPMB_CTCVService _GPMB_CTCVService;
        private IDeleteService _deleteService;
        private IDuAnUserService _DuAnUserService;
        private IDuAnService _DuAnService;
        private INotificationService _NotificationService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        public GPMB_CTCVController(
            IErrorService errorService,
            IGPMB_CTCVService GPMB_CTCVService,
             IQLTD_KeHoachService qLTD_KeHoachService,
             IDuAnUserService duAnUserService,
            INotificationService notificationService,
            IDuAnService duAnService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_CTCVService = GPMB_CTCVService;
            this._deleteService = deleteService;
            _DuAnUserService = duAnUserService;
            _NotificationService = notificationService;
            _DuAnService = duAnService;
            _qLTD_KeHoachService = qLTD_KeHoachService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCVService.GetAll(idKHCV);
                var modelVm = Mapper.Map<IEnumerable<GPMB_CTCV>, IEnumerable<GPMB_CTCV_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallbyidkh")]
        [HttpGet]
        public HttpResponseMessage GetAllByIdKeHoach(HttpRequestMessage request, int idKHCV, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCVService.GetAllByIDKH(idKHCV, filter);
                var a = model.ToList();
                var modelVm = Mapper.Map<IEnumerable<GPMB_CTCV>, IEnumerable<GPMB_CTCV_ViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("addmulti")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateMulti(HttpRequestMessage request, IEnumerable<GPMB_KeHoachCongViecViewModels> vmKHCVs)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in vmKHCVs)
                    {
                        var newCTCV = new GPMB_CTCV();
                        //newCTCV.IdKeHoach = Convert.ToInt32(item.IdKeHoach);
                        newCTCV.IdKeHoachCongViecGPMB = Convert.ToInt32(item.IdKeHoachCongViec);
                        //newCTCV.IdCongViec = Convert.ToInt32(item.IdCongViec);
                        //newCTCV.idLoaiCV = item.QLTD_CongViec.LoaiCongViec;
                        newCTCV.CreatedDate = DateTime.Now;
                        newCTCV.CreatedBy = User.Identity.GetUserId();
                        newCTCV.Status = true;
                        _GPMB_CTCVService.Add(newCTCV);
                        _GPMB_CTCVService.Save();
                    }
                    //var responseData = Mapper.Map<QLTD_CTCV_ChuTruongDauTu, QLTD_CTCV_ChuTruongDauTuViewModels>(newKeHoach);
                    response = request.CreateResponse(HttpStatusCode.Created, "");
                }
                return response;
            });
        }

        [Route("getbyidkhcv")]
        [HttpGet]
        public HttpResponseMessage GetByIdKHCV(HttpRequestMessage request, int idKHCV)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCVService.GetByIdKHCV(idKHCV);
                var modelVm = Mapper.Map<GPMB_CTCV, GPMB_CTCV_ViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_CTCVService.GetByID(id);
                var modelVm = Mapper.Map<GPMB_CTCV, GPMB_CTCV_ViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_CTCV_ViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_CTCVService.GetByID(vmData.IdChiTietCongViec);
                    updateData.SoQuyetDinh = vmData.SoQuyetDinh;
                    if (vmData.NgayPheDuyet != "" && vmData.NgayPheDuyet != null)
                    {
                        updateData.NgayPheDuyet = DateTime.ParseExact(vmData.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.VBGNV_SoVanBan = vmData.VBGNV_SoVanBan;
                    if (vmData.VBGNV_Ngay != "" && vmData.VBGNV_Ngay != null)
                    {
                        updateData.VBGNV_Ngay = DateTime.ParseExact(vmData.VBGNV_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    if (vmData.BBBGMG_Ngay != "" && vmData.BBBGMG_Ngay != null)
                    {
                        updateData.BBBGMG_Ngay = DateTime.ParseExact(vmData.BBBGMG_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.QDTLHD_So = vmData.QDTLHD_So;
                    if (vmData.QDTLHD_Ngay != "" && vmData.QDTLHD_Ngay != null)
                    {
                        updateData.QDTLHD_Ngay = DateTime.ParseExact(vmData.QDTLHD_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.QDTCT_So = vmData.QDTCT_So;
                    if (vmData.QDTCT_Ngay != "" && vmData.QDTCT_Ngay != null)
                    {
                        updateData.QDTCT_Ngay = DateTime.ParseExact(vmData.QDTCT_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.QDPDDT_So = vmData.QDPDDT_So;
                    if (vmData.QDPDDT_NgayBanBanHanh != "" && vmData.QDPDDT_NgayBanBanHanh != null)
                    {
                        updateData.QDPDDT_NgayBanBanHanh = DateTime.ParseExact(vmData.QDPDDT_NgayBanBanHanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    if (vmData.HHD_Ngay != "" && vmData.HHD_Ngay != null)
                    {
                        updateData.HHD_Ngay = DateTime.ParseExact(vmData.HHD_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    if (vmData.HDU_Ngay != "" && vmData.HDU_Ngay != null)
                    {
                        updateData.HDU_Ngay = DateTime.ParseExact(vmData.HDU_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    updateData.QCTDHT_Chon = vmData.QCTDHT_Chon;

                    updateData.TBTHD_SoDoiTuong = vmData.TBTHD_SoDoiTuong;

                    updateData.HDPTK_Chon = vmData.HDPTK_Chon;

                    updateData.DTKKLBB_SoDoiTuong = vmData.DTKKLBB_SoDoiTuong;

                    updateData.CCKDBB_Chon = vmData.CCKDBB_Chon;

                    updateData.LHSGPMB_So = vmData.LHSGPMB_So;

                    updateData.LTHDTT_So = vmData.LTHDTT_So;

                    updateData.SKKTDTPA_So = vmData.SKKTDTPA_So;

                    updateData.THDHTPA_So = vmData.THDHTPA_So;

                    updateData.QDPDPA_So = vmData.QDPDPA_So;
                    updateData.QDPDPA_TGT = vmData.QDPDPA_TGT;

                    updateData.CTTNBG_So = vmData.CTTNBG_So;
                    updateData.CTTNBG_TGT = vmData.CTTNBG_TGT;

                    updateData.CCTHD_Chon = vmData.CCTHD_Chon;

                    updateData.QTKP_So = vmData.QTKP_So;
                    if (vmData.QTKP_Ngay != "" && vmData.QTKP_Ngay != null)
                    {
                        updateData.QTKP_Ngay = DateTime.ParseExact(vmData.QTKP_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    updateData.QTKP_GT = vmData.QTKP_GT;

                    updateData.QDGD_So = vmData.QDGD_So;
                    if (vmData.QDGD_Ngay != "" && vmData.QDGD_Ngay != null)
                    {
                        updateData.QDGD_Ngay = DateTime.ParseExact(vmData.QDGD_Ngay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }

                    if (vmData.HoanThanh == 2 && updateData.NgayHoanThanh == null)
                    {
                        updateData.NgayHoanThanh = DateTime.Now;
                    }
                    updateData.HoanThanh = vmData.HoanThanh;
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _GPMB_CTCVService.Update(updateData);
                    _GPMB_CTCVService.Save();

                    var responseData = Mapper.Map<GPMB_CTCV, GPMB_CTCV_ViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                var keHoach = _qLTD_KeHoachService.GetById(vmData.IdKeHoachCongViecGPMB);
                List<string> listUserId = new List<string>();
                string title = "";
                string body = "";
                if (keHoach != null)
                {
                    var idDuAn = keHoach.IdDuAn;
                    title = "Dự án " + _DuAnService.GetById(idDuAn).TenDuAn;
                    body = "Kế hoạch " + keHoach.NoiDung + " ,Công việc: " + _GPMB_CTCVService.GetByID(vmData.IdChiTietCongViec).GPMB_KeHoachCongViec.GPMB_CongViec.TenCongViec;
                    if (vmData.HoanThanh == 1)
                    {
                        body += " Đang chờ duyệt";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                    }
                    if (vmData.HoanThanh == 2)
                    {
                        body = body = "Kế hoạch " + keHoach.NoiDung + " Đã hoàn thành";
                        listUserId = GetTruongPhongIdByDuAnId(idDuAn);
                        listUserId.Add(GetGiamDocIdByDuAnId(idDuAn));
                        listUserId.Add(GetPhoGiamDocIdByDuAnId(idDuAn));
                    }
                }
                if (listUserId.Count() > 0)
                {
                    _NotificationService.SendNotiToListUser(listUserId.Distinct().ToList(), title, body, User.Identity.GetUserId());
                }
                return response;
            });
        }


        private List<string> GetTruongPhongIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId)
                .Where(x => AppUserManager.GetRoles(x).Contains("Trưởng phòng")).ToList();
            return listUserIdByDuAn;
        }

        private string GetGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Giám đốc")).First();
            return userId;
        }

        private string GetPhoGiamDocIdByDuAnId(int DuAnId)
        {
            var listUserIdByDuAn = _DuAnUserService.GetByIdDuAn(DuAnId).Select(x => x.UserId);
            var userId = listUserIdByDuAn.Where(x => AppUserManager.GetRoles(x).Contains("Phó giám đốc")).FirstOrDefault();
            return userId;
        }

        [Route("congviechoanthanhs")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage CongViecHoanThanhs(HttpRequestMessage request, List<CTCVHoanThanhViewModels> vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (CTCVHoanThanhViewModels item in vmData)
                    {
                        var updateData = _GPMB_CTCVService.GetByID(item.IDChiTietCV);
                        updateData.HoanThanh = 2;
                        updateData.NgayHoanThanh = DateTime.Now;
                        _GPMB_CTCVService.Update(updateData);
                        _GPMB_CTCVService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }
                return response;
            });
        }


        [Route("getcongviecchoduyet")]
        [HttpGet]
        public HttpResponseMessage GetCongViecChoDuyet(HttpRequestMessage request, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var dataAll = _GPMB_CTCVService.GetCongViecChoDuyet(filter).Select(s => new
                {
                    IDDuAn = s.GPMB_KeHoachCongViec.GPMB_KeHoach.IdDuAn,
                    TenDuAn = s.GPMB_KeHoachCongViec.GPMB_KeHoach.DuAn.TenDuAn,
                    IDCongViecGPMB = s.GPMB_KeHoachCongViec.IdCongViecGPMB,
                    TenCongViecGPMB = s.GPMB_KeHoachCongViec.GPMB_CongViec.TenCongViec,
                    IDKeHoachGPMB = s.GPMB_KeHoachCongViec.IdKeHoachGPMB,
                    TenKeHoach = s.GPMB_KeHoachCongViec.GPMB_KeHoach.NoiDung,
                    IDChiTietCV = s.IdChiTietCongViec
                }).ToList();

                var dataReturn = dataAll.GroupBy(x => new { x.IDDuAn, x.TenDuAn })
                    .Select(x => new
                    {
                        IDDuAn = x.Key.IDDuAn,
                        TenDuAn = x.Key.TenDuAn,
                        grpKH = x.GroupBy(y => new { y.IDKeHoachGPMB, y.TenKeHoach }).Select(z => new
                        {
                            IDKH = z.Key.IDKeHoachGPMB,
                            TenKH = z.Key.TenKeHoach,
                            grpCV = z.Select(w => new
                            {
                                IDChiTietCV = w.IDChiTietCV,
                                IDCV = w.IDCongViecGPMB,
                                TenCV = w.TenCongViecGPMB
                            })
                        })
                    }); ;
                response = request.CreateResponse(HttpStatusCode.OK, dataReturn);
                return response;
            });
        }
    }
}
