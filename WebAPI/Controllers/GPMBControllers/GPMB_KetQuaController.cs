﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.QLTD;
using Service;
using Service.GPMBService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.GPMB;
using WebAPI.Providers;

namespace WebAPI.Controllers.GPMBControllers
{
    [RoutePrefix("api/gpmb_ketqua")]
    [Authorize]
    public class GPMB_KetQuaController : ApiControllerBase
    {
        private IGPMB_KetQuaService _GPMB_KetQuaService;
        private IGPMB_KetQua_ChiTietService _GPMB_KetQua_ChiTietService;
        private IDeleteService _deleteService;
        public GPMB_KetQuaController(
            IErrorService errorService,
            IGPMB_KetQuaService GPMB_KetQuaService,
            IGPMB_KetQua_ChiTietService GPMB_KetQua_ChiTietService,
            IDeleteService deleteService) : base(errorService)
        {
            this._GPMB_KetQuaService = GPMB_KetQuaService;
            this._GPMB_KetQua_ChiTietService = GPMB_KetQua_ChiTietService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request, int IdGPMBTongQuanKetQua)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_KetQuaService.GetAll(IdGPMBTongQuanKetQua);
                var modelVm = Mapper.Map<IEnumerable<GPMB_KetQua>, IEnumerable<GPMB_KetQuaViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        //[Route("getallhoantthanh")]
        //[HttpGet]
        //public HttpResponseMessage GetAllHoanThanh(HttpRequestMessage request, int idDuAn)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        var model = _GPMB_KetQuaService.GetAllHoanThanh(idDuAn);
        //        var modelVm = Mapper.Map<IEnumerable<GPMB_KetQua>, IEnumerable<GPMB_KetQuaViewModels>>(model);
        //        response = request.CreateResponse(HttpStatusCode.OK, modelVm);
        //        return response;
        //    });
        //}

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _GPMB_KetQuaService.GetById(id);
                var modelVm = Mapper.Map<GPMB_KetQua, GPMB_KetQuaViewModels>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "GPMBKETQUA")]
        public HttpResponseMessage Create(HttpRequestMessage request, GPMB_KetQuaViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newData = new GPMB_KetQua();
                    //newData.IdDuAn = vmData.IdDuAn;
                    //newData.TieuDe = vmData.TieuDe;

                    //newData.LapTrinhHDKTHoanThienDTPA_DT = vmData.LapTrinhHDKTHoanThienDTPA_DT;
                    //newData.LapTrinhHDKTHoanThienDTPA_SoHo = vmData.LapTrinhHDKTHoanThienDTPA_SoHo;
                    //newData.LapTrinhHDKTHoanThienDTPA_GiaTri = vmData.LapTrinhHDKTHoanThienDTPA_GiaTri;

                    //newData.CongKhaiKTCongKhaiDTPA_DT = vmData.CongKhaiKTCongKhaiDTPA_DT;
                    //newData.CongKhaiKTCongKhaiDTPA_SoHo = vmData.CongKhaiKTCongKhaiDTPA_SoHo;
                    //newData.CongKhaiKTCongKhaiDTPA_GiaTri = vmData.CongKhaiKTCongKhaiDTPA_GiaTri;

                    //newData.TrinhHDTDHoanThienPACT_DT = vmData.TrinhHDTDHoanThienPACT_DT;
                    //newData.TrinhHDTDHoanThienPACT_SoHo = vmData.TrinhHDTDHoanThienPACT_SoHo;
                    //newData.TrinhHDTDHoanThienPACT_GiaTri = vmData.TrinhHDTDHoanThienPACT_GiaTri;

                    //newData.QDThuHoiDatQuyetDinhPDPA_DT = vmData.QDThuHoiDatQuyetDinhPDPA_DT;
                    //newData.QDThuHoiDatQuyetDinhPDPA_SoHo = vmData.QDThuHoiDatQuyetDinhPDPA_SoHo;
                    //newData.QDThuHoiDatQuyetDinhPDPA_GiaTri = vmData.QDThuHoiDatQuyetDinhPDPA_GiaTri;

                    //newData.ChiTraTienNhanBanGiaoDat_DT = vmData.ChiTraTienNhanBanGiaoDat_DT;
                    //newData.ChiTraTienNhanBanGiaoDat_SoHo = vmData.ChiTraTienNhanBanGiaoDat_SoHo;
                    //newData.ChiTraTienNhanBanGiaoDat_GiaTri = vmData.ChiTraTienNhanBanGiaoDat_GiaTri;

                    //newData.DatNNCaTheTuSDOD_DT = vmData.DatNNCaTheTuSDOD_DT;
                    //newData.DatNNCaTheTuSDOD_SoHo = vmData.DatNNCaTheTuSDOD_SoHo;
                    //newData.DatNNCaTheTuSDOD_GiaTri = vmData.DatNNCaTheTuSDOD_GiaTri;
                    //newData.DatNNCong_DT = vmData.DatNNCong_DT;
                    //newData.DatNNCong_SoHo = vmData.DatNNCong_SoHo;
                    //newData.DatNNCong_GiaTri = vmData.DatNNCong_GiaTri;
                    //newData.DatKhac_DT = vmData.DatKhac_DT;
                    //newData.DatKhac_SoHo = vmData.DatKhac_SoHo;
                    //newData.DatKhac_GiaTri = vmData.DatKhac_GiaTri;
                    //newData.MoMa = vmData.MoMa;
                    //newData.MoMa_GiaTri = vmData.MoMa_GiaTri;

                    //newData.DaPheNhungChuaNhanTien = vmData.DaPheNhungChuaNhanTien;
                    //newData.KhongHopTacDieuTraKiemDem = vmData.KhongHopTacDieuTraKiemDem;
                    //newData.ChuaDieuTraKiemDem = vmData.ChuaDieuTraKiemDem;

                    //newData.Tong_DT = vmData.Tong_DT;
                    //newData.Tong_SoHo = vmData.Tong_SoHo;
                    //newData.Tong_GiaTri = vmData.Tong_GiaTri;

                    //newData.IDTongQuanKetQua = 17;

                    newData.CreatedDate = DateTime.Now;
                    newData.CreatedBy = User.Identity.GetUserId();
                    newData.Status = true;

                    _GPMB_KetQuaService.Add(newData);
                    _GPMB_KetQuaService.Save();

                    var responseData = Mapper.Map<GPMB_KetQua, GPMB_KetQuaViewModels>(newData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "GPMBKETQUA")]
        public HttpResponseMessage Update(HttpRequestMessage request, GPMB_KetQuaViewModels vmData)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateData = _GPMB_KetQuaService.GetById(vmData.IdGPMBKetQua);

                    var dt = updateData;
                    var dtThayDoi = vmData;

                    ThayDoiData(dt, dtThayDoi);

                    updateData.UpdateGPMBKetQua(vmData);
                    updateData.UpdatedDate = DateTime.Now;
                    updateData.UpdatedBy = User.Identity.GetUserId();

                    _GPMB_KetQuaService.Update(updateData);
                    _GPMB_KetQuaService.Save();

                    var responseData = Mapper.Map<GPMB_KetQua, GPMB_KetQuaViewModels>(updateData);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        public void ThayDoiData(GPMB_KetQua dt, GPMB_KetQuaViewModels dtThayDoi)
        {
            string szBanDau;
            string szThayDoi;
            double dGiaTri;
            #region Tổng đã hoàn thành
            if (dt.DHT_DT != dtThayDoi.DHT_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DT);
                szBanDau = "Tổng diện tích đã hoàn thành: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.DHT_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.DHT_SoHo != dtThayDoi.DHT_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_SoHo);
                szBanDau = "Tổng số hộ đã hoàn thành: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.DHT_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.DHT_TongKinhPhi != dtThayDoi.DHT_TongKinhPhi)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_TongKinhPhi);
                szBanDau = "Tổng kinh phí đã hoàn thành:" + dGiaTri + " đồng";
                szThayDoi = dtThayDoi.DHT_TongKinhPhi.ToString() + " đồng";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Đã hoàn thành Đất NN 64, cá thể, tự sử dụng ổn định
            if (dt.DHT_DatNNCaThe_DT != dtThayDoi.DHT_DatNNCaThe_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatNNCaThe_DT);
                szBanDau = "Diện tích đất NN 64, cá thể, tự sử dụng ổn định đã hoàn thành: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.DHT_DatNNCaThe_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.DHT_DatNNCaThe_SoHo != dtThayDoi.DHT_DatNNCaThe_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatNNCaThe_SoHo);
                szBanDau = "Số hộ đất NN 64, cá thể, tự sử dụng ổn định đã hoàn thành: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.DHT_DatNNCaThe_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Đã hoàn thành Đất NN công
            if (dt.DHT_DatNNCong_DT != dtThayDoi.DHT_DatNNCong_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatNNCong_DT);
                szBanDau = "Diện tích đất NN công đã hoàn thành: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.DHT_DatNNCong_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.DHT_DatNNCong_SoHo != dtThayDoi.DHT_DatNNCong_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatNNCong_SoHo);
                szBanDau = "Số hộ đất NN công đã hoàn thành: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.DHT_DatNNCong_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            #endregion

            #region Đã hoàn thành Đất khác (đất ở, đất phi NN khác)
            if (dt.DHT_DatKhac_DT != dtThayDoi.DHT_DatKhac_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatKhac_DT);
                szBanDau = "Diện tích đất khác (đất ở, đất phi NN khác) đã hoàn thành:" + dGiaTri + "m2";
                szThayDoi = dtThayDoi.DHT_DatKhac_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.DHT_DatKhac_SoHo != dtThayDoi.DHT_DatKhac_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_DatKhac_SoHo);
                szBanDau = "Số hộ đất khác (đất ở, đất phi NN khác) đã hoàn thành: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.DHT_DatKhac_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Số mộ mả đã hoàn thành
            if (dt.DHT_MoMa != dtThayDoi.DHT_MoMa)
            {
                dGiaTri = InGiaTriNeuBang0(dt.DHT_MoMa);
                szBanDau = "Số mộ mả đã hoàn thành: " + dGiaTri + " ngôi";
                szThayDoi = dtThayDoi.DHT_MoMa.ToString() + " ngôi";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Tổng Chưa hoàn thành xong
            if (dt.CTHX_TongDT != dtThayDoi.CTHX_TongDT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_TongDT);
                szBanDau = "Tổng diện tích chưa hoàn thành xong: " + dt.CTHX_TongDT + " m2";
                szThayDoi = dtThayDoi.CTHX_TongDT.ToString() + "m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.CTHX_TongSoHo != dtThayDoi.CTHX_TongSoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_TongSoHo);
                szBanDau = "Tổng số hộ chưa hoàn thành xong: " + dt.CTHX_TongSoHo + " hộ";
                szThayDoi = dtThayDoi.CTHX_TongSoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Lập, trình Hội đồng thẩm tra, hoàn thiện dự thảo phương án
            if(dt.CTHX_LapTrinhHoiDong_DuThao_DT != dtThayDoi.CTHX_LapTrinhHoiDong_DuThao_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_LapTrinhHoiDong_DuThao_DT);
                szBanDau = "Diện tích Lập, trình Hội đồng thẩm tra, hoàn thiện dự thảo phương án: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.CTHX_LapTrinhHoiDong_DuThao_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if(dt.CTHX_LapTrinhHoiDong_DuThao_SoHo != dtThayDoi.CTHX_LapTrinhHoiDong_DuThao_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_LapTrinhHoiDong_DuThao_SoHo);
                szBanDau = "Số hộ Lập, trình Hội đồng thẩm tra, hoàn thiện dự thảo phương án: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.CTHX_LapTrinhHoiDong_DuThao_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Công khai, kết thúc công khai dự thảo phương án
            if(dt.CTHX_CongKhai_DuThao_DT != dtThayDoi.CTHX_CongKhai_DuThao_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_CongKhai_DuThao_DT);
                szBanDau = "Diện tích công khai, kết thúc công khai dự thảo phương án: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.CTHX_CongKhai_DuThao_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if(dt.CTHX_CongKhai_DuThao_SoHo != dtThayDoi.CTHX_CongKhai_DuThao_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_CongKhai_DuThao_SoHo);
                szBanDau = "Số hộ công khai, kết thúc công khai dự thảo phương án: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.CTHX_CongKhai_DuThao_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Trình Hội đồng thẩm định, hoàn thiện phương án chính thức
            if (dt.CTHX_TrinhHoiDong_ChinhThuc_DT != dtThayDoi.CTHX_TrinhHoiDong_ChinhThuc_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_TrinhHoiDong_ChinhThuc_DT);
                szBanDau = "Diện tích Trình Hội đồng thẩm định, hoàn thiện phương án chính thức: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.CTHX_TrinhHoiDong_ChinhThuc_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }

            if (dt.CTHX_TrinhHoiDong_ChinhThuc_SoHo != dtThayDoi.CTHX_TrinhHoiDong_ChinhThuc_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_TrinhHoiDong_ChinhThuc_SoHo);
                szBanDau = "Số hộ Trình Hội đồng thẩm định, hoàn thiện phương án chính thức: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.CTHX_TrinhHoiDong_ChinhThuc_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Quyết định thu hồi đất, quyết định phê duyệt phương án
            if (dt.CTHX_QuyetDinhThuHoi_DT != dtThayDoi.CTHX_QuyetDinhThuHoi_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_QuyetDinhThuHoi_DT);
                szBanDau = "Diện tích Quyết định thu hồi đất, quyết định phê duyệt phương án: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.CTHX_QuyetDinhThuHoi_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.CTHX_QuyetDinhThuHoi_SoHo != dtThayDoi.CTHX_QuyetDinhThuHoi_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_QuyetDinhThuHoi_SoHo);
                szBanDau = "Số hộ Quyết định thu hồi đất, quyết định phê duyệt phương án: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.CTHX_QuyetDinhThuHoi_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Chi trả tiền, nhận bàn giao đất
            if (dt.CTHX_ChiTraTien_DT != dtThayDoi.CTHX_ChiTraTien_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_ChiTraTien_DT);
                szBanDau = "Diện tích Chi trả tiền, nhận bàn giao đất: " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.CTHX_ChiTraTien_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.CTHX_ChiTraTien_SoHo != dtThayDoi.CTHX_ChiTraTien_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.CTHX_ChiTraTien_SoHo);
                szBanDau = "Số hộ Chi trả tiền, nhận bàn giao đất: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.CTHX_ChiTraTien_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Tồn tại, khó khăn, vướng mắc
            if (dt.KKVM_DT != dtThayDoi.KKVM_DT)
            {
                dGiaTri = InGiaTriNeuBang0(dt.KKVM_DT);
                szBanDau = "Tồn tại, khó khăn, vướng mắc: Diện tích " + dGiaTri + " m2";
                szThayDoi = dtThayDoi.KKVM_DT.ToString() + " m2";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.KKVM_SoHo != dtThayDoi.KKVM_SoHo)
            {
                dGiaTri = InGiaTriNeuBang0(dt.KKVM_SoHo);
                szBanDau = "Tồn tại, khó khăn, vướng mắc: Tổng số hộ " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.KKVM_SoHo.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.KKVM_DaPheChuaNhanTien != dtThayDoi.KKVM_DaPheChuaNhanTien)
            {
                dGiaTri = InGiaTriNeuBang0(dt.KKVM_DaPheChuaNhanTien);
                szBanDau = "Tồn tại, khó khăn, vướng mắc: Số hộ đã phê nhưng chưa nhận tiền từ: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.KKVM_DaPheChuaNhanTien.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.KKVM_KhongHopTac != dtThayDoi.KKVM_KhongHopTac)
            {
                dGiaTri = InGiaTriNeuBang0(dt.KKVM_KhongHopTac);
                szBanDau = "Tồn tại, khó khăn, vướng mắc: Số hộ Không hợp tác điều tra, kiểm đếm từ: " + dGiaTri + " hộ";
                szThayDoi = dtThayDoi.KKVM_KhongHopTac.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            if (dt.KKVM_ChuaDieuTra != dtThayDoi.KKVM_ChuaDieuTra)
            {
                dGiaTri = InGiaTriNeuBang0(dt.KKVM_ChuaDieuTra);
                szBanDau = "Tồn tại, khó khăn, vướng mắc: Số hộ chưa điều tra, kiểm đếm từ: " + dt.KKVM_ChuaDieuTra + " hộ";
                szThayDoi = dtThayDoi.KKVM_ChuaDieuTra.ToString() + " hộ";
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion

            #region Ghi chú
            if (dt.GhiChu != dtThayDoi.GhiChu)
            {
                szBanDau = "Ghi chú:" + dt.GhiChu;
                szThayDoi = dtThayDoi.GhiChu.ToString();
                ThemMoiDataThayDoi(dtThayDoi, szBanDau, szThayDoi);
            }
            #endregion
        }

        public double InGiaTriNeuBang0 (double? dGiaTri)
        {
            if(dGiaTri == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(dGiaTri);
            }
        }

        public void ThemMoiDataThayDoi(GPMB_KetQuaViewModels dtThayDoi, string szBanDau, string szThayDoi)
        {
            var dtKetQuaChiTiet = new GPMB_KetQua_ChiTiet();
            dtKetQuaChiTiet.IdGPMBKetQua = dtThayDoi.IdGPMBKetQua;
            dtKetQuaChiTiet.NoiDung = szBanDau;
            dtKetQuaChiTiet.ThayDoi = szThayDoi;
            dtKetQuaChiTiet.CreatedBy = User.Identity.GetUserId();
            dtKetQuaChiTiet.CreatedDate = DateTime.Now;
            dtKetQuaChiTiet.Status = true;
            _GPMB_KetQua_ChiTietService.Add(dtKetQuaChiTiet);
            _GPMB_KetQua_ChiTietService.Save();
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "GPMBKETQUA")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _GPMB_KetQuaService.Delete(id);
                    _GPMB_KetQuaService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }
    }
}
