﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hinhthucquanly")]
    [Authorize]
    public class HinhThucQuanLyController : ApiControllerBase
    {
        private IHinhThucQuanLyService _hinhThucQuanLyService;
        public HinhThucQuanLyController(IErrorService errorService, IHinhThucQuanLyService hinhThucQuanLyService) : base(errorService)
        {
            this._hinhThucQuanLyService = hinhThucQuanLyService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hinhThucQuanLyService.GetAll().OrderByDescending(x => x.IdHinhThucQuanLy);

                var modelVm = Mapper.Map<IEnumerable<HinhThucQuanLy>, IEnumerable<HinhThucQuanLyViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hinhThucQuanLyService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<HinhThucQuanLy>, IEnumerable<HinhThucQuanLyViewModels>>(model);

                PaginationSet<HinhThucQuanLyViewModels> pagedSet = new PaginationSet<HinhThucQuanLyViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hinhThucQuanLyService.GetById(id);

                var responseData = Mapper.Map<HinhThucQuanLy, HinhThucQuanLyViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage Create(HttpRequestMessage request, HinhThucQuanLyViewModels hinhThucQuanLyViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHinhThucQuanLy = new HinhThucQuanLy();
                    newHinhThucQuanLy.UpdateHinhThucQuanLy(hinhThucQuanLyViewModels);
                    newHinhThucQuanLy.CreatedDate = DateTime.Now;
                    newHinhThucQuanLy.CreatedBy = User.Identity.GetUserId();
                    _hinhThucQuanLyService.Add(newHinhThucQuanLy);
                    _hinhThucQuanLyService.Save();

                    var responseData = Mapper.Map<HinhThucQuanLy, HinhThucQuanLyViewModels>(newHinhThucQuanLy);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage Update(HttpRequestMessage request, HinhThucQuanLyViewModels hinhThucQuanLyViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbHinhThucQuanLy = _hinhThucQuanLyService.GetById(hinhThucQuanLyViewModels.IdHinhThucQuanLy);

                    dbHinhThucQuanLy.UpdateHinhThucQuanLy(hinhThucQuanLyViewModels);
                    dbHinhThucQuanLy.UpdatedDate = DateTime.Now;
                    dbHinhThucQuanLy.UpdatedBy = User.Identity.GetUserId();
                    _hinhThucQuanLyService.Update(dbHinhThucQuanLy);
                    _hinhThucQuanLyService.Save();

                    var responseData = Mapper.Map<HinhThucQuanLy, HinhThucQuanLyViewModels>(dbHinhThucQuanLy);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hinhThucQuanLyService.Delete(id);
                    _hinhThucQuanLyService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCQUANLY")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listHinhThucQuanLy = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listHinhThucQuanLy)
                    {
                        _hinhThucQuanLyService.Delete(item);
                    }

                    _hinhThucQuanLyService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listHinhThucQuanLy.Count);
                }

                return response;
            });
        }
    }
}
