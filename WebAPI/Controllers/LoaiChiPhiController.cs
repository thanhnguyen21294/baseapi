﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/loaichiphi")]
    [Authorize]
    public class LoaiChiPhiController : ApiControllerBase
    {
        private ILoaiChiPhiService _LoaiChiPhiService;
        public LoaiChiPhiController(IErrorService errorService, ILoaiChiPhiService LoaiChiPhiService) : base(errorService)
        {
            this._LoaiChiPhiService = LoaiChiPhiService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _LoaiChiPhiService.GetAll().OrderBy(x => x.Order);

                var modelVm = Mapper.Map<IEnumerable<LoaiChiPhi>, IEnumerable<LoaiChiPhiViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAICHIPHI")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _LoaiChiPhiService.GetByFilter(page, pageSize, out totalRow);

                var modelVm = Mapper.Map<IEnumerable<LoaiChiPhi>, IEnumerable<LoaiChiPhiViewModels>>(model);

                PaginationSet<LoaiChiPhiViewModels> pagedSet = new PaginationSet<LoaiChiPhiViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LOAICHIPHI")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _LoaiChiPhiService.GetById(id);

                var responseData = Mapper.Map<LoaiChiPhi, LoaiChiPhiViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LOAICHIPHI")]
        public HttpResponseMessage Create(HttpRequestMessage request, LoaiChiPhiViewModels LoaiChiPhiViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLoaiChiPhi = new LoaiChiPhi();
                    newLoaiChiPhi.UpdateLoaiChiPhi(LoaiChiPhiViewModels);
                    newLoaiChiPhi.CreatedDate = DateTime.Now;
                    newLoaiChiPhi.CreatedBy = User.Identity.GetUserId();
                    _LoaiChiPhiService.Add(newLoaiChiPhi);
                    _LoaiChiPhiService.Save();

                    var responseData = Mapper.Map<LoaiChiPhi, LoaiChiPhiViewModels>(newLoaiChiPhi);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LOAICHIPHI")]
        public HttpResponseMessage Update(HttpRequestMessage request, LoaiChiPhiViewModels LoaiChiPhiViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLoaiChiPhi = _LoaiChiPhiService.GetById(LoaiChiPhiViewModels.IdLoaiChiPhi);

                    dbLoaiChiPhi.UpdateLoaiChiPhi(LoaiChiPhiViewModels);
                    dbLoaiChiPhi.UpdatedDate = DateTime.Now;
                    dbLoaiChiPhi.UpdatedBy = User.Identity.GetUserId();
                    _LoaiChiPhiService.Update(dbLoaiChiPhi);
                    _LoaiChiPhiService.Save();

                    var responseData = Mapper.Map<LoaiChiPhi, LoaiChiPhiViewModels>(dbLoaiChiPhi);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAICHIPHI")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _LoaiChiPhiService.Delete(id);
                    _LoaiChiPhiService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LOAICHIPHI")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLoaiChiPhi = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLoaiChiPhi)
                    {
                        _LoaiChiPhiService.Delete(item);
                    }

                    _LoaiChiPhiService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLoaiChiPhi.Count);
                }

                return response;
            });
        }
    }
}
