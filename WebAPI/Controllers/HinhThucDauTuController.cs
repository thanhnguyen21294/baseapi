﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hinhthucdautu")]
    [Authorize]
    public class HinhThucDauTuController : ApiControllerBase
    {
        private IHinhThucDauTuService _hinhThucDauTuService;
        public HinhThucDauTuController(IErrorService errorService, IHinhThucDauTuService hinhThucDauTuService) : base(errorService)
        {
            this._hinhThucDauTuService = hinhThucDauTuService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hinhThucDauTuService.GetAll().OrderByDescending(x => x.IdHinhThucDauTu);

                var modelVm = Mapper.Map<IEnumerable<HinhThucDauTu>, IEnumerable<HinhThucDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hinhThucDauTuService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<HinhThucDauTu>, IEnumerable<HinhThucDauTuViewModels>>(model);

                PaginationSet<HinhThucDauTuViewModels> pagedSet = new PaginationSet<HinhThucDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hinhThucDauTuService.GetById(id);

                var responseData = Mapper.Map<HinhThucDauTu, HinhThucDauTuViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage Create(HttpRequestMessage request, HinhThucDauTuViewModels hinhThucDauTuViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHinhThucDauTu = new HinhThucDauTu();
                    newHinhThucDauTu.UpdateHinhThucDauTu(hinhThucDauTuViewModels);
                    newHinhThucDauTu.CreatedDate = DateTime.Now;
                    newHinhThucDauTu.CreatedBy = User.Identity.GetUserId();
                    _hinhThucDauTuService.Add(newHinhThucDauTu);
                    _hinhThucDauTuService.Save();

                    var responseData = Mapper.Map<HinhThucDauTu, HinhThucDauTuViewModels>(newHinhThucDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage Update(HttpRequestMessage request, HinhThucDauTuViewModels hinhThucDauTuViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbHinhThucDauTu = _hinhThucDauTuService.GetById(hinhThucDauTuViewModels.IdHinhThucDauTu);

                    dbHinhThucDauTu.UpdateHinhThucDauTu(hinhThucDauTuViewModels);
                    dbHinhThucDauTu.UpdatedDate = DateTime.Now;
                    dbHinhThucDauTu.UpdatedBy = User.Identity.GetUserId();
                    _hinhThucDauTuService.Update(dbHinhThucDauTu);
                    _hinhThucDauTuService.Save();

                    var responseData = Mapper.Map<HinhThucDauTu, HinhThucDauTuViewModels>(dbHinhThucDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hinhThucDauTuService.Delete(id);
                    _hinhThucDauTuService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HINHTHUCDAUTU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listHinhThucDauTu = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listHinhThucDauTu)
                    {
                        _hinhThucDauTuService.Delete(item);
                    }

                    _hinhThucDauTuService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listHinhThucDauTu.Count);
                }

                return response;
            });
        }
    }
}
