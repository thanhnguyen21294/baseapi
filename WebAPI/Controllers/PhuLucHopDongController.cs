﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/phuluchopdong")]
    [Authorize]
    public class PhuLucHopDongController : ApiControllerBase
    {
        private IPhuLucHopDongService _phuLucHopDongService;
        public PhuLucHopDongController(IErrorService errorService, IPhuLucHopDongService phuLucHopDongService) : base(errorService)
        {
            this._phuLucHopDongService = phuLucHopDongService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _phuLucHopDongService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<PhuLucHopDong>, IEnumerable<PhuLucHopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idHopDong, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _phuLucHopDongService.GetByFilter(idHopDong, page, pageSize, out totalRow, filter).OrderByDescending(x=>x.NgayKy);

                var modelVm = Mapper.Map<IEnumerable<PhuLucHopDong>, IEnumerable<PhuLucHopDongViewModels>>(model);

                PaginationSet<PhuLucHopDongViewModels> pagedSet = new PaginationSet<PhuLucHopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _phuLucHopDongService.GetById(id);

                var responseData = Mapper.Map<PhuLucHopDong, PhuLucHopDongViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, PhuLucHopDongViewModels phuLucHopDongtVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newPhuLucHopDong = new PhuLucHopDong();
                    newPhuLucHopDong.UpdatePhuLucHopDong(phuLucHopDongtVm);
                    newPhuLucHopDong.CreatedDate = DateTime.Now;
                    newPhuLucHopDong.CreatedBy = User.Identity.GetUserId();
                    _phuLucHopDongService.Add(newPhuLucHopDong);
                    _phuLucHopDongService.Save();

                    var responseData = Mapper.Map<PhuLucHopDong, PhuLucHopDongViewModels>(newPhuLucHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, PhuLucHopDongViewModels phuLucHopDongVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbPhuLucHopDong = _phuLucHopDongService.GetById(phuLucHopDongVm.IdPhuLuc);

                    dbPhuLucHopDong.UpdatePhuLucHopDong(phuLucHopDongVm);
                    dbPhuLucHopDong.UpdatedDate = DateTime.Now;
                    dbPhuLucHopDong.UpdatedBy = User.Identity.GetUserId();
                    _phuLucHopDongService.Update(dbPhuLucHopDong);
                    _phuLucHopDongService.Save();

                    var responseData = Mapper.Map<PhuLucHopDong, PhuLucHopDongViewModels>(dbPhuLucHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phuLucHopDongService.Delete(id);
                    _phuLucHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listPhuLucHopDong = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listPhuLucHopDong)
                    {
                        _phuLucHopDongService.Delete(item);
                    }

                    _phuLucHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listPhuLucHopDong.Count);
                }

                return response;
            });
        }
    }
}
