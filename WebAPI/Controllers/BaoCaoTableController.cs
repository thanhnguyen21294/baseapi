﻿using Service;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Providers;
using System.Collections.Generic;
using System.Linq;
using Model.Models;
using System.IO;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/baocaotable")]
    [Authorize]
    public class BaoCaoTableController : ApiControllerBase
    {
        private IBaoCaoTableService _BaoCaoTableService;
        public BaoCaoTableController(IErrorService errorService, IBaoCaoTableService BaoCaoTableService) : base(errorService)
        {
            this._BaoCaoTableService = BaoCaoTableService;
        }
        [Route("phanloai")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "BaoCaoTable")]
        public HttpResponseMessage GetByPhanLoai(HttpRequestMessage request, string phanloai, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                //int totalRow = 0;
                var baocaotables = _BaoCaoTableService.GetByPhanLoai(phanloai);
                var modelData = new
                {
                    Items = baocaotables.Skip(pageSize * (page - 1)).Take(pageSize),
                    Total = baocaotables.Count(),
                };
                //var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, modelData);

                return response;
            });
        }

        //[Route("readhtml")]
        //[HttpGet]
        //public void ReadHTML(string ten)
        //{
        //    Workbook workbook = new Workbook();
        //    var folderReport = "/ReportFolder/";
        //    var templates = "/Templates/";
        //    string filePath = HttpContext.Current.Server.MapPath(folderReport);
        //    string filePathtemplates = HttpContext.Current.Server.MapPath(templates);
        //    workbook.LoadFromFile(filePath+ ten);
        //    //convert Excel to HTML 
        //    Worksheet sheet = workbook.Worksheets[0];
        //    sheet.SaveToHtml(filePathtemplates + "BaoCaoHtml.html");

        //        //Preview HTML 
        //    System.Diagnostics.Process.Start(filePathtemplates+ "BaoCaoHtml.html");
        //    //return CreateHttpResponse(request, () =>
        //    //{
        //    //    //int totalRow = 0;
        //    //    var model = _BaoCaoTableService.GetAll();

        //    //    //var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(model);

        //    //    var response = request.CreateResponse(HttpStatusCode.OK, model);

        //    //    return response;
        //    //});
        //}

        [Route("delete")]
        [HttpPost]
        //[Permission(Action = "Delete", Function = "BAOCAOTABLE")]
        public HttpResponseMessage Delete(HttpRequestMessage request, BaoCaoTable baoCaoTable)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _BaoCaoTableService.Delete(baoCaoTable.IdBaoCao);
                    _BaoCaoTableService.Save();
                    var folderReport = "/ReportFolder/";
                    string filePath = HttpContext.Current.Server.MapPath(folderReport);
                    string fullPath = filePath + baoCaoTable.Ten;
                    File.Delete(fullPath);

                    response = request.CreateResponse(HttpStatusCode.Created, baoCaoTable.IdBaoCao);
                }

                return response;
            });
        }


        //[Route("getall")]
        //[HttpGet]
        //public HttpResponseMessage GetAll(HttpRequestMessage request)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        var model = _BaoCaoTableService.GetAll();

        //        //var modelVm = Mapper.Map<IEnumerable<BaoCaoTable>, IEnumerable<BaoCaoTableViewModels>>(model);

        //        response = request.CreateResponse(HttpStatusCode.OK, model);

        //        return response;
        //    });
        //}

        //[Route("getlistpaging")]
        //[HttpGet]
        //[Permission(Action = "Read", Function = "BaoCaoTable")]
        //public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        int totalRow = 0;

        //        var model = _BaoCaoTableService.GetByFilter(page, pageSize, out totalRow, filter);

        //        var modelVm = Mapper.Map<IEnumerable<BaoCaoTable>, IEnumerable<BaoCaoTableViewModels>>(model);

        //        PaginationSet<BaoCaoTableViewModels> pagedSet = new PaginationSet<BaoCaoTableViewModels>()
        //        {
        //            PageIndex = page,
        //            TotalRows = totalRow,
        //            PageSize = pageSize,
        //            Items = modelVm
        //        };

        //        response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

        //        return response;
        //    });
        //}

        //[Route("getbyid/{id}")]
        //[HttpGet]
        //[Permission(Action = "Read", Function = "BaoCaoTable")]
        //public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        var model = _BaoCaoTableService.GetById(id);

        //        //var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(model);

        //        var response = request.CreateResponse(HttpStatusCode.OK, model);

        //        return response;
        //    });
        //}



        //[Route("add")]
        //[HttpPost]
        //[Permission(Action = "Create", Function = "BaoCaoTable")]
        //public HttpResponseMessage Create(HttpRequestMessage request, BaoCaoTableViewModels baoCaoTableViewModels)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            var newBaoCaoTable = new BaoCaoTable();
        //            //newBaoCaoTable.UpdateBaoCaoTable(BaoCaoTableViewModels);
        //            newBaoCaoTable.Ten = baoCaoTableViewModels.Ten;
        //            newBaoCaoTable.MaBaoCao = baoCaoTableViewModels.MaBaoCao;
        //            newBaoCaoTable.NoiDung = baoCaoTableViewModels.NoiDung;
        //            newBaoCaoTable.CreatedDate = DateTime.Now;
        //            newBaoCaoTable.CreatedBy = User.Identity.GetUserId();
        //            newBaoCaoTable.PhanLoai = baoCaoTableViewModels.PhanLoai;
        //            newBaoCaoTable.URL = baoCaoTableViewModels.URL;
        //            _BaoCaoTableService.Add(newBaoCaoTable);
        //            _BaoCaoTableService.Save();

        //            var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(newBaoCaoTable);
        //            response = request.CreateResponse(HttpStatusCode.Created, responseData);
        //        }

        //        return response;
        //    });
        //}

        //[Route("update")]
        //[HttpPut]
        //[Permission(Action = "Update", Function = "BaoCaoTable")]
        //public HttpResponseMessage Update(HttpRequestMessage request, BaoCaoTableViewModels BaoCaoTableViewModels)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            var dbBaoCaoTable = _BaoCaoTableService.GetById(BaoCaoTableViewModels.IdBaoCao);

        //            //dbBaoCaoTable.UpdateBaoCaoTable(BaoCaoTableViewModels);
        //            dbBaoCaoTable.CreatedDate = DateTime.Now;
        //            dbBaoCaoTable.CreatedBy = User.Identity.GetUserId();
        //            _BaoCaoTableService.Update(dbBaoCaoTable);
        //            _BaoCaoTableService.Save();

        //            var responseData = Mapper.Map<BaoCaoTable, BaoCaoTableViewModels>(dbBaoCaoTable);
        //            response = request.CreateResponse(HttpStatusCode.Created, responseData);
        //        }
        //        return response;
        //    });
        //}



        //[Route("deletemulti")]
        //[HttpDelete]
        //[Permission(Action = "Delete", Function = "BaoCaoTable")]
        //public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            var listBaoCaoTable = new JavaScriptSerializer().Deserialize<List<int>>(listid);
        //            foreach (var item in listBaoCaoTable)
        //            {
        //                _BaoCaoTableService.Delete(item);
        //            }

        //            _BaoCaoTableService.Save();

        //            response = request.CreateResponse(HttpStatusCode.OK, listBaoCaoTable.Count);
        //        }

        //        return response;
        //    });
        //}


        //public static void Main(BaoCaoTable baocaoTable)
        //{
        //    // If using Professional version, put your serial key below.
        //    //SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

        //    ExcelFile ef = ExcelFile.Load(baocaoTable.URL);

        //    var ws = ef.Worksheets[0];

        //    // Some of the properties from ExcelPrintOptions class are supported in HTML export.
        //    ws.PrintOptions.PrintHeadings = true;
        //    ws.PrintOptions.PrintGridlines = true;

        //    // Print area can be used to specify custom cell range which should be exported to HTML.
        //    ws.NamedRanges.SetPrintArea(ws.Cells.GetSubrange("A1", "I42"));

        //    HtmlSaveOptions options = new HtmlSaveOptions()
        //    {
        //        HtmlType = HtmlType.Html,
        //        SelectionType = SelectionType.EntireFile
        //    };

        //    ef.Save("HtmlExport.html", options);
        //}


        //[Route("convertbc/{vbdaId}")]
        //[HttpGet]
        //[Permission(Action = "Read", Function = "CONVERTFILE")]
        //public HttpResponseMessage ConvertFileBC(HttpRequestMessage request, int vbdaId)
        //{
        //    BaoCaoTable baocaoTable = this._BaoCaoTableService.GetById(vbdaId);

        //    var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(baocaoTable.URL));

        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        ExcelFile ef = ExcelFile.Load(path);

        //        var ws = ef.Worksheets[0];

        //        // Some of the properties from ExcelPrintOptions class are supported in HTML export.
        //        ws.PrintOptions.PrintHeadings = true;
        //        ws.PrintOptions.PrintGridlines = true;

        //        // Print area can be used to specify custom cell range which should be exported to HTML.
        //        ws.NamedRanges.SetPrintArea(ws.Cells.GetSubrange("A1", "I42"));

        //        HtmlSaveOptions options = new HtmlSaveOptions()
        //        {
        //            HtmlType = HtmlType.Html,
        //            SelectionType = SelectionType.EntireFile
        //        };

        //        ef.Save("HtmlExport.html", options);
        //        response = request.CreateResponse(HttpStatusCode.OK, options);

        //        return response;
        //    });

        //}
    }
}
