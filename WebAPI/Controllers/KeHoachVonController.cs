﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.KeHoachVon;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/kehoachvon")]
    public class KeHoachVonController : ApiControllerBase
    {
        private IKeHoachVonService _KeHoachVonService;
        private INguonVonKeHoachVonService _NguonVonKeHoachVonService;
        private ICoQuanPheDuyetService _CoQuanPheDuyetService;

        public KeHoachVonController(IErrorService errorService, IKeHoachVonService kehoachvonService, INguonVonKeHoachVonService nguonVonKeHoachVonService, ICoQuanPheDuyetService coQuanPheDuyetService) : base(errorService)
        {
            this._KeHoachVonService = kehoachvonService;
            _NguonVonKeHoachVonService = nguonVonKeHoachVonService;
            this._CoQuanPheDuyetService = coQuanPheDuyetService;
        }

        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHVON")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _KeHoachVonService.GetAll(filter).OrderByDescending(x => x.IdKeHoachVon);

                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getalltoactionhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllToActionHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _KeHoachVonService.GetAll().Where(x=>x.IdKeHoachVonDieuChinh==null);

                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("getallkhvdctoactionhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllKHVDCToActionHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _KeHoachVonService.GetAll().Where(x => x.IdKeHoachVonDieuChinh != null);

                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHVON")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _KeHoachVonService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdKeHoachVon);

                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHVON")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _KeHoachVonService.GetByFilter(idDA, page, pageSize, "", out totalRow, filter);
                foreach (var item in model)
                {
                    int idCoQuanPheDuyet = (item.IdCoQuanPheDuyet ?? 0);
                    item.CoQuanPheDuyet = _CoQuanPheDuyetService.GetById(idCoQuanPheDuyet);
                }
                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                PaginationSet<KeHoachVonViewModel> pagedSet = new PaginationSet<KeHoachVonViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [HttpGet]
        [Route("getbyidparent")]
        public HttpResponseMessage GetByIdParent(HttpRequestMessage request, int idKeHoachVon, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _KeHoachVonService.GetKeHoachVonDieuChinhById(idKeHoachVon, page, pageSize, "", out totalRow, filter).Where(x => x.IdKeHoachVonDieuChinh == idKeHoachVon);
                foreach (var item in model)
                {
                    int idCoQuanPheDuyet = (item.IdCoQuanPheDuyet ?? 0);
                    item.CoQuanPheDuyet = _CoQuanPheDuyetService.GetById(idCoQuanPheDuyet);
                }
                var modelVm = Mapper.Map<IEnumerable<KeHoachVon>, IEnumerable<KeHoachVonViewModel>>(model);

                PaginationSet<KeHoachVonViewModel> pagedSet = new PaginationSet<KeHoachVonViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "KEHOACHVON")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _KeHoachVonService.GetById(id);

                var responseData = Mapper.Map<KeHoachVon, KeHoachVonViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "KEHOACHVON")]
        public HttpResponseMessage Create(HttpRequestMessage request, KeHoachVonViewModel vmKeHoachVon)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newKeHoachVon = new KeHoachVon();
                    newKeHoachVon.SoQuyetDinh = vmKeHoachVon.SoQuyetDinh;
                    newKeHoachVon.IdDuAn = vmKeHoachVon.IdDuAn;// vmKeHoachVon.IdDuAn;
                    newKeHoachVon.IdKeHoachVonDieuChinh = vmKeHoachVon.IdKeHoachVonDieuChinh;
                    newKeHoachVon.IdCoQuanPheDuyet = vmKeHoachVon.IdCoQuanPheDuyet;
                    if (vmKeHoachVon.NgayPheDuyet != "" && vmKeHoachVon.NgayPheDuyet != null)
                        newKeHoachVon.NgayPheDuyet = DateTime.ParseExact(vmKeHoachVon.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newKeHoachVon.NienDo = vmKeHoachVon.NienDo;
                    newKeHoachVon.TinhChatKeHoach = vmKeHoachVon.TinhChatKeHoach;
                    newKeHoachVon.TongGiaTri = vmKeHoachVon.TongGiaTri;
                    newKeHoachVon.XayLap = vmKeHoachVon.XayLap;
                    newKeHoachVon.ThietBi = vmKeHoachVon.ThietBi;
                    newKeHoachVon.GiaiPhongMatBang = vmKeHoachVon.GiaiPhongMatBang;
                    newKeHoachVon.TuVan = vmKeHoachVon.TuVan;
                    newKeHoachVon.QuanLyDuAn = vmKeHoachVon.QuanLyDuAn;
                    newKeHoachVon.Khac = vmKeHoachVon.Khac;
                    newKeHoachVon.DuPhong = vmKeHoachVon.DuPhong;
                    newKeHoachVon.GhiChu = vmKeHoachVon.GhiChu;
                    newKeHoachVon.DeXuatDieuChinh = vmKeHoachVon.DeXuatDieuChinh;
                    newKeHoachVon.CreatedDate = DateTime.Now;
                    newKeHoachVon.CreatedBy = User.Identity.GetUserId();
                    newKeHoachVon.Status = true;

                    List<NguonVonKeHoachVon> lstNguonVonKHV = new List<NguonVonKeHoachVon>();
                    foreach (var item in vmKeHoachVon.NguonVonKeHoachVons)
                    {
                        lstNguonVonKHV.Add(new NguonVonKeHoachVon
                        {
                            IdNguonVonDuAn = item.IdNguonVonDuAn,
                            IdKeHoachVon = item.IdKeHoachVon,
                            GiaTri = item.GiaTri,
                            GiaTriTabmis = item.GiaTriTabmis,
                            Status = true
                        });
                    }
                    newKeHoachVon.NguonVonKeHoachVons = lstNguonVonKHV;

                    _KeHoachVonService.Add(newKeHoachVon);
                    _KeHoachVonService.Save();

                    var responseData = Mapper.Map<KeHoachVon, KeHoachVonViewModel>(newKeHoachVon);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "KEHOACHVON")]
        public HttpResponseMessage Update(HttpRequestMessage request, KeHoachVonViewModel vmKeHoachVon)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoachVon = _KeHoachVonService.GetById(vmKeHoachVon.IdKeHoachVon);
                    updateKeHoachVon.SoQuyetDinh = vmKeHoachVon.SoQuyetDinh;
                    updateKeHoachVon.IdDuAn = vmKeHoachVon.IdDuAn;// vmKeHoachVon.IdDuAn;
                    updateKeHoachVon.IdKeHoachVonDieuChinh = vmKeHoachVon.IdKeHoachVonDieuChinh;
                    updateKeHoachVon.IdCoQuanPheDuyet = vmKeHoachVon.IdCoQuanPheDuyet;
                    if(vmKeHoachVon.NgayPheDuyet != "" && vmKeHoachVon.NgayPheDuyet != null)
                    {
                        updateKeHoachVon.NgayPheDuyet = DateTime.ParseExact(vmKeHoachVon.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayPheDuyet = null;
                    }
                    updateKeHoachVon.NienDo = vmKeHoachVon.NienDo;
                    updateKeHoachVon.TinhChatKeHoach = vmKeHoachVon.TinhChatKeHoach;
                    updateKeHoachVon.TongGiaTri = vmKeHoachVon.TongGiaTri;
                    updateKeHoachVon.XayLap = vmKeHoachVon.XayLap;
                    updateKeHoachVon.ThietBi = vmKeHoachVon.ThietBi;
                    updateKeHoachVon.GiaiPhongMatBang = vmKeHoachVon.GiaiPhongMatBang;
                    updateKeHoachVon.TuVan = vmKeHoachVon.TuVan;
                    updateKeHoachVon.QuanLyDuAn = vmKeHoachVon.QuanLyDuAn;
                    updateKeHoachVon.Khac = vmKeHoachVon.Khac;
                    updateKeHoachVon.DuPhong = vmKeHoachVon.DuPhong;
                    updateKeHoachVon.GhiChu = vmKeHoachVon.GhiChu;
                    updateKeHoachVon.DeXuatDieuChinh = vmKeHoachVon.DeXuatDieuChinh;
                    updateKeHoachVon.UpdatedDate = DateTime.Now;
                    updateKeHoachVon.UpdatedBy = User.Identity.GetUserId();

                    try
                    {
                        _NguonVonKeHoachVonService.DeleteByIdKeHoachVon(vmKeHoachVon.IdKeHoachVon);
                        foreach (var item in vmKeHoachVon.NguonVonKeHoachVons)
                        {
                            _NguonVonKeHoachVonService.Add(new NguonVonKeHoachVon
                            {
                                IdNguonVonDuAn = item.IdNguonVonDuAn,
                                IdKeHoachVon = vmKeHoachVon.IdKeHoachVon,
                                GiaTri = item.GiaTri,
                                GiaTriTabmis = item.GiaTriTabmis,
                                Status = true
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _KeHoachVonService.Update(updateKeHoachVon);
                    _KeHoachVonService.Save();

                    var responseData = Mapper.Map<KeHoachVon, KeHoachVonViewModel>(updateKeHoachVon);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHVON")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonKeHoachVonService.DeleteByIdKeHoachVon(id);
                    _KeHoachVonService.Delete(id);
                    _KeHoachVonService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "KEHOACHVON")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lstKeHoachVon = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in lstKeHoachVon)
                    {
                        _NguonVonKeHoachVonService.DeleteByIdKeHoachVon(item);
                        _KeHoachVonService.Delete(item);
                    }
                    _KeHoachVonService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, lstKeHoachVon.Count);
                }

                return response;
            });
        }
    }
}