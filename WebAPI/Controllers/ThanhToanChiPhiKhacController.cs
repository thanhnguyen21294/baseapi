﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/thanhtoanchiphikhac")]
    public class ThanhToanChiPhiKhacController : ApiControllerBase
    {
        private IThanhToanChiPhiKhacService _ThanhToanChiPhiKhacService;
        private INguonVonThanhToanChiPhiKhacService _NguonVonThanhToanChiPhiKhacServce;
        private INguonVonDuAnService _NguonVonDuAnService;
        private INguonVonService _NguonVonService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        public ThanhToanChiPhiKhacController(IErrorService errorService, IThanhToanChiPhiKhacService thanhToanChiPhiKhac, INguonVonThanhToanChiPhiKhacService nguonVonThanhToanChiPhiKhacServce
            ,INguonVonDuAnService nguonVonDuAnService, INguonVonService nguonVonService, IDanhMucNhaThauService danhMucNhaThauService) : base(errorService)
            {
            this._ThanhToanChiPhiKhacService = thanhToanChiPhiKhac;
            _NguonVonThanhToanChiPhiKhacServce = nguonVonThanhToanChiPhiKhacServce;
            _NguonVonDuAnService = nguonVonDuAnService;
            _NguonVonService = nguonVonService;
            _danhMucNhaThauService = danhMucNhaThauService;
        }
        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanChiPhiKhacService.GetAll(filter).OrderByDescending(x => x.IdThanhToanChiPhiKhac);
                 
                var modelVm = Mapper.Map<IEnumerable<ThanhToanChiPhiKhac>, IEnumerable<ThanhToanChiPhiKhacViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanChiPhiKhacService.GetAll().OrderByDescending(x => x.IdThanhToanChiPhiKhac);

                var modelVm = Mapper.Map<IEnumerable<ThanhToanChiPhiKhac>, IEnumerable<ThanhToanChiPhiKhacViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanChiPhiKhacService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdThanhToanChiPhiKhac);

                var modelVm = Mapper.Map<IEnumerable<ThanhToanChiPhiKhac>, IEnumerable<ThanhToanChiPhiKhacViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getyear")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetYear(HttpRequestMessage request, int year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ThanhToanChiPhiKhacService.GetYear(year).OrderByDescending(x => x.IdThanhToanChiPhiKhac);

                //var modelVm = Mapper.Map<IEnumerable<ThanhToanChiPhiKhac>, IEnumerable<ThanhToanChiPhiKhacViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
             var nguonVonThanhToanCPKs = _NguonVonThanhToanChiPhiKhacServce.GetAll();
             var thanhToanCPKs= _ThanhToanChiPhiKhacService.GetByFilter(idDA, page, pageSize, "", out totalRow, filter);
             var query=   thanhToanCPKs.GroupJoin(nguonVonThanhToanCPKs, x => x.IdThanhToanChiPhiKhac, y => y.IdThanhToanChiPhiKhac, (x, grp) =>
                new ThanhToanChiPhiKhacViewModels {
                    IdThanhToanChiPhiKhac = x.IdThanhToanChiPhiKhac,
                    IdDuAn = x.IdDuAn,
                    NoiDung = x.NoiDung,
                    IdNhaThau = x.IdNhaThau,
                    LoaiThanhToan = x.LoaiThanhToan,
                    NgayThanhToan = x.NgayThanhToan.ToString(),
                    TrangThaiGui = x.TrangThaiGui,
                    GhiChu = x.GhiChu,
                    LoaiChiPhi = x.LoaiChiPhi != null ? x.LoaiChiPhi : null,
                    TongGiaTriNguonVon = grp.Select(a => a.GiaTri).Sum(),
                    TenNguonVons = grp.Join(_NguonVonDuAnService.GetAll(), a => a.IdNguonVonDuAn, b => b.IdNguonVonDuAn, (a, b) => new {
                        IdNguonVon = b.IdNguonVon
                    }).Join(_NguonVonService.GetAll(), a => a.IdNguonVon, b => b.IdNguonVon, (a, b) => new {
                        TenNguonVon = b.TenNguonVon
                    }).Select(c => c.TenNguonVon),
                });




                PaginationSet<ThanhToanChiPhiKhacViewModels> pagedSet = new PaginationSet<ThanhToanChiPhiKhacViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = query,
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ThanhToanChiPhiKhacService.GetById(id);
                
                var responseData = Mapper.Map<ThanhToanChiPhiKhac, ThanhToanChiPhiKhacViewModels>(model);
                responseData.TongGiaTriNguonVon = (model.NguonVonThanhToanChiPhiKhacs!=null)?model.NguonVonThanhToanChiPhiKhacs.Select(x => x.GiaTri).Sum():0;
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage Create(HttpRequestMessage request, ThanhToanChiPhiKhacViewModels vmThanhToanChiPhiKhac)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThanhToanChiPhiKhac = new ThanhToanChiPhiKhac();
                    newThanhToanChiPhiKhac.IdDuAn = vmThanhToanChiPhiKhac.IdDuAn;
                    newThanhToanChiPhiKhac.NoiDung = vmThanhToanChiPhiKhac.NoiDung;
                    newThanhToanChiPhiKhac.IdNhaThau = vmThanhToanChiPhiKhac.IdNhaThau;
                    newThanhToanChiPhiKhac.LoaiThanhToan = vmThanhToanChiPhiKhac.LoaiThanhToan;
                    newThanhToanChiPhiKhac.IdLoaiChiPhi = vmThanhToanChiPhiKhac.IdLoaiChiPhi;
                    if (vmThanhToanChiPhiKhac.NgayThanhToan != "" && vmThanhToanChiPhiKhac.NgayThanhToan != null)
                        newThanhToanChiPhiKhac.NgayThanhToan = DateTime.ParseExact(vmThanhToanChiPhiKhac.NgayThanhToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newThanhToanChiPhiKhac.GhiChu = vmThanhToanChiPhiKhac.GhiChu;
                    newThanhToanChiPhiKhac.CreatedDate = DateTime.Now;
                    newThanhToanChiPhiKhac.CreatedBy = User.Identity.GetUserId();
                    newThanhToanChiPhiKhac.Status = true;

                    List<NguonVonThanhToanChiPhiKhac> lstNguonVonQTDA = new List<NguonVonThanhToanChiPhiKhac>();
                    if (vmThanhToanChiPhiKhac.NguonVonThanhToanChiPhiKhacs.Count() > 0)
                    {
                        foreach (var item in vmThanhToanChiPhiKhac.NguonVonThanhToanChiPhiKhacs)
                        {
                            _NguonVonThanhToanChiPhiKhacServce.Add(new NguonVonThanhToanChiPhiKhac
                            {
                                IdNguonVonDuAn = item.IdNguonVonDuAn,
                                GiaTri = item.GiaTri,
                                GiaTriTabmis = item.GiaTriTabmis,
                                Status = true
                            });
                        }
                    }
                    try
                    {
                        var query =_danhMucNhaThauService .GetAll(vmThanhToanChiPhiKhac.IdDuAn).Where(x =>

                            x.TenNhaThau.Contains(vmThanhToanChiPhiKhac.DanhMucNhaThau.TenNhaThau));
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = vmThanhToanChiPhiKhac.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            newThanhToanChiPhiKhac.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhmuc = query.SingleOrDefault();
                            newThanhToanChiPhiKhac.IdNhaThau = danhmuc.IdNhaThau;
                            danhmuc.DiaChiNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _ThanhToanChiPhiKhacService.Add(newThanhToanChiPhiKhac);
                    _ThanhToanChiPhiKhacService.Save();

                    var responseData = Mapper.Map<ThanhToanChiPhiKhac, ThanhToanChiPhiKhacViewModels>(newThanhToanChiPhiKhac);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThanhToanChiPhiKhacViewModels vmThanhToanChiPhiKhac)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThanhToanChiPhiKhac = _ThanhToanChiPhiKhacService.GetById(vmThanhToanChiPhiKhac.IdThanhToanChiPhiKhac);
                    updateThanhToanChiPhiKhac.IdDuAn = vmThanhToanChiPhiKhac.IdDuAn;
                    updateThanhToanChiPhiKhac.NoiDung = vmThanhToanChiPhiKhac.NoiDung;
                    updateThanhToanChiPhiKhac.IdNhaThau = vmThanhToanChiPhiKhac.IdNhaThau;
                    updateThanhToanChiPhiKhac.LoaiThanhToan = vmThanhToanChiPhiKhac.LoaiThanhToan;
                    updateThanhToanChiPhiKhac.IdLoaiChiPhi = vmThanhToanChiPhiKhac.IdLoaiChiPhi;
                    if (vmThanhToanChiPhiKhac.NgayThanhToan != "" && vmThanhToanChiPhiKhac.NgayThanhToan != null)
                    {
                        updateThanhToanChiPhiKhac.NgayThanhToan = DateTime.ParseExact(vmThanhToanChiPhiKhac.NgayThanhToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateThanhToanChiPhiKhac.NgayThanhToan = null;
                    }
                        
                    updateThanhToanChiPhiKhac.GhiChu = vmThanhToanChiPhiKhac.GhiChu;
                    updateThanhToanChiPhiKhac.UpdatedDate = DateTime.Now;
                    updateThanhToanChiPhiKhac.UpdatedBy = User.Identity.GetUserId();

                    try
                    {
                        if (vmThanhToanChiPhiKhac.NguonVonThanhToanChiPhiKhacs.Count() > 0)
                        {
                            _NguonVonThanhToanChiPhiKhacServce.DeleteByIdThanhToanChiPhiKhac(vmThanhToanChiPhiKhac.IdThanhToanChiPhiKhac);
                            foreach (var item in vmThanhToanChiPhiKhac.NguonVonThanhToanChiPhiKhacs)
                            {
                                _NguonVonThanhToanChiPhiKhacServce.Add(new NguonVonThanhToanChiPhiKhac
                                {
                                    IdNguonVonDuAn = item.IdNguonVonDuAn,
                                    IdThanhToanChiPhiKhac = vmThanhToanChiPhiKhac.IdThanhToanChiPhiKhac,
                                    GiaTri = item.GiaTri,
                                    GiaTriTabmis = item.GiaTriTabmis,
                                    Status = true
                                });
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    try
                    {
                        var query = _danhMucNhaThauService.GetAll(vmThanhToanChiPhiKhac.IdDuAn)
                            .Where(x => x.TenNhaThau == vmThanhToanChiPhiKhac.DanhMucNhaThau.TenNhaThau);
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = vmThanhToanChiPhiKhac.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            updateThanhToanChiPhiKhac.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhMuc = query.SingleOrDefault();
                            updateThanhToanChiPhiKhac.IdNhaThau = danhMuc.IdNhaThau;
                            danhMuc.DiaChiNhaThau = vmThanhToanChiPhiKhac.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _ThanhToanChiPhiKhacService.Update(updateThanhToanChiPhiKhac);
                    _ThanhToanChiPhiKhacService.Save();

                    var responseData = Mapper.Map<ThanhToanChiPhiKhac, ThanhToanChiPhiKhacViewModels>(updateThanhToanChiPhiKhac);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateThanhToanChiPhiKhac = _ThanhToanChiPhiKhacService.GetById(vnTrangThaiUp.ID);
                    updateThanhToanChiPhiKhac.TrangThaiGui = 1;
                    updateThanhToanChiPhiKhac.UpdatedDate = DateTime.Now;
                    updateThanhToanChiPhiKhac.UpdatedBy = User.Identity.GetUserId();
                    _ThanhToanChiPhiKhacService.Update(updateThanhToanChiPhiKhac);
                    _ThanhToanChiPhiKhacService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonThanhToanChiPhiKhacServce.DeleteByIdThanhToanChiPhiKhac(id);
                    _ThanhToanChiPhiKhacService.Delete(id);
                    _ThanhToanChiPhiKhacService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lstThanhToanChiPhiKhac = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in lstThanhToanChiPhiKhac)
                    {
                        _NguonVonThanhToanChiPhiKhacServce.DeleteByIdThanhToanChiPhiKhac(item);
                        _ThanhToanChiPhiKhacService.Delete(item);
                    }
                    _ThanhToanChiPhiKhacService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, lstThanhToanChiPhiKhac.Count);
                }

                return response;
            });
        }
    }
}
