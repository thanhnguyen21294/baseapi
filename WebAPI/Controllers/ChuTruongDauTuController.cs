﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/chutruongdautu")]
    public class ChuTruongDauTuController : ApiControllerBase
    {
        private IChuTruongDauTuService _ChuTruongDauTuService;
        public ChuTruongDauTuController(IErrorService errorService, IChuTruongDauTuService chutruongdautuService) : base(errorService)
        {
            _ChuTruongDauTuService = chutruongdautuService;
        }
        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ChuTruongDauTuService.GetAll(filter).OrderByDescending(x => x.IdChuTruongDauTu);

                var modelVm = Mapper.Map<IEnumerable<ChuTruongDauTu>, IEnumerable<ChuTruongDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getalltoactionhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllToActionHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _ChuTruongDauTuService.GetAll().OrderByDescending(x => x.IdChuTruongDauTu);

                var modelVm = Mapper.Map<IEnumerable<ChuTruongDauTu>, IEnumerable<ChuTruongDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _ChuTruongDauTuService.GetByFilter(idDA, page, pageSize, "", out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<ChuTruongDauTu>, IEnumerable<ChuTruongDauTuViewModels>>(model);

                PaginationSet<ChuTruongDauTuViewModels> pagedSet = new PaginationSet<ChuTruongDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ChuTruongDauTuService.GetById(id);

                var responseData = Mapper.Map<ChuTruongDauTu, ChuTruongDauTuViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage Create(HttpRequestMessage request, ChuTruongDauTuViewModels vmChuTruongDauTu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newChuTruongDauTu = new ChuTruongDauTu();
                    newChuTruongDauTu.SoVanBan = vmChuTruongDauTu.SoVanBan;
                    newChuTruongDauTu.IdDuAn = vmChuTruongDauTu.IdDuAn;
                    newChuTruongDauTu.IdCoQuanPheDuyet = vmChuTruongDauTu.IdCoQuanPheDuyet;
                    if (vmChuTruongDauTu.NgayPheDuyet != "" && vmChuTruongDauTu.NgayPheDuyet != null)
                        newChuTruongDauTu.NgayPheDuyet = DateTime.ParseExact(vmChuTruongDauTu.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newChuTruongDauTu.NguoiPheDuyet = vmChuTruongDauTu.NguoiPheDuyet;
                    newChuTruongDauTu.TongMucDauTu = vmChuTruongDauTu.TongMucDauTu;
                    newChuTruongDauTu.KinhPhi = vmChuTruongDauTu.KinhPhi;
                    newChuTruongDauTu.LoaiDieuChinh = vmChuTruongDauTu.LoaiDieuChinh;
                    newChuTruongDauTu.GhiChu = vmChuTruongDauTu.GhiChu;
                    newChuTruongDauTu.UpdatedDate = DateTime.Now;
                    newChuTruongDauTu.UpdatedBy = User.Identity.GetUserId();
                    newChuTruongDauTu.Status = true;
                    newChuTruongDauTu.CreatedDate = DateTime.Now;
                    newChuTruongDauTu.CreatedBy = User.Identity.GetUserId();

                    _ChuTruongDauTuService.Add(newChuTruongDauTu);
                    _ChuTruongDauTuService.Save();

                    var responseData = Mapper.Map<ChuTruongDauTu, ChuTruongDauTuViewModels>(newChuTruongDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage Update(HttpRequestMessage request, ChuTruongDauTuViewModels vmChuTruongDauTu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateChuTruongDauTu = _ChuTruongDauTuService.GetById(vmChuTruongDauTu.IdChuTruongDauTu);
                    updateChuTruongDauTu.SoVanBan = vmChuTruongDauTu.SoVanBan;
                    updateChuTruongDauTu.IdDuAn = vmChuTruongDauTu.IdDuAn;
                    updateChuTruongDauTu.IdCoQuanPheDuyet = vmChuTruongDauTu.IdCoQuanPheDuyet;
                    if (vmChuTruongDauTu.NgayPheDuyet != "" && vmChuTruongDauTu.NgayPheDuyet != null)
                    {
                        updateChuTruongDauTu.NgayPheDuyet = DateTime.ParseExact(vmChuTruongDauTu.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateChuTruongDauTu.NgayPheDuyet = null;
                    }
                        
                    updateChuTruongDauTu.NguoiPheDuyet = vmChuTruongDauTu.NguoiPheDuyet;
                    updateChuTruongDauTu.TongMucDauTu = vmChuTruongDauTu.TongMucDauTu;
                    updateChuTruongDauTu.KinhPhi = vmChuTruongDauTu.KinhPhi;
                    updateChuTruongDauTu.LoaiDieuChinh = vmChuTruongDauTu.LoaiDieuChinh;
                    updateChuTruongDauTu.GhiChu = vmChuTruongDauTu.GhiChu;
                    updateChuTruongDauTu.UpdatedDate = DateTime.Now;
                    updateChuTruongDauTu.UpdatedBy = User.Identity.GetUserId();
                    _ChuTruongDauTuService.Update(updateChuTruongDauTu);
                    _ChuTruongDauTuService.Save();

                    var responseData = Mapper.Map<ChuTruongDauTu, ChuTruongDauTuViewModels>(updateChuTruongDauTu);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _ChuTruongDauTuService.Delete(id);
                    _ChuTruongDauTuService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CHUTRUONGDAUTU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lstKeHoachVon = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in lstKeHoachVon)
                    {
                        _ChuTruongDauTuService.Delete(item);
                    }
                    _ChuTruongDauTuService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, lstKeHoachVon.Count);
                }

                return response;
            });
        }
    }
}
