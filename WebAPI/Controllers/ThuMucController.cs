﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
using WebAPI.Infrastructure.Core;
using Model.Models;
using WebAPI.Models;
using AutoMapper;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Providers;
using WebAPI.Models.DuAn;

namespace WebAPI.Controllers
{

    [RoutePrefix("api/tm")]
    [Authorize]
    public class ThuMucController : ApiControllerBase
    {
        public IThuMucService _tmService;
        public IVanBanDuAnService _vbdaService;
        public ThuMucController(IThuMucService tmService,IVanBanDuAnService vbdaService, IErrorService errorService) : base(errorService)
        {
            _tmService = tmService;
            _vbdaService = vbdaService;
        }
       
        [Route("gettmbyda/{idDA}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "VANBANDUAN")]
        public HttpResponseMessage GetTMByDA(HttpRequestMessage request, int idDA)
        {

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<ThuMuc> lstTMByDA = _tmService.GetTMsByDA(idDA);
              
                IEnumerable<ThuMucViewModel> lstTMVMByDA = Mapper.Map<IEnumerable<ThuMuc>, IEnumerable<ThuMucViewModel>>(lstTMByDA);
                foreach(var item in lstTMVMByDA)
                {
                    item.SoLuongVB = _vbdaService.TotalVBDAByTM(item.IdThuMuc);
                }
                var responseData = lstTMVMByDA.GroupBy(x => x.IdThuMucCha, (key, x) => new { IdThuMucCha = key, TMs = x.ToList() });
                
                response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });

        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<ThuMuc> lst = _tmService.GetAllForHistory();

                var responseData = Mapper.Map<IEnumerable<ThuMuc>, IEnumerable<ThuMucViewModel>>(lst);
                response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });

        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "VANBANDUAN")]
        public HttpResponseMessage Add(HttpRequestMessage request, ThuMucViewModel tmVM)
        {
            return CreateHttpResponse(request, () =>

            {
                HttpResponseMessage response = null;
                ThuMuc tm = new ThuMuc();
                tm.UpdateThuMuc(tmVM);
                tm.Status = true;
                tm = _tmService.Add(tm);
                _tmService.Save();
                tmVM = Mapper.Map<ThuMuc, ThuMucViewModel>(tm);
                response = request.CreateResponse(HttpStatusCode.Created, tmVM);
                return response;
            });
        }
        [Route("edit")]
        [HttpPost]
        [Permission(Action = "Update", Function = "VANBANDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThuMucViewModel tmVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                ThuMuc tm = new ThuMuc();
                tm.UpdateThuMuc(tmVM);
                _tmService.Update(tm);
                _tmService.Save();
                response = request.CreateResponse(HttpStatusCode.OK,tmVM);
                return response;
            });
        }
        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "VANBANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string arrayId)
        {
            var tempt = arrayId.Split(',');
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                foreach(var idtm in tempt)
                {
                    _tmService.Delete(int.Parse(idtm));
                    _tmService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
    }
}
