﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/quyettoanhopdong")]
    [Authorize]
    public class QuyetToanHopDongController : ApiControllerBase
    {
        private IQuyetToanHopDongService _QuyetToanHopDongService;
        public QuyetToanHopDongController(IErrorService errorService, IQuyetToanHopDongService quyetToanHopDongService) : base(errorService)
        {
            this._QuyetToanHopDongService = quyetToanHopDongService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QuyetToanHopDongService.GetAll().OrderByDescending(x => x.IdQuyetToan);

                var modelVm = Mapper.Map<IEnumerable<QuyetToanHopDong>, IEnumerable<QuyetToanHopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request,int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _QuyetToanHopDongService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getbyidhopdong")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idHopDong)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QuyetToanHopDongService.GetByIdHopDong(idHopDong);
                var modelVm = Mapper.Map<IEnumerable<QuyetToanHopDong>, IEnumerable<QuyetToanHopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QuyetToanHopDongService.GetById(id);

                var responseData = Mapper.Map<QuyetToanHopDong, QuyetToanHopDongViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QuyetToanHopDongViewModels vmQuyetToanHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newQuyetToanHopDong = new QuyetToanHopDong();
                    newQuyetToanHopDong.IdHopDong = vmQuyetToanHopDongViewModels.IdHopDong;
                    newQuyetToanHopDong.NoiDung = vmQuyetToanHopDongViewModels.NoiDung;
                    if (vmQuyetToanHopDongViewModels.NgayQuyetToan != "" && vmQuyetToanHopDongViewModels.NgayQuyetToan != null)
                        newQuyetToanHopDong.NgayQuyetToan = DateTime.ParseExact(vmQuyetToanHopDongViewModels.NgayQuyetToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newQuyetToanHopDong.GiaTriQuyetToan = vmQuyetToanHopDongViewModels.GiaTriQuyetToan;
                    newQuyetToanHopDong.GhiChu = vmQuyetToanHopDongViewModels.GhiChu;
                    newQuyetToanHopDong.CreatedDate = DateTime.Now;
                    newQuyetToanHopDong.CreatedBy = User.Identity.GetUserId();
                    newQuyetToanHopDong.Status = true;

                    _QuyetToanHopDongService.Add(newQuyetToanHopDong);
                    _QuyetToanHopDongService.Save();

                    var responseData = Mapper.Map<QuyetToanHopDong, QuyetToanHopDongViewModels>(newQuyetToanHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QuyetToanHopDongViewModels vmQuyetToanHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateQuyetToanHopDong = _QuyetToanHopDongService.GetById(vmQuyetToanHopDongViewModels.IdQuyetToan);
                    updateQuyetToanHopDong.IdHopDong = vmQuyetToanHopDongViewModels.IdHopDong;
                    updateQuyetToanHopDong.NoiDung = vmQuyetToanHopDongViewModels.NoiDung;
                    if (vmQuyetToanHopDongViewModels.NgayQuyetToan != "" && vmQuyetToanHopDongViewModels.NgayQuyetToan != null)
                    {
                        updateQuyetToanHopDong.NgayQuyetToan = DateTime.ParseExact(vmQuyetToanHopDongViewModels.NgayQuyetToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateQuyetToanHopDong.NgayQuyetToan = null;
                    }
                        
                    updateQuyetToanHopDong.GiaTriQuyetToan = vmQuyetToanHopDongViewModels.GiaTriQuyetToan;
                    updateQuyetToanHopDong.GhiChu = vmQuyetToanHopDongViewModels.GhiChu;
                    updateQuyetToanHopDong.UpdatedDate = DateTime.Now;
                    updateQuyetToanHopDong.UpdatedBy = User.Identity.GetUserId();

                    _QuyetToanHopDongService.Update(updateQuyetToanHopDong);
                    _QuyetToanHopDongService.Save();

                    var responseData = Mapper.Map<QuyetToanHopDong, QuyetToanHopDongViewModels>(updateQuyetToanHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });
        }

        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateQuyetToanHopDong = _QuyetToanHopDongService.GetById(vnTrangThaiUp.ID);
                    updateQuyetToanHopDong.TrangThaiGui = 1;
                    updateQuyetToanHopDong.UpdatedDate = DateTime.Now;
                    updateQuyetToanHopDong.UpdatedBy = User.Identity.GetUserId();
                    _QuyetToanHopDongService.Update(updateQuyetToanHopDong);
                    _QuyetToanHopDongService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _QuyetToanHopDongService.Delete(id);
                    _QuyetToanHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDGoiThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDGoiThau)
                    {
                        _QuyetToanHopDongService.Delete(item);
                    }

                    _QuyetToanHopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listIDGoiThau.Count);
                }

                return response;
            });
        }
    }
}
