﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hosomoithau")]
    [Authorize]
    public class HoSoMoiThauController : ApiControllerBase
    {
        private IHoSoMoiThauService _hoSoMoiThauService;
        public HoSoMoiThauController(IErrorService errorService, IHoSoMoiThauService hoSoMoiThauService) : base(errorService)
        {
            this._hoSoMoiThauService = hoSoMoiThauService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hoSoMoiThauService.GetAll().OrderByDescending(x => x.IdHoSoMoiThau);

                var modelVm = Mapper.Map<IEnumerable<HoSoMoiThau>, IEnumerable<HoSoMoiThauViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOSOMOITHAU")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hoSoMoiThauService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);

                PaginationSet<GoiThauViewModels> pagedSet = new PaginationSet<GoiThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOSOMOITHAU")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hoSoMoiThauService.GetById(id);

                var responseData = Mapper.Map<HoSoMoiThau, HoSoMoiThauViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HOSOMOITHAU")]
        public HttpResponseMessage Create(HttpRequestMessage request, HoSoMoiThauViewModels hoSoMoiThautVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHoSoMoiThau = new HoSoMoiThau();
                    newHoSoMoiThau.UpdateHoSoMoiThau(hoSoMoiThautVm);
                    newHoSoMoiThau.CreatedDate = DateTime.Now;
                    newHoSoMoiThau.CreatedBy = User.Identity.GetUserId();
                    _hoSoMoiThauService.Add(newHoSoMoiThau);
                    _hoSoMoiThauService.Save();

                    var responseData = Mapper.Map<HoSoMoiThau, HoSoMoiThauViewModels>(newHoSoMoiThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HOSOMOITHAU")]
        public HttpResponseMessage Update(HttpRequestMessage request, HoSoMoiThauViewModels hoSoMoiThauVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbHoSoMoiThau = _hoSoMoiThauService.GetById(hoSoMoiThauVm.IdHoSoMoiThau);

                    dbHoSoMoiThau.UpdateHoSoMoiThau(hoSoMoiThauVm);
                    dbHoSoMoiThau.UpdatedDate = DateTime.Now;
                    dbHoSoMoiThau.UpdatedBy = User.Identity.GetUserId();
                    _hoSoMoiThauService.Update(dbHoSoMoiThau);
                    _hoSoMoiThauService.Save();

                    var responseData = Mapper.Map<HoSoMoiThau, HoSoMoiThauViewModels>(dbHoSoMoiThau);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOSOMOITHAU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hoSoMoiThauService.Delete(id);
                    _hoSoMoiThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOSOMOITHAU")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listHoSoMoiThau = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listHoSoMoiThau)
                    {
                        _hoSoMoiThauService.Delete(item);
                    }

                    _hoSoMoiThauService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listHoSoMoiThau.Count);
                }

                return response;
            });
        }
    }
}
