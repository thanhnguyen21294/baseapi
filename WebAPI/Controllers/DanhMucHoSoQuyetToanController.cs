﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/danhmuchosoquyettoan")]
    [Authorize]
    public class DanhMucHoSoQuyetToanController : ApiControllerBase
    {
        private IDanhMucHoSoQuyetToanService _danhMucHoSoQuyetToanService;
        public DanhMucHoSoQuyetToanController(IErrorService errorService, IDanhMucHoSoQuyetToanService danhMucHoSoQuyetToanService) : base(errorService)
        {
            this._danhMucHoSoQuyetToanService = danhMucHoSoQuyetToanService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _danhMucHoSoQuyetToanService.GetAll().OrderByDescending(x => x.IdDanhMucHoSoQuyetToan);

                var modelVm = Mapper.Map<IEnumerable<DanhMucHoSoQuyetToan>, IEnumerable<DanhMucHoSoQuyetToanViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getgroup")]
        [HttpGet]
        public HttpResponseMessage GetGroup(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _danhMucHoSoQuyetToanService.GetAll().OrderBy(x => x.Order).ToList();
                var gpr = model.Where(x => x.IdParent == null).Select(x => new
                {
                    x.IdDanhMucHoSoQuyetToan,
                    x.TenDanhMucHoSoQuyetToan,
                    x.Order,
                    DanhMucCha = model.Where(y => y.IdParent == x.IdDanhMucHoSoQuyetToan).Select(y => new
                    {
                        y.IdDanhMucHoSoQuyetToan,
                        y.TenDanhMucHoSoQuyetToan,
                        y.Order,
                        DanhMucCon = model.Where(z => z.IdParent == y.IdDanhMucHoSoQuyetToan).Select(z => new
                        {
                            z.IdDanhMucHoSoQuyetToan,
                            z.TenDanhMucHoSoQuyetToan,
                            z.Order,
                            DanhMucCon = model.Where(zz => zz.IdParent == z.IdDanhMucHoSoQuyetToan).Select(zz => new
                            {
                                zz.IdDanhMucHoSoQuyetToan,
                                zz.TenDanhMucHoSoQuyetToan,
                                zz.Order
                            }).OrderBy(zz => zz.Order)
                        }).OrderBy(z => z.Order)
                    }).OrderBy(y => y.Order)
                });
                //var modelVm = Mapper.Map<IEnumerable<DanhMucHoSoQuyetToan>, IEnumerable<DanhMucHoSoQuyetToanViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, gpr);
                return response;
            });
        }
       
        [Route("getallcha")]
        [HttpGet]
        public HttpResponseMessage GetAllCha(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _danhMucHoSoQuyetToanService.GetAll().Where(x => x.IdParent == null).OrderBy(x => x.IdDanhMucHoSoQuyetToan);
                var modelVm = Mapper.Map<IEnumerable<DanhMucHoSoQuyetToan>, IEnumerable<DanhMucHoSoQuyetToanViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getallname")]
        [HttpGet]
        public HttpResponseMessage GetAllName(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _danhMucHoSoQuyetToanService.GetAll().OrderByDescending(x => x.IdDanhMucHoSoQuyetToan).Select(x => x.TenDanhMucHoSoQuyetToan);
                                
                response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _danhMucHoSoQuyetToanService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<DanhMucHoSoQuyetToan>, IEnumerable<DanhMucHoSoQuyetToanViewModels>>(model);

                PaginationSet<DanhMucHoSoQuyetToanViewModels> pagedSet = new PaginationSet<DanhMucHoSoQuyetToanViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _danhMucHoSoQuyetToanService.GetById(id);

                var responseData = Mapper.Map<DanhMucHoSoQuyetToan, DanhMucHoSoQuyetToanViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, DanhMucHoSoQuyetToanViewModels danhMucHoSoQuyetToanViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newDanhMucHoSoQuyetToan = new DanhMucHoSoQuyetToan();
                    newDanhMucHoSoQuyetToan.UpdateDanhMucHoSoQuyetToan(danhMucHoSoQuyetToanViewModels);
                    newDanhMucHoSoQuyetToan.CreatedDate = DateTime.Now;
                    newDanhMucHoSoQuyetToan.CreatedBy = User.Identity.GetUserId();
                    _danhMucHoSoQuyetToanService.Add(newDanhMucHoSoQuyetToan);
                    _danhMucHoSoQuyetToanService.Save();

                    var responseData = Mapper.Map<DanhMucHoSoQuyetToan, DanhMucHoSoQuyetToanViewModels>(newDanhMucHoSoQuyetToan);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, DanhMucHoSoQuyetToanViewModels danhMucHoSoQuyetToanViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbDanhMucHoSoQuyetToan = _danhMucHoSoQuyetToanService.GetById(danhMucHoSoQuyetToanViewModels.IdDanhMucHoSoQuyetToan);

                    dbDanhMucHoSoQuyetToan.UpdateDanhMucHoSoQuyetToan(danhMucHoSoQuyetToanViewModels);
                    dbDanhMucHoSoQuyetToan.UpdatedDate = DateTime.Now;
                    dbDanhMucHoSoQuyetToan.UpdatedBy = User.Identity.GetUserId();
                    _danhMucHoSoQuyetToanService.Update(dbDanhMucHoSoQuyetToan);
                    _danhMucHoSoQuyetToanService.Save();

                    var responseData = Mapper.Map<DanhMucHoSoQuyetToan, DanhMucHoSoQuyetToanViewModels>(dbDanhMucHoSoQuyetToan);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _danhMucHoSoQuyetToanService.Delete(id);
                    _danhMucHoSoQuyetToanService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DANHMUCHOSOQUYETTOAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listDanhMucHoSoQuyetToan = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listDanhMucHoSoQuyetToan)
                    {
                        _danhMucHoSoQuyetToanService.Delete(item);
                    }

                    _danhMucHoSoQuyetToanService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listDanhMucHoSoQuyetToan.Count);
                }

                return response;
            });
        }
    }
}
