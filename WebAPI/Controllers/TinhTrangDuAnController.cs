﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tinhtrangduan")]
    [Authorize]
    public class TinhTrangDuAnController : ApiControllerBase
    {
        private ITinhTrangDuAnService _tinhTrangDuAnService;
        public TinhTrangDuAnController(IErrorService errorService, ITinhTrangDuAnService tinhTrangDuAnService) : base(errorService)
        {
            this._tinhTrangDuAnService = tinhTrangDuAnService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _tinhTrangDuAnService.GetAll().OrderBy(x => x.IdTinhTrangDuAn);

                var modelVm = Mapper.Map<IEnumerable<TinhTrangDuAn>, IEnumerable<TinhTrangDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _tinhTrangDuAnService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<TinhTrangDuAn>, IEnumerable<TinhTrangDuAnViewModels>>(model);

                PaginationSet<TinhTrangDuAnViewModels> pagedSet = new PaginationSet<TinhTrangDuAnViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _tinhTrangDuAnService.GetById(id);

                var responseData = Mapper.Map<TinhTrangDuAn, TinhTrangDuAnViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, TinhTrangDuAnViewModels tinhTrangDuAnViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newTinhTrangDuAn = new TinhTrangDuAn();
                    newTinhTrangDuAn.UpdateTinhTrangDuAn(tinhTrangDuAnViewModels);
                    newTinhTrangDuAn.CreatedDate = DateTime.Now;
                    newTinhTrangDuAn.CreatedBy = User.Identity.GetUserId();
                    _tinhTrangDuAnService.Add(newTinhTrangDuAn);
                    _tinhTrangDuAnService.Save();

                    var responseData = Mapper.Map<TinhTrangDuAn, TinhTrangDuAnViewModels>(newTinhTrangDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, TinhTrangDuAnViewModels tinhTrangDuAnViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbTinhTrangDuAn = _tinhTrangDuAnService.GetById(tinhTrangDuAnViewModels.IdTinhTrangDuAn);

                    dbTinhTrangDuAn.UpdateTinhTrangDuAn(tinhTrangDuAnViewModels);
                    dbTinhTrangDuAn.UpdatedDate = DateTime.Now;
                    dbTinhTrangDuAn.UpdatedBy = User.Identity.GetUserId();
                    _tinhTrangDuAnService.Update(dbTinhTrangDuAn);
                    _tinhTrangDuAnService.Save();

                    var responseData = Mapper.Map<TinhTrangDuAn, TinhTrangDuAnViewModels>(dbTinhTrangDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _tinhTrangDuAnService.Delete(id);
                    _tinhTrangDuAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "TINHTRANGDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listTinhTrangDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listTinhTrangDuAn)
                    {
                        _tinhTrangDuAnService.Delete(item);
                    }

                    _tinhTrangDuAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listTinhTrangDuAn.Count);
                }

                return response;
            });
        }
    }
}
