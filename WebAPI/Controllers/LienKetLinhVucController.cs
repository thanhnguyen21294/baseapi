﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/lienketlinhvuc")]
    [Authorize]
    public class LienKetLinhVucController : ApiControllerBase
    {
        private ILienKetLinhVucService _LienKetLinhVucService;
        public LienKetLinhVucController(IErrorService errorService, ILienKetLinhVucService LienKetLinhVucService) : base(errorService)
        {
            this._LienKetLinhVucService = LienKetLinhVucService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _LienKetLinhVucService.GetAll().OrderByDescending(x => x.IdLienKetLinhVuc);

                var modelVm = Mapper.Map<IEnumerable<LienKetLinhVuc>, IEnumerable<LienKetLinhVucViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LienKetLinhVuc")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _LienKetLinhVucService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<LienKetLinhVuc>, IEnumerable<LienKetLinhVucViewModels>>(model);

                PaginationSet<LienKetLinhVucViewModels> pagedSet = new PaginationSet<LienKetLinhVucViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "LienKetLinhVuc")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _LienKetLinhVucService.GetById(id);

                var responseData = Mapper.Map<LienKetLinhVuc, LienKetLinhVucViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "LienKetLinhVuc")]
        public HttpResponseMessage Create(HttpRequestMessage request, LienKetLinhVucViewModels LienKetLinhVucViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newLienKetLinhVuc = new LienKetLinhVuc();
                    newLienKetLinhVuc.TenLienKetLinhVuc =  LienKetLinhVucViewModels.TenLienKetLinhVuc;
                    newLienKetLinhVuc.GhiChu = LienKetLinhVucViewModels.GhiChu;
                    newLienKetLinhVuc.Order = LienKetLinhVucViewModels.Order;
                    newLienKetLinhVuc.CreatedDate = DateTime.Now;
                    newLienKetLinhVuc.CreatedBy = User.Identity.GetUserId();
                    _LienKetLinhVucService.Add(newLienKetLinhVuc);
                    _LienKetLinhVucService.Save();

                    var responseData = Mapper.Map<LienKetLinhVuc, LienKetLinhVucViewModels>(newLienKetLinhVuc);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "LienKetLinhVuc")]
        public HttpResponseMessage Update(HttpRequestMessage request, LienKetLinhVucViewModels LienKetLinhVucViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbLienKetLinhVuc = _LienKetLinhVucService.GetById(LienKetLinhVucViewModels.IdLienKetLinhVuc);
                    dbLienKetLinhVuc.TenLienKetLinhVuc = LienKetLinhVucViewModels.TenLienKetLinhVuc;
                    dbLienKetLinhVuc.GhiChu = LienKetLinhVucViewModels.GhiChu;
                    dbLienKetLinhVuc.Order = LienKetLinhVucViewModels.Order;
                    dbLienKetLinhVuc.UpdatedDate = DateTime.Now;
                    dbLienKetLinhVuc.UpdatedBy = User.Identity.GetUserId();
                    _LienKetLinhVucService.Update(dbLienKetLinhVuc);
                    _LienKetLinhVucService.Save();

                    var responseData = Mapper.Map<LienKetLinhVuc, LienKetLinhVucViewModels>(dbLienKetLinhVuc);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LienKetLinhVuc")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _LienKetLinhVucService.Delete(id);
                    _LienKetLinhVucService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "LienKetLinhVuc")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listLienKetLinhVuc = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listLienKetLinhVuc)
                    {
                        _LienKetLinhVucService.Delete(item);
                    }

                    _LienKetLinhVucService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listLienKetLinhVuc.Count);
                }

                return response;
            });
        }
    }
}
