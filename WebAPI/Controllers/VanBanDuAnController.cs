﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
using WebAPI.Infrastructure.Core;
using Model.Models;
using WebAPI.Models;
using AutoMapper;
using WebAPI.Infrastructure.Extensions;
using Antlr.Runtime.Misc;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/vbda")]
    public class VanBanDuAnController : ApiControllerBase
    {
        public IVanBanDuAnService _vbdaService;
        public VanBanDuAnController(IErrorService errorService, IVanBanDuAnService vbdaService) : base(errorService)
        {
            _vbdaService = vbdaService;
        }

        [Route("getvbdabytm/{idTM}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "VANBANDUAN")]
        public HttpResponseMessage GetVBDAByTMId(HttpRequestMessage request, int idTM, string filter, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<VanBanDuAn> vbdas = _vbdaService.GetVBDAByTM(idTM, filter).OrderBy(x => x.IdVanBan).Skip((page - 1) * pageSize).Take(pageSize);
                int totalRow = _vbdaService.GetVBDAByTM(idTM, filter).ToList().Count;
                IEnumerable<VanBanDuAnViewModel> vbdaVMs = Mapper.Map<IEnumerable<VanBanDuAn>, IEnumerable<VanBanDuAnViewModel>>(vbdas);
                PaginationSet<VanBanDuAnViewModel> pagedSet = new PaginationSet<VanBanDuAnViewModel>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = vbdaVMs
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<VanBanDuAn> lst = _vbdaService.GetAllForHistory();
                var responseData = Mapper.Map<IEnumerable<VanBanDuAn>, IEnumerable<VanBanDuAnViewModel>>(lst);
                response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });

        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "VANBANDUAN")]
        public HttpResponseMessage Add(HttpRequestMessage request, VanBanDuAnViewModel vbdaVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                VanBanDuAn vbda = new VanBanDuAn();
                vbda.UpdateVanBanDuAn(vbdaVM);
                vbda.Status = true;

                vbda = _vbdaService.Add(vbda);
                _vbdaService.Save();
                vbdaVM = Mapper.Map<VanBanDuAn, VanBanDuAnViewModel>(vbda);
                response = request.CreateResponse(HttpStatusCode.Created, vbdaVM);
                return response;

            });
        }
        [Route("update")]
        [HttpPost]
        [Permission(Action = "Update", Function = "VANBANDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, VanBanDuAnViewModel vbdaVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                VanBanDuAn vbda = new VanBanDuAn();
                vbda.UpdateVanBanDuAn(vbdaVM);
                vbda.Status = false;
                _vbdaService.Update(vbda);
                _vbdaService.Save();
                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        [HttpDelete]
        [Route("delete/")]
        [Permission(Action = "Delete", Function = "VANBANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string arrayId)
        {

            var tempt = arrayId.Split(',');

            return CreateHttpResponse(request, () =>
             {
                 HttpResponseMessage response = null;
                 foreach (var item in tempt)
                 {
                     int id = Convert.ToInt16(item);
                     _vbdaService.Delete(id);
                     _vbdaService.Save();

                 }

                 response = request.CreateResponse(HttpStatusCode.OK);
                 return response;
             });
        }
        [Route("getbyid/{id}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "VANABNDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vbdaService.GetById(id);

                var responseData = Mapper.Map<VanBanDuAn, VanBanDuAnViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
    }
}
