﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;
namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/quyettoanduan")]
    public class QuyetToanDuAnController : ApiControllerBase
    {

        private IQuyetToanDuAnService _QuyetToanDuAnService;
        private INguonVonQuyetToanDuAnService _NguonVonQuyetToanDuAnService;
        public QuyetToanDuAnController(IErrorService errorService, IQuyetToanDuAnService quyetToanDuAnService, INguonVonQuyetToanDuAnService nguonVonQuyetToanDuAnService) : base(errorService)
        {
            this._QuyetToanDuAnService = quyetToanDuAnService;
            _NguonVonQuyetToanDuAnService = nguonVonQuyetToanDuAnService;
        }
        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetAll(HttpRequestMessage request, string filter = "")
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QuyetToanDuAnService.GetAll(filter).OrderByDescending(x => x.IdQuyetToanDuAn);

                var modelVm = Mapper.Map<IEnumerable<QuyetToanDuAn>, IEnumerable<QuyetToanDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }
        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllFotHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QuyetToanDuAnService.GetAll().OrderByDescending(x => x.IdQuyetToanDuAn);

                var modelVm = Mapper.Map<IEnumerable<QuyetToanDuAn>, IEnumerable<QuyetToanDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDA)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _QuyetToanDuAnService.GetByIdDuAn(idDA).OrderByDescending(x => x.IdQuyetToanDuAn);

                var modelVm = Mapper.Map<IEnumerable<QuyetToanDuAn>, IEnumerable<QuyetToanDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getyear")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetYear(HttpRequestMessage request, int year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _QuyetToanDuAnService.GetYear(year).OrderByDescending(x => x.IdQuyetToanDuAn);

                    //var modelVm = Mapper.Map<IEnumerable<QuyetToanDuAn>, IEnumerable<QuyetToanDuAnViewModels>>(model);

                    response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _QuyetToanDuAnService.GetByFilter(idDA, page, pageSize, "", out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<QuyetToanDuAn>, IEnumerable<QuyetToanDuAnViewModels>>(model);

                PaginationSet<QuyetToanDuAnViewModels> pagedSet = new PaginationSet<QuyetToanDuAnViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _QuyetToanDuAnService.GetById(id);

                var responseData = Mapper.Map<QuyetToanDuAn, QuyetToanDuAnViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, QuyetToanDuAnViewModels vmQuyetToanDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newQuyetToanDuAn = new QuyetToanDuAn();
                    newQuyetToanDuAn.NoiDung = vmQuyetToanDuAn.NoiDung;
                    newQuyetToanDuAn.IdDuAn = vmQuyetToanDuAn.IdDuAn;
                    if (vmQuyetToanDuAn.NgayKyBienBan != "" && vmQuyetToanDuAn.NgayKyBienBan != null)
                        newQuyetToanDuAn.NgayKyBienBan = DateTime.ParseExact(vmQuyetToanDuAn.NgayKyBienBan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmQuyetToanDuAn.NgayTrinh != "" && vmQuyetToanDuAn.NgayTrinh != null)
                        newQuyetToanDuAn.NgayTrinh = DateTime.ParseExact(vmQuyetToanDuAn.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmQuyetToanDuAn.NgayNhanDuHoSo != "" && vmQuyetToanDuAn.NgayNhanDuHoSo != null)
                        newQuyetToanDuAn.NgayNhanDuHoSo = DateTime.ParseExact(vmQuyetToanDuAn.NgayNhanDuHoSo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmQuyetToanDuAn.NgayThamTra != "" && vmQuyetToanDuAn.NgayThamTra != null)
                        newQuyetToanDuAn.NgayThamTra = DateTime.ParseExact(vmQuyetToanDuAn.NgayThamTra, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    if (vmQuyetToanDuAn.NgayPheDuyetQuyetToan != "" && vmQuyetToanDuAn.NgayPheDuyetQuyetToan != null)
                        newQuyetToanDuAn.NgayPheDuyetQuyetToan = DateTime.ParseExact(vmQuyetToanDuAn.NgayPheDuyetQuyetToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    newQuyetToanDuAn.SoPheDuyet = vmQuyetToanDuAn.SoPheDuyet;
                    newQuyetToanDuAn.IdCoQuanPheDuyet = vmQuyetToanDuAn.IdCoQuanPheDuyet;
                    newQuyetToanDuAn.TongGiaTri = vmQuyetToanDuAn.TongGiaTri;
                    newQuyetToanDuAn.XayLap = vmQuyetToanDuAn.XayLap;
                    newQuyetToanDuAn.ThietBi = vmQuyetToanDuAn.ThietBi;
                    newQuyetToanDuAn.GiaiPhongMatBang = vmQuyetToanDuAn.GiaiPhongMatBang;
                    newQuyetToanDuAn.TuVan = vmQuyetToanDuAn.TuVan;
                    newQuyetToanDuAn.QuanLyDuAn = vmQuyetToanDuAn.QuanLyDuAn;
                    newQuyetToanDuAn.Khac = vmQuyetToanDuAn.Khac;
                    newQuyetToanDuAn.DuPhong = vmQuyetToanDuAn.DuPhong;
                    newQuyetToanDuAn.GhiChu = vmQuyetToanDuAn.GhiChu;
                    newQuyetToanDuAn.CreatedDate = DateTime.Now;
                    newQuyetToanDuAn.CreatedBy = User.Identity.GetUserId();
                    newQuyetToanDuAn.Status = true;

                    List<NguonVonQuyetToanDuAn> lstNguonVonQTDA = new List<NguonVonQuyetToanDuAn>();
                    foreach (var item in vmQuyetToanDuAn.NguonVonQuyetToanDuAns)
                    {
                        _NguonVonQuyetToanDuAnService.Add(new NguonVonQuyetToanDuAn
                        {
                            IdNguonVonDuAn = item.IdNguonVonDuAn,
                            GiaTri = item.GiaTri,
                            Status = true
                        });
                            //lstNguonVonQTDA.Add(new NguonVonQuyetToanDuAn
                            //{
                            //    IdNguonVonDuAn = item.IdNguonVonDuAn,
                            //    GiaTri = item.GiaTri,
                            //    Status = true
                            //});
                        }
                        //newQuyetToanDuAn.NguonVonQuyetToanDuAns = lstNguonVonQTDA;

                        _QuyetToanDuAnService.Add(newQuyetToanDuAn);
                    _QuyetToanDuAnService.Save();

                    var responseData = Mapper.Map<QuyetToanDuAn, QuyetToanDuAnViewModels>(newQuyetToanDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, QuyetToanDuAnViewModels vmQuyetToanDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateKeHoachVon = _QuyetToanDuAnService.GetById(vmQuyetToanDuAn.IdQuyetToanDuAn);
                    updateKeHoachVon.NoiDung = vmQuyetToanDuAn.NoiDung;
                    updateKeHoachVon.IdDuAn = vmQuyetToanDuAn.IdDuAn;
                    if (vmQuyetToanDuAn.NgayKyBienBan != "" && vmQuyetToanDuAn.NgayKyBienBan != null)
                    {
                        updateKeHoachVon.NgayKyBienBan = DateTime.ParseExact(vmQuyetToanDuAn.NgayKyBienBan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayKyBienBan = null;
                    }

                    if (vmQuyetToanDuAn.NgayTrinh != "" && vmQuyetToanDuAn.NgayTrinh != null)
                    {
                        updateKeHoachVon.NgayTrinh = DateTime.ParseExact(vmQuyetToanDuAn.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayTrinh = null;
                    }

                    if (vmQuyetToanDuAn.NgayNhanDuHoSo != "" && vmQuyetToanDuAn.NgayNhanDuHoSo != null)
                    {
                        updateKeHoachVon.NgayNhanDuHoSo = DateTime.ParseExact(vmQuyetToanDuAn.NgayNhanDuHoSo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayNhanDuHoSo = null;
                    }

                    if (vmQuyetToanDuAn.NgayThamTra != "" && vmQuyetToanDuAn.NgayThamTra != null)
                    {
                        updateKeHoachVon.NgayThamTra = DateTime.ParseExact(vmQuyetToanDuAn.NgayThamTra, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayThamTra = null;
                    }

                    if (vmQuyetToanDuAn.NgayPheDuyetQuyetToan != "" && vmQuyetToanDuAn.NgayPheDuyetQuyetToan != null)
                    {
                        updateKeHoachVon.NgayPheDuyetQuyetToan = DateTime.ParseExact(vmQuyetToanDuAn.NgayPheDuyetQuyetToan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        updateKeHoachVon.NgayPheDuyetQuyetToan = null;
                    }

                    updateKeHoachVon.SoPheDuyet = vmQuyetToanDuAn.SoPheDuyet;
                    updateKeHoachVon.IdCoQuanPheDuyet = vmQuyetToanDuAn.IdCoQuanPheDuyet;
                    updateKeHoachVon.TongGiaTri = vmQuyetToanDuAn.TongGiaTri;
                    updateKeHoachVon.XayLap = vmQuyetToanDuAn.XayLap;
                    updateKeHoachVon.ThietBi = vmQuyetToanDuAn.ThietBi;
                    updateKeHoachVon.GiaiPhongMatBang = vmQuyetToanDuAn.GiaiPhongMatBang;
                    updateKeHoachVon.TuVan = vmQuyetToanDuAn.TuVan;
                    updateKeHoachVon.QuanLyDuAn = vmQuyetToanDuAn.QuanLyDuAn;
                    updateKeHoachVon.Khac = vmQuyetToanDuAn.Khac;
                    updateKeHoachVon.DuPhong = vmQuyetToanDuAn.DuPhong;
                    updateKeHoachVon.GhiChu = vmQuyetToanDuAn.GhiChu;
                    updateKeHoachVon.UpdatedDate = DateTime.Now;
                    updateKeHoachVon.UpdatedBy = User.Identity.GetUserId();

                    try
                    {
                        _NguonVonQuyetToanDuAnService.DeleteByIdQuyetToanDuAn(vmQuyetToanDuAn.IdQuyetToanDuAn);
                        foreach (var item in vmQuyetToanDuAn.NguonVonQuyetToanDuAns)
                        {
                            _NguonVonQuyetToanDuAnService.Add(new NguonVonQuyetToanDuAn
                            {
                                IdNguonVonDuAn = item.IdNguonVonDuAn,
                                IdQuyetToanDuAn = vmQuyetToanDuAn.IdQuyetToanDuAn,
                                GiaTri = item.GiaTri,
                                Status = true
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _QuyetToanDuAnService.Update(updateKeHoachVon);
                    _QuyetToanDuAnService.Save();

                    var responseData = Mapper.Map<QuyetToanDuAn, QuyetToanDuAnViewModels>(updateKeHoachVon);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbUpdate = _QuyetToanDuAnService.GetById(vnTrangThaiUp.ID);
                    dbUpdate.TrangThaiGui = 1;
                    dbUpdate.UpdatedDate = DateTime.Now;
                    dbUpdate.UpdatedBy = User.Identity.GetUserId();
                    _QuyetToanDuAnService.Update(dbUpdate);
                    _QuyetToanDuAnService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }
        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _NguonVonQuyetToanDuAnService.DeleteByIdQuyetToanDuAn(id);
                    _QuyetToanDuAnService.Delete(id);
                    _QuyetToanDuAnService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }
                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "QUYETTOANDUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var lstQuyetToanDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in lstQuyetToanDuAn)
                    {
                        _NguonVonQuyetToanDuAnService.DeleteByIdQuyetToanDuAn(item);
                        _QuyetToanDuAnService.Delete(item);
                    }
                    _QuyetToanDuAnService.Save();
                    response = request.CreateResponse(HttpStatusCode.OK, lstQuyetToanDuAn.Count);
                }

                return response;
            });
        }
    }
}
