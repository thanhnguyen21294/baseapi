﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/NhomDuAnTheoUser")]
    [Authorize]
    public class NhomDuAnTheoUserController : ApiControllerBase
    {
        private INhomDuAnTheoUserService _nhomDuAnTheoUserService;
        public NhomDuAnTheoUserController(IErrorService errorService, INhomDuAnTheoUserService nhomDuAnTheoUserService) : base(errorService)
        {
            this._nhomDuAnTheoUserService = nhomDuAnTheoUserService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nhomDuAnTheoUserService.GetAll().OrderByDescending(x => x.IdNhomDuAn);

                var modelVm = Mapper.Map<IEnumerable<NhomDuAnTheoUser>, IEnumerable<NhomDuAnTheoUserViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _nhomDuAnTheoUserService.GetByFilter(page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<NhomDuAnTheoUser>, IEnumerable<NhomDuAnTheoUserViewModels>>(model);

                PaginationSet<NhomDuAnTheoUserViewModels> pagedSet = new PaginationSet<NhomDuAnTheoUserViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nhomDuAnTheoUserService.GetById(id);

                var responseData = Mapper.Map<NhomDuAnTheoUser, NhomDuAnTheoUserViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage Create(HttpRequestMessage request, NhomDuAnTheoUserViewModels nhomDuAnTheoUsertVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newNhomDuAnTheoUser = new NhomDuAnTheoUser();
                    newNhomDuAnTheoUser.UpdateNhomDuAnTheoUser(nhomDuAnTheoUsertVm);
                    newNhomDuAnTheoUser.CreatedDate = DateTime.Now;
                    newNhomDuAnTheoUser.CreatedBy = User.Identity.GetUserId();
                    _nhomDuAnTheoUserService.Add(newNhomDuAnTheoUser);
                    _nhomDuAnTheoUserService.Save();

                    var responseData = Mapper.Map<NhomDuAnTheoUser, NhomDuAnTheoUserViewModels>(newNhomDuAnTheoUser);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage Update(HttpRequestMessage request, NhomDuAnTheoUserViewModels nhomDuAnTheoUserVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbNhomDuAnTheoUser = _nhomDuAnTheoUserService.GetById(nhomDuAnTheoUserVm.IdNhomDuAn);

                    dbNhomDuAnTheoUser.UpdateNhomDuAnTheoUser(nhomDuAnTheoUserVm);
                    dbNhomDuAnTheoUser.UpdatedDate = DateTime.Now;
                    dbNhomDuAnTheoUser.UpdatedBy = User.Identity.GetUserId();
                    _nhomDuAnTheoUserService.Update(dbNhomDuAnTheoUser);
                    _nhomDuAnTheoUserService.Save();

                    var responseData = Mapper.Map<NhomDuAnTheoUser, NhomDuAnTheoUserViewModels>(dbNhomDuAnTheoUser);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhomDuAnTheoUserService.Delete(id);
                    _nhomDuAnTheoUserService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "NHOMDUANTHEOUSER")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listNhomDuAnTheoUser = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listNhomDuAnTheoUser)
                    {
                        _nhomDuAnTheoUserService.Delete(item);
                    }

                    _nhomDuAnTheoUserService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listNhomDuAnTheoUser.Count);
                }

                return response;
            });
        }
    }
}
