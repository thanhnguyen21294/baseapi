﻿using AutoMapper;
using Model.Models;
using Service;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/thamdinhlapquanlydautu")]
    public class ThamDinhLapQuanLyDauTuController : ApiControllerBase
    {
        public IThamDinhLapQuanLyDauTuService _thamDinhLapQuanLyDauTuService;

        public ThamDinhLapQuanLyDauTuController(IThamDinhLapQuanLyDauTuService thamDinhLapQuanLyDauTuService, IErrorService errorService) : base(errorService)
        {
            _thamDinhLapQuanLyDauTuService = thamDinhLapQuanLyDauTuService;
        }

        [Route("getallfothistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _thamDinhLapQuanLyDauTuService.GetAllForHistory();

                IEnumerable<ThamDinhLapQuanLyDauTuViewModels> modelVm = Mapper.Map<IEnumerable<ThamDinhLapQuanLyDauTu>, IEnumerable<ThamDinhLapQuanLyDauTuViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "QUANLYDAUTU")]
        public HttpResponseMessage GetByFilter(HttpRequestMessage request, int idLapQuanLyDauTu, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                var model = _thamDinhLapQuanLyDauTuService.GetByFilter(idLapQuanLyDauTu, page, pageSize, "", out totalRow, filter);

                IEnumerable<ThamDinhLapQuanLyDauTuViewModels> modelVm = Mapper.Map<IEnumerable<ThamDinhLapQuanLyDauTu>, IEnumerable<ThamDinhLapQuanLyDauTuViewModels>>(model);
                PaginationSet<ThamDinhLapQuanLyDauTuViewModels> pagedSet = new PaginationSet<ThamDinhLapQuanLyDauTuViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{idThamDinhLapQuanLyDauTu}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int idThamDinhLapQuanLyDauTu)
        {
            HttpResponseMessage response = null;

            var model = _thamDinhLapQuanLyDauTuService.GetById(idThamDinhLapQuanLyDauTu);

            ThamDinhLapQuanLyDauTuViewModels modelVm = Mapper.Map<ThamDinhLapQuanLyDauTu, ThamDinhLapQuanLyDauTuViewModels>(model);

            response = request.CreateResponse(HttpStatusCode.OK, modelVm);

            return response;
        }

        [HttpPost]
        [Route("add")]
        [Permission(Action = "Create", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Add(HttpRequestMessage request, ThamDinhLapQuanLyDauTuViewModels thamDinhLapQuanLyDauTuVm)
        {
            return CreateHttpResponse(request, () =>
             {
                 HttpResponseMessage response = null;
                 ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu = new ThamDinhLapQuanLyDauTu();
                 thamDinhLapQuanLyDauTu.UpdateThamDinhLapQuanLyDauTu(thamDinhLapQuanLyDauTuVm);
                 thamDinhLapQuanLyDauTu.Status = true;
                 _thamDinhLapQuanLyDauTuService.Add(thamDinhLapQuanLyDauTu);
                 _thamDinhLapQuanLyDauTuService.Save();
                 var responseData = Mapper.Map<ThamDinhLapQuanLyDauTu, ThamDinhLapQuanLyDauTuViewModels>(thamDinhLapQuanLyDauTu);
                 response = request.CreateResponse(HttpStatusCode.Created, responseData);
                 return response;
             });
        }

        [HttpPut]
        [Route("update")]
        [Permission(Action = "Update", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThamDinhLapQuanLyDauTuViewModels thamDinhLapQuanLyDauTuVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu = new ThamDinhLapQuanLyDauTu();
                thamDinhLapQuanLyDauTu.UpdateThamDinhLapQuanLyDauTu(thamDinhLapQuanLyDauTuVm);
                _thamDinhLapQuanLyDauTuService.Update(thamDinhLapQuanLyDauTu);
                _thamDinhLapQuanLyDauTuService.Save();
                var responseData = Mapper.Map<ThamDinhLapQuanLyDauTu, ThamDinhLapQuanLyDauTuViewModels>(thamDinhLapQuanLyDauTu);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                return response;
            });
        }

        [HttpDelete]
        [Route("deletemulti")]
        [Permission(Action = "Delete", Function = "QUANLYDAUTU")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var lstThamDinhLapQuanLyDauTu = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                foreach (var item in lstThamDinhLapQuanLyDauTu)
                {
                    _thamDinhLapQuanLyDauTuService.Delete(item);
                }
                _thamDinhLapQuanLyDauTuService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, lstThamDinhLapQuanLyDauTu.Count);
                return response;
            }
            );
        }
    }
}