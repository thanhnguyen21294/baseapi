﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan03")]
    public class BaoCaoQuyetToan03Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private INguonVonService _nguonVonService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private INguonVonGoiThauThanhToanService _nguonVonGoiThauThanhToanService;
        private ITamUngHopDongService _tamUngHopDongService;
        private INguonVonGoiThauTamUngService _nguonVonGoiThauTamUngService;
        private INguonVonThanhToanChiPhiKhacService _nguonVonThanhToanChiPhiKhacService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        public BaoCaoQuyetToan03Controller(INguonVonGoiThauTamUngService nguonVonGoiThauTamUngService, IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, INguonVonThanhToanChiPhiKhacService nguonVonThanhToanChiPhiKhacService, ITamUngHopDongService tamUngHopDongService, INguonVonGoiThauThanhToanService nguonVonGoiThauThanhToanService, IThanhToanHopDongService thanhToanHopDongService, INguonVonService nguonVonService ,IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._nguonVonService = nguonVonService;
            this._thanhToanHopDongService = thanhToanHopDongService;
            this._nguonVonGoiThauThanhToanService = nguonVonGoiThauThanhToanService;
            this._tamUngHopDongService = tamUngHopDongService;
            this._nguonVonThanhToanChiPhiKhacService = nguonVonThanhToanChiPhiKhacService;
            this._thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            this._nguonVonGoiThauTamUngService = nguonVonGoiThauTamUngService;
        }
        #region Mau03
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau03")]
        [HttpGet]
        public HttpResponseMessage ExportMau03(HttpRequestMessage request, int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau03(iIDDuAn, szNguonVon, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau03(int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-03-Mau_TinhHinhSuDungVon.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc3-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //Tên dự án
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        if (duan.IdDuAnCha != null)
                        {
                            var duancha = _duAnService.GetByIdForBaoCao((int)duan.IdDuAnCha);
                            sheet.Cells["A3"].Value = "Tên dự án: " + duancha.TenDuAn;
                            sheet.Cells["A3"].Style.WrapText = true;
                            sheet.Cells["A4"].Value = "Tên công trình, hạng mục công trình: " + duan.TenDuAn;
                            sheet.Cells["A4"].Style.WrapText = true;
                        }
                        if (duan.IdDuAnCha == null)
                        {
                            sheet.Cells["A3"].Value = "Tên dự án: " + duan.TenDuAn;
                            sheet.Cells["A3"].Style.WrapText = true;
                            sheet.Cells["A4"].Value = "Tên công trình, hạng mục công trình: ";
                        }

                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //Chủ đầu tư
                        sheet.Cells["A5"].Value = "Chủ đầu tư: " + duan.ChuDauTu?.TenChuDauTu;

                        //Nguồn vốn
                        var nguonvon = _nguonVonService.GetById(szNguonVon);
                        sheet.Cells["A2"].Value = "Nguồn vốn: " + nguonvon.TenNguonVon;
                        var thanhtoanhopdong = _thanhToanHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        //thanh toans KLTH
                        double thanhtoanKLTH = 0;
                        foreach (var item in thanhtoanhopdong)
                        {
                            var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                            foreach (var item1 in nguonvongoithauthanhtoan)
                            {
                                thanhtoanKLTH += item1.GiaTri;
                            }
                        }
                        // thanh toan chi phi khac
                        var nguonvonthanhtoanchiphikhac = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        var thanhtoanchiphikhac = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in nguonvonthanhtoanchiphikhac)
                        {
                            thanhtoanKLTH += item.GiaTri;
                        }

                        sheet.Cells["D13"].Value = thanhtoanKLTH / dvt;
                        sheet.Cells["G13"].Value = thanhtoanKLTH / dvt;

                        sheet.Cells["I13"].Value = 0;
                        //tạm ứng
                        double tamung = 0;
                        var tamunghopdong = _tamUngHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        foreach (var item in tamunghopdong)
                        {
                            var tamung1 = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                            foreach (var item1 in tamung1)
                            {
                                tamung += item1.GiaTri;
                            }
                        }

                        sheet.Cells["E13"].Value = tamung / dvt;
                        sheet.Cells["H13"].Value = tamung / dvt;
                        sheet.Cells["C13"].Value = (thanhtoanKLTH + tamung) / dvt;
                        sheet.Cells["F13"].Value = (thanhtoanKLTH + tamung) / dvt;

                        // chi tiết thanh toán tạm ứng
                        //lấy ra năm
                        List<int> namthanhtoan = new List<int>();

                        foreach (var item in thanhtoanhopdong)
                        {
                            if (item.ThoiDiemThanhToan != null)
                            {
                                var nam = item.ThoiDiemThanhToan.Value.Year;
                                var indexnam = 0;
                                for (var i = 0; i < namthanhtoan.Count; i++)
                                {
                                    if (namthanhtoan[i] == nam)
                                    {
                                        indexnam = 1;
                                    }
                                }
                                if (indexnam != 1)
                                {
                                    namthanhtoan.Add(nam);
                                }
                            }
                        }

                        foreach (var item in thanhtoanchiphikhac)
                        {
                            if (item.NgayThanhToan != null)
                            {
                                var nam = item.NgayThanhToan.Value.Year;
                                var indexnam = 0;
                                for (var i = 0; i < namthanhtoan.Count; i++)
                                {
                                    if (namthanhtoan[i] == nam)
                                    {
                                        indexnam = 1;
                                    }
                                }
                                if (indexnam != 1)
                                {
                                    namthanhtoan.Add(nam);
                                }
                            }
                        }
                        //sap xep mang tang dan
                        for (var i = 0; i < namthanhtoan.Count; i++)
                        {
                            for (var j = i + 1; j < namthanhtoan.Count; j++)
                            {
                                if (namthanhtoan[j] > 0 && namthanhtoan[i] > 0 && namthanhtoan[j] < namthanhtoan[i])
                                {
                                    //cach trao doi gia tri
                                    var tmp = namthanhtoan[i];
                                    namthanhtoan[i] = namthanhtoan[j];
                                    namthanhtoan[j] = tmp;
                                }
                            }
                        }
                        var rowIndex = 14;
                        for (var i = 0; i < namthanhtoan.Count; i++)
                        {
                            if (namthanhtoan[i] > 0)
                            {
                                rowIndex++;
                                sheet.InsertRow(rowIndex, 1);
                                sheet.Cells[rowIndex, 2].Value = namthanhtoan[i];
                                sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                //tongsotheoanm
                                double tongsotheonam = 0;
                                foreach (var item in thanhtoanhopdong)
                                {
                                    if (item.ThoiDiemThanhToan != null)
                                    {
                                        if (item.ThoiDiemThanhToan.Value.Year == namthanhtoan[i])
                                        {
                                            var nguonvongt = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                                            foreach (var item1 in nguonvongt)
                                            {
                                                tongsotheonam += item1.GiaTri;
                                            }
                                        }
                                    }
                                }
                                foreach (var item in nguonvonthanhtoanchiphikhac)
                                {
                                    foreach (var item1 in thanhtoanchiphikhac)
                                    {
                                        if (item1.NgayThanhToan != null)
                                        {
                                            if (item1.NgayThanhToan.Value.Year == namthanhtoan[i] && item1.IdThanhToanChiPhiKhac == item.IdThanhToanChiPhiKhac)
                                            {
                                                tongsotheonam += item.GiaTri;
                                            }
                                        }
                                    }
                                }
                                sheet.Cells[rowIndex, 4].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 7].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 9].Value = 0;
                                sheet.Cells[rowIndex, 3].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 7].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 9].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                //tamungtheonam
                                double tamungtheonam = 0;
                                foreach (var item in tamunghopdong)
                                {
                                    if (item.NgayTamUng != null)
                                    {
                                        if (item.NgayTamUng.Value.Year == namthanhtoan[i])
                                        {
                                            var nguonvontu = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                                            foreach (var item1 in nguonvontu)
                                            {
                                                tamungtheonam += item1.GiaTri;
                                            }
                                        }
                                    }
                                }
                                sheet.Cells[rowIndex, 5].Value = tamungtheonam / dvt;
                                sheet.Cells[rowIndex, 8].Value = tamungtheonam / dvt;
                                sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 8].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 3].Value = (tongsotheonam + tamungtheonam) / dvt;
                                sheet.Cells[rowIndex, 6].Value = (tongsotheonam + tamungtheonam) / dvt;
                            }

                        }

                        string dvTinh = sheet.Cells["H9"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["H9"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 03/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
