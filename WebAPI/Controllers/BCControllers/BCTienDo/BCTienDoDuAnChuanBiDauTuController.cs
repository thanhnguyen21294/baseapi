﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Controllers.QLTDControllers;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BCTienDoDuAnChuanBiDauTuController : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_KeHoachTienDoChungService _qLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private IQLTD_CTCV_ChuanBiDauTuService _qLTD_CTCV_ChuanBiDauTuService;
        public BCTienDoDuAnChuanBiDauTuController(IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duAnService,
            IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IBaoCaoTableService baoCaoTableService,
            IQLTD_CTCV_ChuanBiDauTuService qLTD_CTCV_ChuanBiDauTuService) : base(errorService)
        {
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._qLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            this._qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            _qLTD_CTCV_ChuanBiDauTuService = qLTD_CTCV_ChuanBiDauTuService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };

        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaotiendoDACbiDTu")]
        [HttpGet]
        public HttpResponseMessage ExportTienDoDACbiDTu(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = TienDoDAnCbiDTu(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string TienDoDAnCbiDTu(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TienDoDuAnChuanBiDauTu.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoTienDoDuAnChuanBiDauTu_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            //try
            //{
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {

                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                    var dicDuAnTMDT = _qLTD_CTCV_ChuanBiDauTuService.GetTMDT();
                    //int iNam_1 = iNamPD - 1;
                    #region Cây dự án
                    List<object> lstTTDACbiDTu = _baoCaoTienDoService.GetListDACbiDTu(szIDDuAn);
                    Dictionary<int, List<object>> dicDAConCbiDTu = _baoCaoTienDoService.GetDuAnCon(szIDDuAn);

                    List<object> lstTTDATD = _baoCaoTienDoService.GetListDA(szIDDuAn);
                    #endregion

                    Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                    var users = AppUserManager.Users;
                    foreach (AppUser user in users)
                    {
                        var roleTemp = AppUserManager.GetRoles(user.Id);
                        if (roleTemp.Contains("Nhân viên"))
                        {
                            dicUserNhanVien.Add(user.Id, user.FullName);
                        }
                    }
                    var lstPhong_CanBoQuanLy = _duAnService.GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(szIDDuAn, dicUserNhanVien);

                    #region Chủ trương đầu tư
                    Dictionary<int, int> dic_CTDT_TGHTCT = _baoCaoTienDoService.Getdic_CTDT_TGHTCT(szIDDuAn);
                    Dictionary<int, List<object>> dic_CTDT_NGNV = _baoCaoTienDoService.Getdic_CTDT_NGNV(szIDDuAn);
                    Dictionary<int, List<object>> dic_CTDT_CTDD = _baoCaoTienDoService.Getdic_CTDT_CTDD(szIDDuAn);
                    Dictionary<int, List<object>> dic_CTDT_LCTDT = _baoCaoTienDoService.Getdic_CTDT_LCTDT(szIDDuAn);
                    Dictionary<int, List<object>> dic_CTDT_NTHSPD = _baoCaoTienDoService.Getdic_CTDT_NTHSPD(szIDDuAn);
                    Dictionary<int, List<object>> dic_CTDT_PDCTDT = _baoCaoTienDoService.Getdic_CTDT_PDCTDT(szIDDuAn);

                    IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CTDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                    IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CTDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);

                    //Dictionary<int, string> dic_CTDT_KK = _baoCaoTienDoService.Getdic_CTDT_KK(szIDDuAn);
                    //Dictionary<int, string> dic_CTDT_GP = _baoCaoTienDoService.Getdic_CTDT_GP(szIDDuAn);
                    //Dictionary<int, List<object>> dic_CTDT_TDC = _baoCaoTienDoService.Getdic_CTDT_TDC(szIDDuAn);

                    //Dictionary<int, string> dicNgayGiaoNvu = _baoCaoTienDoService.GetNgayGiaoNV_LCTDT(szIDDuAn);
                    //Dictionary<int, List<object>> dicTT_LCTDT = _baoCaoTienDoService.GetTT_LCTDT(szIDDuAn);
                    //Dictionary<int, List<object>> dicTTKK_LCTDT = _baoCaoTienDoService.GetdicKKVM_CTDT(szIDDuAn);
                    #endregion

                    #region Chuẩn bị đầu tư
                    //Dictionary<int, List<object>> dicTT_CBiDTu = _baoCaoTienDoService.GetdicTT_CBiDTu(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_BCPATK = _baoCaoTienDoService.Getdic_CBDT_BCPATK(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_PDT_CBDT = _baoCaoTienDoService.Getdic_CBDT_PDT_CBDT(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_PKHKCNT = _baoCaoTienDoService.Getdic_CBDT_PKHKCNT(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_KSHT_CGDD_HTKT = _baoCaoTienDoService.Getdic_CBDT_KSHT_CGDD_HTKT(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_PDQH = _baoCaoTienDoService.Getdic_CBDT_PDQH(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_KSDC = _baoCaoTienDoService.Getdic_CBDT_KSDC(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_HTPDDA = _baoCaoTienDoService.Getdic_CBDT_HTPDDA(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_TTDPDDA = _baoCaoTienDoService.Getdic_CBDT_TTDPDDA(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_PCCC_TTK = _baoCaoTienDoService.Getdic_CBDT_PCCC_TTK(szIDDuAn);
                    Dictionary<int, List<object>> dic_CBDT_DLHS = _baoCaoTienDoService.Getdic_CBDT_DLHS(szIDDuAn);

                    //Dictionary<int, string> dic_CBDT_KK = _baoCaoTienDoService.Getdic_CBDT_KK(szIDDuAn);
                    //Dictionary<int, string> dic_CBDT_GP = _baoCaoTienDoService.Getdic_CBDT_GP(szIDDuAn);
                    //Dictionary<int, List<object>> dic_CBDT_TDC = _baoCaoTienDoService.Getdic_CBDT_TDC(szIDDuAn);
                    IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                    IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);

                    //dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS
                    #endregion

                    #region Kế hoạch vốn
                    #region Kế hoạch vốn
                    Dictionary<int, double> Dic_TD_DA_CBDT_KHV = _baoCaoTienDoService.GetDic_TD_DA_CBDT_KHV(szIDDuAn, iNamPD);
                    #endregion
                    #region Giải ngân
                    Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN = _vonCapNhatKLNTService.GetGiaiNganVon(szIDDuAn);
                    //Dictionary<int, double> Dic_TD_DA_CBDT_GN = _baoCaoTienDoService.GetDic_TD_DA_CBDT_GN(szIDDuAn, iNamPD);
                    #endregion
                    #region Kế hoạch vốn điều chỉnh
                    Dictionary<int, double> dic_DA_KHV_DXDC = _baoCaoTienDoService.Getdic_DA_KHV_DXDC(szIDDuAn, iNamPD);
                    #endregion
                    #endregion

                    #region Tiến độ thực hiện
                    #region Lập chủ trương đầu tư
                    Dictionary<int, string> dic_TDTH_LCTDT = _baoCaoTienDoService.GetDic_TDTH_LCTDT(szIDDuAn);
                    #endregion
                    #region Chuẩn bị đầu tư
                    Dictionary<int, string> dic_TDTH_CBDT = _baoCaoTienDoService.GetDic_TDTH_CBDT(szIDDuAn);
                    #endregion
                    #endregion

                    int iColumn = 0;
                    int iSTTcap1 = 1;
                    int iSTTcap2 = 1;

                    int rowCDT = 11;

                    int iTongCong = rowCDT - 1;
                    string[] az = new string[] { "C", "D", "E", "F", "L", "M", "O", "V", "Y", "AD", "AF", "AJ", "AN", "AP", "AQ", "BA", "BD", "BE", "BH", "BI", "BJ", "BM", "BP" };

                    int[] ar_So = { 1, 3, 4, 5, 6, 12, 13, 14, 15, 16, 20, 21, 23, 25, 29, 31, 36, 37, 38, 40, 41, 43, 44, 47, 48, 53 };
                    int[] ar_Text = { 2, 7, 8, 9, 10, 11, 17, 18, 19, 24, 26, 27, 28, 33, 34, 35, 39, 45, 46, 49, 50, 51, 52, 54, 55, 66 };
                    int[] ar_Tien = { 22, 30, 32, 42, 56, 57, 58, 60, 61, 62, 65, 68 };
                    int[] ar_PhanTram = { 58 };
                    int[] ar_KHV = { 56, 60, 61, 62 };

                    Format(sheet, 10, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                    sheet.Cells["B10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    List<int> lstInt_TongCong = new List<int>();

                    if (lstTTDACbiDTu.Count != 0)
                    {
                        int iTT = rowCDT;
                        lstInt_TongCong.Add(rowCDT);
                        sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                        sheet.Cells[rowCDT, 2].Value = "CHUẨN BỊ ĐẦU TƯ NĂM " + namPD;
                        sheet.Row(rowCDT).Style.Font.Bold = true;
                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                        rowCDT++;
                        iColumn++;
                        int iSoLaMa = 1;

                        List<int> l_LVNNs = new List<int>();

                        foreach (object oLVNN in lstTTDACbiDTu)
                        {
                            string TenLinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oLVNN, "TenLinhVucNganhNghe");

                            int iLV = rowCDT;
                            l_LVNNs.Add(rowCDT);
                            sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                            sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;
                            sheet.Row(rowCDT).Style.Font.Bold = true;
                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                            rowCDT++;

                            List<object> lGDDT = (List<object>)BaoCaoDungChung.GetValueObject(oLVNN, "grpGDDT");

                            List<int> l_GDDAs = new List<int>();

                            foreach (object oGDDT in lGDDT)
                            {
                                string TenGiaiDoanDuAn = (string)BaoCaoDungChung.GetValueObject(oGDDT, "TenGiaiDoanDuAn");
                                int iGD = rowCDT;
                                l_GDDAs.Add(rowCDT);

                                sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1;
                                sheet.Cells[rowCDT, 2].Value = "Dự án " + TenGiaiDoanDuAn;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                sheet.Row(rowCDT).Style.Font.Italic = true;
                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                rowCDT++;

                                List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oGDDT, "grpTTDA");
                                List<int> l_TTDAs = new List<int>();

                                foreach (object oTTDA in lTTDA)
                                {

                                    string TenTinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");

                                    int iTTDA = rowCDT;
                                    l_TTDAs.Add(rowCDT);
                                    sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1 + "." + iSTTcap2;
                                    sheet.Cells[rowCDT, 2].Value = "Dự án " + TenTinhTrangDuAn;
                                    sheet.Row(rowCDT).Style.Font.Bold = true;
                                    sheet.Row(rowCDT).Style.Font.Italic = true;
                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                    rowCDT++;

                                    List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");

                                    int d = 0;
                                    List<int> l_GDLs = new List<int>();

                                    foreach (object oDACha in lDACha)
                                    {
                                        d++;
                                        XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBoQuanLy,
                                                dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT, /*dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT, /* dic_CBDT_KK, dic_CBDT_GP, dic_CBDT_TDC,*/
                                                Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                dic_TDTH_LCTDT, dic_TDTH_CBDT);

                                        l_GDLs.Add(rowCDT);
                                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                        rowCDT++;

                                        int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");

                                        if (dicDAConCbiDTu.ContainsKey(IDDA))
                                        {

                                            List<object> lDuAnCon = dicDAConCbiDTu[IDDA];

                                            foreach (object oDuAnCon in lDuAnCon)
                                            {
                                                XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBoQuanLy,
                                                    dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT,/* dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                    dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT,/* dic_CBDT_KK, dic_CBDT_GP, dic_CBDT_TDC,*/
                                                    Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                    dic_TDTH_LCTDT, dic_TDTH_CBDT);
                                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                                rowCDT++;
                                            }
                                        }
                                    }
                                    SumRow(sheet, iTTDA, l_GDLs, az);
                                    Ty_Le_Giai_Ngan(sheet, iTTDA);
                                    iSTTcap2++;
                                }
                                iSTTcap2 = 1;
                                SumRow(sheet, iGD, l_TTDAs, az);
                                Ty_Le_Giai_Ngan(sheet, iGD);
                                iSTTcap1++;
                            }
                            iSTTcap1 = 1;
                            SumRow(sheet, iLV, l_GDDAs, az);
                            Ty_Le_Giai_Ngan(sheet, iLV);
                            iSoLaMa++;
                        }
                        SumRow(sheet, iTT, l_LVNNs, az);
                        Ty_Le_Giai_Ngan(sheet, iTT);
                    }
                    iSTTcap1 = 1;
                    iSTTcap2 = 1;

                    SumRow(sheet, iTongCong, lstInt_TongCong, az);
                    Ty_Le_Giai_Ngan(sheet, iTongCong);
                    //UpdateTongXDCB(sheet, 9, l_CDTs);

                    int rowIndex = rowCDT - 1;
                    using (ExcelRange rng = sheet.Cells["A9:BP" + rowIndex])
                    {
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        rng.Style.WrapText = true;
                        rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rng.Style.Font.Name = "Times New Roman";
                        rng.Style.Font.Size = 11;
                        rng.AutoFitColumns();
                    }

                    az = new string[] { "A2", "A3", "Y6", "BA7", "L8", "M8", "BD8", "BH8", "BI8" };
                    foreach (var item in az)
                    {
                        sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{nam}}", namPD);
                    }

                    az = new string[] { "BG5", "V8", "AD8", "AF8", "AP8", "BM6" };
                    foreach (var item in az)
                    {
                        sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    }

                    sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet.Cells.AutoFitColumns();

                    int[] ar_i = new int[] { 2 };
                    DoRongCot(sheet, ar_i, 25);

                    ar_i = new int[] { 7, 9, 10, 11 };
                    DoRongCot(sheet, ar_i, 20);

                    ar_i = new int[] { 14, 16, 17, 18, 19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 38, 39, 41, 42, 44, 45, 46, 47, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58, 60, 61, 62, 65, 68 };
                    DoRongCot(sheet, ar_i, 15);

                    ar_i = new int[] { 1, 3, 4, 5, 6, 12, 13, 15, 25, 36, 40, 43, 53 };
                    DoRongCot(sheet, ar_i, 5);

                    sheet.Column(1).Width = 7;

                    sheet.Column(8).Width = 60;

                    pck.SaveAs(new FileInfo(fullPath));

                    //Thêm báo cáo vào database
                    _baoCaoTableService.AddBaoCao(documentName, "BAOCAOTIENDOCBDT", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }

        public void Format(ExcelWorksheet sheet, int row, int[] ar_So, int[] ar_Text, int[] ar_Tien, int[] ar_PhanTram)
        {
            BaoCaoDungChung.In_Style(sheet, row, ar_So, 1);
            BaoCaoDungChung.In_Style(sheet, row, ar_Text, 2);
            BaoCaoDungChung.In_Style(sheet, row, ar_Tien, 3);
            BaoCaoDungChung.In_Style(sheet, row, ar_PhanTram, 4);
        }

        public void DoRongCot(ExcelWorksheet sheet, int[] ar_So, int dorong)
        {
            foreach (var item in ar_So)
            {
                sheet.Column(item).Width = dorong;
            }
        }

        public void Ty_Le_Giai_Ngan(ExcelWorksheet sheet, int row)
        {
            sheet.Cells[row, 58].Formula = "=(BE" + row + "/BD" + row + ")";
            int[] ar_i = new int[] { 60, 61 };
            string[] ar_sz = new string[] { "BD", "BJ" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, row, ar_i, ar_sz);
        }

        private void XuatDuAnXDCB(ExcelWorksheet sheet, int iNam, string szDonViTinh, object oDA, string d, int rowDA,
            IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> lstPhong_CanBoQuanLy,
            Dictionary<int, int> dic_CTDT_TGHTCT,
            Dictionary<int, List<object>> dic_CTDT_NGNV,
            Dictionary<int, List<object>> dic_CTDT_CTDD,
            Dictionary<int, List<object>> dic_CTDT_LCTDT,
            Dictionary<int, List<object>> dic_CTDT_NTHSPD,
            Dictionary<int, List<object>> dic_CTDT_PDCTDT,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CTDT,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CTDT,
            //Dictionary<int, string> dic_CTDT_KK,
            //Dictionary<int, string> dic_CTDT_GP,
            //Dictionary<int, List<object>> dic_CTDT_TDC,

            //dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS
            Dictionary<int, List<object>> dic_CBDT_BCPATK,
            Dictionary<int, List<object>> dic_CBDT_PDT_CBDT,
            Dictionary<int, List<object>> dic_CBDT_PKHKCNT,
            Dictionary<int, List<object>> dic_CBDT_KSHT_CGDD_HTKT,
            Dictionary<int, List<object>> dic_CBDT_PDQH,
            Dictionary<int, List<object>> dic_CBDT_KSDC,
            Dictionary<int, List<object>> dic_CBDT_HTPDDA,
            Dictionary<int, List<object>> dic_CBDT_TTDPDDA,
            Dictionary<int, List<object>> dic_CBDT_PCCC_TTK,
            Dictionary<int, List<object>> dic_CBDT_DLHS,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBDT,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBDT,
            //Dictionary<int, string> dic_CBDT_KK,
            //Dictionary<int, string> dic_CBDT_GP,
            //Dictionary<int, List<object>> dic_CBDT_TDC,

            //Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC
            Dictionary<int, double> Dic_TD_DA_CBDT_KHV,
            Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN,
            //Dictionary<int, double> Dic_TD_DA_CBDT_GN,
            Dictionary<int, double> dic_DA_KHV_DXDC,

            //dic_TDTH_LCTDT, dic_TDTH_CBDT
            Dictionary<int, string> dic_TDTH_LCTDT,
            Dictionary<int, string> dic_TDTH_CBDT
            )
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            int dDimKLNT = BaoCaoDungChung.GetdDimKLNT(szDonViTinh);

            double dGiaTri = 0;
            bool bol = true;
            string sz1 = "";
            string sz2 = "";
            string sz3 = "";
            string sz4 = "";
            string sz5 = "";
            string sz6 = "";
            int i;
            int[] ar_i;
            string[] ar_Sz1;
            string[] ar_Sz2;
            List<object> lst = new List<object>();
            string[] szAr = { "str1", "str2", "str3" };

            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDA, "IDDA");
            sz1 = (string)BaoCaoDungChung.GetValueObject(oDA, "TenDuAn");
            sheet.Cells[rowDA, 2].Value = sz1;
            sz1 = (string)BaoCaoDungChung.GetValueObject(oDA, "NhomDuAn");
            sz2 = (string)BaoCaoDungChung.GetValueObject(oDA, "DiaDiemXayDung");
            sz3 = (string)BaoCaoDungChung.GetValueObject(oDA, "NangLucThietKe");
            sz4 = (string)BaoCaoDungChung.GetValueObject(oDA, "ThoiGianThucHien");
            sz5 = (string)BaoCaoDungChung.GetValueObject(oDA, "MaSoDuAn");
            sz6 = (string)BaoCaoDungChung.GetValueObject(oDA, "TenChuDauTu");
            string szTMDT = BaoCaoDungChung.GetValueObject(oDA, "TMDT")?.ToString() ?? "";
            string TMDT_ChuaPD = BaoCaoDungChung.GetValueObject(oDA, "TMDT_ChuaPD")?.ToString() ?? "";
            string PDDA_BCKTKT_SoQD = (string)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_SoQD");

            sheet.Cells[rowDA, 1].Value = d;

            ar_i = new int[] { 4, 5, 6 };
            BaoCaoDungChung.In_Nhom_Du_An(sheet, rowDA, sz1, ar_i);

            sheet.Cells[rowDA, 3].Formula = "Sum(" + sheet.Cells[rowDA, 4] + ":" + sheet.Cells[rowDA, 6] + ")";

            ar_i = new int[] { 7, 8, 9, 10, 11 };
            ar_Sz1 = new string[] { sz2, sz3, sz4, sz5, sz6 };
            BaoCaoDungChung.In_Chuoi_Thong_Tin_Du_An(sheet, rowDA, ar_i, ar_Sz1);



            #region Chủ trương đầu tư

            if (dic_CTDT_TGHTCT.ContainsKey(IDDA))
            {
                i = dic_CTDT_TGHTCT[IDDA];
                if (i > iNam)
                {
                    sheet.Cells[rowDA, 13].Value = 1;
                }
                else
                {
                    sheet.Cells[rowDA, 12].Value = 1;
                }
            }

            if (dic_CTDT_NGNV.ContainsKey(IDDA))
            {
                lst = dic_CTDT_NGNV[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    sz1 = BaoCaoDungChung.GetDateValue(sz1);
                    sheet.Cells[rowDA, 14].Value = sz1;
                }
            }

            if (dic_CTDT_CTDD.ContainsKey(IDDA))
            {
                lst = dic_CTDT_CTDD[IDDA];
                foreach (object obj in lst)
                {
                    bol = (bool)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str3");

                    BaoCaoDungChung.In_Bool_value(sheet, rowDA, 15, bol);
                    sz2 = BaoCaoDungChung.GetDateValue(sz2);
                    sheet.Cells[rowDA, 16].Value = BaoCaoDungChung.So_Ngay(sz1, sz2);
                }
            }

            if (dic_CTDT_LCTDT.ContainsKey(IDDA))
            {
                lst = dic_CTDT_LCTDT[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");

                    sheet.Cells[rowDA, 18].Value = sz1;
                    sheet.Cells[rowDA, 19].Value = sz2;
                }
            }

            if (dic_CTDT_NTHSPD.ContainsKey(IDDA))
            {
                lst = dic_CTDT_NTHSPD[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    if (sz1 != "")
                    {
                        sz1 = BaoCaoDungChung.GetDateValue(sz1);
                    }
                    sheet.Cells[rowDA, 20].Value = sz1;
                }
            }

            if (dic_CTDT_PDCTDT.ContainsKey(IDDA))
            {
                lst = dic_CTDT_PDCTDT[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz2 = BaoCaoDungChung.GetDateValue(sz2);
                    dGiaTri = (double)BaoCaoDungChung.GetValueObject(obj, "str3") / dDim;
                    sz4 = (string)BaoCaoDungChung.GetValueObject(obj, "str4");
                    sz4 = BaoCaoDungChung.GetDateValue(sz4);

                    sheet.Cells[rowDA, 21].Value = BaoCaoDungChung.So_Ngay(sz1, sz2);
                    sheet.Cells[rowDA, 22].Value = dGiaTri;
                    sheet.Cells[rowDA, 23].Value = sz4;
                }
            }

            //dic_CTDT_KKGP
            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CTDT, IDDA, sz1, 24);
            ar_i = new int[] { 25, 26, 27 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CTDT, IDDA, sz1, sz2, ar_i);

            //ar_i = new int[] { 24 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_CTDT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //ar_i = new int[] { 25, 26 };
            //BaoCaoDungChung.In_Dic_String(dic_CTDT_KK, IDDA, sheet, rowDA, ar_i, sz1, 1);
            //ar_i = new int[] { 0, 27 };
            //BaoCaoDungChung.In_Dic_String(dic_CTDT_GP, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion

            #region Chuẩn bị đầu tư
            if (dic_CBDT_BCPATK.ContainsKey(IDDA))
            {
                lst = dic_CBDT_BCPATK[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    sheet.Cells[rowDA, 28].Value = sz1;
                }
            }

            ar_i = new int[] { 0, 29, 30 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_PDT_CBDT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 0, 31, 32 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_PKHKCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 33, 34, 35 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_KSHT_CGDD_HTKT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            if (dic_CBDT_PDQH.ContainsKey(IDDA))
            {
                lst = dic_CBDT_PDQH[IDDA];
                foreach (object obj in lst)
                {
                    bol = (bool)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz3 = (string)BaoCaoDungChung.GetValueObject(obj, "str3");
                    sz4 = (string)BaoCaoDungChung.GetValueObject(obj, "str4");
                    sz5 = (string)BaoCaoDungChung.GetValueObject(obj, "str5");

                    if (sz3 != "")
                    {
                        sz3 = BaoCaoDungChung.GetDateValue(sz3);
                    }

                    if (sz5 != "")
                    {
                        sz5 = BaoCaoDungChung.GetDateValue(sz5);
                    }

                    BaoCaoDungChung.In_Bool_value(sheet, rowDA, 36, bol);
                    sheet.Cells[rowDA, 37].Value = BaoCaoDungChung.So_Ngay(sz2, sz3);
                    sheet.Cells[rowDA, 38].Value = BaoCaoDungChung.So_Ngay(sz4, sz5);
                }
            }

            ar_i = new int[] { 39 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_KSDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 43, 44 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBDT_TTDPDDA, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 40, 41, 42 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_HTPDDA, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            if (Convert.ToInt32(sheet.Cells[rowDA, 40].Value) == 1)
            {
                sheet.Cells[rowDA, 43].Value = 0;
            }

            ar_i = new int[] { 45, 46 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str1", "str2" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_PCCC_TTK, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 47 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 0, 48 };
            BaoCaoDungChung.In_Date_Value(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, sz1, "str3");

            ar_i = new int[] { 50, 51 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str4", "str5" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CBDT, IDDA, sz1, 52);
            ar_i = new int[] { 53, 54, 55 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CBDT, IDDA, sz1, sz2, ar_i);

            //ar_i = new int[] { 52 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_CBDT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //ar_i = new int[] { 53, 54 };
            //BaoCaoDungChung.In_Dic_String(dic_CBDT_KK, IDDA, sheet, rowDA, ar_i, sz1, 1);
            //ar_i = new int[] { 0, 55 };
            //BaoCaoDungChung.In_Dic_String(dic_CBDT_GP, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion

            #region Kế hoạch vốn
            double dGiaTri1;
            double dGiaTri2;
            if (Dic_TD_DA_CBDT_KHV.ContainsKey(IDDA))
            {
                dGiaTri = Dic_TD_DA_CBDT_KHV[IDDA] / dDim;
                sheet.Cells[rowDA, 56].Value = dGiaTri;
            }
            else
            {
                sheet.Cells[rowDA, 56].Value = 0;
            }
            dGiaTri1 = dGiaTri;

            //if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            //{
            //    dGiaTri2 = Dic_TD_DA_CBDT_GN[IDDA] / dDim;
            //    sheet.Cells[rowDA, 57].Value = dGiaTri2;
            //        sheet.Cells[rowDA, 58].Formula = "=(BE" + rowDA + "/BD" + rowDA + ")";
            //}
            //else
            //{
            //    sheet.Cells[rowDA, 58].Value = 0;
            //}

            if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(item, "str2");

                    sheet.Cells[rowDA, 57].Value = (Convert.ToDouble(sz1) + Convert.ToDouble(sz2)) / dDimKLNT;
                    sheet.Cells[rowDA, 58].Formula = "=(BE" + rowDA + "/BD" + rowDA + ")";
                }
            }
            else
            {
                sheet.Cells[rowDA, 58].Value = 0;
            }

            if (dic_DA_KHV_DXDC.ContainsKey(IDDA))
            {
                dGiaTri = dic_DA_KHV_DXDC[IDDA] / dDim;
                sheet.Cells[rowDA, 62].Value = dGiaTri;
            }
            else
            {
                dGiaTri = 0;
            }
            dGiaTri2 = dGiaTri;

            ar_i = new int[] { 60, 61 };
            ar_Sz1 = new string[] { "BD", "BJ" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, rowDA, ar_i, ar_Sz1);
            #endregion

            if (lstPhong_CanBoQuanLy.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = lstPhong_CanBoQuanLy.Where(x => x.IDDA == IDDA).Select(x => x.TenPhong).FirstOrDefault();
                sheet.Cells[rowDA, 63].Value = sz1;

                sz2 = "";
                var lstUser = lstPhong_CanBoQuanLy.Where(x => x.IDDA == IDDA).Select(x => x.Users).FirstOrDefault();
                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    sz2 += sz + ",";
                }
                if (sz2.Length > 1)
                {
                    sz2 = sz2.Substring(0, sz2.Length - 1);
                }
                sheet.Cells[rowDA, 64].Value = sz2;
            }
            sheet.Cells[rowDA, 65].Value = !string.IsNullOrEmpty(TMDT_ChuaPD) ? Convert.ToDouble(TMDT_ChuaPD) : 0;
            sheet.Cells[rowDA, 66].Formula = null;
            sheet.Cells[rowDA, 66].Value = PDDA_BCKTKT_SoQD;
            //sheet.Cells[rowDA, 67].Value = PDDA_BCKTKT_NgayPD.ToString("MMMM dd, yyyy");
            //if (PDDA_BCKTKT_NgayPD != null)
            //sheet.Cells[rowDA, 67].Value = PDDA_BCKTKT_NgayPD.ToString();
            //Nullable<DateTime> PDDA_BCKTKT_NgayPD = null;
            try
            {
                if (BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD") != null)
                {
                    DateTime PDDA_BCKTKT_NgayPD = (DateTime)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD");
                    sheet.Cells[rowDA, 67].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
                }
            }
            catch (Exception e) { }

            sheet.Cells[rowDA, 68].Value = !string.IsNullOrEmpty(szTMDT) ? Convert.ToDouble(szTMDT) : 0;

            #region Tiến độ thực hiện
            //dic_TDTH_LCTDT, dic_TDTH_CBDT
            #region Lập chủ trương đầu tư
            if (dic_TDTH_LCTDT.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 17].Value = dic_TDTH_LCTDT[IDDA];
            }
            #endregion
            #region Chuẩn bị đầu tư
            if (dic_TDTH_CBDT.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 49].Value = dic_TDTH_CBDT[IDDA];
            }
            #endregion
            #endregion
            ar_i = new int[] { 1, 3, 4, 5, 6, 12, 13, 14, 15, 16, 20, 21, 23, 25, 29, 31, 36, 37, 38, 40, 41, 43, 44, 47, 48, 53 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 1);

            ar_i = new int[] { 2, 7, 8, 9, 10, 11, 17, 18, 19, 24, 26, 27, 28, 33, 34, 35, 39, 45, 46, 49, 50, 51, 52, 54, 55 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 2);

            ar_i = new int[] { 22, 30, 32, 42, 56, 57, 58, 60, 61, 62, 65, 68 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 3);

            ar_i = new int[] { 58 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 4);

        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {

                foreach (var row in lstInt)
                {
                    str += sz + row.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr)
        {
            foreach (var sz in szAr)
            {
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
            }
        }
        #endregion
    }


}
