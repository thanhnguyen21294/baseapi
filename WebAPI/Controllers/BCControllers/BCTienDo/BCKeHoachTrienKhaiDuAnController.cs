﻿using Common;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BCKeHoachTrienKhaiDuAnController : ApiControllerBase
    {
        private IHopDongService _HopDongService;

        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IBaoCaoTableService _baoCaoTableService;
        private IQLTD_KeHoachTienDoChungService _qLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;

        public BCKeHoachTrienKhaiDuAnController(IErrorService errorService,
            IQLTD_KeHoachService QLTD_KeHoachService,
            IHopDongService HopDongService,
            IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IQLTD_CTCV_ChuanBiDauTuService QLTD_CTCV_ChuanBiDauTuService,
            IQLTD_CTCV_ThucHienDuAnService QLTD_CTCV_ThucHienDuAnService,
            IBaoCaoTableService baoCaoTableService
            ) : base(errorService)
        {
            this._QLTD_KeHoachService = QLTD_KeHoachService;
            this._baoCaoTableService = baoCaoTableService;
            _qLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            _qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            _baoCaoTienDoService = baoCaoTienDoService;
            _vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._HopDongService = HopDongService;
            _QLTD_CTCV_ChuanBiDauTuService = QLTD_CTCV_ChuanBiDauTuService;
            _QLTD_CTCV_ThucHienDuAnService = QLTD_CTCV_ThucHienDuAnService;
        }

        [Route("BC_QLTD_KeHoachTrienKhaiDuAn")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCao_KeHoachTrienKhaiDuAn(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh, int? isTMDT = null, int? anChiTiet = null, string ngayCBDT = null, string ngayTHDT=null)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCaoQLTD_KeHoachTrienKhaiDuAn(iIDDuAn, iNamPD, szDonViTinh, isTMDT, anChiTiet, ngayCBDT, ngayTHDT);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoQLTD_KeHoachTrienKhaiDuAn(string szIDDuAn, int iNamPD, string szDonViTinh, int? isTMDT = null, int? anChiTiet = null, string ngayCBDT = null, string ngayTHDT = null)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/KeHoachTrienKhaiCacDuAnNew.xlsx");
            if (isTMDT == 1)
                templateDocument = HttpContext.Current.Server.MapPath("~/Templates/KeHoachTrienKhaiCacDuAnNew_TMDT.xlsx");
            string documentName = string.Format("BaoCaoQLTD_KeHoachTrienKhaiCacDuAn" + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            //try
            //{
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                    Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                    var users = AppUserManager.Users;
                    foreach (AppUser user in users)
                    {
                        var roleTemp = AppUserManager.GetRoles(user.Id);
                        if (roleTemp.Contains("Nhân viên"))
                        {
                            dicUserNhanVien.Add(user.Id, user.FullName);
                        }
                    }
                    var lstData = _QLTD_KeHoachService.Get_BC_KeHoachTrienKhaiCacDuAn(szIDDuAn, dicUserNhanVien);

                    int rowIndex = 6;
                    int count = 0;

                    rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex, szDonViTinh, dicUserNhanVien, szIDDuAn, iNamPD, isTMDT, ngayCBDT, ngayTHDT);
                    //int a = rowIndex + 3;

                    sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[2, 1].Value = "Ngày " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    sheet.Cells["G5"].Value = sheet.Cells["G5"].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["H5"].Value = sheet.Cells["H5"].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["K5"].Value = sheet.Cells["K5"].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["L5"].Value = sheet.Cells["L5"].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    if (rowIndex > 5)
                    {
                        if (isTMDT == 0)
                        {
                            using (ExcelRange rng = sheet.Cells["A5:V" + rowIndex])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.WrapText = true;
                                rng.Style.Font.Size = 11;
                            }

                            sheet.Cells["W5"].Formula = "=Sum(W6:W" + rowIndex + ")";
                            sheet.Cells["X5"].Formula = "=Sum(X6:X" + rowIndex + ")";
                        }
                        else
                        {
                            using (ExcelRange rng = sheet.Cells["A5:Z" + rowIndex])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.WrapText = true;
                                rng.Style.Font.Size = 11;
                            }
                            sheet.Cells["AA5"].Formula = "=Sum(AA6:AA" + rowIndex + ")";
                            sheet.Cells["AB5"].Formula = "=Sum(AB6:AB" + rowIndex + ")";
                        }
                    }

                    if (anChiTiet == 1)
                    {
                        sheet.Column(5).Hidden = true;
                    }
                    pck.SaveAs(new FileInfo(fullPath));

                    //Thêm báo cáo vào database
                    _baoCaoTableService.AddBaoCao(documentName, "BC_QLTD_KHTKDA", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<BCKeHoachTrienKhaiDuAn> lstData, int stt1, int rowIndex, string szDonViTinh, Dictionary<string, string> dicUserNhanVien, string szIDDuAn, int iNamPD, int? isTMDT, string ngayCBDT, string ngayTHDT)
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            var tdgnAll = _vonCapNhatKLNTService.GetGiaTriAll(szIDDuAn, iNamPD, dicUserNhanVien);
            var lstKHV = _baoCaoTienDoService.Get_KHV_For_BaoCao_KeHoachGN(szIDDuAn, iNamPD).ToList();
            var khtkcacda = _QLTD_KeHoachService.Get_BC_KeHoachTrienKhaiCacDuAn(szIDDuAn, dicUserNhanVien);
            var queryGT = _HopDongService.GetHopDong(dicUserNhanVien, szIDDuAn);
            IEnumerable<BC_Uyban_PL1_KHTDC_New> lst_TDCs = _qLTD_KeHoachTienDoChungService.Get_ND_TDC_For_BC_ThucHien_KeHoachTrienKhaiDA_New(szIDDuAn);
            IEnumerable<BC_Uyban_PL1_KKGP_New> lst_KKGPs = _qLTD_KhoKhanVuongMacService.Get_NoiDung_KK_GP_For_BaoCao_New();
            var dicDuanTMDT = _QLTD_CTCV_ChuanBiDauTuService.GetTMDT();

            Dictionary<int, DuAnHoanThanh> dicDuAnHoanThanhCBDT = new Dictionary<int, DuAnHoanThanh>();
            if (ngayCBDT != null && ngayCBDT != "")
            {
                DateTime date1 = DateTime.ParseExact(ngayCBDT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                dicDuAnHoanThanhCBDT = _QLTD_CTCV_ChuanBiDauTuService.GetDuAnHoanThanhCBDT(date1);
            }

            Dictionary<int, DuAnHoanThanh> dicDuAnHoanThanhTHDT = new Dictionary<int, DuAnHoanThanh>();
            if (ngayTHDT != null && ngayTHDT != "")
            {
                DateTime date2 = DateTime.ParseExact(ngayTHDT, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                dicDuAnHoanThanhTHDT = _QLTD_CTCV_ThucHienDuAnService.GetDuAnHoanThanhTHDT(date2);
            }

            foreach (var da in lstData)
            {
                stt1++;
                var khv = lstKHV.Where(x => x.IDDA == da.IDDA);
                double? dblHuyen = 0, dblTP = 0;
                if (khv.Count() > 0)
                {
                    dblHuyen = khv.FirstOrDefault().dGiaTriHuyen;
                    if (dblHuyen != null)
                        sheet.Cells[rowIndex, 7].Value = dblHuyen / dDim;
                    dblTP = khv.FirstOrDefault().dGiaTriTP;
                    if (dblTP != null)
                        sheet.Cells[rowIndex, 11].Value = dblTP / dDim;
                }

                var temp = tdgnAll.Where(x => x.IDDA == da.IDDA);
                if (temp.Count() > 0)
                {
                    var dbGNHuyen = temp.FirstOrDefault().TDGNHuyen;
                    if (dbGNHuyen != "")
                        sheet.Cells[rowIndex, 8].Value = Convert.ToDouble(dbGNHuyen);

                    var dbGNTP = temp.FirstOrDefault().TDGNTP;
                    if (dbGNTP != "")
                        sheet.Cells[rowIndex, 12].Value = Convert.ToDouble(dbGNTP);
                }
                sheet.Cells[rowIndex, 9].Formula = "If(G" + rowIndex + "= 0, 0, H" + rowIndex + "/G" + rowIndex + ")";
                sheet.Cells[rowIndex, 13].Formula = "If(L" + rowIndex + "= 0, 0, K" + rowIndex + "/L" + rowIndex + ")";

                sheet.Cells[rowIndex, 1].Value = stt1;
                sheet.Cells[rowIndex, 2].Value = da.TenDuAn;

                if (isTMDT == 1)
                {
                    if (dicDuanTMDT.ContainsKey(da.IDDA))
                    {
                        var daTMDT = dicDuanTMDT[da.IDDA];
                        sheet.Cells[rowIndex, 21].Value = daTMDT.TMDTDA;
                        sheet.Cells[rowIndex, 22].Value = daTMDT.SoQD;
                        sheet.Cells[rowIndex, 23].Value = daTMDT.NgayKeHoach;
                        sheet.Cells[rowIndex, 24].Value = daTMDT.TMDTPD;
                    }
                    if (dicDuAnHoanThanhCBDT.ContainsKey(da.IDDA))
                    {
                        sheet.Cells[rowIndex, 25].Value = BaoCaoDungChung.GetDateValue(dicDuAnHoanThanhCBDT[da.IDDA].NgayHoanThanh.ToString());
                        sheet.Cells[rowIndex, 27].Value = dicDuAnHoanThanhCBDT[da.IDDA].SoSanh;
                    }
                    if (dicDuAnHoanThanhTHDT.ContainsKey(da.IDDA))
                    {
                        sheet.Cells[rowIndex, 26].Value = BaoCaoDungChung.GetDateValue(dicDuAnHoanThanhTHDT[da.IDDA].NgayHoanThanh.ToString());
                        sheet.Cells[rowIndex, 28].Value = dicDuAnHoanThanhTHDT[da.IDDA].SoSanh;
                    }
                }
                else
                {
                    if (dicDuAnHoanThanhCBDT.ContainsKey(da.IDDA))
                    {
                        sheet.Cells[rowIndex, 21].Value = BaoCaoDungChung.GetDateValue(dicDuAnHoanThanhCBDT[da.IDDA].NgayHoanThanh.ToString());
                        sheet.Cells[rowIndex, 23].Value = dicDuAnHoanThanhCBDT[da.IDDA].SoSanh;
                    }
                    if (dicDuAnHoanThanhTHDT.ContainsKey(da.IDDA))
                    {
                        sheet.Cells[rowIndex, 22].Value = BaoCaoDungChung.GetDateValue(dicDuAnHoanThanhTHDT[da.IDDA].NgayHoanThanh.ToString());
                        sheet.Cells[rowIndex, 24].Value = dicDuAnHoanThanhTHDT[da.IDDA].SoSanh;
                    }
                }
                sheet.Row(rowIndex).Style.Font.Bold = true;
                sheet.Row(rowIndex).Style.Font.Size = 13;
                rowIndex++;

                sheet.Cells[rowIndex, 2].Value = "Phòng phụ trách: " + da.TenPhong;
                rowIndex++;

                var lstUser = da.Users.ToList();
                string szUser = "";

                foreach (var us in lstUser)
                {
                    string sz = us.Name;
                    szUser += sz + ",";
                }
                if (szUser.Length > 1)
                {
                    szUser = szUser.Substring(0, szUser.Length - 1);
                }
                sheet.Cells[rowIndex, 2].Value = "Cán bộ phụ trách: " + szUser;
                rowIndex++;

                var query = khtkcacda.Where(x => x.IDDA == da.IDDA);
                int iGT = rowIndex;
                List<int> l_GTs = new List<int>();
                foreach (var it1 in query)
                {
                    var stt2 = 0;
                    foreach (var it2 in it1.grpNhomGiaiDoan)
                    {
                        stt2++;
                        sheet.Cells[rowIndex, 1].Value = stt1 + "." + stt2;
                        sheet.Cells[rowIndex, 2].Value = it2.TenNhomGiaiDoan;

                        sheet.Row(rowIndex).Style.Font.Bold = true;
                        sheet.Row(rowIndex).Style.Font.Size = 12;
                        l_GTs.Add(rowIndex);
                        var stt3 = 0;

                        List<DateTime> lstDtGiaiDoan = new List<DateTime>();

                        //List<DateTime> lstDtGiaiDoanTuNgay = new List<DateTime>();
                        //List<DateTime> lstDtGiaiDoanDenNgay = new List<DateTime>();
                        string bdGiaiDoan = "";
                        string ktGiaiDoan = "";
                        int iGD = 0;
                        rowIndex++;

                        foreach (var gd in it2.grpGiaiDoan)
                        {
                            stt3++;
                            iGD++;
                            sheet.Cells[rowIndex, 1].Value = stt1 + "." + stt2 + "." + stt3;
                            sheet.Cells[rowIndex, 2].Value = gd.TenGiaiDoan;

                            sheet.Row(rowIndex).Style.Font.Bold = true;
                            sheet.Row(rowIndex).Style.Font.Italic = true;
                            sheet.Row(rowIndex).Style.Font.Size = 12;
                            l_GTs.Add(rowIndex);
                            rowIndex++;

                            int stt4 = 0;

                            List<DateTime> lstDtCviec = new List<DateTime>();
                            //List<DateTime> lstDtCviecTuNgay = new List<DateTime>();
                            //List<DateTime> lstDtCviecDenNgay = new List<DateTime>();
                            string bdCongViec = "";
                            string ktCongViec = "";
                            List<int> l_TDC = new List<int>();
                            int iIDKH = 0;
                            int HoanThanhGD = -1;
                            foreach (var cv in gd.grpCongViec)
                            {
                                stt4++;
                                iGD++;
                                var szTuNgay = BaoCaoDungChung.GetDateValue(cv.TuNgay);
                                var szDenNgay = BaoCaoDungChung.GetDateValue(cv.DenNgay);

                                if (szTuNgay == "01/01/1970" || cv.TuNgay == "1/1/1970 12:00:00 AM")
                                {
                                    szTuNgay = "";
                                }
                                if (cv.TuNgay != "1/1/1970 12:00:00 AM")
                                {
                                    if (stt4 == 1)
                                    {
                                        bdCongViec = cv.TuNgay;
                                        if (stt3 == 1)
                                        {
                                            bdGiaiDoan = cv.TuNgay;
                                        }
                                    }
                                    else if (bdCongViec == "")
                                    {
                                        bdCongViec = cv.TuNgay;
                                        if (stt3 == 1)
                                        {
                                            bdGiaiDoan = cv.TuNgay;
                                        }
                                    }
                                    ktGiaiDoan = cv.TuNgay;
                                    ktCongViec = cv.TuNgay;
                                }

                                if (cv.DenNgay == "01/01/1970" || cv.DenNgay == "1/1/1970 12:00:00 AM")
                                {
                                    szDenNgay = "";
                                }
                                if (cv.DenNgay != "1/1/1970 12:00:00 AM")
                                {
                                    ktGiaiDoan = cv.DenNgay;
                                    ktCongViec = cv.DenNgay;
                                }

                                sheet.Cells[rowIndex, 1].Value = "-";
                                sheet.Cells[rowIndex, 2].Value = cv.TenCongViec;
                                sheet.Cells[rowIndex, 3].Value = szTuNgay;
                                sheet.Cells[rowIndex, 4].Value = szDenNgay;

                                //AddItemToListDateTime(cv.TuNgay, szTuNgay, lstDtCviecTuNgay);
                                //AddItemToListDateTime(cv.DenNgay, szDenNgay, lstDtCviecDenNgay);

                                sheet.Row(rowIndex).Style.Font.Size = 11;

                                string szNDCV = _QLTD_KeHoachService.Get_NoiDungCV_For_BC_ThucHien_KeHoachTrienKhaiDA(cv.IDKH, cv.IDCV, dDim);
                                sheet.Cells[rowIndex, 5].Value = szNDCV;

                                iIDKH = cv.IDKH;
                                HoanThanhGD = cv.HoanThanh;
                                l_TDC.Add(rowIndex);
                                l_GTs.Add(rowIndex);
                                rowIndex++;
                            }

                            IEnumerable<BC_Uyban_PL1_KHTDC_New> lst_TDC = lst_TDCs.Where(x => x.IDKH == iIDKH);
                            IEnumerable<BC_Uyban_PL1_KKGP_New> lst_KKGP = lst_KKGPs.Where(x => x.IDKH == iIDKH);

                            int iRDau = l_TDC.Min();
                            int iRCuoi = l_TDC.Max();

                            using (ExcelRange rng = sheet.Cells["F" + iRDau + ":F" + iRCuoi])
                            {
                                rng.Merge = true;
                                if (HoanThanhGD < 1)
                                {
                                    In_TDC(sheet, iRDau, lst_TDC, da.IDDA);
                                }
                            }

                            using (ExcelRange rng = sheet.Cells["O" + iRDau + ":O" + iRCuoi])
                            {
                                rng.Merge = true;
                                if (HoanThanhGD < 1)
                                {
                                    In_KK(sheet, iRDau, lst_KKGP, da.IDDA);
                                }
                            }

                            using (ExcelRange rng = sheet.Cells["P" + iRDau + ":P" + iRCuoi])
                            {
                                rng.Merge = true;
                                if (HoanThanhGD < 1)
                                {
                                    In_GP(sheet, iRDau, lst_KKGP, da.IDDA);
                                }
                            }
                            //AddItemToListDateTimeGiaiDoan(lstDtCviecTuNgay, lstDtGiaiDoanTuNgay);
                            //AddItemToListDateTimeGiaiDoan(lstDtCviecDenNgay, lstDtGiaiDoanDenNgay);

                            sheet.Cells[rowIndex - stt4 - 1, 3].Value = BaoCaoDungChung.GetDateValue(bdCongViec);
                            sheet.Cells[rowIndex - stt4 - 1, 4].Value = BaoCaoDungChung.GetDateValue(ktCongViec);
                            //In_Value(lstDtCviecTuNgay, sheet, rowIndex, stt4, 3, 2);
                            //In_Value(lstDtCviecDenNgay, sheet, rowIndex, stt4, 4, 1);
                        }
                        //In_Value(lstDtGiaiDoanTuNgay, sheet, rowIndex, iGD, 3, 2);
                        sheet.Cells[rowIndex - iGD - 1, 3].Value = BaoCaoDungChung.GetDateValue(bdGiaiDoan);
                        sheet.Cells[rowIndex - iGD - 1, 4].Value = BaoCaoDungChung.GetDateValue(ktGiaiDoan);
                        //In_Value(lstDtGiaiDoanDenNgay, sheet, rowIndex, iGD, 4, 1);


                        int iGT_Cuoi = l_GTs.Max();
                        if (queryGT.Count() > 0)
                        {
                            foreach (var itemLV in queryGT)
                            {
                                foreach (var itemGD in itemLV.grpGiaiDoan)
                                {
                                    var itemDAGT = itemGD.grpDuAn.Where(x => x.IDDA == da.IDDA);
                                    foreach (var itemDA in itemDAGT)
                                    {
                                        using (ExcelRange rng = sheet.Cells["T" + iGT + ":T" + iGT_Cuoi])
                                        {
                                            rng.Merge = true;
                                            string szGT = "";
                                            foreach (var itemGT in itemDA.grpGoiThau)
                                            {
                                                string szTenLoaiGoiThau = In_TenLoaiGoiThau(itemGT.LoaiGoiThau);
                                                szGT += szTenLoaiGoiThau + " : " + itemGT.NhaThauThucHien + "(" + BaoCaoDungChung.GetDateValue(itemGT.TuNgay) + " - " + BaoCaoDungChung.GetDateValue(itemGT.DenNgay) + "); ";
                                            }
                                            sheet.Cells[iGT, 20].Value = szGT;
                                            sheet.Cells[iGT, 20].Style.Font.Bold = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return rowIndex;
        }

        public void AddItemToListDateTime(string sz, string szDateTime, List<DateTime> lst)
        {
            if (szDateTime != "")
            {
                if (sz != "1/1/1970 12:00:00 AM")
                {
                    lst.Add(Convert.ToDateTime(sz));
                }
            }
        }

        public void AddItemToListDateTimeGiaiDoan(List<DateTime> lst1, List<DateTime> lst2)
        {
            if (lst1.Count > 0)
            {
                foreach (var item in lst1)
                {
                    lst2.Add(item);
                }
            }
        }

        public void In_Value(List<DateTime> lst, ExcelWorksheet sheet, int rowIndex, int soRowBiTru, int col, int loai)
        {
            string sz;
            if (lst.Count > 0)
            {
                if (loai == 1)
                {
                    sz = lst.OrderBy(x => x.Ticks).FirstOrDefault().ToString();
                }
                else
                {
                    sz = lst.OrderBy(x => x.Ticks).LastOrDefault().ToString();
                }
                sheet.Cells[rowIndex - soRowBiTru - 1, col].Value = BaoCaoDungChung.GetDateValue(sz);
            }
        }

        public void In_TDC(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KHTDC_New> lst_TDC, int? IDDA)
        {
            string sz = "";

            if (lst_TDC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz = lst_TDC.Where(x => x.IDDA == IDDA).Select(x => x.NoiDung).FirstOrDefault();
                sheet.Cells[rowDA, 6].Value = sz;
            }
        }

        public void In_KK(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KKGP_New> lstKKVM, int? IDDA)
        {
            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                string sz1 = "";

                var str = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.KK).FirstOrDefault();
                if (str != null)
                {
                    foreach (var item in str)
                    {
                        sz1 += item + "; ";
                    }
                    sheet.Cells[rowDA, 15].Value = sz1;
                }

            }
        }

        public void In_GP(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KKGP_New> lstKKVM, int? IDDA)
        {
            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                string sz2 = "";

                var str2 = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.GP).FirstOrDefault();
                if (str2 != null)
                {
                    foreach (var item2 in str2)
                    {
                        sz2 += item2 + "; ";
                    }
                    sheet.Cells[rowDA, 16].Value = sz2;
                }
            }
        }

        public string In_TenLoaiGoiThau(int? iLoaiGoiThau)
        {
            string sz = "";
            switch (iLoaiGoiThau)
            {
                case 1:
                    sz = "Khảo sát, lập bản đồ hiện trạng";
                    break;
                case 2:
                    sz = "Khảo sát địa hình bước lập dự án";
                    break;
                case 3:
                    sz = "Khảo sát địa chất bước lập dự án";
                    break;
                case 4:
                    sz = "Tư vấn lập dự án";
                    break;
                case 5:
                    sz = "Tư vấn thẩm tra BCNCKT/BCKTKT";
                    break;
                case 6:
                    sz = "Khảo sát địa hình bước thiết kế BVTC";
                    break;
                case 7:
                    sz = "Khảo sát địa chất bước thiết kế BVTC";
                    break;
                case 8:
                    sz = "Tư vấn TKBVTC";
                    break;
                case 9:
                    sz = "Tư vấn thẩm tra TKBVTC";
                    break;
                case 10:
                    sz = "Xây lắp 1";
                    break;
                case 11:
                    sz = "Xây lắp 2";
                    break;
                case 12:
                    sz = "Xây lắp 3";
                    break;
                case 13:
                    sz = "Xây lắp 4";
                    break;
                case 14:
                    sz = "Xây lắp 5";
                    break;
                case 15:
                    sz = "Xây lắp 6";
                    break;
                case 16:
                    sz = "Xây lắp 7";
                    break;
                case 17:
                    sz = "Xây lắp 8";
                    break;
                case 18:
                    sz = "Xây lắp 9";
                    break;
                case 19:
                    sz = "Xây lắp 10";
                    break;
                case 20:
                    sz = "Tư vấn giám sát 1";
                    break;
                case 21:
                    sz = "Tư vấn giám sát 2";
                    break;
                case 22:
                    sz = "Tư vấn giám sát 3";
                    break;
                case 23:
                    sz = "Tư vấn giám sát 4";
                    break;
                case 24:
                    sz = "Tư vấn giám sát 5";
                    break;
                case 25:
                    sz = "Tư vấn giám sát 6";
                    break;
                case 26:
                    sz = "Tư vấn giám sát 7";
                    break;
                case 27:
                    sz = "Tư vấn giám sát 8";
                    break;
                case 28:
                    sz = "Tư vấn giám sát 9";
                    break;
                case 29:
                    sz = "Tư vấn giám sát 10";
                    break;
                case 30:
                    sz = "Chống mối";
                    break;
                case 31:
                    sz = "Tư vấn giám sát chống mối";
                    break;
                case 32:
                    sz = "PCCC";
                    break;
                case 33:
                    sz = "Tư vấn giám sát PCCC";
                    break;
                case 34:
                    sz = "Thiết bị";
                    break;
                case 35:
                    sz = "Tư vấn giám sát thiết bị";
                    break;
                case 36:
                    sz = "Cây xanh";
                    break;
                case 37:
                    sz = "Tư vấn giám sát cây xanh";
                    break;
                case 38:
                    sz = "Hạ ngầm, di chuyển/TBA";
                    break;
                case 39:
                    sz = "Tư vấn giám sát hạ ngầm, di chuyển/TBA";
                    break;
                case 40:
                    sz = "Khác";
                    break;
                case 41:
                    sz = "Tư vấn giám sát khác";
                    break;
                case 42:
                    sz = "Bảo hiểm";
                    break;
                case 43:
                    sz = "Kiểm toán";
                    break;
                default:
                    break;
            }

            return sz;
        }
    }
}
