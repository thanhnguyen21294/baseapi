﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BCTienDoQuyetToanDuAnController : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        public BCTienDoQuyetToanDuAnController(IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duAnService, 
            IBaoCaoTienDoService baoCaoTienDoService, 
            IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaoTiendoQuyettoanDuan")]
        [HttpGet]
        public HttpResponseMessage ExportTienDoQToanDuAn(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = TienDoQuyetToanDuAn(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string TienDoQuyetToanDuAn(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TienDoQuyetToanDuAn.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoTienDoQuyetToanDuAn_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        #region Cây dự án
                        //List<object> lstTTDACbiDTu = _baoCaoTienDoService.GetListDACbiDTu(szIDDuAn);
                        List<object> lst_QT_CDA = _baoCaoTienDoService.GetList_QT_DA(szIDDuAn);
                        Dictionary<int, List<object>> dicDACon = _baoCaoTienDoService.GetDuAnCon(szIDDuAn);

                        //List<object> lstTTDATD = _baoCaoTienDoService.GetListDA(szIDDuAn);
                        #endregion

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }
                        var lstPhong_CanBo = _duAnService.GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(szIDDuAn, dicUserNhanVien);

                        #region Quyết Toán
                        Dictionary<int, List<object>> dic_QT_HTQT = _baoCaoTienDoService.Getdic_QT_HTQT(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_QLCL = _baoCaoTienDoService.Getdic_QT_HTHS_QLCL(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_HSQT = _baoCaoTienDoService.Getdic_QT_HTHS_HSQT(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_KTDL = _baoCaoTienDoService.Getdic_QT_HTHS_KTDL(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_TTQT = _baoCaoTienDoService.Getdic_QT_HTHS_TTQT(szIDDuAn);
                        Dictionary<int, string> dic_QT_KKGP = _baoCaoTienDoService.Getdic_QT_KKGP(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_TDC = _baoCaoTienDoService.Getdic_QT_TDC(szIDDuAn);
                        
                        #endregion

                        #region Kế hoạch vốn
                        #region Kế hoạch vốn
                        Dictionary<int, double> Dic_TD_DA_CBDT_KHV = _baoCaoTienDoService.GetDic_TD_DA_CBDT_KHV(szIDDuAn, iNamPD);
                        #endregion
                        #region Giải ngân
                        Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN = _vonCapNhatKLNTService.GetGiaiNganVon(szIDDuAn);
                        //Dictionary<int, double> Dic_TD_DA_CBDT_GN = _baoCaoTienDoService.GetDic_TD_DA_CBDT_GN(szIDDuAn, iNamPD);
                        #endregion
                        #region Kế hoạch vốn điều chỉnh
                        Dictionary<int, double> dic_DA_KHV_DXDC = _baoCaoTienDoService.Getdic_DA_KHV_DXDC(szIDDuAn, iNamPD);
                        #endregion
                        #endregion

                        int iColumn = 0;
                        int iSTTcap1 = 1;

                        int rowCDT = 11;

                        int[] ar_So = new int[] { 1, 3, 4, 5, 6, 12, 13, 18, 19, 20, 22, 26, 39, 42 };
                        int[] ar_Text = new int[] { 2, 7, 8, 9, 10, 11, 23, 25, 28, 29, 40, 41 };
                        int[] ar_Tien = new int[] { 14, 15, 16, 17, 21, 24, 27, 30, 31, 34, 35, 36, 39, 42 };
                        int[] ar_PhanTram = new int[] { 32 };
                        Format(sheet, 10, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                        int[] ar_KHV = new int[] { 30, 34, 35, 36 };
                        List<int> l_GDLs = new List<int>();
                        string[] az = { "C", "D", "E", "F", "L", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "AA", "AD", "AE", "AH", "AI", "AJ", "AM", "AP" };

                        int iTongCong = rowCDT - 1;
                        sheet.Cells["B10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        List<int> listInt_TongCong = new List<int>();

                        if (lst_QT_CDA.Count != 0)
                        {
                            foreach (object oTTDATD in lst_QT_CDA)
                            {
                                listInt_TongCong.Add(rowCDT);
                                int m = rowCDT;
                                string GiaiDoanDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDATD, "GiaiDoanDuAn");
                                sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                                sheet.Cells[rowCDT, 2].Value = GiaiDoanDuAn + " năm " + namPD;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                //iCDT = rowCDT--;
                                //rowCDT++;

                                rowCDT++;

                                //l_CDTs.Add(iCDT);

                                //sheet.Row(iCDT).Style.Font.Bold = true;

                                //int iTTDA;
                                List<object> lLVNN = (List<object>)BaoCaoDungChung.GetValueObject(oTTDATD, "grpLVNN");
                                List<int> listInt_LVNN = new List<int>();
                                int iSoLaMa = 1;

                                foreach (object oLVNN in lLVNN)
                                {
                                    int iLVNN = rowCDT;
                                    listInt_LVNN.Add(rowCDT);
                                    string TenLinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oLVNN, "TenLinhVucNganhNghe");

                                    sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                                    sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;
                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                    sheet.Row(rowCDT).Style.Font.Bold = true;

                                    //b++;

                                    //iTTDA = rowCDT--;
                                    //rowCDT++;
                                    rowCDT++;

                                    //l_TTDAs.Add(iTTDA);

                                    //sheet.Row(iTTDA).Style.Font.Bold = true;

                                    List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oLVNN, "grpTTDA");
                                    List<int> listInt_TTDA = new List<int>();

                                    //int c = 0;
                                    //int iLVNN;

                                    foreach (object oTTDA in lTTDA)
                                    {
                                        int iTTDA = rowCDT;
                                        listInt_TTDA.Add(rowCDT);
                                        string TenTinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");

                                        sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1;
                                        sheet.Cells[rowCDT, 2].Value = "Dự án " + TenTinhTrangDuAn;
                                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                        //c++;
                                        //iLVNN = rowCDT--;
                                        //rowCDT++;
                                        sheet.Row(rowCDT).Style.Font.Bold = true;

                                        sheet.Row(rowCDT).Style.Font.Italic = true;

                                        rowCDT++;

                                        //l_LVNNs.Add(iLVNN);


                                        //sheet.Row(iLVNN).Style.Font.Bold = true;

                                        List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");
                                        List<int> l_DAChas = new List<int>();

                                        int d = 0;

                                        foreach (object oDACha in lDACha)
                                        {
                                            d++;
                                            XuatDuAnXDCB(sheet, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBo,
                                                        dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, dic_QT_KKGP, dic_QT_TDC,
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC);
                                            l_DAChas.Add(rowCDT);
                                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                            //iDACha = rowCDT--;
                                            //rowCDT++;
                                            rowCDT++;

                                            //l_DAChas.Add(iDACha);

                                            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");



                                            if (dicDACon.ContainsKey(IDDA))
                                            {

                                                List<object> lDuAnCon = dicDACon[IDDA];

                                                foreach (object oDuAnCon in lDuAnCon)
                                                {
                                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                                    XuatDuAnXDCB(sheet, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBo,
                                                        dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, dic_QT_KKGP, dic_QT_TDC,
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC);
                                                    rowCDT++;
                                                }
                                            }

                                        }
                                        SumRow(sheet, iTTDA, l_DAChas, az);
                                        Ty_Le_Giai_Ngan(sheet, iTTDA);
                                        iSTTcap1++;
                                    }
                                    SumRow(sheet, iLVNN, listInt_TTDA, az);
                                    Ty_Le_Giai_Ngan(sheet, iLVNN);
                                    iSTTcap1 = 1;
                                    iSoLaMa++;
                                }
                                SumRow(sheet, m, listInt_LVNN, az);
                                Ty_Le_Giai_Ngan(sheet, m);
                                //UpdateTongXDCB(sheet, iCDT, l_TTDAs);
                                iSoLaMa = 0;
                            }
                            iColumn++;

                            SumRow(sheet, iTongCong, listInt_TongCong, az);
                            Ty_Le_Giai_Ngan(sheet, iTongCong);
                            //UpdateTongXDCB(sheet, 9, l_CDTs);

                            using (ExcelRange rng = sheet.Cells["A9:AP" + (rowCDT - 1)])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.WrapText = true;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                                rng.AutoFitColumns();
                            }
                        }

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        sheet.Cells.AutoFitColumns();

                        az = new string[] { "A2", "A3", "AD8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{nam}}", namPD);
                        }

                        az = new string[] { "AG5", "N8", "O8", "P8", "U8", "AA8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                        }

                        int[] ar_i = new int[] { 2 };
                        DoRongCot(sheet, ar_i, 25);

                        ar_i = new int[] { 7, 9, 10, 11 };
                        DoRongCot(sheet, ar_i, 20);

                        ar_i = new int[] { 13, 14, 15, 16, 17, 21, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,36 };
                        DoRongCot(sheet, ar_i, 15);

                        ar_i = new int[] { 1, 3, 4, 5, 6, 12, 18, 19, 20, 22 };
                        DoRongCot(sheet, ar_i, 5);

                        sheet.Column(1).Width = 8;
                        sheet.Column(8).Width = 60;

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BAOCAOTIENDOQTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public void Ty_Le_Giai_Ngan(ExcelWorksheet sheet, int row)
        {
            sheet.Cells[row, 32].Formula = "=(AE" + row + "/AD" + row + ")";

            int[] ar_i = new int[] { 34, 35 };
            string[] ar_sz = new string[] { "AD", "AJ" };

            BaoCaoDungChung.Tang_Giam_KHV(sheet, row,ar_i, ar_sz);
        }

        public void Format(ExcelWorksheet sheet, int row, int[] ar_So, int[] ar_Text, int[] ar_Tien, int[] ar_PhanTram)
        {
            BaoCaoDungChung.In_Style(sheet, row, ar_So, 1);
            BaoCaoDungChung.In_Style(sheet, row, ar_Text, 2);
            BaoCaoDungChung.In_Style(sheet, row, ar_Tien, 3);
            BaoCaoDungChung.In_Style(sheet, row, ar_PhanTram, 4);

        }

        public void DoRongCot(ExcelWorksheet sheet, int[] ar_So, int dorong)
        {
            foreach (var item in ar_So)
            {
                sheet.Column(item).Width = dorong;
            }
        }


        private void XuatDuAnXDCB(ExcelWorksheet sheet, string szDonViTinh, object oDA, string d, int rowDA,
            IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> lstPhong_CanBo,
            //dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT
            Dictionary<int, List<object>> dic_QT_HTQT,
            Dictionary<int, List<object>> dic_QT_HTHS_QLCL,
            Dictionary<int, List<object>> dic_QT_HTHS_HSQT,
            Dictionary<int, List<object>> dic_QT_HTHS_KTDL,
            Dictionary<int, List<object>> dic_QT_HTHS_TTQT,
            
            Dictionary<int, string> dic_QT_KKGP,
            Dictionary<int, List<object>> dic_QT_TDC,

            //dic_KHV_BD, dic_KHV_SDC, dic_KHV_TTCPK, dic_KHV_TTHD
            Dictionary<int, double> Dic_TD_DA_CBDT_KHV,
            //Dictionary<int, double> Dic_TD_DA_CBDT_GN,
            Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN,
            Dictionary<int, double> dic_DA_KHV_DXDC
            )
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            int dDimKLNT = BaoCaoDungChung.GetdDimKLNT(szDonViTinh);

            double dGiaTri = 0;
            bool bol = true;
            string sz1 = "";
            string sz2 = "";
            int[] ar_i;
            string[] ar_Sz1;
            string[] ar_Sz2;
            List<object> lst = new List<object>();
            string[] szAr = { "str1", "str2", "str3" };

            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "TenDuAn");
            string NhomDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "NhomDuAn");
            string DiaDiemXayDung = (string)BaoCaoDungChung.GetValueObject(oDA, "DiaDiemXayDung");
            string NangLucThietKe = (string)BaoCaoDungChung.GetValueObject(oDA, "NangLucThietKe");
            string ThoiGianThucHien = (string)BaoCaoDungChung.GetValueObject(oDA, "ThoiGianThucHien");
            string MaSoDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "MaSoDuAn");
            string TenChuDauTu = (string)BaoCaoDungChung.GetValueObject(oDA, "TenChuDauTu");
            
            sheet.Cells[rowDA, 1].Value = d;
            sheet.Cells[rowDA, 2].Value = TenDuAn;

            ar_i = new int[] { 4, 5, 6 };
            BaoCaoDungChung.In_Nhom_Du_An(sheet, rowDA, NhomDuAn, ar_i);

            sheet.Cells[rowDA, 3].Formula = "Sum(" + sheet.Cells[rowDA, 4] + ":" + sheet.Cells[rowDA, 6] + ")";

            ar_i = new int[] { 7, 8, 9, 10, 11 };
            ar_Sz1 = new string[] { DiaDiemXayDung, NangLucThietKe, ThoiGianThucHien, MaSoDuAn, TenChuDauTu };
            BaoCaoDungChung.In_Chuoi_Thong_Tin_Du_An(sheet, rowDA, ar_i, ar_Sz1);

            sheet.Cells[rowDA, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

            #region Quyết toán dự án
            //dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT
            //dic_QT_HTQT
            ar_i = new int[] { 12, 13, 14 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);
            ar_i = new int[] { 0, 15 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str4", dDim);
            ar_i = new int[] { 0, 16 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str5", dDim);
            ar_i = new int[] { 0, 17 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str6", dDim);

            //dic_QT_HTHS_QLCL
            ar_i = new int[] { 18, 19 };
            BaoCaoDungChung.In_Bool_Tong_value(dic_QT_HTHS_QLCL, IDDA, lst, sheet, rowDA, ar_i, bol, "str");

            //dic_QT_HTHS_HSQT
            ar_i = new int[] { 20, 21 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTHS_HSQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str", dDim);

            //dic_QT_HTHS_KTDL
            ar_i = new int[] { 0, 22 };
            BaoCaoDungChung.In_Bool_Tong_value(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, bol, "str1");

            ar_i = new int[] { 23 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str2" };
            BaoCaoDungChung.In_Chuoi(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 24 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str3", dDim);

            //dic_QT_HTHS_TTQT
            ar_i = new int[] { 25 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str1" };
            BaoCaoDungChung.In_Chuoi(dic_QT_HTHS_TTQT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 26, 27 };
            szAr = new string[] { "str2", "str3", "str4" };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_QT_HTHS_TTQT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 28 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str" };
            BaoCaoDungChung.In_Chuoi(dic_QT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 29 };
            BaoCaoDungChung.In_Dic_String(dic_QT_KKGP, IDDA, sheet, rowDA, ar_i, sz1, 0);

            //dic_CBDT_KKGP, dic_CBTH_KKGP, dic_THDT_KKGP, dic_QT_KKGP
            //if (dic_QT_KKGP.ContainsKey(IDDA))
            //{
            //    lst = dic_QT_KKGP[IDDA];
            //    foreach (object obj in lst)
            //    {
            //        sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str1");
            //        sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
            //        sz3 = sz1 + "," + sz2;
            //        sheet.Cells[rowDA, 28].Value = sz3;
            //    }
            //}
            #endregion
            
            try
            {
                string szTMDT = BaoCaoDungChung.GetValueObject(oDA, "TMDT")?.ToString() ?? "";
                string TMDT_ChuaPD = BaoCaoDungChung.GetValueObject(oDA, "TMDT_ChuaPD")?.ToString() ?? "";
                string PDDA_BCKTKT_SoQD = (string)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_SoQD");
                DateTime PDDA_BCKTKT_NgayPD = (DateTime)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD");
                sheet.Cells[rowDA, 41].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
                sheet.Cells[rowDA, 39].Value = !string.IsNullOrEmpty(TMDT_ChuaPD) ? Convert.ToDouble(TMDT_ChuaPD) : 0;
                sheet.Cells[rowDA, 40].Value = PDDA_BCKTKT_SoQD;
                sheet.Cells[rowDA, 42].Value = !string.IsNullOrEmpty(szTMDT) ? Convert.ToDouble(szTMDT) : 0;
            }
            catch (Exception e) { }
            
            #region Kế hoạch vốn
            double dGiaTri1;
            double dGiaTri2;
            if (Dic_TD_DA_CBDT_KHV.ContainsKey(IDDA))
            {
                dGiaTri = Dic_TD_DA_CBDT_KHV[IDDA] / dDim;
                sheet.Cells[rowDA, 30].Value = dGiaTri;
            }
            else
            {
                sheet.Cells[rowDA, 30].Value = 0;
            }
            dGiaTri1 = dGiaTri;

            if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(item, "str2");

                    sheet.Cells[rowDA, 31].Value = (Convert.ToDouble(sz1) + Convert.ToDouble(sz2))/dDimKLNT;
                    sheet.Cells[rowDA, 32].Formula = "=(AE" + rowDA + "/AD" + rowDA + ")";
                }
            }
            else
            {
                sheet.Cells[rowDA, 31].Value = 0;
            }

            //if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            //{
            //    dGiaTri2 = Dic_TD_DA_CBDT_GN[IDDA] / dDim;
            //    sheet.Cells[rowDA, 31].Value = dGiaTri2;

            //    sheet.Cells[rowDA, 32].Formula = "=(AE" + rowDA + "/AD" + rowDA + ")";
            //}
            //else
            //{
            //    sheet.Cells[rowDA, 31].Value = 0;
            //}

            if (dic_DA_KHV_DXDC.ContainsKey(IDDA))
            {
                dGiaTri = dic_DA_KHV_DXDC[IDDA] / dDim;
                sheet.Cells[rowDA, 36].Value = dGiaTri;
            }
            else
            {
                dGiaTri = 0;
            }
            dGiaTri2 = dGiaTri;

            //ar_i = new int[] { 30, 34, 35, 36 };
            ar_i = new int[] { 34, 35 };
            ar_Sz1 = new string[] { "AD", "AJ" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, rowDA,ar_i,ar_Sz1);


            if (lstPhong_CanBo.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.TenPhong).FirstOrDefault();
                sheet.Cells[rowDA, 37].Value = sz1;

                sz2 = "";
                var lstUser = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.Users).FirstOrDefault();
                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    sz2 += sz + ",";
                }
                if (sz2.Length > 1)
                {
                    sz2 = sz2.Substring(0, sz2.Length - 1);
                }
                sheet.Cells[rowDA, 38].Value = sz2;
            }

            #endregion

            ar_i = new int[] { 1, 3, 4, 5, 6, 12, 13, 18, 19, 20, 22, 26 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 1);

            ar_i = new int[] { 2, 7, 8, 9, 10, 11, 23, 25, 28,29 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 2);

            ar_i = new int[] { 14, 15, 16, 17, 21, 24, 27, 30, 31, 34, 35, 36 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 3);

            ar_i = new int[] { 32 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 4);
        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {

                foreach (var row in lstInt)
                {
                    str += sz + row.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr)
        {
            foreach (var sz in szAr)
            {
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
            }
        }
        #endregion
    }
}
