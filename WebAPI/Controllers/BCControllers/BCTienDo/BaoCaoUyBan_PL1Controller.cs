﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BaoCaoUyBan_PL1Controller : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_CTCV_ChuanBiDauTuService _qLTD_CTCV_ChuanBiDauTuService;
        private IQLTD_KeHoachTienDoChungService _qLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private IQLTD_CTCV_ChuTruongDauTuService _qLTD_CTCV_ChuTruongDauTuService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        private IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;

        public BaoCaoUyBan_PL1Controller(IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duAnService,
            IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_CTCV_ChuanBiDauTuService qLTD_CTCV_ChuanBiDauTuService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IQLTD_CTCV_ChuTruongDauTuService qLTD_CTCV_ChuTruongDauTuService,
            IQLTD_KeHoachService qLTD_KeHoachService,
                        IQLTD_CTCV_ChuanBiDauTuService QLTD_CTCV_ChuanBiDauTuService,
            IQLTD_CTCV_ThucHienDuAnService QLTD_CTCV_ThucHienDuAnService,
            IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._qLTD_CTCV_ChuanBiDauTuService = qLTD_CTCV_ChuanBiDauTuService;
            this._qLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            this._qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            this._qLTD_CTCV_ChuTruongDauTuService = qLTD_CTCV_ChuTruongDauTuService;
            this._qLTD_KeHoachService = qLTD_KeHoachService;
            _QLTD_CTCV_ChuanBiDauTuService = QLTD_CTCV_ChuanBiDauTuService;
            _QLTD_CTCV_ThucHienDuAnService = QLTD_CTCV_ThucHienDuAnService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaoUybanPL1")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoUyBan_PL1(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCaoUyBan_PL1(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoUyBan_PL1(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BaoCaoUyBan_PL1.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoUyBan_PL1-{0:ddMMMyyyy-H'h'm'm's}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            //try
            //{
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                    #region Cây dự án
                    //var lst = _baoCaoTienDoService.GetCayDuAn(szIDDuAn);

                    List<object> lstTD_DA_CBDT_CDA = _baoCaoTienDoService.GetListTD_DA_CBDT_CDA(szIDDuAn);
                    //Dictionary<int, List<object>> dicDACon = _baoCaoTienDoService.GetdicTD_DA_DAC(szIDDuAn);
                    Dictionary<int, List<object>> dicDAConCbiDTu = _baoCaoTienDoService.GetDuAnCon(szIDDuAn);
                    Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                    var users = AppUserManager.Users;
                    foreach (AppUser user in users)
                    {
                        var roleTemp = AppUserManager.GetRoles(user.Id);
                        if (roleTemp.Contains("Nhân viên"))
                        {
                            dicUserNhanVien.Add(user.Id, user.FullName);
                        }
                    }
                    IEnumerable<BC_TienDo_UB_PL1_CayDuAn> lstData = _baoCaoTienDoService.Get_BC_UB_PL1_CayDA(szIDDuAn, dicUserNhanVien);

                    #endregion

                    #region Kế hoạch vốn
                    var lst_KHV = _baoCaoTienDoService.Get_KHV_For_BaoCao(szIDDuAn, iNamPD);
                    #endregion
                    #region Giải ngân
                    var lst_GN = _baoCaoTienDoService.Get_GN_For_BaoCao_PL1(szIDDuAn, iNamPD);
                    #endregion

                    #region Sửa
                    IEnumerable<BC_Uyban_PL1_PDDA> dic_PDDA_CBDT = _baoCaoTienDoService.GetDic_PDDA_CBDT(szIDDuAn);

                    IEnumerable<BC_Uyban_PL1_PDDA> dic_THS_CBDT = _qLTD_CTCV_ChuanBiDauTuService.Get_TrinhHoSo_ForBC_TienDo_PL1(szIDDuAn);
                    IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CBDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CBDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 1);

                    IEnumerable<BC_Uyban_PL1_PDDA> dic_PD_CTDT = _qLTD_CTCV_ChuTruongDauTuService.Get_PDCTDT_ForBC_TienDo_PL1(szIDDuAn);
                    IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CTDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CTDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 1);

                    IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_DBTD = _qLTD_CTCV_ChuanBiDauTuService.Get_DamBaoTienDo_ForBC_TienDo_PL1(szIDDuAn);
                    IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_Truoc3110 = _qLTD_CTCV_ChuanBiDauTuService.Get_ThucHienTruoc3110_ForBC_TienDo_PL1(szIDDuAn, iNamPD);

                    IEnumerable<BC_Uyban_PL1_PDDA> lst_CBDT_DaPheDuyet = _qLTD_CTCV_ChuanBiDauTuService.Get_CBDT_DaPheDuyet_ForBC_TienDo_PL1(szIDDuAn);
                    #endregion
                    
                    int rowIndex = 9;
                    int count = 0;

                    rowIndex = XuatBaoCao(sheet,
                        lstData,
                        dic_PDDA_CBDT,
                        dic_THS_CBDT,
                        dic_TDC_CBDT,
                        dic_KKGP_CBDT,
                        dic_PD_CTDT,
                        dic_TDC_CTDT,
                        dic_KKGP_CTDT,
                        dic_DA_DBTD,
                        dic_DA_Truoc3110,
                        lst_CBDT_DaPheDuyet,
                        lst_KHV,
                        lst_GN,
                        count, rowIndex, szDonViTinh, lstTD_DA_CBDT_CDA, dicDAConCbiDTu);

                    int[] ar_i = new int[] { 1, 5, 11, 12, 13, 14, 15, 16, 28, 26, 29 };
                    BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 1);

                    ar_i = new int[] { 2, 3, 4, 17, 18, 19, 20, 28 };
                    BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 2);

                    ar_i = new int[] { 6, 7, 26, 29 };
                    BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 3);

                    ar_i = new int[] { 8 };
                    BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 4);

                    if (rowIndex > 9)
                    {
                        using (ExcelRange rng = sheet.Cells["A9:AC" + (rowIndex - 1)])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.WrapText = true;
                        }
                    }

                    sheet.Cells["O4"].Value = sheet.Cells["O4"].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["E5"].Value = sheet.Cells["E5"].Value.ToString().Replace("{{nam}}", iNamPD.ToString());
                    sheet.Cells["G5"].Value = sheet.Cells["G5"].Value.ToString().Replace("{{nam}}", iNamPD.ToString());
                    sheet.Cells["V5"].Value = sheet.Cells["V5"].Value.ToString().Replace("{{nam + 1}}", (iNamPD + 1).ToString());
                    int[] ar_Tien = { 25, 28 };
                    pck.SaveAs(new FileInfo(fullPath));

                    //Thêm báo cáo vào database
                    _baoCaoTableService.AddBaoCao(documentName, "BC_UB_PL1", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception)
            //{
            //    return string.Empty;
            //}
        }

        private int XuatBaoCao(ExcelWorksheet sheet,
            IEnumerable<BC_TienDo_UB_PL1_CayDuAn> lstData,
            IEnumerable<BC_Uyban_PL1_PDDA> dic_PDDA_CBDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_THS_CBDT,
            IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CBDT,
            IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CBDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_PD_CTDT,
            IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CTDT,
            IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CTDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_DBTD,
            IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_Truoc3110,

            IEnumerable<BC_Uyban_PL1_PDDA> lst_CBDT_DaPheDuyet,
            IEnumerable<BC_TienDo_KHV> lst_KHV,
            IEnumerable<BC_TienDo_GN> lst_GN,
            int count, int rowIndex, string szDonViTinh, List<object> lstObj, Dictionary<int, List<object>> dic)
        {
            //dic_PDDA_CBDT, dic_THS_CBDT, dic_TDC_CBDT, dic_KKGP_CBDT, dic_PD_CTDT, dic_TDC_CTDT, dic_KKGP_CTDT, dic_DA_DBTD, dic_DA_Truoc3110, lst_CBDT_DaPheDuyet, lst_KHV
            string[] szAr = { "E", "F", "J", "K", "L", "M", "N", "O", "U", "Z","AC" };
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            BaoCaoDungChung.In_Style(sheet, 8, new int[] { 28 }, 3);
            BaoCaoDungChung.In_Style(sheet, 9, new int[] { 25, 28 }, 3);
            BaoCaoDungChung.In_Style(sheet, 10, new int[] { 25, 28 }, 3);
            var dicDuanTMDT = _QLTD_CTCV_ChuanBiDauTuService.GetTMDT();
            List<int> l_tongcong = new List<int>();
            foreach (var lvnn in lstData)
            {
                sheet.Cells[rowIndex, 1].Value = Columns[count];
                sheet.Cells[rowIndex, 2].Value = lvnn.TenLVNN;
                count++;
                sheet.Row(rowIndex).Style.Font.Bold = true;
                int iLV = rowIndex;
                l_tongcong.Add(rowIndex);
                List<int> l_LV = new List<int>();
                rowIndex++;
                int stt1 = 0;

                foreach (var ttda in lvnn.grpTTDA)
                {
                    stt1++;
                    sheet.Cells[rowIndex, 1].Value = BaoCaoDungChung.ToRoman(stt1);
                    sheet.Cells[rowIndex, 2].Value = ttda.TenTTDA;
                    sheet.Cells[rowIndex, 4].Value = ttda.DiaDiem;
                    sheet.Row(rowIndex).Style.Font.Bold = true;
                    sheet.Row(rowIndex).Style.Font.Italic = true;

                    int iTT = rowIndex;
                    l_LV.Add(rowIndex);
                    rowIndex++;
                    List<int> l_TT = new List<int>();

                    int stt2 = 0;
                    foreach (var daCha in ttda.grpDACha)
                    {
                        stt2++;
                        sheet.Cells[rowIndex, 1].Value = stt2;
                        sheet.Cells[rowIndex, 2].Value = daCha.TenDACha;
                        sheet.Cells[rowIndex, 3].Value = daCha.ThoiGianThucHienCha;
                        sheet.Cells[rowIndex, 4].Value = daCha.DiaDiem;
                        sheet.Cells[rowIndex, 5].Value = 1;
                        sheet.Cells[rowIndex, 20].Value = daCha.TenChuDauTuCha;
                        sheet.Cells[rowIndex, 24].Value = daCha.TenPhongCha;

                        var lstUser = daCha.Users.ToList();
                        string szUser = "";

                        foreach (var item in lstUser)
                        {
                            string sz = item.Name;
                            szUser += sz + ", ";
                        }
                        if (szUser.IndexOf(",") > -1)
                        {
                            szUser = szUser.Substring(0, szUser.Length - 2);
                        }

                        sheet.Cells[rowIndex, 25].Value = szUser;

                        In_NoiDung(sheet,
                            dic_PDDA_CBDT,
                            dic_THS_CBDT,
                            dic_TDC_CBDT,
                            dic_KKGP_CBDT,
                            dic_PD_CTDT,
                            dic_TDC_CTDT,
                            dic_KKGP_CTDT,
                            dic_DA_DBTD,
                            dic_DA_Truoc3110,
                            lst_CBDT_DaPheDuyet,
                            lst_KHV,
                            lst_GN,
                            daCha.IDDACha,
                            rowIndex, dDim);
                        l_TT.Add(rowIndex);
                        int iDACHa = rowIndex;
                        List<int> l_DACha = new List<int>();

                        //var baoCao = _baoCaoTienDoService.GetDACbiDTu(daCha.IDDACha);
                        //if (baoCao != null)
                        //{
                        //    sheet.Cells[rowIndex, 26].Value = baoCao.TongMucDauTu;
                        //    sheet.Cells[rowIndex, 27].Value = baoCao.PDDA_BCKTKT_SoQD;
                        //    sheet.Cells[rowIndex, 28].Value = baoCao.PDDA_BCKTKT_NgayPD;
                        //    sheet.Cells[rowIndex, 29].Value = baoCao.PDDA_BCKTKT_GiaTri;
                        //}
                        if (dicDuanTMDT.ContainsKey(daCha.IDDACha))
                        {
                            var daTMDT = dicDuanTMDT[daCha.IDDACha];
                            sheet.Cells[rowIndex, 26].Value = daTMDT.TMDTDA;
                            sheet.Cells[rowIndex, 27].Value = daTMDT.SoQD;
                            sheet.Cells[rowIndex, 28].Value = daTMDT.NgayKeHoach;
                            sheet.Cells[rowIndex, 29].Value = daTMDT.TMDTPD;
                        }
                        rowIndex++;

                        foreach (var dacon in daCha.grpDACon)
                        {
                            sheet.Cells[rowIndex, 1].Value = "-";
                            sheet.Cells[rowIndex, 2].Value = dacon.TenDA;
                            sheet.Cells[rowIndex, 3].Value = dacon.ThoiGianThucHien;
                            sheet.Cells[rowIndex, 4].Value = dacon.DiaDiem;
                            sheet.Cells[rowIndex, 20].Value = dacon.TenChuDauTu;
                            sheet.Cells[rowIndex, 24].Value = dacon.TenPhong;
                            if (dicDuanTMDT.ContainsKey(dacon.IDDA))
                            {
                                var daTMDT = dicDuanTMDT[dacon.IDDA];
                                sheet.Cells[rowIndex, 26].Value = daTMDT.TMDTDA;
                                sheet.Cells[rowIndex, 27].Value = daTMDT.SoQD;
                                sheet.Cells[rowIndex, 28].Value = daTMDT.NgayKeHoach;
                                sheet.Cells[rowIndex, 29].Value = daTMDT.TMDTPD;
                            }
                            var lstUsercon = dacon.Users.ToList();
                            string szUsercon = "";

                            foreach (var item in lstUsercon)
                            {
                                string sz = item.Name;
                                szUsercon += sz + ", ";
                            }
                            if (szUser.IndexOf(",") > -1)
                            {
                                szUsercon = szUser.Substring(0, szUsercon.Length - 2);
                            }

                            sheet.Cells[rowIndex, 25].Value = szUsercon;
                            In_NoiDung(sheet,
                                dic_PDDA_CBDT,
                                dic_THS_CBDT,
                                dic_TDC_CBDT,
                                dic_KKGP_CBDT,
                                dic_PD_CTDT,
                                dic_TDC_CTDT,
                                dic_KKGP_CTDT,
                                dic_DA_DBTD,
                                dic_DA_Truoc3110,
                                lst_CBDT_DaPheDuyet,
                                lst_KHV,
                                lst_GN,
                                dacon.IDDA,
                                rowIndex, dDim);

                            l_DACha.Add(rowIndex);
                            rowIndex++;
                        }
                    }
                    SumRow(sheet, iTT, l_TT, szAr);
                    sheet.Cells[iTT, 8].Formula = "=IF(F" + iTT + "=0,0, G" + iTT + "/F" + iTT + ")";
                }
                SumRow(sheet, iLV, l_LV, szAr);
                sheet.Cells[iLV, 8].Formula = "=IF(F" + iLV + "=0,0, G" + iLV + "/F" + iLV + ")";
            }
            SumRow(sheet, 8, l_tongcong, szAr);
            sheet.Cells[8, 8].Formula = "=IF(F8=0,0,G8/F8)";

            return rowIndex;
        }
        private void XuatThemBaoCao(ExcelWorksheet sheet, object oDA, int rowDA)
        {

            try
            {
                string TMDT_ChuaPD = BaoCaoDungChung.GetValueObject(oDA, "TMDT_ChuaPD")?.ToString() ?? "";
                string PDDA_BCKTKT_SoQD = (string)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_SoQD");
                string szTMDT = BaoCaoDungChung.GetValueObject(oDA, "TMDT")?.ToString() ?? "";
                sheet.Cells[rowDA, 25].Value = !string.IsNullOrEmpty(TMDT_ChuaPD) ? Convert.ToDouble(TMDT_ChuaPD) : 0;
                BaoCaoDungChung.In_Style(sheet, rowDA, new int[] { 25 }, 3);
                sheet.Cells[rowDA, 26].Value = PDDA_BCKTKT_SoQD;
                DateTime PDDA_BCKTKT_NgayPD = (DateTime)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD");
                //sheet.Cells[rowDA, 27].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
                sheet.Cells[rowDA, 27].Value = PDDA_BCKTKT_NgayPD.ToString("dd/MM/yyyy");
                sheet.Cells[rowDA, 28].Value = !string.IsNullOrEmpty(szTMDT) ? Convert.ToDouble(szTMDT) : 0;
                BaoCaoDungChung.In_Style(sheet, rowDA, new int[] { 28 }, 3);
            }
            catch (Exception e) { Console.WriteLine(e); }
        }
        public void In_NoiDung(ExcelWorksheet sheet,
            IEnumerable<BC_Uyban_PL1_PDDA> dic_PDDA_CBDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_THS_CBDT,
            IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CBDT,
            IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CBDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_PD_CTDT,
            IEnumerable<BC_Uyban_PL1_KHTDC> dic_TDC_CTDT,
            IEnumerable<BC_Uyban_PL1_KKGP> dic_KKGP_CTDT,

            IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_DBTD,
            IEnumerable<BC_Uyban_PL1_PDDA> dic_DA_Truoc3110,

            IEnumerable<BC_Uyban_PL1_PDDA> lst_CBDT_DaPheDuyet,
            IEnumerable<BC_TienDo_KHV> lst_KHV,
            IEnumerable<BC_TienDo_GN> lst_GN,
            int IDDA, int rowIndex, int dDim)
        {
            if (dic_PD_CTDT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowIndex, 13].Value = 1;
                if (lst_CBDT_DaPheDuyet.Any(x => x.IDDA == IDDA))
                {
                    In_TDC_KKGP(sheet, rowIndex, dic_TDC_CBDT, dic_KKGP_CBDT, IDDA);
                }
                else
                {
                    In_TDC_KKGP(sheet, rowIndex, dic_TDC_CTDT, dic_KKGP_CTDT, IDDA);
                }
            }
            else
            {
                sheet.Cells[rowIndex, 12].Value = 1;

            }

            if (dic_THS_CBDT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowIndex, 12].Value = 0;
                sheet.Cells[rowIndex, 13].Value = 0;
                sheet.Cells[rowIndex, 14].Value = 1;
                In_TDC_KKGP(sheet, rowIndex, dic_TDC_CBDT, dic_KKGP_CBDT, IDDA);
            }


            if (dic_PDDA_CBDT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowIndex, 11].Value = 1;

                for (int i1 = 12; i1 <= 14; i1++)
                {
                    sheet.Cells[rowIndex, i1].Value = 0;
                }
                for (int i2 = 17; i2 <= 19; i2++)
                {
                    sheet.Cells[rowIndex, i2].Value = "";
                }
            }

            if (dic_DA_DBTD.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowIndex, 16].Value = 1;
            }

            if (dic_DA_Truoc3110.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowIndex, 22].Value = 1;
            }

            if (lst_KHV.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                var dbl = lst_KHV.Where(x => x.IDDA == IDDA).Select(x => x.dGiaTri).FirstOrDefault() == null ? 0 : lst_KHV.Where(x => x.IDDA == IDDA).Select(x => x.dGiaTri).FirstOrDefault();
                sheet.Cells[rowIndex, 6].Value = dbl / dDim;
            }

            if (lst_GN.Where(x => x.IDDA == IDDA.ToString()).Count() > 0)
            {
                string sz = lst_GN.Where(x => x.IDDA == IDDA.ToString()).Select(x => x.dGiaTriGN).FirstOrDefault();

                sheet.Cells[rowIndex, 7].Value = Convert.ToDouble(sz) / dDim;
            }

            sheet.Cells[rowIndex, 8].Formula = "=IF(F" + rowIndex + "=0,0,G" + rowIndex + "/F" + rowIndex + ")";
        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {
                foreach (var row in lstInt)
                    str += sz + row.ToString() + "+";
                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr)
        {
            foreach (var sz in szAr)
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
        }
        #endregion

        public void In_TDC_KKGP(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KHTDC> lst_TDC, IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM, int IDDA)
        {
            string sz1 = "";
            string sz2 = "";
            string sz3 = "";

            //if (loai == 1)
            //{
            //    int[] ar_i = new int[] { 16, 17, 18 };
            //    foreach (var i in ar_i)
            //    {
            //        sheet.Cells[rowDA, i].Value = "";
            //    }
            //}

            In_TDC(sheet, rowDA, lst_TDC, IDDA, sz3);
            In_KKGP(sheet, rowDA, lstKKVM, IDDA, sz1, sz2);
        }

        public void In_TDC(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KHTDC> lst_TDC, int IDDA, string sz)
        {
            sz = "";

            if (lst_TDC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz = lst_TDC.Where(x => x.IDDA == IDDA).Select(x => x.NoiDung).FirstOrDefault();
                sheet.Cells[rowDA, 17].Value = sz;
            }
        }

        public void In_KKGP(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM, int IDDA, string sz1, string sz2)
        {
            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = "";
                sz2 = "";

                var str = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.KK).FirstOrDefault();
                if (str != null)
                {
                    foreach (var item in str)
                    {
                        sz1 += item + "; ";
                    }
                    sheet.Cells[rowDA, 18].Value = sz1;
                }

                var str2 = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.GP).FirstOrDefault();
                if (str2 != null)
                {
                    foreach (var item2 in str2)
                    {
                        sz2 += item2 + "; ";
                    }
                    sheet.Cells[rowDA, 19].Value = sz2;
                }
            }
        }
    }
}
