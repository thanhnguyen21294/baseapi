﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BCTienDoDuAnThucHienDauTuController : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_KeHoachTienDoChungService _qLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private IQLTD_CTCV_ThucHienDuAnService _qLTD_CTCV_ThucHienDuAnService;

        public BCTienDoDuAnThucHienDauTuController(IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duAnService, 
            IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IQLTD_CTCV_ThucHienDuAnService qLTD_CTCV_ThucHienDuAnService,
            IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._qLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            this._qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            this._qLTD_CTCV_ThucHienDuAnService = qLTD_CTCV_ThucHienDuAnService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaoTiendoThuchienDuan")]
        [HttpGet]
        public HttpResponseMessage ExportTienDoThucHienDA(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = TienDoTHienDAn(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string TienDoTHienDAn(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TienDoDuAnThucHienDauTu.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoTienDoDuAnThucHienDauTu_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }
                        var lstPhong_CanBo = _duAnService.GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(szIDDuAn, dicUserNhanVien);

                        #region Cây dự án
                        List<object> List_TH_DA = _baoCaoTienDoService.GetList_TH_DA(szIDDuAn);
                        Dictionary<int, List<object>> dicDAConCbiDTu = _baoCaoTienDoService.GetDuAnCon(szIDDuAn);
                        #endregion

                        #region Chuẩn bị thực hiện
                        //Chuẩn bị thực hiện
                        //Dictionary<int, List<object>> dicPheDuToanCBTHDT = _baoCaoTienDoService.GetdicPheDuToanCBTHDT(szIDDuAn);
                        //Dictionary<int, List<object>> dicPheKHLCNT = _baoCaoTienDoService.GetdicPheKHLCNT(szIDDuAn);
                        //Dictionary<int, List<object>> dic_HTPD_THBV = _baoCaoTienDoService.Getdic_HTPD_THBV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_PDT = _baoCaoTienDoService.Getdic_CBTH_PDT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_PKHLCNT = _baoCaoTienDoService.Getdic_CBTH_PKHLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_HTPD_THBV = _baoCaoTienDoService.Getdic_CBTH_HTPD_THBV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_TTD_TKBV = _baoCaoTienDoService.Getdic_CBTH_TTD_TKBV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_DLHS = _baoCaoTienDoService.Getdic_CBTH_DLHS(szIDDuAn);
                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBTH = _qLTD_KeHoachTienDoChungService.Get_KHTDC_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBTH = _qLTD_KhoKhanVuongMacService.Get_KK_GP_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);

                        //Dictionary<int, string> dic_CBTH_KK = _baoCaoTienDoService.Getdic_CBTH_KK(szIDDuAn);
                        //Dictionary<int, string> dic_CBTH_GP = _baoCaoTienDoService.Getdic_CBTH_GP(szIDDuAn);
                        //Dictionary<int, List<object>> dic_CBTH_TDC = _baoCaoTienDoService.Getdic_CBTH_TDC(szIDDuAn);
                        //dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC
                        #endregion

                        #region Thực hiện
                        Dictionary<int, List<object>> dic_THDT_HTTC = _baoCaoTienDoService.Getdic_THDT_HTTC(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_DTCXD = _baoCaoTienDoService.Getdic_THDT_DTCXD(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_DLCNT = _baoCaoTienDoService.Getdic_THDT_DLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_CTHLCNT = _baoCaoTienDoService.Getdic_THDT_CTHLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_GPMB = _baoCaoTienDoService.Getdic_THDT_GPMB(szIDDuAn);
                        IEnumerable<BC_Uyban_PL1_PDDA> lstDanhSo_CTHLCNT = _qLTD_CTCV_ThucHienDuAnService.Get_DanhSo_For_CV_CTHLCNT_For_BC_TienDo_ThucHien(szIDDuAn);
                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_TH = _qLTD_KeHoachTienDoChungService.Get_KHTDC_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_TH = _qLTD_KhoKhanVuongMacService.Get_KK_GP_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        var lstDTKTC = _qLTD_CTCV_ThucHienDuAnService.Get_DangTrienKhaiThiCong_For_BC_Uyban_PL2(szIDDuAn);
                        var lstDLCNT = _qLTD_CTCV_ThucHienDuAnService.Get_DangLuaChonNhaThau_For_BC_Uyban_PL2(szIDDuAn);
                        var lstHTTC = _qLTD_CTCV_ThucHienDuAnService.Get_DaHoanThanhThiCong_For_BC_Uyban_PL2(szIDDuAn);
                        
                        //Dictionary<int, string> dic_TH_KK = _baoCaoTienDoService.Getdic_TH_KK(szIDDuAn);
                        //Dictionary<int, string> dic_TH_GP = _baoCaoTienDoService.Getdic_TH_GP(szIDDuAn);
                        //Dictionary<int, List<object>> dic_TH_TDC = _baoCaoTienDoService.Getdic_TH_TDC(szIDDuAn);
                        //dic_TH_KK,dic_TH_GP,dic_TH_TDC
                        //dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB


                        #endregion

                        #region Kế hoạch vốn
                        #region Kế hoạch vốn
                        Dictionary<int, double> Dic_TD_DA_CBDT_KHV = _baoCaoTienDoService.GetDic_TD_DA_CBDT_KHV(szIDDuAn, iNamPD);
                        #endregion
                        #region Giải ngân
                        Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN = _vonCapNhatKLNTService.GetGiaiNganVon(szIDDuAn);
                        //Dictionary<int, double> Dic_TD_DA_CBDT_GN = _baoCaoTienDoService.GetDic_TD_DA_CBDT_GN(szIDDuAn, iNamPD);
                        #endregion
                        #region Kế hoạch vốn điều chỉnh
                        Dictionary<int, double> dic_DA_KHV_DXDC = _baoCaoTienDoService.Getdic_DA_KHV_DXDC(szIDDuAn, iNamPD);
                        #endregion
                        #endregion

                        #region Tiến độ thực hiện
                        #region Chuẩn bị thực hiện
                        Dictionary<int, string> dic_TDTH_CBTH = _baoCaoTienDoService.GetDic_TDTH_CBTH(szIDDuAn);
                        #endregion
                        #region Thực hiện dự án - Đang thi công xây dựng
                        Dictionary<int, string> dic_TDTH_TH_DTCXD = _baoCaoTienDoService.GetDic_TDTH_TH_DTCXD(szIDDuAn);
                        #endregion
                        #region Thực hiện dự án - Giải phóng mặt bằng
                        Dictionary<int, string> dic_TDTH_TH_GPMB = _baoCaoTienDoService.GetDic_TDTH_TH_GPMB(szIDDuAn);
                        #endregion
                        #endregion


                        int iColumn = 0;
                        int iSoLaMa = 0;
                        int iSTTcap1 = 1;
                        int rowCDT = 11;

                        List<int> l_GDLs = new List<int>();

                        string[] az = { "C", "D", "E", "F", "M", "O", "P", "R", "S", "Y", "AB", "AD", "AI", "AL", "AM", "AY", "BB", "BC", "BF", "BG", "BH", "BK", "BN" };

                        int[] ar_So = new int[] { 1, 3, 4, 5, 6, 12, 14, 16, 17, 19, 20, 25, 28, 29, 30, 31, 32, 35, 39, 40, 42, 50, 62, 63, 66 };
                        int[] ar_Text = new int[] { 2, 7, 8, 9, 10, 11, 21, 22, 23, 24, 26, 27, 33, 34, 36, 45, 46, 47, 48, 49, 50, 52, 53, 57, 64, 65 };
                        int[] ar_Tien = new int[] { 13, 15, 18, 38, 54, 55, 58, 59, 60, 62, 63, 66 };
                        int[] ar_PhanTram = new int[] { 56 };
                        int[] ar_KHV = new int[] { 54, 58, 59, 60 };
                        int iTongCong = rowCDT - 1;
                        Format(sheet, 10, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                        List<int> listInt_TongCong = new List<int>();


                        iSTTcap1 = 1;

                        if (List_TH_DA.Count > 0)
                        {
                            foreach (object oTTDATD in List_TH_DA)
                            {
                                listInt_TongCong.Add(rowCDT);
                                int m = rowCDT;
                                string GiaiDoanDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDATD, "GiaiDoanDuAn");
                                sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                                sheet.Cells[rowCDT, 2].Value = GiaiDoanDuAn + " năm " + namPD;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                rowCDT++;
                                iSoLaMa = 1;

                                List<object> lLVNN = (List<object>)BaoCaoDungChung.GetValueObject(oTTDATD, "grpLVNN");
                                List<int> listInt_LVNN = new List<int>();


                                foreach (object oLVNN in lLVNN)
                                {
                                    listInt_LVNN.Add(rowCDT);
                                    int iLV = rowCDT;

                                    string TenLinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oLVNN, "TenLinhVucNganhNghe");

                                    sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                                    sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;
                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                    sheet.Row(rowCDT).Style.Font.Bold = true;
                                    rowCDT++;
                                    List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oLVNN, "grpTTDA");
                                    List<int> l_TTDAs = new List<int>();
                                    foreach (object oTTDA in lTTDA)
                                    {
                                        int iTT = rowCDT;
                                        l_TTDAs.Add(rowCDT);
                                        string TenTinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");
                                        sheet.Row(rowCDT).Style.Font.Italic = true;
                                        sheet.Row(rowCDT).Style.Font.Bold = true;

                                        sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1;
                                        sheet.Cells[rowCDT, 2].Value = "Dự án " + TenTinhTrangDuAn;
                                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                        rowCDT++;
                                        List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");
                                        List<int> l_DAChas = new List<int>();

                                        int d = 0;

                                        foreach (object oDACha in lDACha)
                                        {
                                            d++;
                                            XuatDuAnXDCB(sheet, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBo,
                                                        dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDanhSo_CTHLCNT, lstDTKTC, lstDLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                        dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);
                                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                            l_DAChas.Add(rowCDT);
                                            rowCDT++;
                                            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");
                                            if (dicDAConCbiDTu.ContainsKey(IDDA))
                                            {
                                                List<object> lDuAnCon = dicDAConCbiDTu[IDDA];

                                                foreach (object oDuAnCon in lDuAnCon)
                                                {
                                                    XuatDuAnXDCB(sheet, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBo,
                                                        dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDanhSo_CTHLCNT, lstDTKTC, lstDLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                        dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);
                                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);

                                                    rowCDT++;
                                                }
                                            }
                                        }
                                        SumRow(sheet, iTT, l_DAChas, az);
                                        Ty_Le_Giai_Ngan(sheet, iTT);
                                        iSTTcap1++;
                                    }
                                    SumRow(sheet, iLV, l_TTDAs, az);
                                    Ty_Le_Giai_Ngan(sheet, iLV);
                                    iSTTcap1 = 1;
                                    iSoLaMa++;
                                }
                                SumRow(sheet, m, listInt_LVNN, az);
                                Ty_Le_Giai_Ngan(sheet, m);
                                iSoLaMa = 0;
                                iColumn++;
                            }

                            SumRow(sheet, iTongCong, listInt_TongCong, az);
                            Ty_Le_Giai_Ngan(sheet, iTongCong);
                            Format(sheet, iTongCong, ar_So, ar_Text, ar_Tien, ar_PhanTram);

                            //UpdateTongXDCB(sheet, 9, l_CDTs);

                            int rowIndex = rowCDT - 1;
                            //using (ExcelRange rng = sheet.Cells["A9:BN" + rowIndex])
                            using (ExcelRange rng = sheet.Cells["A9:EA" + rowIndex])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.WrapText = true;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                                rng.AutoFitColumns();
                            }
                            Style(sheet, rowIndex, "A10:AN");
                            Style(sheet, rowIndex, "AP10:BJ");
                        }
                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["B10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        az = new string[] { "A2", "A3", "Y7", "AY7", "BB8", "BF8", "BG8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{nam}}", namPD);
                        }

                        az = new string[] { "BE5", "M8", "O8", "R8", "AL8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                        }

                        sheet.Column(41).Hidden = true;

                        int[] ar_i = new int[] { 7, 9, 10, 11 };
                        DoRongCot(sheet, ar_i, 20);

                        ar_i = new int[] { 12, 13, 14, 15, 17, 18, 20, 21, 22, 23, 25, 26, 28, 30, 31, 32, 33, 35, 36, 37, 38, 39, 42, 43, 44, 45, 46, 47, 48, 50, 51, 52, 53, 54, 55, 56 };
                        DoRongCot(sheet, ar_i, 15);

                        ar_i = new int[] { 1, 3, 4, 5, 6, 16, 19, 24, 27, 29, 34, 38, 49 };
                        DoRongCot(sheet, ar_i, 5);
                        sheet.Column(1).Width = 7;
                        sheet.Column(2).Width = 25;
                        sheet.Column(8).Width = 40;

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BAOCAOTIENDOTHDT", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #region Style
        public void Style(ExcelWorksheet sheet, int row, string sz)
        {
            using (ExcelRange rng = sheet.Cells[sz + row])
            {
                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rng.Style.WrapText = true;
                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rng.Style.Font.Name = "Times New Roman";
                rng.Style.Font.Size = 11;
                rng.AutoFitColumns();
            }
        }

        public void Ty_Le_Giai_Ngan(ExcelWorksheet sheet, int row)
        {
            sheet.Cells[row, 56].Formula = "=(BC" + row + "/BB" + row + ")";
            int[] ar_i = new int[] { 58, 59 };
            string[] arSz = new string[] { "BB", "BH" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, row, ar_i, arSz);
        }
        public void Format(ExcelWorksheet sheet, int row, int[] ar_So, int[] ar_Text, int[] ar_Tien, int[] ar_PhanTram)
        {
            BaoCaoDungChung.In_Style(sheet, row, ar_So, 1);
            BaoCaoDungChung.In_Style(sheet, row, ar_Text, 2);
            BaoCaoDungChung.In_Style(sheet, row, ar_Tien, 3);
            BaoCaoDungChung.In_Style(sheet, row, ar_PhanTram, 4);
        }

        public void DoRongCot(ExcelWorksheet sheet, int[] ar_So, int dorong)
        {
            foreach (var item in ar_So)
            {
                sheet.Column(item).Width = dorong;
            }
        }
        #endregion


        private void XuatDuAnXDCB(ExcelWorksheet sheet, string szDonViTinh, object oDA, string d, int rowDA,
            IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> lstPhong_CanBo,
            //,dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS
            Dictionary<int, List<object>> dic_CBTH_PDT,
            Dictionary<int, List<object>> dic_CBTH_PKHLCNT,
            Dictionary<int, List<object>> dic_CBTH_HTPD_THBV,
            Dictionary<int, List<object>> dic_CBTH_TTD_TKBV,
            Dictionary<int, List<object>> dic_CBTH_DLHS,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBTH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBTH,
            //Dictionary<int, string> dic_CBTH_KK,
            //Dictionary<int, string> dic_CBTH_GP,
            //Dictionary<int, List<object>> dic_CBTH_TDC,


            //dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB
            Dictionary<int, List<object>> dic_THDT_HTTC,
            Dictionary<int, List<object>> dic_THDT_DTCXD,
            Dictionary<int, List<object>> dic_THDT_DLCNT,
            Dictionary<int, List<object>> dic_THDT_CTHLCNT,
            Dictionary<int, List<object>> dic_THDT_GPMB,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_TH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_TH,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDanhSo_CTHLCNT,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDTKTC,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDLCNT,
            IEnumerable<BC_Uyban_PL1_PDDA> lstHTTC,
            //Dictionary<int, string> dic_TH_KK,
            //Dictionary<int, string> dic_TH_GP,
            //Dictionary<int, List<object>> dic_TH_TDC, lstDTKTC, lstDLCNT,

            //dic_KHV_BD, dic_KHV_SDC, dic_KHV_TTCPK, dic_KHV_TTHD
            Dictionary<int, double> Dic_TD_DA_CBDT_KHV,
            Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN,
            //Dictionary<int, double> Dic_TD_DA_CBDT_GN,
            Dictionary<int, double> dic_DA_KHV_DXDC,

            //dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB
            Dictionary<int, string> dic_TDTH_CBTH,
            Dictionary<int, string> dic_TDTH_TH_DTCXD,
            Dictionary<int, string> dic_TDTH_TH_GPMB
            )
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            int dDimKLNT = BaoCaoDungChung.GetdDimKLNT(szDonViTinh);

            double dGiaTri = 0;
            string sz1 = "";
            string sz2 = "";
            string sz3 = "";
            int[] ar_i;
            string[] ar_Sz1;
            string[] ar_Sz2;
            List<object> lst = new List<object>();
            string[] szAr = { "str1", "str2", "str3" };

            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "TenDuAn");
            string NhomDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "NhomDuAn");
            string DiaDiemXayDung = (string)BaoCaoDungChung.GetValueObject(oDA, "DiaDiemXayDung");
            string NangLucThietKe = (string)BaoCaoDungChung.GetValueObject(oDA, "NangLucThietKe");
            string ThoiGianThucHien = (string)BaoCaoDungChung.GetValueObject(oDA, "ThoiGianThucHien");
            string MaSoDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "MaSoDuAn");
            string TenChuDauTu = (string)BaoCaoDungChung.GetValueObject(oDA, "TenChuDauTu");

            sheet.Cells[rowDA, 1].Value = d;
            sheet.Cells[rowDA, 2].Value = TenDuAn;

            ar_i = new int[] { 4, 5, 6 };
            BaoCaoDungChung.In_Nhom_Du_An(sheet, rowDA, NhomDuAn, ar_i);

            sheet.Cells[rowDA, 3].Formula = "Sum(" + sheet.Cells[rowDA, 4] + ":" + sheet.Cells[rowDA, 6] + ")";

            ar_i = new int[] { 7, 8, 9, 10, 11 };
            ar_Sz1 = new string[] { DiaDiemXayDung , NangLucThietKe , ThoiGianThucHien , MaSoDuAn , TenChuDauTu };
            BaoCaoDungChung.In_Chuoi_Thong_Tin_Du_An(sheet, rowDA, ar_i, ar_Sz1);

            #region Chuẩn bị thực hiện
            ar_i = new int[] { 0, 12, 13 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_PDT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 0, 14, 15 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_PKHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 19, 20 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBTH_TTD_TKBV, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 16, 17, 18 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_HTPD_THBV, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            if(Convert.ToInt32(sheet.Cells[rowDA, 16].Value) == 1)
            {
                sheet.Cells[rowDA, 19].Value = 0;
            }

            ar_i = new int[] { 22, 23 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str1", "str2" };
            BaoCaoDungChung.In_Chuoi(dic_CBTH_DLHS, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CBTH, IDDA, sz1, 24);
            ar_i = new int[] { 25, 26, 27 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CBTH, IDDA, sz1, sz2, ar_i);
            #endregion

            #region Thực hiện
            ar_i = new int[] { 0, 29 };
            BaoCaoDungChung.In_Date_Value(dic_THDT_HTTC, IDDA, lst, sheet, rowDA, ar_i, sz1, "str");

            if(lstDTKTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 30].Value = 1;
            }

            ar_i = new int[] { 0, 31 };
            BaoCaoDungChung.In_Date_Value(dic_THDT_DTCXD, IDDA, lst, sheet, rowDA, ar_i, sz1, "str1");

            ar_i = new int[] { 32, 34 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_DTCXD, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            if(lstDLCNT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 35].Value = 1;
            }

            ar_i = new int[] { 0, 37 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_THDT_DLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 0, 38 };
            BaoCaoDungChung.In_GiaTri_Value(dic_THDT_DLCNT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str3", dDim);

            if(lstDanhSo_CTHLCNT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 39].Value = 1;
            }

            ar_i = new int[] { 0, 40 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 42 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //BaoCaoDungChung.In_Date_Value(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, "str3");

            ar_i = new int[] { 43, 44, 45 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str4", "str5", "str6" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 47, 48, 49 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_GPMB, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            if (lstHTTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 28].Value = 1;
                ar_i = new int[] { 30, 35, 39 };
                foreach(var item in ar_i)
                {
                    sheet.Cells[rowDA, item].Value = 0;
                }
            }
            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_TH, IDDA, sz1, 50);
            ar_i = new int[] { 51, 52, 53 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_TH, IDDA, sz1, sz2, ar_i);
            #endregion

            #region Kế hoạch vốn
            double dGiaTri1;
            double dGiaTri2;
            if (Dic_TD_DA_CBDT_KHV.ContainsKey(IDDA))
            {
                dGiaTri = Dic_TD_DA_CBDT_KHV[IDDA] / dDim;
                sheet.Cells[rowDA, 54].Value = dGiaTri;
            }
            else
            {
                sheet.Cells[rowDA, 54].Value = 0;
            }
            dGiaTri1 = dGiaTri;

            if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(item, "str2");

                    sheet.Cells[rowDA, 55].Value = (Convert.ToDouble(sz1) + Convert.ToDouble(sz2))/dDimKLNT;
                }
            }

            //if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            //{
            //    dGiaTri2 = Dic_TD_DA_CBDT_GN[IDDA] / dDim;
            //    sheet.Cells[rowDA, 55].Value = dGiaTri2;
            //}
            //else
            //{
            //    sheet.Cells[rowDA, 55].Value = 0;
            //}
            sheet.Cells[rowDA, 56].Formula = "=(BC" + rowDA + "/BB" + rowDA + ")";

            if (dic_DA_KHV_DXDC.ContainsKey(IDDA))
            {
                dGiaTri = dic_DA_KHV_DXDC[IDDA] / dDim;
                sheet.Cells[rowDA, 60].Value = dGiaTri;
            }
            else
            {
                dGiaTri = 0;
            }
            dGiaTri2 = dGiaTri;

            if (lstPhong_CanBo.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.TenPhong).FirstOrDefault();
                sheet.Cells[rowDA, 61].Value = sz1;

                sz2 = "";
                var lstUser = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.Users).FirstOrDefault();
                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    sz2 += sz + ",";
                }
                if (sz2.Length > 1)
                {
                    sz2 = sz2.Substring(0, sz2.Length - 1);
                }
                sheet.Cells[rowDA, 62].Value = sz2;
            }

            ar_i = new int[] { 58, 59 };
            ar_Sz1 = new string[] { "BB", "BH" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, rowDA, ar_i, ar_Sz1);
            #endregion

            string TMDT_ChuaPD = BaoCaoDungChung.GetValueObject(oDA, "TMDT_ChuaPD")?.ToString() ?? "";
            string PDDA_BCKTKT_SoQD = (string)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_SoQD");
            
            string szTMDT = BaoCaoDungChung.GetValueObject(oDA, "TMDT")?.ToString() ?? "";
            sheet.Cells[rowDA, 63].Value = !string.IsNullOrEmpty(TMDT_ChuaPD) ? Convert.ToDouble(TMDT_ChuaPD) : 0;
            sheet.Cells[rowDA, 64].Value = PDDA_BCKTKT_SoQD;
            try
            {
                if (BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD") != null)
                {
                    DateTime PDDA_BCKTKT_NgayPD = (DateTime)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD");
                    sheet.Cells[rowDA, 65].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
                }
            }
            catch (Exception e) { }
            //sheet.Cells[rowDA, 65].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
            sheet.Cells[rowDA, 66].Value = !string.IsNullOrEmpty(szTMDT) ? Convert.ToDouble(szTMDT) : 0;

            #region Tiến độ thực hiện
            //dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB
            if (dic_TDTH_CBTH.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 21].Value = dic_TDTH_CBTH[IDDA];
            }
            if (dic_TDTH_TH_DTCXD.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 33].Value = dic_TDTH_TH_DTCXD[IDDA];
            }
            if (dic_TDTH_TH_GPMB.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 46].Value = dic_TDTH_TH_GPMB[IDDA];
            }
            #endregion

            ar_i = new int[] { 1, 3, 4, 5, 6, 12, 14, 16, 17, 19, 20, 25, 28, 29, 30, 31, 32, 35, 39, 40, 42, 50 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 1);

            ar_i = new int[] { 2, 7, 8, 9, 10, 11, 21, 22, 23, 24, 26, 27, 33, 34, 36, 45, 46, 47, 48, 49, 50, 52, 53, 57 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 2);

            ar_i = new int[] { 13, 15, 18, 38, 54, 55, 58, 59, 60 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 3);

            ar_i = new int[] { 56 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 4);

            for (int i = 4; i <= 6; i++)
            {
                sheet.Cells[rowDA, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {

                foreach (var row in lstInt)
                {
                    str += sz + row.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr)
        {
            foreach (var sz in szAr)
            {
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
            }
        }
        #endregion

    }
}
