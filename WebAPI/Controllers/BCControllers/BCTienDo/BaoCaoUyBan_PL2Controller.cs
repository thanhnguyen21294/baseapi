﻿using Common;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCTienDo
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BaoCaoUyBan_PL2Controller : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;
        private IQLTD_KeHoachTienDoChungService _QLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _QLTD_KhoKhanVuongMacService;
        private IQLTD_CTCV_ChuanBiDauTuService _QLTD_CTCV_ChuanBiDauTuService;


        public BaoCaoUyBan_PL2Controller(IErrorService errorService,
            IVonCapNhatKLNTService vonCapNhatKLNTService,
            IDuAnService duAnService,
            IBaoCaoTienDoService baoCaoTienDoService,
            IQLTD_CTCV_ThucHienDuAnService QLTD_CTCV_ThucHienDuAnService,
            IQLTD_KeHoachTienDoChungService QLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService QLTD_KhoKhanVuongMacService,
            IQLTD_CTCV_ChuanBiDauTuService QLTD_CTCV_ChuanBiDauTuService,
            IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._QLTD_CTCV_ThucHienDuAnService = QLTD_CTCV_ThucHienDuAnService;
            this._QLTD_KeHoachTienDoChungService = QLTD_KeHoachTienDoChungService;
            this._QLTD_KhoKhanVuongMacService = QLTD_KhoKhanVuongMacService;
            this._baoCaoTableService = baoCaoTableService;
            _QLTD_CTCV_ChuanBiDauTuService = QLTD_CTCV_ChuanBiDauTuService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };

        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaoUybanPL2")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoUyBan_PL2(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh, int isTMDT)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCaoUyBan_PL2(iIDDuAn, iNamPD, szDonViTinh, isTMDT);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoUyBan_PL2(string szIDDuAn, int iNamPD, string szDonViTinh, int isTMDT)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = "";
            if(isTMDT == 0)
                templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BC_UB_PL2.xlsx");
            else
            {
                templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BC_UB_PL2_TMDT.xlsx");
            }
            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoUyBan_PL2_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);
            var dicDuanTMDT = _QLTD_CTCV_ChuanBiDauTuService.GetTMDT();
            //Read Template
            //try
            //{
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                    //int iNam_1 = iNamPD - 1;

                    #region Cây dự án
                    List<object> lstTD_DA_CBDT_CDA = _baoCaoTienDoService.GetListTD_DA_TH_CDA(szIDDuAn);
                    Dictionary<int, List<object>> dicDACon = _baoCaoTienDoService.GetdicTD_DA_DAC(szIDDuAn);
                    #endregion

                    Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                    var users = AppUserManager.Users;
                    foreach (AppUser user in users)
                    {
                        var roleTemp = AppUserManager.GetRoles(user.Id);
                        if (roleTemp.Contains("Nhân viên"))
                        {
                            dicUserNhanVien.Add(user.Id, user.FullName);
                        }
                    }
                    var lstPhong_CanBo = _duAnService.GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(szIDDuAn, dicUserNhanVien);

                    #region Kế hoạch vốn
                    //Dictionary<int, double> Dic_TD_DA_CBDT_KHV = _baoCaoTienDoService.GetDic_TD_DA_CBDT_KHV(szIDDuAn, iNamPD);
                    #endregion

                    #region Giải ngân
                    Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN = _vonCapNhatKLNTService.GetGiaiNganVon(szIDDuAn);
                    //Dictionary<int, double> Dic_TD_DA_CBDT_GN = _baoCaoTienDoService.GetDic_TD_DA_CBDT_GN(szIDDuAn, iNamPD);

                    #endregion

                    #region Số dự án được duyệt
                    Dictionary<int, List<object>> Dic_CBDT_HTPDDA = _baoCaoTienDoService.Getdic_CBDT_HTPDDA(szIDDuAn);
                    #endregion

                    #region TKKT_TDT
                    Dictionary<int, List<object>> Dic_CBTH_HTPD_THBV = _baoCaoTienDoService.Getdic_CBTH_HTPD_THBV(szIDDuAn);
                    #endregion

                    #region Tiến độ thực hiện
                    #region Giải phóng mặt bằng
                    var lstGPMB = _QLTD_CTCV_ThucHienDuAnService.Get_DangThucHienGPMB_For_BC_Uyban_PL2(szIDDuAn);
                    #endregion
                    #region Đang lựa chọn nhà thầu
                    var lstDLCNT = _QLTD_CTCV_ThucHienDuAnService.Get_DangLuaChonNhaThau_For_BC_Uyban_PL2(szIDDuAn);
                    #endregion
                    #region Đang triển khai thi công
                    var lstDTKTC = _QLTD_CTCV_ThucHienDuAnService.Get_DangTrienKhaiThiCong_For_BC_Uyban_PL2(szIDDuAn);
                    #endregion
                    #region Hoàn thành thi công
                    var lstHTTC = _QLTD_CTCV_ThucHienDuAnService.Get_DaHoanThanhThiCong_For_BC_Uyban_PL2(szIDDuAn);
                    #endregion
                    #endregion

                    #region Khối lượng thực hiện
                    Dictionary<int, List<object>> Dic_KLTH = _vonCapNhatKLNTService.GetKLTH(szIDDuAn);
                    //Dictionary<int, double> Dic_KLTH = _baoCaoTienDoService.GetDic_KLTH(szIDDuAn, iNamPD);
                    #endregion

                    #region Tiến độ chung
                    var lstTDC_CBTH = _QLTD_KeHoachTienDoChungService.Get_KHTDC_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    var lstTDC_TH = _QLTD_KeHoachTienDoChungService.Get_KHTDC_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    #endregion

                    #region Khó khăn, vướng mắc
                    var lstKKVM_CBTH = _QLTD_KhoKhanVuongMacService.Get_KK_GP_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    var lstKKVM_TH = _QLTD_KhoKhanVuongMacService.Get_KK_GP_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 1);
                    #endregion

                    int iColumn = 0;
                    int rowCDT = 10;
                    int rowEnd = 0;
                    string[] az = new string[] { "C", "L", "N", "O", "P", "Q", "R", "S", "W", "X" };

                    

                    int[] ar_So = new int[] { 1, 3, 9, 11, 13, 16, 17, 18, 19 };

                    int[] ar_Tien = new int[] { 12, 14, 15, 23, 24 };

                    if (isTMDT == 1)
                    {
                        az = new string[] { "C", "L", "N", "O", "P", "Q", "R", "S", "W", "X", "AD", "AG" };
                        ar_Tien = new int[] { 12, 14, 15, 23, 24,30,33 };
                    }
                    sheet.Cells["B8"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    Format(sheet, 9, ar_So, ar_Tien);


                    List<int> l_LVNNs = new List<int>();
                    List<int> l_GDDAs = new List<int>();

                    int a = rowCDT - 1;

                    if (lstTD_DA_CBDT_CDA.Count != 0)
                    {
                        foreach (object oTD_DA_CBDT_CDA in lstTD_DA_CBDT_CDA)
                        {
                            Format(sheet, rowCDT, ar_So, ar_Tien);
                            string LinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oTD_DA_CBDT_CDA, "LinhVucNganhNghe");
                            sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                            sheet.Cells[rowCDT, 2].Value = LinhVucNganhNghe;
                            l_LVNNs.Add(rowCDT);
                            int b = rowCDT;
                            sheet.Row(rowCDT).Style.Font.Bold = true;
                            rowCDT++;
                            List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oTD_DA_CBDT_CDA, "grpTTDA");
                            int c = 0;
                            List<int> l_TTDAs = new List<int>();
                            int iSoLaMa = 1;

                            foreach (object oTTDA in lTTDA)
                            {
                                Format(sheet, rowCDT, ar_So, ar_Tien);
                                string TinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");

                                sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                                sheet.Cells[rowCDT, 2].Value = "Dự án " + TinhTrangDuAn;
                                l_TTDAs.Add(rowCDT);
                                c++;
                                int i = rowCDT;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                sheet.Row(rowCDT).Style.Font.Italic = true;
                                rowCDT++;

                                List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");
                                List<int> l_GDLs = new List<int>();
                                int d = 0;

                                foreach (object oDACha in lDACha)
                                {
                                    d++;
                                    Format(sheet, rowCDT, ar_So, ar_Tien);
                                    XuatDuAnXDCB(sheet, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBo,
                                        Dic_CBDT_HTPDDA,
                                               Dic_CBTH_HTPD_THBV,
                                               Dic_KLTH,
                                               Dic_TD_DA_CBDT_GN,
                                               lstGPMB, lstDLCNT, lstDTKTC, lstHTTC,
                                               lstTDC_CBTH, lstTDC_TH,
                                               lstKKVM_CBTH, lstKKVM_TH, dicDuanTMDT, isTMDT
                                        );
                                    l_GDLs.Add(rowCDT);
                                    rowCDT++;

                                    int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");

                                    if (dicDACon.ContainsKey(IDDA))
                                    {

                                        List<object> lDuAnCon = dicDACon[IDDA];

                                        foreach (object oDuAnCon in lDuAnCon)
                                        {
                                            Format(sheet, rowCDT, ar_So, ar_Tien);
                                            XuatDuAnXDCB(sheet, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBo,
                                               Dic_CBDT_HTPDDA,
                                               Dic_CBTH_HTPD_THBV,
                                               Dic_KLTH,
                                               Dic_TD_DA_CBDT_GN,
                                                lstGPMB, lstDLCNT, lstDTKTC, lstHTTC,
                                                lstTDC_CBTH, lstTDC_TH,
                                                lstKKVM_CBTH, lstKKVM_TH, dicDuanTMDT, isTMDT
                                                );
                                            rowCDT++;
                                        }
                                    }

                                }
                                SumRow(sheet, i, l_GDLs, az, isTMDT);
                                iSoLaMa++;
                            }
                            SumRow(sheet, b, l_TTDAs, az, isTMDT);
                            iSoLaMa = 0;
                            iColumn++;
                        }
                        SumRow(sheet, a, l_LVNNs, az, isTMDT);

                        int rowIndex = rowCDT - 1;

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        for (int i = 4; i <= 8; i++)
                        {
                            sheet.Column(i).Hidden = true;
                        }

                        Style(sheet, rowIndex, "A10:C");
                        if(isTMDT == 0)
                            Style(sheet, rowIndex, "I10:AC");
                        if (isTMDT == 1)
                            Style(sheet, rowIndex, "I10:AG");
                    }

                    int[] ar_i = new int[] { 2, 10, 20, 21, 22, 25 };
                    if (isTMDT == 1)
                    {
                        ar_i = new int[] { 2, 10, 20, 21, 22, 25 };
                    }
                    DoRongCot(sheet, ar_i, 25);
                    ar_i = new int[] { 9, 11, 12, 13, 14, 15, 23, 24 };
                    DoRongCot(sheet, ar_i, 15);

                    ar_i = new int[] { 1, 3 };
                    DoRongCot(sheet, ar_i, 5);

                    az = new string[] { "A2", "O5", "W5", "W6", "X6" };

                    foreach (var item in az)
                    {
                        sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{nam}}", namPD);
                    }

                    az = new string[] { "Y4" };
                    foreach (var item in az)
                    {
                        sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                    }

                    pck.SaveAs(new FileInfo(fullPath));

                    //Thêm báo cáo vào database
                    _baoCaoTableService.AddBaoCao(documentName, "BC_UB_PL2", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception)
            //{
            //    return string.Empty;
            //}
        }

        public void Style(ExcelWorksheet sheet, int row, string sz)
        {
            using (ExcelRange rng = sheet.Cells[sz + row])
            {
                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rng.Style.WrapText = true;
                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rng.Style.Font.Name = "Times New Roman";
                rng.Style.Font.Size = 11;
                rng.AutoFitColumns();
            }
        }

        public void DoRongCot(ExcelWorksheet sheet, int[] ar, int i)
        {
            foreach (var item in ar)
            {
                sheet.Column(item).Width = i;
            }
        }
        public void Format(ExcelWorksheet sheet, int row, int[] ar_So, int[] ar_Tien)
        {
            BaoCaoDungChung.In_Style(sheet, row, ar_So, 1);
            BaoCaoDungChung.In_Style(sheet, row, ar_Tien, 3);
        }

        private void XuatDuAnXDCB(ExcelWorksheet sheet, string szDonViTinh, object oDA, string d, int rowDA,
            IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> lstPhong_CanBo,
            Dictionary<int, List<object>> Dic_CBDT_HTPDDA,
            Dictionary<int, List<object>> Dic_CBTH_HTPD_THBV,
            Dictionary<int, List<object>> Dic_KLTH,
            Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN,
            IEnumerable<BC_Uyban_PL1_PDDA> lstGPMB,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDLCNT,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDTKTC,
            IEnumerable<BC_Uyban_PL1_PDDA> lstHTTC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBTH,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_TH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM_CBTH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM_TH,
            Dictionary<int, DuAnTMDT> dicDuanTMDT,
            int isTMDT
            )
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            int dDimKLNT = BaoCaoDungChung.GetdDimKLNT(szDonViTinh);

            double dGiaTri = 0;
            string sz1 = "";
            string sz2 = "";
            string sz3 = "";
            string sz4 = "";
            int[] ar_i;
            string[] ar_Sz1;
            List<object> lst = new List<object>();
            string[] szAr = { "str1", "str2", "str3" };

            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDA, "IDDA");
            sz1 = (string)BaoCaoDungChung.GetValueObject(oDA, "TenDuAn");
            sz2 = (string)BaoCaoDungChung.GetValueObject(oDA, "ThoiGianThucHien");
            sz3 = (string)BaoCaoDungChung.GetValueObject(oDA, "NangLucThietKe");
            sz4 = (string)BaoCaoDungChung.GetValueObject(oDA, "TenChuDauTu");

            sheet.Cells[rowDA, 1].Value = d;
            sheet.Cells[rowDA, 2].Value = sz1;
            if (sz1 != "")
            {
                sheet.Cells[rowDA, 3].Value = 1;
            }

            ar_i = new int[] { 9, 10, 25 };
            ar_Sz1 = new string[] { sz2, sz3, sz4 };
            BaoCaoDungChung.In_Chuoi_Thong_Tin_Du_An(sheet, rowDA, ar_i, ar_Sz1);
            //sheet.Cells[rowDA, 9].Value = sz2;
            //sheet.Cells[rowDA, 10].Value = sz3;
            //sheet.Cells[rowDA, 25].Value = sz4;

            //Dic_CBDT_HTPDDA
            ar_i = new int[] { 0, 11, 12 };
            szAr = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_SoNgayPD_TGT(Dic_CBDT_HTPDDA, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            //Dic_CBTH_HTPD_THBV
            ar_i = new int[] { 0, 13, 14 };
            szAr = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_SoNgayPD_TGT(Dic_CBTH_HTPD_THBV, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ////Dic_TD_DA_CBDT_KHV
            //ar_i = new int[] { 0, 15 };
            //In_GiaTri_Value(Dic_TD_DA_CBDT_KHV, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, dDim);

            if (lstGPMB.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 16].Value = 1;
            }

            if (lstDLCNT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 17].Value = 1;
            }

            if (lstDTKTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 18].Value = 1;
            }

            if (lstHTTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 16].Value = 0;
                sheet.Cells[rowDA, 17].Value = 0;
                sheet.Cells[rowDA, 18].Value = 0;
                sheet.Cells[rowDA, 19].Value = 1;

            }

            In_TDC_KKGP(sheet, rowDA, lstTDC_TH, lstKKVM_TH, IDDA, sz1, sz2, sz3, 0);

            //Dic_KLTH
            //ar_i = new int[] { 0, 23 };
            //In_GiaTri_Value(Dic_KLTH, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, dDim);

            if (Dic_KLTH.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");

                    sheet.Cells[rowDA, 23].Value = (Convert.ToDouble(sz1)) / dDimKLNT;
                }
            }

            if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");

                    sheet.Cells[rowDA, 24].Value = (Convert.ToDouble(sz1)) / dDimKLNT;
                }
            }

            if (lstPhong_CanBo.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.TenPhong).FirstOrDefault();
                sheet.Cells[rowDA, 28].Value = sz1;

                sz2 = "";
                var lstUser = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.Users).FirstOrDefault();
                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    sz2 += sz + ",";
                }
                if (sz2.Length > 1)
                {
                    sz2 = sz2.Substring(0, sz2.Length - 1);
                }
                sheet.Cells[rowDA, 29].Value = sz2;
            }
            if (isTMDT == 1)
            {
                if (dicDuanTMDT.ContainsKey(IDDA))
                {
                    var daTMDT = dicDuanTMDT[IDDA];
                    sheet.Cells[rowDA, 30].Value = daTMDT.TMDTDA;
                    sheet.Cells[rowDA, 31].Value = daTMDT.SoQD;
                    sheet.Cells[rowDA, 32].Value = daTMDT.NgayKeHoach;
                    sheet.Cells[rowDA, 33].Value = daTMDT.TMDTPD;
                }
            }
            ////Dic_TD_DA_CBDT_GN
            //ar_i = new int[] { 0, 24 };
            //In_GiaTri_Value(Dic_TD_DA_CBDT_GN, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, dDim);

            var ar_i_1 = new int[] { 1, 3, 9, 11, 13, 16, 17, 18, 19 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i_1, 1);

            var ar_i_2 = new int[] { 2, 10, 20, 21, 22, 25 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i_2, 2);

            var ar_i_3 = new int[] { 12, 14, 15, 23, 24 };
            if (isTMDT == 1)
            {
                ar_i_3 = new int[] { 12, 14, 15, 23, 24,30,33 };
            }
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i_3, 3);
        }

        public void In_TDC_KKGP(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KHTDC> lst_TDC, IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM, int IDDA, string sz1, string sz2, string sz3, int loai)
        {
            sz1 = "";
            sz2 = "";
            sz3 = "";

            if (loai == 1)
            {
                int[] ar_i = new int[] { 20, 21, 22 };
                foreach (var i in ar_i)
                {
                    sheet.Cells[rowDA, i].Value = "";
                }
            }

            if (lst_TDC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz3 = lst_TDC.Where(x => x.IDDA == IDDA).Select(x => x.NoiDung).FirstOrDefault();
                sheet.Cells[rowDA, 20].Value = sz3;
            }

            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                var str = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.KK).FirstOrDefault();
                if (str != null)
                {
                    foreach (var item in str)
                    {
                        sz1 += item + "; ";
                    }
                    sheet.Cells[rowDA, 21].Value = sz1;
                }

                var str2 = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.GP).FirstOrDefault();
                if (str2 != null)
                {
                    foreach (var item2 in str2)
                    {
                        sz2 += item2 + "; ";
                    }
                    sheet.Cells[rowDA, 22].Value = sz2;
                }
            }
        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {

                foreach (var row in lstInt)
                {
                    str += sz + row.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr, int isTMDT)
        {
            foreach (var sz in szAr)
            {
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
            }
        }
        #endregion
    }
}
