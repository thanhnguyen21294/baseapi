﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BC_LocDuAn_PhanQuyen")]
    public class BC_LocDuAn_PhanQuyenController : ApiControllerBase
    {
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private IDuAnService _duAnService;

        public BC_LocDuAn_PhanQuyenController(IErrorService errorService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            IDuAnService duAnService
            ) : base(errorService)
        {
            this._qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            this._duAnService = duAnService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };

        [Route("BC_LocDuAn")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCao_DanhSachLuaChonNhaThau_ThiCong(HttpRequestMessage request, string filter = null, int IdGiaiDoan = -1, string uid = null, int pid = 0, int lvid = 0)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string documentName = BaoCao_LocDuAn(filter, IdGiaiDoan,uid,pid,lvid);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCao_LocDuAn(string filter, int IdGiaiDoan, string uid, int pid, int lvid)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BC_LocDuAn_PhanQuyen.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCao_LocDuAn-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        int totalRow = 0;
                        int? idNhomDuAnTheoUser = null;
                        string idUser = "";
                        if (!User.IsInRole("Admin"))
                        {
                            //idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                            idUser = User.Identity.GetUserId();
                        }

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }
                        var lst = _duAnService.GetPhanQuyen(idNhomDuAnTheoUser, idUser, 0, 0, out totalRow, filter, null, IdGiaiDoan, uid, pid, lvid, dicUserNhanVien);

                        var qr = lst[1].Select(x => x.IdDuAn);

                        var lstData = _duAnService.GetList_LocDuAn_For_BaoCao(qr, dicUserNhanVien);

                        int rowIndex = 3;
                        int count = 0;

                        int rowEnd = XuatBaoCao(sheet, lstData, count, rowIndex);

                        for(int i = 2; i <= 9;i++)
                        {
                            sheet.Column(i).Style.WrapText = true;
                        }

                        
                        if (rowEnd > 3)
                        {
                            using (ExcelRange rng = sheet.Cells["B4:I" + rowEnd])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                            }
                            using (ExcelRange rng = sheet.Cells["A4:A" + rowEnd])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                            }

                        }

                        pck.SaveAs(new FileInfo(fullPath));
                        
                        return documentName;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<BC_LocDuAn_PhanQuyen_PhongCT> lstData, int count, int rowIndex)
        {
            foreach (var p in lstData)
            {
                rowIndex++;
                count++;
                sheet.Cells[rowIndex, 1].Value = BaoCaoDungChung.ToRoman(count);
                var idPhong = p.IDPhong;
                string szTenPhong = "";
                if (idPhong != null)
                {
                    if (idPhong == 10)
                    {
                        szTenPhong = "Phòng ĐHDA";
                    }
                    else if (idPhong == 11)
                    {
                        szTenPhong = "Phòng QLKTHT&GPMB";
                    }
                    else if (idPhong == 12)
                    {
                        szTenPhong = "Phòng KHTH";
                    }
                    else
                    {
                        szTenPhong = "Phòng Kế toán - Tài vụ";
                    }
                }

                sheet.Cells[rowIndex, 2].Value = szTenPhong;
                sheet.Row(rowIndex).Style.Font.Bold = true;
                int stt = 0;
                foreach (var dacha in p.grpDuAnCha)
                {
                    rowIndex++;
                    stt++;
                    sheet.Cells[rowIndex, 1].Value = stt;
                    sheet.Cells[rowIndex, 2].Value = dacha.TenDuAnCha;
                    sheet.Cells[rowIndex, 3].Value = dacha.MaDuAnCha;
                    sheet.Cells[rowIndex, 4].Value = dacha.TenNhomDuAnCha;
                    sheet.Cells[rowIndex, 5].Value = dacha.TenLVNNCha;
                    sheet.Cells[rowIndex, 6].Value = dacha.TenGDCha;
                    sheet.Cells[rowIndex, 7].Value = dacha.ThoiGianThucHienCha;
                    sheet.Cells[rowIndex, 8].Value = dacha.QuanLyCha;

                    var lstUser = dacha.Users.ToList();
                    string szUser = "";

                    foreach (var item in lstUser)
                    {
                        string sz = item.Name;
                        szUser += sz + ",";
                    }
                    if (szUser.Length > 1)
                    {
                        szUser = szUser.Substring(0, szUser.Length - 1);
                    }
                    sheet.Cells[rowIndex, 9].Value = szUser;

                    

                    foreach (var dacon in dacha.grpDuAnCon)
                    {
                        rowIndex++;
                        sheet.Cells[rowIndex, 1].Value = "-";
                        sheet.Cells[rowIndex, 2].Value = dacha.TenDuAnCha;
                        sheet.Cells[rowIndex, 3].Value = dacha.MaDuAnCha;
                        sheet.Cells[rowIndex, 4].Value = dacha.TenNhomDuAnCha;
                        sheet.Cells[rowIndex, 5].Value = dacha.TenLVNNCha;
                        sheet.Cells[rowIndex, 6].Value = dacha.TenGDCha;
                        sheet.Cells[rowIndex, 7].Value = dacha.ThoiGianThucHienCha;
                        sheet.Cells[rowIndex, 8].Value = dacha.QuanLyCha;

                        var lstUsercon = dacha.Users.ToList();
                        string szUsercon = "";

                        foreach (var item in lstUser)
                        {
                            string sz = item.Name;
                            szUsercon += sz + ",";
                        }
                        if (szUsercon.Length > 1)
                        {
                            szUsercon = szUsercon.Substring(0, szUsercon.Length - 1);
                        }
                        sheet.Cells[rowIndex, 9].Value = szUsercon;
                    }
                }
            }
            return rowIndex;
        }
    }
}
