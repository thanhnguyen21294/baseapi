﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCDanhSachNhaThau
{
    [Authorize]
    [RoutePrefix("api/BCDanhMucNhaThau")]
    public class TatCaGiaiDoanController : ApiControllerBase
    {
        private IHopDongService _HopDongService;
        private IBaoCaoTableService _baoCaoTableService;
        public TatCaGiaiDoanController(IErrorService errorService,
            IHopDongService HopDongService,
            IBaoCaoTableService baoCaoTableService
            ) : base(errorService)
        {
            this._HopDongService = HopDongService;
            this._baoCaoTableService = baoCaoTableService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };

        [Route("BC_DSLCNT_TatCa")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCao_DanhSachLuaChonNhaThau_TatCa(HttpRequestMessage request, string iIDDuAn)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCao_DSLCNT_TatCaGiaiDoan(iIDDuAn);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCao_DSLCNT_TatCaGiaiDoan(string szIDDuAn)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/DanhSachNhaThauTatCaGiaiDoan.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCao_DanhMucNhaThau" + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            //try
            //{
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }

                        var lstData = _HopDongService.GetHopDong(dicUserNhanVien, szIDDuAn);

                        int rowIndex = 6;
                        int count = 0;

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        sheet.Column(1).Width = 7;
                        sheet.Column(2).Width = 35;
                        sheet.Column(3).Width = 20;

                        for (int i = 4; i <= 73; i++)
                        {
                            sheet.Column(i).Width = 25;
                        }

                        for(int j = 22; j <= 51;j++)
                        {
                            sheet.Column(j).Hidden = true;
                        }

                        rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex, Columns);

                        if (rowIndex > 7)
                        {
                            using (ExcelRange rng = sheet.Cells["A7:BU" + (rowIndex)])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.WrapText = true;
                            }
                        }

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BC_DMNT_TatCa", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<DanhSachLuaChonNhaThau> lstData, int stt1, int rowIndex, string[] Columns)
        {
            foreach (var lv in lstData)
            {
                stt1++;
                rowIndex++;
                sheet.Cells[rowIndex, 1].Value = Columns[0];
                sheet.Cells[rowIndex, 2].Value = lv.TenLinhVuc;
                sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                sheet.Row(rowIndex).Style.Font.Bold = true;
                sheet.Row(rowIndex).Style.Font.Size = 12;
                var stt2 = 0;

                foreach (var gd in lv.grpGiaiDoan)
                {
                    stt2++;
                    rowIndex++;
                    sheet.Cells[rowIndex, 1].Value = BaoCaoDungChung.ToRoman(stt2);
                    sheet.Cells[rowIndex, 2].Value = gd.TenGiaiDoan;
                    sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                    sheet.Row(rowIndex).Style.Font.Bold = true;
                    sheet.Row(rowIndex).Style.Font.Italic = true;
                    sheet.Row(rowIndex).Style.Font.Size = 12;

                    var stt3 = 0;
                    
                    foreach (var da in gd.grpDuAn)
                    {
                        stt3++;
                        rowIndex++;

                        sheet.Cells[rowIndex, 1].Value = stt3;
                        sheet.Cells[rowIndex, 2].Value = da.TenDuAn;
                        sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        var lstUser = da.Users.ToList();
                        string szUser = "";

                        foreach (var item in lstUser)
                        {
                            string sz = item.Name;
                            szUser += sz + ", ";
                        }
                        if (szUser.IndexOf(",") > -1)
                        {
                            szUser = szUser.Substring(0, szUser.Length - 2);
                        }

                        sheet.Cells[rowIndex, 3].Value = szUser;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        foreach (var gt in da.grpGoiThau)
                        {
                            InThuTuCot(gt, sheet, rowIndex);
                        }

                        for (int i = 4; i <= 73; i++)
                        {
                            sheet.Cells[rowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        }
                    }

                    //foreach (var gt in gd.grpGoiThau)
                    //{
                    //    string sz = gt.TenDuAn;

                    //    if(sheet.Cells[rowIndex, 2].Value != null && sheet.Cells[rowIndex, 2].Value.ToString().IndexOf(sz) > -1)
                    //    {
                    //        InThuTuCot(gt, sheet, rowIndex);
                    //    }
                    //    else
                    //    {
                    //        rowIndex++;
                    //        stt3++;
                    //        sheet.Cells[rowIndex, 1].Value = stt3;
                    //        sheet.Cells[rowIndex, 2].Value = gt.TenDuAn;
                    //        InThuTuCot(gt, sheet, rowIndex);
                    //    }
                        
                    //}
                }
            }
            return rowIndex;
        }

        public void InThuTuCot(GoiThauLuaChonNhaThau gt, ExcelWorksheet sheet, int rowIndex)
        {
            string szThoiGian = BaoCaoDungChung.GetDateValue(gt.TuNgay) + " - " + BaoCaoDungChung.GetDateValue(gt.DenNgay);

            int i = 0;
            int j = 0;
            int k = 0;
            string sz = "";
            var iLoaiGT = gt.LoaiGoiThau;

            List<string> lst = new List<string>();
            lst = In_GiaTri_O(iLoaiGT, i, j, k, sz);

            i = Convert.ToInt32(lst[0]);
            j = Convert.ToInt32(lst[1]);
            k = Convert.ToInt32(lst[2]);
            sz = lst[3];
            //worksheet.Column(columnPosition).Hidden = true / false;

            if (i > 0)
            { 
                sheet.Cells[rowIndex, i].Value = gt.NhaThauThucHien;
                if (i >= 22 && i<= 51)
                { 
                    sheet.Column(i).Hidden = false;
                    sheet.Column(i + 1).Hidden = false;
                }
            }

            if (j > 0)
            { 
                sheet.Cells[rowIndex, j].Value = szThoiGian;
                if(j>=22 && j <= 51)
                {
                    sheet.Column(j).Hidden = false;
                }
            }

            if (k > 0)
            { 
                sheet.Cells[rowIndex, k].Value = gt.NhaThauThucHien;
                if(k >= 22 && k <= 51)
                { 
                    sheet.Column(k).Hidden = false;
                }
            }

            //if (k != 0)
            //{
            //    sheet.Cells[rowIndex, i].Value = gt.NhaThauThucHien;
            //    sheet.Cells[rowIndex, k].Value = sz;
            //    sheet.Cells[rowIndex, j].Value = szThoiGian;
            //}
            //else
            //{
            //    sheet.Cells[rowIndex, i].Value = gt.NhaThauThucHien;
            //    sheet.Cells[rowIndex, j].Value = szThoiGian;
            //}
        }

        public List<string> In_GiaTri_O (int? LoaiGoiThau, int i, int j, int k, string sz)
        {
            switch (LoaiGoiThau)
            {
                case 1:
                    i = 4;
                    j = 5;
                    break;
                case 2:
                    i = 6;
                    j = 7;
                    break;
                case 3:
                    i = 8;
                    j = 9;
                    break;
                case 4:
                    i = 10;
                    j = 11;
                    break;
                case 5:
                    i = 12;
                    j = 13;
                    break;
                case 6:
                    i = 14;
                    j = 15;
                    break;
                case 7:
                    i = 16;
                    j = 17;
                    break;
                case 8:
                    i = 18;
                    j = 19;
                    break;
                case 9:
                    i = 20;
                    j = 21;
                    break;
                case 10:
                    i = 22;
                    //sz = "Tư vấn giám sát 1";
                    //k = 23;
                    j = 24;
                    break;
                case 11:
                    i = 25;
                    //k = 26;
                    //sz = "Tư vấn giám sát 2";
                    j = 27;
                    break;
                case 12:
                    i = 28;
                    //k = 29;
                    //sz = "Tư vấn giám sát 3";
                    j = 30;
                    break;
                case 13:
                    i = 31;
                    //k = 32;
                    //sz = "Tư vấn giám sát 4";
                    j = 33;
                    break;
                case 14:
                    i = 34;
                    //k = 35;
                    //sz = "Tư vấn giám sát 5";
                    j = 36;
                    break;
                case 15:
                    i = 37;
                    //k = 38;
                    //sz = "Tư vấn giám sát 6";
                    j = 39;
                    break;
                case 16:
                    i = 40;
                    //k = 41;
                    //sz = "Tư vấn giám sát 7";
                    j = 42;
                    break;
                case 17:
                    i = 43;
                    //k = 44;
                    //sz = "Tư vấn giám sát 8";
                    j = 45;
                    break;
                case 18:
                    i = 46;
                    //k = 47;
                    //sz = "Tư vấn giám sát 9";
                    j = 48;
                    break;
                case 19:
                    i = 49;
                    //k = 50;
                    //sz = "Tư vấn giám sát 10";
                    j = 51;
                    break;

                case 20:
                    //i = 22;
                    //sz = "x";
                    k = 23;
                    //j = 24;
                    break;
                case 21:
                    //i = 25;
                    //sz = "x";
                    k = 26;
                    //j = 27;
                    break;
                case 22:
                    //i = 28;
                    k = 29;
                    //sz = "x";
                    //j = 30;
                    break;
                case 23:
                    //i = 31;
                    k = 32;
                    //sz = "x";
                    //j = 33;
                    break;
                case 24:
                    //i = 34;
                    k = 35;
                    //sz = "x";
                    //j = 36;
                    break;
                case 25:
                    //i = 37;
                    k = 38;
                    //sz = "x";
                    //j = 39;
                    break;
                case 26:
                    //i = 40;
                    k = 41;
                    //sz = "x";
                    //j = 42;
                    break;
                case 27:
                    //i = 43;
                    k = 44;
                    //sz = "x";
                    //j = 45;
                    break;
                case 28:
                    //i = 46;
                    k = 47;
                    //sz = "x";
                    //j = 48;
                    break;
                case 29:
                    //i = 49;
                    k = 50;
                    //sz = "x";
                    //j = 51;
                    break;


                case 30:
                    i = 52;
                    //k = 53;
                    //sz = "Tư vấn giám sát chống mối";
                    j = 54;
                    break;
                case 31:
                    //i = 52;
                    k = 53;
                    //sz = "x";
                    //j = 54;
                    break;


                case 32:
                    i = 55;
                    //k = 56;
                    //sz = "Tư vấn giám sát PCCC";
                    j = 57;
                    break;
                case 33:
                    //i = 55;
                    k = 56;
                    //sz = "x";
                    //j = 57;
                    break;


                case 34:
                    i = 58;
                    //k = 59;
                    //sz = "Tư vấn giám sát thiết bị";
                    j = 60;
                    break;
                case 35:
                    //i = 58;
                    k = 59;
                    //sz = "x";
                    //j = 60;
                    break;

                case 36:
                    i = 61;
                    //k = 62;
                    //sz = "Tư vấn giám sát cây xanh";
                    j = 63;
                    break;
                case 37:
                    //i = 61;
                    k = 62;
                    //sz = "x";
                    //j = 63;
                    break;

                case 38:
                    i = 64;
                    //k = 65;
                    //sz = "Tư vấn giám sát hạ ngầm, di chuyển/TBA";
                    j = 66;
                    break;
                case 39:
                    //i = 64;
                    k = 65;
                    //sz = "x";
                    //j = 66;
                    break;

                case 40:
                    i = 67;
                    //k = 68;
                    //sz = "Tư vấn giám sát khác";
                    j = 69;
                    break;
                case 41:
                    //i = 67;
                    k = 68;
                    //sz = "x";
                    //j = 69;
                    break;

                case 42:
                    i = 70;
                    j = 71;
                    break;
                case 43:
                    i = 72;
                    j = 73;
                    break;

                default:
                    break;
            }
            List<string> lst = new List<string>();
            lst.Add(i.ToString());
            lst.Add(j.ToString());
            lst.Add(k.ToString());
            lst.Add(sz);
            return lst;
        }
    }
}
