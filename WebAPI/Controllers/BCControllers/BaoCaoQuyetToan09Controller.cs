﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan09")]
    public class BaoCaoQuyetToan09Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IQuyetToanDuAnService _quyetToanDuAnService;
        private ILapQuanLyDauTuService _lapQuanLyDauTuService;
        public BaoCaoQuyetToan09Controller(ILapQuanLyDauTuService lapQuanLyDauTuService, IQuyetToanDuAnService quyetToanDuAnService, IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._quyetToanDuAnService = quyetToanDuAnService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
        }
        #region Mau09
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau09")]
        [HttpGet]
        public HttpResponseMessage ExportMau09(HttpRequestMessage request, string iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau09(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau09(string iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-09-MauPhuLuc9.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc9-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        double tongconggiatridt = 0;
                        double tongconggiatriqt = 0;
                        string[] arrListStr = iIDDuAn.Split(',');
                        List<int> danhsachduandaxuat = new List<int>();
                        int rowIndex = 17;
                        int indexduanchinh = 0;
                        foreach (var itemduan in arrListStr)
                        {
                            int iditem = Int32.Parse(itemduan);
                            var duan = _duAnService.GetByIdForBaoCao(iditem);
                            if (!danhsachduandaxuat.Contains(iditem))
                            {
                                danhsachduandaxuat.Add(iditem);

                                if (duan.NhomDuAn?.TenNhomDuAn == "Nhóm A" || duan.NhomDuAn?.TenNhomDuAn == "Quan trọng quốc gia")
                                {

                                    var quyettoanduancuoi = _quyetToanDuAnService.GetNewestByIdDuAn(iditem);
                                    //bắt đầu từ hàng 17
                                    double tonggiatridt = 0;
                                    double tonggiatriqt = 0;
                                    rowIndex++;
                                    indexduanchinh++;
                                    sheet.InsertRow(rowIndex, 1);
                                    sheet.Cells[rowIndex, 1].Value = indexduanchinh;
                                    sheet.Cells[rowIndex, 2].Value = "Dự án chính: " + '\n' + "-" + duan.TenDuAn;
                                    sheet.Cells[rowIndex, 2].Style.WrapText = true;
                                    if (quyettoanduancuoi != null)
                                    {
                                        sheet.Cells[rowIndex, 3].Value = quyettoanduancuoi.CoQuanPheDuyet?.TenCoQuanPheDuyet;
                                        sheet.Cells[rowIndex, 3].Style.WrapText = true;
                                    }
                                    else
                                    {
                                        sheet.Cells[rowIndex, 3].Value = "";
                                    }
                                    sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[rowIndex, 1, rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[rowIndex, 1, rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[rowIndex, 4].Value = duan.ChuDauTu?.TenChuDauTu;
                                    sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    sheet.Cells[rowIndex, 4].Style.WrapText = true;
                                    var quyettoanduanhoanthanh = _quyetToanDuAnService.GetByIdDuAn(iditem);
                                    double tonggiagiaquyettoanhoanthanh = 0;
                                    foreach (var item in quyettoanduanhoanthanh)
                                    {
                                        if (item.NgayPheDuyetQuyetToan != null)
                                        {
                                            if (item.TongGiaTri == null)
                                            {
                                                item.TongGiaTri = 0;
                                            }
                                            tonggiagiaquyettoanhoanthanh += (double)item.TongGiaTri;
                                        }
                                    }
                                    tonggiatridt += tonggiagiaquyettoanhoanthanh;
                                    sheet.Cells[rowIndex, 5].Value = tonggiagiaquyettoanhoanthanh / dvt;
                                    sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                    sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    double giatriqt = 0;
                                    if (quyettoanduancuoi != null)
                                    {
                                        foreach (var item in quyettoanduancuoi.NguonVonQuyetToanDuAns)
                                        {
                                            giatriqt += item.GiaTri;
                                        }
                                    }
                                    tonggiatriqt += giatriqt;
                                    sheet.Cells[rowIndex, 7].Value = giatriqt / dvt;
                                    sheet.Cells[rowIndex, 7].Style.Numberformat.Format = "#,##0";
                                    sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    //nếu dự án có dự án con
                                    List<DuAn> list1 = new List<DuAn>();
                                    List<DuAn> list2 = new List<DuAn>();
                                    _duAnService.GetChildByIdDuAn(iditem, list1, list2);
                                    var newlist = list1;
                                    sheet.DeleteRow(rowIndex + 1);
                                    sheet.DeleteRow(rowIndex + 1);
                                    int indexduanthanhphan = 0;
                                    foreach (var item in newlist)
                                    {
                                        danhsachduandaxuat.Add(item.IdDuAn);
                                        rowIndex++;
                                        indexduanthanhphan++;
                                        sheet.InsertRow(rowIndex, 1);
                                        var quyettoanduancuoi1 = _quyetToanDuAnService.GetNewestByIdDuAn(item.IdDuAn);
                                        sheet.Cells[rowIndex, 1].Value = indexduanchinh + "." + indexduanthanhphan;
                                        sheet.Cells[rowIndex, 2].Value = "Dự án thành phần: " + '\n' + "-" + item.TenDuAn;
                                        sheet.Cells[rowIndex, 2].Style.WrapText = true;
                                        if (quyettoanduancuoi1 != null)
                                        {
                                            sheet.Cells[rowIndex, 3].Value = quyettoanduancuoi1.CoQuanPheDuyet?.TenCoQuanPheDuyet;
                                            sheet.Cells[rowIndex, 3].Style.WrapText = true;
                                        }
                                        else
                                        {
                                            sheet.Cells[rowIndex, 3].Value = "";
                                        }
                                        sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        sheet.Cells[rowIndex, 1, rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        sheet.Cells[rowIndex, 1, rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        sheet.Cells[rowIndex, 4].Value = duan.ChuDauTu?.TenChuDauTu;
                                        sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        sheet.Cells[rowIndex, 4].Style.WrapText = true;
                                        var quyettoanduanhoanthanh1 = _quyetToanDuAnService.GetByIdDuAn(item.IdDuAn);
                                        double tonggiagiaquyettoanhoanthanh1 = 0;
                                        foreach (var item1 in quyettoanduanhoanthanh1)
                                        {
                                            if (item1.NgayPheDuyetQuyetToan != null)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tonggiagiaquyettoanhoanthanh1 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        tonggiatridt += tonggiagiaquyettoanhoanthanh1;
                                        sheet.Cells[rowIndex, 5].Value = tonggiagiaquyettoanhoanthanh1 / dvt;
                                        sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                        sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        double giatriqt1 = 0;
                                        if (quyettoanduancuoi1 != null)
                                        {
                                            foreach (var item1 in quyettoanduancuoi1.NguonVonQuyetToanDuAns)
                                            {
                                                giatriqt1 += item1.GiaTri;
                                            }
                                        }
                                        tonggiatriqt += giatriqt1;
                                        sheet.Cells[rowIndex, 7].Value = giatriqt1 / dvt;
                                        sheet.Cells[rowIndex, 7].Style.Numberformat.Format = "#,##0";
                                        sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    }
                                    tongconggiatridt += tonggiatridt;
                                    tongconggiatriqt += tonggiatriqt;

                                }

                            }
                        }
                        
                        
                        sheet.Cells["E17"].Value = tongconggiatridt / dvt;
                        sheet.Cells["G17"].Value = tongconggiatriqt / dvt;
                        sheet.Cells["E17"].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["G17"].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["E17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["G17"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        string dvTinh = sheet.Cells["G14"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["G14"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 09/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
