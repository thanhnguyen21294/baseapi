﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;
using Service.BCService;

using System.Linq;
using System.Collections.Generic;
using OfficeOpenXml.Style;
using System.Drawing;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/TheoDoiBaoLanh")]
    public class TheoDoiBaoLanhController : ApiControllerBase
    {
        private IBaoLanhTHHDService _baoLanhTHHDService;
        private IBaoCaoTableService _baoCaoTableService;
        public TheoDoiBaoLanhController(IBaoCaoTableService baoCaoTableService, IBaoLanhTHHDService baoLanhTHHDService, IErrorService errorService) : base(errorService)
        {
            _baoCaoTableService = baoCaoTableService;
            _baoLanhTHHDService = baoLanhTHHDService;
        }

        #region Tinh Phe Duyet
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("XuatBaoCao")]
        [HttpGet]
        public HttpResponseMessage Export(HttpRequestMessage request, string iIDDuAn, string szNgayHetHan, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName=TinhPheDuyet(iIDDuAn, szNgayHetHan, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string TinhPheDuyet(string iIDDuAn, string szNgayHetHan, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            int dvt = 1;
            if (szDonViTinh == "Đồng")
            {
                dvt = 1;
            }
            if (szDonViTinh == "Nghìn Đồng")
            {
                dvt = 1000;
            }
            if (szDonViTinh == "Triệu Đồng")
            {
                dvt = 1000000;
            }
            if (szDonViTinh == "Tỷ Đồng")
            {
                dvt = 1000000000;
            }
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            //" + szNgayHetHan.Replace("\\\"","-") + "-
            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BC-TheoDoiBaoLanhHD.xlsx");
            string documentName = string.Format("BC-TheoDoiBaoLanhHD-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);

            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                    string title = "(Bao gồm DA đã quá hạn và DA đến hạn trước "+ szNgayHetHan +")";
                    sheet.Cells["A2"].Value = title;
                    string dvTinh = "Đơn vị tính: " + szDonViTinh;
                    sheet.Cells["K3"].Value = dvTinh;

                    var lstBaoLanh = this._baoLanhTHHDService.GetByNgayHetHan(iIDDuAn, szNgayHetHan).Select(x => new {
                        x.HopDong.GoiThau.DuAn.IdDuAn,
                         x.HopDong.GoiThau.DuAn.TenDuAn,
                         x.HopDong.GoiThau.TenGoiThau,
                         x. HopDong.SoHopDong,
                         x.HopDong.DanhMucNhaThau?.TenNhaThau,
                         x.LoaiBaoLanh,
                         x.SoBaoLanh,
                        x.NgayBaoLanh,
                        x.NgayHetHan,
                        x.SoTien,
                        x.HopDong.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                        x.HopDong.DuAn.IdNhomDuAnTheoUser,
                        Users =x.HopDong.DuAn.DuAnUsers.Select(y => new
                        {
                            TenUser = y.AppUsers.FullName,
                            Nhom = y.AppUsers.IdNhomDuAnTheoUser
                        }).ToList()
                    }).ToList();

                    var grpBaoLanh = lstBaoLanh.GroupBy(x => new { x.IdDuAn, x.TenDuAn }).Select(x => new
                    {
                        IdDuAn = x.Key.IdDuAn,
                        TenDuAn = x.Key.TenDuAn,
                        GoiThaus = x.Select(y => new
                        {
                            y.TenGoiThau,
                            y.SoHopDong,
                            y.TenNhaThau,
                            y.LoaiBaoLanh,
                            y.SoBaoLanh,
                            y.NgayBaoLanh,
                            y.NgayHetHan,
                            y.SoTien,
                            y.TenNhomDuAn,
                            User = string.Join(", ", y.Users.Where(z => z.Nhom == y.IdNhomDuAnTheoUser).Select(z => z.TenUser))
                        }).ToList()
                    }).ToList();
                  
                    int index = 5;
                    int indexS = 0;
                    int indexE = 0;
                    for (var i = 0; i < grpBaoLanh.Count; i++)
                    {
                        indexS = index;
                        //sheet.InsertRow(index, 1);
                        //sheet.Cells["A" + index + ":O" + index].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //sheet.Cells["A" + index + ":O" + index].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        //sheet.Cells["A" + index + ":O" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //sheet.Cells["C" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //sheet.Cells["C" + index].Style.WrapText = true;
                        //sheet.Cells["D" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //sheet.Cells["E" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //sheet.Cells["F" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //sheet.Cells["G" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //sheet.Cells["H" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        //sheet.Cells["I" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //sheet.Cells["J" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //sheet.Cells["K" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //sheet.Cells["K" + index].Style.Numberformat.Format = "#";
                        //sheet.Cells["L" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        var da = grpBaoLanh[i];
                        string tenDa = da.TenDuAn;
                        int iMegerRow = da.GoiThaus.Count();
                        sheet.Cells["A" + index].Value = (i+1).ToString();
                        sheet.Cells["B" + index].Value = tenDa;
                        for (var j = 0; j < da.GoiThaus.Count; j++)
                        {
                            var gt = da.GoiThaus[j];
                            sheet.Cells["C" + index].Value = gt.TenGoiThau;
                            sheet.Cells["D" + index].Value = gt.SoHopDong;
                            sheet.Cells["E" + index].Value = gt.TenNhaThau;
                            sheet.Cells["F" + index].Value = gt.LoaiBaoLanh == 1 ? "1" : "";
                            sheet.Cells["G" + index].Value = gt.LoaiBaoLanh == 2 ? "1" : "";
                            sheet.Cells["H" + index].Value = gt.SoBaoLanh;
                            sheet.Cells["I" + index].Value = (DateTime)gt.NgayBaoLanh;
                            sheet.Cells["J" + index].Value = (DateTime)gt.NgayHetHan;
                            sheet.Cells["K" + index].Value = gt.SoTien;
                            sheet.Cells["L" + index].Value = "";
                            sheet.Cells["M" + index].Value = gt.TenNhomDuAn;
                            sheet.Cells["N" + index].Value = gt.User;
                            sheet.Cells["O" + index].Value = "";

                            sheet.Cells["A" + index + ":O" + index].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["A" + index + ":O" + index].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["A" + index + ":O" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["B" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["C" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["C" + index].Style.WrapText = true;
                            sheet.Cells["D" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["E" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["F" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["G" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["H" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["I" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["J" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["I" + index].Style.Numberformat.Format = "dd/mm/yyyy";
                            sheet.Cells["J" + index].Style.Numberformat.Format = "dd/mm/yyyy";
                            sheet.Cells["K" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["K" + index].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["L" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["M" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["N" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["M" + index].Style.WrapText = true;
                            sheet.Cells["N" + index].Style.WrapText = true;
                            indexE = index;
                            index++;
                        }
                        if(indexS != indexE)
                        {
                            sheet.Cells["A" + indexS + ":A" + indexE].Merge = true;
                            sheet.Cells["B" + indexS + ":B" + indexE].Merge = true;
                        }
                    }

                    pck.SaveAs(new FileInfo(fullPath));
                    _baoCaoTableService.AddBaoCao(documentName, "TheoDoiBaoLanh", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }
        #endregion
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            
            var TenGoiThau = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return TenGoiThau;
        }
        
    }
}

