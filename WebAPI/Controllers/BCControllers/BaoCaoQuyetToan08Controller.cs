﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan08")]
    public class BaoCaoQuyetToan08Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVanBanDuAnService _vanBanDuAnService;
        private INguonVonService _nguonVonService;
        private INguonVonDuAnService _nguonVonDuAnService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private INguonVonGoiThauThanhToanService _nguonVonGoiThauThanhToanService;
        private INguonVonThanhToanChiPhiKhacService _nguonVonThanhToanChiPhiKhacService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        private ILapQuanLyDauTuService _lapQuanLyDauTuService;
        private IHopDongService _hopDongService;
        public BaoCaoQuyetToan08Controller(IHopDongService hopDongService, ILapQuanLyDauTuService lapQuanLyDauTuService, IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, INguonVonThanhToanChiPhiKhacService nguonVonThanhToanChiPhiKhacService, INguonVonGoiThauThanhToanService nguonVonGoiThauThanhToanService, IThanhToanHopDongService thanhToanHopDongService, INguonVonDuAnService nguonVonDuAnService, INguonVonService nguonVonService, IVanBanDuAnService vanBanDuAnService, IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._vanBanDuAnService = vanBanDuAnService;
            this._nguonVonService = nguonVonService;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._thanhToanHopDongService = thanhToanHopDongService;
            this._nguonVonGoiThauThanhToanService = nguonVonGoiThauThanhToanService;
            this._nguonVonThanhToanChiPhiKhacService = nguonVonThanhToanChiPhiKhacService;
            this._thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
            this._hopDongService = hopDongService;
        }
        #region Mau08
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau08")]
        [HttpGet]
        public HttpResponseMessage ExportMau08(HttpRequestMessage request, int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau08(iIDDuAn, szNguonVon, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau08(int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-08-MauPhuLuc8.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc8-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // ten du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = "Của dự án: " + duan.TenDuAn;
                        sheet.Cells["A2"].Style.WrapText = true;
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        // van ban phap ly
                        var vanbanphaply = _vanBanDuAnService.GetByIdDuAn(iIDDuAn);
                        //khai bao row exel bat dau tu hang so 7
                        int rowIndex = 7;
                        int demsovanban = 0;
                        foreach (var item in vanbanphaply)
                        {
                            rowIndex++;
                            demsovanban++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = demsovanban;
                            sheet.Cells[rowIndex, 2].Value = item.TenVanBan;
                            sheet.Cells[rowIndex, 3].Value = item.SoVanBan + ", " + item.NgayBanHanh?.ToString("dd/MM/yyyy");
                            sheet.Cells[rowIndex, 4].Value = item.CoQuanBanHanh;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        //thuc hien dau tu
                        //cong them 5 hang
                        rowIndex = rowIndex + 5;
                        //gan gia tri nguon von
                        sheet.Cells[rowIndex, 4].Value = "Đơn vị: " + szDonViTinh;
                        //nguon von
                        //cong them 4 hang
                        //duoc duyet
                        rowIndex = rowIndex + 4;
                        var nguonvon = _nguonVonService.GetById(szNguonVon).TenNguonVon;
                        sheet.Cells[rowIndex, 1].Value = nguonvon;
                        var nguonvonduan = _nguonVonDuAnService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        double tongsonguonvonduanduocduyet = 0;
                        foreach (var item in nguonvonduan)
                        {
                            if (item.GiaTri == null)
                            {
                                item.GiaTri = 0;
                            }
                            tongsonguonvonduanduocduyet += (double)item.GiaTri;
                        }
                        sheet.Cells[rowIndex, 3].Value = tongsonguonvonduanduocduyet / dvt;
                        //thuc hien
                        //thanh toans KLTH
                        var thanhtoanhopdong = _thanhToanHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        double thanhtoanKLTH = 0;
                        foreach (var item in thanhtoanhopdong)
                        {
                            var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                            foreach (var item1 in nguonvongoithauthanhtoan)
                            {
                                thanhtoanKLTH += item1.GiaTri;
                            }
                        }
                        // thanh toan chi phi khac
                        var nguonvonthanhtoanchiphikhac = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        var thanhtoanchiphikhac = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in nguonvonthanhtoanchiphikhac)
                        {
                            thanhtoanKLTH += item.GiaTri;
                        }
                        //tạm ứng
                        //var tamunghopdong = _tamUngHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        //foreach (var item in tamunghopdong)
                        //{
                        //    var tamung1 = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                        //    foreach (var item1 in tamung1)
                        //    {
                        //        thanhtoanKLTH += item1.GiaTri;
                        //    }
                        //}
                        sheet.Cells[rowIndex, 4].Value = thanhtoanKLTH / dvt;
                        //chi phi dau tu
                        //cong them 3 dong
                        rowIndex = rowIndex + 3;
                        sheet.Cells[rowIndex, 4].Value = "Đơn vị: " + szDonViTinh;
                        //khai bao cac bien
                        double tongso = 0;
                        double gpmb = 0;
                        double xd = 0;
                        double tb = 0;
                        double qlda = 0;
                        double tv = 0;
                        double khac = 0;
                        var lapquanlydautu = _lapQuanLyDauTuService.GetNewestTheoDuAn(iIDDuAn);
                        if (lapquanlydautu != null)
                        {
                            if (lapquanlydautu.TongGiaTri == null)
                            {
                                lapquanlydautu.TongGiaTri = 0;
                            }
                            tongso += (double)lapquanlydautu.TongGiaTri;
                            if (lapquanlydautu.GiaiPhongMatBang == null)
                            {
                                lapquanlydautu.GiaiPhongMatBang = 0;
                            }
                            gpmb += (double)lapquanlydautu.GiaiPhongMatBang;
                            if (lapquanlydautu.XayLap == null)
                            {
                                lapquanlydautu.XayLap = 0;
                            }
                            xd += (double)lapquanlydautu.XayLap;
                            if (lapquanlydautu.ThietBi == null)
                            {
                                lapquanlydautu.ThietBi = 0;
                            }
                            tb += (double)lapquanlydautu.ThietBi;
                            if (lapquanlydautu.QuanLyDuAn == null)
                            {
                                lapquanlydautu.QuanLyDuAn = 0;
                            }
                            qlda += (double)lapquanlydautu.QuanLyDuAn;
                            if (lapquanlydautu.TuVan == null)
                            {
                                lapquanlydautu.TuVan = 0;
                            }
                            tv += (double)lapquanlydautu.TuVan;
                            if (lapquanlydautu.Khac == null)
                            {
                                lapquanlydautu.Khac = 0;
                            }
                            khac += (double)lapquanlydautu.Khac;
                            if (lapquanlydautu.DuPhong == null)
                            {
                                lapquanlydautu.DuPhong = 0;
                            }
                            khac += (double)lapquanlydautu.DuPhong;
                        }
                        rowIndex = rowIndex + 3;
                        sheet.Cells[rowIndex, 3].Value = tongso / dvt;
                        sheet.Cells[rowIndex + 1, 3].Value = gpmb / dvt;
                        sheet.Cells[rowIndex + 2, 3].Value = xd / dvt;
                        sheet.Cells[rowIndex + 3, 3].Value = tb / dvt;
                        sheet.Cells[rowIndex + 4, 3].Value = qlda / dvt;
                        sheet.Cells[rowIndex + 5, 3].Value = tv / dvt;
                        sheet.Cells[rowIndex + 6, 3].Value = khac / dvt;
                        //chi phi dau tu de nghi quyet toan
                        double gpmbqt = 0;
                        double xdqt = 0;
                        double tbqt = 0;
                        double qldaqt = 0;
                        double tvqt = 0;
                        double khacqt = 0;
                        //boi thuong ho tro
                        var hopdonggpmb = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 3);
                        foreach (var item in hopdonggpmb)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach(var item2 in nguonvongoithautamung)
                            //    {
                            //        gpmbqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    gpmbqt += item2.GiaTri;
                                }
                            }
                        }
                        //xay lap
                        var hopdongxd = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 1);
                        foreach (var item in hopdongxd)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        xdqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    xdqt += item2.GiaTri;
                                }
                            }
                        }
                        //thiet bi
                        var hopdongtb = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 2);
                        foreach (var item in hopdongtb)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        tbqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    tbqt += item2.GiaTri;
                                }
                            }
                        }
                        //tu van
                        var hopdongtv = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 5);
                        foreach (var item in hopdongtv)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        tvqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    tvqt += item2.GiaTri;
                                }
                            }
                        }
                        //khac
                        var hopdongkhac = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 4);
                        foreach (var item in hopdongkhac)
                        {
                            //var thanhtoans = thanh.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        khacqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    khacqt += item2.GiaTri;
                                }
                            }
                        }
                        var thanhtoanchiphikhacqt = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in thanhtoanchiphikhac)
                        {
                            var nguonvonttcpk = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                            if (item.LoaiThanhToan == null)
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khacqt += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.LoaiThanhToan.Contains("1") && item.NgayThanhToan != null)
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        qldaqt += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.NgayThanhToan != null && !item.LoaiThanhToan.Contains("1"))
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khacqt += item1.GiaTri;
                                    }
                                }
                            }
                        }
                        sheet.Cells[rowIndex, 4].Value = (gpmbqt + xdqt + tbqt + qldaqt + tvqt + khacqt) / dvt;
                        sheet.Cells[rowIndex + 1, 4].Value = gpmbqt / dvt;
                        sheet.Cells[rowIndex + 2, 4].Value = xdqt / dvt;
                        sheet.Cells[rowIndex + 3, 4].Value = tbqt / dvt;
                        sheet.Cells[rowIndex + 4, 4].Value = qldaqt / dvt;
                        sheet.Cells[rowIndex + 5, 4].Value = tvqt / dvt;
                        sheet.Cells[rowIndex + 6, 4].Value = khacqt / dvt;
                        sheet.Cells[rowIndex, 5].Value = (gpmbqt + xdqt + tbqt + qldaqt + tvqt + khacqt - gpmb - xd - tb - qlda - tv - khac) / dvt;
                        sheet.Cells[rowIndex + 1, 5].Value = (gpmbqt - gpmb) / dvt;
                        sheet.Cells[rowIndex + 2, 5].Value = (xdqt - xd) / dvt;
                        sheet.Cells[rowIndex + 3, 5].Value = (tbqt - tb) / dvt;
                        sheet.Cells[rowIndex + 4, 5].Value = (qldaqt - qlda) / dvt;
                        sheet.Cells[rowIndex + 5, 5].Value = (tvqt - tv) / dvt;
                        sheet.Cells[rowIndex + 6, 5].Value = (khacqt - khac) / dvt;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 08/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
