﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan11")]
    public class BaoCaoQuyetToan11Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private ILapQuanLyDauTuService _lapQuanLyDauTuService;
        private IHopDongService _hopDongService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private IQuyetToanDuAnService _quyetToanDuAnService;
        private ICoQuanPheDuyetService _coQuanPheDuyetService;
        public BaoCaoQuyetToan11Controller(ICoQuanPheDuyetService coQuanPheDuyetService, IQuyetToanDuAnService quyetToanDuAnService, IThanhToanHopDongService thanhToanHopDongService, IHopDongService hopDongService, ILapQuanLyDauTuService lapQuanLyDauTuService, IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
            this._hopDongService = hopDongService;
            this._thanhToanHopDongService = thanhToanHopDongService;
            this._quyetToanDuAnService = quyetToanDuAnService;
            this._coQuanPheDuyetService = coQuanPheDuyetService;
        }
        #region Mau11
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau11")]
        [HttpGet]
        public HttpResponseMessage ExportMau11(HttpRequestMessage request, int szDonViBaoCao, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau11(szDonViBaoCao, szLoaiBaoCao, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau11(int szDonViBaoCao, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-11-MauPhuLuc11.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc11-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        int checkday = 32;
                        int checkmonth = 0;
                        int checkyear = 0;
                        if(szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            checkday = 32;
                            checkmonth = 6;
                            checkyear = iNamPD;
                        }
                        if(szLoaiBaoCao == "cả năm")
                        {
                            checkyear = 32;
                            checkmonth = 12;
                            checkyear = iNamPD;
                        }
                        //co quan phe duyet
                        var coquanpheduyet = _coQuanPheDuyetService.GetById(szDonViBaoCao);
                        sheet.Cells["A2"].Value = coquanpheduyet.TenCoQuanPheDuyet;
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //lay tat ca cac du an da quyet toan theo loaibaocao va nam truyen vao
                        var duans = _duAnService.GetByLoaiBaoCaoByYearByQuyetToanHoanThanh(szDonViBaoCao, szLoaiBaoCao, iNamPD);
                        int nhomaindex = 0;
                        int nhombindex = 0;
                        int nhomcindex = 0;
                        double tongmucdautunhoma = 0;
                        double tongmucdautunhomb = 0;
                        double tongmucdautunhomc = 0;
                        double denghiquyettoannhoma = 0;
                        double denghiquyettoannhomb = 0;
                        double denghiquyettoannhomc = 0;
                        double giatridaquyettoannhoma = 0;
                        double giatridaquyettoannhomb = 0;
                        double giatridaquyettoannhomc = 0;
                        foreach (var item in duans)
                        {
                            if(item.NhomDuAn?.TenNhomDuAn == "Nhóm A")
                            {
                                nhomaindex++;
                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                if(tongmucdautu!=null && tongmucdautu.NgayPheDuyet.Value.Day<=checkday && tongmucdautu.NgayPheDuyet.Value.Month<=checkmonth && tongmucdautu.NgayPheDuyet.Value.Year<=checkyear)
                                {
                                    if (tongmucdautu.TongGiaTri == null)
                                    {
                                        tongmucdautu.TongGiaTri = 0;
                                    }
                                    tongmucdautunhoma += (double)tongmucdautu.TongGiaTri;
                                }
                                else
                                {
                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                    {
                                        if (tongmucdautu2.TongGiaTri == null)
                                        {
                                            tongmucdautu2.TongGiaTri = 0;
                                        }
                                        tongmucdautunhoma += (double)tongmucdautu2.TongGiaTri;
                                    }
                                }
                                var hopdong = _hopDongService.GetByIdDuAn(item.IdDuAn);
                                foreach(var item1 in hopdong)
                                {
                                    var thanhtoanhopdong = _thanhToanHopDongService.GetByIdHopDong(item1.IdHopDong);
                                    foreach(var item2 in thanhtoanhopdong)
                                    {
                                        if(item2.ThoiDiemThanhToan != null)
                                        {
                                            if(item2.ThoiDiemThanhToan.Value.Day<=checkday && item2.ThoiDiemThanhToan.Value.Month<=checkmonth && item2.ThoiDiemThanhToan.Value.Year <= checkyear)
                                            {
                                                if(item2.DeNghiThanhToan == null)
                                                {
                                                    item2.DeNghiThanhToan = 0;
                                                }
                                                denghiquyettoannhoma +=(double)item2.DeNghiThanhToan;
                                            }
                                        }
                                    }
                                }
                                var quyettoanduanhoanthanhtheoduan = _quyetToanDuAnService.GetHoanThanhByIdDuAn(item.IdDuAn);
                                foreach(var item1 in quyettoanduanhoanthanhtheoduan)
                                {
                                    if(item1.NgayPheDuyetQuyetToan.Value.Day <= checkday && item1.NgayPheDuyetQuyetToan.Value.Month<=checkmonth && item1.NgayPheDuyetQuyetToan.Value.Year <= checkyear)
                                    {
                                        if(item1.TongGiaTri == null)
                                        {
                                            item1.TongGiaTri = 0;
                                        }
                                        giatridaquyettoannhoma +=(double)item1.TongGiaTri;
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm B")
                            {
                                nhombindex++;
                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                {
                                    if (tongmucdautu.TongGiaTri == null)
                                    {
                                        tongmucdautu.TongGiaTri = 0;
                                    }
                                    tongmucdautunhomb += (double)tongmucdautu.TongGiaTri;
                                }
                                else
                                {
                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                    {
                                        if (tongmucdautu2.TongGiaTri == null)
                                        {
                                            tongmucdautu2.TongGiaTri = 0;
                                        }
                                        tongmucdautunhomb += (double)tongmucdautu2.TongGiaTri;
                                    }
                                }
                                var hopdong = _hopDongService.GetByIdDuAn(item.IdDuAn);
                                foreach (var item1 in hopdong)
                                {
                                    var thanhtoanhopdong = _thanhToanHopDongService.GetByIdHopDong(item1.IdHopDong);
                                    foreach (var item2 in thanhtoanhopdong)
                                    {
                                        if (item2.ThoiDiemThanhToan != null)
                                        {
                                            if (item2.ThoiDiemThanhToan.Value.Day <= checkday && item2.ThoiDiemThanhToan.Value.Month <= checkmonth && item2.ThoiDiemThanhToan.Value.Year <= checkyear)
                                            {
                                                if (item2.DeNghiThanhToan == null)
                                                {
                                                    item2.DeNghiThanhToan = 0;
                                                }
                                                denghiquyettoannhomb += (double)item2.DeNghiThanhToan;
                                            }
                                        }
                                    }
                                }
                                var quyettoanduanhoanthanhtheoduan = _quyetToanDuAnService.GetHoanThanhByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduanhoanthanhtheoduan)
                                {
                                    if (item1.NgayPheDuyetQuyetToan.Value.Day <= checkday && item1.NgayPheDuyetQuyetToan.Value.Month <= checkmonth && item1.NgayPheDuyetQuyetToan.Value.Year <= checkyear)
                                    {
                                        if (item1.TongGiaTri == null)
                                        {
                                            item1.TongGiaTri = 0;
                                        }
                                        giatridaquyettoannhomb += (double)item1.TongGiaTri;
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm C")
                            {
                                nhomcindex++;
                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                {
                                    if (tongmucdautu.TongGiaTri == null)
                                    {
                                        tongmucdautu.TongGiaTri = 0;
                                    }
                                    tongmucdautunhomc += (double)tongmucdautu.TongGiaTri;
                                }
                                else
                                {
                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                    {
                                        if (tongmucdautu2.TongGiaTri == null)
                                        {
                                            tongmucdautu2.TongGiaTri = 0;
                                        }
                                        tongmucdautunhomc += (double)tongmucdautu2.TongGiaTri;
                                    }
                                }
                                var hopdong = _hopDongService.GetByIdDuAn(item.IdDuAn);
                                foreach (var item1 in hopdong)
                                {
                                    var thanhtoanhopdong = _thanhToanHopDongService.GetByIdHopDong(item1.IdHopDong);
                                    foreach (var item2 in thanhtoanhopdong)
                                    {
                                        if (item2.ThoiDiemThanhToan != null)
                                        {
                                            if (item2.ThoiDiemThanhToan.Value.Day <= checkday && item2.ThoiDiemThanhToan.Value.Month <= checkmonth && item2.ThoiDiemThanhToan.Value.Year <= checkyear)
                                            {
                                                if (item2.DeNghiThanhToan == null)
                                                {
                                                    item2.DeNghiThanhToan = 0;
                                                }
                                                denghiquyettoannhomc += (double)item2.DeNghiThanhToan;
                                            }
                                        }
                                    }
                                }
                                var quyettoanduanhoanthanhtheoduan = _quyetToanDuAnService.GetHoanThanhByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduanhoanthanhtheoduan)
                                {
                                    if (item1.NgayPheDuyetQuyetToan.Value.Day <= checkday && item1.NgayPheDuyetQuyetToan.Value.Month <= checkmonth && item1.NgayPheDuyetQuyetToan.Value.Year <= checkyear)
                                    {
                                        if (item1.TongGiaTri == null)
                                        {
                                            item1.TongGiaTri = 0;
                                        }
                                        giatridaquyettoannhomc += (double)item1.TongGiaTri;
                                    }
                                }
                            }
                        }
                        sheet.Cells[12, 3].Value = nhomaindex;
                        sheet.Cells[13, 3].Value = nhombindex;
                        sheet.Cells[14, 3].Value = nhomcindex;
                        sheet.Cells["E12"].Value = tongmucdautunhoma/dvt;
                        sheet.Cells["E13"].Value = tongmucdautunhomb/dvt;
                        sheet.Cells["E14"].Value = tongmucdautunhomc / dvt;
                        sheet.Cells["C11"].Value = nhomaindex + nhombindex + nhomcindex;
                        sheet.Cells["E11"].Value = (tongmucdautunhoma + tongmucdautunhomb + tongmucdautunhomc) / dvt;
                        sheet.Cells["G12"].Value = denghiquyettoannhoma / dvt;
                        sheet.Cells["G13"].Value = denghiquyettoannhomb / dvt;
                        sheet.Cells["G14"].Value = denghiquyettoannhomc / dvt;
                        sheet.Cells["G11"].Value = (denghiquyettoannhoma + denghiquyettoannhomb + denghiquyettoannhomc) / dvt;
                        sheet.Cells["I12"].Value = giatridaquyettoannhoma / dvt;
                        sheet.Cells["I13"].Value = giatridaquyettoannhomb / dvt;
                        sheet.Cells["I14"].Value = giatridaquyettoannhomc / dvt;
                        sheet.Cells["I11"].Value = (giatridaquyettoannhoma + giatridaquyettoannhomb + giatridaquyettoannhomc) / dvt;
                        sheet.Cells["K11"].Value = (giatridaquyettoannhoma + giatridaquyettoannhomb + giatridaquyettoannhomc - (denghiquyettoannhoma + denghiquyettoannhomb + denghiquyettoannhomc)) / dvt;
                        sheet.Cells["K12"].Value = (giatridaquyettoannhoma - denghiquyettoannhoma) / dvt;
                        sheet.Cells["K13"].Value = (giatridaquyettoannhomb - denghiquyettoannhomb) / dvt;
                        sheet.Cells["K14"].Value = (giatridaquyettoannhomc - denghiquyettoannhomc) / dvt;

                        //lay tat ca cac du an chua duoc quyet toan theo loaibaocao va nam truyen vao
                        //theo quyet toan 
                        var duandanophosoquyettoans = _duAnService.GetByLoaiBaoCaoByYearByQuyetToanNhanHoSo(szDonViBaoCao, szLoaiBaoCao, iNamPD);
                        //du an da nop ho so quyet toan
                        int indexduandanophosoqtnhomaloai1 = 0;
                        int indexduandanophosoqtnhomaloai2 = 0;
                        int indexduandanophosoqtnhomaloai3 = 0;
                        int indexduandanophosoqtnhombloai1 = 0;
                        int indexduandanophosoqtnhombloai2 = 0;
                        int indexduandanophosoqtnhombloai3 = 0;
                        int indexduandanophosoqtnhomcloai1 = 0;
                        int indexduandanophosoqtnhomcloai2 = 0;
                        int indexduandanophosoqtnhomcloai3 = 0;
                        double tongmucdaudanophosoqttunhomaloai1 = 0;
                        double tongmucdaudanophosoqttunhomaloai2 = 0;
                        double tongmucdaudanophosoqttunhomaloai3 = 0;
                        double tongmucdaudanophosoqttunhombloai1 = 0;
                        double tongmucdaudanophosoqttunhombloai2 = 0;
                        double tongmucdaudanophosoqttunhombloai3 = 0;
                        double tongmucdaudanophosoqttunhomcloai1 = 0;
                        double tongmucdaudanophosoqttunhomcloai2 = 0;
                        double tongmucdaudanophosoqttunhomcloai3 = 0;
                        double tongdenghiqtdanophosonhomaloai1 = 0;
                        double tongdenghiqtdanophosonhomaloai2 = 0;
                        double tongdenghiqtdanophosonhomaloai3 = 0;
                        double tongdenghiqtdanophosonhombloai1 = 0;
                        double tongdenghiqtdanophosonhombloai2 = 0;
                        double tongdenghiqtdanophosonhombloai3 = 0;
                        double tongdenghiqtdanophosonhomcloai1 = 0;
                        double tongdenghiqtdanophosonhomcloai2 = 0;
                        double tongdenghiqtdanophosonhomcloai3 = 0;
                        foreach (var item in duandanophosoquyettoans)
                        {
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm A")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduannophosotheoduan = _quyetToanDuAnService.GetNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduannophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(33).AddDays(1) && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Year <=checkyear)
                                        {
                                            indexduandanophosoqtnhomaloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhomaloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhomaloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhomaloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(33) && item1.NgayKyBienBan?.AddMonths(33).Day <= checkday && item1.NgayKyBienBan?.AddMonths(33).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(33).Year <= checkyear)
                                        {
                                            indexduandanophosoqtnhomaloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhomaloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                    {
                                                        if (tongmucdautu2.TongGiaTri == null)
                                                        {
                                                            tongmucdautu2.TongGiaTri = 0;
                                                        }
                                                        tongmucdaudanophosoqttunhomaloai2 += (double)tongmucdautu2.TongGiaTri;
                                                    }
                                                
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhomaloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                                indexduandanophosoqtnhomaloai1++;
                                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu.TongGiaTri == null)
                                                    {
                                                        tongmucdautu.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhomaloai1 += (double)tongmucdautu.TongGiaTri;
                                                }
                                                else
                                                {
                                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                    {
                                                        if (tongmucdautu2.TongGiaTri == null)
                                                        {
                                                            tongmucdautu2.TongGiaTri = 0;
                                                        }
                                                        tongmucdaudanophosoqttunhomaloai1 += (double)tongmucdautu2.TongGiaTri;
                                                    }
                                                }

                                                if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                                {
                                                    if (item1.TongGiaTri == null)
                                                    {
                                                        item1.TongGiaTri = 0;
                                                    }
                                                    tongdenghiqtdanophosonhomaloai1 += (double)item1.TongGiaTri;
                                                }
                                            
                                        }
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm B")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduannophosotheoduan = _quyetToanDuAnService.GetNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduannophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(30).AddDays(1) && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Year <= checkyear)
                                        {
                                            indexduandanophosoqtnhombloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhombloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhombloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhombloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(30) && item1.NgayKyBienBan?.AddMonths(30).Day <= checkday && item1.NgayKyBienBan?.AddMonths(30).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(30).Year <= checkyear)
                                        {
                                            indexduandanophosoqtnhombloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhombloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhombloai2 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhombloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                                indexduandanophosoqtnhombloai1++;
                                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu.TongGiaTri == null)
                                                    {
                                                        tongmucdautu.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhombloai1 += (double)tongmucdautu.TongGiaTri;
                                                }
                                                else
                                                {
                                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                    {
                                                        if (tongmucdautu2.TongGiaTri == null)
                                                        {
                                                            tongmucdautu2.TongGiaTri = 0;
                                                        }
                                                        tongmucdaudanophosoqttunhombloai1 += (double)tongmucdautu2.TongGiaTri;
                                                    }
                                                }

                                                if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                                {
                                                    if (item1.TongGiaTri == null)
                                                    {
                                                        item1.TongGiaTri = 0;
                                                    }
                                                    tongdenghiqtdanophosonhombloai1 += (double)item1.TongGiaTri;
                                                }
                                            
                                        }
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm C")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduannophosotheoduan = _quyetToanDuAnService.GetNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduannophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(27).AddDays(1) && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Year <= checkyear)
                                        {
                                            indexduandanophosoqtnhomcloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhomcloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhomcloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhomcloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(27) && item1.NgayKyBienBan?.AddMonths(27).Day <= checkday && item1.NgayKyBienBan?.AddMonths(27).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(27).Year <= checkyear)
                                        {
                                            indexduandanophosoqtnhomcloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdaudanophosoqttunhomcloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhomcloai2 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtdanophosonhomcloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                                indexduandanophosoqtnhomcloai1++;
                                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu.TongGiaTri == null)
                                                    {
                                                        tongmucdautu.TongGiaTri = 0;
                                                    }
                                                    tongmucdaudanophosoqttunhomcloai1 += (double)tongmucdautu.TongGiaTri;
                                                }
                                                else
                                                {
                                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                    {
                                                        if (tongmucdautu2.TongGiaTri == null)
                                                        {
                                                            tongmucdautu2.TongGiaTri = 0;
                                                        }
                                                        tongmucdaudanophosoqttunhomcloai1 += (double)tongmucdautu2.TongGiaTri;
                                                    }
                                                }

                                                if (item1.NgayNhanDuHoSo.Value.Day <= checkday && item1.NgayNhanDuHoSo.Value.Month <= checkmonth && item1.NgayNhanDuHoSo.Value.Year <= checkyear)
                                                {
                                                    if (item1.TongGiaTri == null)
                                                    {
                                                        item1.TongGiaTri = 0;
                                                    }
                                                    tongdenghiqtdanophosonhomcloai1 += (double)item1.TongGiaTri;
                                                }
                                            
                                            
                                        }
                                    }
                                }
                            }                            
                        }
                        //
                        sheet.Cells["C23"].Value = indexduandanophosoqtnhomaloai1;
                        sheet.Cells["C24"].Value = indexduandanophosoqtnhomaloai2;
                        sheet.Cells["C25"].Value = indexduandanophosoqtnhomaloai3;
                        sheet.Cells["C22"].Value = indexduandanophosoqtnhomaloai1 + indexduandanophosoqtnhomaloai2 + indexduandanophosoqtnhomaloai3;
                        sheet.Cells["F23"].Value = indexduandanophosoqtnhombloai1;
                        sheet.Cells["F24"].Value = indexduandanophosoqtnhombloai2;
                        sheet.Cells["F25"].Value = indexduandanophosoqtnhombloai3;
                        sheet.Cells["F22"].Value = indexduandanophosoqtnhombloai1 + indexduandanophosoqtnhombloai2 + indexduandanophosoqtnhombloai3;
                        sheet.Cells["I23"].Value = indexduandanophosoqtnhomcloai1;
                        sheet.Cells["I24"].Value = indexduandanophosoqtnhomcloai2;
                        sheet.Cells["I25"].Value = indexduandanophosoqtnhomcloai3;
                        sheet.Cells["I22"].Value = indexduandanophosoqtnhomcloai1 + indexduandanophosoqtnhomcloai2 + indexduandanophosoqtnhomcloai3;
                        sheet.Cells["D23"].Value = tongmucdaudanophosoqttunhomaloai1 / dvt;
                        sheet.Cells["D24"].Value = tongmucdaudanophosoqttunhomaloai2 / dvt;
                        sheet.Cells["D25"].Value = tongmucdaudanophosoqttunhomaloai3 / dvt;
                        sheet.Cells["D22"].Value = (tongmucdaudanophosoqttunhomaloai1 + tongmucdaudanophosoqttunhomaloai2 + tongmucdaudanophosoqttunhomaloai3) / dvt;
                        sheet.Cells["G23"].Value = tongmucdaudanophosoqttunhombloai1 / dvt;
                        sheet.Cells["G24"].Value = tongmucdaudanophosoqttunhombloai2 / dvt;
                        sheet.Cells["G25"].Value = tongmucdaudanophosoqttunhombloai3 / dvt;
                        sheet.Cells["G22"].Value = (tongmucdaudanophosoqttunhombloai1 + tongmucdaudanophosoqttunhombloai2 + tongmucdaudanophosoqttunhombloai3) / dvt;
                        sheet.Cells["J23"].Value = tongmucdaudanophosoqttunhomcloai1 / dvt;
                        sheet.Cells["J24"].Value = tongmucdaudanophosoqttunhomcloai2 / dvt;
                        sheet.Cells["J25"].Value = tongmucdaudanophosoqttunhomcloai3 / dvt;
                        sheet.Cells["J22"].Value = (tongmucdaudanophosoqttunhomcloai1 + tongmucdaudanophosoqttunhomcloai2 + tongmucdaudanophosoqttunhomcloai3) / dvt;
                        sheet.Cells["E23"].Value = tongdenghiqtdanophosonhomaloai1 / dvt;
                        sheet.Cells["E24"].Value = tongdenghiqtdanophosonhomaloai2 / dvt;
                        sheet.Cells["E25"].Value = tongdenghiqtdanophosonhomaloai3 / dvt;
                        sheet.Cells["E22"].Value = (tongdenghiqtdanophosonhomaloai1 + tongdenghiqtdanophosonhomaloai2 + tongdenghiqtdanophosonhomaloai3) / dvt;
                        sheet.Cells["H23"].Value = tongdenghiqtdanophosonhombloai1 / dvt;
                        sheet.Cells["H24"].Value = tongdenghiqtdanophosonhombloai2 / dvt;
                        sheet.Cells["H25"].Value = tongdenghiqtdanophosonhombloai3 / dvt;
                        sheet.Cells["H22"].Value = (tongdenghiqtdanophosonhombloai1 + tongdenghiqtdanophosonhombloai2 + tongdenghiqtdanophosonhombloai3) / dvt;
                        sheet.Cells["K23"].Value = tongdenghiqtdanophosonhomcloai1 / dvt;
                        sheet.Cells["K24"].Value = tongdenghiqtdanophosonhomcloai2 / dvt;
                        sheet.Cells["K25"].Value = tongdenghiqtdanophosonhomcloai1 / dvt;
                        sheet.Cells["K22"].Value = (tongdenghiqtdanophosonhomcloai1 + tongdenghiqtdanophosonhomcloai2 + tongdenghiqtdanophosonhomcloai3) / dvt;
                        var duanchuanophosoquyettoans = _duAnService.GetByLoaiBaoCaoByYearByQuyetToanChuaNhanHoSo(szDonViBaoCao, szLoaiBaoCao, iNamPD);
                        //du an da nop ho so quyet toan
                        int indexduanchuanophosoqtnhomaloai1 = 0;
                        int indexduanchuanophosoqtnhomaloai2 = 0;
                        int indexduanchuanophosoqtnhomaloai3 = 0;
                        int indexduanchuanophosoqtnhombloai1 = 0;
                        int indexduanchuanophosoqtnhombloai2 = 0;
                        int indexduanchuanophosoqtnhombloai3 = 0;
                        int indexduanchuanophosoqtnhomcloai1 = 0;
                        int indexduanchuanophosoqtnhomcloai2 = 0;
                        int indexduanchuanophosoqtnhomcloai3 = 0;
                        double tongmucdauchuanophosoqttunhomaloai1 = 0;
                        double tongmucdauchuanophosoqttunhomaloai2 = 0;
                        double tongmucdauchuanophosoqttunhomaloai3 = 0;
                        double tongmucdauchuanophosoqttunhombloai1 = 0;
                        double tongmucdauchuanophosoqttunhombloai2 = 0;
                        double tongmucdauchuanophosoqttunhombloai3 = 0;
                        double tongmucdauchuanophosoqttunhomcloai1 = 0;
                        double tongmucdauchuanophosoqttunhomcloai2 = 0;
                        double tongmucdauchuanophosoqttunhomcloai3 = 0;
                        double tongdenghiqtchuanophosonhomaloai1 = 0;
                        double tongdenghiqtchuanophosonhomaloai2 = 0;
                        double tongdenghiqtchuanophosonhomaloai3 = 0;
                        double tongdenghiqtchuanophosonhombloai1 = 0;
                        double tongdenghiqtchuanophosonhombloai2 = 0;
                        double tongdenghiqtchuanophosonhombloai3 = 0;
                        double tongdenghiqtchuanophosonhomcloai1 = 0;
                        double tongdenghiqtchuanophosonhomcloai2 = 0;
                        double tongdenghiqtchuanophosonhomcloai3 = 0;
                        foreach (var item in duanchuanophosoquyettoans)
                        {
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm A")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduanchuanophosotheoduan = _quyetToanDuAnService.GetChuaNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduanchuanophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(33).AddDays(1) && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(33).AddDays(1).Year<=checkyear)
                                        {
                                            indexduanchuanophosoqtnhomaloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhomaloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomaloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhomaloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(33) && item1.NgayKyBienBan?.AddMonths(33).Day <= checkday && item1.NgayKyBienBan?.AddMonths(33).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(33).Year <= checkyear)
                                        {
                                            indexduanchuanophosoqtnhomaloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhomaloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomaloai2 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhomaloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                                indexduanchuanophosoqtnhomaloai1++;
                                                var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu.TongGiaTri == null)
                                                    {
                                                        tongmucdautu.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomaloai1 += (double)tongmucdautu.TongGiaTri;
                                                }
                                                else
                                                {
                                                    var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                    if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                    {
                                                        if (tongmucdautu2.TongGiaTri == null)
                                                        {
                                                            tongmucdautu2.TongGiaTri = 0;
                                                        }
                                                        tongmucdauchuanophosoqttunhomaloai1 += (double)tongmucdautu2.TongGiaTri;
                                                    }
                                                }

                                                if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                                {
                                                    if (item1.TongGiaTri == null)
                                                    {
                                                        item1.TongGiaTri = 0;
                                                    }
                                                    tongdenghiqtchuanophosonhomaloai1 += (double)item1.TongGiaTri;
                                                }
                                            
                                        }
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm B")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduanchuanophosotheoduan = _quyetToanDuAnService.GetChuaNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduanchuanophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(30).AddDays(1) && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(30).AddDays(1).Year <= checkyear)
                                        {
                                            indexduanchuanophosoqtnhombloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhombloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhombloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhombloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(30) && item1.NgayKyBienBan?.AddMonths(30).Day <= checkday && item1.NgayKyBienBan?.AddMonths(30).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(30).Year <= checkyear)
                                        {
                                            indexduanchuanophosoqtnhombloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhombloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhombloai2 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhombloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                            indexduanchuanophosoqtnhombloai1++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhombloai1 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhombloai1 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhombloai1 += (double)item1.TongGiaTri;
                                            }
                                        }
                                    }
                                }
                            }
                            if (item.NhomDuAn?.TenNhomDuAn == "Nhóm C")
                            {
                                //xet du an trong thoi gian tham tra
                                var quyettoanduanchuanophosotheoduan = _quyetToanDuAnService.GetChuaNhanDuHoSoByIdDuAn(item.IdDuAn);
                                foreach (var item1 in quyettoanduanchuanophosotheoduan)
                                {
                                    if (item1.NgayTrinh != null && item1.NgayKyBienBan != null)
                                    {
                                        if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(27).AddDays(1) && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Day <= checkday && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(27).AddDays(1).Year <= checkyear)
                                        {
                                            indexduanchuanophosoqtnhomcloai3++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhomcloai3 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomcloai3 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhomcloai3 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else if (item1.NgayTrinh > item1.NgayKyBienBan?.AddMonths(27) && item1.NgayKyBienBan?.AddMonths(27).Day <= checkday && item1.NgayKyBienBan?.AddMonths(27).Month <= checkmonth && item1.NgayKyBienBan?.AddMonths(27).Year <= checkyear)
                                        {
                                            indexduanchuanophosoqtnhomcloai2++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhomcloai2 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomcloai2 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhomcloai2 += (double)item1.TongGiaTri;
                                            }
                                        }
                                        else
                                        {
                                            indexduanchuanophosoqtnhomcloai1++;
                                            var tongmucdautu = _lapQuanLyDauTuService.GetLastTheoDieuChinhTheoDuAn(item.IdDuAn);
                                            if (tongmucdautu != null && tongmucdautu.NgayPheDuyet.Value.Day <= checkday && tongmucdautu.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu.NgayPheDuyet.Value.Year <= checkyear)
                                            {
                                                if (tongmucdautu.TongGiaTri == null)
                                                {
                                                    tongmucdautu.TongGiaTri = 0;
                                                }
                                                tongmucdauchuanophosoqttunhomcloai1 += (double)tongmucdautu.TongGiaTri;
                                            }
                                            else
                                            {
                                                var tongmucdautu2 = _lapQuanLyDauTuService.GetNewestTheoDuAn(item.IdDuAn);
                                                if (tongmucdautu2.NgayPheDuyet != null && tongmucdautu2.NgayPheDuyet.Value.Day <= checkday && tongmucdautu2.NgayPheDuyet.Value.Month <= checkmonth && tongmucdautu2.NgayPheDuyet.Value.Year <= checkyear)
                                                {
                                                    if (tongmucdautu2.TongGiaTri == null)
                                                    {
                                                        tongmucdautu2.TongGiaTri = 0;
                                                    }
                                                    tongmucdauchuanophosoqttunhomcloai1 += (double)tongmucdautu2.TongGiaTri;
                                                }
                                            }

                                            if (item1.NgayKyBienBan.Value.Day <= checkday && item1.NgayKyBienBan.Value.Month <= checkmonth && item1.NgayKyBienBan.Value.Year <= checkyear)
                                            {
                                                if (item1.TongGiaTri == null)
                                                {
                                                    item1.TongGiaTri = 0;
                                                }
                                                tongdenghiqtchuanophosonhomcloai1 += (double)item1.TongGiaTri;
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                        sheet.Cells["C33"].Value = indexduanchuanophosoqtnhomaloai1;
                        sheet.Cells["C34"].Value = indexduanchuanophosoqtnhomaloai2;
                        sheet.Cells["C35"].Value = indexduanchuanophosoqtnhomaloai3;
                        sheet.Cells["C32"].Value = indexduanchuanophosoqtnhomaloai1 + indexduanchuanophosoqtnhomaloai2 + indexduanchuanophosoqtnhomaloai3;
                        sheet.Cells["F33"].Value = indexduanchuanophosoqtnhombloai1;
                        sheet.Cells["F34"].Value = indexduanchuanophosoqtnhombloai2;
                        sheet.Cells["F35"].Value = indexduanchuanophosoqtnhombloai3;
                        sheet.Cells["F32"].Value = indexduanchuanophosoqtnhombloai1 + indexduanchuanophosoqtnhombloai2 + indexduanchuanophosoqtnhombloai3;
                        sheet.Cells["I33"].Value = indexduanchuanophosoqtnhomcloai1;
                        sheet.Cells["I34"].Value = indexduanchuanophosoqtnhomcloai2;
                        sheet.Cells["I35"].Value = indexduanchuanophosoqtnhomcloai3;
                        sheet.Cells["I32"].Value = indexduanchuanophosoqtnhomcloai1 + indexduanchuanophosoqtnhomcloai2 + indexduanchuanophosoqtnhomcloai3;
                        sheet.Cells["D33"].Value = tongmucdauchuanophosoqttunhomaloai1 / dvt;
                        sheet.Cells["D34"].Value = tongmucdauchuanophosoqttunhomaloai2 / dvt;
                        sheet.Cells["D35"].Value = tongmucdauchuanophosoqttunhomaloai3 / dvt;
                        sheet.Cells["D32"].Value = (tongmucdauchuanophosoqttunhomaloai1 + tongmucdauchuanophosoqttunhomaloai2 + tongmucdauchuanophosoqttunhomaloai3) / dvt;
                        sheet.Cells["G33"].Value = tongmucdauchuanophosoqttunhombloai1 / dvt;
                        sheet.Cells["G34"].Value = tongmucdauchuanophosoqttunhombloai2 / dvt;
                        sheet.Cells["G35"].Value = tongmucdauchuanophosoqttunhombloai3 / dvt;
                        sheet.Cells["G32"].Value = (tongmucdauchuanophosoqttunhombloai1 + tongmucdauchuanophosoqttunhombloai2 + tongmucdauchuanophosoqttunhombloai3) / dvt;
                        sheet.Cells["J33"].Value = tongmucdauchuanophosoqttunhomcloai1 / dvt;
                        sheet.Cells["J34"].Value = tongmucdauchuanophosoqttunhomcloai2 / dvt;
                        sheet.Cells["J35"].Value = tongmucdauchuanophosoqttunhomcloai3 / dvt;
                        sheet.Cells["J32"].Value = (tongmucdauchuanophosoqttunhomcloai1 + tongmucdauchuanophosoqttunhomcloai2 + tongmucdauchuanophosoqttunhomcloai3) / dvt;
                        sheet.Cells["E33"].Value = tongdenghiqtchuanophosonhomaloai1 / dvt;
                        sheet.Cells["E34"].Value = tongdenghiqtchuanophosonhomaloai2 / dvt;
                        sheet.Cells["E35"].Value = tongdenghiqtchuanophosonhomaloai3 / dvt;
                        sheet.Cells["E32"].Value = (tongdenghiqtchuanophosonhomaloai1 + tongdenghiqtchuanophosonhomaloai2 + tongdenghiqtchuanophosonhomaloai3) / dvt;
                        sheet.Cells["H33"].Value = tongdenghiqtchuanophosonhombloai1 / dvt;
                        sheet.Cells["H34"].Value = tongdenghiqtchuanophosonhombloai2 / dvt;
                        sheet.Cells["H35"].Value = tongdenghiqtchuanophosonhombloai3 / dvt;
                        sheet.Cells["H32"].Value = (tongdenghiqtchuanophosonhombloai1 + tongdenghiqtchuanophosonhombloai2 + tongdenghiqtchuanophosonhombloai3) / dvt;
                        sheet.Cells["K33"].Value = tongdenghiqtchuanophosonhomcloai1 / dvt;
                        sheet.Cells["K34"].Value = tongdenghiqtchuanophosonhomcloai2 / dvt;
                        sheet.Cells["K35"].Value = tongdenghiqtchuanophosonhomcloai1 / dvt;
                        sheet.Cells["K32"].Value = (tongdenghiqtchuanophosonhomcloai1 + tongdenghiqtchuanophosonhomcloai2 + tongdenghiqtchuanophosonhomcloai3) / dvt;
                        //
                        if (szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "6 tháng đầu năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }
                        if (szLoaiBaoCao == "Cả năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "Cả năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }

                        string dvTinh = sheet.Cells["J8"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["J8"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 11/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
