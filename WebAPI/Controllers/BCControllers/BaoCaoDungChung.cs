﻿using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebAPI.Controllers.BCControllers
{
    public class BaoCaoDungChung
    {
        public static int GetdDim(string szDonViTinh)
        {
            int dDim = 1;
            if (szDonViTinh == "Nghìn Đồng")
            {
                dDim = 1000;
            }
            else if (szDonViTinh == "Triệu Đồng")
            {
                dDim = 1000000;
            }
            else if (szDonViTinh == "Tỷ Đồng")
            {
                dDim = 1000000000;
            }
            else
            {
                dDim = 1;
            }

            return dDim;
        }

        public static int GetdDimKLNT(string szDonViTinh)
        {
            int dDim = 1;
            if (szDonViTinh == "Nghìn Đồng")
            {
                dDim = 1 / 1000;
            }
            else if (szDonViTinh == "Triệu Đồng")
            {
                dDim = 1;
            }
            else if (szDonViTinh == "Tỷ Đồng")
            {
                dDim = 1000;
            }
            else
            {
                dDim = 1 / 1000000;
            }

            return dDim;
        }

        public static object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }

        public static string GetParseDate(DateTime dt)
        {
            string sz = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            return sz;
        }

        public static string GetDateValue(string sz)
        {
            DateTime dt;

            if (sz != "")
            {
                dt = Convert.ToDateTime(sz);
                sz = GetParseDate(dt);
            }
            else
            {
                sz = "";
            }
            return sz;
        }

        public static string So_Ngay(string so, string ngay)
        {
            string sz;
            if (so != "")
            {
                sz = so + ";" + ngay;
            }
            else
            {
                sz = ngay;
            }
            return sz;
        }

        public static void Tang_Giam_KHV(ExcelWorksheet sheet, int row, int[] arColInt, string[] arColString)
        {
            sheet.Cells[row, arColInt[1]].Formula = "=IF((" + arColString[0] + row + " - " + arColString[1] + row + ") >= 0," + arColString[0] + row + " - " + arColString[1] + row + "," + "\"\"" + ")";
            sheet.Cells[row, arColInt[0]].Formula = "=IF((" + arColString[0] + row + " - " + arColString[1] + row + ") >= 0," + "\"\"" + ", ABS(" + arColString[0] + row + " - " + arColString[1] + row + "))";
        }

        public static void In_Bool_value(ExcelWorksheet sheet, int row, int col, bool bol)
        {
            if (bol == true)
            {
                sheet.Cells[row, col].Value = 1;
            }
        }

        public static void In_Bool_Tong_value(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, bool bol, string sz)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    bol = (bool)GetValueObject(obj, sz);
                    if (col[0] != 0)
                    {
                        if (bol == true)
                        {
                            sheet.Cells[row, col[1]].Value = 1;
                        }
                        else
                        {
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                    }
                    else
                    {
                        if (bol == true)
                        {
                            sheet.Cells[row, col[1]].Value = 1;
                        }
                    }
                }
            }
        }

        public static void In_SoNgayPD_TGT(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, string sz1, string sz2, string[] szAr, double dGiaTri, int dDim)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)GetValueObject(obj, szAr[0]);
                    sz2 = (string)GetValueObject(obj, szAr[1]);
                    dGiaTri = (double)GetValueObject(obj, szAr[2]) / dDim;

                    if (sz2 != "")
                    {
                        sz2 = GetDateValue(sz2);
                    }

                    if (sz1 != "" || sz2 != "")
                    {
                        if (col[0] != 0)
                        {
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                    }
                    sheet.Cells[row, col[1]].Value = So_Ngay(sz1, sz2);
                    sheet.Cells[row, col[2]].Value = dGiaTri;
                }
            }
        }

        public static void In_Chuoi(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, string[] arSz1, string[] arSz2, int a)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    for (int i = 0; i < arSz1.Count(); i++)
                    {
                        arSz1[i] = (string)GetValueObject(obj, arSz2[i]);
                        sheet.Cells[row, col[i]].Value = arSz1[i];
                    }
                    if (a != 0)
                    {
                        sheet.Cells[row, col[0] - 1].Value = 1;
                    }
                }
            }
        }

        public static void In_So_Ngay_Value(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, string sz1, string sz2)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)GetValueObject(obj, "str1");
                    sz2 = (string)GetValueObject(obj, "str2");
                    if (sz2 != "")
                    {
                        sz2 = GetDateValue(sz2);
                    }
                    sheet.Cells[row, col[1]].Value = So_Ngay(sz1, sz2);
                    if (sz1 != "")
                    {
                        if (col[0] != 0)
                        {
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                    }
                }
            }
        }

        public static void In_Date_Value(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, string sz1, string sz)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)GetValueObject(obj, sz);
                    if (sz1 != "")
                    {
                        if (col[0] != 0)
                        {
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                        sheet.Cells[row, col[1]].Value = GetDateValue(sz1);
                    }
                }
            }
        }

        public static void In_GiaTri_Value(Dictionary<int, List<object>> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, double d, string sz, int dDim)
        {
            if (dic.ContainsKey(IDDA))
            {
                lst = dic[IDDA];
                foreach (object obj in lst)
                {
                    d = (double)GetValueObject(obj, sz) / dDim;
                    sheet.Cells[row, col[1]].Value = d;
                    if (d != 0)
                    {
                        if (col[0] != 0)
                        {
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                    }
                }
            }
            else
            {
                sheet.Cells[row, col[1]].Value = 0;
            }
        }

        public static void In_GiaTri_UyBan_Value(Dictionary<int, double> dic, int IDDA, List<object> lst, ExcelWorksheet sheet, int row, int[] col, double d, int dDim)
        {
            if (dic.ContainsKey(IDDA))
            {
                d = dic[IDDA] / dDim;
                sheet.Cells[row, col[1]].Value = d;
                if (d != 0)
                {
                    if (col[0] != 0)
                    {
                        sheet.Cells[row, col[0]].Value = 1;
                    }
                }
            }
            else
            {
                sheet.Cells[row, col[1]].Value = 0;
            }
        }

        public static void In_Dic_String(Dictionary<int, string> dic, int IDDA, ExcelWorksheet sheet, int row, int[] col, string sz1, int a)
        {
            if (dic.ContainsKey(IDDA))
            {
                sz1 = dic[IDDA];
                sheet.Cells[row, col[1]].Value = sz1;
                if (a != 0)
                {
                    if (sz1 != "")
                    {
                        if(col[0]!= 0)
                        { 
                            sheet.Cells[row, col[0]].Value = 1;
                        }
                    }
                }
            }
        }

        public static void In_Style(ExcelWorksheet sheet, int row, int[] col, int loai)
        {
            foreach (var item in col)
            {
                switch (loai)
                {
                    //Số, ngày
                    case 1:
                        sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        break;
                    //Text
                    case 2:
                        sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        break;
                    //Tiền
                    case 3:
                        sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[row, item].Style.Numberformat.Format = "#,##0";
                        break;
                    //Phần trăm
                    default:
                        sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[row, item].Style.Numberformat.Format = "0.00";
                        break;

                }
                    
                ////Số, ngày
                //if (loai == 1)
                //{
                //    sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //}
                ////Text
                //else if (loai == 2)
                //{
                //    sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                //}
                ////Tiền
                //else if (loai == 3)
                //{
                //    sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                //    sheet.Cells[row, item].Style.Numberformat.Format = "#,##0";
                //}
                //else
                //{
                //    sheet.Cells[row, item].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                //    sheet.Cells[row, item].Style.Numberformat.Format = "0.00";
                //}
            }
        }

        public static void In_Nhom_Du_An(ExcelWorksheet sheet, int row, string NhomDuAn,int[] arCol)
        {
            if (NhomDuAn == "Nhóm A")
            {
                sheet.Cells[row, arCol[0]].Value = 1;
            }
            else if (NhomDuAn == "Nhóm B")
            {
                sheet.Cells[row, arCol[1]].Value = 1;
            }
            else if (NhomDuAn == "Nhóm C")
            {
                sheet.Cells[row, arCol[2]].Value = 1;
            }
        }

        public static void In_Chuoi_Thong_Tin_Du_An(ExcelWorksheet sheet, int row, int[] ar_i, string[] ar_Sz)
        {
            for(int i = 0; i < ar_i.Count(); i++)
            { 
                sheet.Cells[row, ar_i[i]].Value = ar_Sz[i];
            }
        }

        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public static void In_TDC(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KHTDC> lst_TDC, int IDDA, string sz, int iCot)
        {
            sz = "";

            if (lst_TDC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz = lst_TDC.Where(x => x.IDDA == IDDA).Select(x => x.NoiDung).FirstOrDefault();
                sheet.Cells[rowDA, iCot].Value = sz;
            }
        }

        public static void In_KKGP(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_Uyban_PL1_KKGP> lstKKVM, int IDDA, string sz1, string sz2, int[] ar_i)
        {
            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, ar_i[0]].Value = 1;
                sz1 = "";
                sz2 = "";

                var str = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.KK).FirstOrDefault();
                if (str != null)
                {
                    foreach (var item in str)
                    {
                        sz1 += item + "; ";
                    }
                    sheet.Cells[rowDA, ar_i[1]].Value = sz1;
                }

                var str2 = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.GP).FirstOrDefault();
                if (str2 != null)
                {
                    foreach (var item2 in str2)
                    {
                        sz2 += item2 + "; ";
                    }
                    sheet.Cells[rowDA, ar_i[2]].Value = sz2;
                }
            }
        }

        public static void In_KKGP_QT(ExcelWorksheet sheet, int rowDA, IEnumerable<BC_TienDo_KK_QT> lstKKVM, int IDDA, string sz1, int iCot)
        {
            if (lstKKVM.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = "";
                IEnumerable<BC_TienDo_KK_QT_NoiDung> lst = lstKKVM.Where(x => x.IDDA == IDDA).Select(x => x.grpKKVM).FirstOrDefault();
                foreach (var item in lst)
                {
                    sz1 += item.KKVM;
                }
                sheet.Cells[rowDA, iCot].Value = sz1;
            }
        }
    }
}