﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan05")]
    public class BaoCaoQuyetToan05Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IHopDongService _hopDongService;
        private IQuyetToanHopDongService _quyetToanHopDongService;
        private INguonVonThanhToanChiPhiKhacService _nguonVonThanhToanChiPhiKhacService;
        private IQuyetToanDuAnService _quyetToanDuAnService;
        private INguonVonService _nguonVonService;
        public BaoCaoQuyetToan05Controller(INguonVonService nguonVonService, IQuyetToanDuAnService quyetToanDuAnService, INguonVonThanhToanChiPhiKhacService nguonVonThanhToanChiPhiKhacService, IQuyetToanHopDongService quyetToanHopDongService, IHopDongService hopDongService, IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._hopDongService = hopDongService;
            this._quyetToanHopDongService = quyetToanHopDongService;
            this._nguonVonThanhToanChiPhiKhacService = nguonVonThanhToanChiPhiKhacService;
            this._quyetToanDuAnService = quyetToanDuAnService;
            this._nguonVonService = nguonVonService;
        }
        #region Mau05
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau05")]
        [HttpGet]
        public HttpResponseMessage ExportMau05(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau05(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau05(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-05-MauPhuLuc5.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc5-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // lay ra thong tin du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = duan.TenDuAn;
                        sheet.Cells["A2"].Style.WrapText = true;
                        string dvTinh = sheet.Cells["H3"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["H3"].Value = dvTinh;
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //lay ra hang
                        int rowIndex = 7;
                        sheet.InsertRow(rowIndex, 1);
                        sheet.Cells[rowIndex, 1].Value = 1;
                        sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells[rowIndex, 2].Value = duan.TenDuAn;
                        sheet.Cells[rowIndex, 2].Style.WrapText = true;
                        sheet.Cells[rowIndex, 3].Value = "Công trình";
                        sheet.Cells[rowIndex, 4].Value = 1;
                        sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //gia don vi
                        double giadonvi = 0;
                        var hopdong = _hopDongService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in hopdong)
                        {
                            var quyettoanhopdong = _quyetToanHopDongService.GetByIdHopDong(item.IdHopDong);
                            foreach (var item1 in quyettoanhopdong)
                            {
                                if (item1.NgayQuyetToan != null)
                                {
                                    if (item1.GiaTriQuyetToan == null)
                                    {
                                        item1.GiaTriQuyetToan = 0;
                                    }
                                    giadonvi += (double)item1.GiaTriQuyetToan;
                                }
                            }
                        }
                        var nguonvonthanhtoanchiphikhac = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in nguonvonthanhtoanchiphikhac)
                        {
                            giadonvi += item.GiaTri;
                        }
                        sheet.Cells[rowIndex, 5].Value = giadonvi / dvt;
                        sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                        //tong nguyen gia
                        double tongnguyengia = 0;
                        var quyetoanduan = _quyetToanDuAnService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in quyetoanduan)
                        {
                            if (item.NgayPheDuyetQuyetToan != null)
                            {
                                if (item.TongGiaTri == null)
                                {
                                    item.TongGiaTri = 0;
                                }
                                tongnguyengia += (double)item.TongGiaTri;
                            }
                        }
                        sheet.Cells[rowIndex, 6].Value = tongnguyengia / dvt;
                        sheet.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";
                        //lay ngay ket thuc theo quyet toan du an
                        var ngayketthucthucte = _quyetToanDuAnService.GetNewestByIdDuAn(iIDDuAn).NgayKyBienBan?.ToString("dd/MM/yyyy");
                        sheet.Cells[rowIndex, 7].Value = ngayketthucthucte;
                        sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //lay ra nguon von theo quyet toan du an hoan thanh
                        List<NguonVon> nguonvons = new List<NguonVon>();
                        foreach (var item in quyetoanduan)
                        {
                            if (item.NgayPheDuyetQuyetToan != null)
                            {
                                var nguonvon = _nguonVonService.GetByIdQuyetToanDuAn(item.IdQuyetToanDuAn);
                                foreach (var item1 in nguonvon)
                                {
                                    nguonvons.Add(item1);
                                }
                            }
                        }
                        //loc nguon von bi trung
                        for (var i = 0; i < nguonvons.Count; i++)
                        {
                            for (var j = i + 1; j < nguonvons.Count; j++)
                            {
                                if (nguonvons[i] == nguonvons[j])
                                {
                                    nguonvons[j] = null;
                                }
                            }
                        }
                        string tennguonvons = "";
                        foreach (var item in nguonvons)
                        {
                            if (item != null)
                            {
                                tennguonvons += item.TenNguonVon + ", ";
                            }

                        }
                        if (tennguonvons.Length > 0)
                        {
                            tennguonvons = tennguonvons.Substring(0, tennguonvons.Length - 2);
                        }
                        sheet.Cells[rowIndex, 8].Value = tennguonvons;
                        sheet.Cells[rowIndex, 8].Style.WrapText = true;
                        sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 05/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
