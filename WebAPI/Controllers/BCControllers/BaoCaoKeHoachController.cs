﻿using AutoMapper;
using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;
using Data;
using Model.Models.BCModel;
using Model.Models;
using Data.Repositories;
using Data.Repositories.BCRepository;
using Service.BCService;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OfficeOpenXml.Style;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Providers;
using WebAPI.Models.DuAn;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoKeHoach")]
    public class BaoCaoKeHoachController : ApiControllerBase
    {
        private IBaoCaoKeHoachService _baoCaoKeHoachService;
        private IBaoCaoTableService _baoCaoTableService;
        public BaoCaoKeHoachController(IErrorService errorService, IBaoCaoKeHoachService baoCaoKeHoachService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._baoCaoKeHoachService = baoCaoKeHoachService;
            this._baoCaoTableService = baoCaoTableService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };

        #region Vốn Đầu tư XDCB Năm
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("VonDTXDCBNam")]
        [HttpGet]
        public HttpResponseMessage ExportXDCBNam(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = VonDTXDCBNam(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string VonDTXDCBNam(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/KeHoachVonDauTuXDCBNam.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("KeHoachVonDauTuXDCBNam_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        int iNam_1 = iNamPD - 1;

                        List<object> BCVonDauTuXDCBNam = _baoCaoKeHoachService.GetBCVonDauTuXDCBNam(szIDDuAn);
                        Dictionary<int, List<object>> dicDuAnCon = _baoCaoKeHoachService.GetDuAnCon(szIDDuAn);

                        Dictionary<int, string> TMDTLDADTSoQD = _baoCaoKeHoachService.GetTMDTLDADTSoQD();
                        Dictionary<int, string> TMDTLTDTSoQD = _baoCaoKeHoachService.GetTMDTLTDTSoQD();

                        Dictionary<int, double> TMDTLDADTTongGiaTri = _baoCaoKeHoachService.GetTMDTLDADTTongGiaTri();
                        Dictionary<int, double> TMDTLTDTTongGiaTri = _baoCaoKeHoachService.GetTMDTLTDTTongGiaTri();

                        Dictionary<int, List<object>> DicKhoiLuongThucHien = _baoCaoKeHoachService.GetDicKhoiLuongThucHien(iNamPD);

                        Dictionary<int, List<object>> DicKHVN_1XDCB = _baoCaoKeHoachService.GetDicKHVN_1XDCB(iNamPD);

                        Dictionary<int, List<object>> DicGNN_1 = _baoCaoKeHoachService.GetDicGNN_1XDCB(iNamPD);

                        Dictionary<int, double> DicGNN_2 = _baoCaoKeHoachService.GetDicGNN_2(iNamPD);

                        Dictionary<int, List<object>> DicKHVN_XDCB = _baoCaoKeHoachService.GetDicKHVN_XDCB(iNamPD);


                        int rowCDT = 10;

                        int iCDT;

                        int a = 0;

                        List<int> l_CDTs = new List<int>();

                        foreach (object oCDT in BCVonDauTuXDCBNam)
                        {

                            string TenChuDauTu = (string)GetValueObject(oCDT, "TenChuDauTu");

                            sheet.Cells[rowCDT, 1].Value = Columns[a];
                            sheet.Cells[rowCDT, 2].Value = TenChuDauTu;

                            a++;

                            iCDT = rowCDT--;
                            rowCDT++;
                            rowCDT++;

                            l_CDTs.Add(iCDT);

                            sheet.Row(iCDT).Style.Font.Bold = true;

                            int iTTDA;
                            List<object> lTTDA = (List<object>)GetValueObject(oCDT, "grpTTDA");
                            List<int> l_TTDAs = new List<int>();

                            int b = 0;

                            foreach (object oTTDA in lTTDA)
                            {


                                string TenTinhTrangDuAn = (string)GetValueObject(oTTDA, "TenTinhTrangDuAn");

                                sheet.Cells[rowCDT, 1].Value = SoLaMa[b];
                                sheet.Cells[rowCDT, 2].Value = TenTinhTrangDuAn;


                                b++;

                                iTTDA = rowCDT--;
                                rowCDT++;
                                rowCDT++;

                                l_TTDAs.Add(iTTDA);

                                sheet.Row(iTTDA).Style.Font.Bold = true;

                                List<object> lLVNN = (List<object>)GetValueObject(oTTDA, "grpLVNN");
                                List<int> l_LVNNs = new List<int>();

                                int c = 0;
                                int iLVNN;

                                foreach (object oLVNN in lLVNN)
                                {

                                    string TenLinhVucNganhNghe = (string)GetValueObject(oLVNN, "TenLinhVucNganhNghe");

                                    sheet.Cells[rowCDT, 1].Value = Columns[c].ToLower();
                                    sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;

                                    c++;
                                    iLVNN = rowCDT--;
                                    rowCDT++;
                                    rowCDT++;

                                    l_LVNNs.Add(iLVNN);


                                    sheet.Row(iLVNN).Style.Font.Bold = true;

                                    List<object> lDACha = (List<object>)GetValueObject(oLVNN, "grpDuAn");
                                    List<int> l_DAChas = new List<int>();

                                    int d = 0;

                                    int iDACha;
                                    foreach (object oDACha in lDACha)
                                    {
                                        d++;
                                        XuatDuAnXDCB(sheet, szDonViTinh, oDACha, d.ToString(), rowCDT, TMDTLDADTSoQD, TMDTLTDTSoQD, TMDTLDADTTongGiaTri, TMDTLTDTTongGiaTri, DicKhoiLuongThucHien, DicKHVN_1XDCB, DicGNN_2, DicGNN_1, DicKHVN_XDCB);

                                        iDACha = rowCDT--;
                                        rowCDT++;
                                        rowCDT++;

                                        l_DAChas.Add(iDACha);

                                        int IDDA = (int)GetValueObject(oDACha, "IDDA");



                                        if (dicDuAnCon.ContainsKey(IDDA))
                                        {

                                            List<object> lDuAnCon = dicDuAnCon[IDDA];

                                            foreach (object oDuAnCon in lDuAnCon)
                                            {


                                                XuatDuAnXDCB(sheet, szDonViTinh, oDuAnCon, "-", rowCDT, TMDTLDADTSoQD, TMDTLTDTSoQD, TMDTLDADTTongGiaTri, TMDTLTDTTongGiaTri, DicKhoiLuongThucHien, DicKHVN_1XDCB, DicGNN_2, DicGNN_1, DicKHVN_XDCB);
                                                rowCDT++;
                                            }
                                        }

                                    }
                                    UpdateTongXDCB(sheet, iLVNN, l_DAChas);

                                }
                                UpdateTongXDCB(sheet, iTTDA, l_LVNNs);

                            }
                            UpdateTongXDCB(sheet, iCDT, l_TTDAs);

                        }
                        UpdateTongXDCB(sheet, 9, l_CDTs);

                        sheet.Row(9).Style.Font.Bold = true;


                        int rowIndex = rowCDT - 1;
                        using (ExcelRange rng = sheet.Cells["A9:AH" + rowIndex])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.WrapText = true;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Numberformat.Format = "#,##0";
                        }


                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        string dvTinh = sheet.Cells["AF4"].Value.ToString();
                        dvTinh = dvTinh.Replace("Đơn vị tính:", "Đơn vị tính: " + szDonViTinh + "");
                        sheet.Cells["AF4"].Value = dvTinh;

                        sheet.Cells["I7"].Value = "Từ khởi công đến hết 31/12/" + (iNamPD - 2).ToString();
                        sheet.Cells["J7"].Value = "Cả năm " + (iNamPD - 1).ToString();
                        sheet.Cells["K5"].Value = "Kế hoạch vốn năm " + (iNamPD - 1).ToString();
                        sheet.Cells["S7"].Value = "Từ khởi công đến hết 31/12/" + (iNamPD - 2).ToString();
                        sheet.Cells["T7"].Value = "Năm " + (iNamPD - 1).ToString();
                        sheet.Cells["AA5"].Value = "Kế hoạch vốn năm " + (iNamPD).ToString();



                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "KeHoachVonDauTuXDCBNam", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private void XuatDuAnXDCB(ExcelWorksheet sheet, string szDonViTinh, object oDA, string d, int rowDA,
            Dictionary<int, string> dicTMDTLDADTSoQD,
            Dictionary<int, string> dicTMDTLTDTSoQD,
            Dictionary<int, double> dicTMDTLDADTTongGiaTri,
            Dictionary<int, double> dicTMDTLTDTTongGiaTri,

            Dictionary<int, List<object>> dicKLTH,

            Dictionary<int, List<object>> dicKHVN_1,


            Dictionary<int, double> dicGNN_2,
            Dictionary<int, List<object>> dicGNN_1,

            Dictionary<int, List<object>> dicKHVN
            )
        {
            int dDim = 1;
            if (szDonViTinh == "Nghìn Đồng")
            {
                dDim = 1000;
            }
            else if (szDonViTinh == "Triệu Đồng")
            {
                dDim = 1000000;
            }
            else if (szDonViTinh == "Tỷ Đồng")
            {
                dDim = 1000000000;
            }
            else
            {
                dDim = 1;
            }

            int IDDA = (int)GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)GetValueObject(oDA, "TenDuAn");
            string DiaDiemXayDung = (string)GetValueObject(oDA, "DiaDiemXayDung");
            string NangLucThietKe = (string)GetValueObject(oDA, "NangLucThietKe");
            string ThoiGianKhoiCongHoanThanh = (string)GetValueObject(oDA, "ThoiGianKhoiCongHoanThanh");
            sheet.Cells[rowDA, 1].Value = d;
            sheet.Cells[rowDA, 2].Value = TenDuAn;
            sheet.Cells[rowDA, 3].Value = DiaDiemXayDung;
            sheet.Cells[rowDA, 4].Value = NangLucThietKe;

            sheet.Cells[rowDA, 7].Value = ThoiGianKhoiCongHoanThanh;

            string So_NgayQD = "";
            double TongGiaTri = 0;

            if (dicTMDTLDADTSoQD.ContainsKey(IDDA))
            {
                So_NgayQD = dicTMDTLDADTSoQD[IDDA];
                sheet.Cells[rowDA, 5].Value = So_NgayQD;
            }

            if (!dicTMDTLDADTSoQD.ContainsKey(IDDA) && dicTMDTLTDTSoQD.ContainsKey(IDDA))
            {
                So_NgayQD = dicTMDTLTDTSoQD[IDDA];

                sheet.Cells[rowDA, 5].Value = So_NgayQD;
            }

            if (dicTMDTLDADTTongGiaTri.ContainsKey(IDDA))
            {
                TongGiaTri = dicTMDTLDADTTongGiaTri[IDDA] / dDim;
                sheet.Cells[rowDA, 6].Value = TongGiaTri;
            }

            if (!dicTMDTLDADTTongGiaTri.ContainsKey(IDDA) && dicTMDTLTDTTongGiaTri.ContainsKey(IDDA))
            {
                TongGiaTri = dicTMDTLTDTTongGiaTri[IDDA] / dDim;
                sheet.Cells[rowDA, 6].Value = TongGiaTri;
            }

            if (dicKLTH.ContainsKey(IDDA))
            {
                List<object> lKLTH = dicKLTH[IDDA];
                foreach (object oKLTH in lKLTH)
                {
                    double KhoiLuongThucHienDenNgay = (double)GetValueObject(oKLTH, "KLTHDenNgay") / dDim;
                    double KhoiLuongThucHienCaNam = (double)GetValueObject(oKLTH, "KLTHCaNam") / dDim;

                    sheet.Cells[rowDA, 8].Formula = "=Sum(" + sheet.Cells[rowDA, 9] + ":" + sheet.Cells[rowDA, 10] + ")";

                    sheet.Cells[rowDA, 9].Value = KhoiLuongThucHienDenNgay;
                    sheet.Cells[rowDA, 10].Value = KhoiLuongThucHienCaNam;

                }
            }

            if (dicKHVN_1.ContainsKey(IDDA))
            {
                List<object> lKHVN_1 = dicKHVN_1[IDDA];
                foreach (object oKHVN_1 in lKHVN_1)
                {
                    double KeHoachVonNam_NSTP = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_NSTP") / dDim;
                    double KeHoachVonNam_SDDDD = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_SDDDD") / dDim;
                    double KeHoachVonNam_NSTT = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_NSTT") / dDim;
                    double KeHoachVonNam_NST = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_NST") / dDim;
                    double KeHoachVonNam_ODA = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_ODA") / dDim;
                    double KeHoachVonNam_TPCP = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_TPCP") / dDim;

                    sheet.Cells[rowDA, 11].Formula = "=Sum(" + sheet.Cells[rowDA, 12] + ":" + sheet.Cells[rowDA, 17] + ")";
                    sheet.Cells[rowDA, 12].Value = KeHoachVonNam_NSTP;
                    sheet.Cells[rowDA, 13].Value = KeHoachVonNam_SDDDD;
                    sheet.Cells[rowDA, 14].Value = KeHoachVonNam_NSTT;
                    sheet.Cells[rowDA, 15].Value = KeHoachVonNam_NST;
                    sheet.Cells[rowDA, 16].Value = KeHoachVonNam_ODA;
                    sheet.Cells[rowDA, 17].Value = KeHoachVonNam_TPCP;
                }
            }

            sheet.Cells[rowDA, 18].Formula = "Sum(" + sheet.Cells[rowDA, 19] + ":" + sheet.Cells[rowDA, 20] + ")";

            if (dicGNN_2.ContainsKey(IDDA))
            {
                sheet.Cells[rowDA, 19].Value = dicGNN_2[IDDA] / dDim;
            }

            if (dicGNN_1.ContainsKey(IDDA))
            {
                List<object> lGNN_1 = dicGNN_1[IDDA];
                foreach (object oGNN_1 in lGNN_1)
                {
                    double GiaTriNguonSDD = (double)GetValueObject(oGNN_1, "GiaTriNguonSDD") / dDim;
                    double GiaTriNguonSDDDD = (double)GetValueObject(oGNN_1, "GiaTriNguonSDDDD") / dDim;
                    double GiaTriNguonTinh = (double)GetValueObject(oGNN_1, "GiaTriNguonTinh") / dDim;
                    double GiaTriNguonTT = (double)GetValueObject(oGNN_1, "GiaTriNguonTT") / dDim;
                    double GiaTriNguonODA = (double)GetValueObject(oGNN_1, "GiaTriNguonODA") / dDim;
                    double GiaTriNguonTPCP = (double)GetValueObject(oGNN_1, "GiaTriNguonTPCP") / dDim;

                    sheet.Cells[rowDA, 20].Formula = "=Sum(" + sheet.Cells[rowDA, 21] + ":" + sheet.Cells[rowDA, 26] + ")";
                    sheet.Cells[rowDA, 21].Value = GiaTriNguonSDD;
                    sheet.Cells[rowDA, 22].Value = GiaTriNguonSDDDD;
                    sheet.Cells[rowDA, 23].Value = GiaTriNguonTT;
                    sheet.Cells[rowDA, 24].Value = GiaTriNguonTinh;
                    sheet.Cells[rowDA, 25].Value = GiaTriNguonODA;
                    sheet.Cells[rowDA, 26].Value = GiaTriNguonTPCP;
                }
            }

            if (dicKHVN.ContainsKey(IDDA))
            {
                List<object> lKHVN = dicKHVN[IDDA];
                foreach (object oKHVN in lKHVN)
                {
                    double KeHoachVonNam_NSTP = (double)GetValueObject(oKHVN, "KeHoachVonNam_NSTP") / dDim;
                    double KeHoachVonNam_SDDDD = (double)GetValueObject(oKHVN, "KeHoachVonNam_SDDDD") / dDim;
                    double KeHoachVonNam_NSTT = (double)GetValueObject(oKHVN, "KeHoachVonNam_NSTT") / dDim;
                    double KeHoachVonNam_NST = (double)GetValueObject(oKHVN, "KeHoachVonNam_NST") / dDim;
                    double KeHoachVonNam_ODA = (double)GetValueObject(oKHVN, "KeHoachVonNam_ODA") / dDim;
                    double KeHoachVonNam_TPCP = (double)GetValueObject(oKHVN, "KeHoachVonNam_TPCP") / dDim;

                    sheet.Cells[rowDA, 27].Formula = "=Sum(" + sheet.Cells[rowDA, 28] + ":" + sheet.Cells[rowDA, 33] + ")";
                    sheet.Cells[rowDA, 28].Value = KeHoachVonNam_NSTP;
                    sheet.Cells[rowDA, 29].Value = KeHoachVonNam_SDDDD;
                    sheet.Cells[rowDA, 30].Value = KeHoachVonNam_NSTT;
                    sheet.Cells[rowDA, 31].Value = KeHoachVonNam_NST;
                    sheet.Cells[rowDA, 32].Value = KeHoachVonNam_ODA;
                    sheet.Cells[rowDA, 33].Value = KeHoachVonNam_TPCP;
                }
            }
        }

        private void UpdateTongXDCB(ExcelWorksheet sheet, int row, List<int> lstInt)
        {
            if (lstInt.Count != 0)
            {
                string str = "";
                string str1 = "";
                string str2 = "";
                string str3 = "";
                string str4 = "";
                string str5 = "";
                string str6 = "";
                string str7 = "";
                string str8 = "";
                string str9 = "";
                string str10 = "";
                string str11 = "";
                string str12 = "";
                string str13 = "";
                string str14 = "";
                string str15 = "";
                string str16 = "";
                string str17 = "";
                string str18 = "";
                string str19 = "";
                string str20 = "";
                string str21 = "";
                string str22 = "";
                string str23 = "";
                string str24 = "";
                string str25 = "";
                string str26 = "";



                foreach (var row1 in lstInt)
                {
                    str += "H" + row1.ToString() + "+";
                    str1 += "I" + row1.ToString() + "+";
                    str2 += "J" + row1.ToString() + "+";
                    str3 += "K" + row1.ToString() + "+";
                    str4 += "L" + row1.ToString() + "+";
                    str5 += "M" + row1.ToString() + "+";
                    str6 += "N" + row1.ToString() + "+";
                    str7 += "O" + row1.ToString() + "+";
                    str8 += "P" + row1.ToString() + "+";
                    str9 += "Q" + row1.ToString() + "+";
                    str10 += "R" + row1.ToString() + "+";
                    str11 += "S" + row1.ToString() + "+";
                    str12 += "T" + row1.ToString() + "+";
                    str13 += "U" + row1.ToString() + "+";
                    str14 += "V" + row1.ToString() + "+";
                    str15 += "W" + row1.ToString() + "+";
                    str16 += "X" + row1.ToString() + "+";
                    str17 += "Y" + row1.ToString() + "+";
                    str18 += "Z" + row1.ToString() + "+";
                    str19 += "AA" + row1.ToString() + "+";
                    str20 += "AB" + row1.ToString() + "+";
                    str21 += "AC" + row1.ToString() + "+";
                    str22 += "AD" + row1.ToString() + "+";
                    str23 += "AE" + row1.ToString() + "+";
                    str24 += "AF" + row1.ToString() + "+";
                    str25 += "AG" + row1.ToString() + "+";
                    str26 += "F" + row1.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
                str1 = str1.Substring(0, str1.Length - 1);
                str2 = str2.Substring(0, str2.Length - 1);
                str3 = str3.Substring(0, str3.Length - 1);
                str4 = str4.Substring(0, str4.Length - 1);
                str5 = str5.Substring(0, str5.Length - 1);
                str6 = str6.Substring(0, str6.Length - 1);
                str7 = str7.Substring(0, str7.Length - 1);
                str8 = str8.Substring(0, str8.Length - 1);
                str9 = str9.Substring(0, str9.Length - 1);
                str10 = str10.Substring(0, str10.Length - 1);
                str11 = str11.Substring(0, str11.Length - 1);
                str12 = str12.Substring(0, str12.Length - 1);
                str13 = str13.Substring(0, str13.Length - 1);
                str14 = str14.Substring(0, str14.Length - 1);
                str15 = str15.Substring(0, str15.Length - 1);
                str16 = str16.Substring(0, str16.Length - 1);
                str17 = str17.Substring(0, str17.Length - 1);
                str18 = str18.Substring(0, str18.Length - 1);
                str19 = str19.Substring(0, str19.Length - 1);
                str20 = str20.Substring(0, str20.Length - 1);
                str21 = str21.Substring(0, str21.Length - 1);
                str22 = str22.Substring(0, str22.Length - 1);
                str23 = str23.Substring(0, str23.Length - 1);
                str24 = str24.Substring(0, str24.Length - 1);
                str25 = str25.Substring(0, str25.Length - 1);
                str26 = str26.Substring(0, str26.Length - 1);

                sheet.Cells["H" + row].Formula = "=Sum(" + str + ")";
                sheet.Cells["I" + row].Formula = "=Sum(" + str1 + ")";
                sheet.Cells["J" + row].Formula = "=Sum(" + str2 + ")";
                sheet.Cells["K" + row].Formula = "=Sum(" + str3 + ")";
                sheet.Cells["L" + row].Formula = "=Sum(" + str4 + ")";
                sheet.Cells["M" + row].Formula = "=Sum(" + str5 + ")";
                sheet.Cells["N" + row].Formula = "=Sum(" + str6 + ")";
                sheet.Cells["O" + row].Formula = "=Sum(" + str7 + ")";
                sheet.Cells["P" + row].Formula = "=Sum(" + str8 + ")";
                sheet.Cells["Q" + row].Formula = "=Sum(" + str9 + ")";
                sheet.Cells["R" + row].Formula = "=Sum(" + str10 + ")";
                sheet.Cells["S" + row].Formula = "=Sum(" + str11 + ")";
                sheet.Cells["T" + row].Formula = "=Sum(" + str12 + ")";
                sheet.Cells["U" + row].Formula = "=Sum(" + str13 + ")";
                sheet.Cells["V" + row].Formula = "=Sum(" + str14 + ")";
                sheet.Cells["W" + row].Formula = "=Sum(" + str15 + ")";
                sheet.Cells["X" + row].Formula = "=Sum(" + str16 + ")";
                sheet.Cells["Y" + row].Formula = "=Sum(" + str17 + ")";
                sheet.Cells["Z" + row].Formula = "=Sum(" + str18 + ")";
                sheet.Cells["AA" + row].Formula = "=Sum(" + str19 + ")";
                sheet.Cells["AB" + row].Formula = "=Sum(" + str20 + ")";
                sheet.Cells["AC" + row].Formula = "=Sum(" + str21 + ")";
                sheet.Cells["AD" + row].Formula = "=Sum(" + str22 + ")";
                sheet.Cells["AE" + row].Formula = "=Sum(" + str23 + ")";
                sheet.Cells["AF" + row].Formula = "=Sum(" + str24 + ")";
                sheet.Cells["AG" + row].Formula = "=Sum(" + str25 + ")";
                sheet.Cells["F" + row].Formula = "=Sum(" + str26 + ")";
            }
        }

        #endregion

        #region Vốn TPCP
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("VonTPCP")]
        [HttpGet]
        public HttpResponseMessage ExportVonTPCP(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = VonTPCP(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string VonTPCP(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/KeHoachVonDauTuNganSachNhaNuoc-TPCP.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("KeHoachVonDauTuNganSachNhaNuoc_TPCP_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.

                        int iNam_1 = iNamPD - 1;

                        List<object> ThongTinDuAnCha = _baoCaoKeHoachService.GetThongTinDuAnCha(szIDDuAn);
                        List<object> ThongTinDuAnCon = _baoCaoKeHoachService.GetThongTinDuAnCon(szIDDuAn);

                        Dictionary<int, string> TMDTLDADTSoQD = _baoCaoKeHoachService.GetTMDTLDADTSoQD();
                        Dictionary<int, string> TMDTLTDTSoQD = _baoCaoKeHoachService.GetTMDTLTDTSoQD();

                        Dictionary<int, double> TMDTLDADTTongGiaTri = _baoCaoKeHoachService.GetTMDTLDADTTongGiaTri();
                        Dictionary<int, double> TMDTLTDTTongGiaTri = _baoCaoKeHoachService.GetTMDTLTDTTongGiaTri();

                        List<object> KHVNNSNNN_1 = _baoCaoKeHoachService.GetKHVNNSNN(iNam_1);
                        List<object> KHVNDCNSNNN_1 = _baoCaoKeHoachService.GetKHVNDCNSNN(iNam_1);

                        List<object> LuyKeKeHoachVonNSNN = _baoCaoKeHoachService.GetLuyKeKeHoachVonNSNN(iNam_1);
                        List<object> LuyKeKeHoachVonDieuChinhNSNN = _baoCaoKeHoachService.GetLuyKeKeHoachVonDieuChinhNSNN(iNam_1);

                        Dictionary<int, List<object>> dicKLTH = _baoCaoKeHoachService.GetDicKLTH(iNamPD);

                        List<object> UocThanhToanChiPhiKhacs = _baoCaoKeHoachService.GetUocThanhToanChiPhiKhacs(iNamPD);
                        List<object> UocThanhToanHopDong = _baoCaoKeHoachService.GetUocThanhToanHopDong(iNamPD);

                        Dictionary<int, double> dicKHGiaiNgan = _baoCaoKeHoachService.GetDicKHGN(iNamPD);

                        List<object> KHVNNSNNN = _baoCaoKeHoachService.GetKHVNNSNN(iNamPD);
                        List<object> KHVNDCNSNN = _baoCaoKeHoachService.GetKHVNDCNSNN(iNamPD);

                        Dictionary<int, List<object>> dicDuAnCon = (from item in
                                                                        (from item in ThongTinDuAnCon
                                                                         select new
                                                                         {
                                                                             IDDACha = (int)GetValueObject(item, "IDDACha"),
                                                                             IDDA = (int)GetValueObject(item, "IDDA"),
                                                                             TenDuAn = GetValueObject(item, "TenDuAn"),
                                                                             DiaDiemXayDung = GetValueObject(item, "DiaDiemXayDung"),
                                                                             NangLucThietKe = GetValueObject(item, "NangLucThietKe"),
                                                                             ThoiGianKhoiCongHoanThanh = GetValueObject(item, "ThoiGianKhoiCongHoanThanh")
                                                                         })
                                                                    group item by new { item.IDDACha }
                                                                   into item1
                                                                    select new
                                                                    {
                                                                        IDDACha = item1.Key.IDDACha,
                                                                        grpDACon = item1.Select(x => new
                                                                        {
                                                                            IDDA = x.IDDA,
                                                                            TenDuAn = x.TenDuAn,
                                                                            DiaDiemXayDung = x.DiaDiemXayDung,
                                                                            NangLucThietKe = x.NangLucThietKe,
                                                                            ThoiGianKhoiCongHoanThanh = x.ThoiGianKhoiCongHoanThanh
                                                                        }).ToList<object>()
                                                                    }).ToDictionary(x => x.IDDACha, x => x.grpDACon);

                        Dictionary<int, double> dicKHVN_1 = (from item in KHVNNSNNN_1
                                                             join item1 in KHVNDCNSNNN_1 on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                                                             from item2 in lstDCKHV.DefaultIfEmpty()
                                                             select new
                                                             {
                                                                 IDDA = (int)GetValueObject(item, "IDDA"),
                                                                 GiaTri = item2 == null ? (double)GetValueObject(item, "GiaTri") : (double)GetValueObject(item2, "GiaTri")
                                                             }).ToDictionary(x => x.IDDA, x => x.GiaTri);

                        Dictionary<int, List<object>> dicLuyKeVon = (from item in
                                                                        (from item in
                                                                            (from item in LuyKeKeHoachVonNSNN
                                                                             join item1 in LuyKeKeHoachVonDieuChinhNSNN on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                                                                             from item2 in lstDCKHV
                                                                             select new
                                                                             {
                                                                                 IDDA = (int)GetValueObject(item, "IDDA"),
                                                                                 LuyKeKHVDenN_1_Tinh = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1_Tinh") : (double)GetValueObject(item2, "LuyKeKHVDenN_1_Tinh"),
                                                                                 LuyKeKHVDenN_1_Huyen = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1_Huyen") : (double)GetValueObject(item2, "LuyKeKHVDenN_1_Huyen"),
                                                                                 LuyKeKHVDenN_1_Xa = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1_Xa") : (double)GetValueObject(item2, "LuyKeKHVDenN_1_Xa"),
                                                                                 LuyKeKHVDenN_1_NSTW = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1_NSTW") : (double)GetValueObject(item2, "LuyKeKHVDenN_1_NSTW"),
                                                                                 LuyKeKHVDenN_1TPCP = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1TPCP") : (double)GetValueObject(item2, "LuyKeKHVDenN_1TPCP"),
                                                                                 LuyKeKHVDenN_1_Khac = item2 == null ? (double)GetValueObject(item, "LuyKeKHVDenN_1_Khac") : (double)GetValueObject(item2, "LuyKeKHVDenN_1_Khac")
                                                                             })
                                                                         group item by new { item.IDDA }
                                                                         into item1
                                                                         select new
                                                                         {
                                                                             IDDA = item1.Key.IDDA,
                                                                             LuyKeKHVDenN_1_Tinh = item1.Select(x => x.LuyKeKHVDenN_1_Tinh).Sum(),
                                                                             LuyKeKHVDenN_1_Huyen = item1.Select(x => x.LuyKeKHVDenN_1_Huyen).Sum(),
                                                                             LuyKeKHVDenN_1_Xa = item1.Select(x => x.LuyKeKHVDenN_1_Xa).Sum(),
                                                                             LuyKeKHVDenN_1_NSTW = item1.Select(x => x.LuyKeKHVDenN_1_NSTW).Sum(),
                                                                             LuyKeKHVDenN_1TPCP = item1.Select(x => x.LuyKeKHVDenN_1TPCP).Sum(),
                                                                             LuyKeKHVDenN_1_Khac = item1.Select(x => x.LuyKeKHVDenN_1_Khac).Sum()
                                                                         })
                                                                     group item by new { item.IDDA }
                                                                   into item1
                                                                     select new
                                                                     {
                                                                         IDDA = item1.Key.IDDA,
                                                                         grpGiaTri = item1.Select(x => new
                                                                         {
                                                                             LuyKeKHVDenN_1_Tinh = x.LuyKeKHVDenN_1_Tinh,
                                                                             LuyKeKHVDenN_1_Huyen = x.LuyKeKHVDenN_1_Huyen,
                                                                             LuyKeKHVDenN_1_Xa = x.LuyKeKHVDenN_1_Xa,
                                                                             LuyKeKHVDenN_1_NSTW = x.LuyKeKHVDenN_1_NSTW,
                                                                             LuyKeKHVDenN_1TPCP = x.LuyKeKHVDenN_1TPCP,
                                                                             LuyKeKHVDenN_1_Khac = x.LuyKeKHVDenN_1_Khac
                                                                         }).ToList<object>()
                                                                     }).ToDictionary(x => x.IDDA, x => x.grpGiaTri);

                        Dictionary<int, List<object>> dicUocGiaiNgan = (from item in
                                                                             (from item in UocThanhToanChiPhiKhacs
                                                                              join item1 in UocThanhToanHopDong
                                                                              on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                                                                              select new
                                                                              {
                                                                                  IDDA = (int)GetValueObject(item, "IDDA"),

                                                                                  GiaTriNguonTinh = (double)GetValueObject(item, "GiaTriNguonTinh") + (double)GetValueObject(item1, "GiaTriNguonTinh"),

                                                                                  GiaTriNguonHuyen = (double)GetValueObject(item, "GiaTriNguonHuyen") + (double)GetValueObject(item1, "GiaTriNguonHuyen"),

                                                                                  GiaTriNguonXa = (double)GetValueObject(item, "GiaTriNguonXa") + (double)GetValueObject(item1, "GiaTriNguonXa"),

                                                                                  GiaTriNguonNSTW = (double)GetValueObject(item, "GiaTriNguonNSTW") + (double)GetValueObject(item1, "GiaTriNguonNSTW"),

                                                                                  GiaTriNguonTPCP = (double)GetValueObject(item, "GiaTriNguonTPCP") + (double)GetValueObject(item1, "GiaTriNguonTPCP"),

                                                                                  GiaTriNguonKhac = (double)GetValueObject(item, "GiaTriNguonKhac") + (double)GetValueObject(item1, "GiaTriNguonKhac"),
                                                                              })
                                                                        group item by new { item.IDDA }
                                                                        into item1
                                                                        select new
                                                                        {
                                                                            IDDA = item1.Key.IDDA,
                                                                            grpUocGiaiNgan = item1.Select(x => new
                                                                            {
                                                                                GiaTriNguonTinh = x.GiaTriNguonTinh,
                                                                                GiaTriNguonHuyen = x.GiaTriNguonHuyen,
                                                                                GiaTriNguonXa = x.GiaTriNguonXa,
                                                                                GiaTriNguonNSTW = x.GiaTriNguonNSTW,
                                                                                GiaTriNguonTPCP = x.GiaTriNguonTPCP,
                                                                                GiaTriNguonKhac = x.GiaTriNguonKhac
                                                                            }).ToList<object>()
                                                                        }).ToDictionary(x => x.IDDA, x => x.grpUocGiaiNgan);

                        Dictionary<int, double> dicKHVN = (from item in KHVNNSNNN
                                                           join item1 in KHVNDCNSNN on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                                                           from item2 in lstDCKHV.DefaultIfEmpty()
                                                           select new
                                                           {
                                                               IDDA = (int)GetValueObject(item, "IDDA"),
                                                               GiaTri = item2 == null ? (double)GetValueObject(item, "GiaTri") : (double)GetValueObject(item2, "GiaTri")
                                                           }).ToDictionary(x => x.IDDA, x => x.GiaTri);

                        int rowTTDA = 11;
                        int a = 0;
                        int iTTDA;
                        List<int> l_TTDAs = new List<int>();

                        foreach (object oTTDA in ThongTinDuAnCha)
                        {
                            string TinhTrangDuAn = (string)GetValueObject(oTTDA, "TenTinhTrangDuAn");

                            sheet.Cells[rowTTDA, 1].Value = SoLaMa[a];
                            sheet.Cells[rowTTDA, 2].Value = TinhTrangDuAn;

                            List<object> lDACha = (List<object>)GetValueObject(oTTDA, "grpDuAnCha");

                            a++;

                            iTTDA = rowTTDA--;
                            rowTTDA++;
                            rowTTDA++;

                            l_TTDAs.Add(iTTDA);
                            sheet.Row(iTTDA).Style.Font.Bold = true;

                            int b = 0;
                            int iDACha;
                            List<int> lDACha_s = new List<int>();
                            foreach (object oDACha in lDACha)
                            {
                                b++;
                                XuatDuAnTPCP(sheet, szDonViTinh, oDACha, b.ToString(), rowTTDA, TMDTLDADTSoQD, TMDTLTDTSoQD, TMDTLDADTTongGiaTri, TMDTLTDTTongGiaTri, dicLuyKeVon, /*KHVNNSNNN_1, KHVNDCNSNNN_1,*/ dicKHVN_1, dicKLTH, dicUocGiaiNgan, dicKHGiaiNgan, dicKHVN/*, KHVNNSNNN, KHVNDCNSNN*/);

                                iDACha = rowTTDA--;
                                rowTTDA++;
                                rowTTDA++;

                                lDACha_s.Add(iDACha);
                                int IDDA = (int)GetValueObject(oDACha, "IDDA");

                                if (dicDuAnCon.ContainsKey(IDDA))
                                {
                                    List<object> lDuAnCon = dicDuAnCon[IDDA];
                                    foreach (object oDuAnCon in lDuAnCon)
                                    {
                                        XuatDuAnTPCP(sheet, szDonViTinh, oDuAnCon, "-", rowTTDA, TMDTLDADTSoQD, TMDTLTDTSoQD, TMDTLDADTTongGiaTri, TMDTLTDTTongGiaTri, dicLuyKeVon, /*KHVNNSNNN_1, KHVNDCNSNNN_1,*/dicKHVN_1, dicKLTH, dicUocGiaiNgan, dicKHGiaiNgan, dicKHVN/*, KHVNNSNNN, KHVNDCNSNN*/);
                                        rowTTDA++;

                                    }
                                }
                            }
                            UpdateTongTPCP(sheet, iTTDA, lDACha_s);
                        }
                        UpdateTongTPCP(sheet, 10, l_TTDAs);

                        sheet.Row(10).Style.Font.Bold = true;

                        int rowIndex = rowTTDA - 1;

                        using (ExcelRange rng = sheet.Cells["A10:AD" + rowIndex])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.WrapText = true;
                            rng.Style.Font.Size = 14;

                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Numberformat.Format = "#,##0";
                        }

                        string title = sheet.Cells["A2"].Value.ToString();
                        title = title.Replace("{{N}}", namPD + "");
                        sheet.Cells["A2"].Value = title;

                        sheet.Cells["AB4"].Value = "Đơn vị tính: " + szDonViTinh;

                        sheet.Cells["H5"].Value = "Lũy kế vốn đã bố trí đến hết KH năm " + (iNamPD - 1).ToString();
                        sheet.Cells["O6"].Value = "Trong đó: Kế hoạch năm " + (iNamPD - 1).ToString();
                        sheet.Cells["P5"].Value = "Ước khối lượng thực hiện từ khởi công đến 31/12/" + (iNamPD - 1).ToString();
                        sheet.Cells["Q6"].Value = "Trong đó: Kế hoạch năm " + (iNamPD - 1).ToString();
                        sheet.Cells["R5"].Value = "Ước giải ngân từ khởi công đến 31/01/" + (iNamPD).ToString();
                        sheet.Cells["Y6"].Value = "Trong đó: Kế hoạch năm " + (iNamPD - 1).ToString();
                        sheet.Cells["AC5"].Value = "Dự kiến KH vốn năm " + (iNamPD).ToString();

                        KhoangNam(sheet, iNamPD);

                        sheet.Cells["AB4"].Style.Font.Size = 12;


                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "KEHOACHVONTPCP", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();

                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        private void XuatDuAnTPCP(ExcelWorksheet sheet, string szDonViTinh, object oDA, string d, int rowCDT,

            Dictionary<int, string> dicTMDTLDADTSoQD,
            Dictionary<int, string> dicTMDTLTDTSoQD,
            Dictionary<int, double> dicTMDTLDADTTongGiaTri,
            Dictionary<int, double> dicTMDTLTDTTongGiaTri,

            Dictionary<int, List<object>> dicLuyKeVon,

            Dictionary<int, double> dicKHVN_1,

            Dictionary<int, List<object>> dicKLTH,

            Dictionary<int, List<object>> dicUocGiaiNgan,
            Dictionary<int, double> dicKHGiaiNgan,

            Dictionary<int, double> dicKHVN
            )
        {
            int dDim = 1;
            if (szDonViTinh == "Nghìn Đồng")
            {
                dDim = 1000;
            }
            else if (szDonViTinh == "Triệu Đồng")
            {
                dDim = 1000000;
            }
            else if (szDonViTinh == "Tỷ Đồng")
            {
                dDim = 1000000000;
            }


            int IDDA = (int)GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)GetValueObject(oDA, "TenDuAn");
            string DiaDiemXayDung = (string)GetValueObject(oDA, "DiaDiemXayDung");
            string NangLucThietKe = (string)GetValueObject(oDA, "NangLucThietKe");
            string ThoiGianKhoiCongHoanThanh = (string)GetValueObject(oDA, "ThoiGianKhoiCongHoanThanh");

            sheet.Cells[rowCDT, 1].Value = d;
            sheet.Cells[rowCDT, 2].Value = TenDuAn;
            sheet.Cells[rowCDT, 3].Value = DiaDiemXayDung;
            sheet.Cells[rowCDT, 4].Value = NangLucThietKe;

            sheet.Cells[rowCDT, 5].Value = ThoiGianKhoiCongHoanThanh;

            string szSoQD = "";
            double dTongGiaTri = 0;

            if (dicTMDTLDADTSoQD.ContainsKey(IDDA))
            {
                szSoQD = dicTMDTLDADTSoQD[IDDA];
                sheet.Cells[rowCDT, 6].Value = szSoQD;
            }

            if (!dicTMDTLDADTSoQD.ContainsKey(IDDA) && dicTMDTLTDTSoQD.ContainsKey(IDDA))
            {
                szSoQD = dicTMDTLTDTSoQD[IDDA];
                sheet.Cells[rowCDT, 6].Value = szSoQD;
            }

            if (dicTMDTLDADTTongGiaTri.ContainsKey(IDDA))
            {
                dTongGiaTri = dicTMDTLDADTTongGiaTri[IDDA] / dDim;
                sheet.Cells[rowCDT, 7].Value = dTongGiaTri;
            }

            if (!dicTMDTLDADTTongGiaTri.ContainsKey(IDDA) && dicTMDTLTDTTongGiaTri.ContainsKey(IDDA))
            {
                dTongGiaTri = dicTMDTLTDTTongGiaTri[IDDA] / dDim;
                sheet.Cells[rowCDT, 7].Value = dTongGiaTri;
            }

            if (dicLuyKeVon.ContainsKey(IDDA))
            {
                List<object> lLuyKeVon = dicLuyKeVon[IDDA];
                foreach (object oLuyKeVon in lLuyKeVon)
                {
                    double LuyKeKHVDenN_1_Tinh = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1_Tinh") / dDim;
                    double LuyKeKHVDenN_1_Huyen = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1_Huyen") / dDim;
                    double LuyKeKHVDenN_1_Xa = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1_Xa") / dDim;
                    double LuyKeKHVDenN_1_NSTW = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1_NSTW") / dDim;
                    double LuyKeKHVDenN_1TPCP = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1TPCP") / dDim;
                    double LuyKeKHVDenN_1_Khac = (double)GetValueObject(oLuyKeVon, "LuyKeKHVDenN_1_Khac") / dDim;

                    sheet.Cells[rowCDT, 8].Formula = "=Sum(" + sheet.Cells[rowCDT, 9] + ":" + sheet.Cells[rowCDT, 14] + ")";
                    sheet.Cells[rowCDT, 9].Value = LuyKeKHVDenN_1_Tinh;
                    sheet.Cells[rowCDT, 10].Value = LuyKeKHVDenN_1_Huyen;
                    sheet.Cells[rowCDT, 11].Value = LuyKeKHVDenN_1_Xa;
                    sheet.Cells[rowCDT, 12].Value = LuyKeKHVDenN_1_NSTW;
                    sheet.Cells[rowCDT, 13].Value = LuyKeKHVDenN_1TPCP;
                    sheet.Cells[rowCDT, 14].Value = LuyKeKHVDenN_1_Khac;
                }
            }

            if (dicKHVN_1.ContainsKey(IDDA))
            {
                sheet.Cells[rowCDT, 15].Value = dicKHVN_1[IDDA] / dDim;
            }

            if (dicKLTH.ContainsKey(IDDA))
            {
                List<object> lKLTH = dicKLTH[IDDA];
                foreach (object oKLTH in lKLTH)
                {
                    double KhoiLuongThucHienDenHetNamN_1 = (double)GetValueObject(oKLTH, "KLTHDenHetNam") / dDim;
                    double KhoiLuongThucHienCaNamN_1 = (double)GetValueObject(oKLTH, "KLTHCaNam") / dDim;

                    sheet.Cells[rowCDT, 16].Value = KhoiLuongThucHienDenHetNamN_1;
                    sheet.Cells[rowCDT, 17].Value = KhoiLuongThucHienCaNamN_1;
                }
            }

            if (dicUocGiaiNgan.ContainsKey(IDDA))
            {
                List<object> lUocGiaiNgan = dicUocGiaiNgan[IDDA];
                foreach (object oUocGiaiNgan in lUocGiaiNgan)
                {
                    double GiaTriNguonTinh = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonTinh") / dDim;
                    double GiaTriNguonHuyen = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonHuyen") / dDim;
                    double GiaTriNguonXa = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonXa") / dDim;
                    double GiaTriNguonNSTW = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonNSTW") / dDim;
                    double GiaTriNguonTPCP = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonTPCP") / dDim;
                    double GiaTriNguonKhac = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonKhac") / dDim;

                    sheet.Cells[rowCDT, 18].Formula = "=Sum(" + sheet.Cells[rowCDT, 19] + ":" + sheet.Cells[rowCDT, 24] + ")";
                    sheet.Cells[rowCDT, 19].Value = GiaTriNguonTinh;
                    sheet.Cells[rowCDT, 20].Value = GiaTriNguonHuyen;
                    sheet.Cells[rowCDT, 21].Value = GiaTriNguonXa;
                    sheet.Cells[rowCDT, 22].Value = GiaTriNguonNSTW;
                    sheet.Cells[rowCDT, 23].Value = GiaTriNguonTPCP;
                    sheet.Cells[rowCDT, 24].Value = GiaTriNguonKhac;
                }
            }

            if (dicKHGiaiNgan.ContainsKey(IDDA))
            {
                sheet.Cells[rowCDT, 25].Value = dicKHGiaiNgan[IDDA] / dDim;
            }

            sheet.Cells[rowCDT, 26].Formula = "=" + sheet.Cells[rowCDT, 16] + "-" + sheet.Cells[rowCDT, 18];

            sheet.Cells[rowCDT, 27].Formula = "=" + sheet.Cells[rowCDT, 7] + "-" + sheet.Cells[rowCDT, 18];

            if (dicKHVN.ContainsKey(IDDA))
            {
                sheet.Cells[rowCDT, 29].Value = dicKHVN[IDDA] / dDim;
            }

        }
        private void UpdateTongTPCP(ExcelWorksheet sheet, int row, List<int> lstInt)
        {
            if (lstInt.Count != 0)
            {
                string str = "";
                string str1 = "";
                string str2 = "";
                string str3 = "";
                string str4 = "";
                string str5 = "";
                string str6 = "";
                string str7 = "";
                string str8 = "";
                string str9 = "";
                string str10 = "";
                string str11 = "";
                string str12 = "";
                string str13 = "";
                string str14 = "";
                string str15 = "";
                string str16 = "";
                string str17 = "";
                string str18 = "";
                string str19 = "";
                string str20 = "";
                string str21 = "";
                string str22 = "";

                foreach (var row1 in lstInt)
                {
                    str += "H" + row1.ToString() + "+";
                    str1 += "I" + row1.ToString() + "+";
                    str2 += "J" + row1.ToString() + "+";
                    str3 += "K" + row1.ToString() + "+";
                    str4 += "L" + row1.ToString() + "+";
                    str5 += "M" + row1.ToString() + "+";
                    str6 += "N" + row1.ToString() + "+";
                    str7 += "O" + row1.ToString() + "+";
                    str8 += "P" + row1.ToString() + "+";
                    str9 += "Q" + row1.ToString() + "+";
                    str10 += "R" + row1.ToString() + "+";
                    str11 += "S" + row1.ToString() + "+";
                    str12 += "T" + row1.ToString() + "+";
                    str13 += "U" + row1.ToString() + "+";
                    str14 += "V" + row1.ToString() + "+";
                    str15 += "W" + row1.ToString() + "+";
                    str16 += "X" + row1.ToString() + "+";
                    str17 += "Y" + row1.ToString() + "+";
                    str18 += "Z" + row1.ToString() + "+";
                    str19 += "AA" + row1.ToString() + "+";
                    str20 += "AB" + row1.ToString() + "+";
                    str21 += "AC" + row1.ToString() + "+";
                    str22 += "G" + row1.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
                str1 = str1.Substring(0, str1.Length - 1);
                str2 = str2.Substring(0, str2.Length - 1);
                str3 = str3.Substring(0, str3.Length - 1);
                str4 = str4.Substring(0, str4.Length - 1);
                str5 = str5.Substring(0, str5.Length - 1);
                str6 = str6.Substring(0, str6.Length - 1);
                str7 = str7.Substring(0, str7.Length - 1);
                str8 = str8.Substring(0, str8.Length - 1);
                str9 = str9.Substring(0, str9.Length - 1);
                str10 = str10.Substring(0, str10.Length - 1);
                str11 = str11.Substring(0, str11.Length - 1);
                str12 = str12.Substring(0, str12.Length - 1);
                str13 = str13.Substring(0, str13.Length - 1);
                str14 = str14.Substring(0, str14.Length - 1);
                str15 = str15.Substring(0, str15.Length - 1);
                str16 = str16.Substring(0, str16.Length - 1);
                str17 = str17.Substring(0, str17.Length - 1);
                str18 = str18.Substring(0, str18.Length - 1);
                str19 = str19.Substring(0, str19.Length - 1);
                str20 = str20.Substring(0, str20.Length - 1);
                str21 = str21.Substring(0, str21.Length - 1);
                str22 = str22.Substring(0, str22.Length - 1);


                sheet.Cells["H" + row].Formula = "=Sum(" + str + ")";
                sheet.Cells["I" + row].Formula = "=Sum(" + str1 + ")";
                sheet.Cells["J" + row].Formula = "=Sum(" + str2 + ")";
                sheet.Cells["K" + row].Formula = "=Sum(" + str3 + ")";
                sheet.Cells["L" + row].Formula = "=Sum(" + str4 + ")";
                sheet.Cells["M" + row].Formula = "=Sum(" + str5 + ")";
                sheet.Cells["N" + row].Formula = "=Sum(" + str6 + ")";
                sheet.Cells["O" + row].Formula = "=Sum(" + str7 + ")";
                sheet.Cells["P" + row].Formula = "=Sum(" + str8 + ")";
                sheet.Cells["Q" + row].Formula = "=Sum(" + str9 + ")";
                sheet.Cells["R" + row].Formula = "=Sum(" + str10 + ")";
                sheet.Cells["S" + row].Formula = "=Sum(" + str11 + ")";
                sheet.Cells["T" + row].Formula = "=Sum(" + str12 + ")";
                sheet.Cells["U" + row].Formula = "=Sum(" + str13 + ")";
                sheet.Cells["V" + row].Formula = "=Sum(" + str14 + ")";
                sheet.Cells["W" + row].Formula = "=Sum(" + str15 + ")";
                sheet.Cells["X" + row].Formula = "=Sum(" + str16 + ")";
                sheet.Cells["Y" + row].Formula = "=Sum(" + str17 + ")";
                sheet.Cells["Z" + row].Formula = "=Sum(" + str18 + ")";
                sheet.Cells["AA" + row].Formula = "=Sum(" + str19 + ")";
                sheet.Cells["AB" + row].Formula = "=Sum(" + str20 + ")";
                sheet.Cells["AC" + row].Formula = "=Sum(" + str21 + ")";
                sheet.Cells["G" + row].Formula = "=Sum(" + str22 + ")";
            }
        }

        private void KhoangNam(ExcelWorksheet sheet, int iNam)
        {
            string szNam = "2016 - 2020";

            if (2006 <= iNam && iNam <= 2010)
            {
                szNam = "2006 - 2010";
            }
            else if (2011 <= iNam && iNam <= 2015)
            {
                szNam = "2011 - 2015";
            }
            else if (2021 <= iNam && iNam <= 2025)
            {
                szNam = "2021 - 2025";
            }
            else if (2026 <= iNam && iNam <= 2030)
            {
                szNam = "2026 - 2030";
            }

            sheet.Cells["AA5"].Value = "Dự kiến KH đầu tư trung hạn 5 năm " + szNam;
        }
        #endregion

        #region Tình hình thực hiện dự án sử dụng vốn ODA
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("ThucHienODA")]
        [HttpGet]
        public HttpResponseMessage ExportThucHienVonODA(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = ThucHienVonODA(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string ThucHienVonODA(string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TinhHinhThucHienCacDuAnDauTuNguonVonODA.xlsx");

            string documentName = string.Format("TinhHinhThucHienCacDuAnDauTuNguonVonODA_" + iNamPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.

                        int iNam_1 = iNamPD - 1;

                        List<object> lstDuAnCha = _baoCaoKeHoachService.GetDuAnChaODA(iIDDuAn);

                        List<object> lstDuAnCon = _baoCaoKeHoachService.GetDuAnConODA(iIDDuAn);

                        Dictionary<int, string> TMDTLDADTSoQDBanDau = _baoCaoKeHoachService.GetTMDTLDADTSoQDBanDau();
                        Dictionary<int, string> TMDTLTDTSoQDBanDau = _baoCaoKeHoachService.GetTMDTLTDTSoQDBanDau();

                        Dictionary<int, double> TMDTLDADTTongGiaTriBanDau = _baoCaoKeHoachService.GetTMDTLDADTTongGiaTriBanDau();
                        Dictionary<int, double> TMDTLTDTTongGiaTriBanDau = _baoCaoKeHoachService.GetTMDTLTDTTongGiaTriBanDau();

                        Dictionary<int, string> TMDTLDADTSoQDSuaDoi = _baoCaoKeHoachService.GetTMDTLDADTSoQDSuaDoi();
                        Dictionary<int, string> TMDTLTDTSoQDSuaDoi = _baoCaoKeHoachService.GetTMDTLTDTSoQDSuaDoi();

                        Dictionary<int, double> TMDTLDADTTongGiaTriSuaDoi = _baoCaoKeHoachService.GetTMDTLDADTTongGiaTriSuaDoi();
                        Dictionary<int, double> TMDTLTDTTongGiaTriSuaDoi = _baoCaoKeHoachService.GetTMDTLTDTTongGiaTriSuaDoi();

                        List<object> lstLuyKeVonThanhToanCPKs = _baoCaoKeHoachService.GetLuyKeVonThanhToanCPKs(iNamPD);
                        List<object> lstLuyKeVonThanhToanHD = _baoCaoKeHoachService.GetLuyKeVonThanhToanHD(iNamPD);

                        List<object> KHVN_ODA = _baoCaoKeHoachService.GetDicKHVN_ODA(iNam_1);
                        List<object> KHVNDC_ODA = _baoCaoKeHoachService.GetDicKHVNDC_ODA(iNam_1);

                        List<object> lstUocThanhToanCPKs = _baoCaoKeHoachService.GetUocThanhToanCPKs(iNamPD);
                        List<object> lstUocThanhToanHD = _baoCaoKeHoachService.GetUocThanhToanHD(iNamPD);

                        List<object> lstThanhToanCPKsNam = _baoCaoKeHoachService.GetThanhToanCPKsNam(iNamPD);
                        List<object> lstThanhToanHDNam = _baoCaoKeHoachService.GetThanhToanHDNam(iNamPD);


                        Dictionary<int, List<object>> dicDuAnCon = (from item in
                                                                        (from item in lstDuAnCon
                                                                         select new
                                                                         {
                                                                             IDDA = (int)GetValueObject(item, "IDDA"),
                                                                             IDDACha = (int)GetValueObject(item, "IDDACha"),
                                                                             TenDuAn = GetValueObject(item, "TenDuAn")
                                                                         })
                                                                    group item by new { item.IDDACha }
                                                                   into item1
                                                                    select new
                                                                    {
                                                                        IDDACha = item1.Key.IDDACha,
                                                                        grpDuAnCon = item1.Select(x => new
                                                                        {
                                                                            IDDA = x.IDDA,
                                                                            TenDuAn = x.TenDuAn
                                                                        }).ToList<object>()
                                                                    }).ToDictionary(x => x.IDDACha, x => x.grpDuAnCon);

                        Dictionary<int, List<object>> dicGiaiNganNam = (from item in
                                                                           (from item in lstThanhToanCPKsNam
                                                                            join item1 in lstThanhToanHDNam
                                                                            on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                                                                            select new
                                                                            {
                                                                                IDDA = (int)GetValueObject(item, "IDDA"),

                                                                                GiaTriNguonNSTW = (double)GetValueObject(item, "GiaTriNguonNSTW") + (double)GetValueObject(item1, "GiaTriNguonNSTW"),
                                                                                GiaTriNguonTPCP = (double)GetValueObject(item, "GiaTriNguonTPCP") + (double)GetValueObject(item1, "GiaTriNguonTPCP"),
                                                                                GiaTriNguonKhac = (double)GetValueObject(item, "GiaTriNguonKhac") + (double)GetValueObject(item1, "GiaTriNguonKhac"),
                                                                                GiaTriVonVayNuocNgoai = (double)GetValueObject(item, "GiaTriVonVayNuocNgoai") + (double)GetValueObject(item1, "GiaTriVonVayNuocNgoai")
                                                                            })
                                                                        group item by new { item.IDDA }
                                                                      into item1
                                                                        select new
                                                                        {
                                                                            IDDA = item1.Key.IDDA,
                                                                            grpGiaiNganNam = item1.Select(x => new
                                                                            {
                                                                                GiaTriNguonNSTW = x.GiaTriNguonNSTW,
                                                                                GiaTriNguonTPCP = x.GiaTriNguonTPCP,
                                                                                GiaTriNguonKhac = x.GiaTriNguonKhac,
                                                                                GiaTriVonVayNuocNgoai = x.GiaTriVonVayNuocNgoai
                                                                            }).ToList<object>()
                                                                        }).ToDictionary(x => x.IDDA, x => x.grpGiaiNganNam);

                        Dictionary<int, List<object>> dicUocGiaiNgan = (from item in
                                                                            (from item in lstUocThanhToanCPKs
                                                                             join item1 in lstUocThanhToanHD
                                                                             on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                                                                             select new
                                                                             {
                                                                                 IDDA = (int)GetValueObject(item, "IDDA"),

                                                                                 GiaTriNguonNSTW = (double)GetValueObject(item, "GiaTriNguonNSTW") + (double)GetValueObject(item1, "GiaTriNguonNSTW"),
                                                                                 GiaTriNguonTPCP = (double)GetValueObject(item, "GiaTriNguonTPCP") + (double)GetValueObject(item1, "GiaTriNguonTPCP"),
                                                                                 GiaTriNguonKhac = (double)GetValueObject(item, "GiaTriNguonKhac") + (double)GetValueObject(item1, "GiaTriNguonKhac"),
                                                                                 GiaTriVonVayNuocNgoai = (double)GetValueObject(item, "GiaTriVonVayNuocNgoai") + (double)GetValueObject(item1, "GiaTriVonVayNuocNgoai")
                                                                             })
                                                                        group item by new { item.IDDA }
                                                                       into item1
                                                                        select new
                                                                        {
                                                                            IDDA = item1.Key.IDDA,
                                                                            grpUocGiaiNgan = item1.Select(x => new
                                                                            {
                                                                                GiaTriNguonNSTW = x.GiaTriNguonNSTW,
                                                                                GiaTriNguonTPCP = x.GiaTriNguonTPCP,
                                                                                GiaTriNguonKhac = x.GiaTriNguonKhac,
                                                                                GiaTriVonVayNuocNgoai = x.GiaTriVonVayNuocNgoai
                                                                            }).ToList<object>()
                                                                        }).ToDictionary(x => x.IDDA, x => x.grpUocGiaiNgan);

                        Dictionary<int, List<object>> dicLuyKeVonGiaiNgan = (from item in
                                                                                 (from item in lstLuyKeVonThanhToanCPKs
                                                                                  join item1 in lstLuyKeVonThanhToanHD
                                                                                  on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                                                                                  select new
                                                                                  {
                                                                                      IDDA = (int)GetValueObject(item, "IDDA"),

                                                                                      GiaTriNguonNSTW = (double)GetValueObject(item, "GiaTriNguonNSTW") + (double)GetValueObject(item1, "GiaTriNguonNSTW"),
                                                                                      GiaTriNguonTPCP = (double)GetValueObject(item, "GiaTriNguonTPCP") + (double)GetValueObject(item1, "GiaTriNguonTPCP"),
                                                                                      GiaTriNguonKhac = (double)GetValueObject(item, "GiaTriNguonKhac") + (double)GetValueObject(item1, "GiaTriNguonKhac"),
                                                                                      GiaTriVonVayNuocNgoai = (double)GetValueObject(item, "GiaTriVonVayNuocNgoai") + (double)GetValueObject(item1, "GiaTriVonVayNuocNgoai"),
                                                                                  })
                                                                             group item by new { item.IDDA }
                                                                             into item1
                                                                             select new
                                                                             {
                                                                                 IDDA = item1.Key.IDDA,
                                                                                 grpLuyKeVonGiaiNgan = item1.Select(x => new
                                                                                 {
                                                                                     GiaTriNguonNSTW = x.GiaTriNguonNSTW,
                                                                                     GiaTriNguonTPCP = x.GiaTriNguonTPCP,
                                                                                     GiaTriNguonKhac = x.GiaTriNguonKhac,
                                                                                     GiaTriVonVayNuocNgoai = x.GiaTriVonVayNuocNgoai
                                                                                 }).ToList<object>()
                                                                             }).ToDictionary(x => x.IDDA, x => x.grpLuyKeVonGiaiNgan);

                        Dictionary<int, List<object>> dicKHVN_1 = (from item in
                                                                    (from item in KHVN_ODA
                                                                     join item1 in KHVNDC_ODA on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                                                                     from item2 in lstDCKHV.DefaultIfEmpty()
                                                                     select new
                                                                     {
                                                                         IDDA = (int)GetValueObject(item, "IDDA"),
                                                                         KeHoachVonNam_NSTW = item2 == null ? GetValueObject(item, "KeHoachVonNam_NSTW") : GetValueObject(item2, "KeHoachVonNam_NSTW"),
                                                                         KeHoachVonNam_TPCP = item2 == null ? GetValueObject(item, "KeHoachVonNam_TPCP") : GetValueObject(item2, "KeHoachVonNam_TPCP"),
                                                                         KeHoachVonNam_Khac = item2 == null ? GetValueObject(item, "KeHoachVonNam_Khac") : GetValueObject(item2, "KeHoachVonNam_Khac"),
                                                                         KeHoachVonNam_NuocNgoai = item2 == null ? GetValueObject(item, "KeHoachVonNam_NuocNgoai") : GetValueObject(item2, "KeHoachVonNam_NuocNgoai"),
                                                                     })
                                                                   group item by new { item.IDDA }
                                                                   into item1
                                                                   select new
                                                                   {
                                                                       IDDA = item1.Key.IDDA,
                                                                       grpGiaTri = item1.Select(x => new
                                                                       {
                                                                           KeHoachVonNam_NSTW = x.KeHoachVonNam_NSTW,
                                                                           KeHoachVonNam_TPCP = x.KeHoachVonNam_TPCP,
                                                                           KeHoachVonNam_Khac = x.KeHoachVonNam_Khac,
                                                                           KeHoachVonNam_NuocNgoai = x.KeHoachVonNam_NuocNgoai,
                                                                       }).ToList<object>()
                                                                   }).ToDictionary(x => x.IDDA, x => x.grpGiaTri);

                        int rowLVNN = 12;

                        int a = 0;

                        int iLVNN;

                        List<int> l_LVNNs = new List<int>();

                        foreach (object oLVNN in lstDuAnCha)
                        {
                            string TenLinhVucNganhNghe = (string)GetValueObject(oLVNN, "LinhVucNganhNghe");

                            sheet.Cells[rowLVNN, 1].Value = Columns[a];
                            sheet.Cells[rowLVNN, 2].Value = TenLinhVucNganhNghe;

                            a++;

                            iLVNN = rowLVNN--;
                            rowLVNN++;
                            rowLVNN++;

                            l_LVNNs.Add(iLVNN);

                            sheet.Row(iLVNN).Style.Font.Bold = true;


                            List<object> lstTTDA = (List<object>)GetValueObject(oLVNN, "grpTinhTrangDuAn");

                            int iTTDA;

                            int b = 0;

                            List<int> l_TTDAs = new List<int>();

                            foreach (object oTTDA in lstTTDA)
                            {
                                string TenTinhTrangDuAn = (string)GetValueObject(oTTDA, "TinhTrangDuAn");

                                sheet.Cells[rowLVNN, 1].Value = SoLaMa[b];
                                sheet.Cells[rowLVNN, 2].Value = TenTinhTrangDuAn;

                                b++;

                                iTTDA = rowLVNN--;
                                rowLVNN++;
                                rowLVNN++;

                                sheet.Row(iTTDA).Style.Font.Bold = true;

                                l_TTDAs.Add(iTTDA);

                                int iDACha;

                                int c = 0;

                                List<object> lstDACha = (List<object>)GetValueObject(oTTDA, "grpDuAnCha");

                                List<int> l_DAChas = new List<int>();

                                foreach (object oDACha in lstDACha)
                                {
                                    c++;

                                    XuatDuAnODA(sheet, szDonViTinh, oDACha, c.ToString(), rowLVNN, TMDTLDADTSoQDBanDau, TMDTLTDTSoQDBanDau, TMDTLDADTTongGiaTriBanDau, TMDTLTDTTongGiaTriBanDau, TMDTLDADTSoQDSuaDoi, TMDTLTDTSoQDSuaDoi, TMDTLDADTTongGiaTriSuaDoi, TMDTLTDTTongGiaTriSuaDoi, dicLuyKeVonGiaiNgan, dicKHVN_1, dicUocGiaiNgan, dicGiaiNganNam);

                                    iDACha = rowLVNN--;
                                    rowLVNN++;
                                    rowLVNN++;

                                    l_DAChas.Add(iDACha);

                                    int IDDA = (int)GetValueObject(oDACha, "IDDA");

                                    if (dicDuAnCon.ContainsKey(IDDA))
                                    {
                                        List<object> lDuAnCon = dicDuAnCon[IDDA];

                                        foreach (object oDuAnCon in lDuAnCon)
                                        {
                                            XuatDuAnODA(sheet, szDonViTinh, oDuAnCon, "-", rowLVNN, TMDTLDADTSoQDBanDau, TMDTLTDTSoQDBanDau, TMDTLDADTTongGiaTriBanDau, TMDTLTDTTongGiaTriBanDau, TMDTLDADTSoQDSuaDoi, TMDTLTDTSoQDSuaDoi, TMDTLDADTTongGiaTriSuaDoi, TMDTLTDTTongGiaTriSuaDoi, dicLuyKeVonGiaiNgan, dicKHVN_1, dicUocGiaiNgan, dicGiaiNganNam);
                                            rowLVNN++;
                                        }
                                    }
                                }
                                UpdateTongTinhHinhODA(sheet, iTTDA, l_DAChas);
                            }
                            UpdateTongTinhHinhODA(sheet, iLVNN, l_TTDAs);
                        }
                        UpdateTongTinhHinhODA(sheet, 11, l_LVNNs);

                        int rowIndex = rowLVNN - 1;
                        using (ExcelRange rng = sheet.Cells["A11:AQ" + rowIndex])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.WrapText = true;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Column(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        sheet.Row(11).Style.Font.Bold = true;

                        string title = sheet.Cells["A2"].Value.ToString();
                        title = title.Replace("{{N}}", iNamPD.ToString());
                        sheet.Cells["A2"].Value = title;

                        sheet.Cells["AO4"].Value = "Đơn vị tính: " + szDonViTinh;
                        sheet.Cells["S5"].Value = "Lũy kế vốn giải ngân từ khởi công đến hết kế hoạch năm " + (iNamPD - 2).ToString();
                        sheet.Cells["Y5"].Value = "Kế hoạch năm " + (iNamPD - 1).ToString();
                        sheet.Cells["AE5"].Value = "Ước giải ngân KH năm " + (iNamPD - 1).ToString() + " từ 01/01/" + (iNamPD - 1).ToString() + " đến 31/01/" + (iNamPD).ToString();
                        sheet.Cells["AK5"].Value = "Dự kiến khả năng giải ngân kế hoạch năm " + (iNamPD).ToString();

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "ODA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();

                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        private void XuatDuAnODA(ExcelWorksheet sheet, string szDonViTinh, object oDA, string c, int rowDA,
            Dictionary<int, string> dicTMDTLDADTSoQDBanDau,
            Dictionary<int, string> dicTMDTLTDTSoQDBanDau,

            Dictionary<int, double> dicTMDTLDADTTongGiaTriBanDau,
            Dictionary<int, double> dicTMDTLTDTTongGiaTriBanDau,

            Dictionary<int, string> dicTMDTLDADTSoQDSuaDoi,
            Dictionary<int, string> dicTMDTLTDTSoQDSuaDoi,

            Dictionary<int, double> dicTMDTLDADTTongGiaTriSuaDoi,
            Dictionary<int, double> dicTMDTLTDTTongGiaTriSuaDoi,

            Dictionary<int, List<object>> dicLuyKeVonGiaiNgan,

            Dictionary<int, List<object>> dicKHVN_1,

            Dictionary<int, List<object>> dicUocGiaiNgan,
            Dictionary<int, List<object>> dicGiaiNganKeHoachNam
            )
        {
            int dDim = 1;
            if (szDonViTinh == "Tỷ Đồng")
            {
                dDim = 1000000000;
            }
            else if (szDonViTinh == "Triệu Đồng")
            {
                dDim = 1000000;
            }
            else if (szDonViTinh == "Nghìn Đồng")
            {
                dDim = 1000;
            }

            int IDDA = (int)GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)GetValueObject(oDA, "TenDuAn");

            sheet.Cells[rowDA, 1].Value = c;
            sheet.Cells[rowDA, 2].Value = TenDuAn;

            string szSo_QDBanDau = "";

            if (dicTMDTLDADTSoQDBanDau.ContainsKey(IDDA))
            {
                szSo_QDBanDau = dicTMDTLDADTSoQDBanDau[IDDA];

                sheet.Cells[rowDA, 5].Value = szSo_QDBanDau;
            }
            if (!dicTMDTLDADTSoQDBanDau.ContainsKey(IDDA) && dicTMDTLTDTSoQDBanDau.ContainsKey(IDDA))
            {
                szSo_QDBanDau = dicTMDTLTDTSoQDBanDau[IDDA];

                sheet.Cells[rowDA, 5].Value = szSo_QDBanDau;
            }

            double dTongGiaTriBanDau = 0;

            if (dicTMDTLDADTTongGiaTriBanDau.ContainsKey(IDDA))
            {
                dTongGiaTriBanDau = dicTMDTLDADTTongGiaTriBanDau[IDDA] / dDim;
                sheet.Cells[rowDA, 6].Value = dTongGiaTriBanDau;
            }
            if (!dicTMDTLDADTTongGiaTriBanDau.ContainsKey(IDDA) && dicTMDTLTDTTongGiaTriBanDau.ContainsKey(IDDA))
            {
                dTongGiaTriBanDau = dicTMDTLTDTTongGiaTriBanDau[IDDA] / dDim;
                sheet.Cells[rowDA, 6].Value = dTongGiaTriBanDau;
            }

            string szSoQDSuaDoi = "";

            if (dicTMDTLDADTSoQDSuaDoi.ContainsKey(IDDA))
            {
                szSoQDSuaDoi = dicTMDTLDADTSoQDSuaDoi[IDDA];
                sheet.Cells[rowDA, 12].Value = szSoQDSuaDoi;
            }
            if (!dicTMDTLDADTSoQDSuaDoi.ContainsKey(IDDA) && dicTMDTLTDTSoQDSuaDoi.ContainsKey(IDDA))
            {
                szSoQDSuaDoi = dicTMDTLTDTSoQDSuaDoi[IDDA];
                sheet.Cells[rowDA, 12].Value = szSoQDSuaDoi;
            }

            double dTongGiaTriSuaDoi = 0;

            if (dicTMDTLDADTTongGiaTriSuaDoi.ContainsKey(IDDA))
            {
                dTongGiaTriSuaDoi = dicTMDTLDADTTongGiaTriSuaDoi[IDDA] / dDim;
                sheet.Cells[rowDA, 13].Value = dTongGiaTriSuaDoi;
            }
            if (!dicTMDTLDADTTongGiaTriSuaDoi.ContainsKey(IDDA) && dicTMDTLTDTTongGiaTriSuaDoi.ContainsKey(IDDA))
            {
                dTongGiaTriSuaDoi = dicTMDTLTDTTongGiaTriSuaDoi[IDDA] / dDim;
                sheet.Cells[rowDA, 13].Value = dTongGiaTriSuaDoi;
            }


            if (dicLuyKeVonGiaiNgan.ContainsKey(IDDA))
            {
                List<object> lstLuyKeVonGiaiNgan = dicLuyKeVonGiaiNgan[IDDA];
                foreach (object oLuyKeVonGiaiNgan in lstLuyKeVonGiaiNgan)
                {
                    double GiaTriNguonNSTW = (double)GetValueObject(oLuyKeVonGiaiNgan, "GiaTriNguonNSTW") / dDim;
                    double GiaTriNguonTPCP = (double)GetValueObject(oLuyKeVonGiaiNgan, "GiaTriNguonTPCP") / dDim;
                    double GiaTriNguonKhac = (double)GetValueObject(oLuyKeVonGiaiNgan, "GiaTriNguonKhac") / dDim;
                    double GiaTriVonVayNuocNgoai = (double)GetValueObject(oLuyKeVonGiaiNgan, "GiaTriVonVayNuocNgoai") / dDim;

                    sheet.Cells[rowDA, 19].Formula = "=" + sheet.Cells[rowDA, 20] + "+" + sheet.Cells[rowDA, 24];
                    sheet.Cells[rowDA, 20].Formula = "=Sum(" + sheet.Cells[rowDA, 21] + ":" + sheet.Cells[rowDA, 23] + ")";
                    sheet.Cells[rowDA, 21].Value = GiaTriNguonNSTW;
                    sheet.Cells[rowDA, 22].Value = GiaTriNguonTPCP;
                    sheet.Cells[rowDA, 23].Value = GiaTriNguonKhac;
                    sheet.Cells[rowDA, 24].Value = GiaTriVonVayNuocNgoai;

                }
            }

            if (dicKHVN_1.ContainsKey(IDDA))
            {
                List<object> lstKHVN_1 = dicKHVN_1[IDDA];
                foreach (object oKHVN_1 in lstKHVN_1)
                {
                    double KeHoachVonNam_NSTW = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_NSTW");
                    double KeHoachVonNam_TPCP = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_TPCP");
                    double KeHoachVonNam_Khac = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_Khac");
                    double KeHoachVonNam_NuocNgoai = (double)GetValueObject(oKHVN_1, "KeHoachVonNam_NuocNgoai");

                    sheet.Cells[rowDA, 25].Formula = "=" + sheet.Cells[rowDA, 26] + "+" + sheet.Cells[rowDA, 30];
                    sheet.Cells[rowDA, 26].Formula = "=Sum(" + sheet.Cells[rowDA, 27] + ":" + sheet.Cells[rowDA, 29] + ")";
                    sheet.Cells[rowDA, 27].Value = KeHoachVonNam_NSTW;
                    sheet.Cells[rowDA, 28].Value = KeHoachVonNam_TPCP;
                    sheet.Cells[rowDA, 29].Value = KeHoachVonNam_Khac;
                    sheet.Cells[rowDA, 30].Value = KeHoachVonNam_NuocNgoai;
                }
            }

            if (dicUocGiaiNgan.ContainsKey(IDDA))
            {
                List<object> lstUocGiaiNgan = dicUocGiaiNgan[IDDA];

                foreach (object oUocGiaiNgan in lstUocGiaiNgan)
                {
                    double GiaTriNguonNSTW = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonNSTW") / dDim;
                    double GiaTriNguonTPCP = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonTPCP") / dDim;
                    double GiaTriNguonKhac = (double)GetValueObject(oUocGiaiNgan, "GiaTriNguonKhac") / dDim;
                    double GiaTriVonVayNuocNgoai = (double)GetValueObject(oUocGiaiNgan, "GiaTriVonVayNuocNgoai") / dDim;

                    sheet.Cells[rowDA, 31].Formula = "=" + sheet.Cells[rowDA, 32] + "+" + sheet.Cells[rowDA, 36];
                    sheet.Cells[rowDA, 32].Formula = "=Sum(" + sheet.Cells[rowDA, 33] + ":" + sheet.Cells[rowDA, 35] + ")";
                    sheet.Cells[rowDA, 33].Value = GiaTriNguonNSTW;
                    sheet.Cells[rowDA, 34].Value = GiaTriNguonTPCP;
                    sheet.Cells[rowDA, 35].Value = GiaTriNguonKhac;
                    sheet.Cells[rowDA, 36].Value = GiaTriVonVayNuocNgoai;
                }
            }

            if (dicGiaiNganKeHoachNam.ContainsKey(IDDA))
            {
                List<object> lstGiaiNganKeHoachNam = dicGiaiNganKeHoachNam[IDDA];

                foreach (object oGiaiNganKeHoachNam in lstGiaiNganKeHoachNam)
                {
                    double GiaTriNguonNSTW = (double)GetValueObject(oGiaiNganKeHoachNam, "GiaTriNguonNSTW") / dDim;
                    double GiaTriNguonTPCP = (double)GetValueObject(oGiaiNganKeHoachNam, "GiaTriNguonTPCP") / dDim;
                    double GiaTriNguonKhac = (double)GetValueObject(oGiaiNganKeHoachNam, "GiaTriNguonKhac") / dDim;
                    double GiaTriVonVayNuocNgoai = (double)GetValueObject(oGiaiNganKeHoachNam, "GiaTriVonVayNuocNgoai") / dDim;

                    sheet.Cells[rowDA, 37].Formula = "=" + sheet.Cells[rowDA, 38] + "+" + sheet.Cells[rowDA, 42];
                    sheet.Cells[rowDA, 38].Formula = "=Sum(" + sheet.Cells[rowDA, 39] + ":" + sheet.Cells[rowDA, 41] + ")";
                    sheet.Cells[rowDA, 39].Value = GiaTriNguonNSTW;
                    sheet.Cells[rowDA, 40].Value = GiaTriNguonTPCP;
                    sheet.Cells[rowDA, 41].Value = GiaTriNguonKhac;
                    sheet.Cells[rowDA, 42].Value = GiaTriVonVayNuocNgoai;
                }
            }
        }

        private void UpdateTongTinhHinhODA(ExcelWorksheet sheet, int row, List<int> lstInt)
        {
            if (lstInt.Count != 0)
            {
                string str = "";
                string str1 = "";
                string str2 = "";
                string str3 = "";
                string str4 = "";
                string str5 = "";
                string str6 = "";
                string str7 = "";
                string str8 = "";
                string str9 = "";
                string str10 = "";
                string str11 = "";
                string str12 = "";
                string str13 = "";
                string str14 = "";
                string str15 = "";
                string str16 = "";
                string str17 = "";
                string str18 = "";
                string str19 = "";
                string str20 = "";
                string str21 = "";
                string str22 = "";
                string str23 = "";
                string str24 = "";
                string str25 = "";

                foreach (var row1 in lstInt)
                {
                    str += "F" + row1.ToString() + "+";
                    str1 += "M" + row1.ToString() + "+";
                    str2 += "S" + row1.ToString() + "+";
                    str3 += "T" + row1.ToString() + "+";
                    str4 += "U" + row1.ToString() + "+";
                    str5 += "V" + row1.ToString() + "+";
                    str6 += "W" + row1.ToString() + "+";
                    str7 += "X" + row1.ToString() + "+";
                    str8 += "Y" + row1.ToString() + "+";
                    str9 += "Z" + row1.ToString() + "+";
                    str10 += "AA" + row1.ToString() + "+";
                    str11 += "AB" + row1.ToString() + "+";
                    str12 += "AC" + row1.ToString() + "+";
                    str13 += "AD" + row1.ToString() + "+";
                    str14 += "AE" + row1.ToString() + "+";
                    str15 += "AF" + row1.ToString() + "+";
                    str16 += "AG" + row1.ToString() + "+";
                    str17 += "AH" + row1.ToString() + "+";
                    str18 += "AI" + row1.ToString() + "+";
                    str19 += "AJ" + row1.ToString() + "+";
                    str20 += "AK" + row1.ToString() + "+";
                    str21 += "AL" + row1.ToString() + "+";
                    str22 += "AM" + row1.ToString() + "+";
                    str23 += "AN" + row1.ToString() + "+";
                    str24 += "AO" + row1.ToString() + "+";
                    str25 += "AP" + row1.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
                str1 = str1.Substring(0, str1.Length - 1);
                str2 = str2.Substring(0, str2.Length - 1);
                str3 = str3.Substring(0, str3.Length - 1);
                str4 = str4.Substring(0, str4.Length - 1);
                str5 = str5.Substring(0, str5.Length - 1);
                str6 = str6.Substring(0, str6.Length - 1);
                str7 = str7.Substring(0, str7.Length - 1);
                str8 = str8.Substring(0, str8.Length - 1);
                str9 = str9.Substring(0, str9.Length - 1);
                str10 = str10.Substring(0, str10.Length - 1);
                str11 = str11.Substring(0, str11.Length - 1);
                str12 = str12.Substring(0, str12.Length - 1);
                str13 = str13.Substring(0, str13.Length - 1);
                str14 = str14.Substring(0, str14.Length - 1);
                str15 = str15.Substring(0, str15.Length - 1);
                str16 = str16.Substring(0, str16.Length - 1);
                str17 = str17.Substring(0, str17.Length - 1);
                str18 = str18.Substring(0, str18.Length - 1);
                str19 = str19.Substring(0, str19.Length - 1);
                str20 = str20.Substring(0, str20.Length - 1);
                str21 = str21.Substring(0, str21.Length - 1);
                str22 = str22.Substring(0, str22.Length - 1);
                str23 = str23.Substring(0, str23.Length - 1);
                str24 = str24.Substring(0, str24.Length - 1);
                str25 = str25.Substring(0, str25.Length - 1);

                sheet.Cells["F" + row].Formula = "=Sum(" + str + ")";
                sheet.Cells["M" + row].Formula = "=Sum(" + str1 + ")";
                sheet.Cells["S" + row].Formula = "=Sum(" + str2 + ")";
                sheet.Cells["T" + row].Formula = "=Sum(" + str3 + ")";
                sheet.Cells["U" + row].Formula = "=Sum(" + str4 + ")";
                sheet.Cells["V" + row].Formula = "=Sum(" + str5 + ")";
                sheet.Cells["W" + row].Formula = "=Sum(" + str6 + ")";
                sheet.Cells["X" + row].Formula = "=Sum(" + str7 + ")";
                sheet.Cells["Y" + row].Formula = "=Sum(" + str8 + ")";
                sheet.Cells["Z" + row].Formula = "=Sum(" + str9 + ")";
                sheet.Cells["AA" + row].Formula = "=Sum(" + str10 + ")";
                sheet.Cells["AB" + row].Formula = "=Sum(" + str11 + ")";
                sheet.Cells["AC" + row].Formula = "=Sum(" + str12 + ")";
                sheet.Cells["AD" + row].Formula = "=Sum(" + str13 + ")";
                sheet.Cells["AE" + row].Formula = "=Sum(" + str14 + ")";
                sheet.Cells["AF" + row].Formula = "=Sum(" + str15 + ")";
                sheet.Cells["AG" + row].Formula = "=Sum(" + str16 + ")";
                sheet.Cells["AH" + row].Formula = "=Sum(" + str17 + ")";
                sheet.Cells["AI" + row].Formula = "=Sum(" + str18 + ")";
                sheet.Cells["AJ" + row].Formula = "=Sum(" + str19 + ")";
                sheet.Cells["AK" + row].Formula = "=Sum(" + str20 + ")";
                sheet.Cells["AL" + row].Formula = "=Sum(" + str21 + ")";
                sheet.Cells["AM" + row].Formula = "=Sum(" + str22 + ")";
                sheet.Cells["AN" + row].Formula = "=Sum(" + str23 + ")";
                sheet.Cells["AO" + row].Formula = "=Sum(" + str24 + ")";
                sheet.Cells["AP" + row].Formula = "=Sum(" + str25 + ")";
            }
        }

        #endregion

        #region Nguồn vốn huyện xã
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("NguonHuyenXa")]
        [HttpGet]
        public HttpResponseMessage ExportNguonHuyenXa(HttpRequestMessage request, int iIDDuAn, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = NguonHuyenXa(iIDDuAn, szLoaiBaoCao, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string NguonHuyenXa(int iIDDuAn, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TinhHinhThucHienKeHoachVonDauTuXDCBNam-NguonHuyenXa.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("TinhHinhThucHienKeHoachVonDauTuXDCBNam-NguonHuyenXa_" + namPD + "-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}

                        //string dvTinh = sheet.Cells["N3"].Value.ToString();
                        //dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        //sheet.Cells["H3"].Value = dvTinh;

                        sheet.Cells["N3"].Value = "Đơn vị tính: " + szDonViTinh;

                        if (szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            sheet.Cells["A1"].Value = "KẾ HOẠCH VỐN ĐẦU TƯ XDCB 6 THÁNG ĐẦU NĂM " + namPD + " CÁC NGUỒN VỐN DO CẤP HUYỆN, CẤP XÃ QUẢN LÝ";
                            sheet.Cells["G4"].Value = "Lũy kế vốn đã bố trí đến hết KH năm " + namPD;
                            sheet.Cells["H5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["I4"].Value = "Ước khối lượng thực hiện từ khởi công đến 30/6/ " + namPD;
                            sheet.Cells["J5"].Value = "Tr.đó: 6 tháng đầu năm " + namPD;
                            sheet.Cells["K4"].Value = "Ước giải ngân từ khởi công đến 30/6/ " + namPD;
                            sheet.Cells["L5"].Value = "Tr.đó: 6 tháng đầu năm " + namPD;
                            sheet.Cells["M4"].Value = "Ước thực hiện kế hoạch năm " + namPD;
                        }

                        if (szLoaiBaoCao == "Cả năm")
                        {
                            sheet.Cells["A1"].Value = "KẾ HOẠCH VỐN ĐẦU TƯ XDCB NĂM " + namPD + " CÁC NGUỒN VỐN DO CẤP HUYỆN, CẤP XÃ QUẢN LÝ";
                            sheet.Cells["G4"].Value = "Lũy kế vốn đã bố trí đến hết KH năm " + namPD;
                            sheet.Cells["H5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["I4"].Value = "Ước khối lượng thực hiện từ khởi công đến 31/12/ " + namPD;
                            sheet.Cells["J5"].Value = "Tr.đó: năm " + namPD;
                            sheet.Cells["K4"].Value = "Ước giải ngân từ khởi công đến 31/12/ " + namPD;
                            sheet.Cells["L5"].Value = "Tr.đó: năm " + namPD;
                            sheet.Cells["M4"].Value = "Ước thực hiện kế hoạch năm " + namPD;
                        }




                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Nguồn vốn xã
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("NguonXa")]
        [HttpGet]
        public HttpResponseMessage ExportMau06(HttpRequestMessage request, string szIDDuAn, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = NguonXa(szIDDuAn, szLoaiBaoCao, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string NguonXa(string szIDDuAn, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/TinhHinhThucHienKeHoachVonDauTuXDCBNam-NguonXa.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("TinhHinhThucHienKeHoachVonDauTuXDCBNam-NguonXa" + namPD + "-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            //string documentName = string.Format("BC_KetQuaLuaChonNhaThau_UBNDTinhPD_-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        if (szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            sheet.Cells["A1"].Value = "KẾ HOẠCH VỐN ĐẦU TƯ XDCB 6 THÁNG ĐẦU NĂM " + namPD + " - CÁC NGUỒN VỐN DO CẤP XÃ QUẢN LÝ";
                            sheet.Cells["H4"].Value = "Lũy kế vốn đã bố trí đến hết KH năm " + namPD;
                            sheet.Cells["I5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["J4"].Value = "Khối lượng thực hiện từ khởi công đến 30/06/" + namPD;
                            sheet.Cells["K5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["L4"].Value = "Giải ngân từ khởi công đến 30/06/" + namPD;
                            sheet.Cells["M5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["N4"].Value = "Dự kiến KH đầu tư trung hạn 5 năm " + namPD + " - " + (iNamPD + 4).ToString();
                            sheet.Cells["O4"].Value = "Dự kiến KH vốn năm " + (iNamPD + 1).ToString();
                        }

                        if (szLoaiBaoCao == "Cả năm")
                        {
                            sheet.Cells["A1"].Value = "KẾ HOẠCH VỐN ĐẦU TƯ XDCB NĂM " + namPD + " - CÁC NGUỒN VỐN DO CẤP XÃ QUẢN LÝ";
                            sheet.Cells["H4"].Value = "Lũy kế vốn đã bố trí đến hết KH năm " + namPD;
                            sheet.Cells["I5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["J4"].Value = "Khối lượng thực hiện từ khởi công đến 31/12/" + namPD;
                            sheet.Cells["K5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["L4"].Value = "Giải ngân từ khởi công đến 31/12/" + namPD;
                            sheet.Cells["M5"].Value = "Tr.đó: Kế hoạch năm " + namPD;
                            sheet.Cells["N4"].Value = "Dự kiến KH đầu tư trung hạn 5 năm " + namPD + " - " + (iNamPD + 4).ToString();
                            sheet.Cells["O4"].Value = "Dự kiến KH vốn năm " + (iNamPD + 1).ToString();
                        }

                        sheet.Cells["N3"].Value = "Đơn vị tính: " + szDonViTinh;

                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Dùng chung

        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }


        #endregion
    }
}
