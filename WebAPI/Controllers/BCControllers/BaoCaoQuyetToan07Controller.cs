﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan07")]
    public class BaoCaoQuyetToan07Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        private IHopDongService _hopDongService;
        private ITamUngHopDongService _tamUngHopDongService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        public BaoCaoQuyetToan07Controller(IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, IThanhToanHopDongService thanhToanHopDongService, ITamUngHopDongService tamUngHopDongService, IHopDongService hopDongService, IDanhMucNhaThauService danhMucNhaThauService, IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._danhMucNhaThauService = danhMucNhaThauService;
            this._hopDongService = hopDongService;
            this._tamUngHopDongService = tamUngHopDongService;
            this._thanhToanHopDongService = thanhToanHopDongService;
            this._thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
        }
        #region Mau07
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau07")]
        [HttpGet]
        public HttpResponseMessage ExportMau07(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau07(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau07(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-07-PL7.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc7-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // lay du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = duan.TenDuAn;
                        sheet.Cells["A2"].Style.WrapText = true;
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //danh muc nha thau
                        //lay ra tat ca cac dnah muc theo du an
                        var danhmucnhathaus = _danhMucNhaThauService.GetAll(iIDDuAn);
                        List<HopDong> hopdongs = new List<HopDong>();
                        foreach (var item in danhmucnhathaus)
                        {
                            var hopdong = _hopDongService.GetByIdNhaThau(item.IdNhaThau);
                            foreach (var item1 in hopdong)
                            {
                                hopdongs.Add(item1);
                            }
                        }
                        //List<TamUngHopDong> tamunghopdongs = new List<TamUngHopDong>();
                        List<ThanhToanHopDong> thanhtoanhopdongs = new List<ThanhToanHopDong>();
                        foreach (var item in hopdongs)
                        {
                            //var tamunghopdong = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamunghopdong)
                            //{
                            //    tamunghopdongs.Add(item1);
                            //}
                            var thanhtoanhopdong = _thanhToanHopDongService.GetByIdHopDong(item.IdHopDong);
                            foreach (var item1 in thanhtoanhopdong)
                            {
                                thanhtoanhopdongs.Add(item1);
                            }
                        }
                        int rowIndex = 9;
                        int index = 0;
                        foreach (var item in danhmucnhathaus)
                        {                            
                            int demsotamunghoacthanhtoan = 0;
                            //foreach (var item1 in tamunghopdongs)
                            //{
                            //    var hopdong2 = _hopDongService.GetByIdNhaThau(item.IdNhaThau);
                            //    int indexhopdong2 = 0;
                            //    foreach (var item2 in hopdong2)
                            //    {
                            //        if(item2.IdHopDong == item1.IdHopDong)
                            //        {
                            //            indexhopdong2 = 1;
                            //        }
                            //    }
                            //    if (indexhopdong2 == 1)
                            //    {
                            //        rowIndex++;
                            //        demsotamunghoacthanhtoan++;
                            //        sheet.InsertRow(rowIndex, 1);
                            //        sheet.Cells[rowIndex, 3].Value = "Tạm ứng " + item1.NgayTamUng?.ToString("dd/MM/yyy") + ". Hợp đồng: " + item1.HopDong?.TenHopDong;
                            //        sheet.Cells[rowIndex, 3].Style.WrapText = true;
                            //        sheet.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //    }
                            //}

                            foreach (var item1 in thanhtoanhopdongs)
                            {
                                var hopdong2 = _hopDongService.GetByIdNhaThau(item.IdNhaThau);
                                int indexhopdong2 = 0;
                                foreach (var item2 in hopdong2)
                                {
                                    if (item2.IdHopDong == item1.IdHopDong)
                                    {
                                        indexhopdong2 = 1;
                                    }
                                }
                                if (indexhopdong2 == 1)
                                {
                                    rowIndex++;
                                    demsotamunghoacthanhtoan++;
                                    sheet.InsertRow(rowIndex, 1);
                                    sheet.Cells[rowIndex, 3].Value = "Thanh toán " + item1.ThoiDiemThanhToan?.ToString("dd/MM/yyy") + ". Hợp đồng: " + item1.HopDong?.TenHopDong;
                                    sheet.Cells[rowIndex, 3].Style.WrapText = true;
                                    sheet.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    if (item1.DeNghiThanhToan != null)
                                    {
                                        sheet.Cells[rowIndex, 4].Value = item1.DeNghiThanhToan/dvt;
                                    }
                                    if (item1.DeNghiThanhToan == null)
                                    {
                                        sheet.Cells[rowIndex, 4].Value = item1.DeNghiThanhToan;
                                    }

                                    sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                                    double giatridatamungthanhtoan = 0;
                                    foreach(var item2 in item1.NguonVonGoiThauThanhToans)
                                    {
                                        giatridatamungthanhtoan += item2.GiaTri;
                                    }
                                    sheet.Cells[rowIndex, 5].Value = giatridatamungthanhtoan/dvt;
                                    sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                    sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[rowIndex, 4].Style.WrapText = true;
                                    sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    sheet.Cells[rowIndex, 5].Style.WrapText = true;
                                    //phan phai thu va phai tra
                                    if (item1.DeNghiThanhToan == null)
                                    {
                                        item1.DeNghiThanhToan = 0;
                                    }
                                    double giatritruoc =(double)item1.DeNghiThanhToan;
                                    double giatrisau = giatridatamungthanhtoan;
                                    double giatrithutra = giatritruoc - giatrisau;
                                    if (giatrithutra > 0)
                                    {
                                        sheet.Cells[rowIndex, 6].Value = giatrithutra/dvt;
                                        sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";
                                    }
                                    if (giatrithutra < 0)
                                    {
                                        sheet.Cells[rowIndex, 7].Value = giatrithutra*(-1)/dvt;
                                        sheet.Cells[rowIndex, 7].Style.Numberformat.Format = "#,##0";
                                    }
                                }
                            }
                            var thanhtoanchiphikhacs = _thanhToanChiPhiKhacService.GetByIdNhaThau(item.IdNhaThau);
                            foreach(var item1 in thanhtoanchiphikhacs)
                            {
                                rowIndex++;
                                demsotamunghoacthanhtoan++;
                                sheet.InsertRow(rowIndex, 1);
                                if (item1.LoaiThanhToan == "1")
                                {
                                    item1.LoaiThanhToan = "Chi phí QLDA";
                                }
                                if (item1.LoaiThanhToan == "2")
                                {
                                    item1.LoaiThanhToan = "Chi phí bảo hiểm";
                                }
                                if (item1.LoaiThanhToan == "3")
                                {
                                    item1.LoaiThanhToan = "Chi phí thẩm tra phê duyệt";
                                }
                                if (item1.LoaiThanhToan == "4")
                                {
                                    item1.LoaiThanhToan = "Chi phí khác";
                                }
                                sheet.Cells[rowIndex, 3].Value = item1.LoaiThanhToan + " " + item1.NgayThanhToan?.ToString("dd/MM/yyyy") + " " + item1.NoiDung;
                                sheet.Cells[rowIndex, 3].Style.WrapText = true;
                                sheet.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                double giatridathanhtoantamung = 0;
                                foreach(var item2 in item1.NguonVonThanhToanChiPhiKhacs)
                                {
                                    giatridathanhtoantamung += item2.GiaTri;
                                }
                                sheet.Cells[rowIndex, 5].Value = giatridathanhtoantamung/dvt;
                                sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 5].Style.WrapText = true;
                            }
                            if (demsotamunghoacthanhtoan > 0)
                            {
                                index++;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 1, rowIndex, 1].Merge = true;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 1].Value = index;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 2, rowIndex, 2].Merge = true;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 2].Value = item.TenNhaThau;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 2].Style.WrapText = true;
                                sheet.Cells[rowIndex - demsotamunghoacthanhtoan + 1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            }
                            sheet.Cells[10, 1, rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[10, 1, rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        }

                        string dvTinh = sheet.Cells["G5"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["G5"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 07/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
