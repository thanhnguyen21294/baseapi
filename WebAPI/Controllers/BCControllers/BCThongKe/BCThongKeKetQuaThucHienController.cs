﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCThongKe
{
    [Authorize]
    [RoutePrefix("api/BaocaoThongke")]
    public class BCThongKeKetQuaThucHienController : ApiControllerBase
    {
        IQLTD_KeHoachCongViecService _QLTD_KeHoachCongViecService;
        public BCThongKeKetQuaThucHienController(IErrorService errorService,
            IQLTD_KeHoachCongViecService QLTD_KeHoachCongViecService
            ) : base(errorService)
        {
            this._QLTD_KeHoachCongViecService = QLTD_KeHoachCongViecService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("ketquadathuchien")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoThongKeKetQuaDaThucHien(HttpRequestMessage request, string tungay, string denngay)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            //{
            //    return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            //}

            string documentName = BaoCaoThongKeKetQuaDaThucHien(tungay, denngay);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoThongKeKetQuaDaThucHien(string szTuNgay, string szDenNgay)
        {
            //string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/ThongKeKetQuaThucHien.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoKetQuaThucHien_" + szTuNgay + "_" + szDenNgay + ".xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }

                        var lstData = _QLTD_KeHoachCongViecService.GetCVDaThucHienNgayDenNgay(dicUserNhanVien, szTuNgay, szDenNgay);

                        sheet.Cells[2, 1].Value = sheet.Cells[2, 1].Value.ToString().Replace("{{tungay}}", szTuNgay).Replace("{{denngay}}", szDenNgay);

                        int rowIndex = 5;
                        int count = 0;
                        rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex);


                        using (ExcelRange rng = sheet.Cells["A5:D" + (rowIndex - 1)])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.Font.Size = 14;
                            rng.Style.WrapText = true;
                        }

                        sheet.Cells["B5:B" + rowIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                        sheet.Column(1).Width = 9;
                        sheet.Column(2).Width = 63;
                        sheet.Column(3).Width = 34;
                        sheet.Column(4).Width = 24;

                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<BaoCaoKetQuaThucHien> lstData, int count, int rowIndex)
        {
            foreach (var tb in lstData)
            {
                count++;

                sheet.Cells[rowIndex, 1].Value = BaoCaoDungChung.ToRoman(count);
                sheet.Cells[rowIndex, 2].Value = tb.TenDA;

                var lstUser = tb.Users.ToList();
                string szUser = "";

                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    szUser += sz + ", ";
                }
                if(szUser.IndexOf(",") > -1)
                {
                    szUser = szUser.Substring(0, szUser.Length - 2);
                }

                sheet.Cells[rowIndex, 3].Value = szUser;
                sheet.Row(rowIndex).Style.Font.Bold = true;
                rowIndex++;

                int iKH = 0;
                foreach (var kh in tb.grpKH)
                {
                    iKH++;
                    sheet.Cells[rowIndex, 1].Value = iKH;
                    sheet.Cells[rowIndex, 2].Value = kh.TenKH;
                    sheet.Row(rowIndex).Style.Font.Bold = true;
                    sheet.Row(rowIndex).Style.Font.Italic = true;
                    rowIndex++;

                    int iCV = 0;
                    foreach (var cv in kh.grpCV)
                    {
                        iCV++;
                        sheet.Cells[rowIndex, 1].Value = iKH + "." + iCV;
                        sheet.Cells[rowIndex, 2].Value = cv.TenCV;
                        sheet.Cells[rowIndex, 4].Value = cv.NgayHoanThanh.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        rowIndex++;
                    }
                }
            }
            return rowIndex;
        }
    }
}
