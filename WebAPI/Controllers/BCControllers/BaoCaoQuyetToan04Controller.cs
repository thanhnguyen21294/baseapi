﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan04")]
    public class BaoCaoQuyetToan04Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private ILapQuanLyDauTuService _lapQuanLyDauTuService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        private IHopDongService _hopDongService;
        public BaoCaoQuyetToan04Controller(IHopDongService hopDongService, IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, IErrorService errorService, IDuAnService duAnService, ILapQuanLyDauTuService lapQuanLyDauTuService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
            this._baoCaoTableService = baoCaoTableService;
            this._thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            this._hopDongService = hopDongService;
        }
        #region Mau04
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau04")]
        [HttpGet]
        public HttpResponseMessage ExportMau04(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau04(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau04(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-04-Mau_PL4.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc4-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();
                        var duan = _duAnService.GetById(iIDDuAn);
                        string tenDuan = sheet.Cells["A2"].Value.ToString();
                        tenDuan = tenDuan.Replace("{{tenduan}}", duan.TenDuAn);
                        sheet.Cells["A2"].Value = tenDuan;
                        sheet.Cells["A2"].Style.WrapText = true;
                        sheet.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        int index = 0;
                        double tongso = 0;
                        double gpmb = 0;
                        double xd = 0;
                        double tb = 0;
                        double qlda = 0;
                        double qldatheoab = 0;
                        double tv = 0;
                        double khac = 0;
                        double khactheoab = 0;
                        var lapquanlydautu = _lapQuanLyDauTuService.GetNewestTheoDuAn(iIDDuAn);
                        if (lapquanlydautu != null)
                        {
                            if (lapquanlydautu.TongGiaTri == null)
                            {
                                lapquanlydautu.TongGiaTri = 0;
                            }
                            tongso += (double)lapquanlydautu.TongGiaTri;
                            if (lapquanlydautu.GiaiPhongMatBang == null)
                            {
                                lapquanlydautu.GiaiPhongMatBang = 0;
                            }
                            gpmb += (double)lapquanlydautu.GiaiPhongMatBang;
                            if (lapquanlydautu.XayLap == null)
                            {
                                lapquanlydautu.XayLap = 0;
                            }
                            xd += (double)lapquanlydautu.XayLap;
                            if (lapquanlydautu.ThietBi == null)
                            {
                                lapquanlydautu.ThietBi = 0;
                            }
                            tb += (double)lapquanlydautu.ThietBi;
                            if (lapquanlydautu.QuanLyDuAn == null)
                            {
                                lapquanlydautu.QuanLyDuAn = 0;
                            }
                            qlda += (double)lapquanlydautu.QuanLyDuAn;
                            if (lapquanlydautu.TuVan == null)
                            {
                                lapquanlydautu.TuVan = 0;
                            }
                            tv += (double)lapquanlydautu.TuVan;
                            if (lapquanlydautu.Khac == null)
                            {
                                lapquanlydautu.Khac = 0;
                            }
                            khac += (double)lapquanlydautu.Khac;
                            if (lapquanlydautu.DuPhong == null)
                            {
                                lapquanlydautu.DuPhong = 0;
                            }
                            khac += (double)lapquanlydautu.DuPhong;
                        }
                        var thanhtoanchiphikhac = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in thanhtoanchiphikhac)
                        {
                            if (item.LoaiThanhToan == null)
                            {
                                foreach (var item1 in item.NguonVonThanhToanChiPhiKhacs)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khactheoab += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.LoaiThanhToan.Contains("1") && item.NgayThanhToan != null)
                            {
                                foreach (var item1 in item.NguonVonThanhToanChiPhiKhacs)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        qlda += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.NgayThanhToan != null && !item.LoaiThanhToan.Contains("1"))
                            {
                                foreach (var item1 in item.NguonVonThanhToanChiPhiKhacs)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khactheoab += item1.GiaTri;
                                    }
                                }
                            }
                        }
                        sheet.Cells[5, 3].Value = tongso / dvt;
                        sheet.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        int rowIndex = 6;
                        //"Bồi thường, hỗ trợ, TĐC";
                        var hopdong1 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 3);
                        double gt1 = 0;
                        int rowgt1 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = gpmb / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong1)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt1 += (double)item1.GiaTriQuyetToan; ;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt / dvt;
                            sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt1, 4].Value = gt1 / dvt;
                        sheet.Cells[rowgt1, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Xây dựng
                        var hopdong2 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 1);
                        rowIndex++;
                        double gt2 = 0;
                        int rowgt2 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = xd / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong2)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt2 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt / dvt;
                            sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt2, 4].Value = gt2 / dvt;
                        sheet.Cells[rowgt2, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Thiết bị
                        var hopdong3 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 2);
                        rowIndex++;
                        double gt3 = 0;
                        int rowgt3 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = tb / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong3)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt3 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt / dvt;
                            sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt3, 4].Value = gt3 / dvt;
                        sheet.Cells[rowgt3, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Quản lí dự án
                        rowIndex++;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = qlda / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 4].Value = qldatheoab / dvt;
                        sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        //Tư vấn
                        var hopdong4 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 5);
                        rowIndex++;
                        double gt4 = 0;
                        int rowgt4 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = tv / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong4)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt4 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt / dvt;
                            sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt4, 4].Value = gt4 / dvt;
                        sheet.Cells[rowgt4, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Khác
                        var hopdong5 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 4);
                        rowIndex++;
                        double gt5 = 0;
                        index = 0;
                        int rowgt5 = rowIndex;
                        sheet.Cells[rowIndex, 3].Value = khac / dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong5)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt5 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt / dvt;
                            sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt5, 4].Value = (gt5 + khactheoab) / dvt;
                        sheet.Cells[rowgt5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        string dvTinh = sheet.Cells["D3"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["D3"].Value = dvTinh;
                        sheet.Cells[5, 4].Value = (gt1 + gt2 + gt3 + gt4 + gt5 + khactheoab + qldatheoab) / dvt;
                        sheet.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        pck.SaveAs(new FileInfo(fullPath));

                        _baoCaoTableService.AddBaoCao(documentName, "Mau 04/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }
        #endregion
    }
}
