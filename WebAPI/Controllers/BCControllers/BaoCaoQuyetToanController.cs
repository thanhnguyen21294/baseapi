﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan")]
    public class BaoCaoQuyetToanController : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IHopDongService _hopDongService;
        private IThanhToanChiPhiKhacService _thanhToanChiPhiKhacService;
        private ILapQuanLyDauTuService _lapQuanLyDauTuService;
        private IVanBanDuAnService _vanBanDuAnService;
        private INguonVonService _nguonVonService;
        private IThanhToanHopDongService _thanhToanHopDongService;
        private INguonVonGoiThauThanhToanService _nguonVonGoiThauThanhToanService;
        private INguonVonGoiThauTamUngService _nguonVonGoiThauTamUngService;
        private ITamUngHopDongService _tamUngHopDongService;
        private INguonVonThanhToanChiPhiKhacService _nguonVonThanhToanChiPhiKhacService;
        private IQuyetToanHopDongService _quyetToanHopDongService;
        private IChuDauTuService _chuDauTuService;
        private IQTDA01Service _qtda01Service;
        private IQTDA10Service _qtda10Service;
        private IQuyetToanDuAnService _quyetToanDuAnService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        private INguonVonDuAnService _nguonVonDuAnService;


        public BaoCaoQuyetToanController(IErrorService errorService, IDuAnService duAnService, IBaoCaoTableService baoCaoTableService, IHopDongService hopDongService,
            IThanhToanChiPhiKhacService thanhToanChiPhiKhacService, ILapQuanLyDauTuService lapQuanLyDauTuService, IVanBanDuAnService vanBanDuAnService, INguonVonService nguonVonService,
            IThanhToanHopDongService thanhToanHopDongService, INguonVonGoiThauThanhToanService nguonVonGoiThauThanhToanService, ITamUngHopDongService tamUngHopDongService,
            INguonVonGoiThauTamUngService nguonVonGoiThauTamUngService, IQTDA01Service qtda01Service, IChuDauTuService chuDauTuService, INguonVonThanhToanChiPhiKhacService nguonVonThanhToanChiPhiKhacService,
            IQuyetToanHopDongService quyetToanHopDongService, INguonVonDuAnService nguonVonDuAnService,
            IQuyetToanDuAnService quyetToanDuAnService,IDanhMucNhaThauService danhMucNhaThauService, IQTDA10Service qtda10Service) : base(errorService)

        {
            this._duAnService = duAnService;
            this._chuDauTuService = chuDauTuService;
            this._qtda01Service = qtda01Service;
            this._baoCaoTableService = baoCaoTableService;
            this._hopDongService = hopDongService;
            this._thanhToanChiPhiKhacService = thanhToanChiPhiKhacService;
            this._lapQuanLyDauTuService = lapQuanLyDauTuService;
            this._vanBanDuAnService = vanBanDuAnService;
            this._thanhToanHopDongService = thanhToanHopDongService;
            this._nguonVonGoiThauThanhToanService = nguonVonGoiThauThanhToanService;
            this._nguonVonService = nguonVonService;
            this._nguonVonGoiThauTamUngService = nguonVonGoiThauTamUngService;
            this._tamUngHopDongService = tamUngHopDongService;
            this._qtda10Service = qtda10Service;
            this._nguonVonThanhToanChiPhiKhacService = nguonVonThanhToanChiPhiKhacService;
            this._quyetToanHopDongService = quyetToanHopDongService;
            this._quyetToanDuAnService = quyetToanDuAnService;
            this._danhMucNhaThauService = danhMucNhaThauService;
            this._nguonVonDuAnService = nguonVonDuAnService;
        }

        #region Mau01
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau01")]
        [HttpGet]
        public HttpResponseMessage ExportMau01(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau01(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau01(int iIDDuAn, string szDonViTinh)
        {
            int dvt = 1;
            if (szDonViTinh == "Đồng")
            {
                dvt = 1;
            }
            if (szDonViTinh == "Nghìn Đồng")
            {
                dvt = 1000;
            }
            if (szDonViTinh == "Triệu Đồng")
            {
                dvt = 1000000;
            }
            if (szDonViTinh == "Tỷ Đồng")
            {
                dvt = 1000000000;
            }
            int indexBT=0, indexXD=0, indexTB=0, indexQLDA=0, indexTV=0, indexCPK = 0;
            DuAn duan = _duAnService.GetById(iIDDuAn);
            ChuDauTu chuDauTu = new ChuDauTu();
            if (duan.IdChuDauTu!=null)
            {
                int idChuDauTu = duan.IdChuDauTu ?? 0;
               chuDauTu = _chuDauTuService.GetById(idChuDauTu);
            }
             
             DuAn duanCha = new DuAn();
            if(duan.IdDuAnCha!=null)
            {
                int idDuAnCha = duan.IdDuAnCha ?? 0;
               duanCha= _duAnService.GetById(idDuAnCha);
            }
            string[] ndChiPhis = new string[] { "Bồi thường, hỗ trợ, TĐC", "Xây dựng", "Thiết bị", "Quản lý dự án", "Tư vấn", "Chi phí khác" };
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-01-MauPhuLuc1.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc1-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            //try
            //{
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}
                        string tenDuAn = sheet.Cells["B3"].Value.ToString();
                        string tencongTrinh = sheet.Cells["B4"].Value.ToString();
                        if(duan.IdDuAnCha!=null)
                        {
                            tenDuAn = tenDuAn.Replace("{{tenduan}}", duanCha.TenDuAn + "");
                            tencongTrinh = tencongTrinh.Replace("{{tentieuduan}}", duan.TenDuAn + "");
                        }
                        else
                        {
                            tenDuAn = tenDuAn.Replace("{{tenduan}}", duan.TenDuAn + "");
                            tencongTrinh = tencongTrinh.Replace("{{tentieuduan}}", "");
                        }
                        string tenchuDauTu = sheet.Cells["B5"].Value.ToString();
                        if (duan.IdChuDauTu!=null)
                        {
                            tenchuDauTu= tenchuDauTu.Replace("{{chudautu}}", chuDauTu.TenChuDauTu + "");
                        }
                        else
                            tenchuDauTu= tenchuDauTu.Replace("{{chudautu}}","");
                        sheet.Cells["B3"].Value = tenDuAn;
                        sheet.Cells["B4"].Value = tencongTrinh;
                        sheet.Cells["B5"].Value = tenchuDauTu;
                        string tenCoQuanPheDuyet = sheet.Cells["B6"].Value.ToString();
                        tenCoQuanPheDuyet = tenCoQuanPheDuyet.Replace("{{cqpheduyet}}", "");
                        sheet.Cells["B6"].Value = tenCoQuanPheDuyet;
                        string diaDiem = sheet.Cells["B7"].Value.ToString();
                        diaDiem = diaDiem.Replace("{{diadiemxaydung}}", duan.DiaDiem);
                         sheet.Cells["B7"].Value = diaDiem;
                    string quyMo = sheet.Cells["B8"].Value.ToString();
                        quyMo = quyMo.Replace("{{quymo}}", duan.QuyMoNangLuc);
                        sheet.Cells["B8"].Value = quyMo;
                        sheet.Cells["B10"].Value = "Thời gian khởi công - hoàn thành:" + duan.ThoiGianThucHien;
                      string donViTinhH12= sheet.Cells["H12"].Value.ToString();
                    donViTinhH12 = donViTinhH12.Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["H12"].Value = donViTinhH12;
                    var nguonVonDauTus= _qtda01Service.GetNguonVonDauTu(iIDDuAn);
                        var chiPhiDauTuFromTTHD=_qtda01Service.GetChiPhiDauTuFromQTHD(iIDDuAn);
                        var chiPhiDauTuFromTTCPK = _qtda01Service.GetChiPhiDauTuFromTTCPK(iIDDuAn);
                        var quyetToanDuAn = _qtda01Service.GetQuyetToanByDuAn(iIDDuAn);
                        int index = 17;
                        double tongGiaTriDauTu = 0, tongGiaTriKH = 0, tongGiaTriTT = 0;
                        sheet.InsertRow(17, nguonVonDauTus.Count);
                              
                    foreach (var nvdt in nguonVonDauTus)
                        {
                        
                        sheet.Cells["E" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["G" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["I" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["E" + index].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["G" + index].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["I" + index].Style.Numberformat.Format = "#,##0";
                        string tenCotB = "B" + index;
                            string tenCotD = "D" + index;
                            string tenCotE = "E" + index;
                            string tenCotF = "F" + index;
                            string tenCotG = "G" + index;
                            string tenCotH = "H" + index;
                            string tenCotI = "I" + index;
                            string tenCotJ = "J" + index;
                        sheet.Cells[tenCotB+":"+tenCotD].Merge = true;
                        sheet.Cells[tenCotE + ":" + tenCotF].Merge = true;
                        sheet.Cells[tenCotG + ":" + tenCotH].Merge = true;
                        sheet.Cells[tenCotI + ":" + tenCotJ].Merge = true;
                        sheet.Cells[tenCotB + ":" + tenCotJ].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[tenCotB+":"+ tenCotJ].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[tenCotJ].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        string tenNguonVon = (string)GetValueObject(nvdt, "TenNguonVon");
                            sheet.Cells["B" + index].Value = tenNguonVon;
                            double giaTriNguonVon = (double)GetValueObject(nvdt, "GiaTriNV");
                        tongGiaTriDauTu += giaTriNguonVon;
                            sheet.Cells["E" + index].Value = giaTriNguonVon/dvt;
                            double giaTriKH = (double)GetValueObject(nvdt, "GiaTriKH");
                            tongGiaTriKH += giaTriKH;
                            sheet.Cells["G" + index].Value = giaTriKH/dvt;
                            double giaTriTT = (double)GetValueObject(nvdt, "GiaTriTT");
                            tongGiaTriTT += giaTriTT;
                            sheet.Cells["I" + index].Value = giaTriTT/dvt;
                            index++;
                        }

                    sheet.Cells["E16:I16"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells["E16:I16"].Style.Numberformat.Format = "#,##0";
                    sheet.Cells["E16"].Value = tongGiaTriDauTu/dvt;
                    sheet.Cells["G16"].Value = tongGiaTriKH/dvt;
                    sheet.Cells["I16"].Value = tongGiaTriTT/dvt;
                        index++;
                        string donViTinh= sheet.Cells["H" +index].Value.ToString();
                        donViTinh = donViTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["H" + index].Value = donViTinh;
                        int indexTongCongCP = index + 3;
                        index=indexTongCongCP+1;
                        double tongDuToanDuocDuyet = 0;
                        double tongDeNghiQuyetToan = 0;
                        for(int i=0;i<ndChiPhis.Length;i++)
                        {
                            switch (i)
                            {
                                case 0:
                                     double?  giaTriGiaiPhongMatBang= (double?)GetValueObject(quyetToanDuAn, "GiaiPhongMatBang");
                                     sheet.Cells["E" + index].Value = giaTriGiaiPhongMatBang/dvt;
                                tongDuToanDuocDuyet += giaTriGiaiPhongMatBang ?? 0;
                                indexBT = index;
                                     break;
                                case 1:
                                 double? giaTriXayLap= (double?)GetValueObject(quyetToanDuAn, "XayLap");
                                sheet.Cells["E" + index].Value = giaTriXayLap/dvt;
                                tongDuToanDuocDuyet += giaTriXayLap ?? 0;
                                indexXD = index;
                                    break;
                                case 2:
                                double? giaTriThietBi = (double?)GetValueObject(quyetToanDuAn, "ThietBi");
                                    sheet.Cells["E" + index].Value = giaTriThietBi/dvt;
                                tongDuToanDuocDuyet += giaTriThietBi ?? 0;
                                indexTB = index;
                                    break;
                                case 3:
                                double? giaTriQLDA = (double?)GetValueObject(quyetToanDuAn, "QuanLyDuAn");
                                sheet.Cells["E" + index].Value = giaTriQLDA/dvt;
                                tongDuToanDuocDuyet += giaTriQLDA ?? 0;
                                indexQLDA = index;
                                    break;
                                case 4:
                                double? giaTriTuVan=(double?)GetValueObject(quyetToanDuAn, "TuVan");
                                sheet.Cells["E" + index].Value = giaTriTuVan/dvt;
                                tongDuToanDuocDuyet += giaTriTuVan ?? 0;
                                indexTV = index;
                                    break;
                                case 5:
                                double? giaTriKhac= (double?)GetValueObject(quyetToanDuAn, "Khac");
                                sheet.Cells["E" + index].Value = giaTriKhac/dvt;
                                tongDuToanDuocDuyet += giaTriKhac ?? 0;
                                indexCPK = index;
                                    break;
                            }
                           
                              
                            sheet.Cells["B" + index].Value =i+1;
                            sheet.Cells["C" + index].Value = ndChiPhis[i];
                            index++;
                        }
                    double giaTriTMDT = tongDuToanDuocDuyet / dvt;
                        sheet.Cells["E" + indexTongCongCP].Value = giaTriTMDT;
                    string  b9Value= sheet.Cells["B9"].Value.ToString();
                    
                    string stGiaTriTMDT  =String.Format("{0:0,0}", giaTriTMDT);
                   
                    b9Value = b9Value.Replace("{{tmdtduocduyet}}", stGiaTriTMDT);

                    sheet.Cells["B9"].Value = b9Value;
                        foreach (var item in chiPhiDauTuFromTTHD)
                        {
                            int? loaiGoiThau = (int?)GetValueObject(item, "LoaiGoiThau");
                            switch(loaiGoiThau)
                            {
                                case 1:
                                    {
                                        double giaTriXD= (double)GetValueObject(item, "TongGiaTri");
                                        sheet.Cells["G" + indexXD].Value = giaTriXD/dvt;
                                        sheet.Cells["I" + indexXD].Value = giaTriXD/dvt - double.Parse(sheet.Cells["E" + indexXD].Value.ToString());
                                        tongDeNghiQuyetToan += giaTriXD;
                                        break;
                                    }
                                case 2:
                                    {
                                    double giaTriTB= (double)GetValueObject(item, "TongGiaTri");
                                    sheet.Cells["G" + indexTB].Value = giaTriTB/dvt;
                                    sheet.Cells["I" + indexTB].Value = giaTriTB/dvt - double.Parse(sheet.Cells["E" + indexTB].Value.ToString());
                                    tongDeNghiQuyetToan += giaTriTB;
                                        break;
                                    }
                                case 3:
                                    {
                                    double giaTriBT =(double)GetValueObject(item, "TongGiaTri");
                                    sheet.Cells["G" + indexBT].Value = giaTriBT/dvt;
                                    sheet.Cells["I" + indexBT].Value = giaTriBT/dvt - double.Parse(sheet.Cells["E" + indexBT].Value.ToString());
                                    tongDeNghiQuyetToan += giaTriBT;
                                        break;
                                    }
                                case 5:
                                    {
                                       double giaTriTV= (double)GetValueObject(item, "TongGiaTri");
                                    sheet.Cells["G" + indexTV].Value = giaTriTV/dvt;
                                    sheet.Cells["I" + indexTV].Value = giaTriTV/dvt - double.Parse(sheet.Cells["E" + indexTV].Value.ToString());
                                    tongDeNghiQuyetToan += giaTriTV;
                                        break;
                                    }

                            }

                        }
                    
                    double giaTriCPK = 0;
                        foreach ( var item in chiPhiDauTuFromTTCPK)
                        {
                            string loaiThanhToan = (string)GetValueObject(item, "LoaiThanhToan");
                            if(loaiThanhToan=="1")
                            {
                            double giaTriQLDA = (double)GetValueObject(item, "TongGiaTri");
                            sheet.Cells["G" + indexQLDA].Value = giaTriQLDA / dvt;
                            sheet.Cells["I" + indexQLDA].Value = giaTriQLDA / dvt - double.Parse(sheet.Cells["E" + indexQLDA].Value.ToString());
                            tongDeNghiQuyetToan += giaTriQLDA;
                            
                            }
                            else
                            {
                            double giaTri = (double)GetValueObject(item, "TongGiaTri");
                            giaTriCPK +=giaTri ;
                            tongDeNghiQuyetToan += giaTri;
                           
                            }
                        }
                    sheet.Cells["G" + indexCPK].Value = giaTriCPK / dvt;
                    sheet.Cells["I" + indexCPK].Value = giaTriCPK / dvt - double.Parse(sheet.Cells["E" + indexCPK].Value.ToString());
                    sheet.Cells["G" + indexTongCongCP].Value = tongDeNghiQuyetToan/dvt;
                       sheet.Cells["I" + indexTongCongCP].Value = tongDeNghiQuyetToan/dvt - tongDuToanDuocDuyet/dvt;
                    index += 7;
                    sheet.Cells["H" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells["H" + index].Style.Numberformat.Format = "#,##0";
                    sheet.Cells["H" + index].Value = tongDeNghiQuyetToan;
                    index++;
                    sheet.Cells["H" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells["H" + index].Style.Numberformat.Format = "#,##0";
                    sheet.Cells["H" + index].Value= tongDeNghiQuyetToan;
                        
                        pck.SaveAs(new FileInfo(fullPath));
                    _baoCaoTableService.AddBaoCao(documentName, "Mau 01/QTDA", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                    return documentName;
                }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var TenGoiThau = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return TenGoiThau;
        }
        #endregion

        #region Mau02
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau02")]
        [HttpGet]
        public HttpResponseMessage ExportMau02(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau02(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau02(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-02-CacVanBanPhapLy.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc2-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        var duan = _duAnService.GetById(iIDDuAn);
                        sheet.Cells["A3"].Value = duan.TenDuAn;
                        sheet.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        //Lấy văn bản dự án
                        var vbda = _vanBanDuAnService.GetByIdDuAn(iIDDuAn);
                        int rowIndex = 6;
                        int index = 0;
                        foreach(var item in vbda)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenVanBan;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            sheet.Cells[rowIndex, 3].Value = item.SoVanBan + " " + item.NgayBanHanh?.ToString("dd/MM/yyyy");
                            sheet.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            sheet.Cells[rowIndex, 4].Value = item.CoQuanBanHanh;
                            sheet.Cells[rowIndex, 6].Value = item.GhiChu;

                        }

                        sheet.Cells["F4"].Value = "Đơn vị tính: " + szDonViTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 02/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau03
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau03")]
        [HttpGet]
        public HttpResponseMessage ExportMau03(HttpRequestMessage request, int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau03(iIDDuAn, szNguonVon, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau03(int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-03-Mau_TinhHinhSuDungVon.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc3-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //Tên dự án
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        if (duan.IdDuAnCha != null)
                        {
                            var duancha = _duAnService.GetByIdForBaoCao((int)duan.IdDuAnCha);
                            sheet.Cells["A3"].Value = "Tên dự án: " + duancha.TenDuAn;
                            sheet.Cells["A4"].Value = "Tên công trình, hạng mục công trình: " + duan.TenDuAn;
                        }
                        if (duan.IdDuAnCha == null)
                        {
                            sheet.Cells["A3"].Value = "Tên dự án: " + duan.TenDuAn;
                            sheet.Cells["A4"].Value = "Tên công trình, hạng mục công trình: ";
                        }

                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //Chủ đầu tư
                        sheet.Cells["A5"].Value = "Chủ đầu tư: " + duan.ChuDauTu?.TenChuDauTu;

                        //Nguồn vốn
                        var nguonvon = _nguonVonService.GetById(szNguonVon);
                        sheet.Cells["A2"].Value = "Nguồn vốn: " + nguonvon.TenNguonVon;
                        var thanhtoanhopdong = _thanhToanHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        //thanh toans KLTH
                        double thanhtoanKLTH = 0;
                        foreach (var item in thanhtoanhopdong)
                        {
                            var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                            foreach (var item1 in nguonvongoithauthanhtoan)
                            {
                                thanhtoanKLTH += item1.GiaTri;
                            }
                        }
                        sheet.Cells["C13"].Value = thanhtoanKLTH / dvt;
                        sheet.Cells["D13"].Value = thanhtoanKLTH / dvt;
                        sheet.Cells["G13"].Value = thanhtoanKLTH / dvt;
                        sheet.Cells["F13"].Value = thanhtoanKLTH / dvt;
                        sheet.Cells["I13"].Value = 0;
                        //tạm ứng
                        double tamung = 0;
                        var tamunghopdong = _tamUngHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        foreach (var item in tamunghopdong)
                        {
                            var tamung1 = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                            foreach (var item1 in tamung1)
                            {
                                tamung += item1.GiaTri;
                            }
                        }

                        sheet.Cells["E13"].Value = tamung / dvt;
                        sheet.Cells["H13"].Value = tamung / dvt;

                        // chi tiết thanh toán tạm ứng
                        //lấy ra năm
                        int demnamthanhtoan = 0;
                        int[] namthanhtoan = new int[1000];

                        foreach (var item in thanhtoanhopdong)
                        {
                            if (item.ThoiDiemThanhToan != null)
                            {

                                var nam = item.ThoiDiemThanhToan.Value.Year;
                                var indexnam = 0;
                                for (var i = 0; i < namthanhtoan.Length; i++)
                                {
                                    if (namthanhtoan[i] == nam)
                                    {
                                        indexnam = 1;
                                    }
                                }
                                if (indexnam != 1)
                                {
                                    demnamthanhtoan++;
                                    namthanhtoan[demnamthanhtoan] = nam;
                                }

                            }

                        }
                        var rowIndex = 14;
                        for (var i = 0; i < namthanhtoan.Length; i++)
                        {
                            if (namthanhtoan[i] > 0)
                            {
                                rowIndex++;
                                sheet.InsertRow(rowIndex, 1);
                                sheet.Cells[rowIndex, 2].Value = namthanhtoan[i];
                                sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                //tongsotheoanm
                                double tongsotheonam = 0;
                                foreach (var item in thanhtoanhopdong)
                                {
                                    if (item.ThoiDiemThanhToan != null)
                                    {
                                        if (item.ThoiDiemThanhToan.Value.Year == namthanhtoan[i])
                                        {
                                            var nguonvongt = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                                            foreach (var item1 in nguonvongt)
                                            {
                                                tongsotheonam += item1.GiaTri;
                                            }
                                        }
                                    }
                                }
                                sheet.Cells[rowIndex, 3].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 4].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 6].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 7].Value = tongsotheonam / dvt;
                                sheet.Cells[rowIndex, 9].Value = 0;
                                sheet.Cells[rowIndex, 3].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 7].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 9].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                //tamungtheonam
                                double tamungtheonam = 0;
                                foreach (var item in tamunghopdong)
                                {
                                    if (item.NgayTamUng != null)
                                    {
                                        if (item.NgayTamUng.Value.Year == namthanhtoan[i])
                                        {
                                            var nguonvontu = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                                            foreach (var item1 in nguonvontu)
                                            {
                                                tamungtheonam += item1.GiaTri;
                                            }
                                        }
                                    }
                                }
                                sheet.Cells[rowIndex, 5].Value = tamungtheonam / dvt;
                                sheet.Cells[rowIndex, 8].Value = tamungtheonam / dvt;
                                sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 8].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[rowIndex, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[rowIndex, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            }

                        }

                        string dvTinh = sheet.Cells["H9"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["H9"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 03/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau04
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau04")]
        [HttpGet]
        public HttpResponseMessage ExportMau04(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau04(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau04(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-04-Mau_PL4.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc4-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();
                        var duan = _duAnService.GetById(iIDDuAn);                   
                        string tenDuan = sheet.Cells["A2"].Value.ToString();
                        tenDuan = tenDuan.Replace("{{tenduan}}", duan.TenDuAn);
                        sheet.Cells["A2"].Value = tenDuan;
                        sheet.Cells["A2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        int index = 0;
                        double tongso = 0;
                        double gpmb = 0;
                        double xd = 0;
                        double tb = 0;
                        double qlda = 0;
                        double qldatheoab = 0;
                        double tv = 0;
                        double khac = 0;
                        double khactheoab = 0;
                        var lapquanlydautu = _lapQuanLyDauTuService.GetNewestTheoDuAn(iIDDuAn);
                        if (lapquanlydautu != null)
                        {
                            if (lapquanlydautu.TongGiaTri == null)
                            {
                                lapquanlydautu.TongGiaTri = 0;
                            }
                            tongso +=(double)lapquanlydautu.TongGiaTri;
                            if (lapquanlydautu.GiaiPhongMatBang == null)
                            {
                                lapquanlydautu.GiaiPhongMatBang = 0;
                            }
                            gpmb += (double)lapquanlydautu.GiaiPhongMatBang;
                            if (lapquanlydautu.XayLap == null)
                            {
                                lapquanlydautu.XayLap = 0;
                            }
                            xd += (double)lapquanlydautu.XayLap;
                            if (lapquanlydautu.ThietBi == null)
                            {
                                lapquanlydautu.ThietBi = 0;
                            }
                            tb += (double)lapquanlydautu.ThietBi;
                            if (lapquanlydautu.QuanLyDuAn == null)
                            {
                                lapquanlydautu.QuanLyDuAn = 0;
                            }
                            qlda += (double)lapquanlydautu.QuanLyDuAn;
                            if (lapquanlydautu.TuVan == null)
                            {
                                lapquanlydautu.TuVan = 0;
                            }
                            tv += (double)lapquanlydautu.TuVan;
                            if (lapquanlydautu.Khac == null)
                            {
                                lapquanlydautu.Khac = 0;
                            }
                            khac += (double)lapquanlydautu.Khac;
                            if (lapquanlydautu.DuPhong == null)
                            {
                                lapquanlydautu.DuPhong = 0;
                            }
                            khac +=(double)lapquanlydautu.DuPhong;
                        }
                        //foreach (var item in duan.QuyetToanDuAns)
                        //{
                        //    if(item.NgayPheDuyetQuyetToan != null)
                        //    {
                        //        if (item.TongGiaTri == null)
                        //        {
                        //            item.TongGiaTri = 0;
                        //        }
                        //        tongso += (double)item.TongGiaTri;
                        //        if (item.GiaiPhongMatBang == null)
                        //        {
                        //            item.GiaiPhongMatBang = 0;
                        //        }
                        //        gpmb += (double)item.GiaiPhongMatBang;
                        //        if (item.XayLap == null)
                        //        {
                        //            item.XayLap = 0;
                        //        }

                        //        xd += (double)item.XayLap;
                        //        if (item.ThietBi == null)
                        //        {
                        //            item.ThietBi = 0;
                        //        }
                        //        tb += (double)item.ThietBi;
                        //        if (item.QuanLyDuAn == null)
                        //        {
                        //            item.QuanLyDuAn = 0;
                        //        }
                        //        qlda += (double)item.QuanLyDuAn;
                        //        if (item.TuVan == null)
                        //        {
                        //            item.TuVan = 0;
                        //        }
                        //        tv += (double)item.TuVan;
                        //        if (item.Khac == null)
                        //        {
                        //            item.Khac = 0;
                        //        }
                        //        khac += (double)item.Khac;
                        //        if (item.DuPhong == null)
                        //        {
                        //            item.DuPhong = 0;
                        //        }
                        //        khac += (double)item.DuPhong;
                        //    }

                        //}
                        var thanhtoanchiphikhac = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach(var item in thanhtoanchiphikhac)
                        {
                            if(item.LoaiThanhToan!=null && item.LoaiThanhToan.Contains("1") && item.NgayThanhToan !=null)
                            {
                                foreach(var item1 in item.NguonVonThanhToanChiPhiKhacs)
                                {
                                    qldatheoab += item1.GiaTri;
                                }
                            }
                            if(item.LoaiThanhToan!=null && item.NgayThanhToan != null && !item.LoaiThanhToan.Contains("1"))
                            {
                                foreach (var item1 in item.NguonVonThanhToanChiPhiKhacs)
                                {
                                    khactheoab += item1.GiaTri;
                                }
                            }
                        }
                        sheet.Cells[5, 3].Value = tongso/dvt;
                        sheet.Cells[5, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        int rowIndex = 6;
                        //"Bồi thường, hỗ trợ, TĐC";
                        var hopdong1 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn,3);
                        double gt1 = 0;
                        int rowgt1 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = gpmb/dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong1)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                            double gt = 0;
                            foreach(var item1 in item.QuyetToanHopDongs)
                            {
                                if(item1.GiaTriQuyetToan != null && item1.NgayQuyetToan !=null)
                                {
                                    gt +=(double)item1.GiaTriQuyetToan;
                                    gt1+= (double)item1.GiaTriQuyetToan; ;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt/dvt;
                            sheet.Cells[rowIndex,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }                       
                        sheet.Cells[rowgt1, 4].Value = gt1 / dvt;
                        sheet.Cells[rowgt1,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Xây dựng
                        var hopdong2 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 1);
                        rowIndex++;
                        double gt2 = 0;
                        int rowgt2 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = xd/dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong2)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt2 += (double)item1.GiaTriQuyetToan; 
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt/dvt;
                            sheet.Cells[rowIndex,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt2, 4].Value = gt2 / dvt;
                        sheet.Cells[rowgt2,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Thiết bị
                        var hopdong3 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 2);
                        rowIndex++;
                        double gt3 = 0;
                        int rowgt3 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = tb/dvt;
                        sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong3)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt3 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt/dvt;
                            sheet.Cells[rowIndex,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt3, 4].Value = gt3 / dvt;
                        sheet.Cells[rowgt3,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Quản lí dự án
                        rowIndex++;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = qlda/dvt;
                        sheet.Cells[rowIndex,3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 4].Value = qldatheoab / dvt;
                        sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        //Tư vấn
                        var hopdong4 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 5);
                        rowIndex++;
                        double gt4 = 0;
                        int rowgt4 = rowIndex;
                        index = 0;
                        sheet.Cells[rowIndex, 3].Value = tv/dvt;
                        sheet.Cells[rowIndex,3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong4)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt4 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt/dvt;
                            sheet.Cells[rowIndex,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt4, 4].Value = gt4 / dvt;
                        sheet.Cells[rowgt4,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        //Khác
                        var hopdong5 = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 4);
                        rowIndex++;
                        double gt5 = 0;
                        index = 0;
                        int rowgt5 = rowIndex;
                        sheet.Cells[rowIndex, 3].Value = khac/dvt;
                        sheet.Cells[rowIndex,3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        foreach (var item in hopdong5)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenHopDong;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            double gt = 0;
                            foreach (var item1 in item.QuyetToanHopDongs)
                            {
                                if (item1.GiaTriQuyetToan != null && item1.NgayQuyetToan != null)
                                {
                                    gt += (double)item1.GiaTriQuyetToan;
                                    gt5 += (double)item1.GiaTriQuyetToan;
                                }
                            }
                            sheet.Cells[rowIndex, 4].Value = gt/dvt;
                            sheet.Cells[rowIndex,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";
                        }
                        sheet.Cells[rowgt5, 4].Value = (gt5+khactheoab) / dvt;
                        sheet.Cells[rowgt5,4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        string dvTinh = sheet.Cells["D3"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["D3"].Value = dvTinh;
                        sheet.Cells[5, 4].Value = (gt1 + gt2 + gt3 + gt4 + gt5 + khactheoab + qldatheoab) / dvt;
                        sheet.Cells[5, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        pck.SaveAs(new FileInfo(fullPath));

                        _baoCaoTableService.AddBaoCao(documentName, "Mau 04/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }
        #endregion

        #region Mau05
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau05")]
        [HttpGet]
        public HttpResponseMessage ExportMau05(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau05(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau05(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-05-MauPhuLuc5.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc5-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // lay ra thong tin du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = duan.TenDuAn;
                        string dvTinh = sheet.Cells["H3"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["H3"].Value = dvTinh;
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        //lay ra hang
                        int rowIndex = 7;
                        sheet.InsertRow(rowIndex, 1);
                        sheet.Cells[rowIndex, 1].Value = 1;
                        sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells[rowIndex, 2].Value = duan.TenDuAn;
                        sheet.Cells[rowIndex, 2].Style.WrapText = true;
                        sheet.Cells[rowIndex, 3].Value = "Công trình";
                        sheet.Cells[rowIndex, 4].Value = 1;
                        sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //gia don vi
                        double giadonvi = 0;
                        var hopdong = _hopDongService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in hopdong)
                        {
                            var quyettoanhopdong = _quyetToanHopDongService.GetByIdHopDong(item.IdHopDong);
                            foreach (var item1 in quyettoanhopdong)
                            {
                                if (item1.NgayQuyetToan != null)
                                {
                                    if (item1.GiaTriQuyetToan == null)
                                    {
                                        item1.GiaTriQuyetToan = 0;
                                    }
                                    giadonvi += (double)item1.GiaTriQuyetToan;
                                }
                            }
                        }
                        var nguonvonthanhtoanchiphikhac = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in nguonvonthanhtoanchiphikhac)
                        {
                            giadonvi += item.GiaTri;
                        }
                        sheet.Cells[rowIndex, 5].Value = giadonvi / dvt;
                        sheet.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 5].Style.Numberformat.Format = "#,##0";
                        //tong nguyen gia
                        double tongnguyengia = 0;
                        var quyetoanduan = _quyetToanDuAnService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in quyetoanduan)
                        {
                            if (item.NgayPheDuyetQuyetToan != null)
                            {
                                if (item.TongGiaTri == null)
                                {
                                    item.TongGiaTri = 0;
                                }
                                tongnguyengia += (double)item.TongGiaTri;
                            }
                        }
                        sheet.Cells[rowIndex, 6].Value = tongnguyengia / dvt;
                        sheet.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";                        
                        //lay ngay ket thuc theo quyet toan du an
                        var ngayketthucthucte = _quyetToanDuAnService.GetNewestByIdDuAn(iIDDuAn).NgayKyBienBan?.ToString("dd/MM/yyyy");
                        sheet.Cells[rowIndex, 7].Value = ngayketthucthucte;
                        sheet.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //lay ra nguon von theo quyet toan du an hoan thanh
                        List<NguonVon> nguonvons = new List<NguonVon>();
                        foreach (var item in quyetoanduan)
                        {
                            if (item.NgayPheDuyetQuyetToan != null)
                            {
                                var nguonvon = _nguonVonService.GetByIdQuyetToanDuAn(item.IdQuyetToanDuAn);
                                foreach (var item1 in nguonvon)
                                {
                                    nguonvons.Add(item1);
                                }
                            }
                        }
                        //loc nguon von bi trung
                        for (var i = 0; i < nguonvons.Count; i++)
                        {
                            for (var j = i + 1; j < nguonvons.Count; j++)
                            {
                                if (nguonvons[i] == nguonvons[j])
                                {
                                    nguonvons[j] = null;
                                }
                            }
                        }
                        string tennguonvons = "";
                        foreach (var item in nguonvons)
                        {
                            if (item != null)
                            {
                                tennguonvons += item.TenNguonVon + ", ";
                            }

                        }
                        if (tennguonvons.Length > 0)
                        {
                            tennguonvons = tennguonvons.Substring(0, tennguonvons.Length - 2);
                        }
                        sheet.Cells[rowIndex, 8].Value = tennguonvons;
                        sheet.Cells[rowIndex, 8].Style.WrapText = true;
                        sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[rowIndex, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 05/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau06
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau06")]
        [HttpGet]
        public HttpResponseMessage ExportMau06(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau06(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau06(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-06-MauPhuLuc6.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc6-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            //string documentName = string.Format("BC_KetQuaLuaChonNhaThau_UBNDTinhPD_-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        var duans = _duAnService.GetAll();

                        int rowIndex = 1;
                        foreach (var da in duans)
                        {
                            if(da.IdDuAn == iIDDuAn)
                            {
                                rowIndex++;
                                sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                            }
                        }
                        string dvTinh = sheet.Cells["H3"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["H3"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 06/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }



        #endregion

        #region Mau07
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau07")]
        [HttpGet]
        public HttpResponseMessage ExportMau07(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau07(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau07(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-07-PL7.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc7-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // lay du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = duan.TenDuAn;
                        sheet.Cells["A2"].Style.WrapText = true;
                        //danh muc nha thau
                        //lay ra tat ca cac dnah muc theo du an
                        var danhmucnhathaus = _danhMucNhaThauService.GetAll(iIDDuAn);
                        List<HopDong> hopdongs = new List<HopDong>();
                        foreach(var item in danhmucnhathaus)
                        {
                            var hopdong = _hopDongService.GetByIdNhaThau(item.IdNhaThau);
                            foreach(var item1 in hopdong)
                            {
                                hopdongs.Add(item1);
                            }
                        }
                        List<TamUngHopDong> tamunghopdongs = new List<TamUngHopDong>();
                        List<ThanhToanHopDong> thanhtoanhopdongs = new List<ThanhToanHopDong>();
                        foreach(var item in hopdongs)
                        {
                            var tamunghopdong = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            foreach(var item1 in tamunghopdong)
                            {
                                tamunghopdongs.Add(item1);
                            }
                            var thanhtoanhopdong = _thanhToanHopDongService.GetByIdHopDong(item.IdHopDong);
                            foreach(var item1 in thanhtoanhopdong)
                            {
                                thanhtoanhopdongs.Add(item1);
                            }
                        }
                        int rowIndex = 9;
                        int index = 0;
                        foreach(var item in danhmucnhathaus)
                        {
                            index++;
                            int demtu = 0;
                            foreach (var item1 in tamunghopdongs)
                            {
                                var hopdong2 = _hopDongService.GetByIdNhaThau(item.IdNhaThau);

                                if (hopdong2.ToString().Contains("IdHopDong: " + item1.IdHopDong))
                                {
                                    rowIndex++;
                                    demtu++;
                                    sheet.InsertRow(rowIndex, 1);
                                    sheet.Cells[rowIndex, 3].Value = "Tạm ứng " + item1.NgayTamUng?.ToString("dd/MM/yyy") + ". Hợp đồng: " + item1.IdHopDong;
                                }
                                
                            }

                            foreach (var item1 in thanhtoanhopdongs)
                            {
                                var hopdong2 = _hopDongService.GetByIdNhaThau(item.IdNhaThau);

                                if (hopdong2.ToString().Contains("IdHopDong: " + item1.IdHopDong))
                                {
                                    rowIndex++;
                                    demtu++;
                                    sheet.InsertRow(rowIndex, 1);
                                    sheet.Cells[rowIndex, 3].Value = "Thanh toán " + item1.ThoiDiemThanhToan?.ToString("dd/MM/yyy") + ". Hợp đồng: " + item1.IdHopDong;
                                }
                            }
                            if (demtu > 0)
                            {
                                sheet.Cells[rowIndex - demtu+1, 1, rowIndex, 1].Merge = true;
                                sheet.Cells[rowIndex - demtu + 1, 1].Value = index;
                                sheet.Cells[rowIndex - demtu + 1, 2, rowIndex, 2].Merge = true;
                                sheet.Cells[rowIndex - demtu + 1, 2].Value = item.TenNhaThau;
                            }


                        }

                        string dvTinh = sheet.Cells["G5"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["G5"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 07/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau08
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau08")]
        [HttpGet]
        public HttpResponseMessage ExportMau08(HttpRequestMessage request, int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau08(iIDDuAn, szNguonVon, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau08(int iIDDuAn, int szNguonVon, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-08-MauPhuLuc8.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc8-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        // ten du an
                        var duan = _duAnService.GetByIdForBaoCao(iIDDuAn);
                        sheet.Cells["A2"].Value = "Của dự án: " + duan.TenDuAn;
                        //Don vi tinh
                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }
                        // van ban phap ly
                        var vanbanphaply = _vanBanDuAnService.GetByIdDuAn(iIDDuAn);
                        //khai bao row exel bat dau tu hang so 7
                        int rowIndex = 7;
                        int demsovanban = 0;
                        foreach (var item in vanbanphaply)
                        {
                            rowIndex++;
                            demsovanban++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = demsovanban;
                            sheet.Cells[rowIndex, 2].Value = item.TenVanBan;
                            sheet.Cells[rowIndex, 3].Value = item.SoVanBan + ", " + item.NgayBanHanh?.ToString("dd/MM/yyyy");
                            sheet.Cells[rowIndex, 4].Value = item.CoQuanBanHanh;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        //thuc hien dau tu
                        //cong them 5 hang
                        rowIndex = rowIndex + 5;
                        //gan gia tri nguon von
                        sheet.Cells[rowIndex, 4].Value = "Đơn vị: " + szDonViTinh;
                        //nguon von
                        //cong them 4 hang
                        //duoc duyet
                        rowIndex = rowIndex + 4;
                        var nguonvon = _nguonVonService.GetById(szNguonVon).TenNguonVon;
                        sheet.Cells[rowIndex, 1].Value = nguonvon;
                        var nguonvonduan = _nguonVonDuAnService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        double tongsonguonvonduanduocduyet = 0;
                        foreach (var item in nguonvonduan)
                        {
                            if (item.GiaTri == null)
                            {
                                item.GiaTri = 0;
                            }
                            tongsonguonvonduanduocduyet += (double)item.GiaTri;
                        }
                        sheet.Cells[rowIndex, 3].Value = tongsonguonvonduanduocduyet / dvt;
                        //thuc hien
                        //thanh toans KLTH
                        var thanhtoanhopdong = _thanhToanHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        double thanhtoanKLTH = 0;
                        foreach (var item in thanhtoanhopdong)
                        {
                            var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item.IdThanhToanHopDong);
                            foreach (var item1 in nguonvongoithauthanhtoan)
                            {
                                thanhtoanKLTH += item1.GiaTri;
                            }
                        }
                        // thanh toan chi phi khac
                        var nguonvonthanhtoanchiphikhac = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        var thanhtoanchiphikhac = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in nguonvonthanhtoanchiphikhac)
                        {
                            thanhtoanKLTH += item.GiaTri;
                        }
                        //tạm ứng
                        //var tamunghopdong = _tamUngHopDongService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                        //foreach (var item in tamunghopdong)
                        //{
                        //    var tamung1 = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item.IdTamUng);
                        //    foreach (var item1 in tamung1)
                        //    {
                        //        thanhtoanKLTH += item1.GiaTri;
                        //    }
                        //}
                        sheet.Cells[rowIndex, 4].Value = thanhtoanKLTH / dvt;
                        //chi phi dau tu
                        //cong them 3 dong
                        rowIndex = rowIndex + 3;
                        sheet.Cells[rowIndex, 4].Value = "Đơn vị: " + szDonViTinh;
                        //khai bao cac bien
                        double tongso = 0;
                        double gpmb = 0;
                        double xd = 0;
                        double tb = 0;
                        double qlda = 0;
                        double tv = 0;
                        double khac = 0;
                        var lapquanlydautu = _lapQuanLyDauTuService.GetNewestTheoDuAn(iIDDuAn);
                        if (lapquanlydautu != null)
                        {
                            if (lapquanlydautu.TongGiaTri == null)
                            {
                                lapquanlydautu.TongGiaTri = 0;
                            }
                            tongso += (double)lapquanlydautu.TongGiaTri;
                            if (lapquanlydautu.GiaiPhongMatBang == null)
                            {
                                lapquanlydautu.GiaiPhongMatBang = 0;
                            }
                            gpmb += (double)lapquanlydautu.GiaiPhongMatBang;
                            if (lapquanlydautu.XayLap == null)
                            {
                                lapquanlydautu.XayLap = 0;
                            }
                            xd += (double)lapquanlydautu.XayLap;
                            if (lapquanlydautu.ThietBi == null)
                            {
                                lapquanlydautu.ThietBi = 0;
                            }
                            tb += (double)lapquanlydautu.ThietBi;
                            if (lapquanlydautu.QuanLyDuAn == null)
                            {
                                lapquanlydautu.QuanLyDuAn = 0;
                            }
                            qlda += (double)lapquanlydautu.QuanLyDuAn;
                            if (lapquanlydautu.TuVan == null)
                            {
                                lapquanlydautu.TuVan = 0;
                            }
                            tv += (double)lapquanlydautu.TuVan;
                            if (lapquanlydautu.Khac == null)
                            {
                                lapquanlydautu.Khac = 0;
                            }
                            khac += (double)lapquanlydautu.Khac;
                            if (lapquanlydautu.DuPhong == null)
                            {
                                lapquanlydautu.DuPhong = 0;
                            }
                            khac += (double)lapquanlydautu.DuPhong;
                        }
                        rowIndex = rowIndex + 3;
                        sheet.Cells[rowIndex, 3].Value = tongso / dvt;
                        sheet.Cells[rowIndex + 1, 3].Value = gpmb / dvt;
                        sheet.Cells[rowIndex + 2, 3].Value = xd / dvt;
                        sheet.Cells[rowIndex + 3, 3].Value = tb / dvt;
                        sheet.Cells[rowIndex + 4, 3].Value = qlda / dvt;
                        sheet.Cells[rowIndex + 5, 3].Value = tv / dvt;
                        sheet.Cells[rowIndex + 6, 3].Value = khac / dvt;
                        //chi phi dau tu de nghi quyet toan
                        double gpmbqt = 0;
                        double xdqt = 0;
                        double tbqt = 0;
                        double qldaqt = 0;
                        double tvqt = 0;
                        double khacqt = 0;
                        //boi thuong ho tro
                        var hopdonggpmb = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 3);
                        foreach (var item in hopdonggpmb)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach(var item2 in nguonvongoithautamung)
                            //    {
                            //        gpmbqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    gpmbqt += item2.GiaTri;
                                }
                            }
                        }
                        //xay lap
                        var hopdongxd = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 1);
                        foreach (var item in hopdongxd)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        xdqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    xdqt += item2.GiaTri;
                                }
                            }
                        }
                        //thiet bi
                        var hopdongtb = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 2);
                        foreach (var item in hopdongtb)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        tbqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    tbqt += item2.GiaTri;
                                }
                            }
                        }
                        //tu van
                        var hopdongtv = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 5);
                        foreach (var item in hopdongtv)
                        {
                            //var tamung = _tamUngHopDongService.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        tvqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    tvqt += item2.GiaTri;
                                }
                            }
                        }
                        //khac
                        var hopdongkhac = _hopDongService.GetByIdDuAnLoaiGoiThau(iIDDuAn, 4);
                        foreach (var item in hopdongkhac)
                        {
                            //var thanhtoans = thanh.GetByIdHopDong(item.IdHopDong);
                            //foreach (var item1 in tamung)
                            //{
                            //    var nguonvongoithautamung = _nguonVonGoiThauTamUngService.GetByTamUngHopDong(item1.IdTamUng);
                            //    foreach (var item2 in nguonvongoithautamung)
                            //    {
                            //        khacqt += item2.GiaTri;
                            //    }
                            //}
                            var thanhtoan = _thanhToanHopDongService.GetByIdDuAnIdNguonVonIdHopDong(iIDDuAn, szNguonVon, item.IdHopDong);
                            foreach (var item1 in thanhtoan)
                            {
                                var nguonvongoithauthanhtoan = _nguonVonGoiThauThanhToanService.GetByThanhToanHopDong(item1.IdThanhToanHopDong);
                                foreach (var item2 in nguonvongoithauthanhtoan)
                                {
                                    khacqt += item2.GiaTri;
                                }
                            }
                        }
                        var thanhtoanchiphikhacqt = _thanhToanChiPhiKhacService.GetByIdDuAn(iIDDuAn);
                        foreach (var item in thanhtoanchiphikhac)
                        {
                            var nguonvonttcpk = _nguonVonThanhToanChiPhiKhacService.GetByIdDuAnIdNguonVon(iIDDuAn, szNguonVon);
                            if (item.LoaiThanhToan == null)
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khacqt += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.LoaiThanhToan.Contains("1") && item.NgayThanhToan != null)
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        qldaqt += item1.GiaTri;
                                    }
                                }
                            }
                            if (item.LoaiThanhToan != null && item.NgayThanhToan != null && !item.LoaiThanhToan.Contains("1"))
                            {
                                foreach (var item1 in nguonvonttcpk)
                                {
                                    if (item.IdThanhToanChiPhiKhac == item1.IdThanhToanChiPhiKhac)
                                    {
                                        khacqt += item1.GiaTri;
                                    }
                                }
                            }
                        }
                        sheet.Cells[rowIndex, 4].Value = (gpmbqt + xdqt + tbqt + qldaqt + tvqt + khacqt) / dvt;
                        sheet.Cells[rowIndex + 1, 4].Value = gpmbqt / dvt;
                        sheet.Cells[rowIndex + 2, 4].Value = xdqt / dvt;
                        sheet.Cells[rowIndex + 3, 4].Value = tbqt / dvt;
                        sheet.Cells[rowIndex + 4, 4].Value = qldaqt / dvt;
                        sheet.Cells[rowIndex + 5, 4].Value = tvqt / dvt;
                        sheet.Cells[rowIndex + 6, 4].Value = khacqt / dvt;
                        sheet.Cells[rowIndex, 5].Value = (gpmbqt + xdqt + tbqt + qldaqt + tvqt + khacqt - gpmb - xd - tb - qlda - tv - khac) / dvt;
                        sheet.Cells[rowIndex + 1, 5].Value = (gpmbqt - gpmb) / dvt;
                        sheet.Cells[rowIndex + 2, 5].Value = (xdqt - xd) / dvt;
                        sheet.Cells[rowIndex + 3, 5].Value = (tbqt - tb) / dvt;
                        sheet.Cells[rowIndex + 4, 5].Value = (qldaqt - qlda) / dvt;
                        sheet.Cells[rowIndex + 5, 5].Value = (tvqt - tv) / dvt;
                        sheet.Cells[rowIndex + 6, 5].Value = (khacqt - khac) / dvt;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 08/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau09
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau09")]
        [HttpGet]
        public HttpResponseMessage ExportMau09(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau09(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau09(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-09-MauPhuLuc9.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc9-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}
                        string dvTinh = sheet.Cells["G14"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["G14"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau10
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau10")]
        [HttpGet]
        public HttpResponseMessage ExportMau10(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
           
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau10(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau10(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            string[] ndChiPhis = new string[] { "Bồi thường, hỗ trợ, TĐC", "Xây dựng", "Thiết bị", "Quản lý dự án", "Tư vấn", "Chi phí khác" };
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-10-MauPhuLuc10.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc10-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            DuAn duan = _duAnService.GetById(iIDDuAn);
            DuAn duanCha = new DuAn();
            if(duan.IdDuAnCha!=null)
            {
                duanCha = _duAnService.GetById(duan.IdDuAnCha??0);
            }
            ChuDauTu chuDauTu = new ChuDauTu();
            if (duan.IdChuDauTu != null)
            {
                int idChuDauTu = duan.IdChuDauTu ?? 0;
                chuDauTu = _chuDauTuService.GetById(idChuDauTu);
            }
            int[] indexDVTs = { 23, 37, 46 };
            
            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        int dvt = 1;
                        if (szDonViTinh == "Đồng")
                        {
                            dvt = 1;
                        }
                        if (szDonViTinh == "Nghìn Đồng")
                        {
                            dvt = 1000;
                        }
                        if (szDonViTinh == "Triệu Đồng")
                        {
                            dvt = 1000000;
                        }
                        if (szDonViTinh == "Tỷ Đồng")
                        {
                            dvt = 1000000000;
                        }

                        string dvTinh1 = sheet.Cells["D15"].Value.ToString();
                        dvTinh1 = dvTinh1.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["D15"].Value = dvTinh1;

                        string A4value = sheet.Cells["A4"].Value.ToString();
                        var tenQD= _qtda10Service.GetTenQD(iIDDuAn);
                        A4value = A4value.Replace("{{CQPDTHQT}}", tenQD);
                        sheet.Cells["A4"].Value = A4value;
                        string tenDuAn = sheet.Cells["A9"].Value.ToString();
                        string tencongTrinh = sheet.Cells["A10"].Value.ToString();
                        if (duan.IdDuAnCha != null)
                        {
                            tenDuAn = tenDuAn.Replace("{{tenduan}}", duanCha.TenDuAn + "");
                            tencongTrinh = tencongTrinh.Replace("{{tentieuduan}}", duan.TenDuAn + "");
                        }
                        else
                        {
                            tenDuAn = tenDuAn.Replace("{{tenduan}}", duan.TenDuAn + "");
                            tencongTrinh = tencongTrinh.Replace("{{tieuduan}}", "");
                        }
                        string tenchuDauTu = sheet.Cells["A11"].Value.ToString();
                        if (duan.IdChuDauTu != null)
                        {
                            tenchuDauTu = tenchuDauTu.Replace("{{chudautu}}", chuDauTu.TenChuDauTu + "");
                        }
                        else
                            tenchuDauTu = tenchuDauTu.Replace("{{chudautu}}", "");
                        sheet.Cells["A9"].Value = tenDuAn;
                        sheet.Cells["A10"].Value = tencongTrinh;
                        sheet.Cells["A11"].Value = tenchuDauTu;
                        string diaDiem = sheet.Cells["A12"].Value.ToString();
                        diaDiem = diaDiem.Replace("{{diadiem}}", duan.DiaDiem);
                        sheet.Cells["A12"].Value = diaDiem;
                       sheet.Cells["A13"].Value = "Thời gian khởi công - hoàn thành:" + duan.ThoiGianThucHien;
                        pck.SaveAs(new FileInfo(fullPath));
                        var nguonVonDauTus = _qtda10Service.GetNguonVonDauTu(iIDDuAn);
                        sheet.InsertRow(21, nguonVonDauTus.Count);
                        int indexRowTable4 = 50 + nguonVonDauTus.Count;
                        sheet.InsertRow(indexRowTable4, nguonVonDauTus.Count);
                        int index = 21;
                        double tongGTDuocDuyet = 0;
                        double tongGTDaThanhToan = 0;
                        
                        foreach(var nguonVonDauTu in nguonVonDauTus)
                        {
                            string positionFirst = "A" + index;
                            string positionLast = "E" + index;
                            sheet.Cells[positionFirst + ":" + positionLast].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[positionFirst + ":" + positionLast].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[positionFirst + ":" + "B" + index].Merge=true;
                            sheet.Cells["C" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["D" + index].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["C" + index].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["D" + index].Style.Numberformat.Format = "#,##0";
                         
                            string positionFirstTable4= "A" + indexRowTable4;
                            string positionLastTable4 = "E" + indexRowTable4;
                            sheet.Cells[positionFirstTable4 + ":" + "B" + indexRowTable4].Merge = true;
                            sheet.Cells[positionFirstTable4 + ":" + positionLastTable4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[positionFirstTable4 + ":" + positionLastTable4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["C" + indexRowTable4].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["D" + indexRowTable4].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["C" + indexRowTable4+":" + "D" + indexRowTable4].Merge = true;
                            sheet.Cells["C" + indexRowTable4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["D" + indexRowTable4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            string tenNguonVon= (string)GetValueObject(nguonVonDauTu, "TenNguonVon");
                            sheet.Cells["A" + index].Value = tenNguonVon;

                            sheet.Cells["A" + indexRowTable4].Value = tenNguonVon;
                            double giaTriDuocDuyet = (double)GetValueObject(nguonVonDauTu, "GiaTriDuocDuyet");
                            tongGTDuocDuyet += giaTriDuocDuyet;
                            sheet.Cells["C" + index].Value =giaTriDuocDuyet/dvt ;
                            double giaTriDaThanhToan = (double)GetValueObject(nguonVonDauTu, "GiaTriDaThanhToan");
                            sheet.Cells["D" + index].Value = giaTriDaThanhToan/dvt;
                            sheet.Cells["C" + indexRowTable4].Value = giaTriDaThanhToan/dvt;
                            tongGTDaThanhToan += giaTriDaThanhToan;
                            index++;
                            indexRowTable4++;

                        }
                        sheet.Cells["C20"].Value = tongGTDuocDuyet/dvt;
                        sheet.Cells["D20"].Value = tongGTDaThanhToan/dvt;
                        sheet.Cells["E20"].Value = (tongGTDuocDuyet - tongGTDaThanhToan)/dvt;
                        sheet.Cells["C20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["D20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["E20"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        string indexTong = (49 + nguonVonDauTus.Count).ToString();
                        sheet.Cells["C"+indexTong].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells["C" +indexTong].Value = tongGTDaThanhToan/dvt;
                        index = index + 5;
                        var chiPhiDauTuFromTTHD = _qtda01Service.GetChiPhiDauTuFromQTHD(iIDDuAn);
                        var chiPhiDauTuFromTTCPK = _qtda01Service.GetChiPhiDauTuFromTTCPK(iIDDuAn);
                        var quyetToanDuAn = _qtda01Service.GetQuyetToanByDuAn(iIDDuAn);
                        
                        int indexBT = 0, indexXD = 0, indexTB = 0, indexQLDA = 0, indexTV = 0, indexCPK = 0;
                        for (int i = 0; i < ndChiPhis.Length; i++)
                        {
                            switch (i)
                            {
                                case 0:
                                    double? giaTriGiaiPhongMatBang = (double?)GetValueObject(quyetToanDuAn, "GiaiPhongMatBang");
                                    sheet.Cells["B" + index].Value = giaTriGiaiPhongMatBang / dvt;
                                   
                                    indexBT = index;
                                    break;
                                case 1:
                                    double? giaTriXayLap = (double?)GetValueObject(quyetToanDuAn, "XayLap");
                                    sheet.Cells["B" + index].Value = giaTriXayLap / dvt;
                                    
                                    indexXD = index;
                                    break;
                                case 2:
                                    double? giaTriThietBi = (double?)GetValueObject(quyetToanDuAn, "ThietBi");
                                    sheet.Cells["B" + index].Value = giaTriThietBi / dvt;
                                   
                                    indexTB = index;
                                    break;
                                case 3:
                                    double? giaTriQLDA = (double?)GetValueObject(quyetToanDuAn, "QuanLyDuAn");
                                    sheet.Cells["B" + index].Value = giaTriQLDA / dvt;
                                   
                                    indexQLDA = index;
                                    break;
                                case 4:
                                    double? giaTriTuVan = (double?)GetValueObject(quyetToanDuAn, "TuVan");
                                    sheet.Cells["B" + index].Value = giaTriTuVan / dvt;
                                   
                                    indexTV = index;
                                    break;
                                case 5:
                                    double? giaTriKhac = (double?)GetValueObject(quyetToanDuAn, "Khac");
                                    sheet.Cells["B" + index].Value = giaTriKhac / dvt;
                                   
                                    indexCPK = index;
                                    break;
                            }


                            sheet.Cells["A" + index].Value = i+1+ "."+ ndChiPhis[i];
                            index++;
                        }

                        double tongQT = 0;
                        foreach (var item in chiPhiDauTuFromTTHD)
                        {
                            int? loaiGoiThau = (int?)GetValueObject(item, "LoaiGoiThau");
                            switch (loaiGoiThau)
                            {
                                case 1:
                                    {
                                        double giaTriXD = (double)GetValueObject(item, "TongGiaTri");
                                        tongQT += giaTriXD;
                                        sheet.Cells["D" + indexXD].Value = giaTriXD / dvt;
                                        break;
                                    }
                                case 2:
                                    {
                                        double giaTriTB = (double)GetValueObject(item, "TongGiaTri");
                                        tongQT += giaTriTB;
                                        sheet.Cells["D" + indexTB].Value = giaTriTB / dvt;
                                        break;
                                    }
                                case 3:
                                    {
                                        double giaTriBT = (double)GetValueObject(item, "TongGiaTri");
                                        tongQT += giaTriBT;
                                        sheet.Cells["D" + indexBT].Value = giaTriBT / dvt;
                                        break;
                                    }
                                case 5:
                                    {
                                        double giaTriTV = (double)GetValueObject(item, "TongGiaTri");
                                        tongQT += giaTriTV;
                                        sheet.Cells["D" + indexTV].Value = giaTriTV / dvt;
                                        break;
                                    }

                            }

                        }

                        double giaTriCPK = 0;
                        foreach (var item in chiPhiDauTuFromTTCPK)
                        {
                            string loaiThanhToan = (string)GetValueObject(item, "LoaiThanhToan");
                            if (loaiThanhToan == "1")
                            {
                                double giaTriQLDA = (double)GetValueObject(item, "TongGiaTri");
                                tongQT += giaTriQLDA;
                                sheet.Cells["D" + indexQLDA].Value = giaTriQLDA / dvt;
                               
                            }
                            else
                            {
                                double giaTri = (double)GetValueObject(item, "TongGiaTri");
                                giaTriCPK += giaTri;
                                

                            }
                        }
                        sheet.Cells["D" + indexCPK].Value = giaTriCPK/dvt;
                        tongQT += giaTriCPK;
                        index = index + 9;
                        sheet.Cells["B" + index].Value = tongQT/dvt;
                        sheet.Cells["D" + index].Value = tongQT/dvt;
                        index++;
                        sheet.Cells["B" + index].Value = tongQT / dvt;
                        sheet.Cells["D" + index].Value = tongQT / dvt;
                        foreach (int indexDVT in indexDVTs)
                        {
                            int tempt = indexDVT + nguonVonDauTus.Count;
                            string valueDVT = sheet.Cells["D" + tempt].Value.ToString();
                            valueDVT = valueDVT.Replace("{{donvitinh}}", szDonViTinh);
                            sheet.Cells["D" + tempt].Value = valueDVT;

                        }
                        int lastIndexDVT = 56 + nguonVonDauTus.Count * 2;
                        string value = sheet.Cells["D" + lastIndexDVT].Value.ToString();
                        value = value.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["D" + lastIndexDVT].Value = value;
                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 10/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau11
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau11")]
        [HttpGet]
        public HttpResponseMessage ExportMau11(HttpRequestMessage request, string szDonViBaoCao, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau11(szDonViBaoCao, szLoaiBaoCao, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau11(string szDonViBaoCao, string szLoaiBaoCao, int iNamPD,  string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-11-MauPhuLuc11.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc11-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}
                        if (szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "6 tháng đầu năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }
                        if (szLoaiBaoCao == "Cả năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "Cả năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }

                        string dvTinh = sheet.Cells["J8"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["J8"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Mau12
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau12")]
        [HttpGet]
        public HttpResponseMessage ExportMau12(HttpRequestMessage request, string szDonViBaoCao, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau12(szDonViBaoCao, szLoaiBaoCao, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau12(string szDonViBaoCao, string szLoaiBaoCao, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-12-MauPhuLuc12.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc12-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}
                        if (szLoaiBaoCao == "6 tháng đầu năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "6 tháng đầu năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }
                        if (szLoaiBaoCao == "Cả năm")
                        {
                            string tgian = sheet.Cells["A5"].Value.ToString();
                            tgian = tgian.Replace("{{thoigian}}", "Cả năm " + namPD);
                            sheet.Cells["A5"].Value = tgian;
                        }

                        string dvTinh = sheet.Cells["J8"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["J8"].Value = dvTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}



