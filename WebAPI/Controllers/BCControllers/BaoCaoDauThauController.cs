﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;
using Service.BCService;

using System.Linq;
using System.Collections.Generic;
using OfficeOpenXml.Style;
using System.Drawing;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoDauthau")]
    public class BaoCaoDauThauController : ApiControllerBase
    {
        private IBaoCaoTableService _baoCaoTableService;
        private ILinhVucDauThauService _linhVucDauThauService;
        private IBaoCaoDauThauService _baoCaoDauThauService;
        private IHinhThucLuaChonNhaThauService _hinhThucLuaChonNhaThauService;
        public BaoCaoDauThauController( IBaoCaoDauThauService baoCaoDauThauService, ILinhVucDauThauService linhVucDauThauService, IHinhThucLuaChonNhaThauService hinhThucLuaChonNhaThauService, IBaoCaoTableService baoCaoTableService, IErrorService errorService) : base(errorService)
        {
            this._linhVucDauThauService = linhVucDauThauService;
            this._baoCaoDauThauService = baoCaoDauThauService;
            this._hinhThucLuaChonNhaThauService = hinhThucLuaChonNhaThauService;
            this._baoCaoTableService = baoCaoTableService;
        }

        #region Tinh Phe Duyet
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Tinh")]
        [HttpGet]
        public HttpResponseMessage ExportTinh(HttpRequestMessage request, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName=TinhPheDuyet(iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string TinhPheDuyet(int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            int dvt = 1;
            if (szDonViTinh == "Đồng")
            {
                dvt = 1;
            }
            if (szDonViTinh == "Nghìn Đồng")
            {
                dvt = 1000;
            }
            if (szDonViTinh == "Triệu Đồng")
            {
                dvt = 1000000;
            }
            if (szDonViTinh == "Tỷ Đồng")
            {
                dvt = 1000000000;
            }
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/MauBCKQLCNT_UBNDTinh.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + "-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            //try
            //{
            //Read Template
            using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
            {
                //Create Excel EPPlus Package based on template stream
                using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                {
                    //Grab the sheet with the template, sheet name is "BOL".
                    ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                    string title = sheet.Cells["A2"].Value.ToString();
                    title = title.Replace("{{nam}}", " NĂM " + namPD);
                    sheet.Cells["A2"].Value = title;

                    string title1 = sheet.Cells["A3"].Value.ToString();
                    title1 = title1.Replace("{{nam}}", namPD);
                    sheet.Cells["A3"].Value = title1;

                    string dvTinh = sheet.Cells["N4"].Value.ToString();
                    dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                    sheet.Cells["N4"].Value = dvTinh;
                    var lstDataLinhVucDauThau = this._baoCaoDauThauService.GetKetQuaDauThauTheoLinhVuc_CoQuanPheDuyet(0,iNamPD);
                    var lstDataHinhThucDauThau = this._baoCaoDauThauService.GetKetQuaDauThauTheoHinhThuc_CoQuanPheDuyet(0, iNamPD);
                    var lstLinhVucDauThau = _linhVucDauThauService.GetAll().ToList();
                    int[] idLinhVucs = new int[lstLinhVucDauThau.Count];
                    int indexLinhVuc = 9;
                    for (var i = 0; i < lstLinhVucDauThau.Count; i++)
                    {
                        idLinhVucs[i] = lstLinhVucDauThau[i].IdLinhVucDauThau;
                        sheet.InsertRow(indexLinhVuc, 1);

                        var firstNameCell = "A" + indexLinhVuc;
                        var lastNameCell = "R" + indexLinhVuc;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["C" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["C" + indexLinhVuc].Style.Numberformat.Format = "#";
                        sheet.Cells["G" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["G" + indexLinhVuc].Style.Numberformat.Format = "#";
                        sheet.Cells["K" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["K" + indexLinhVuc].Style.Numberformat.Format = "#";
                        sheet.Cells["O" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["O" + indexLinhVuc].Style.Numberformat.Format = "#";
                        sheet.Cells["B" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        sheet.Cells["B" + indexLinhVuc].Style.Numberformat.Format = "#";
                        sheet.Cells["B" + indexLinhVuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                        sheet.Cells["B" + indexLinhVuc].Value = lstLinhVucDauThau[i].TenLinhVucDauThau;
                        indexLinhVuc++;
                    }
                    var lstHTLCNT = _hinhThucLuaChonNhaThauService.GetAll().ToList();
                    int[] idHinhThucs = new int[lstHTLCNT.Count];
                    int indexHinhThuc = 11 + lstLinhVucDauThau.Count;
                    for (var i = 0; i < lstHTLCNT.Count; i++)
                    {
                        idHinhThucs[i] = lstHTLCNT[i].IdHinhThucLuaChon;
                        sheet.InsertRow(indexHinhThuc, 1);
                        var firstNameCell = "A" + indexHinhThuc;
                        var lastNameCell = "R" + indexHinhThuc;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                        sheet.Cells["C" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["C" + indexHinhThuc].Style.Numberformat.Format = "#";
                        sheet.Cells["G" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["G" + indexHinhThuc].Style.Numberformat.Format = "#";
                        sheet.Cells["K" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["K" + indexHinhThuc].Style.Numberformat.Format = "#";
                        sheet.Cells["O" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["O" + indexHinhThuc].Style.Numberformat.Format = "#";
                        sheet.Cells["B" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        sheet.Cells["B" + indexHinhThuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                        sheet.Cells["B" + indexHinhThuc].Style.Numberformat.Format = "#";
                        sheet.Cells["B" + indexHinhThuc].Value = lstHTLCNT[i].TenHinhThucLuaChon;
                        indexHinhThuc++;
                    }

                    int indexTongLV = 9 + lstLinhVucDauThau.Count;
                    foreach (var item in lstDataLinhVucDauThau)
                    {
                        int idLinhVuc = (int)GetValueObject(item, "IdLinhVucDauThau");
                        int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                        double giaDuThau = (double)GetValueObject(item, "GiaDuThau") / dvt;
                        double giaGoiThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                        int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                        int index = Array.IndexOf(idLinhVucs, idLinhVuc) + 9;

                        if (idNhomDuAn == 2)
                        {

                            string lastNameCell = "C" + index;
                            string firstNameCell = "F" + index;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["C" + index].Value = soGoiThau;
                            sheet.Cells["D" + index].Value = giaDuThau;
                            sheet.Cells["E" + index].Value = giaGoiThau;
                            sheet.Cells["F" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;

                            if (sheet.Cells["C" + indexTongLV].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["C" + indexTongLV].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["C" + indexTongLV].Value = tongSoGoiThau;

                            if (sheet.Cells["D" + indexTongLV].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["D" + indexTongLV].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["D" + indexTongLV].Value = tongGiaDuThau;
                            if (sheet.Cells["E" + indexTongLV].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["E" + indexTongLV].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["E" + indexTongLV].Value = tongGiaGoiThau;
                            if (sheet.Cells["F" + indexTongLV].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["F" + indexTongLV].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["F" + indexTongLV].Value = tongChenhLech;

                        }
                        if (idNhomDuAn == 3)
                        {
                            string lastNameCell = "J" + index;
                            string firstNameCell = "G" + index;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["G" + index].Value = soGoiThau;
                            sheet.Cells["H" + index].Value = giaDuThau;
                            sheet.Cells["I" + index].Value = giaGoiThau;
                            sheet.Cells["J" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                            if (sheet.Cells["G" + indexTongLV].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["G" + indexTongLV].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["G" + indexTongLV].Value = tongSoGoiThau;

                            if (sheet.Cells["H" + indexTongLV].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["H" + indexTongLV].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["H" + indexTongLV].Value = tongGiaDuThau;
                            if (sheet.Cells["I" + indexTongLV].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["I" + indexTongLV].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["I" + indexTongLV].Value = tongGiaGoiThau;
                            if (sheet.Cells["J" + indexTongLV].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["J" + indexTongLV].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["J" + indexTongLV].Value = tongChenhLech;
                        }
                        if (idNhomDuAn == 4)
                        {
                            string lastNameCell = "N" + index;
                            string firstNameCell = "K" + index;

                            sheet.Cells["K" + index].Value = soGoiThau;
                            sheet.Cells["L" + index].Value = giaDuThau;
                            sheet.Cells["M" + index].Value = giaGoiThau;
                            sheet.Cells["N" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                            if (sheet.Cells["K" + indexTongLV].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["K" + indexTongLV].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["K" + indexTongLV].Value = tongSoGoiThau;

                            if (sheet.Cells["L" + indexTongLV].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["L" + indexTongLV].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["L" + indexTongLV].Value = tongGiaDuThau;
                            if (sheet.Cells["M" + indexTongLV].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["M" + indexTongLV].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["M" + indexTongLV].Value = tongGiaGoiThau;
                            if (sheet.Cells["N" + indexTongLV].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["N" + indexTongLV].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["N" + indexTongLV].Value = tongChenhLech;

                        }
                    }
                    int indexTongHinhThuc = 11 + lstLinhVucDauThau.Count + lstHTLCNT.Count;
                    foreach (var item in lstDataHinhThucDauThau)
                    {
                        int idHinhThuc = (int)GetValueObject(item, "IdHinhThucLuaChon");
                        int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                        double giaDuThau = (double)GetValueObject(item, "GiaDuThau") / dvt;
                        double giaGoiThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                        int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                        int index = Array.IndexOf(idHinhThucs, idHinhThuc) + 12 + lstLinhVucDauThau.Count;

                        if (idNhomDuAn == 2)
                        {

                            string lastNameCell = "F" + index;
                            string firstNameCell = "C" + index;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells["E" + index].Style.Numberformat.Format = "#,##0";
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells["C" + index].Value = soGoiThau;
                            sheet.Cells["D" + index].Value = giaDuThau;
                            sheet.Cells["E" + index].Value = giaGoiThau;
                            sheet.Cells["F" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                            if (sheet.Cells["C" + indexTongHinhThuc].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["C" + indexTongHinhThuc].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["C" + indexTongHinhThuc].Value = tongSoGoiThau;

                            if (sheet.Cells["D" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["D" + indexTongHinhThuc].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["D" + indexTongHinhThuc].Value = tongGiaDuThau;
                            if (sheet.Cells["E" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["E" + indexTongHinhThuc].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["E" + indexTongHinhThuc].Value = tongGiaGoiThau;
                            if (sheet.Cells["F" + indexTongHinhThuc].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["F" + indexTongHinhThuc].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["F" + indexTongHinhThuc].Value = tongChenhLech;
                        }
                        if (idNhomDuAn == 3)
                        {
                            sheet.Cells["G" + index].Value = soGoiThau;
                            sheet.Cells["H" + index].Value = giaDuThau;
                            sheet.Cells["I" + index].Value = giaGoiThau;
                            sheet.Cells["J" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                            if (sheet.Cells["G" + indexTongHinhThuc].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["G" + indexTongHinhThuc].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["G" + indexTongHinhThuc].Value = tongSoGoiThau;

                            if (sheet.Cells["H" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["H" + indexTongHinhThuc].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["H" + indexTongHinhThuc].Value = tongGiaDuThau;
                            if (sheet.Cells["I" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["I" + indexTongHinhThuc].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["I" + indexTongHinhThuc].Value = tongGiaGoiThau;
                            if (sheet.Cells["J" + indexTongHinhThuc].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["J" + indexTongHinhThuc].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["J" + indexTongHinhThuc].Value = tongChenhLech;
                        }
                        if (idNhomDuAn == 4)
                        {
                            sheet.Cells["K" + index].Value = soGoiThau;
                            sheet.Cells["L" + index].Value = giaDuThau;
                            sheet.Cells["M" + index].Value = giaGoiThau;
                            sheet.Cells["N" + index].Value = giaDuThau - giaGoiThau;
                            int tongSoGoiThau = 0;
                            double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                            if (sheet.Cells["K" + indexTongHinhThuc].Value != null)
                            {
                                tongSoGoiThau = int.Parse(sheet.Cells["K" + indexTongHinhThuc].Value.ToString());
                                tongSoGoiThau = tongSoGoiThau + soGoiThau;
                            }
                            else
                            {
                                tongSoGoiThau = soGoiThau;
                            }
                            sheet.Cells["K" + indexTongHinhThuc].Value = tongSoGoiThau;

                            if (sheet.Cells["L" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaDuThau = double.Parse(sheet.Cells["L" + indexTongHinhThuc].Value.ToString());
                                tongGiaDuThau = tongGiaDuThau + giaDuThau;
                            }
                            else
                            {
                                tongGiaDuThau = giaDuThau;
                            }
                            sheet.Cells["L" + indexTongHinhThuc].Value = tongGiaDuThau;
                            if (sheet.Cells["M" + indexTongHinhThuc].Value != null)
                            {
                                tongGiaGoiThau = double.Parse(sheet.Cells["M" + indexTongHinhThuc].Value.ToString());
                                tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                            }
                            else
                            {
                                tongGiaGoiThau = giaGoiThau;
                            }
                            sheet.Cells["M" + indexTongHinhThuc].Value = tongGiaGoiThau;
                            if (sheet.Cells["N" + indexTongHinhThuc].Value != null)
                            {
                                tongChenhLech = double.Parse(sheet.Cells["N" + indexTongHinhThuc].Value.ToString());
                                tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                            }
                            else
                            {
                                tongChenhLech = giaDuThau - giaGoiThau;
                            }
                            sheet.Cells["N" + indexTongHinhThuc].Value = tongChenhLech;

                        }
                    }
                    indexLinhVuc = 9;
                    indexHinhThuc = 11 + lstLinhVucDauThau.Count;

                    for (var i = 0; i < lstLinhVucDauThau.Count + 1; i++)
                    {
                        int tongSoGoiThauLV = 0;
                        double tongGiaDuThauLV = 0, tongGiaGoiThauLV = 0, tongChenhLechLV = 0;
                        if (sheet.Cells["C" + indexLinhVuc].Value != null)
                        {
                            tongSoGoiThauLV += int.Parse(sheet.Cells["C" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["G" + indexLinhVuc].Value != null)
                        {

                            tongSoGoiThauLV += int.Parse(sheet.Cells["G" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["K" + indexLinhVuc].Value != null)
                        {
                            tongSoGoiThauLV += int.Parse(sheet.Cells["K" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["D" + indexLinhVuc].Value != null)
                        {
                            tongGiaDuThauLV += double.Parse(sheet.Cells["D" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["H" + indexLinhVuc].Value != null)
                        {

                            tongGiaDuThauLV += double.Parse(sheet.Cells["H" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["L" + indexLinhVuc].Value != null)
                        {
                            tongGiaDuThauLV += double.Parse(sheet.Cells["L" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["E" + indexLinhVuc].Value != null)
                        {
                            tongGiaGoiThauLV += double.Parse(sheet.Cells["E" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["I" + indexLinhVuc].Value != null)
                        {

                            tongGiaGoiThauLV += double.Parse(sheet.Cells["I" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["M" + indexLinhVuc].Value != null)
                        {
                            tongGiaGoiThauLV += double.Parse(sheet.Cells["M" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["F" + indexLinhVuc].Value != null)
                        {
                            tongChenhLechLV += double.Parse(sheet.Cells["F" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["J" + indexLinhVuc].Value != null)
                        {

                            tongChenhLechLV += double.Parse(sheet.Cells["J" + indexLinhVuc].Value.ToString());
                        }
                        if (sheet.Cells["N" + indexLinhVuc].Value != null)
                        {
                            tongChenhLechLV += double.Parse(sheet.Cells["N" + indexLinhVuc].Value.ToString());
                        }
                        if (tongSoGoiThauLV != 0)
                            sheet.Cells["O" + indexLinhVuc].Value = tongSoGoiThauLV;
                        if (tongGiaDuThauLV != 0)
                            sheet.Cells["P" + indexLinhVuc].Value = tongGiaDuThauLV;
                        if (tongGiaGoiThauLV != 0)
                            sheet.Cells["Q" + indexLinhVuc].Value = tongGiaGoiThauLV;
                        if (tongChenhLechLV != 0)
                            sheet.Cells["R" + indexLinhVuc].Value = tongChenhLechLV;
                        indexLinhVuc++;
                    }

                    for (var i = 0; i < lstHTLCNT.Count + 1; i++)
                    {
                        int tongSoGoiThauHT = 0;
                        double tongGiaDuThauHT = 0, tongGiaGoiThauHT = 0, tongChenhLechHT = 0;
                        if (sheet.Cells["C" + indexHinhThuc].Value != null)
                        {
                            tongSoGoiThauHT += int.Parse(sheet.Cells["C" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["G" + indexHinhThuc].Value != null)
                        {

                            tongSoGoiThauHT += int.Parse(sheet.Cells["G" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["K" + indexHinhThuc].Value != null)
                        {
                            tongSoGoiThauHT += int.Parse(sheet.Cells["K" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["D" + indexHinhThuc].Value != null)
                        {
                            tongGiaDuThauHT += double.Parse(sheet.Cells["D" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["H" + indexHinhThuc].Value != null)
                        {

                            tongGiaDuThauHT += double.Parse(sheet.Cells["H" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["L" + indexHinhThuc].Value != null)
                        {
                            tongGiaDuThauHT += double.Parse(sheet.Cells["L" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["E" + indexHinhThuc].Value != null)
                        {
                            tongGiaGoiThauHT += double.Parse(sheet.Cells["E" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["I" + indexHinhThuc].Value != null)
                        {

                            tongGiaGoiThauHT += double.Parse(sheet.Cells["I" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["M" + indexHinhThuc].Value != null)
                        {
                            tongGiaGoiThauHT += double.Parse(sheet.Cells["M" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["F" + indexHinhThuc].Value != null)
                        {
                            tongChenhLechHT += double.Parse(sheet.Cells["F" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["J" + indexHinhThuc].Value != null)
                        {

                            tongChenhLechHT += double.Parse(sheet.Cells["J" + indexHinhThuc].Value.ToString());
                        }
                        if (sheet.Cells["N" + indexHinhThuc].Value != null)
                        {
                            tongChenhLechHT += double.Parse(sheet.Cells["N" + indexHinhThuc].Value.ToString());
                        }
                        if (tongSoGoiThauHT != 0)
                            sheet.Cells["O" + indexHinhThuc].Value = tongSoGoiThauHT;
                        if (tongGiaDuThauHT != 0)
                            sheet.Cells["P" + indexHinhThuc].Value = tongGiaDuThauHT;
                        if (tongGiaGoiThauHT != 0)
                            sheet.Cells["Q" + indexHinhThuc].Value = tongGiaGoiThauHT;
                        if (tongChenhLechHT != 0)
                            sheet.Cells["R" + indexHinhThuc].Value = tongChenhLechHT;
                        indexHinhThuc++;
                    }


                    pck.SaveAs(new FileInfo(fullPath));
                    _baoCaoTableService.AddBaoCao(documentName, "BaoCaoDauThauTheoNguonVonTinh", folderReport, User.Identity.Name);
                    _baoCaoTableService.Save();
                }
                return documentName;
            }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }
        #endregion

        #region Thanh Pho Phe Duyet
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("ThanhPho")]
        [HttpGet]
        public HttpResponseMessage ExportThanhPho(HttpRequestMessage request, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = ThanhPho(iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string ThanhPho(int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            int dvt = 1;
            if (szDonViTinh == "Đồng")
            {
                dvt = 1;
            }
            if (szDonViTinh == "Nghìn Đồng")
            {
                dvt = 1000;
            }
            if (szDonViTinh == "Triệu Đồng")
            {
                dvt = 1000000;
            }
            if (szDonViTinh == "Tỷ Đồng")
            {
                dvt = 1000000000;
            }
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/Mau_TongHopBCKQLCNT_UBNDThanhPho.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_TongHopKetQuaLuaChonNhaThau_UBNDThanhPho_"+ namPD +"-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
          
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                       
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                      

                        string title = sheet.Cells["A2"].Value.ToString();
                        title = title.Replace("{{nam}}", namPD);
                        sheet.Cells["A2"].Value = title;

                        string title1 = sheet.Cells["A3"].Value.ToString();
                        title1 = title1.Replace("{{nam}}", namPD);
                        sheet.Cells["A3"].Value = title1;

                        string dvTinh = sheet.Cells["N4"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh);
                        sheet.Cells["N4"].Value = dvTinh;
                        var lstDataLinhVucDauThau = _baoCaoDauThauService.GetKetQuaDauThauTheoLinhVuc_CoQuanPheDuyet(1,iNamPD);
                        var lstDataHinhThucDauThau =_baoCaoDauThauService.GetKetQuaDauThauTheoHinhThuc_CoQuanPheDuyet(1,iNamPD);
                        var lstLinhVucDauThau = _linhVucDauThauService.GetAll().ToList();
                        int[] idLinhVucs = new int[lstLinhVucDauThau.Count];
                        int indexLinhVuc = 9;
                        for (var i = 0; i < lstLinhVucDauThau.Count; i++)
                        {
                            idLinhVucs[i] = lstLinhVucDauThau[i].IdLinhVucDauThau;
                            sheet.InsertRow(indexLinhVuc, 1);

                            var firstNameCell = "A" + indexLinhVuc;
                            var lastNameCell = "R" + indexLinhVuc;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["C" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["C" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["G" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["G" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["K" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["K" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["O" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["O" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["B" + indexLinhVuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                            sheet.Cells["B" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexLinhVuc].Value = lstLinhVucDauThau[i].TenLinhVucDauThau;
                            indexLinhVuc++;
                        }
                        var lstHTLCNT = _hinhThucLuaChonNhaThauService.GetAll().ToList();
                        int[] idHinhThucs = new int[lstHTLCNT.Count];
                        int indexHinhThuc = 11 + lstLinhVucDauThau.Count;
                        for (var i = 0; i < lstHTLCNT.Count; i++)
                        {
                            idHinhThucs[i] = lstHTLCNT[i].IdHinhThucLuaChon;
                            sheet.InsertRow(indexHinhThuc, 1);
                            var firstNameCell = "A" + indexHinhThuc;
                            var lastNameCell = "R" + indexHinhThuc;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["C" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["C" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["G" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["G" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["K" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["K" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["O" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells["O" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["B" + indexHinhThuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                            sheet.Cells["B" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexHinhThuc].Value = lstHTLCNT[i].TenHinhThucLuaChon;
                            indexHinhThuc++;
                        }

                        int indexTongLV = 9 + lstLinhVucDauThau.Count;
                        foreach (var item in lstDataLinhVucDauThau)
                        {
                            int idLinhVuc = (int)GetValueObject(item, "IdLinhVucDauThau");
                            int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                            double giaDuThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                            double giaGoiThau = (double)GetValueObject(item, "GiaTrungThau") / dvt;
                            int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                            int index = Array.IndexOf(idLinhVucs, idLinhVuc) + 9;

                            if (idNhomDuAn == 2)
                            {

                                string lastNameCell = "F" + index;
                                string firstNameCell = "C" + index;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells["C" + index].Value = soGoiThau;
                                sheet.Cells["D" + index].Value = giaDuThau;
                                sheet.Cells["E" + index].Value = giaGoiThau;
                                sheet.Cells["F" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;

                                if (sheet.Cells["C" + indexTongLV].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["C" + indexTongLV].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["C" + indexTongLV].Value = tongSoGoiThau;

                                if (sheet.Cells["D" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["D" + indexTongLV].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["D" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["E" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["E" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["E" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["F" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["F" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["F" + indexTongLV].Value = tongChenhLech;

                            }
                            if (idNhomDuAn == 3)
                            {
                                string lastNameCell = "J" + index;
                                string firstNameCell = "G" + index;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells["G" + index].Value = soGoiThau;
                                sheet.Cells["H" + index].Value = giaDuThau;
                                sheet.Cells["I" + index].Value = giaGoiThau;
                                sheet.Cells["J" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["G" + indexTongLV].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["G" + indexTongLV].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["G" + indexTongLV].Value = tongSoGoiThau;

                                if (sheet.Cells["H" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["H" + indexTongLV].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["H" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["I" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["I" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["I" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["J" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["J" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["J" + indexTongLV].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 4)
                            {
                                string lastNameCell = "N" + index;
                                string firstNameCell = "K" + index;

                                sheet.Cells["K" + index].Value = soGoiThau;
                                sheet.Cells["L" + index].Value = giaDuThau;
                                sheet.Cells["M" + index].Value = giaGoiThau;
                                sheet.Cells["N" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["K" + indexTongLV].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["K" + indexTongLV].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["K" + indexTongLV].Value = tongSoGoiThau;

                                if (sheet.Cells["L" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["L" + indexTongLV].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["L" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["M" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["M" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["M" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["N" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["N" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["N" + indexTongLV].Value = tongChenhLech;

                            }
                        }
                        int indexTongHinhThuc = 11 + lstLinhVucDauThau.Count + lstHTLCNT.Count;
                        foreach (var item in lstDataHinhThucDauThau)
                        {
                            int idHinhThuc = (int)GetValueObject(item, "IdHinhThucLuaChon");
                            int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                            double giaDuThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                            double giaGoiThau = (double)GetValueObject(item, "GiaTrungThau") / dvt;
                            int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                            int index = Array.IndexOf(idHinhThucs, idHinhThuc) + 12 + lstLinhVucDauThau.Count;

                            if (idNhomDuAn == 2)
                            {

                                string lastNameCell = "F" + index;
                                string firstNameCell = "C" + index;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells["E" + index].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells["C" + index].Value = soGoiThau;
                                sheet.Cells["D" + index].Value = giaDuThau;
                                sheet.Cells["E" + index].Value = giaGoiThau;
                                sheet.Cells["F" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["C" + indexTongHinhThuc].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["C" + indexTongHinhThuc].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["C" + indexTongHinhThuc].Value = tongSoGoiThau;

                                if (sheet.Cells["D" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["D" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["D" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["E" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["E" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["E" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["F" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["F" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["F" + indexTongHinhThuc].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 3)
                            {
                                sheet.Cells["G" + index].Value = soGoiThau;
                                sheet.Cells["H" + index].Value = giaDuThau;
                                sheet.Cells["I" + index].Value = giaGoiThau;
                                sheet.Cells["J" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["G" + indexTongHinhThuc].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["G" + indexTongHinhThuc].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["G" + indexTongHinhThuc].Value = tongSoGoiThau;

                                if (sheet.Cells["H" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["H" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["H" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["I" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["I" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["I" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["J" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["J" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["J" + indexTongHinhThuc].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 4)
                            {
                                sheet.Cells["K" + index].Value = soGoiThau;
                                sheet.Cells["L" + index].Value = giaDuThau;
                                sheet.Cells["M" + index].Value = giaGoiThau;
                                sheet.Cells["N" + index].Value = giaDuThau - giaGoiThau;
                                int tongSoGoiThau = 0;
                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["K" + indexTongHinhThuc].Value != null)
                                {
                                    tongSoGoiThau = int.Parse(sheet.Cells["K" + indexTongHinhThuc].Value.ToString());
                                    tongSoGoiThau = tongSoGoiThau + soGoiThau;
                                }
                                else
                                {
                                    tongSoGoiThau = soGoiThau;
                                }
                                sheet.Cells["K" + indexTongHinhThuc].Value = tongSoGoiThau;

                                if (sheet.Cells["L" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["L" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["L" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["M" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["M" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["M" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["N" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["N" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["N" + indexTongHinhThuc].Value = tongChenhLech;

                            }
                        }
                        indexLinhVuc = 9;
                        indexHinhThuc = 11 + lstLinhVucDauThau.Count;

                        for (var i = 0; i < lstLinhVucDauThau.Count + 1; i++)
                        {
                            int tongSoGoiThauLV = 0;
                            double tongGiaDuThauLV = 0, tongGiaGoiThauLV = 0, tongChenhLechLV = 0;
                            if (sheet.Cells["C" + indexLinhVuc].Value != null)
                            {
                                tongSoGoiThauLV += int.Parse(sheet.Cells["C" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["G" + indexLinhVuc].Value != null)
                            {

                                tongSoGoiThauLV += int.Parse(sheet.Cells["G" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["K" + indexLinhVuc].Value != null)
                            {
                                tongSoGoiThauLV += int.Parse(sheet.Cells["K" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["D" + indexLinhVuc].Value != null)
                            {
                                tongGiaDuThauLV += double.Parse(sheet.Cells["D" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["H" + indexLinhVuc].Value != null)
                            {

                                tongGiaDuThauLV += double.Parse(sheet.Cells["H" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["L" + indexLinhVuc].Value != null)
                            {
                                tongGiaDuThauLV += double.Parse(sheet.Cells["L" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["E" + indexLinhVuc].Value != null)
                            {
                                tongGiaGoiThauLV += double.Parse(sheet.Cells["E" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["I" + indexLinhVuc].Value != null)
                            {

                                tongGiaGoiThauLV += double.Parse(sheet.Cells["I" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["M" + indexLinhVuc].Value != null)
                            {
                                tongGiaGoiThauLV += double.Parse(sheet.Cells["M" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["F" + indexLinhVuc].Value != null)
                            {
                                tongChenhLechLV += double.Parse(sheet.Cells["F" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["J" + indexLinhVuc].Value != null)
                            {

                                tongChenhLechLV += double.Parse(sheet.Cells["J" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["N" + indexLinhVuc].Value != null)
                            {
                                tongChenhLechLV += double.Parse(sheet.Cells["N" + indexLinhVuc].Value.ToString());
                            }
                            if(tongSoGoiThauLV!=0)
                            sheet.Cells["O" + indexLinhVuc].Value = tongSoGoiThauLV;
                            if (tongGiaDuThauLV != 0)
                                sheet.Cells["P" + indexLinhVuc].Value = tongGiaDuThauLV;
                            if (tongGiaGoiThauLV != 0)
                                sheet.Cells["Q" + indexLinhVuc].Value = tongGiaGoiThauLV;
                            if (tongChenhLechLV != 0)
                                sheet.Cells["R" + indexLinhVuc].Value = tongChenhLechLV;
                            indexLinhVuc++;
                        }

                        for (var i = 0; i < lstHTLCNT.Count + 1; i++)
                        {
                            int tongSoGoiThauHT = 0;
                            double tongGiaDuThauHT = 0, tongGiaGoiThauHT = 0, tongChenhLechHT = 0;
                            if (sheet.Cells["C" + indexHinhThuc].Value != null)
                            {
                                tongSoGoiThauHT += int.Parse(sheet.Cells["C" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["G" + indexHinhThuc].Value != null)
                            {

                                tongSoGoiThauHT += int.Parse(sheet.Cells["G" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["K" + indexHinhThuc].Value != null)
                            {
                                tongSoGoiThauHT += int.Parse(sheet.Cells["K" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["D" + indexHinhThuc].Value != null)
                            {
                                tongGiaDuThauHT += double.Parse(sheet.Cells["D" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["H" + indexHinhThuc].Value != null)
                            {

                                tongGiaDuThauHT += double.Parse(sheet.Cells["H" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["L" + indexHinhThuc].Value != null)
                            {
                                tongGiaDuThauHT += double.Parse(sheet.Cells["L" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["E" + indexHinhThuc].Value != null)
                            {
                                tongGiaGoiThauHT += double.Parse(sheet.Cells["E" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["I" + indexHinhThuc].Value != null)
                            {

                                tongGiaGoiThauHT += double.Parse(sheet.Cells["I" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["M" + indexHinhThuc].Value != null)
                            {
                                tongGiaGoiThauHT += double.Parse(sheet.Cells["M" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["F" + indexHinhThuc].Value != null)
                            {
                                tongChenhLechHT += double.Parse(sheet.Cells["F" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["J" + indexHinhThuc].Value != null)
                            {

                                tongChenhLechHT += double.Parse(sheet.Cells["J" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["N" + indexHinhThuc].Value != null)
                            {
                                tongChenhLechHT += double.Parse(sheet.Cells["N" + indexHinhThuc].Value.ToString());
                            }
                            if (tongSoGoiThauHT!=0)
                            sheet.Cells["O" + indexHinhThuc].Value = tongSoGoiThauHT;
                            if(tongGiaDuThauHT!=0)
                            sheet.Cells["P" + indexHinhThuc].Value = tongGiaDuThauHT;
                            if(tongGiaGoiThauHT!=0)
                            sheet.Cells["Q" + indexHinhThuc].Value = tongGiaGoiThauHT;
                            if(tongChenhLechHT!=0)
                            sheet.Cells["R" + indexHinhThuc].Value = tongChenhLechHT;
                            indexHinhThuc++;
                        }
                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "BaoCaoDauThauTheoNguonVonTP", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
           
        }
        #endregion

        #region Cac du an su dung nguon von ODA
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("KQLCNTVonODA")]
        [HttpGet]
        public HttpResponseMessage ExportKQLCNTVonODA(HttpRequestMessage request, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = KQLCNTVonODA(iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string KQLCNTVonODA(int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            int dvt = 1;
            if (szDonViTinh == "Đồng")
            {
                dvt = 1;
            }
            if (szDonViTinh == "Nghìn Đồng")
            {
                dvt = 1000;
            }
            if (szDonViTinh == "Triệu Đồng")
            {
                dvt = 1000000;
            }
            if (szDonViTinh == "Tỷ Đồng")
            {
                dvt = 1000000000;
            }
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/Mau_THKetQuaLuaChonNhaThau_CacDuAnSuDungVonODA.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("THKetQuaLuaChonNhaThau_CacDuAnSuDungVonODA_" + namPD + "-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            //string documentName = string.Format("BC_KetQuaLuaChonNhaThau_UBNDTinhPD_-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
     
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        //Data Access, load order header data.
                        //var duans = _duAnService.GetAll();

                        //int rowIndex = 1;
                        //foreach (var da in duans)
                        //{
                        //    if (da.IdDuAn == iIDDuAn)
                        //    {
                        //        rowIndex++;
                        //        sheet.Cells[rowIndex, 1].Value = da.TenDuAn;
                        //    }
                        //}
                        string title = sheet.Cells["A2"].Value.ToString();
                        title = title.Replace("{{nam}}", namPD);
                        sheet.Cells["A2"].Value = title;

                        string title1 = sheet.Cells["A3"].Value.ToString();
                        title1 = title1.Replace("{{nam}}", namPD);
                        sheet.Cells["A3"].Value = title1;

                        string dvTinh = sheet.Cells["K4"].Value.ToString();
                        dvTinh = dvTinh.Replace("{{donvitinh}}", szDonViTinh + "");
                        sheet.Cells["K4"].Value = dvTinh;
                        var lstDataLinhVucDauThau = this._baoCaoDauThauService.GetKetQuaDauThauTheoLinhVuc_NguonVon(3,iNamPD);
                        var lstDataHinhThucDauThau = this._baoCaoDauThauService.GetKetQuaDauThauTheoHinhThuc_NguonVon(3,iNamPD);
                        var lstLinhVucDauThau = _linhVucDauThauService.GetAll().ToList();
                        int[] idLinhVucs = new int[lstLinhVucDauThau.Count];
                        int indexLinhVuc = 9;
                        for (var i = 0; i < lstLinhVucDauThau.Count; i++)
                        {
                            idLinhVucs[i] = lstLinhVucDauThau[i].IdLinhVucDauThau;
                            sheet.InsertRow(indexLinhVuc, 1);

                            var firstNameCell = "A" + indexLinhVuc;
                            var lastNameCell = "N" + indexLinhVuc;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["B" + indexLinhVuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["B" + indexLinhVuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                            sheet.Cells["B" + indexLinhVuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexLinhVuc].Value = lstLinhVucDauThau[i].TenLinhVucDauThau;
                            indexLinhVuc++;
                        }
                        var lstHTLCNT = _hinhThucLuaChonNhaThauService.GetAll().ToList();
                        int[] idHinhThucs = new int[lstHTLCNT.Count];
                        int indexHinhThuc = 11 + lstLinhVucDauThau.Count;
                        for (var i = 0; i < lstHTLCNT.Count; i++)
                        {
                            idHinhThucs[i] = lstHTLCNT[i].IdHinhThucLuaChon;
                            sheet.InsertRow(indexHinhThuc, 1);
                            var firstNameCell = "A" + indexHinhThuc;
                            var lastNameCell = "N" + indexHinhThuc;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                            sheet.Cells["B" + indexHinhThuc].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            sheet.Cells["B" + indexHinhThuc].Style.Font.SetFromFont(new Font("Times New Roman", 14));
                            sheet.Cells["B" + indexHinhThuc].Style.Numberformat.Format = "#";
                            sheet.Cells["B" + indexHinhThuc].Value = lstHTLCNT[i].TenHinhThucLuaChon;
                            indexHinhThuc++;
                        }

                        int indexTongLV = 9 + lstLinhVucDauThau.Count;
                        foreach (var item in lstDataLinhVucDauThau)
                        {
                            int idLinhVuc = (int)GetValueObject(item, "IdLinhVucDauThau");
                            int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                            double giaDuThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                            double giaGoiThau = (double)GetValueObject(item, "GiaTrungThau") / dvt;
                            int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                            int index = Array.IndexOf(idLinhVucs, idLinhVuc) + 9;

                            if (idNhomDuAn == 2)
                            {

                                string lastNameCell = "E" + index;
                                string firstNameCell = "C" + index;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                sheet.Cells["C" + index].Value = giaDuThau;
                                sheet.Cells["D" + index].Value = giaGoiThau;
                                sheet.Cells["E" + index].Value = giaDuThau - giaGoiThau;


                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;



                                if (sheet.Cells["C" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["C" + indexTongLV].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["C" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["D" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["D" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["D" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["E" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["E" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["E" + indexTongLV].Value = tongChenhLech;

                            }
                            if (idNhomDuAn == 3)
                            {
                                string lastNameCell = "H" + index;
                                string firstNameCell = "F" + index;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Numberformat.Format = "#,##0";
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet.Cells[firstNameCell + ":" + lastNameCell].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                sheet.Cells["F" + index].Value = giaDuThau;
                                sheet.Cells["G" + index].Value = giaGoiThau;
                                sheet.Cells["H" + index].Value = giaDuThau - giaGoiThau;

                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;


                                if (sheet.Cells["F" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["F" + indexTongLV].Value.ToString());

                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["F" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["G" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["G" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["G" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["H" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["H" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["H" + indexTongLV].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 4)
                            {
                                string lastNameCell = "K" + index;
                                string firstNameCell = "I" + index;
                                sheet.Cells["I" + index].Value = giaDuThau;
                                sheet.Cells["J" + index].Value = giaGoiThau;
                                sheet.Cells["K" + index].Value = giaDuThau - giaGoiThau;

                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;


                                if (sheet.Cells["I" + indexTongLV].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["I" + indexTongLV].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["I" + indexTongLV].Value = tongGiaDuThau;
                                if (sheet.Cells["J" + indexTongLV].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["J" + indexTongLV].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["J" + indexTongLV].Value = tongGiaGoiThau;
                                if (sheet.Cells["K" + indexTongLV].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["K" + indexTongLV].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["K" + indexTongLV].Value = tongChenhLech;

                            }
                        }
                        int indexTongHinhThuc = 11 + lstLinhVucDauThau.Count + lstHTLCNT.Count;
                        foreach (var item in lstDataHinhThucDauThau)
                        {
                            int idHinhThuc = (int)GetValueObject(item, "IdHinhThucLuaChon");
                            int idNhomDuAn = (int)GetValueObject(item, "IdNhomDuAn");
                            double giaDuThau = (double)GetValueObject(item, "GiaGoiThau") / dvt;
                            double giaGoiThau = (double)GetValueObject(item, "GiaTrungThau") / dvt;
                            int soGoiThau = (int)GetValueObject(item, "SoGoiThau");
                            int index = Array.IndexOf(idHinhThucs, idHinhThuc) + 12 + lstLinhVucDauThau.Count;

                            if (idNhomDuAn == 2)
                            {


                                sheet.Cells["C" + index].Value = giaDuThau;
                                sheet.Cells["D" + index].Value = giaGoiThau;
                                sheet.Cells["E" + index].Value = giaDuThau - giaGoiThau;

                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["C" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["C" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["C" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["D" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["D" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["D" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["E" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["E" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["E" + indexTongHinhThuc].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 3)
                            {

                                sheet.Cells["F" + index].Value = giaDuThau;
                                sheet.Cells["G" + index].Value = giaGoiThau;
                                sheet.Cells["H" + index].Value = giaDuThau - giaGoiThau;

                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;

                                if (sheet.Cells["F" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["F" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["F" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["G" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["G" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["G" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["H" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["H" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["H" + indexTongHinhThuc].Value = tongChenhLech;
                            }
                            if (idNhomDuAn == 4)
                            {

                                sheet.Cells["I" + index].Value = giaDuThau;
                                sheet.Cells["J" + index].Value = giaGoiThau;
                                sheet.Cells["K" + index].Value = giaDuThau - giaGoiThau;

                                double tongGiaDuThau = 0, tongGiaGoiThau = 0, tongChenhLech = 0;
                                if (sheet.Cells["I" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaDuThau = double.Parse(sheet.Cells["I" + indexTongHinhThuc].Value.ToString());
                                    tongGiaDuThau = tongGiaDuThau + giaDuThau;
                                }
                                else
                                {
                                    tongGiaDuThau = giaDuThau;
                                }
                                sheet.Cells["I" + indexTongHinhThuc].Value = tongGiaDuThau;
                                if (sheet.Cells["J" + indexTongHinhThuc].Value != null)
                                {
                                    tongGiaGoiThau = double.Parse(sheet.Cells["J" + indexTongHinhThuc].Value.ToString());
                                    tongGiaGoiThau = tongGiaGoiThau + giaGoiThau;
                                }
                                else
                                {
                                    tongGiaGoiThau = giaGoiThau;
                                }
                                sheet.Cells["J" + indexTongHinhThuc].Value = tongGiaGoiThau;
                                if (sheet.Cells["K" + indexTongHinhThuc].Value != null)
                                {
                                    tongChenhLech = double.Parse(sheet.Cells["K" + indexTongHinhThuc].Value.ToString());
                                    tongChenhLech = tongChenhLech + giaDuThau - giaGoiThau;
                                }
                                else
                                {
                                    tongChenhLech = giaDuThau - giaGoiThau;
                                }
                                sheet.Cells["K" + indexTongHinhThuc].Value = tongChenhLech;

                            }
                        }
                        indexLinhVuc = 9;
                        indexHinhThuc = 11 + lstLinhVucDauThau.Count;

                        for (var i = 0; i < lstLinhVucDauThau.Count + 1; i++)
                        {

                            double tongGiaDuThauLV = 0, tongGiaGoiThauLV = 0, tongChenhLechLV = 0;

                            if (sheet.Cells["C" + indexLinhVuc].Value != null)
                            {
                                tongGiaDuThauLV += double.Parse(sheet.Cells["C" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["F" + indexLinhVuc].Value != null)
                            {

                                tongGiaDuThauLV += double.Parse(sheet.Cells["F" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["I" + indexLinhVuc].Value != null)
                            {
                                tongGiaDuThauLV += double.Parse(sheet.Cells["I" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["D" + indexLinhVuc].Value != null)
                            {
                                tongGiaGoiThauLV += double.Parse(sheet.Cells["D" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["G" + indexLinhVuc].Value != null)
                            {

                                tongGiaGoiThauLV += double.Parse(sheet.Cells["G" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["J" + indexLinhVuc].Value != null)
                            {
                                tongGiaGoiThauLV += double.Parse(sheet.Cells["J" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["E" + indexLinhVuc].Value != null)
                            {
                                tongChenhLechLV += double.Parse(sheet.Cells["E" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["H" + indexLinhVuc].Value != null)
                            {

                                tongChenhLechLV += double.Parse(sheet.Cells["H" + indexLinhVuc].Value.ToString());
                            }
                            if (sheet.Cells["K" + indexLinhVuc].Value != null)
                            {
                                tongChenhLechLV += double.Parse(sheet.Cells["K" + indexLinhVuc].Value.ToString());
                            }
                            if (tongGiaDuThauLV != 0)
                                sheet.Cells["L" + indexLinhVuc].Value = tongGiaDuThauLV;
                            if (tongGiaGoiThauLV != 0)
                                sheet.Cells["M" + indexLinhVuc].Value = tongGiaGoiThauLV;
                            if (tongChenhLechLV != 0)
                                sheet.Cells["N" + indexLinhVuc].Value = tongChenhLechLV;
                            indexLinhVuc++;
                        }

                        for (var i = 0; i < lstHTLCNT.Count + 1; i++)
                        {

                            double tongGiaDuThauHT = 0, tongGiaGoiThauHT = 0, tongChenhLechHT = 0;

                            if (sheet.Cells["C" + indexHinhThuc].Value != null)
                            {
                                tongGiaDuThauHT += double.Parse(sheet.Cells["C" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["F" + indexHinhThuc].Value != null)
                            {

                                tongGiaDuThauHT += double.Parse(sheet.Cells["F" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["I" + indexHinhThuc].Value != null)
                            {
                                tongGiaDuThauHT += double.Parse(sheet.Cells["I" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["D" + indexHinhThuc].Value != null)
                            {
                                tongGiaGoiThauHT += double.Parse(sheet.Cells["D" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["G" + indexHinhThuc].Value != null)
                            {

                                tongGiaGoiThauHT += double.Parse(sheet.Cells["G" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["J" + indexHinhThuc].Value != null)
                            {
                                tongGiaGoiThauHT += double.Parse(sheet.Cells["J" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["E" + indexHinhThuc].Value != null)
                            {
                                tongChenhLechHT += double.Parse(sheet.Cells["E" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["H" + indexHinhThuc].Value != null)
                            {

                                tongChenhLechHT += double.Parse(sheet.Cells["H" + indexHinhThuc].Value.ToString());
                            }
                            if (sheet.Cells["K" + indexHinhThuc].Value != null)
                            {
                                tongChenhLechHT += double.Parse(sheet.Cells["K" + indexHinhThuc].Value.ToString());
                            }
                            if (tongGiaDuThauHT != 0)
                                sheet.Cells["L" + indexHinhThuc].Value = tongGiaDuThauHT;
                            if (tongGiaGoiThauHT != 0)
                                sheet.Cells["M" + indexHinhThuc].Value = tongGiaGoiThauHT;
                            if (tongChenhLechHT != 0)
                                sheet.Cells["N" + indexHinhThuc].Value = tongChenhLechHT;
                            indexHinhThuc++;
                        }


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "BaoCaoDauThauTheoVonODA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
          
        }
        #endregion
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            
            var TenGoiThau = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return TenGoiThau;
        }
        
    }
}

