﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.BCService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaoCaoTienDo")]
    public class BaoCaoTienDoController : ApiControllerBase
    {
        private IBaoCaoTienDoService _baoCaoTienDoService;
        private IDuAnService _duAnService;
        private IBaoCaoTableService _baoCaoTableService;
        private IVonCapNhatKLNTService _vonCapNhatKLNTService;
        private IQLTD_CTCV_ChuTruongDauTuService _qLTD_CTCV_ChuTruongDauTuService;
        private IQLTD_CTCV_ChuanBiDauTuService _qLTD_CTCV_ChuanBiDauTuService;
        private IQLTD_CTCV_ChuanBiThucHienService _qLTD_CTCV_ChuanBiThucHienService;
        private IQLTD_CTCV_ThucHienDuAnService _qLTD_CTCV_ThucHienDuAnService;
        private IQLTD_CTCV_QuyetToanDuAnService _qLTD_CTCV_QuyetToanDuAnService;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        private IQLTD_KeHoachTienDoChungService _qLTD_KeHoachTienDoChungService;
        private IQLTD_KhoKhanVuongMacService _qLTD_KhoKhanVuongMacService;
        private INhomDuAnTheoUserService _nhomDuAnTheoUserService;

        public BaoCaoTienDoController(IErrorService errorService,
            IDuAnService duAnService,
            IBaoCaoTienDoService baoCaoTienDoService,
            IBaoCaoTableService baoCaoTableService,
            IQLTD_CTCV_ChuTruongDauTuService qLTD_CTCV_ChuTruongDauTuService,
            IQLTD_CTCV_ChuanBiDauTuService qLTD_CTCV_ChuanBiDauTuService,
            IQLTD_CTCV_ChuanBiThucHienService qLTD_CTCV_ChuanBiThucHienService,
            IQLTD_CTCV_ThucHienDuAnService qLTD_CTCV_ThucHienDuAnService,
            IQLTD_CTCV_QuyetToanDuAnService qLTD_CTCV_QuyetToanDuAnService,
            IQLTD_KeHoachService qLTD_KeHoachService,
            IQLTD_KeHoachTienDoChungService qLTD_KeHoachTienDoChungService,
            IQLTD_KhoKhanVuongMacService qLTD_KhoKhanVuongMacService,
            INhomDuAnTheoUserService nhomDuAnTheoUserService,
            IVonCapNhatKLNTService vonCapNhatKLNTService) : base(errorService)
        {
            this._baoCaoTienDoService = baoCaoTienDoService;
            this._duAnService = duAnService;
            this._baoCaoTableService = baoCaoTableService;
            this._vonCapNhatKLNTService = vonCapNhatKLNTService;
            this._qLTD_CTCV_ChuTruongDauTuService = qLTD_CTCV_ChuTruongDauTuService;
            this._qLTD_CTCV_ChuanBiDauTuService = qLTD_CTCV_ChuanBiDauTuService;
            this._qLTD_CTCV_ChuanBiThucHienService = qLTD_CTCV_ChuanBiThucHienService;
            this._qLTD_CTCV_ThucHienDuAnService = qLTD_CTCV_ThucHienDuAnService;
            this._qLTD_CTCV_QuyetToanDuAnService = qLTD_CTCV_QuyetToanDuAnService;
            this._qLTD_KeHoachService = qLTD_KeHoachService;
            this._qLTD_KeHoachTienDoChungService = qLTD_KeHoachTienDoChungService;
            this._qLTD_KhoKhanVuongMacService = qLTD_KhoKhanVuongMacService;
            this._nhomDuAnTheoUserService = nhomDuAnTheoUserService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        string[] SoLaMacap1 = new[] { "I", "I.1", "I.2", "I.3", "I.4", "I.5", "II", "II.1", "II.2", "II.3", "II.4", "II.5", "III", "III.1", "III.2", "III.3", "III.4", "III.5", "IV", "IV.1", "IV.2", "IV.3", "IV.4", "IV.5", "V", "V.1", "V.2", "V.3", "V.4", "V.5" };
        string[] STT = new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30" };
        string[] STT1 = new[] { "1.1", "1.2", "1.3", "2.1", "2.2", "2.3", "3.1", "3.2", "3.3", "4.1", "4.2", "4.3", "5.1", "5.2", "5.3" };

        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("baocaotiendoDA")]
        [HttpGet]
        public HttpResponseMessage ExportXDCBNam(HttpRequestMessage request, string iIDDuAn, int iNamPD, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = VonDTXDCBNam(iIDDuAn, iNamPD, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string VonDTXDCBNam(string szIDDuAn, int iNamPD, string szDonViTinh)
        {
            string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BaoCaoQLTD.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoTienDoDuAn_" + namPD + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        //int iNam_1 = iNamPD - 1;

                        List<object> lstTTDACbiDTu = _baoCaoTienDoService.GetListDACbiDTu(szIDDuAn);
                        Dictionary<int, List<object>> dicDAConCbiDTu = _baoCaoTienDoService.GetDuAnCon(szIDDuAn);

                        List<object> lstTTDATD = _baoCaoTienDoService.GetListDA(szIDDuAn);

                        #region Phòng, cán bộ quản lý
                        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
                        var users = AppUserManager.Users;
                        foreach (AppUser user in users)
                        {
                            var roleTemp = AppUserManager.GetRoles(user.Id);
                            if (roleTemp.Contains("Nhân viên"))
                            {
                                dicUserNhanVien.Add(user.Id, user.FullName);
                            }
                        }

                        var lstPhong_CanBo = _duAnService.GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(szIDDuAn, dicUserNhanVien);
                        #endregion

                        #region Chủ trương đầu tư
                        Dictionary<int, int> dic_CTDT_TGHTCT = _baoCaoTienDoService.Getdic_CTDT_TGHTCT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CTDT_NGNV = _baoCaoTienDoService.Getdic_CTDT_NGNV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CTDT_CTDD = _baoCaoTienDoService.Getdic_CTDT_CTDD(szIDDuAn);
                        Dictionary<int, List<object>> dic_CTDT_LCTDT = _baoCaoTienDoService.Getdic_CTDT_LCTDT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CTDT_NTHSPD = _baoCaoTienDoService.Getdic_CTDT_NTHSPD(szIDDuAn);
                        Dictionary<int, List<object>> dic_CTDT_PDCTDT = _baoCaoTienDoService.Getdic_CTDT_PDCTDT(szIDDuAn);

                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CTDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CTDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_LCTDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        #endregion

                        #region Chuẩn bị đầu tư
                        Dictionary<int, List<object>> dic_CBDT_BCPATK = _baoCaoTienDoService.Getdic_CBDT_BCPATK(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_PDT_CBDT = _baoCaoTienDoService.Getdic_CBDT_PDT_CBDT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_PKHKCNT = _baoCaoTienDoService.Getdic_CBDT_PKHKCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_KSHT_CGDD_HTKT = _baoCaoTienDoService.Getdic_CBDT_KSHT_CGDD_HTKT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_PDQH = _baoCaoTienDoService.Getdic_CBDT_PDQH(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_KSDC = _baoCaoTienDoService.Getdic_CBDT_KSDC(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_HTPDDA = _baoCaoTienDoService.Getdic_CBDT_HTPDDA(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_TTDPDDA = _baoCaoTienDoService.Getdic_CBDT_TTDPDDA(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_PCCC_TTK = _baoCaoTienDoService.Getdic_CBDT_PCCC_TTK(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBDT_DLHS = _baoCaoTienDoService.Getdic_CBDT_DLHS(szIDDuAn);

                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBDT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBDT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_CBDT_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        #endregion

                        #region Chuẩn bị thực hiện
                        Dictionary<int, List<object>> dic_CBTH_PDT = _baoCaoTienDoService.Getdic_CBTH_PDT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_PKHLCNT = _baoCaoTienDoService.Getdic_CBTH_PKHLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_HTPD_THBV = _baoCaoTienDoService.Getdic_CBTH_HTPD_THBV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_TTD_TKBV = _baoCaoTienDoService.Getdic_CBTH_TTD_TKBV(szIDDuAn);
                        Dictionary<int, List<object>> dic_CBTH_DLHS = _baoCaoTienDoService.Getdic_CBTH_DLHS(szIDDuAn);

                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBTH = _qLTD_KeHoachTienDoChungService.Get_KHTDC_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBTH = _qLTD_KhoKhanVuongMacService.Get_KK_GP_CBTH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        #endregion

                        #region Thực hiện
                        Dictionary<int, List<object>> dic_THDT_HTTC = _baoCaoTienDoService.Getdic_THDT_HTTC(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_DTCXD = _baoCaoTienDoService.Getdic_THDT_DTCXD(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_DLCNT = _baoCaoTienDoService.Getdic_THDT_DLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_CTHLCNT = _baoCaoTienDoService.Getdic_THDT_CTHLCNT(szIDDuAn);
                        Dictionary<int, List<object>> dic_THDT_GPMB = _baoCaoTienDoService.Getdic_THDT_GPMB(szIDDuAn);

                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_TH = _qLTD_KeHoachTienDoChungService.Get_KHTDC_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_TH = _qLTD_KhoKhanVuongMacService.Get_KK_GP_TH_For_BaoCao_TienDo_PL1(szIDDuAn, 2);
                        IEnumerable<BC_Uyban_PL1_PDDA> lstDanhSo_CTHLCNT = _qLTD_CTCV_ThucHienDuAnService.Get_DanhSo_For_CV_CTHLCNT_For_BC_TienDo_ThucHien(szIDDuAn);
                        var lstDTKTC = _qLTD_CTCV_ThucHienDuAnService.Get_DangTrienKhaiThiCong_For_BC_Uyban_PL2(szIDDuAn);
                        var lstDLCNT = _qLTD_CTCV_ThucHienDuAnService.Get_DangLuaChonNhaThau_For_BC_Uyban_PL2(szIDDuAn);
                        var lstHTTC = _qLTD_CTCV_ThucHienDuAnService.Get_DaHoanThanhThiCong_For_BC_Uyban_PL2(szIDDuAn);
                        
                        #endregion

                        #region Quyết Toán
                        Dictionary<int, List<object>> dic_QT_HTQT = _baoCaoTienDoService.Getdic_QT_HTQT(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_QLCL = _baoCaoTienDoService.Getdic_QT_HTHS_QLCL(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_HSQT = _baoCaoTienDoService.Getdic_QT_HTHS_HSQT(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_KTDL = _baoCaoTienDoService.Getdic_QT_HTHS_KTDL(szIDDuAn);
                        Dictionary<int, List<object>> dic_QT_HTHS_TTQT = _baoCaoTienDoService.Getdic_QT_HTHS_TTQT(szIDDuAn);

                        IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_QT = _qLTD_KeHoachTienDoChungService.Get_KHTDC_QT_For_BaoCao_TienDo(szIDDuAn);
                        IEnumerable<BC_TienDo_KK_QT> lstKKGP_QT = _qLTD_KhoKhanVuongMacService.Get_KK_GP_QT_For_BaoCao_TienDo(szIDDuAn);
                        
                        #endregion

                        #region Kế hoạch vốn
                        #region Kế hoạch vốn
                        Dictionary<int, double> Dic_TD_DA_CBDT_KHV = _baoCaoTienDoService.GetDic_TD_DA_CBDT_KHV(szIDDuAn, iNamPD);
                        #endregion
                        #region Giải ngân
                        //Dictionary<int, double> Dic_TD_DA_CBDT_GN = _baoCaoTienDoService.GetDic_TD_DA_CBDT_GN(szIDDuAn, iNamPD);
                        Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN = _vonCapNhatKLNTService.GetGiaiNganVon(szIDDuAn);
                        #endregion
                        #region Kế hoạch vốn điều chỉnh
                        Dictionary<int, double> dic_DA_KHV_DXDC = _baoCaoTienDoService.Getdic_DA_KHV_DXDC(szIDDuAn, iNamPD);
                        #endregion
                        //Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN
                        #endregion

                        #region Tiến độ thực hiện
                        #region Lập chủ trương đầu tư
                        Dictionary<int, string> dic_TDTH_LCTDT = _baoCaoTienDoService.GetDic_TDTH_LCTDT(szIDDuAn);
                        #endregion
                        #region Chuẩn bị đầu tư
                        Dictionary<int, string> dic_TDTH_CBDT = _baoCaoTienDoService.GetDic_TDTH_CBDT(szIDDuAn);
                        #endregion
                        #region Chuẩn bị thực hiện
                        Dictionary<int, string> dic_TDTH_CBTH = _baoCaoTienDoService.GetDic_TDTH_CBTH(szIDDuAn);
                        #endregion
                        #region Thực hiện dự án - Đang thi công xây dựng
                        Dictionary<int, string> dic_TDTH_TH_DTCXD = _baoCaoTienDoService.GetDic_TDTH_TH_DTCXD(szIDDuAn);
                        #endregion
                        #region Thực hiện dự án - Giải phóng mặt bằng
                        Dictionary<int, string> dic_TDTH_TH_GPMB = _baoCaoTienDoService.GetDic_TDTH_TH_GPMB(szIDDuAn);
                        #endregion


                        #endregion

                        int iColumn = 0;
                        int iSoLaMa = 1;
                        int iSTTcap1 = 1;
                        int iSTTcap2 = 1;

                        int rowCDT = 11;
                        List<int> l_TongCongs = new List<int>();

                        int iBD = rowCDT - 1;
                        string[] az = { "C", "D", "E", "F", "L", "M", "O", "V", "Y", "AD", "AF", "AJ", "AN", "AP", "AQ", "BA", "BE", "BG", "BH", "BJ", "BK", "BQ", "BT", "BV", "CA", "CD", "CE","CQ","CT","CV","CW","CX", "CY","CZ","DA","DB","DC","DD","DF","DI", "DL","DM"/*,"DS"*/, "DX", "EA" };

                        int[] ar_So = { 1, 3, 4, 5, 6, 12, 13, 14, 15, 16, 20, 21, 23, 25, 29, 31, 36, 37, 38, 40, 41, 43, 44, 47, 48, 53, 56, 58, 60, 61, 63, 64, 69, 72, 73, 74, 75, 76, 79, 80, 81, 83, 84, 86, 87, 95, 98, 99, 104, 105, 106, 108, 112, 128, 131 };
                        int[] ar_Text = { 2, 7, 8, 9, 10, 11, 17, 24, 26, 27, 28, 33, 34, 35, 39, 45, 46, 49, 50, 51, 52, 54, 55, 65, 66, 67, 68, 70, 71, 77, 78, 88, 89, 90, 91, 92, 93, 94, 96, 97, 109, 111, 129, 130 };
                        int[] ar_Tien = { 22, 30, 32, 42, 57, 59, 62, 82, 100, 101, 102, 103, 107, 110, 113, 116, 117, 120, 121, 122, 123, 128, 131 };
                        int[] ar_PhanTram = { 118 };
                        int[] ar_KHV = { 116, 120, 121, 122 };
                        Format(sheet, 10, ar_So, ar_Text, ar_Tien, ar_PhanTram);

                        if (lstTTDACbiDTu.Count != 0)
                        {
                            sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                            sheet.Cells[rowCDT, 2].Value = "CHUẨN BỊ ĐẦU TƯ NĂM " + namPD;
                            sheet.Row(rowCDT).Style.Font.Bold = true;
                            l_TongCongs.Add(rowCDT);
                            int a = rowCDT;

                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);

                            rowCDT++;
                            iSoLaMa = 1;

                            List<int> l_LVNNs = new List<int>();

                            foreach (object oLVNN in lstTTDACbiDTu)
                            {
                                string TenLinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oLVNN, "TenLinhVucNganhNghe");

                                sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                                sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                int b = rowCDT;
                                l_LVNNs.Add(rowCDT);
                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                rowCDT++;

                                List<object> lGDDT = (List<object>)BaoCaoDungChung.GetValueObject(oLVNN, "grpGDDT");

                                List<int> l_GDDAs = new List<int>();
                                for (int temp = 128; temp < 132; temp++)
                                {
                                    sheet.Cells[rowCDT, temp].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[rowCDT, temp].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    sheet.Cells[rowCDT, temp].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                }
                                foreach (object oGDDT in lGDDT)
                                {
                                    string TenGiaiDoanDuAn = (string)BaoCaoDungChung.GetValueObject(oGDDT, "TenGiaiDoanDuAn");
                                    int c = rowCDT;
                                    l_GDDAs.Add(rowCDT);
                                    sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1;
                                    sheet.Cells[rowCDT, 2].Value = "Dự án " + TenGiaiDoanDuAn;
                                    sheet.Row(rowCDT).Style.Font.Bold = true;
                                    sheet.Row(rowCDT).Style.Font.Italic = true;
                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                    rowCDT++;

                                    List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oGDDT, "grpTTDA");
                                    List<int> l_TTDAs = new List<int>();
                                    for (int temp = 128; temp < 132; temp++)
                                    {
                                        sheet.Cells[rowCDT, temp].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        sheet.Cells[rowCDT, temp].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        sheet.Cells[rowCDT, temp].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    }
                                    foreach (object oTTDA in lTTDA)
                                    {
                                        int x = rowCDT;
                                        l_TTDAs.Add(rowCDT);
                                        string TenTinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");

                                        sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1 + "." + iSTTcap2;
                                        sheet.Cells[rowCDT, 2].Value = "Dự án " + TenTinhTrangDuAn;
                                        sheet.Row(rowCDT).Style.Font.Bold = true;
                                        sheet.Row(rowCDT).Style.Font.Italic = true;
                                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                        rowCDT++;

                                        List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");

                                        int d = 0;
                                        List<int> l_GDLs = new List<int>();
                                        foreach (object oDACha in lDACha)
                                        {
                                            d++;
                                            XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBo,
                                                    dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT, /*dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                    dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT, /*dic_CBDT_KK, dic_CBDT_GP, dic_CBDT_TDC,*/
                                                    dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                    dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDTKTC, lstDLCNT, lstDanhSo_CTHLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                    dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, /*dic_QT_KKGP, dic_QT_TDC,*/ lstTDC_QT, lstKKGP_QT,
                                                    Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                    dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);

                                            l_GDLs.Add(rowCDT);
                                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                            rowCDT++;

                                            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");

                                            if (dicDAConCbiDTu.ContainsKey(IDDA))
                                            {

                                                List<object> lDuAnCon = dicDAConCbiDTu[IDDA];

                                                foreach (object oDuAnCon in lDuAnCon)
                                                {
                                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);

                                                    XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBo,
                                                        dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT, /*dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                        dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDTKTC, lstDLCNT, lstDanhSo_CTHLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                        dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, lstTDC_QT, lstKKGP_QT, /* dic_QT_KKGP, dic_QT_TDC,*/
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                        dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);
                                                    rowCDT++;
                                                }
                                            }

                                        }
                                        SumRow(sheet, x, l_GDLs, az);
                                        Ty_Le_Giai_Ngan(sheet, x);
                                        iSTTcap2++;
                                    }
                                    iSTTcap2 = 1;
                                    SumRow(sheet, c, l_TTDAs, az);
                                    Ty_Le_Giai_Ngan(sheet, c);
                                    iSTTcap1++;
                                }
                                iSTTcap1 = 1;
                                SumRow(sheet, b, l_GDDAs, az);
                                Ty_Le_Giai_Ngan(sheet, b);
                                iSoLaMa++;
                            }
                            SumRow(sheet, a, l_LVNNs, az);
                            Ty_Le_Giai_Ngan(sheet, a);
                            iColumn++;
                        }
                        iSTTcap1 = 1;
                        iSTTcap2 = 1;

                        if (lstTTDATD.Count != 0)
                        {
                            foreach (object oTTDATD in lstTTDATD)
                            {
                                l_TongCongs.Add(rowCDT);
                                int m = rowCDT;
                                string GiaiDoanDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDATD, "GiaiDoanDuAn");
                                sheet.Cells[rowCDT, 1].Value = Columns[iColumn];
                                sheet.Cells[rowCDT, 2].Value = GiaiDoanDuAn + " năm " + namPD;
                                sheet.Row(rowCDT).Style.Font.Bold = true;
                                iColumn++;
                                iSoLaMa = 1;

                                Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                rowCDT++;

                                List<object> lLVNN = (List<object>)BaoCaoDungChung.GetValueObject(oTTDATD, "grpLVNN");
                                List<int> lstInt_LVNN = new List<int>();

                                foreach (object oLVNN in lLVNN)
                                {
                                    string TenLinhVucNganhNghe = (string)BaoCaoDungChung.GetValueObject(oLVNN, "LinhVucNganhNghe");

                                    sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa);
                                    sheet.Cells[rowCDT, 2].Value = TenLinhVucNganhNghe;
                                    int n = rowCDT;
                                    lstInt_LVNN.Add(rowCDT);
                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                    sheet.Row(rowCDT).Style.Font.Bold = true;
                                    sheet.Row(rowCDT).Style.Font.Italic = true;
                                    rowCDT++;

                                    List<object> lTTDA = (List<object>)BaoCaoDungChung.GetValueObject(oLVNN, "grpTTDA");
                                    List<int> lstInt_TTDA = new List<int>();

                                    foreach (object oTTDA in lTTDA)
                                    {

                                        string TenTinhTrangDuAn = (string)BaoCaoDungChung.GetValueObject(oTTDA, "TinhTrangDuAn");
                                        lstInt_TTDA.Add(rowCDT);
                                        int o = rowCDT;
                                        sheet.Cells[rowCDT, 1].Value = BaoCaoDungChung.ToRoman(iSoLaMa) + "." + iSTTcap1;
                                        sheet.Cells[rowCDT, 2].Value = "Dự án " + TenTinhTrangDuAn;
                                        sheet.Row(rowCDT).Style.Font.Bold = true;
                                        sheet.Row(rowCDT).Style.Font.Italic = true;
                                        Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                        rowCDT++;

                                        List<object> lDACha = (List<object>)BaoCaoDungChung.GetValueObject(oTTDA, "grpDuAn");
                                        List<int> l_DAChas = new List<int>();

                                        int d = 0;

                                        foreach (object oDACha in lDACha)
                                        {
                                            d++;
                                            XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDACha, d.ToString(), rowCDT, lstPhong_CanBo,
                                                    dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT, /*dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                    dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT, /*dic_CBDT_KK, dic_CBDT_GP, dic_CBDT_TDC,*/
                                                    dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                    dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDTKTC, lstDLCNT, lstDanhSo_CTHLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                    dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, lstTDC_QT, lstKKGP_QT,/* dic_QT_KKGP, dic_QT_TDC,*/
                                                    Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                    dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);
                                            l_DAChas.Add(rowCDT);
                                            Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                            rowCDT++;

                                            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDACha, "IDDA");

                                            if (dicDAConCbiDTu.ContainsKey(IDDA))
                                            {
                                                List<object> lDuAnCon = dicDAConCbiDTu[IDDA];

                                                foreach (object oDuAnCon in lDuAnCon)
                                                {
                                                    Format(sheet, rowCDT, ar_So, ar_Text, ar_Tien, ar_PhanTram);
                                                    XuatDuAnXDCB(sheet, iNamPD, szDonViTinh, oDuAnCon, "-", rowCDT, lstPhong_CanBo,
                                                        dic_CTDT_TGHTCT, dic_CTDT_NGNV, dic_CTDT_CTDD, dic_CTDT_LCTDT, dic_CTDT_NTHSPD, dic_CTDT_PDCTDT, lstTDC_CTDT, lstKKGP_CTDT, /*dic_CTDT_KK, dic_CTDT_GP, dic_CTDT_TDC,*/
                                                        dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS, lstTDC_CBDT, lstKKGP_CBDT, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS, lstTDC_CBTH, lstKKGP_CBTH, /*dic_CBTH_KK, dic_CBTH_GP, dic_CBTH_TDC,*/
                                                        dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB, lstTDC_TH, lstKKGP_TH, lstDTKTC, lstDLCNT, lstDanhSo_CTHLCNT, lstHTTC, /*dic_TH_KK, dic_TH_GP, dic_TH_TDC,*/
                                                        dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT, lstTDC_QT, lstKKGP_QT,/* dic_QT_KKGP, dic_QT_TDC,*/
                                                        Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN, dic_DA_KHV_DXDC,
                                                        dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB);
                                                    rowCDT++;
                                                }
                                            }
                                        }
                                        SumRow(sheet, o, l_DAChas, az);
                                        Ty_Le_Giai_Ngan(sheet, o);
                                        iSTTcap1++;
                                    }
                                    SumRow(sheet, n, lstInt_TTDA, az);
                                    Ty_Le_Giai_Ngan(sheet, n);
                                    iSTTcap1 = 1;
                                    iSoLaMa++;
                                }
                                SumRow(sheet, m, lstInt_LVNN, az);
                                Ty_Le_Giai_Ngan(sheet, m);
                                iSoLaMa = 0;
                            }
                            iColumn++;

                        }

                        SumRow(sheet, iBD, l_TongCongs, az);
                        Ty_Le_Giai_Ngan(sheet, iBD);

                        Style(sheet, rowCDT - 1, "A10:CF");
                        Style(sheet, rowCDT - 1, "CH10:DS");
                        sheet.Cells["B10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells["DG6"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells["DS7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        
                        sheet.Column(125).Style.WrapText = true;
                        sheet.Column(126).Style.WrapText = true;
                        Style(sheet, rowCDT - 1, "DT10:DU");
                        int[] ar_an = new int[] { 85, 123, 126, 127 };
                        foreach (var item in ar_an)
                        {
                            sheet.Column(item).Hidden = true;
                        }

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        az = new string[] { "A2", "A3", "Y6", "BA7", "BQ7", "CQ7", "L8", "M8", "CG8", "DL8", "DP8", "DQ8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{nam}}", namPD);
                        }

                        az = new string[] { "DP5", "V8", "AD8", "AF8", "AP8", "BE8", "BG8", "BJ8", "CD8", "CV8", "CW8", "CX8", "CY8", "DC8", "DF8", "DI8", "DP8", "DQ8", "DR8" };
                        foreach (var item in az)
                        {
                            sheet.Cells[item].Value = sheet.Cells[item].Value.ToString().Replace("{{donvitinh}}", szDonViTinh);
                        }

                        int[] ar_i = new int[] { 2 };
                        DoRongCot(sheet, ar_i, 25);

                        ar_i = new int[] { 7, 9, 10, 11 };
                        DoRongCot(sheet, ar_i, 20);

                        ar_i = new int[] { 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 37, 38, 39, 41, 42, 45, 46, 47, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58, 59, 61, 62, 64, 65, 66, 67, 68, 70, 71, 73, 75, 76, 77, 78, 80, 81, 82, 84, 86, 87, 88, 89, 90, 91, 92, 93, 94, 96, 97, 99, 100, 101, 102, 103, 107, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 120, 121, 122 };
                        DoRongCot(sheet, ar_i, 15);
                        sheet.Column(8).Width = 60;

                        ar_i = new int[] { 1, 3, 4, 5, 6, 12, 13, 15, 25, 40, 43, 53, 60, 63, 69, 72, 74, 79, 83, 95, 98, 106, 104, 105, 108 };
                        DoRongCot(sheet, ar_i, 5);
                        sheet.Column(1).Width = 7;

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BAOCAOTIENDOKHXDCB", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #region Style
        public void Style(ExcelWorksheet sheet, int row, string sz)
        {
            using (ExcelRange rng = sheet.Cells[sz + row])
            {
                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rng.Style.WrapText = true;
                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rng.Style.Font.Name = "Times New Roman";
                rng.Style.Font.Size = 11;
                rng.AutoFitColumns();
            }
        }

        public void Ty_Le_Giai_Ngan(ExcelWorksheet sheet, int row)
        {
            sheet.Cells[row, 118].Formula = "=(DM" + row + "/DL" + row + ")";
            int[] ar_i = new int[] { 120,121 };
            string[] ar_sz = new string[] { "DL", "DS" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, row, ar_i, ar_sz);
        }

        public void Format(ExcelWorksheet sheet, int row, int[] ar_So, int[] ar_Text, int[] ar_Tien, int[] ar_PhanTram)
        {
            BaoCaoDungChung.In_Style(sheet, row, ar_So, 1);
            BaoCaoDungChung.In_Style(sheet, row, ar_Text, 2);
            BaoCaoDungChung.In_Style(sheet, row, ar_Tien, 3);
            BaoCaoDungChung.In_Style(sheet, row, ar_PhanTram, 4);
        }

        public void DoRongCot(ExcelWorksheet sheet, int[] ar_So, int dorong)
        {
            foreach (var item in ar_So)
            {
                sheet.Column(item).Width = dorong;
            }
        }
        #endregion

        private void XuatDuAnXDCB(ExcelWorksheet sheet, int iNam, string szDonViTinh, object oDA, string d, int rowDA,
            IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> lstPhong_CanBo,

            Dictionary<int, int> dic_CTDT_TGHTCT,
            Dictionary<int, List<object>> dic_CTDT_NGNV,
            Dictionary<int, List<object>> dic_CTDT_CTDD,
            Dictionary<int, List<object>> dic_CTDT_LCTDT,
            Dictionary<int, List<object>> dic_CTDT_NTHSPD,
            Dictionary<int, List<object>> dic_CTDT_PDCTDT,
            //Dictionary<int, string> dic_CTDT_KK,
            //Dictionary<int, string> dic_CTDT_GP,
            //Dictionary<int, List<object>> dic_CTDT_TDC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CTDT,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CTDT,

            //dic_CBDT_BCPATK, dic_CBDT_PDT_CBDT, dic_CBDT_PKHKCNT, dic_CBDT_KSHT_CGDD_HTKT, dic_CBDT_PDQH, dic_CBDT_KSDC, dic_CBDT_HTPDDA, dic_CBDT_TTDPDDA, dic_CBDT_PCCC_TTK, dic_CBDT_DLHS
            Dictionary<int, List<object>> dic_CBDT_BCPATK,
            Dictionary<int, List<object>> dic_CBDT_PDT_CBDT,
            Dictionary<int, List<object>> dic_CBDT_PKHKCNT,
            Dictionary<int, List<object>> dic_CBDT_KSHT_CGDD_HTKT,
            Dictionary<int, List<object>> dic_CBDT_PDQH,
            Dictionary<int, List<object>> dic_CBDT_KSDC,
            Dictionary<int, List<object>> dic_CBDT_HTPDDA,
            Dictionary<int, List<object>> dic_CBDT_TTDPDDA,
            Dictionary<int, List<object>> dic_CBDT_PCCC_TTK,
            Dictionary<int, List<object>> dic_CBDT_DLHS,
            //Dictionary<int, List<object>> dic_CBDT_KKGP,
            //Dictionary<int, string> dic_CBDT_KK,
            //Dictionary<int, string> dic_CBDT_GP,
            //Dictionary<int, List<object>> dic_CBDT_TDC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBDT,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBDT,

            //,dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS
            Dictionary<int, List<object>> dic_CBTH_PDT,
            Dictionary<int, List<object>> dic_CBTH_PKHLCNT,
            Dictionary<int, List<object>> dic_CBTH_HTPD_THBV,
            Dictionary<int, List<object>> dic_CBTH_TTD_TKBV,
            Dictionary<int, List<object>> dic_CBTH_DLHS,
            //Dictionary<int, List<object>> dic_CBTH_KKGP,
            //Dictionary<int, string> dic_CBTH_KK,
            //Dictionary<int, string> dic_CBTH_GP,
            //Dictionary<int, List<object>> dic_CBTH_TDC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_CBTH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_CBTH,

            //dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB
            Dictionary<int, List<object>> dic_THDT_HTTC,
            Dictionary<int, List<object>> dic_THDT_DTCXD,
            Dictionary<int, List<object>> dic_THDT_DLCNT,
            Dictionary<int, List<object>> dic_THDT_CTHLCNT,
            Dictionary<int, List<object>> dic_THDT_GPMB,
            //Dictionary<int, List<object>> dic_THDT_KKGP,
            //Dictionary<int, string> dic_TH_KK,
            //Dictionary<int, string> dic_TH_GP,
            //Dictionary<int, List<object>> dic_TH_TDC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_TH,
            IEnumerable<BC_Uyban_PL1_KKGP> lstKKGP_TH,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDTKTC,
            IEnumerable<BC_Uyban_PL1_PDDA> lstDLCNT,
            IEnumerable<BC_Uyban_PL1_PDDA> lstCTHLCNT,
            IEnumerable<BC_Uyban_PL1_PDDA> lstHTTC,

            //dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT
            Dictionary<int, List<object>> dic_QT_HTQT,
            Dictionary<int, List<object>> dic_QT_HTHS_QLCL,
            Dictionary<int, List<object>> dic_QT_HTHS_HSQT,
            Dictionary<int, List<object>> dic_QT_HTHS_KTDL,
            Dictionary<int, List<object>> dic_QT_HTHS_TTQT,
            //Dictionary<int, string> dic_QT_KKGP,
            //Dictionary<int, List<object>> dic_QT_TDC,
            IEnumerable<BC_Uyban_PL1_KHTDC> lstTDC_QT,
            IEnumerable<BC_TienDo_KK_QT> lstKKGP_QT,

            ////Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN
            Dictionary<int, double> Dic_TD_DA_CBDT_KHV,
            Dictionary<int, List<object>> Dic_TD_DA_CBDT_GN,
            Dictionary<int, double> dic_DA_KHV_DXDC,

            //dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB
            Dictionary<int, string> dic_TDTH_LCTDT,
            Dictionary<int, string> dic_TDTH_CBDT,
            Dictionary<int, string> dic_TDTH_CBTH,
            Dictionary<int, string> dic_TDTH_TH_DTCXD,
            Dictionary<int, string> dic_TDTH_TH_GPMB
            )
        {
            int dDim = BaoCaoDungChung.GetdDim(szDonViTinh);
            int dDimKLNT = BaoCaoDungChung.GetdDimKLNT(szDonViTinh);

            double dGiaTri = 0;
            bool bol = true;
            string sz1 = "";
            string sz2 = "";
            string sz3 = "";
            string sz4 = "";
            string sz5 = "";
            int i;
            int[] ar_i;
            string[] ar_Sz1;
            string[] ar_Sz2;
            List<object> lst = new List<object>();
            string[] szAr = { "str1", "str2", "str3" };

            int IDDA = (int)BaoCaoDungChung.GetValueObject(oDA, "IDDA");
            string TenDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "TenDuAn");
            string NhomDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "NhomDuAn");
            string DiaDiemXayDung = (string)BaoCaoDungChung.GetValueObject(oDA, "DiaDiemXayDung");
            string NangLucThietKe = (string)BaoCaoDungChung.GetValueObject(oDA, "NangLucThietKe");
            string ThoiGianThucHien = (string)BaoCaoDungChung.GetValueObject(oDA, "ThoiGianThucHien");
            string MaSoDuAn = (string)BaoCaoDungChung.GetValueObject(oDA, "MaSoDuAn");
            string TenChuDauTu = (string)BaoCaoDungChung.GetValueObject(oDA, "TenChuDauTu");

            sheet.Cells[rowDA, 1].Value = d;
            sheet.Cells[rowDA, 2].Value = TenDuAn;

            ar_i = new int[] { 4, 5, 6 };
            BaoCaoDungChung.In_Nhom_Du_An(sheet, rowDA, NhomDuAn, ar_i);

            //sheet.Cells[rowDA, 18].Formula = "Sum(" + sheet.Cells[rowDA, 19] + ":" + sheet.Cells[rowDA, 20] + ")";

            sheet.Cells[rowDA, 3].Formula = "Sum(" + sheet.Cells[rowDA, 4] + ":" + sheet.Cells[rowDA, 6] + ")";

            ar_i = new int[] { 7, 8, 9, 10, 11 };
            ar_Sz1 = new string[] { DiaDiemXayDung, NangLucThietKe, ThoiGianThucHien, MaSoDuAn, TenChuDauTu };
            BaoCaoDungChung.In_Chuoi_Thong_Tin_Du_An(sheet, rowDA, ar_i, ar_Sz1);


            sheet.Cells[rowDA, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

            #region Chủ trương đầu tư
            if (dic_CTDT_TGHTCT.ContainsKey(IDDA))
            {
                i = dic_CTDT_TGHTCT[IDDA];
                if (i > iNam)
                {
                    sheet.Cells[rowDA, 13].Value = 1;
                }
                else
                {
                    sheet.Cells[rowDA, 12].Value = 1;
                }
            }

            if (dic_CTDT_NGNV.ContainsKey(IDDA))
            {
                lst = dic_CTDT_NGNV[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    sz1 = BaoCaoDungChung.GetDateValue(sz1);
                    sheet.Cells[rowDA, 14].Value = sz1;
                }
            }

            if (dic_CTDT_CTDD.ContainsKey(IDDA))
            {
                lst = dic_CTDT_CTDD[IDDA];
                foreach (object obj in lst)
                {
                    bol = (bool)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str3");

                    BaoCaoDungChung.In_Bool_value(sheet, rowDA, 15, bol);
                    sz2 = BaoCaoDungChung.GetDateValue(sz2);
                    sheet.Cells[rowDA, 16].Value = BaoCaoDungChung.So_Ngay(sz1, sz2);
                }
            }

            if (dic_CTDT_LCTDT.ContainsKey(IDDA))
            {
                lst = dic_CTDT_LCTDT[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");

                    sheet.Cells[rowDA, 18].Value = sz1;
                    sheet.Cells[rowDA, 19].Value = sz2;
                }
            }

            if (dic_CTDT_NTHSPD.ContainsKey(IDDA))
            {
                lst = dic_CTDT_NTHSPD[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    if (sz1 != "")
                    {
                        sz1 = BaoCaoDungChung.GetDateValue(sz1);
                    }
                    sheet.Cells[rowDA, 20].Value = sz1;
                }
            }

            if (dic_CTDT_PDCTDT.ContainsKey(IDDA))
            {
                lst = dic_CTDT_PDCTDT[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz2 = BaoCaoDungChung.GetDateValue(sz2);
                    dGiaTri = (double)BaoCaoDungChung.GetValueObject(obj, "str3") / dDim;
                    sz4 = (string)BaoCaoDungChung.GetValueObject(obj, "str4");
                    sz4 = BaoCaoDungChung.GetDateValue(sz4);

                    sheet.Cells[rowDA, 21].Value = BaoCaoDungChung.So_Ngay(sz1, sz2);
                    sheet.Cells[rowDA, 22].Value = dGiaTri;
                    sheet.Cells[rowDA, 23].Value = sz4;
                }
            }
            //dic_CTDT_KK
            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CTDT, IDDA, sz1, 24);

            ar_i = new int[] { 25, 26, 27 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CTDT, IDDA, sz1, sz2, ar_i);
            //ar_i = new int[] { 24 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_CTDT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            //ar_i = new int[] { 25, 26 };
            //BaoCaoDungChung.In_Dic_String(dic_CTDT_KK, IDDA, sheet, rowDA, ar_i, sz1, 1);
            //ar_i = new int[] { 0, 27 };
            //BaoCaoDungChung.In_Dic_String(dic_CTDT_GP, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion

            #region Chuẩn bị đầu tư
            if (dic_CBDT_BCPATK.ContainsKey(IDDA))
            {
                lst = dic_CBDT_BCPATK[IDDA];
                foreach (object obj in lst)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(obj, "str");
                    sheet.Cells[rowDA, 28].Value = sz1;
                }
            }

            ar_i = new int[] { 0, 29, 30 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_PDT_CBDT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 0, 31, 32 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_PKHKCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 33, 34, 35 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_KSHT_CGDD_HTKT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            if (dic_CBDT_PDQH.ContainsKey(IDDA))
            {
                lst = dic_CBDT_PDQH[IDDA];
                foreach (object obj in lst)
                {
                    bol = (bool)BaoCaoDungChung.GetValueObject(obj, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(obj, "str2");
                    sz3 = (string)BaoCaoDungChung.GetValueObject(obj, "str3");
                    sz4 = (string)BaoCaoDungChung.GetValueObject(obj, "str4");
                    sz5 = (string)BaoCaoDungChung.GetValueObject(obj, "str5");

                    if (sz3 != "")
                    {
                        sz3 = BaoCaoDungChung.GetDateValue(sz3);
                    }

                    if (sz5 != "")
                    {
                        sz5 = BaoCaoDungChung.GetDateValue(sz5);
                    }

                    BaoCaoDungChung.In_Bool_value(sheet, rowDA, 36, bol);
                    sheet.Cells[rowDA, 37].Value = BaoCaoDungChung.So_Ngay(sz2, sz3);
                    sheet.Cells[rowDA, 38].Value = BaoCaoDungChung.So_Ngay(sz4, sz5);
                }
            }

            ar_i = new int[] { 39 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_KSDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            
            ar_i = new int[] { 43, 44 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBDT_TTDPDDA, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);
            
            ar_i = new int[] { 40, 41, 42 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBDT_HTPDDA, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);
            if(Convert.ToInt32(sheet.Cells[rowDA, 40].Value) == 1)
            {
                sheet.Cells[rowDA, 43].Value = 0;
            }

            ar_i = new int[] { 45, 46 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str1", "str2" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_PCCC_TTK, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 47 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 0, 48 };
            BaoCaoDungChung.In_Date_Value(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, sz1, "str3");

            ar_i = new int[] { 50, 51 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str4", "str5" };
            BaoCaoDungChung.In_Chuoi(dic_CBDT_DLHS, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CBDT, IDDA, sz1, 52);
            ar_i = new int[] { 53, 54, 55 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CBDT, IDDA, sz1, sz2, ar_i);

            //ar_i = new int[] { 52 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_CBDT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //ar_i = new int[] { 53, 54 };
            //BaoCaoDungChung.In_Dic_String(dic_CBDT_KK, IDDA, sheet, rowDA, ar_i, sz1, 1);
            //ar_i = new int[] { 0, 55 };
            //BaoCaoDungChung.In_Dic_String(dic_CBDT_GP, IDDA, sheet, rowDA, ar_i, sz1, 0);

            #endregion

            #region Chuẩn bị thực hiện
            //,dic_CBTH_PDT, dic_CBTH_PKHLCNT, dic_CBTH_HTPD_THBV, dic_CBTH_TTD_TKBV, dic_CBTH_DLHS
            ar_i = new int[] { 0, 56, 57 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_PDT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 0, 58, 59 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_PKHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            ar_i = new int[] { 63, 64 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_CBTH_TTD_TKBV, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            ar_i = new int[] { 60, 61, 62 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_CBTH_HTPD_THBV, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);
            if(Convert.ToInt32(sheet.Cells[rowDA, 60].Value) == 1)
            {
                sheet.Cells[rowDA, 63].Value = 0;
            }

            ar_i = new int[] { 66, 67 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str1", "str2" };
            BaoCaoDungChung.In_Chuoi(dic_CBTH_DLHS, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_CBTH, IDDA, sz1, 68);
            ar_i = new int[] { 69, 70, 71 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_CBTH, IDDA, sz1, sz2, ar_i);

            //ar_i = new int[] { 68 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_CBTH_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //ar_i = new int[] { 69, 70 };
            //BaoCaoDungChung.In_Dic_String(dic_CBTH_KK, IDDA, sheet, rowDA, ar_i, sz1, 1);
            //ar_i = new int[] { 0, 71 };
            //BaoCaoDungChung.In_Dic_String(dic_CBTH_GP, IDDA, sheet, rowDA, ar_i, sz1, 0);

            #endregion

            #region Thực hiện
            //dic_THDT_HTTC, dic_THDT_DTCXD, dic_THDT_DLCNT, dic_THDT_CTHLCNT, dic_THDT_GPMB
            ar_i = new int[] { 0, 75 };
            BaoCaoDungChung.In_Date_Value(dic_THDT_DTCXD, IDDA, lst, sheet, rowDA, ar_i, sz1, "str1");

            if(lstDTKTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 74].Value = 1;
            }

            ar_i = new int[] { 76, 78 };
            ar_Sz1 = new string[] { sz1, sz2 };
            ar_Sz2 = new string[] { "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_DTCXD, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 81 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_THDT_DLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);

            if (lstDLCNT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 79].Value = 1;
            }

            ar_i = new int[] { 0, 82 };
            BaoCaoDungChung.In_GiaTri_Value(dic_THDT_DLCNT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str3", dDim);

            if(lstCTHLCNT.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 83].Value = 1;
            }

            ar_i = new int[] { 0, 84 };
            BaoCaoDungChung.In_So_Ngay_Value(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2);


            ar_i = new int[] { 86 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);
            //BaoCaoDungChung.In_Date_Value(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, sz1, "str3");
            
            ar_i = new int[] { 87, 88, 89 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str4", "str5", "str6" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_CTHLCNT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 91, 92, 93 };
            ar_Sz1 = new string[] { sz1, sz2, sz3 };
            ar_Sz2 = new string[] { "str1", "str2", "str3" };
            BaoCaoDungChung.In_Chuoi(dic_THDT_GPMB, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 73 };
            BaoCaoDungChung.In_Date_Value(dic_THDT_HTTC, IDDA, lst, sheet, rowDA, ar_i, sz1, "str");

            if(lstHTTC.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sheet.Cells[rowDA, 72].Value = 1;

                ar_i = new int[] { 74, 79, 83 };
                foreach (var item in ar_i)
                {
                    sheet.Cells[rowDA, item].Value = 0;
                }
            }

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_TH, IDDA, sz1, 94);
            ar_i = new int[] { 95, 96, 97 };
            BaoCaoDungChung.In_KKGP(sheet, rowDA, lstKKGP_TH, IDDA, sz1, sz2, ar_i);
            

            #endregion

            #region Quyết toán dự án
            //dic_QT_HTQT, dic_QT_HTHS_QLCL, dic_QT_HTHS_HSQT, dic_QT_HTHS_KTDL, dic_QT_HTHS_TTQT
            //dic_QT_HTQT
            ar_i = new int[] { 98, 99, 100 };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);
            ar_i = new int[] { 0, 101 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str4", dDim);
            ar_i = new int[] { 0, 102 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str5", dDim);
            ar_i = new int[] { 0, 103 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str6", dDim);

            //dic_QT_HTHS_QLCL
            ar_i = new int[] { 104, 105 };
            BaoCaoDungChung.In_Bool_Tong_value(dic_QT_HTHS_QLCL, IDDA, lst, sheet, rowDA, ar_i, bol, "str");

            //dic_QT_HTHS_HSQT
            ar_i = new int[] { 106, 107 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTHS_HSQT, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str", dDim);

            //dic_QT_HTHS_KTDL
            ar_i = new int[] { 0, 108 };
            BaoCaoDungChung.In_Bool_Tong_value(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, bol, "str1");

            ar_i = new int[] { 109 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str2" };
            BaoCaoDungChung.In_Chuoi(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 110 };
            BaoCaoDungChung.In_GiaTri_Value(dic_QT_HTHS_KTDL, IDDA, lst, sheet, rowDA, ar_i, dGiaTri, "str3", dDim);

            //dic_QT_HTHS_TTQT
            ar_i = new int[] { 111 };
            ar_Sz1 = new string[] { sz1 };
            ar_Sz2 = new string[] { "str1" };
            BaoCaoDungChung.In_Chuoi(dic_QT_HTHS_TTQT, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            ar_i = new int[] { 0, 112, 113 };
            szAr = new string[] { "str2", "str3", "str4" };
            BaoCaoDungChung.In_SoNgayPD_TGT(dic_QT_HTHS_TTQT, IDDA, lst, sheet, rowDA, ar_i, sz1, sz2, szAr, dGiaTri, dDim);

            BaoCaoDungChung.In_TDC(sheet, rowDA, lstTDC_QT, IDDA, sz1, 114);
            BaoCaoDungChung.In_KKGP_QT(sheet, rowDA, lstKKGP_QT, IDDA, sz1, 115);
            //ar_i = new int[] { 114 };
            //ar_Sz1 = new string[] { sz1 };
            //ar_Sz2 = new string[] { "str" };
            //BaoCaoDungChung.In_Chuoi(dic_QT_TDC, IDDA, lst, sheet, rowDA, ar_i, ar_Sz1, ar_Sz2, 0);

            //ar_i = new int[] { 0, 115 };
            //BaoCaoDungChung.In_Dic_String(dic_QT_KKGP, IDDA, sheet, rowDA, ar_i, sz1, 0);

            #endregion

            #region Kế hoạch vốn
            //Dic_TD_DA_CBDT_KHV, Dic_TD_DA_CBDT_GN
            //dic_KHV_BD
            double dGiaTri1;
            double dGiaTri2;
            if (Dic_TD_DA_CBDT_KHV.ContainsKey(IDDA))
            {
                dGiaTri = Dic_TD_DA_CBDT_KHV[IDDA] / dDim;
                sheet.Cells[rowDA, 116].Value = dGiaTri;
            }
            else
            {
                sheet.Cells[rowDA, 116].Value = 0;
            }
            dGiaTri1 = dGiaTri;

            if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            {
                List<object> lst1 = Dic_TD_DA_CBDT_GN[IDDA];
                foreach (var item in lst1)
                {
                    sz1 = (string)BaoCaoDungChung.GetValueObject(item, "str1");
                    sz2 = (string)BaoCaoDungChung.GetValueObject(item, "str2");

                    sheet.Cells[rowDA, 117].Value = (Convert.ToDouble(sz1) + Convert.ToDouble(sz2)) / dDimKLNT;
                }
            }
            //if (Dic_TD_DA_CBDT_GN.ContainsKey(IDDA))
            //{
            //    dGiaTri2 = Dic_TD_DA_CBDT_GN[IDDA] / dDim;
            //    sheet.Cells[rowDA, 117].Value = dGiaTri2;
            //}
            //else
            //{
            //    sheet.Cells[rowDA, 117].Value = 0;
            //}



            sheet.Cells[rowDA, 118].Formula = "=(DM" + rowDA + "/DL" + rowDA + ")";

            if (dic_DA_KHV_DXDC.ContainsKey(IDDA))
            {
                dGiaTri = dic_DA_KHV_DXDC[IDDA] / dDim;
                sheet.Cells[rowDA, 122].Value = dGiaTri;
            }
            else
            {
                dGiaTri = 0;
            }
            dGiaTri2 = dGiaTri;


            ar_i = new int[] { 120,121  };
            ar_Sz1 = new string[] { "DL", "DS" };
            BaoCaoDungChung.Tang_Giam_KHV(sheet, rowDA, ar_i, ar_Sz1);

            #endregion
            try
            {
                string szTMDT = BaoCaoDungChung.GetValueObject(oDA, "TMDT")?.ToString() ?? "";
                string TMDT_ChuaPD = BaoCaoDungChung.GetValueObject(oDA, "TMDT_ChuaPD")?.ToString() ?? "";
                string PDDA_BCKTKT_SoQD = (string)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_SoQD");
                if (BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD") != null)
                {
                    DateTime PDDA_BCKTKT_NgayPD = (DateTime)BaoCaoDungChung.GetValueObject(oDA, "PDDA_BCKTKT_NgayPD");
                    sheet.Cells[rowDA, 130].Value = PDDA_BCKTKT_NgayPD.ToString("D", CultureInfo.CreateSpecificCulture("vi-VN"));
                }
                sheet.Cells[rowDA, 128].Value = !string.IsNullOrEmpty(TMDT_ChuaPD) ? Convert.ToDouble(TMDT_ChuaPD) : 0;
                sheet.Cells[rowDA, 129].Value = PDDA_BCKTKT_SoQD;
                sheet.Cells[rowDA, 131].Value = !string.IsNullOrEmpty(szTMDT) ? Convert.ToDouble(szTMDT) : 0;
            }
            catch (Exception e) { Console.WriteLine(e); }
            for (int temp = 128; temp < 132; temp++)
            {
                sheet.Cells[rowDA, temp].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                sheet.Cells[rowDA, temp].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                sheet.Cells[rowDA, temp].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            }
            
            #region Tiến độ thực hiện
            //dic_TDTH_LCTDT, dic_TDTH_CBDT, dic_TDTH_CBTH, dic_TDTH_TH_DTCXD, dic_TDTH_TH_GPMB
            #region Chủ trương đầu tư
            ar_i = new int[] { 0, 17 };
            BaoCaoDungChung.In_Dic_String(dic_TDTH_LCTDT, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion
            #region Chuẩn bị đầu tư
            ar_i = new int[] { 0, 49 };
            BaoCaoDungChung.In_Dic_String(dic_TDTH_CBDT, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion
            #region Chuẩn bị thực hiện
            ar_i = new int[] { 0, 65 };
            BaoCaoDungChung.In_Dic_String(dic_TDTH_CBTH, IDDA, sheet, rowDA, ar_i, sz1, 0);
            #endregion
            #region Thực hiện
            //Đang thi công xây dựng
            ar_i = new int[] { 0, 77 };
            BaoCaoDungChung.In_Dic_String(dic_TDTH_TH_DTCXD, IDDA, sheet, rowDA, ar_i, sz1, 0);
            //Giải phóng mặt bằng
            ar_i = new int[] { 0, 90 };
            BaoCaoDungChung.In_Dic_String(dic_TDTH_TH_GPMB, IDDA, sheet, rowDA, ar_i, sz1,0);
            #endregion
            #endregion

            if(lstPhong_CanBo.Where(x => x.IDDA == IDDA).Count() > 0)
            {
                sz1 = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.TenPhong).FirstOrDefault();
                sheet.Cells[rowDA, 124].Value = sz1;

                sz2 = "";
                var lstUser = lstPhong_CanBo.Where(x => x.IDDA == IDDA).Select(x => x.Users).FirstOrDefault();
                foreach (var item in lstUser)
                {
                    string sz = item.Name;
                    sz2 += sz + ",";
                }
                if (sz2.Length > 1)
                {
                    sz2 = sz2.Substring(0, sz2.Length - 1);
                }
                sheet.Cells[rowDA, 125].Value = sz2;
            }

            ar_i = new int[] { 1, 3, 4, 5, 6, 12, 13, 14, 15, 16, 20, 21, 23, 25, 29, 31, 36, 37, 38, 40, 41, 43, 44, 47, 48, 53, 56, 58, 60, 61, 63, 64, 69, 72, 73, 74, 75, 76, 79, 80, 81, 83, 84, 86, 87, 95, 98, 99, 104, 105, 106, 108, 112, 130 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 1);

            ar_i = new int[] { 2, 7, 8, 9, 10, 11, 17, 24, 26, 27, 28, 33, 34, 35, 39, 45, 46, 49, 50, 51, 52, 54, 55, 65, 66, 67, 68, 70, 71, 77, 78, 88, 89, 90, 91, 92, 93, 94, 96, 97, 109, 111, 129, 130 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 2);

            ar_i = new int[] { 22, 30, 32, 42, 57, 59, 62, 82, 100, 101, 102, 103, 107, 110, 113, 116, 117, 120, 121, 122, 123, 128, 131 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 3);

            ar_i = new int[] { 118 };
            BaoCaoDungChung.In_Style(sheet, rowDA, ar_i, 4);

        }

        #region Tính tổng
        public string GetSumString(string sz, List<int> lstInt)
        {
            string str = "";

            if (lstInt.Count != 0)
            {

                foreach (var row in lstInt)
                {
                    str += sz + row.ToString() + "+";
                }

                str = str.Substring(0, str.Length - 1);
            }
            return str;
        }

        public void SumRow(ExcelWorksheet sheet, int row, List<int> lstInt, string[] szAr)
        {
            foreach (var sz in szAr)
            {
                sheet.Cells[sz + row].Formula = "=SUM(" + GetSumString(sz, lstInt) + ")";
            }
        }
        #endregion

        


        
    }
}
