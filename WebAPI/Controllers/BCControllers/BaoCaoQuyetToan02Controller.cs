﻿using Service;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using OfficeOpenXml;
using WebAPI.Infrastructure.Core;

using Model.Models;
using Service.BCService;

using OfficeOpenXml.Style;
using System.Collections.Generic;

namespace WebAPI.Controllers.BCControllers
{
    [Authorize]
    [RoutePrefix("api/BaocaoQuyettoan02")]
    public class BaoCaoQuyetToan02Controller : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IVanBanDuAnService _vanBanDuAnService;
        private IBaoCaoTableService _baoCaoTableService;
        public BaoCaoQuyetToan02Controller(IErrorService errorService, IDuAnService duAnService, IVanBanDuAnService vanBanDuAnService, IBaoCaoTableService baoCaoTableService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._vanBanDuAnService = vanBanDuAnService;
            this._baoCaoTableService = baoCaoTableService;
        }
        #region Mau02
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("Mau02")]
        [HttpGet]
        public HttpResponseMessage ExportMau02(HttpRequestMessage request, int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string documentName = Mau02(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string Mau02(int iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //Template File
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PL-02-CacVanBanPhapLy.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BC_QuyetToanPhuLuc2-{0:yyyyMMddhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Result Output
            //MemoryStream output = new MemoryStream();
            try
            {
                //Read Template
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        var duan = _duAnService.GetById(iIDDuAn);
                        sheet.Cells["A3"].Value = duan.TenDuAn;
                        sheet.Cells["A3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        sheet.Cells["A3"].Style.WrapText = true;
                        //Lấy văn bản dự án
                        var vbda = _vanBanDuAnService.GetByIdDuAn(iIDDuAn);
                        int rowIndex = 6;
                        int index = 0;
                        foreach (var item in vbda)
                        {
                            rowIndex++;
                            index++;
                            sheet.InsertRow(rowIndex, 1);
                            sheet.Cells[rowIndex, 1].Value = index;
                            sheet.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet.Cells[rowIndex, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            sheet.Cells[rowIndex, 2].Value = item.TenVanBan;
                            sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                            sheet.Cells[rowIndex, 3].Value = item.SoVanBan + ", " + item.NgayBanHanh?.ToString("dd/MM/yyyy");
                            sheet.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            sheet.Cells[rowIndex, 4].Value = item.CoQuanBanHanh;
                            sheet.Cells[rowIndex, 6].Value = item.GhiChu;

                        }

                        sheet.Cells["F4"].Value = "Đơn vị tính: " + szDonViTinh;


                        pck.SaveAs(new FileInfo(fullPath));
                        _baoCaoTableService.AddBaoCao(documentName, "Mau 02/QTDA", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
