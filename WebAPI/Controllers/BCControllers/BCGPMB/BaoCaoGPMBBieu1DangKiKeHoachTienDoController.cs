﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.GPMBService;
using Service.GPMBService.GPMB_KeHoachTienDoService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCGPMB
{
    [Authorize]
    [RoutePrefix("api/BaocaoGPMB")]
    public class BaoCaoGPMBBieu1DangKiKeHoachTienDoController : ApiControllerBase
    {
        private IGPMB_CTCVService _GPMB_CTCVService;
        private IBaoCaoTableService _baoCaoTableService;
        public BaoCaoGPMBBieu1DangKiKeHoachTienDoController(IErrorService errorService,
            IGPMB_CTCVService GPMB_CTCVService,
            IBaoCaoTableService baoCaoTableService
            ) : base(errorService)
        {
            this._GPMB_CTCVService = GPMB_CTCVService;
            this._baoCaoTableService = baoCaoTableService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        //Xuất Excel báo cáo tỉnh phê duyệt

        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("BC_GPMB_DangKiKeHoachTienDo")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoUyBan_PL1(HttpRequestMessage request, string iIDDuAn)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCaoGPMB_TongHopKetQuaThucHien(iIDDuAn);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoGPMB_TongHopKetQuaThucHien(string szIDDuAn)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/BieuSo1GPMB_DangKiKeHoach.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoGPMB_DangKiKeHoachTienDoChiTiet" + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            //try
            //{
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        var lstData = _GPMB_CTCVService.GetBaoCaoDangKiKeHoachTienDoChiTiet(szIDDuAn);
                    
                        int rowIndex = 7;
                        int count = 0;

                        rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex);
                        int a = rowIndex + 3;

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        sheet.Column(1).Width = 7;
                        sheet.Column(2).Width = 30;
                        for (int i = 3; i <= 21; i++)
                        {
                            sheet.Column(i).Width = 18;
                        }
                        sheet.Column(22).Width = 23;

                        for(int i = 1; i<= 22; i++)
                        {
                            sheet.Column(i).Style.WrapText = true;
                            sheet.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        }

                        if (rowIndex > 7)
                        {
                            using (ExcelRange rng = sheet.Cells["A7:V" + (rowIndex - 1)])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                            }
                        }

                        //string[] arSz = { "B", "G" };
                        //string sz = "CÁC THÀNH VIÊN TCT ĐĂNG KÝ KẾ HOẠCH TIẾN ĐỘ";
                        //Dong_Duoi_Bang(sheet, a, sz, arSz);

                        //arSz = new string[] { "I", "M" };
                        //sz = "XÁC NHẬN CỦA TRƯỞNG PHÒNG PHỤ TRÁCH";
                        //Dong_Duoi_Bang(sheet, a, sz, arSz);

                        //arSz = new string[] { "P", "T" };
                        //sz = "PHÓ GIÁM ĐỐC PHỤ TRÁCH PHÊ DUYỆT";
                        //Dong_Duoi_Bang(sheet, a, sz, arSz);

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BC_GPMB_DKKHTDCT", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            //}
            //catch (Exception e)
            //{
            //    return string.Empty;
            //}
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<GPMB_KeHoachTienDo_BC> lstData, int count, int rowIndex)
        {
            foreach (var kq in lstData)
            {
                count++;

                sheet.Cells[rowIndex, 1].Value = count;
                sheet.Cells[rowIndex, 2].Value = kq.TenDuAn;

                sheet.Cells[rowIndex, 3].Value = GetString(new string[] { kq.CBTH_VanBanGiaoNhiemVu_SoVB, BaoCaoDungChung.GetDateValue(kq.CBTH_VanBanGiaoNhiemVu_NgayQD) });
                sheet.Cells[rowIndex, 4].Value = GetString(new string[] { BaoCaoDungChung.GetDateValue(kq.CBTH_BienBanBanGiaoMocGioi_NgayQD) });
                sheet.Cells[rowIndex, 5].Value = GetString(new string[] { kq.CBTH_QuyetDinhThanhLapHoiDong_SoVB, BaoCaoDungChung.GetDateValue(kq.CBTH_QuyetDinhThanhLapHoiDong_NgayQD), kq.CBTH_QuyetDinhTCT_SoVB, BaoCaoDungChung.GetDateValue(kq.CBTH_QuyetDinhTCT_NgayQD) });
                sheet.Cells[rowIndex, 6].Value = GetString(new string[] { kq.CBTH_QuyetDinhThanhLapHoiDong_SoVB, BaoCaoDungChung.GetDateValue(kq.CBTH_QuyetDinhThanhLapHoiDong_NgayQD)});
                sheet.Cells[rowIndex, 7].Value = GetString(new string[] { BaoCaoDungChung.GetDateValue(kq.CBTH_HopHoiDong_HHD), BaoCaoDungChung.GetDateValue(kq.CBTH_HopHoiDong_HDU) });
                sheet.Cells[rowIndex, 8].Value = GetString(new string[] { kq.CBTH_TrichDoHienTrang_Chon } );
                sheet.Cells[rowIndex, 9].Value = GetString(new string[] { kq.CBTH_ThongBaoThuHoiDat_SoVB } );

                sheet.Cells[rowIndex, 10].Value = GetString(new string[] { kq.TH_HopDan_Chon });
                sheet.Cells[rowIndex, 11].Value = GetString(new string[] { kq.TH_DieuTraKeKhai_SoVB });
                sheet.Cells[rowIndex, 12].Value = GetString(new string[] { kq.TH_CuongCheKiemDem_Chon });
                sheet.Cells[rowIndex, 13].Value = GetString(new string[] { kq.TH_LapHoSoGPMB_SoVB });
                sheet.Cells[rowIndex, 14].Value = GetString(new string[] { kq.TH_LapTrinhHoiDongThamTra_SoVB });
                sheet.Cells[rowIndex, 15].Value = GetString(new string[] { kq.TH_CongKhaiDuThao_SoVB});
                sheet.Cells[rowIndex, 16].Value = GetString(new string[] { kq.TH_TrinhHoiDongThamDinh_SoVB });
                sheet.Cells[rowIndex, 17].Value = GetString(new string[] { kq.TH_QuyetDinhThuHoiDat_SoVB, kq.TH_QuyetDinhThuHoiDat_GiaTri });
                sheet.Cells[rowIndex, 18].Value = GetString(new string[] { kq.TH_ChiTraTien_SoVB, kq.TH_ChiTraTien_GiaTri });
                sheet.Cells[rowIndex, 19].Value = GetString(new string[] { kq.TH_CuongCheThuHoiDat_Chon });
                sheet.Cells[rowIndex, 20].Value = GetString(new string[] { kq.TH_QuyetToanKinhPhi_SoVB, BaoCaoDungChung.GetDateValue(kq.TH_QuyetToanKinhPhi_NgayQD) , kq.TH_QuyetToanKinhPhi_GiaTri });
                sheet.Cells[rowIndex, 21].Value = GetString(new string[] { kq.TH_LapHoSoDeNghiTP_SoVB, BaoCaoDungChung.GetDateValue(kq.TH_LapHoSoDeNghiTP_NgayQD) });

                sheet.Cells[rowIndex, 22].Value = kq.GhiChu;

                for(int i = 3; i <= 21; i++)
                {
                    sheet.Cells[rowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[rowIndex, i].Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
                }
                sheet.Cells[rowIndex, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                sheet.Cells[rowIndex, 22].Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;

                rowIndex++;
            }
            return rowIndex;
        }

        private void In_Style(ExcelWorksheet sheet, int row, int col)
        {
            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet.Cells[row, col].Style.Numberformat.Format = "#,###.##";
        }

        private string GetString(string[] lst)
        {
            string sz = "";
            foreach (string s in lst)
            {
                if (s != "" && s != null)
                {
                    sz += s + "; ";
                }
            }
            if (sz != "")
                sz = sz.Substring(0, sz.Length - 2);
            return sz;
        }

        //private void Dong_Duoi_Bang(ExcelWorksheet sheet, int a, string sz, string[] arSz)
        //{
        //    sheet.Cells[arSz[0] + a + ":" + arSz[1] + a].Merge = true;
        //    sheet.Cells[arSz[0] + a].Value = sz;
        //    sheet.Cells[arSz[0] + a].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //    sheet.Cells[arSz[0] + a].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        //    sheet.Cells[arSz[0] + a].Style.Font.Bold = true;
        //}
    }
}
