﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.QLTD.GPMB;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.GPMBService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCGPMB
{
    [Authorize]
    [RoutePrefix("api/BaocaoGPMB")]
    public class BcDieuHanhGPMBTongHopKetQuaThucHienController : ApiControllerBase
    {
        IGPMB_KetQuaService _GPMB_KetQuaService;
        public BcDieuHanhGPMBTongHopKetQuaThucHienController(IErrorService errorService,
            IGPMB_KetQuaService GPMB_KetQuaService
            ) : base(errorService)
        {
            this._GPMB_KetQuaService = GPMB_KetQuaService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("ketquathuchiengpmb")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoGPMBTongHopKetQua(HttpRequestMessage request,string filter = null)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            //if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            //{
            //    return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            //}

            string documentName = BaoCaoGPMBTongHopKetQua(filter);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoGPMBTongHopKetQua(string filter)
        {
            //string namPD = iNamPD.ToString();

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/KetQuaThucHienGPMB.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("KetQuaThucHienGPMB-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];

                        var lstData = _GPMB_KetQuaService.GetGPMB_BanLamViec(filter);

                        int rowIndex = 5;
                        int count = 0;

                        rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex);

                        using (ExcelRange rng = sheet.Cells["A5:L" + (rowIndex-1) ])
                        {
                            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            rng.Style.Font.Name = "Times New Roman";
                            rng.Style.Font.Size = 14;
                            rng.Style.WrapText = true;
                        }

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int[] ar_i = new int[] { 5, 31, 16, 16, 16, 16, 16, 16, 16, 16, 16, 28 };
                        for(int i = 1; i<=12;i++)
                        {
                            sheet.Column(i).Width = ar_i[(i - 1)];
                        }

                        pck.SaveAs(new FileInfo(fullPath));
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<GPMB_BanLamViec> lstData, int count, int rowIndex)
        {
            foreach (var kq in lstData)
            {
                count++;

                sheet.Cells[rowIndex, 1].Value = count;
                sheet.Cells[rowIndex, 2].Value = kq.TenDuAn;
                sheet.Cells[rowIndex, 3].Value = kq.TongQuan_TongDienTich;
                sheet.Cells[rowIndex, 4].Value = kq.TongQuan_TongSoHo;
                sheet.Cells[rowIndex, 5].Value = kq.DHT_DienTich;
                sheet.Cells[rowIndex, 6].Value = kq.DHT_SoHo;
                sheet.Cells[rowIndex, 7].Value = kq.KKVM_DienTich;
                sheet.Cells[rowIndex, 8].Value = kq.KKVM_SoHo;
                sheet.Cells[rowIndex, 9].Value = kq.KKVM_SoHo_DaPheChuaNhanTien;
                sheet.Cells[rowIndex, 10].Value = kq.KKVM_SoHo_KhongHopTacDieuTra;
                sheet.Cells[rowIndex, 11].Value = kq.KKVM_SoHo_ChuaDieuTra;
                sheet.Cells[rowIndex, 12].Value = kq.GhiChu;

                int[] ar_i = new int[] { 1, 2, 12 };
                BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 2);

                ar_i = new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11 };
                BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 3);

                for (int i = 3; i <= 7; i=i+2)
                {
                    sheet.Cells[rowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[rowIndex, i].Style.Numberformat.Format = "#,##0.##";
                }

                sheet.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[rowIndex, 4].Style.Numberformat.Format = "#,##0";

                sheet.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[rowIndex, 6].Style.Numberformat.Format = "#,##0";

                for (int i = 8; i <= 11; i++)
                {
                    sheet.Cells[rowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[rowIndex, i].Style.Numberformat.Format = "#,##0";
                }

                rowIndex++;


            }
            return rowIndex;
        }
    }
}
