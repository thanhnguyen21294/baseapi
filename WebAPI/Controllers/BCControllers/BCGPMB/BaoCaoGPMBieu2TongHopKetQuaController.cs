﻿using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.QLTD.GPMB;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Service;
using Service.GPMBService;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.BCControllers.BCGPMB
{
    [Authorize]
    [RoutePrefix("api/BaocaoGPMB")]
    public class BaoCaoGPMBieu2TongHopKetQuaController : ApiControllerBase
    {
        private IQLTD_KeHoachService _QLTD_KeHoachService;

        private IGPMB_KetQuaService _GPMB_KetQuaService;
        private IBaoCaoTableService _baoCaoTableService;
        public BaoCaoGPMBieu2TongHopKetQuaController(IErrorService errorService,
            IGPMB_KetQuaService GPMB_KetQuaService,
            IBaoCaoTableService baoCaoTableService,
            IQLTD_KeHoachService QLTD_KeHoachService
            ) : base(errorService)
        {
            this._GPMB_KetQuaService = GPMB_KetQuaService;
            this._baoCaoTableService = baoCaoTableService;
            this._QLTD_KeHoachService = QLTD_KeHoachService;
        }

        string[] Columns = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG" };
        string[] SoLaMa = new[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", };
        //Xuất Excel báo cáo tỉnh phê duyệt

        //Xuất Excel báo cáo tỉnh phê duyệt
        [Route("BC_GPMB_TonghopKetquaThuchien")]
        [HttpGet]
        public HttpResponseMessage ExportBaoCaoUyBan_PL1(HttpRequestMessage request, string iIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            if (iIDDuAn == "undefined" || string.IsNullOrEmpty(iIDDuAn))
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Chưa chọn dự án để xuất");
            }

            string documentName = BaoCaoGPMB_TongHopKetQuaThucHien(iIDDuAn, szDonViTinh);

            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Lỗi xuất báo cáo");
            }
            //If something fails or somebody calls invalid URI, throw error.
        }

        private string BaoCaoGPMB_TongHopKetQuaThucHien(string szIDDuAn, string szDonViTinh)
        {
            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/GPMB_TongHopKetQuaThucHien.xlsx");

            //string sTenBC = "BC_KetQuaLuaChonNhaThau_UBNDTinhPD_" + namPD + ".";

            string documentName = string.Format("BaoCaoGPMB_TongHopKetQuaThucHien" + "-{0:ddMMyyyyhhmmsss}.xlsx", DateTime.Now);

            string fullPath = Path.Combine(filePath, documentName);

            //Read Template
            try
            {
                using (FileStream templateDocumentStream = File.OpenRead(templateDocument))
                {
                    //Create Excel EPPlus Package based on template stream
                    using (ExcelPackage pck = new ExcelPackage(templateDocumentStream))
                    {
                        //Grab the sheet with the template, sheet name is "BOL".
                        ExcelWorksheet sheet = pck.Workbook.Worksheets["Sheet1"];
                        var lstData = _GPMB_KetQuaService.GetBC_GPMB_TongHopKetQua(szIDDuAn);
                        int rowIndex = 8;
                        int count = 0;

                        rowIndex = XuatBaoCao(sheet, lstData, count, rowIndex, szDonViTinh);

                        sheet.Cells["N5"].Value = sheet.Cells["N5"].Value.ToString() + "(" + szDonViTinh + ")";
                        var a = sheet.Cells["N5"].Value;

                        if (rowIndex > 8)
                        {
                            using (ExcelRange rng = sheet.Cells["A8:AL" + (rowIndex - 1)])
                            {
                                rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                rng.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                rng.Style.Font.Name = "Times New Roman";
                                rng.Style.Font.Size = 11;
                                rng.Style.WrapText = true;
                            }
                        }

                        sheet.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int[] ar_i = new int[] { 5, 35, 18, 18, 18, 10, 18, 10, 18, 10, 10, 18, 18, 15, 18, 10, 18, 10, 18, 10, 10, 18, 18, 18, 10, 18, 10, 18, 10, 18, 10, 18, 10, 18, 10, 18, 18, 18, 18, 18, 30 };
                        for (int i = 1; i <= 34; i++)
                        {
                            sheet.Column(i).Width = ar_i[(i - 1)];
                        }

                        pck.SaveAs(new FileInfo(fullPath));

                        //Thêm báo cáo vào database
                        _baoCaoTableService.AddBaoCao(documentName, "BC_GPMB_THKQTH", folderReport, User.Identity.Name);
                        _baoCaoTableService.Save();
                    }
                    return documentName;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private int XuatBaoCao(ExcelWorksheet sheet, IEnumerable<BaoCaoGPMBTongHopKQ> lstData, int count, int rowIndex, string szDonViTinh)
        {
            int dDim;
            switch (szDonViTinh)
            {
                case "Đồng":
                    dDim = 1000;
                    break;
                case "Triệu Đồng":
                    dDim = 1000000;
                    break;
                case "Tỷ Đồng":
                    dDim = 1000000000;
                    break;
                default:
                    dDim = 1;
                    break;
            }

            foreach (var kq in lstData)
            {
                count++;

                sheet.Cells[rowIndex, 1].Value = count;
                sheet.Cells[rowIndex, 2].Value = kq.TenDuAn;

                sheet.Cells[rowIndex, 3].Value = kq.TongQuan_TongDT;
                sheet.Cells[rowIndex, 4].Value = kq.TongQuan_TongSH;
                sheet.Cells[rowIndex, 5].Value = kq.TongQuan_NNCaThe_DT;
                sheet.Cells[rowIndex, 6].Value = kq.TongQuan_NNCaThe_SH;
                sheet.Cells[rowIndex, 7].Value = kq.TongQuan_NNCong_DT;
                sheet.Cells[rowIndex, 8].Value = kq.TongQuan_NNCong_SH;
                sheet.Cells[rowIndex, 9].Value = kq.TongQuan_DatKhac_DT;
                sheet.Cells[rowIndex, 10].Value = kq.TongQuan_DatKhac_SH;
                sheet.Cells[rowIndex, 11].Value = kq.TongQuan_MoMa;

                sheet.Cells[rowIndex, 12].Value = kq.DaHT_DT;
                sheet.Cells[rowIndex, 13].Value = kq.DaHT_SH;
                sheet.Cells[rowIndex, 14].Value = kq.DaHT_TGT / dDim;
                sheet.Cells[rowIndex, 15].Value = kq.DaHT_NNCaThe_DT;
                sheet.Cells[rowIndex, 16].Value = kq.DaHT_NNCaThe_SH;
                sheet.Cells[rowIndex, 17].Value = kq.DaHT_NNCong_DT;
                sheet.Cells[rowIndex, 18].Value = kq.DaHT_NNCong_SH;
                sheet.Cells[rowIndex, 19].Value = kq.DaHT_DatKhac_DT;
                sheet.Cells[rowIndex, 20].Value = kq.DaHT_DatKhac_SH;
                sheet.Cells[rowIndex, 21].Value = kq.DaHT_MoMa;

                sheet.Cells[rowIndex, 22].Value = kq.CTHX_DT;
                sheet.Cells[rowIndex, 23].Value = kq.CTHX_SH;
                sheet.Cells[rowIndex, 24].Value = kq.CTHX_HTDTPA_DT;
                sheet.Cells[rowIndex, 25].Value = kq.CTHX_HTDTPA_SH;
                sheet.Cells[rowIndex, 26].Value = kq.CTHX_CKDTPA_DT;
                sheet.Cells[rowIndex, 27].Value = kq.CTHX_CKDTPA_SH;
                sheet.Cells[rowIndex, 28].Value = kq.CTHX_HTPACT_DT;
                sheet.Cells[rowIndex, 29].Value = kq.CTHX_HTPACT_SH;

                sheet.Cells[rowIndex, 30].Value = kq.KKVM_DT;
                sheet.Cells[rowIndex, 31].Value = kq.KKVM_SH;
                sheet.Cells[rowIndex, 32].Value = kq.KKVM_SH_CNT;
                sheet.Cells[rowIndex, 33].Value = kq.KKVM_SH_KHTDT;
                sheet.Cells[rowIndex, 34].Value = kq.KKVM_SH_CDT;

                sheet.Cells[rowIndex, 35].Value = kq.GhiChu;

                int[] ar_i = new int[] { 1, 2, 35 };
                BaoCaoDungChung.In_Style(sheet, rowIndex, ar_i, 2);

                int i = 0;
                for (i = 3; i <= 34; i++)
                {
                    sheet.Cells[rowIndex, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[rowIndex, i].Style.Numberformat.Format = "#,##0";
                }

                for (i = 3; i <= 9; i = i + 2)
                {
                    In_Style(sheet, rowIndex, i);
                }

                In_Style(sheet, rowIndex, 12);

                for (i = 15; i <= 19; i = i + 2)
                {
                    In_Style(sheet, rowIndex, i);
                }

                for (i = 22; i <= 29; i++)
                {
                    In_Style(sheet, rowIndex, i);
                }

                In_Style(sheet, rowIndex, 30);

                rowIndex++;



            }
            return rowIndex;
        }

        private void In_Style(ExcelWorksheet sheet, int row, int col)
        {
            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet.Cells[row, col].Style.Numberformat.Format = "#,###.##";
        }
    }
}
