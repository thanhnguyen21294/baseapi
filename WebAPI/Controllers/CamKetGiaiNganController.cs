﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/camketgiaingan")]
    [Authorize]
    public class CamKetGiaiNganController : ApiControllerBase
    {
        private ICamKetGiaiNganService _CamKetGiaiNganService;
        public CamKetGiaiNganController(IErrorService errorService, ICamKetGiaiNganService CamKetGiaiNganService) : base(errorService)
        {
            this._CamKetGiaiNganService = CamKetGiaiNganService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _CamKetGiaiNganService.GetAll().OrderByDescending(x => x.IdCamKetGiaiNgan);

                var modelVm = Mapper.Map<IEnumerable<CamKetGiaiNgan>, IEnumerable<CamKetGiaiNganViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _CamKetGiaiNganService.GetByFilter(page, pageSize, out totalRow);

                var modelVm = Mapper.Map<IEnumerable<CamKetGiaiNgan>, IEnumerable<CamKetGiaiNganViewModels>>(model);

                PaginationSet<CamKetGiaiNganViewModels> pagedSet = new PaginationSet<CamKetGiaiNganViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _CamKetGiaiNganService.GetById(id);

                var responseData = Mapper.Map<CamKetGiaiNgan, CamKetGiaiNganViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, CamKetGiaiNganViewModels CamKetGiaiNganViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newCamKetGiaiNgan = new CamKetGiaiNgan();
                    newCamKetGiaiNgan.UpdateCamKetGiaiNgan(CamKetGiaiNganViewModels);
                    newCamKetGiaiNgan.CreatedDate = DateTime.Now;
                    newCamKetGiaiNgan.CreatedBy = User.Identity.GetUserId();
                    _CamKetGiaiNganService.Add(newCamKetGiaiNgan);
                    _CamKetGiaiNganService.Save();

                    var responseData = Mapper.Map<CamKetGiaiNgan, CamKetGiaiNganViewModels>(newCamKetGiaiNgan);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, CamKetGiaiNganViewModels CamKetGiaiNganViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbCamKetGiaiNgan = _CamKetGiaiNganService.GetById(CamKetGiaiNganViewModels.IdCamKetGiaiNgan);

                    dbCamKetGiaiNgan.UpdateCamKetGiaiNgan(CamKetGiaiNganViewModels);
                    dbCamKetGiaiNgan.UpdatedDate = DateTime.Now;
                    dbCamKetGiaiNgan.UpdatedBy = User.Identity.GetUserId();
                    _CamKetGiaiNganService.Update(dbCamKetGiaiNgan);
                    _CamKetGiaiNganService.Save();

                    var responseData = Mapper.Map<CamKetGiaiNgan, CamKetGiaiNganViewModels>(dbCamKetGiaiNgan);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbCamKetGiaiNgan = _CamKetGiaiNganService.GetById(vnTrangThaiUp.ID);
                    dbCamKetGiaiNgan.TrangThaiGui = 1;
                    dbCamKetGiaiNgan.UpdatedDate = DateTime.Now;
                    dbCamKetGiaiNgan.UpdatedBy = User.Identity.GetUserId();
                    _CamKetGiaiNganService.Update(dbCamKetGiaiNgan);
                    _CamKetGiaiNganService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }


        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _CamKetGiaiNganService.Delete(id);
                    _CamKetGiaiNganService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "CAMKETGIAINGAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listCamKetGiaiNgan = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listCamKetGiaiNgan)
                    {
                        _CamKetGiaiNganService.Delete(item);
                    }

                    _CamKetGiaiNganService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listCamKetGiaiNgan.Count);
                }

                return response;
            });
        }
    }
}
