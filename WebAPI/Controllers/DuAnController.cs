﻿using AutoMapper;
using Data;
using Microsoft.AspNet.Identity;
using Model.Models;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using Service;
using Service.QLTDService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Providers;
using WebAPI.SignalR;

namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/duan")]
    public class DuAnController : ApiControllerBase
    {
        private IDuAnService _duAnService;
        private IDuAnUserService _duAnUserService;
        private IPhongCongTacService _phongCongTacService;
        private INguonVonDuAnService _nguonVonDuAnService;
        private IThuMucService _thumucService;
        private IHopDongService _hopDongService;
        private IDeleteService _deleteService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        private IDuAnQuanLyService _duAnQuanLyService;
        private IGoiThauService _goiThauService;
        private IQLTD_CTCV_ThucHienDuAnService _QLTD_CTCV_ThucHienDuAnService;
        private IQuanLyDAService _quanLyDAService;
        public DuAnController(IErrorService errorService, IDuAnService duAnService,
            IDuAnUserService duAnUserService,
            IPhongCongTacService phongCongTacService,
            IDanhMucNhaThauService danhMucNhaThauService,
            IDuAnQuanLyService duAnQuanLyService,
            IQLTD_CTCV_ThucHienDuAnService QLTD_CTCV_ThucHienDuAnService,
            IHopDongService hopDongService,
            IGoiThauService goiThauService,
        INguonVonDuAnService nguonVonDuAnService, IDeleteService deleteService, IThuMucService thuMucService, IQuanLyDAService quanLyDAService) : base(errorService)
        {
            this._duAnService = duAnService;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._duAnUserService = duAnUserService;
            _phongCongTacService = phongCongTacService;
             _goiThauService = goiThauService;
            this._thumucService = thuMucService;
            this._deleteService = deleteService;
            this._hopDongService = hopDongService;
            this._duAnQuanLyService = duAnQuanLyService;
            _danhMucNhaThauService = danhMucNhaThauService;
            _QLTD_CTCV_ThucHienDuAnService  = QLTD_CTCV_ThucHienDuAnService;
            _quanLyDAService = quanLyDAService;
        }
        Dictionary<string, string> dicUserNhanVien = new Dictionary<string, string>();
        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }
                var model = _duAnService.GetListDuAns(idNhomDuAnTheoUser, idUser);
                var modelVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<ListDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getchonduan")]
        [HttpGet]
        public HttpResponseMessage GetChonDuAn(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }
                var model = _duAnService.GetListChonDuAns(idNhomDuAnTheoUser, idUser);
                var modelVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<ListDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getalltoactionhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllToActionHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _duAnService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<DuAn>, IEnumerable<ListDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null, int IdGiaiDoan = -1)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    //if (User.IsInRole("Nhân viên"))
                    //{
                    //    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    //    {
                    //        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    //    }
                    //}
                    idUser = User.Identity.GetUserId();
                }

                var model = _duAnService.GetByFilter(idNhomDuAnTheoUser, idUser, page, pageSize, out totalRow, filter, IdGiaiDoan);
                //var model = _duAnService.GetByFilter(idNhomDuAnTheoUser, page, pageSize, out totalRow, filter);
                // var model = _duAnService.GetByFilter(null, page, pageSize, out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[0]);
                var modelAllVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[1]);
                PaginationSet<DuAnSearchViewModels> pagedSet = new PaginationSet<DuAnSearchViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                    AllItems = modelAllVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getfilterlistpaging")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetFilterListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null, string thoigianthuchien = null, string diadiem = null, string strIdChuDauTu = null, string strIdNhomDuAn = null, string strIdTinhTrangDuAn = null, string strIdLinhVucNganhNghe = null, string strIdNguonVon = null, string strIdGiaiDoan = null, string strIdQuanLy = null, string strIdPhongBan = null, string tenDA = null, int IdGiaiDoan = -1)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";

                List<DuAn> allDuAns = new List<DuAn>();
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }

                var model = _duAnService.GetByNewFilter(idNhomDuAnTheoUser, idUser, page, pageSize, out totalRow, filter, thoigianthuchien, diadiem, strIdChuDauTu, strIdNhomDuAn, strIdTinhTrangDuAn, strIdLinhVucNganhNghe, strIdNguonVon, strIdGiaiDoan, strIdQuanLy, strIdPhongBan, tenDA, IdGiaiDoan);
                var modelVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[0]);
                //var modelAllVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[1]);
                PaginationSet<DuAnSearchViewModels> pagedSet = new PaginationSet<DuAnSearchViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm,
                    AllItems = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getlistphanquyen")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetListPhanQuyen(HttpRequestMessage request, int page, int pageSize, string filter = null, string diadiem = null, int IdGiaiDoan = -1, string uid = null, int pid = 0, int lvid = 0, string fullname = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";
                
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }
                dicUserNhanVien.Clear();
                //var users = AppUserManager.Users;
                //foreach (AppUser user in users)
                //{
                //    var roleTemp = AppUserManager.GetRoles(user.Id);
                //    if (roleTemp.Contains("Nhân viên"))
                //    {
                //        dicUserNhanVien.Add(user.Id, user.FullName);
                //    }
                //}
                var model = _duAnService.GetPhanQuyenNew(idNhomDuAnTheoUser, idUser, page, pageSize, out totalRow, filter, diadiem, IdGiaiDoan, uid, pid, lvid, dicUserNhanVien, fullname);
                //var modelVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[0]);
                //var modelAllVm = Mapper.Map<IEnumerable<DuAnSearch>, IEnumerable<DuAnSearchViewModels>>(model[1]);
                //PaginationSet<DuAnSearchViewModels> pagedSet = new PaginationSet<DuAnSearchViewModels>()
                //{
                //    PageIndex = page,
                //    TotalRows = totalRow,
                //    PageSize = pageSize,
                //    Items = modelVm,
                //    AllItems = modelAllVm
                //};

                var returnNew = new
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = model[0],
                    AllItems = model[1]
                };
                response = request.CreateResponse(HttpStatusCode.OK, returnNew);

                return response;
            });
        }




        [Route("getbyyearname")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPTINHHINHPHEDUYET")]
        public HttpResponseMessage GetByYear(HttpRequestMessage request, string year, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {
                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                }

                var model = _duAnService.DuAnByYearName(year, filter, idNhomDuAnTheoUser);

                var modelVm = Mapper.Map<IEnumerable<DuAn>, IEnumerable<DuAnViewModels>>(model);

                PaginationSet<DuAnViewModels> pagedSet = new PaginationSet<DuAnViewModels>()
                {
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbynguonvonyearname")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage DuAnNguonVonByYearName(HttpRequestMessage request, int year, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var model = _duAnService.DuAnNguonVonByYearName(year, filter).Select(x => new
                {
                    IdDuAn = x.IdDuAn,
                    TenDuAn = x.TenDuAn,
                    IdDuAnCha = x.IdDuAnCha,
                    NguonVons = x.NguonVonDuAns.Select(a => a.NguonVon.TenNguonVon),
                    ThoiGianThucHien = x.ThoiGianThucHien,
                    LapQuanLyDauTus = x.LapQuanLyDauTus.Select(a => new { a.NgayTrinh, a.NgayPheDuyet, a.SoQuyetDinh, a.TongGiaTri, a.LoaiQuanLyDauTu }),
                    KeHoachVons = x.KeHoachVons.Select(a => new { a.TongGiaTri, a.NgayPheDuyet }),
                });
                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("duanguonvonkhv")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage DuAnNguonVonKHV(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                int? idNhomDuAnTheoUser = null;

                if (!User.IsInRole("Admin"))
                {
                    if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                    {
                        idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                    }
                }

                var model = _duAnService.DuAnNguonVonKHV(idNhomDuAnTheoUser).Select(x => new
                {
                    IdDuAn = x.IdDuAn,
                    TenDuAn = x.TenDuAn,
                    IdDuAnCha = x.IdDuAnCha,
                    NguonVons = x.NguonVonDuAns.Select(a => a.NguonVon.TenNguonVon),
                    ThoiGianThucHien = x.ThoiGianThucHien,
                    LapQuanLyDauTus = x.LapQuanLyDauTus.Select(a => new { a.NgayTrinh, a.NgayPheDuyet, a.SoQuyetDinh, a.TongGiaTri, a.LoaiQuanLyDauTu }),
                    KeHoachVons = x.KeHoachVons.Select(a => new { a.TongGiaTri, a.NgayPheDuyet, a.NienDo }),
                });

                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _duAnService.GetById(id);

                var responseData = Mapper.Map<DuAn, DuAnViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("check")]
        [HttpGet]
        [Permission(Action = "Create", Function = "DUAN")]
        public int Check(DuAnViewModels duAntVm)
        {
            var listDuAnTen = _duAnService.GetAll().Where(x => x.TenDuAn.Trim() == duAntVm.TenDuAn.Trim());
            return listDuAnTen.Count();
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, DuAnViewModels duAntVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    //var listDuAn = _duAnService.GetAll().Where(x => x.Ma.Trim() == duAntVm.Ma.Trim());
                    var l = _duAnService.GetAll().Select(x => x.Ma).ToList();
                    var listDuAn = l.Where(y => y.Trim() == duAntVm.Ma.Trim());
                    if (listDuAn.Count() > 0)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, "Mã dự án đã tồn tại.");
                    }

                    //var listDuAnTen = _duAnService.GetAll().Where(x => x.TenDuAn.Trim() == duAntVm.TenDuAn.Trim());
                    var listTenDuAn = _duAnService.GetAll().Select(x => x.TenDuAn).ToList();
                    //var listMaDuAn = _duAnService.GetAll().Select(y => y.Ma).ToList();
                    var listTenDuAnTrung = listTenDuAn.Where(y => y.Trim() == duAntVm.TenDuAn.Trim());
                    if (listTenDuAnTrung.Count() > 0)
                    {
                        return request.CreateResponse("Tên dự án đã tồn tại");
                    }

                    //listDuAnTenTrungNhieu.Where(y => y.TenDuAn.Trim().Length == duAntVm.TenDuAn.Trim().Length*0.8);
                    //var listDATenTrung = listTenDuAn.Where(y => y.Trim().Length == duAntVm.TenDuAn.Trim().Length*0.8);
                    char[] delimiterChars = { ' ', ',' };
                    string[] tenNhapTemp = duAntVm.TenDuAn.Trim().Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                    string[] tenNhap = tenNhapTemp.Distinct().ToArray();
                    //List<string> tenNhapList = tenNhap.ToList();
                    //tenNhapList.RemoveAll(i => i == "");
                    int dem = 0;
                    //List<string> listTenDuAnTu80 = null;
                    //List<PhanHoi> phLst = null;
                    PhanHoi ph = new PhanHoi();
                    //HttpResponseMessage hrm = new HttpResponseMessage();
                    foreach (var tenDuAn in listTenDuAn)
                    {
                        string[] chusTemp = tenDuAn.Trim().Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
                        string[] chus = chusTemp.Distinct().ToArray();
                        //List<string> chuList = chus.ToList();
                        //chuList.RemoveAll(i => i == "");
                        foreach (var chu in chus)
                            foreach (var ten in tenNhap)
                                //if (ten.Equals(chu))
                                if (chu.Equals(ten))
                                    dem++;
                        double tiLe = (double) dem / chus.Length;
                        if (tiLe >= 0.5 )
                        {
                            //PhanHoi ph = new PhanHoi() { phanTram = tiLe, tenDuAn = tenDuAn };
                            ph.phanTram.Add(tiLe);
                            ph.tenDuAn.Add(tenDuAn);
                            //phLst.Add(ph);
                            //response.Content = new ObjectContent<List<PhanHoi>>(phLst, new JsonMediaTypeFormatter(), "application/json");
                            //HttpResponseMessage hrm = request.CreateResponse("Tên dự án đã bị trùng ít nhất là 80%");
                            //return hrm;
                        }
                        dem = 0;
                    }
                    //duAntVm.PhanHoi = ph;
                    //if (listDATenTrung.Count() > 0)
                    //    return request.CreateResponse("Tên dự án đã bị trùng 80%");
                    response = request.CreateResponse(HttpStatusCode.Created, ph);
                    if (response != null)
                        return response;
                    response = OnlyAdd(request, duAntVm);
                }
                return response;
            });
        }
        private HttpResponseMessage OnlyAdd(HttpRequestMessage request, DuAnViewModels duAntVm)
        {
            HttpResponseMessage response = null;
            // tạo mới dự án
            var newDuAn = new DuAn();
            newDuAn.UpdateDuAn(duAntVm);
            newDuAn.CreatedDate = DateTime.Now;
            newDuAn.CreatedBy = User.Identity.GetUserId();
            // phân quyền cho trưởng phòng
            int? idNhomDuAnTheoUser = null;
            if (!User.IsInRole("Admin"))
            {
                idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
            }
            newDuAn.IdNhomDuAnTheoUser = idNhomDuAnTheoUser;
            try
            {
                List<NguonVonDuAn> listNguonVonDuAns = new List<NguonVonDuAn>();
                foreach (var item in duAntVm.NguonVonDuAns)
                {
                    listNguonVonDuAns.Add(new NguonVonDuAn()
                    {
                        IdNguonVon = item.IdNguonVon,
                        GiaTri = item.GiaTri,
                                GiaTriTrungHan = item.GiaTriTrungHan
                    });
                }
                newDuAn.NguonVonDuAns = listNguonVonDuAns;
            }
            catch (Exception e)
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
            }
            foreach (var item in duAntVm.DuAnQuanLys)
            {
                DuAnQuanLy newDAQL = new DuAnQuanLy();
                newDAQL.IdDuAn = duAntVm.IdDuAn;
                newDAQL.IdQuanLyDA = item.IdQuanLyDA;
                _duAnQuanLyService.Add(newDAQL);
                _duAnQuanLyService.Save();
            }
            _duAnService.Add(newDuAn);
            _duAnService.Save();
            _thumucService.AddThuMucWhenDuAnCreated(newDuAn.IdDuAn);
            // Quyền user tạo ra
            if (!User.IsInRole("Admin"))
            {
                // Thêm quyền cho nhân viên tạo ra
                string idUser = User.Identity.GetUserId();
                var newDuanUser = new DuAnUser();
                newDuanUser.UserId = idUser;
                newDuanUser.IdDuAn = newDuAn.IdDuAn;
                newDuanUser.CreatedDate = DateTime.Now;
                newDuanUser.CreatedBy = User.Identity.GetUserId();
                newDuanUser.Status = true;
                _duAnUserService.Add(newDuanUser);
                _duAnUserService.Save();
                // thêm quyền cho PGD vs GD
                var users = AppUserManager.Users;
                foreach (AppUser user in users)
                {
                    var roleTemp = AppUserManager.GetRoles(user.Id);
                    if (roleTemp.Contains("Phó giám đốc") || roleTemp.Contains("Giám đốc"))
                    {
                        var newDuanUserPGD = new DuAnUser();
                        newDuanUser.UserId = user.Id;
                        newDuanUser.IdDuAn = newDuAn.IdDuAn;
                        newDuanUser.CreatedDate = DateTime.Now;
                        newDuanUser.CreatedBy = User.Identity.GetUserId();
                        newDuanUser.Status = true;
                        _duAnUserService.Add(newDuanUser);
                        _duAnUserService.Save();
                    }
                    if (roleTemp.Contains("Trưởng phòng") && user.IdNhomDuAnTheoUser == newDuAn.IdNhomDuAnTheoUser)
                    {
                        var newDuanUserPGD = new DuAnUser();
                        newDuanUser.UserId = user.Id;
                        newDuanUser.IdDuAn = newDuAn.IdDuAn;
                        newDuanUser.CreatedDate = DateTime.Now;
                        newDuanUser.CreatedBy = User.Identity.GetUserId();
                        newDuanUser.Status = true;
                        _duAnUserService.Add(newDuanUser);
                        _duAnUserService.Save();
                    }
                    if (user.IdNhomDuAnTheoUser == 13)
                    {
                        var newDuanUserPGD = new DuAnUser();
                        newDuanUser.UserId = user.Id;
                        newDuanUser.IdDuAn = newDuAn.IdDuAn;
                        newDuanUser.CreatedDate = DateTime.Now;
                        newDuanUser.CreatedBy = User.Identity.GetUserId();
                        newDuanUser.Status = true;
                        _duAnUserService.Add(newDuanUser);
                        _duAnUserService.Save();
                    }
                }

                var newPCT = new PhongCongTac();
                newPCT.IdDuAn = newDuAn.IdDuAn;
                newPCT.IdNhomDuAnTheoUser = 13;
                newPCT.CreatedDate = DateTime.Now;
                newPCT.CreatedBy = User.Identity.GetUserId();
                newPCT.Status = true;
                _phongCongTacService.Add(newPCT);
                _phongCongTacService.Save();
            }

            //SignalR
            //push notification
            QLDAHub.PushDAToAllUsers(Mapper.Map<DuAn, DuAnViewModels>(newDuAn), null);
            // end.

            var responseData = Mapper.Map<DuAn, DuAnViewModels>(newDuAn);
            response = request.CreateResponse(HttpStatusCode.Created, responseData);
            return response;
        }

        [Route("addNoCheck")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage CreateNoCheck(HttpRequestMessage request, DuAnViewModels duAntVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    response = OnlyAdd(request, duAntVm);
                }
                return response;
            });
        }

        [Route("UpdatePTC")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage UpdatePTC(HttpRequestMessage request, DuAnViewModels duAntVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    string bbb = "1410fe8b-4fde-4db1-b0bc-43631dbd6aa5";
                    var listDuAn = _duAnService.GetAll().ToList();
                    foreach (var da in listDuAn)
                    {
                        var aaa = _duAnUserService.GetAll().Where(x => x.IdDuAn == da.IdDuAn && x.UserId == bbb).FirstOrDefault();
                        if (aaa == null)
                        {
                            var newDuanUser = new DuAnUser();
                            newDuanUser.UserId = bbb;
                            newDuanUser.IdDuAn = da.IdDuAn;
                            newDuanUser.CreatedDate = DateTime.Now;
                            newDuanUser.CreatedBy = User.Identity.GetUserId();
                            newDuanUser.Status = true;
                            _duAnUserService.Add(newDuanUser);
                            _duAnUserService.Save();
                        }
                    }
                }
                response = request.CreateResponse(HttpStatusCode.Created, "OK");
                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, DuAnViewModels duAnVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbDuAn = _duAnService.GetById(duAnVm.IdDuAn);

                    dbDuAn.UpdateDuAn(duAnVm);
                    dbDuAn.UpdatedDate = DateTime.Now;
                    dbDuAn.UpdatedBy = User.Identity.GetUserId();
                    try
                    {
                        //_nguonVonDuAnService.DeleteByIdDuAn(duAnVm.IdDuAn);
                        var listNguonVonDuAnOld = _nguonVonDuAnService.GetByIdDuAn(duAnVm.IdDuAn).Where(x => x.IdLapQuanLyDauTu == null);
                        var listDeleteNguonVonDuAn = listNguonVonDuAnOld.Where(x =>
                            !duAnVm.NguonVonDuAns.Select(y => y.IdNguonVonDuAn).Contains(x.IdNguonVonDuAn)).Where(x => x.IdLapQuanLyDauTu == null);
                        foreach (var item in listDeleteNguonVonDuAn)
                        {
                            _nguonVonDuAnService.Delete(item.IdNguonVonDuAn);
                        }
                        foreach (var item in duAnVm.NguonVonDuAns)
                        {
                            if (listNguonVonDuAnOld.Count(x => x.IdNguonVonDuAn == item.IdNguonVonDuAn) == 0)
                            {
                                _nguonVonDuAnService.Add(new NguonVonDuAn()
                                {
                                    IdDuAn = duAnVm.IdDuAn,
                                    IdNguonVon = item.IdNguonVon,
                                    GiaTri = item.GiaTri,
                                    GiaTriTrungHan = item.GiaTriTrungHan
                                });
                            }
                            else
                            {
                                var editNguonVonDuAn = _nguonVonDuAnService.GetById(item.IdNguonVonDuAn);
                                editNguonVonDuAn.UpdateNguonVonDuAn(item);
                                editNguonVonDuAn.IdDuAn = duAnVm.IdDuAn;
                            }
                        }
                        IEnumerable<DuAnQuanLy> daqls = _duAnQuanLyService.GetByIdDA(duAnVm.IdDuAn);
                        foreach (DuAnQuanLy item in daqls)
                        {
                            _duAnQuanLyService.Delete(item.Id);
                        }
                        foreach (var item in duAnVm.DuAnQuanLys)
                        {
                            DuAnQuanLy newDAQL = new DuAnQuanLy();
                            newDAQL.IdDuAn = duAnVm.IdDuAn;
                            newDAQL.IdQuanLyDA = item.IdQuanLyDA;
                            _duAnQuanLyService.Add(newDAQL);
                        }
                        _duAnQuanLyService.Save();
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _duAnService.Update(dbDuAn);
                    _duAnService.Save();

                    var responseData = Mapper.Map<DuAn, DuAnViewModels>(dbDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("getghichuketoan/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByDuAnGhiChuKeToan(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _duAnService.GetById(id);
                var response = request.CreateResponse(HttpStatusCode.OK, model.GhiChuKeToan);
                return response;
            });
        }

        [Route("updateghichu")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage UpdateGhiChu(HttpRequestMessage request, DuAnGhiChuViewModels duAnVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbDuAn = _duAnService.GetById(duAnVm.IdDuAn);
                    dbDuAn.GhiChuKeToan = duAnVm.GhiChuKeToan;
                    dbDuAn.UpdatedDate = DateTime.Now;
                    dbDuAn.UpdatedBy = User.Identity.GetUserId();
                    _duAnService.Update(dbDuAn);
                    _duAnService.Save();

                    var responseData = Mapper.Map<DuAn, DuAnViewModels>(dbDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("updatephongban")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public async Task<HttpResponseMessage> UpdatePhongBan(HttpRequestMessage request, DuAnViewModels duAnVm)
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                var dbDuAn = _duAnService.GetById(duAnVm.IdDuAn);
                // xóa hết user đang tồn tại của dự án

                var modelDuAnUser = _duAnUserService.GetByIdDuAn(duAnVm.IdDuAn).OrderByDescending(x => x.IdDuanUser);
                foreach (DuAnUser das in modelDuAnUser)
                {
                    //var roles = await AppUserManager.GetRolesAsync(das.UserId);
                    //if (roles.Contains("Nhân viên"))
                    //{
                    if (das.AppUsers.IdNhomDuAnTheoUser == dbDuAn.IdNhomDuAnTheoUser)
                    {
                        _duAnUserService.Delete(das.IdDuanUser);
                        _duAnUserService.Save();
                    }
                    //}
                }
                // end
                // thêm mới user của trưởng phòng mới
                var query = AppUserManager.Users.Where(x => x.Delete == false).Include("NhomDuAnTheoUser");
                query = query.Where(x => x.IdNhomDuAnTheoUser == duAnVm.IdNhomDuAnTheoUser);
                foreach (var item in query)
                {
                    var roles = await AppUserManager.GetRolesAsync(item.Id);
                    if (roles.Contains("Trưởng phòng"))
                    {
                        var newDuanUser = new DuAnUser();
                        newDuanUser.UserId = item.Id;
                        newDuanUser.IdDuAn = duAnVm.IdDuAn;
                        newDuanUser.CreatedDate = DateTime.Now;
                        newDuanUser.CreatedBy = User.Identity.GetUserId();
                        newDuanUser.Status = true;
                        _duAnUserService.Add(newDuanUser);
                        _duAnUserService.Save();
                    }
                }
                // end
                // thay đổi ID phòng ban mới
                dbDuAn.IdNhomDuAnTheoUser = duAnVm.IdNhomDuAnTheoUser;
                dbDuAn.UpdatedDate = DateTime.Now;
                dbDuAn.UpdatedBy = User.Identity.GetUserId();
                _duAnService.Update(dbDuAn);
                _duAnService.Save();
                // end

                var responseData = Mapper.Map<DuAn, DuAnViewModels>(dbDuAn);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
            }
            return response;
        }

        [Route("phongcongtac")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public async Task<HttpResponseMessage> PhongCongTac(HttpRequestMessage request, UpdateDuAnUserViewModels listIdPhongBan)
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                // lấy dự án đang xét
                var dbDuAn = _duAnService.GetById(listIdPhongBan.IdDuAn);
                // lấy danh sách các phòng ban đang được xét quyền cho dự án khác với phòng ban đang có
                var modelPhongBanDangSet = _phongCongTacService.GetByIdDuAn(listIdPhongBan.IdDuAn).Where(w => w.IdNhomDuAnTheoUser != dbDuAn.IdNhomDuAnTheoUser).Select(x => x.IdNhomDuAnTheoUser.ToString()).ToList();
                // phòng công tác bị xóa đi
                var pctXoa = modelPhongBanDangSet.Except(listIdPhongBan.lstIdUser).ToList();
                foreach (string idPCT in pctXoa)
                {
                    var pctDel = _phongCongTacService.GetByIdDuAnIDPhong(listIdPhongBan.IdDuAn, Convert.ToInt32(idPCT));
                    _phongCongTacService.Delete(pctDel.IdChuDauTu);
                    _phongCongTacService.Save();
                    // Xóa User đã set quyền
                    var users = AppUserManager.Users;
                    foreach (var item in users)
                    {
                        if (item.IdNhomDuAnTheoUser == Convert.ToInt32(idPCT))
                        {
                            var userXoas = _duAnUserService.GetByIdDuAnIDUser(listIdPhongBan.IdDuAn, item.Id).ToList();
                            if (userXoas.Count() > 0)
                            {
                                foreach (var userXoa in userXoas)
                                {
                                    _duAnUserService.Delete(userXoa.IdDuanUser);
                                    _duAnUserService.Save();
                                }
                            }
                        }
                    }
                }

                // Phòng công tác sẽ thêm mới
                var pctThem = listIdPhongBan.lstIdUser.Except(modelPhongBanDangSet).ToList();
                foreach (string uNew in pctThem)
                {
                    var newPCT = new PhongCongTac();
                    newPCT.IdNhomDuAnTheoUser = Convert.ToInt32(uNew);
                    newPCT.IdDuAn = listIdPhongBan.IdDuAn;
                    newPCT.CreatedDate = DateTime.Now;
                    newPCT.CreatedBy = User.Identity.GetUserId();
                    newPCT.Status = true;
                    _phongCongTacService.Add(newPCT);
                    _phongCongTacService.Save();
                    // thêm mới user của trưởng phòng mới
                    var users = AppUserManager.Users;
                    foreach (var item in users)
                    {
                        var roles = await AppUserManager.GetRolesAsync(item.Id);
                        if (roles.Contains("Trưởng phòng") && item.IdNhomDuAnTheoUser == Convert.ToInt32(uNew))
                        {
                            var newDuanUser = new DuAnUser();
                            newDuanUser.UserId = item.Id;
                            newDuanUser.IdDuAn = listIdPhongBan.IdDuAn;
                            newDuanUser.CreatedDate = DateTime.Now;
                            newDuanUser.CreatedBy = User.Identity.GetUserId();
                            newDuanUser.Status = true;
                            _duAnUserService.Add(newDuanUser);
                            _duAnUserService.Save();
                        }
                    }
                    // end
                }
                response = request.CreateResponse(HttpStatusCode.Created, "OK");
            }
            return response;
        }

        [Route("getduanphongcongtac")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetDuanPhongCongTac(HttpRequestMessage request, int idDuAn)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _phongCongTacService.GetByIdDuAn(idDuAn);
                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _duAnService.Delete(id);
                    _duAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                string a = "";
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listDuAn)
                    {
                        _deleteService.DeleteDuAn(item);
                    }
                    try
                    {
                        _deleteService.Save();
                    }
                    catch (Exception ex)
                    {
                        a = ex.StackTrace;
                    }

                    response = request.CreateResponse(HttpStatusCode.OK, a);
                }

                return response;
            });
        }

        //[Route("getlistpagingduanfull")]
        //[HttpGet]
        //[Permission(Action = "Read", Function = "DUAN")]
        //public HttpResponseMessage GetListPagingDuAnFull(HttpRequestMessage request, int page, int pageSize, string filter = null)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        int totalRow = 0;
        //        int? idNhomDuAnTheoUser = null;
        //        if (!User.IsInRole("Admin"))
        //        {
        //            idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
        //        }

        //        var model = _duAnService.DuAnFull(idNhomDuAnTheoUser, page, pageSize, out totalRow, filter);

        //        var modelVm = Mapper.Map<IEnumerable<DuAn>, IEnumerable<DuAnViewModels>>(model);

        //        PaginationSet<DuAnViewModels> pagedSet = new PaginationSet<DuAnViewModels>()
        //        {
        //            PageIndex = page,
        //            TotalRows = totalRow,
        //            PageSize = pageSize,
        //            Items = modelVm
        //        };

        //        response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

        //        return response;
        //    });
        //}
        [HttpGet]
        [Route("filter")]
        public HttpResponseMessage Filter(HttpRequestMessage request, string tenDA, string strIdDanhMucNhaThau, string strIdTinhTrangDuAn, string strIdNhomDuAn, string strIdChuDauTu, string strIdLinhVucNganhNghe, string strIdNguonVon, string strIdQuanLy)
        {
            HttpResponseMessage response = null;

            int? idNhomDuAnTheoUser = null;
            string idUser = "";
            if (!User.IsInRole("Admin"))
            {
                idUser = User.Identity.GetUserId();
                if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
            }

            List<DuAn> allDuAns = new List<DuAn>();

            if (User.IsInRole("Admin"))
            {
                allDuAns = _duAnService.GetAll().ToList();
            }
            else
            {
                allDuAns = _duAnService.GetAll().Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).ToList();
            }

            List<DuAn> listDuAnByTinhTrangDuAn = allDuAns;
            List<DuAn> listDuAnByNhomDuAn = allDuAns;
            List<DuAn> listDuAnByLinhVucNganhNghe = allDuAns;
            List<DuAn> listDuAnByChuDauTu = allDuAns;
            List<DuAn> listDuAnByNguonVon = allDuAns;
            List<DuAn> listDuAnByDanhMucNhaThau = allDuAns;
            List<DuAn> listDuAnByQuanLy = allDuAns;

            if (strIdDanhMucNhaThau != null)
            {
                var listIdDanhMucNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(strIdDanhMucNhaThau);
                var lstAllDanhMuc = _danhMucNhaThauService.GetAll();
                var lstTenNhaThau = lstAllDanhMuc.Where(x => listIdDanhMucNhaThau.Contains(x.IdNhaThau)).Select(x => x.TenNhaThau.ToLower()).ToList();
                var lstIDNhaThau = lstAllDanhMuc.Where(x => lstTenNhaThau.Contains(x.TenNhaThau.ToLower())).Select(x => x.IdNhaThau);
                var lstFilterNhaThau = _danhMucNhaThauService.GetAll().Where(x => lstIDNhaThau.Contains(x.IdNhaThau)).Select(x => x.IdDuAn).ToList();
                listDuAnByDanhMucNhaThau = _duAnService.GetAll().Where(x => lstFilterNhaThau.Contains(x.IdDuAn)).ToList();
            }

            if (strIdTinhTrangDuAn != null)
            {
                var listIdTinhTrangDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdTinhTrangDuAn);
                listDuAnByTinhTrangDuAn = _duAnService.Filter(x => listIdTinhTrangDuAn.Any(y => y == x.IdTinhTrangDuAn)).ToList();
            }
            if (strIdNhomDuAn != null)
            {
                var listIdNhomDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdNhomDuAn);
                listDuAnByNhomDuAn = _duAnService.Filter(x => listIdNhomDuAn.Any(y => y == x.IdNhomDuAn)).ToList();
            }
            if (strIdLinhVucNganhNghe != null)
            {
                var listIdLinhVucNganhNghe = new JavaScriptSerializer().Deserialize<List<int>>(strIdLinhVucNganhNghe);
                listDuAnByLinhVucNganhNghe = _duAnService.Filter(x => listIdLinhVucNganhNghe.Any(y => y == x.IdLinhVucNganhNghe)).ToList();
            }
            if (strIdChuDauTu != null)
            {
                var listIdChuDauTu = new JavaScriptSerializer().Deserialize<List<int>>(strIdChuDauTu);
                listDuAnByChuDauTu = _duAnService.Filter(x => listIdChuDauTu.Any(y => y == x.IdChuDauTu)).ToList();
            }

            if (strIdNguonVon != null)
            {
                var listIdNguonVon = new JavaScriptSerializer().Deserialize<List<int>>(strIdNguonVon);
                listDuAnByNguonVon = _duAnService.GetAllNguonVonDuAn().Where(x => (x.NguonVonDuAns.Where(x1 => listIdNguonVon.Any(y => y == x1.IdNguonVon)).Count() > 0)).ToList();
            }
            if (strIdQuanLy != null)
            {
                var listIdQuanLy = new JavaScriptSerializer().Deserialize<List<int>>(strIdQuanLy);
                listDuAnByQuanLy = _duAnService.GetAllQuanLy().Where(x => listIdQuanLy.All(x.DuAnQuanLys.Select(x1 => x1.IdQuanLyDA).Contains)).ToList();
            }
            var duans = allDuAns.Join(
                listDuAnByTinhTrangDuAn,
                a => a.IdDuAn,
                b => b.IdDuAn,
                (a, b) => new
                {
                    a
                }).Join(listDuAnByNhomDuAn, ab => ab.a.IdDuAn, c => c.IdDuAn, (ab, c) => new
                {
                    c
                }).Join(listDuAnByChuDauTu, abc => abc.c.IdDuAn, d => d.IdDuAn, (abc, d) => new
                {
                    d
                }).Join(listDuAnByNguonVon, abcd => abcd.d.IdDuAn, e => e.IdDuAn, (abcd, e) => new
                {
                    e
                }).Join(listDuAnByDanhMucNhaThau, abcde => abcde.e.IdDuAn, f => f.IdDuAn, (abcde, f) => new
                {
                    f
                }).Join(listDuAnByQuanLy, abcdef => abcdef.f.IdDuAn, g => g.IdDuAn, (abcdef, g) => new
                {
                    g
                })
                .Join(listDuAnByLinhVucNganhNghe, abcdefg => abcdefg.g.IdDuAn, h => h.IdDuAn, (abcdefg, h) => new DuAn
                {
                    IdDuAn = h.IdDuAn,
                    IdDuAnCha = h.IdDuAnCha,
                    Ma = h.Ma,
                    TenDuAn = h.TenDuAn,
                    ThoiGianThucHien = h.ThoiGianThucHien,
                    NhomDuAn = h.NhomDuAn,
                    GiaiDoanDuAn = h.GiaiDoanDuAn,
                }).ToList();

            var modelData = _duAnService.FilterByNameDuAn(tenDA, duans).Select(x => new
            {
                IdDuAn = x.IdDuAn,
                IdDuAnCha = x.IdDuAnCha,
                Ma = x.Ma,
                TenDuAn = x.TenDuAn,
                ThoiGianThucHien = x.ThoiGianThucHien,
                TenNhomDuAn = (x.NhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",
                TenGiaiDoanDuAn = (x.GiaiDoanDuAn != null) ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
            });

            response = request.CreateResponse(HttpStatusCode.OK, modelData);

            return response;
        }

        [HttpGet]
        [Route("filternew")]
        public HttpResponseMessage FilterNew(HttpRequestMessage request, bool isTHDT, string tenDA, string strIdDanhMucNhaThau, string strIdTinhTrangDuAn,
            string strIdNhomDuAn, string strIdChuDauTu, string strIdLinhVucNganhNghe, string strIdNguonVon, string strIdQuanLy, string strIdLoaiGoiThau, string tuNgay, string denNgay)
        {
            HttpResponseMessage response = null;

            int? idNhomDuAnTheoUser = null;
            string idUser = "";
            if (!User.IsInRole("Admin"))
            {
                idUser = User.Identity.GetUserId();
                if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
            }

            List<DuAn> allDuAns = new List<DuAn>();

            //Lấy dự án theo role
            if (User.IsInRole("Admin"))
            {
                allDuAns = _duAnService.GetForLocDuAn().ToList();
            }
            else
            {
                allDuAns = _duAnService.GetForLocDuAn().Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).ToList();
            }

            List<DuAn> listDuAnByTinhTrangDuAn = allDuAns;
            List<DuAn> listDuAnByNhomDuAn = allDuAns;
            List<DuAn> listDuAnByLinhVucNganhNghe = allDuAns;
            List<DuAn> listDuAnByChuDauTu = allDuAns;
            List<DuAn> listDuAnByNguonVon = allDuAns;
            List<DuAn> listDuAnByDanhMucNhaThau = allDuAns;
            List<DuAn> listDuAnByQuanLy = allDuAns;

            List<DuAn> listDuAnByHopDong = allDuAns;
            List<DuAn> listDuAnByLoaiGoiThau = allDuAns;


            
            if (tuNgay != null || denNgay != null)
            {
                DateTime? t = null;
                DateTime? d = null;
                if (tuNgay != null && tuNgay != "" && tuNgay != "null")
                    t = DateTime.ParseExact(tuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                if (denNgay != null && denNgay != "" && denNgay != "null")
                    d = DateTime.ParseExact(denNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));

                List<int> idHopDongs = _hopDongService.GetForLocBaoCao(t, d).ToList();
                listDuAnByHopDong = allDuAns.Where(x => idHopDongs.Contains(x.IdDuAn)).ToList();
            }
            if (strIdLoaiGoiThau != null && strIdLoaiGoiThau != "")
            {
                var lstSerializer = new JavaScriptSerializer().Deserialize<List<int>>(strIdLoaiGoiThau);
                List<int> idGoiThaus = _goiThauService.GetForLocBaoCao(lstSerializer).Distinct().ToList();
                listDuAnByLoaiGoiThau = allDuAns.Where(x => idGoiThaus.Contains(x.IdDuAn)).ToList();
            }

            if (strIdDanhMucNhaThau != null)
            {
                var listIdDanhMucNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(strIdDanhMucNhaThau);
                var lstDuAnTheoNhaThau = _danhMucNhaThauService.GetByListIDNhaThau(listIdDanhMucNhaThau).Select(x => x.IdDuAn).ToList();
                listDuAnByDanhMucNhaThau = allDuAns.Where(x => lstDuAnTheoNhaThau.Contains(x.IdDuAn)).ToList();
            }

            if (strIdTinhTrangDuAn != null)
            {
                var listIdTinhTrangDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdTinhTrangDuAn);
                listDuAnByTinhTrangDuAn = allDuAns.Where(x => listIdTinhTrangDuAn.Any(y => y == x.IdTinhTrangDuAn)).ToList();
            }
            if (strIdNhomDuAn != null)
            {
                var listIdNhomDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdNhomDuAn);
                listDuAnByNhomDuAn = allDuAns.Where(x => listIdNhomDuAn.Any(y => y == x.IdNhomDuAn)).ToList();
            }
            if (strIdLinhVucNganhNghe != null)
            {
                var listIdLinhVucNganhNghe = new JavaScriptSerializer().Deserialize<List<int>>(strIdLinhVucNganhNghe);
                listDuAnByLinhVucNganhNghe = allDuAns.Where(x => listIdLinhVucNganhNghe.Any(y => y == x.IdLinhVucNganhNghe)).ToList();
            }
            if (strIdChuDauTu != null)
            {
                var listIdChuDauTu = new JavaScriptSerializer().Deserialize<List<int>>(strIdChuDauTu);
                listDuAnByChuDauTu = allDuAns.Where(x => listIdChuDauTu.Any(y => y == x.IdChuDauTu)).ToList();
            }

            if (strIdNguonVon != null)
            {
                var listIdNguonVon = new JavaScriptSerializer().Deserialize<List<int>>(strIdNguonVon);
                listDuAnByNguonVon = allDuAns.Where(x => (x.NguonVonDuAns.Where(x1 => listIdNguonVon.Any(y => y == x1.IdNguonVon)).Count() > 0)).ToList();
            }
            if (strIdQuanLy != null)
            {
                var listIdQuanLy = new JavaScriptSerializer().Deserialize<List<int>>(strIdQuanLy);
                listDuAnByQuanLy = _duAnService.GetAllQuanLy().Where(x => listIdQuanLy.All(x.DuAnQuanLys.Select(x1 => x1.IdQuanLyDA).Contains)).ToList();
            }
            if (isTHDT)
            {
                //var listIdTHDT = _QLTD_CTCV_ThucHienDuAnService.
            }
            var duans = allDuAns.Join(
                listDuAnByTinhTrangDuAn,
                a => a.IdDuAn,
                b => b.IdDuAn,
                (a, b) => new
                {
                    a
                }).Join(listDuAnByNhomDuAn, ab => ab.a.IdDuAn, c => c.IdDuAn, (ab, c) => new
                {
                    c
                }).Join(listDuAnByChuDauTu, abc => abc.c.IdDuAn, d => d.IdDuAn, (abc, d) => new
                {
                    d
                }).Join(listDuAnByNguonVon, abcd => abcd.d.IdDuAn, e => e.IdDuAn, (abcd, e) => new
                {
                    e
                }).Join(listDuAnByDanhMucNhaThau, abcde => abcde.e.IdDuAn, f => f.IdDuAn, (abcde, f) => new
                {
                    f
                }).Join(listDuAnByQuanLy, abcdef => abcdef.f.IdDuAn, g => g.IdDuAn, (abcdef, g) => new
                {
                    g
                })
                .Join(listDuAnByLinhVucNganhNghe, abcdefg => abcdefg.g.IdDuAn, h => h.IdDuAn, (abcdefg, h) => new
                {
                    h
                })
                .Join(listDuAnByLoaiGoiThau, abcdefgh => abcdefgh.h.IdDuAn, i => i.IdDuAn, (abcdefgh, i) => new 
                { 
                    i   
                })
                .Join(listDuAnByHopDong, abcdefghi => abcdefghi.i.IdDuAn, j => j.IdDuAn, (abcdefghi, j) => new DuAn
                {
                    IdDuAn = j.IdDuAn,
                    IdDuAnCha = j.IdDuAnCha,
                    Ma = j.Ma,
                    TenDuAn = j.TenDuAn,
                    ThoiGianThucHien = j.ThoiGianThucHien,
                    NhomDuAn = j.NhomDuAn,
                    GiaiDoanDuAn = j.GiaiDoanDuAn,
                }).ToList();
            
            var modelData = _duAnService.FilterByNameDuAn(tenDA, duans).Select(x => new
            {
                IdDuAn = x.IdDuAn,
                IdDuAnCha = x.IdDuAnCha,
                Ma = x.Ma,
                TenDuAn = x.TenDuAn,
                ThoiGianThucHien = x.ThoiGianThucHien,
                TenNhomDuAn = (x.NhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",
                TenGiaiDoanDuAn = (x.GiaiDoanDuAn != null) ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
            });

            response = request.CreateResponse(HttpStatusCode.OK, modelData);

            return response;
        }

        [HttpGet]
        [Route("filterduannew")]
        public HttpResponseMessage FilterDuAnNew(HttpRequestMessage request, bool isTHDT, string tenDA, string strIdDanhMucNhaThau, string strIdTinhTrangDuAn,
            string strIdNhomDuAn, string strIdChuDauTu, string strIdLinhVucNganhNghe, string strIdNguonVon, string strIdQuanLy, string strIdLoaiGoiThau, string strIdGiaiDoan, string strIdPhongBan, string tuNgay, string denNgay)
        {
            HttpResponseMessage response = null;

            int? idNhomDuAnTheoUser = null;
            string idUser = "";
            if (!User.IsInRole("Admin"))
            {
                idUser = User.Identity.GetUserId();
                if (((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value != "")
                {
                    idNhomDuAnTheoUser = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("IdNhomDuAnTheoUser").Value);
                }
            }

            List<DuAn> allDuAns = new List<DuAn>();

            if (User.IsInRole("Admin"))
            {
                allDuAns = _duAnService.GetForLocDuAn().ToList();
            }
            else
            {
                allDuAns = _duAnService.GetForLocDuAn().Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).ToList();
            }

            List<DuAn> listDuAnByTinhTrangDuAn = allDuAns;
            List<DuAn> listDuAnByNhomDuAn = allDuAns;
            List<DuAn> listDuAnByLinhVucNganhNghe = allDuAns;
            List<DuAn> listDuAnByChuDauTu = allDuAns;
            List<DuAn> listDuAnByNguonVon = allDuAns;
            List<DuAn> listDuAnByDanhMucNhaThau = allDuAns;
            List<DuAn> listDuAnByQuanLy = allDuAns;

            List<DuAn> listDuAnByHopDong = allDuAns;
            List<DuAn> listDuAnByLoaiGoiThau = allDuAns;

            List<DuAn> listDuAnByGiaiDoan = allDuAns;
            List<DuAn> listDuAnByPhongBan = allDuAns;

            if (tuNgay != null || denNgay != null)
            {
                DateTime? t = null;
                DateTime? d = null;
                if (tuNgay != null && tuNgay != "" && tuNgay != "null")
                    t = DateTime.ParseExact(tuNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                if (denNgay != null && denNgay != "" && denNgay != "null")
                    d = DateTime.ParseExact(denNgay, "dd/MM/yyyy", new CultureInfo("vi-VN"));

                List<int> idHopDongs = _hopDongService.GetForLocBaoCao(t, d).ToList();
                listDuAnByHopDong = allDuAns.Where(x => idHopDongs.Contains(x.IdDuAn)).ToList();
            }
            if (strIdLoaiGoiThau != null && strIdLoaiGoiThau != "")
            {
                var lstSerializer = new JavaScriptSerializer().Deserialize<List<int>>(strIdLoaiGoiThau);
                List<int> idGoiThaus = _goiThauService.GetForLocBaoCao(lstSerializer).Distinct().ToList();
                listDuAnByLoaiGoiThau = allDuAns.Where(x => idGoiThaus.Contains(x.IdDuAn)).ToList();
            }

            if (strIdDanhMucNhaThau != null)
            {
                var listIdDanhMucNhaThau = new JavaScriptSerializer().Deserialize<List<int>>(strIdDanhMucNhaThau);
                var lstDuAnTheoNhaThau = _danhMucNhaThauService.GetByListIDNhaThau(listIdDanhMucNhaThau).Select(x => x.IdDuAn).ToList();
                listDuAnByDanhMucNhaThau = allDuAns.Where(x => lstDuAnTheoNhaThau.Contains(x.IdDuAn)).ToList();
            }

            if (strIdTinhTrangDuAn != null)
            {
                var listIdTinhTrangDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdTinhTrangDuAn);
                listDuAnByTinhTrangDuAn = allDuAns.Where(x => listIdTinhTrangDuAn.Any(y => y == x.IdTinhTrangDuAn)).ToList();
            }
            if (strIdNhomDuAn != null)
            {
                var listIdNhomDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdNhomDuAn);
                listDuAnByNhomDuAn = allDuAns.Where(x => listIdNhomDuAn.Any(y => y == x.IdNhomDuAn)).ToList();
            }
            if (strIdLinhVucNganhNghe != null)
            {
                var listIdLinhVucNganhNghe = new JavaScriptSerializer().Deserialize<List<int>>(strIdLinhVucNganhNghe);
                listDuAnByLinhVucNganhNghe = allDuAns.Where(x => listIdLinhVucNganhNghe.Any(y => y == x.IdLinhVucNganhNghe)).ToList();
            }
            if (strIdChuDauTu != null)
            {
                var listIdChuDauTu = new JavaScriptSerializer().Deserialize<List<int>>(strIdChuDauTu);
                listDuAnByChuDauTu = allDuAns.Where(x => listIdChuDauTu.Any(y => y == x.IdChuDauTu)).ToList();
            }

            if (strIdNguonVon != null)
            {
                var listIdNguonVon = new JavaScriptSerializer().Deserialize<List<int>>(strIdNguonVon);
                listDuAnByNguonVon = allDuAns.Where(x => (x.NguonVonDuAns.Where(x1 => listIdNguonVon.Any(y => y == x1.IdNguonVon)).Count() > 0)).ToList();
            }
            if (strIdQuanLy != null)
            {
                var listIdQuanLy = new JavaScriptSerializer().Deserialize<List<int>>(strIdQuanLy);
                listDuAnByQuanLy = _duAnService.GetAllQuanLy().Where(x => listIdQuanLy.All(x.DuAnQuanLys.Select(x1 => x1.IdQuanLyDA).Contains)).ToList();
            }
            if (strIdGiaiDoan != null)
            {
                var listIdGiaiDoan = new JavaScriptSerializer().Deserialize<List<int>>(strIdGiaiDoan);
                listDuAnByGiaiDoan = allDuAns.Where(x => listIdGiaiDoan.Any(y => y == x.IdGiaiDoanDuAn)).ToList();
            }
            if (strIdPhongBan != null)
            {
                var listIdPhongBan = new JavaScriptSerializer().Deserialize<List<int>>(strIdPhongBan);
                listDuAnByPhongBan = allDuAns.Where(x => listIdPhongBan.Any(y => y == x.IdNhomDuAn)).ToList();
            }
            if (isTHDT)
            {
                //var listIdTHDT = _QLTD_CTCV_ThucHienDuAnService.
            }
            var duans = allDuAns.Join(
                listDuAnByTinhTrangDuAn,
                a => a.IdDuAn,
                b => b.IdDuAn,
                (a, b) => new
                {
                    a
                }).Join(listDuAnByNhomDuAn, ab => ab.a.IdDuAn, c => c.IdDuAn, (ab, c) => new
                {
                    c
                }).Join(listDuAnByChuDauTu, abc => abc.c.IdDuAn, d => d.IdDuAn, (abc, d) => new
                {
                    d
                }).Join(listDuAnByNguonVon, abcd => abcd.d.IdDuAn, e => e.IdDuAn, (abcd, e) => new
                {
                    e
                }).Join(listDuAnByDanhMucNhaThau, abcde => abcde.e.IdDuAn, f => f.IdDuAn, (abcde, f) => new
                {
                    f
                }).Join(listDuAnByQuanLy, abcdef => abcdef.f.IdDuAn, g => g.IdDuAn, (abcdef, g) => new
                {
                    g
                })
                .Join(listDuAnByLinhVucNganhNghe, abcdefg => abcdefg.g.IdDuAn, h => h.IdDuAn, (abcdefg, h) => new
                {
                    h
                })
                .Join(listDuAnByLoaiGoiThau, abcdefgh => abcdefgh.h.IdDuAn, i => i.IdDuAn, (abcdefgh, i) => new
                {
                    i
                })
                .Join(listDuAnByGiaiDoan, abcdefghi => abcdefghi.i.IdDuAn, j => j.IdDuAn, (abcdefghi, j) => new
                {
                    j
                })
                .Join(listDuAnByPhongBan, abcdefghij => abcdefghij.j.IdDuAn, k=> k.IdDuAn, (abcdefghij, k) => new { 
                    k
                })
                .Join(listDuAnByHopDong, abcdefghijk => abcdefghijk.k.IdDuAn, l => l.IdDuAn, (abcdefghijk, l) => new DuAn
                {
                    IdDuAn = l.IdDuAn,
                    IdDuAnCha = l.IdDuAnCha,
                    Ma = l.Ma,
                    TenDuAn = l.TenDuAn,
                    ThoiGianThucHien = l.ThoiGianThucHien,
                    NhomDuAn = l.NhomDuAn,
                    GiaiDoanDuAn = l.GiaiDoanDuAn,                    
                }).ToList();

            var modelData = _duAnService.FilterByNameDuAn(tenDA, duans).Select(x => new
            {
                IdDuAn = x.IdDuAn,
                IdDuAnCha = x.IdDuAnCha,
                Ma = x.Ma,
                TenDuAn = x.TenDuAn,
                ThoiGianThucHien = x.ThoiGianThucHien,
                TenNhomDuAn = (x.NhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",
                TenGiaiDoanDuAn = (x.GiaiDoanDuAn != null) ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
                TenPhongBan = (x.IdNhomDuAn != null) ? x.NhomDuAn.TenNhomDuAn : "",
            });

            response = request.CreateResponse(HttpStatusCode.OK, modelData);

            return response;
        }

        ////SignalR
        //[Route("getTopMyDuAn")]
        //[HttpGet]
        //public HttpResponseMessage GetTopMyDuAn(HttpRequestMessage request)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        int totalRow = 0;
        //        List<DuAn> model = _duAnService.ListAllUnread(User.Identity.GetUserId(), 1, 10, out totalRow);
        //        IEnumerable<DuAnViewModels> modelVm = Mapper.Map<List<DuAn>, List<DuAnViewModels>>(model);

        //        PaginationSet<DuAnViewModels> pagedSet = new PaginationSet<DuAnViewModels>()
        //        {
        //            PageIndex = 1,
        //            TotalRows = totalRow,
        //            PageSize = 10,
        //            Items = modelVm
        //        };
        //        response = request.CreateResponse(HttpStatusCode.OK, modelVm);

        //        return response;
        //    });
        //}

        //[HttpGet]
        //[Route("markDAAsRead")]
        //public HttpResponseMessage MarkAsRead(HttpRequestMessage request, int IdDuAn)
        //{
        //    try
        //    {
        //        _duAnService.MarkAsRead(User.Identity.GetUserId(), IdDuAn);
        //        _duAnService.Save();
        //        return request.CreateResponse(HttpStatusCode.OK, IdDuAn);
        //    }
        //    catch (Exception ex)
        //    {
        //        return request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
        //    }
        //}

        [Route("updateDuAnCayThuMuc")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage UpdateDuAnCayThuMuc(HttpRequestMessage request, DuAnViewModels duAntVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listDuAn = _duAnService.GetAll();
                    var idDuAns = listDuAn.Select(x => x.IdDuAn).ToList();

                    foreach (var idDuan in idDuAns)
                    {
                        _thumucService.AddThuMucWhenDuAnCreated(idDuan);
                    }

                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getallCount")]
        [HttpGet]
        public HttpResponseMessage GetAllCount(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int? idNhomDuAnTheoUser = null;
                string idUser = "";
                if (!User.IsInRole("Admin"))
                {
                    idUser = User.Identity.GetUserId();
                }
                var model = _duAnService.GetListDuAns(idNhomDuAnTheoUser, idUser);
                var count = model.Count();
                response = request.CreateResponse(HttpStatusCode.OK, count);
                return response;
            });
        }
    }
}