﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/nguonvonduan")]
    [Authorize]
    public class NguonVonDuAnController : ApiControllerBase
    {
        private INguonVonDuAnService _nguonVonDuAnService;
        public NguonVonDuAnController(IErrorService errorService, INguonVonDuAnService nguonVonDuAnService) : base(errorService)
        {
            _nguonVonDuAnService = nguonVonDuAnService;
        }
        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nguonVonDuAnService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<NguonVonDuAn>, IEnumerable<NguonVonDuAnViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nguonVonDuAnService.GetById(id);

                var responseData = Mapper.Map<NguonVonDuAn, NguonVonViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDuan)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nguonVonDuAnService.GetAll().Where(x => x.IdDuAn == idDuan);
                var modelVm = Mapper.Map<IEnumerable<NguonVonDuAn>, IEnumerable<NguonVonDuAnViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "DUAN")]
        public HttpResponseMessage GetByDuAn(HttpRequestMessage request, int idDuan)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _nguonVonDuAnService.GetByDuAn(idDuan);
                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "DUAN")]
        public HttpResponseMessage Create(HttpRequestMessage request, NguonVonDuAnViewModels nguonVonDuAnViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newNguonVonDuAn = new NguonVonDuAn();
                    newNguonVonDuAn.UpdateNguonVonDuAn(nguonVonDuAnViewModels);
                    newNguonVonDuAn.CreatedDate = DateTime.Now;
                    newNguonVonDuAn.CreatedBy = User.Identity.GetUserId();
                    _nguonVonDuAnService.Add(newNguonVonDuAn);
                    _nguonVonDuAnService.Save();

                    var responseData = Mapper.Map<NguonVonDuAn, NguonVonDuAnViewModels>(newNguonVonDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "DUAN")]
        public HttpResponseMessage Update(HttpRequestMessage request, NguonVonDuAnViewModels nguonVonDuAnViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbNguonVonDuAn = _nguonVonDuAnService.GetById(nguonVonDuAnViewModels.IdNguonVonDuAn);

                    dbNguonVonDuAn.UpdateNguonVonDuAn(nguonVonDuAnViewModels);
                    dbNguonVonDuAn.UpdatedDate = DateTime.Now;
                    dbNguonVonDuAn.UpdatedBy = User.Identity.GetUserId();
                    _nguonVonDuAnService.Update(dbNguonVonDuAn);
                    _nguonVonDuAnService.Save();

                    var responseData = Mapper.Map<NguonVonDuAn, NguonVonViewModels>(dbNguonVonDuAn);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nguonVonDuAnService.Delete(id);
                    _nguonVonDuAnService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "DUAN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listNguonVonDuAn = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listNguonVonDuAn)
                    {
                        _nguonVonDuAnService.Delete(item);
                    }

                    _nguonVonDuAnService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listNguonVonDuAn.Count);
                }
                return response;
            });
        }
    }
}
