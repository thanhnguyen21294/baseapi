﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
using WebAPI.Infrastructure.Core;
using Model.Models;
using WebAPI.Models;
using AutoMapper;
using WebAPI.Infrastructure.Extensions;
using Antlr.Runtime.Misc;
using WebAPI.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WebAPI.SignalR;
using System.IO;
using System.Web;
using Microsoft.Office.Interop.Word;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/vanbanden")]
    public class VanBanDenController : ApiControllerBase
    {
        public IVanBanDenService _vanBanDenService;
        public IVanBanDenUserService _vanBanDenUserService;
        public VanBanDenController(IErrorService errorService, IVanBanDenService vbdaService,
            IVanBanDenUserService vanBanDenUserService) : base(errorService)
        {
            _vanBanDenService = vbdaService;
            _vanBanDenUserService = vanBanDenUserService;
        }

        [Route("getvbbytm/{idTM}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "VANBANDEN")]
        public HttpResponseMessage GetVBByTMId(HttpRequestMessage request, int idTM, string filter, int page, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                    DateTime dateNow = DateTime.Now.Date;
                    string uID = User.Identity.GetUserId();
                    List<VanBanDen> lst = _vanBanDenService.GetVBByTM(idTM, filter, uID).ToList();
                    int soVBHoanThanh = lst.Where(x => x.TrangThai == 2).ToList().Count();
                    var vbChuaXl = lst.Where(x => x.TrangThai != 2).ToList();
                    int soVBChuaQuaHan = 0;
                    int soVBQuaHan = vbChuaXl.Count();// vbChuaXl.Where(x => Convert.ToDateTime(x.ThoiHanHoanThanh).Date < dateNow == true).ToList().Count();

                    QLDAHub.PushVanBanDenCham(soVBQuaHan, null);

                    int totalRow = lst.Count;
                    List<VanBanDen> vbds = lst.OrderBy(x => x.IdVanBanDen).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                    IEnumerable<VanBanDenViewModels> vbdaVMs = Mapper.Map<IEnumerable<VanBanDen>, IEnumerable<VanBanDenViewModels>>(vbds);
                    PaginationSet<VanBanDenViewModels> pagedSet = new PaginationSet<VanBanDenViewModels>()
                    {
                        PageIndex = page,
                        TotalRows = totalRow,
                        PageSize = pageSize,
                        Items = vbdaVMs
                    };

                    response = request.CreateResponse(HttpStatusCode.OK, new
                    {
                        thongke = new
                        {
                            soVBHoanThanh = soVBHoanThanh,
                            soVBChuaQuaHan = soVBChuaQuaHan,
                            soVBQuaHan = soVBQuaHan
                        },
                        data = pagedSet
                    });
                    return response;
            });
        
        }

        [Route("getallforhistory")]
        [HttpGet]
        public HttpResponseMessage GetAllForHistory(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                //IEnumerable<VanBanDen> lst = _vanBanDenService.GetAllForHistory();
                //var responseData = Mapper.Map<IEnumerable<VanBanDen>, IEnumerable<VanBanDenViewModels>>(lst);
                //response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });

        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "VANBANDEN")]
        public HttpResponseMessage Add(HttpRequestMessage request, VanBanDenPostPutViewModels vbdaVM)
        {
            return CreateHttpResponse(request, () =>
            {
                VanBanDenViewModels vm = new VanBanDenViewModels();
                vm.IdVanBanDen = vbdaVM.IdVanBanDen;
                vm.SoVanBanDen = vbdaVM.SoVanBanDen;
                vm.TenVanBanDen = vbdaVM.TenVanBanDen;
                vm.FileName = vbdaVM.FileName;
                vm.FileUrl = vbdaVM.FileUrl;
                vm.NgayDen = vbdaVM.NgayDen;
                vm.NoiGui = vbdaVM.NoiGui;
                vm.MoTa = vbdaVM.MoTa;
                vm.NgayNhan = vbdaVM.NgayNhan;
                vm.TrangThai = vbdaVM.TrangThai;
                vm.ThoiHanHoanThanh = vbdaVM.ThoiHanHoanThanh;
                vm.DeXuatTPTH = vbdaVM.DeXuatTPTH;
                vm.YKienChiDaoGiamDoc = vbdaVM.YKienChiDaoGiamDoc;
                vm.TPYeuCau = vbdaVM.TPYeuCau;
                vm.GhiChu = vbdaVM.GhiChu;
                vm.IdThuMucVanBanDen = vbdaVM.IdThuMucVanBanDen;
                vm.ThuMucVanBanDen = vbdaVM.ThuMucVanBanDen;
                vm.VanBanDenUsers = vbdaVM.VanBanDenUsers;

                HttpResponseMessage response = null;
                VanBanDen vbda = new VanBanDen();
                vbda.UpdateVanBanDen(vm);
                vbda.Status = true;

                vbda = _vanBanDenService.Add(vbda);
                _vanBanDenService.Save();

                XuLyVanBanDenUser(vbda.IdVanBanDen, vbdaVM.DanhSachUser);

                VanBanDenViewModels vbdaReturn = Mapper.Map<VanBanDen, VanBanDenViewModels>(vbda);
                response = request.CreateResponse(HttpStatusCode.Created, vbdaReturn);
                return response;

            });
        }
        [Route("update")]
        [HttpPost]
        [Permission(Action = "Update", Function = "VANBANDEN")]
        public HttpResponseMessage Update(HttpRequestMessage request, VanBanDenPostPutViewModels vbdaVM)
        {
            return CreateHttpResponse(request, () =>
            {
                VanBanDenViewModels vm = new VanBanDenViewModels();
                vm.IdVanBanDen = vbdaVM.IdVanBanDen;
                vm.SoVanBanDen = vbdaVM.SoVanBanDen;
                vm.TenVanBanDen = vbdaVM.TenVanBanDen;
                vm.FileName = vbdaVM.FileName;
                vm.FileUrl = vbdaVM.FileUrl;
                vm.NgayDen = vbdaVM.NgayDen;
                vm.NoiGui = vbdaVM.NoiGui;
                vm.MoTa = vbdaVM.MoTa;
                vm.NgayNhan = vbdaVM.NgayNhan;
                vm.TrangThai = vbdaVM.TrangThai;
                vm.ThoiHanHoanThanh = vbdaVM.ThoiHanHoanThanh;
                vm.DeXuatTPTH = vbdaVM.DeXuatTPTH;
                vm.YKienChiDaoGiamDoc = vbdaVM.YKienChiDaoGiamDoc;
                vm.TPYeuCau = vbdaVM.TPYeuCau;
                vm.GhiChu = vbdaVM.GhiChu;
                vm.IdThuMucVanBanDen = vbdaVM.IdThuMucVanBanDen;
                vm.ThuMucVanBanDen = vbdaVM.ThuMucVanBanDen;
                vm.VanBanDenUsers = vbdaVM.VanBanDenUsers;

                HttpResponseMessage response = null;
                VanBanDen vbda = new VanBanDen();
                vbda.UpdateVanBanDen(vm);
                vbda.Status = false;
                _vanBanDenService.Update(vbda);
                _vanBanDenService.Save();

                XuLyVanBanDenUser(vbda.IdVanBanDen, vbdaVM.DanhSachUser);

                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        [HttpDelete]
        [Route("delete/")]
        [Permission(Action = "Delete", Function = "VANBANDEN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string arrayId)
        {

            var tempt = arrayId.Split(',');

            return CreateHttpResponse(request, () =>
             {
                 HttpResponseMessage response = null;
                 foreach (var item in tempt)
                 {
                     int id = Convert.ToInt16(item);
                     _vanBanDenService.Delete(id);
                     _vanBanDenService.Save();

                 }

                 response = request.CreateResponse(HttpStatusCode.OK);
                 return response;
             });
        }
        [Route("getbyid/{id}")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "VANABNDUAN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vanBanDenService.GetById(id);
                try
                {
                    var responseData = Mapper.Map<VanBanDen, VanBanDenViewModels>(model);

                    var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                    return response;
                }
                catch (Exception ex)
                {
                    return null;
                }
            });
        }


        [Route("ChuyenNhanVien")]
        [HttpPost]
        [Permission(Action = "Create", Function = "VANBANDEN")]
        public HttpResponseMessage ChuyenNhanVien(HttpRequestMessage request, VanBanDenPostPutViewModels vbdaVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                try
                {
                    XuLyVanBanDenUser(vbdaVM.IdVanBanDen, vbdaVM.DanhSachUser);
                    response = request.CreateResponse(HttpStatusCode.Created, "OK");
                }catch(Exception ex)
                {
                    response = request.CreateResponse(HttpStatusCode.Created, ex.ToString());
                }
                return response;
            });
        }


        private void XuLyVanBanDenUser(int idVBD, List<string> isUsers)
        {
            _vanBanDenUserService.DeleteByIdVanBan(idVBD);
            VanBanDenUser vbu_c = new VanBanDenUser();
            vbu_c.IdVanBanDen = idVBD;
            vbu_c.UserId = User.Identity.GetUserId();
            vbu_c.HasRead = true;
            _vanBanDenUserService.Add(vbu_c);
            if (isUsers != null && isUsers.Count() > 0)
            {
                foreach (var item in isUsers)
                {
                    VanBanDenUser vbu = new VanBanDenUser();
                    vbu.IdVanBanDen = idVBD;
                    vbu.UserId = item;
                    vbu.HasRead = true;
                    _vanBanDenUserService.Add(vbu);
                }
            }
            _vanBanDenUserService.Save();
        }

        [Route("XuatFile")]
        [HttpGet]
        public HttpResponseMessage XuatFile(HttpRequestMessage request, int idVB)
        {

            var model = _vanBanDenService.GetById(idVB);

            Microsoft.Office.Interop.Word.Application appWord = null;
            Microsoft.Office.Interop.Word.Document docWord = null;

            var folderReport = "/ReportFolder";
            string filePath = HttpContext.Current.Server.MapPath(folderReport);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string templateDocument = HttpContext.Current.Server.MapPath("~/Templates/PhieuXuLyVanBanDen.docx");
            string tempPathCopy = HttpContext.Current.Server.MapPath("~/Templates/PhieuXuLyVanBanDen_Copy" + string.Format("{0:yyyyMMddhhmmsss}.docx", DateTime.Now));
            System.IO.File.Copy(templateDocument, tempPathCopy);
            string documentName = string.Format("PhieuXuLyVanBanDen_{0:ddMMyyyyhhmmsss}.docx", DateTime.Now);
            string fullPath = Path.Combine(filePath, documentName);
            try
            {
                object _MissingValue = System.Reflection.Missing.Value;
                //Create an instance for word app  
                appWord = new Application();
                docWord = new Document();
                docWord = appWord.Documents.Open(tempPathCopy, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue,
                    _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue, _MissingValue);
                docWord.Activate();
                string ngayThangVanBan = "";
                if (model.NgayDen != null)
                {
                    ngayThangVanBan = model.NgayDen != null ? Convert.ToDateTime(model.NgayDen).ToString("dd/MM/yyyy") : "";
                }
                string ngayNhanVanBan = "";
                if (model.NgayNhan != null)
                {
                    ngayNhanVanBan = model.NgayNhan != null ? Convert.ToDateTime(model.NgayNhan).ToString("dd/MM/yyyy") : "";
                }
                string thoiHanHoanThanh = "";
                if (model.ThoiHanHoanThanh != null)
                {
                    thoiHanHoanThanh = model.ThoiHanHoanThanh != null ? Convert.ToDateTime(model.ThoiHanHoanThanh).ToString("dd/MM/yyyy") : "";
                }


                docWord.Tables[1].Cell(1, 2).Range.FormattedText.Text = model.TenVanBanDen;
                docWord.Tables[1].Cell(2, 2).Range.FormattedText.Text = model.SoVanBanDen;
                docWord.Tables[1].Cell(3, 2).Range.FormattedText.Text = model.NoiGui;
                docWord.Tables[1].Cell(4, 2).Range.FormattedText.Text = ngayThangVanBan;
                docWord.Tables[1].Cell(5, 2).Range.FormattedText.Text = ngayNhanVanBan;
                docWord.Tables[1].Cell(6, 2).Range.FormattedText.Text = model.MoTa;
                docWord.Tables[1].Cell(7, 2).Range.FormattedText.Text = thoiHanHoanThanh;

                docWord.Tables[2].Cell(2, 1).Range.FormattedText.Text = model.DeXuatTPTH;
                docWord.Tables[2].Cell(4, 1).Range.FormattedText.Text = model.YKienChiDaoGiamDoc;

                docWord.SaveAs2(fullPath, ref _MissingValue, ref _MissingValue, ref _MissingValue,
                      ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue,
                      ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue, ref _MissingValue);
            }
            catch (Exception ex)
            {
                string Error = ex.ToString();
            }
            finally
            {
                docWord?.Close();
                appWord?.Quit();
                if (System.IO.File.Exists(tempPathCopy))
                {
                    System.IO.File.Delete(tempPathCopy);
                }
            }
            if (!string.IsNullOrEmpty(documentName))
            {
                return request.CreateErrorResponse(HttpStatusCode.OK, folderReport + "/" + documentName);
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error Export");
            }
        }

        [Route("getvanbancham")]
        [HttpGet]
        [Permission(Action = "Read", Function = "VANBANDEN")]
        public HttpResponseMessage GetVanBanDenCham(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                DateTime dateNow = DateTime.Now.Date;
                HttpResponseMessage response = null;
                string uID = User.Identity.GetUserId();
                List<VanBanDen> lst = _vanBanDenService.GetVanBanCham(uID).ToList();
                var vbChuaXl = lst.Where(x => x.TrangThai != 2).ToList();
                int soVBQuaHan = vbChuaXl.Count();//.Where(x => Convert.ToDateTime(x.ThoiHanHoanThanh).Date < dateNow == true).ToList().Count();
                QLDAHub.PushVanBanDenCham(soVBQuaHan, null);
                response = request.CreateResponse(HttpStatusCode.OK, soVBQuaHan);
                return response;
            });
        }
    }
}
