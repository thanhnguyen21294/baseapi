﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using WebAPI.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/vanbandenthuchien")]
    [Authorize]
    public class VanBanDenThucHienController : ApiControllerBase
    {
        private IVanBanDenThucHienService _VanBanDenThucHienService;
        private IVanBanDenService _VanBanDenService;
        public VanBanDenThucHienController(IErrorService errorService, IVanBanDenThucHienService VanBanDenThucHienService, IVanBanDenService vanBanDenService) : base(errorService)
        {
            this._VanBanDenThucHienService = VanBanDenThucHienService;
            _VanBanDenService = vanBanDenService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<VanBanDenThucHien> model = _VanBanDenThucHienService.GetAll();
                IEnumerable<VanBanDenThucHienViewModes> modelVm = Mapper.Map<IEnumerable<VanBanDenThucHien>, IEnumerable<VanBanDenThucHienViewModes>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _VanBanDenThucHienService.GetByFilter(page, pageSize, out totalRow);

                var modelVm = Mapper.Map<IEnumerable<VanBanDenThucHien>, IEnumerable<VanBanDenThucHienViewModes>>(model);

                PaginationSet<VanBanDenThucHienViewModes> pagedSet = new PaginationSet<VanBanDenThucHienViewModes>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _VanBanDenThucHienService.GetById(id);

                var responseData = Mapper.Map<VanBanDenThucHien, VanBanDenThucHienViewModes>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyidvanban/{id}")]
        [HttpGet]
        public HttpResponseMessage GetByIdVanBan(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _VanBanDenThucHienService.GetByIdVanBan(id);

                var responseData = Mapper.Map<IEnumerable<VanBanDenThucHien>, IEnumerable< VanBanDenThucHienViewModes>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, VanBanDenThucHienViewModes VanBanDenThucHienViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newVanBanDenThucHien = new VanBanDenThucHien();
                    newVanBanDenThucHien.IdVanBanDen = VanBanDenThucHienViewModels.IdVanBanDen;
                    newVanBanDenThucHien.NoiDung = VanBanDenThucHienViewModels.NoiDung;
                    newVanBanDenThucHien.CreatedDate = DateTime.Now;
                    newVanBanDenThucHien.CreatedBy = User.Identity.GetUserId();
                    _VanBanDenThucHienService.Add(newVanBanDenThucHien);
                    _VanBanDenThucHienService.Save();

                    var responseData = Mapper.Map<VanBanDenThucHien, VanBanDenThucHienViewModes>(newVanBanDenThucHien);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, VanBanDenThucHienViewModes VanBanDenThucHienViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbVanBanDenThucHien = _VanBanDenThucHienService.GetById(VanBanDenThucHienViewModels.IdVanBanDenThucHien);

                    dbVanBanDenThucHien.NoiDung = VanBanDenThucHienViewModels.NoiDung;
                    dbVanBanDenThucHien.IdVanBanDen = VanBanDenThucHienViewModels.IdVanBanDen;
                    dbVanBanDenThucHien.UpdatedDate = DateTime.Now;
                    dbVanBanDenThucHien.UpdatedBy = User.Identity.GetUserId();
                    _VanBanDenThucHienService.Update(dbVanBanDenThucHien);
                    _VanBanDenThucHienService.Save();

                    var responseData = Mapper.Map<VanBanDenThucHien, VanBanDenThucHienViewModes>(dbVanBanDenThucHien);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _VanBanDenThucHienService.Delete(id);
                    _VanBanDenThucHienService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listVanBanDenThucHien = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listVanBanDenThucHien)
                    {
                        _VanBanDenThucHienService.Delete(item);
                    }

                    _VanBanDenThucHienService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listVanBanDenThucHien.Count);
                }

                return response;
            });
        }
    }
}
