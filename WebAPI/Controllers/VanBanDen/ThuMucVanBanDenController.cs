﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/thumucvanbanden")]
    [Authorize]
    public class ThuMucVanBanDenController : ApiControllerBase
    {
        private IThuMucVanBanDenService _ThuMucVanBanDenService;
        private IVanBanDenService _VanBanDenService;
        public ThuMucVanBanDenController(IErrorService errorService, IThuMucVanBanDenService ThuMucVanBanDenService, IVanBanDenService vanBanDenService) : base(errorService)
        {
            this._ThuMucVanBanDenService = ThuMucVanBanDenService;
            _VanBanDenService = vanBanDenService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                IEnumerable<ThuMucVanBanDen> model = _ThuMucVanBanDenService.GetAll();
                IEnumerable<ThuMucVanBanDenViewModels> modelVm = Mapper.Map<IEnumerable<ThuMucVanBanDen>, IEnumerable<ThuMucVanBanDenViewModels>>(model);
                string uID = User.Identity.GetUserId();
                foreach (var item in modelVm)
                {
                    item.SoLuongVB = _VanBanDenService.TotalVBByTM(item.IdThuMucVanBanDen, uID);
                }
                var responseData = modelVm.GroupBy(x => x.IdThuMucCha, (key, x) => new { IdThuMucCha = key, TMs = x.ToList() });
                response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _ThuMucVanBanDenService.GetByFilter(page, pageSize, out totalRow);

                var modelVm = Mapper.Map<IEnumerable<ThuMucVanBanDen>, IEnumerable<ThuMucVanBanDenViewModels>>(model);

                PaginationSet<ThuMucVanBanDenViewModels> pagedSet = new PaginationSet<ThuMucVanBanDenViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ThuMucVanBanDenService.GetById(id);

                var responseData = Mapper.Map<ThuMucVanBanDen, ThuMucVanBanDenViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage Create(HttpRequestMessage request, ThuMucVanBanDenViewModels ThuMucVanBanDenViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newThuMucVanBanDen = new ThuMucVanBanDen();
                    newThuMucVanBanDen.TenThuMucVanBanDen = ThuMucVanBanDenViewModels.TenThuMucVanBanDen;
                    newThuMucVanBanDen.IdThuMucCha = ThuMucVanBanDenViewModels.IdThuMucCha;
                    newThuMucVanBanDen.CreatedDate = DateTime.Now;
                    newThuMucVanBanDen.CreatedBy = User.Identity.GetUserId();
                    _ThuMucVanBanDenService.Add(newThuMucVanBanDen);
                    _ThuMucVanBanDenService.Save();

                    var responseData = Mapper.Map<ThuMucVanBanDen, ThuMucVanBanDenViewModels>(newThuMucVanBanDen);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThuMucVanBanDenViewModels ThuMucVanBanDenViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbThuMucVanBanDen = _ThuMucVanBanDenService.GetById(ThuMucVanBanDenViewModels.IdThuMucVanBanDen);

                    dbThuMucVanBanDen.TenThuMucVanBanDen = ThuMucVanBanDenViewModels.TenThuMucVanBanDen;
                    dbThuMucVanBanDen.IdThuMucCha = ThuMucVanBanDenViewModels.IdThuMucCha;
                    dbThuMucVanBanDen.UpdatedDate = DateTime.Now;
                    dbThuMucVanBanDen.UpdatedBy = User.Identity.GetUserId();
                    _ThuMucVanBanDenService.Update(dbThuMucVanBanDen);
                    _ThuMucVanBanDenService.Save();

                    var responseData = Mapper.Map<ThuMucVanBanDen, ThuMucVanBanDenViewModels>(dbThuMucVanBanDen);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _ThuMucVanBanDenService.Delete(id);
                    _ThuMucVanBanDenService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "THUMUCVANBANDEN")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listThuMucVanBanDen = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listThuMucVanBanDen)
                    {
                        _ThuMucVanBanDenService.Delete(item);
                    }

                    _ThuMucVanBanDenService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listThuMucVanBanDen.Count);
                }

                return response;
            });
        }
    }
}
