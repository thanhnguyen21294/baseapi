﻿using AutoMapper;
using Common.Exceptions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.System;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/appUser")]
    [Authorize]
    public class AppUserController : ApiControllerBase
    {
        private IAppRoleServices _AppRoleServices;
        private IAppUserService _AppUserService;
        private IDuAnService _duAnService;
        private IDuAnUserService _duAnUserService;
        public AppUserController(IErrorService errorService,
            IDuAnService duAnService,
            IAppRoleServices appRoleServices,
            IDuAnUserService duAnUserService,
            IAppUserService appUserService)
            : base(errorService)
        {
            this._duAnUserService = duAnUserService;
            _AppRoleServices = appRoleServices;
            _AppUserService = appUserService;
            this._duAnService = duAnService;
        }

        [Route("getlistpaging")]
        [HttpGet]
        //[Permission(Action = "Read", Function = "USER")]
        public async Task<HttpResponseMessage> GetListPaging(HttpRequestMessage request, int page, int pageSize, string filter = null)
        {
            HttpResponseMessage response;
            int totalRow;
            var query = AppUserManager.Users.Where(x => x.Delete == false).Include("NhomDuAnTheoUser");
            List<AppUserViewModel> appusernew = new List<AppUserViewModel>();
            List<object> l1 = query.ToList<object>();

            if (!string.IsNullOrEmpty(filter))
            {
                query = query.Where(x => x.UserName.Contains(filter) || x.FullName.Contains(filter));
            }

            foreach (var item in query)
            {
                var user = await AppUserManager.FindByIdAsync(item.Id);
                var roles = await AppUserManager.GetRolesAsync(item.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                applicationUserViewModel.Roles = roles;
                appusernew.Add(applicationUserViewModel);
            }
            totalRow = appusernew.Count();
            var model = appusernew.OrderBy(x => x.FullName).Skip((page - 1) * pageSize).Take(pageSize);

            PaginationSet<AppUserViewModel> pagedSet = new PaginationSet<AppUserViewModel>()
            {
                PageIndex = page,
                PageSize = pageSize,
                TotalRows = totalRow,
                Items = model,
            };

            response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

            return response;
        }

        [Route("getall")]
        [HttpGet]
        [Permission(Action = "Get", Function = "USER")]
        public HttpResponseMessage GetList(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response;
                var model = AppUserManager.Users.Where(x => x.Delete == false && x.Status == true);
                IEnumerable<AppUserViewModel> modelVm = Mapper.Map<IEnumerable<AppUser>, IEnumerable<AppUserViewModel>>(model).Where(x => x.Status).OrderBy(x => x.FullName);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getallbyidphongban")]
        [HttpGet]
        [Permission(Action = "Get", Function = "USER")]
        public async Task<HttpResponseMessage> GetListByIdPB(HttpRequestMessage request, int idPB)
        {
            HttpResponseMessage response;
            var query = AppUserManager.Users.Where(x => x.Delete == false).Include("NhomDuAnTheoUser");
            List<AppUserViewModel> appusernew = new List<AppUserViewModel>();

            foreach (var item in query)
            {
                var user = await AppUserManager.FindByIdAsync(item.Id);
                var roles = await AppUserManager.GetRolesAsync(item.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                applicationUserViewModel.Roles = roles;
                appusernew.Add(applicationUserViewModel);
            }
            if (!User.IsInRole("Giám đốc"))
            {
                var model = appusernew.Where(x => x.IdNhomDuAnTheoUser == idPB).OrderBy(x => x.Id);
                response = request.CreateResponse(HttpStatusCode.OK, model);
            }
            else
            {
                var model = appusernew.Where(x => !x.Roles.Contains("Admin")).OrderBy(x => x.Id);
                response = request.CreateResponse(HttpStatusCode.OK, model);
            }


            return response;

            //return CreateHttpResponse(request, () =>
            //{
            //    HttpResponseMessage response;
            //    var model = AppUserManager.Users.Where(x => x.Delete == false && x.Status == true && x.IdNhomDuAnTheoUser == idPB);
            //    IEnumerable<AppUserViewModel> modelVm = Mapper.Map<IEnumerable<AppUser>, IEnumerable<AppUserViewModel>>(model).Where(x => x.Status).OrderBy(x => x.FullName);

            //    response = request.CreateResponse(HttpStatusCode.OK, modelVm);

            //    return response;
            //});
        }

        [Route("getfilteruserbyidphongban")]
        [HttpGet]
        [Permission(Action = "Get", Function = "USER")]
        public async Task<HttpResponseMessage> GetFilterUserByIdPB(HttpRequestMessage request, int idPB, string filter = null)
        {
            HttpResponseMessage response;
            var query = AppUserManager.Users.Where(x => x.Delete == false).Include("NhomDuAnTheoUser");
            List<AppUserViewModel> appusernew = new List<AppUserViewModel>();

            foreach (var item in query)
            {
                var user = await AppUserManager.FindByIdAsync(item.Id);
                var roles = await AppUserManager.GetRolesAsync(item.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                applicationUserViewModel.Roles = roles;
                appusernew.Add(applicationUserViewModel);
            }
            if (!User.IsInRole("Giám đốc"))
            {
                var model = appusernew.Where(x => x.IdNhomDuAnTheoUser == idPB).OrderBy(x => x.Id);
                response = request.CreateResponse(HttpStatusCode.OK, model);
                if (filter != null)
                {
                    var model1 = appusernew.Where(x => x.FullName.ToLower().Contains(filter.ToLower())).OrderBy(x => x.Id);
                    response = request.CreateResponse(HttpStatusCode.OK, model1);
                }
            }
            else
            {
                var model = appusernew.Where(x => !x.Roles.Contains("Admin")).OrderBy(x => x.Id);
                response = request.CreateResponse(HttpStatusCode.OK, model);
                if (filter != null)
                {
                    var model2 = appusernew.Where(x => x.FullName.ToLower().Contains(filter.ToLower())).OrderBy(x => x.Id);
                    response = request.CreateResponse(HttpStatusCode.OK, model2);
                }
            }
            

            return response;
        }


        [Route("getallforvanbanden")]
        [HttpGet]
        [Permission(Action = "Get", Function = "USER")]
        public async Task<HttpResponseMessage> GetAllForVanBanDen(HttpRequestMessage request)
        {
            HttpResponseMessage response;
            //var Roles = AppRoleManager.Roles.Include(m => m.Users).ToList();// ToDictionary(x => x.I, x => x.Name);
            //using (var context = new ApplicationDbContext())
            //{
            //    //var roleStore = new RoleStore<IdentityRole>(context);
            //    //var roleManager = new RoleManager<IdentityRole>(roleStore);
            //    //var roles = (from r in roleManager.Roles select r).ToList();
            //    var userStore = new UserStore<ApplicationUser>(context);
            //    var userManager = new UserManager<ApplicationUser>(userStore);

            //    var users = (from u in userManager.Users select u).ToList();
            //}
           // var appUsers = _AppUserService.GetAllUser().ToList();
            var UserRoles = _AppRoleServices.GetUserRole().ToDictionary(x => x.UserId, x=> x.RoleName);
            try
            {
                var query = AppUserManager.Users.Where(x => x.Delete == false && x.IdNhomDuAnTheoUser != null && x.IdNhomDuAnTheoUser != 14).Include(x => x.NhomDuAnTheoUser).ToList().Select(x => new
                {
                    Id = x.Id,
                    IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                    FullName = x.FullName,
                    TenNhomDuAn = x.NhomDuAnTheoUser?.TenNhomDuAn,
                    Roles = UserRoles.ContainsKey(x.Id) ? UserRoles[x.Id] : ""
                });
                var lst = query.GroupBy(x => new { x.IdNhomDuAnTheoUser, x.TenNhomDuAn }).Select(x => new
                {
                    ID = x.Key.IdNhomDuAnTheoUser,
                    Ten = x.Key.TenNhomDuAn,
                    Users = x.Select(y => new
                    {
                        Id = y.Id,
                        IdNhomDuAnTheoUser = y.IdNhomDuAnTheoUser,
                        FullName = y.FullName,
                        TenNhomDuAn = y.TenNhomDuAn,
                        Roles = y.Roles
                    }).OrderByDescending(y=> y.Roles)
                }).OrderBy(x => x.ID).ToList();
                response = request.CreateResponse(HttpStatusCode.OK, lst);
                return response;
            }
            catch(Exception ex) {
                response = request.CreateResponse(HttpStatusCode.OK, ex.ToString());
                return response;
            }
        }

        [Route("getbyidphongban")]
        [HttpGet]
        [Permission(Action = "Get", Function = "USER")]
        public async Task<HttpResponseMessage> GetByIdPB(HttpRequestMessage request, int idPB)
        {
            HttpResponseMessage response;
            var query = AppUserManager.Users.Where(x => x.Delete == false).Include("NhomDuAnTheoUser");
            List<AppUserViewModel> appusernew = new List<AppUserViewModel>();

            foreach (var item in query)
            {
                var user = await AppUserManager.FindByIdAsync(item.Id);
                var roles = await AppUserManager.GetRolesAsync(item.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                applicationUserViewModel.Roles = roles;
                appusernew.Add(applicationUserViewModel);
            }
            var model = appusernew.Where(x => x.IdNhomDuAnTheoUser == idPB).OrderBy(x => x.Id);
            response = request.CreateResponse(HttpStatusCode.OK, model);



            return response;
        }

        [Route("detail/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "USER")]
        public async Task<HttpResponseMessage> Details(HttpRequestMessage request, string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, nameof(id) + " không có giá trị.");
            }
            var user = await AppUserManager.FindByIdAsync(id);
            if (user == null)
            {
                return request.CreateErrorResponse(HttpStatusCode.NoContent, "Không có dữ liệu");
            }
            else
            {
                var roles = await AppUserManager.GetRolesAsync(user.Id);
                var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(user);
                applicationUserViewModel.Roles = roles;
                return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
            }
        }

        [HttpPost]
        [Route("add")]

        [Permission(Action = "Create", Function = "USER")]
        public async Task<HttpResponseMessage> Create(HttpRequestMessage request, AppUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var newAppUser = new AppUser();
                newAppUser.UpdateUser(applicationUserViewModel);
                try
                {
                    newAppUser.Id = Guid.NewGuid().ToString();
                    var result = await AppUserManager.CreateAsync(newAppUser, applicationUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        var roles = applicationUserViewModel.Roles.ToArray();
                        await AppUserManager.AddToRolesAsync(newAppUser.Id, roles);

                        UpdateDuAnChoUserTP_PGD_GD(roles, newAppUser.Id, applicationUserViewModel.IdNhomDuAnTheoUser);

                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("update")]
        [Permission(Action = "Update", Function = "USER")]
        public async Task<HttpResponseMessage> Update(HttpRequestMessage request, AppUserViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                var appUser = await AppUserManager.FindByIdAsync(applicationUserViewModel.Id);
                try
                {
                    appUser.UpdateUser(applicationUserViewModel);
                    var result = await AppUserManager.UpdateAsync(appUser);
                    if (result.Succeeded)
                    {
                        var userRoles = await AppUserManager.GetRolesAsync(appUser.Id);
                        var selectedRole = applicationUserViewModel.Roles.ToArray();

                        selectedRole = selectedRole ?? new string[] { };

                        UpdateDuAnChoUserTP_PGD_GD(selectedRole, applicationUserViewModel.Id, applicationUserViewModel.IdNhomDuAnTheoUser);

                        await AppUserManager.RemoveFromRolesAsync(appUser.Id, userRoles.Except(selectedRole).ToArray());
                        await AppUserManager.AddToRolesAsync(appUser.Id, selectedRole.Except(userRoles).ToArray());
                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        private void UpdateDuAnChoUserTP_PGD_GD(string[] selectedRole, string idUser, int? IdNhomDuAnTheoUser)
        {
            try
            {
                var listDuAnUserDelete = _duAnUserService.GetAll().Where(x => x.UserId == idUser).ToList();
                foreach (var da in listDuAnUserDelete)
                {
                    _duAnUserService.Delete(da.IdDuanUser);
                    _duAnUserService.Save();
                }

                if (selectedRole.Contains("Trưởng phòng"))
                {
                    var query = AppUserManager.Users.Where(x => x.Delete == false && x.Status == true);
                    List<AppUserViewModel> appusernew = new List<AppUserViewModel>();
                    foreach (var item in query)
                    {
                        var user = AppUserManager.FindByIdAsync(item.Id).Result;
                        var roles = AppUserManager.GetRolesAsync(item.Id).Result;
                        var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                        applicationUserViewModel.Roles = roles;
                        appusernew.Add(applicationUserViewModel);
                    }

                    string idTruongPhongHienTai = appusernew.Where(x => x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser
                    && x.Roles.Contains("Trưởng phòng")
                    && x.Id != idUser).FirstOrDefault().Id;

                    var listDuAn = _duAnUserService.GetAll().Where(x => x.UserId == idTruongPhongHienTai).ToList();
                    foreach (var da in listDuAn)
                    {
                        var lstDAUSEx = _duAnUserService.GetAll().Where(x => x.IdDuAn == da.IdDuAn && x.UserId == idUser).FirstOrDefault();
                        if (lstDAUSEx == null)
                        {
                            var newDuanUser = new DuAnUser();
                            newDuanUser.UserId = idUser;
                            newDuanUser.IdDuAn = da.IdDuAn;
                            newDuanUser.CreatedDate = DateTime.Now;
                            newDuanUser.CreatedBy = User.Identity.GetUserId();
                            newDuanUser.Status = true;
                            _duAnUserService.Add(newDuanUser);
                            _duAnUserService.Save();
                        }
                    }
                }

                if (selectedRole.Contains("Giám đốc") || selectedRole.Contains("Phó giám đốc"))
                {
                    var listDuAn = _duAnUserService.GetAll().ToList();
                    foreach (var da in listDuAn)
                    {
                        var lstDAUSEx = _duAnUserService.GetAll().Where(x => x.IdDuAn == da.IdDuAn && x.UserId == idUser).FirstOrDefault();
                        if (lstDAUSEx == null)
                        {
                            var newDuanUser = new DuAnUser();
                            newDuanUser.UserId = idUser;
                            newDuanUser.IdDuAn = da.IdDuAn;
                            newDuanUser.CreatedDate = DateTime.Now;
                            newDuanUser.CreatedBy = User.Identity.GetUserId();
                            newDuanUser.Status = true;
                            _duAnUserService.Add(newDuanUser);
                            _duAnUserService.Save();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string aaaa = e.Message;
            }
        }

        [HttpDelete]
        [Route("delete")]
        [Permission(Action = "Delete", Function = "USER")]
        public async Task<HttpResponseMessage> Delete(HttpRequestMessage request, string id)
        {
            var appUser = await AppUserManager.FindByIdAsync(id);
            var result = await AppUserManager.DeleteAsync(appUser);
            if (result.Succeeded)
                return request.CreateResponse(HttpStatusCode.OK, id);
            else
                return request.CreateErrorResponse(HttpStatusCode.OK, string.Join(",", result.Errors));
        }

        [HttpDelete]
        [Route("disable")]
        [Permission(Action = "Delete", Function = "USER")]
        public async Task<HttpResponseMessage> Disable(HttpRequestMessage request, string id)
        {
            var appUser = await AppUserManager.FindByIdAsync(id);
            appUser.Delete = true;
            var result = await AppUserManager.UpdateAsync(appUser);
            if (result.Succeeded)
                return request.CreateResponse(HttpStatusCode.OK, id);
            else
                return request.CreateErrorResponse(HttpStatusCode.OK, string.Join(",", result.Errors));
        }

        [HttpPut]
        [Route("updatepass")]
        public async Task<HttpResponseMessage> UpdatePass(HttpRequestMessage request, ChangePasswordViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await AppUserManager.ChangePasswordAsync(User.Identity.GetUserId(), applicationUserViewModel.currentPassword, applicationUserViewModel.Password);

                    if (result.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPut]
        [Route("updateAvatar")]
        public async Task<HttpResponseMessage> UpdateAvatar(HttpRequestMessage request, string avatarUrl)
        {
            if (!string.IsNullOrEmpty(avatarUrl))
            {
                string userId = User.Identity.GetUserId();
                var appUser = await AppUserManager.FindByIdAsync(userId);
                appUser.Avatar = avatarUrl;
                var result = await AppUserManager.UpdateAsync(appUser);
                if (result.Succeeded)
                {
                    return request.CreateResponse(HttpStatusCode.Created, true);
                }
            }
            return request.CreateResponse(HttpStatusCode.BadRequest, false);
        }

        [HttpPut]
        [Route("updatePassByUser")]
        [Permission(Action = "Update", Function = "USER")]
        public async Task<HttpResponseMessage> UpdatePassByUser(HttpRequestMessage request, ChangePasswordViewModel applicationUserViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AppUserManager.RemovePassword(applicationUserViewModel.Id);
                    var result = await AppUserManager.AddPasswordAsync(applicationUserViewModel.Id, applicationUserViewModel.Password);
                    if (result.Succeeded)
                    {
                        return request.CreateResponse(HttpStatusCode.OK, applicationUserViewModel);
                    }
                    else
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                }
                catch (NameDuplicatedException dex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, dex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        [HttpPost]
        [Authorize]
        [Route("logout")]
        public HttpResponseMessage Logout(HttpRequestMessage request)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            return request.CreateResponse(HttpStatusCode.OK, new { success = true });
        }

    }
}
