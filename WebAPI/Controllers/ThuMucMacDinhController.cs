﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Service;
using WebAPI.Infrastructure.Core;
using Model.Models;
using WebAPI.Models.DuAn;
using AutoMapper;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/tmmd")]
    public class ThuMucMacDinhController : ApiControllerBase
    {
        public IThuMucMacDinhService _tmmdService;
        public ThuMucMacDinhController(IThuMucMacDinhService tmmdService,IErrorService errorService) : base(errorService)
        {
            _tmmdService = tmmdService;
        }
        [HttpGet]
        [Route("gettmmd")]
        [Permission(Action = "Get", Function = "THUMUCMACDINH")]
        public HttpResponseMessage GetTMMD(HttpRequestMessage request, string filter, int page, int pageSize)
        {
            return CreateHttpResponse(request, () => {
                HttpResponseMessage response = null;
                IEnumerable<ThuMucMacDinh> tmmds = _tmmdService.GetByFilter(filter).OrderBy(x => x.IdThuMucMacDinh).Skip((page - 1) * pageSize).Take(pageSize);
                
                IEnumerable<ThuMucMacDinhViewModel> tmmdVMs = Mapper.Map<IEnumerable<ThuMucMacDinh>, IEnumerable<ThuMucMacDinhViewModel>>(tmmds);
                PaginationSet<ThuMucMacDinhViewModel> pagedSet = new PaginationSet<ThuMucMacDinhViewModel>()
                {
                    PageIndex = page,
                    TotalRows = tmmds.ToList().Count,
                    PageSize = pageSize,
                    Items = tmmdVMs
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }
        [HttpGet]
        [Route("getall")]
        [Permission(Action = "Get", Function = "THUMUCMACDINH")]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () => {
                HttpResponseMessage response = null;
                IEnumerable<ThuMucMacDinh> tmmds = _tmmdService.GetAll();

                IEnumerable<ThuMucMacDinhViewModel> tmmdVMs = Mapper.Map<IEnumerable<ThuMucMacDinh>, IEnumerable<ThuMucMacDinhViewModel>>(tmmds);
               

                response = request.CreateResponse(HttpStatusCode.OK, tmmdVMs);
                return response;
            });
        }

        [HttpPost]
        [Permission(Action = "Add", Function = "THUMUCMACDINH")]
        [Route("add")]
        public HttpResponseMessage Add(HttpRequestMessage request, ThuMucMacDinhViewModel tmmdVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                ThuMucMacDinh tmmd = new ThuMucMacDinh();
                tmmd.UpdateThuMucMacDinh(tmmdVM);
                tmmd= _tmmdService.Add(tmmd);
                _tmmdService.Save();
                var responseData =Mapper.Map<ThuMucMacDinh, ThuMucMacDinhViewModel>(tmmd);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                return response;
            });

        }
        [HttpPut]
        [Permission(Action = "Update", Function = "THUMUCMACDINH")]
        [Route("update")]
        public HttpResponseMessage Update(HttpRequestMessage request, ThuMucMacDinhViewModel tmmdVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                ThuMucMacDinh tmmd = new ThuMucMacDinh();
                tmmd.UpdateThuMucMacDinh(tmmdVM);
               _tmmdService.Update(tmmd);
                _tmmdService.Save();
                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });

        }
        [HttpDelete]
        [Route("delete/")]
        [Permission(Action = "Delete", Function = "THUMUCMACDINH")]
        public HttpResponseMessage Delete(HttpRequestMessage request, string arrayId)
        {

            var tempt = arrayId.Split(',');

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                foreach (var item in tempt)
                {
                    int id = Convert.ToInt16(item);
                    _tmmdService.Delete(id);
                    _tmmdService.Save();

                }

                response = request.CreateResponse(HttpStatusCode.OK);
                return response;
            });
        }
        [HttpGet]
        [Route("gettmmdbyid/{id}")]
        [Permission(Action="Get",Function ="THUMUCMACDINH")]
        public HttpResponseMessage GetTMMDById(int id)
        {
            return CreateHttpResponse(Request, () =>
            {
                HttpResponseMessage response = null;
                var responseData = _tmmdService.GetTMMDById(id);
                response = Request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }
    }
}
