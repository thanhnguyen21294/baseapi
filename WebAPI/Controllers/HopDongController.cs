﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/hopdong")]
    [Authorize]
    public class HopDongController : ApiControllerBase
    {
        private IHopDongService _hopDongService;
        private IDanhMucNhaThauService _danhMucNhaThauService;
        private IDeleteService _deleteService;
        public HopDongController(IErrorService errorService, IHopDongService hopDongService, IDanhMucNhaThauService danhMucNhaThauService, IDeleteService deleteService) : base(errorService)
        {
            this._hopDongService = hopDongService;
            this._danhMucNhaThauService = danhMucNhaThauService;
            this._deleteService = deleteService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetAll().OrderByDescending(x => x.IdGoiThau);

                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDuAn, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hopDongService.GetByFilter(idDuAn, page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<GoiThau>, IEnumerable<GoiThauViewModels>>(model);

                PaginationSet<GoiThauViewModels> pagedSet = new PaginationSet<GoiThauViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }
        [Route("getbyyear")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetByYear(HttpRequestMessage request, string year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetByYear(year).OrderByDescending(x => x.IdGoiThau);

                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyidduangrouploaigoithau")]
        [HttpGet]
        [Permission(Action = "Read", Function = "TONGHOPGIAINGAN")]
        public HttpResponseMessage GetByIdDuAnGroupLoaiGoiThau(HttpRequestMessage request, int idDa, string year)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetByIdDuAnGroupLoaiGoiThau(idDa,year);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, int idDa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetByIdDuAn(idDa);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyidduanbaolanh")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetByIdDuAn_BaoLanh(HttpRequestMessage request, int idDa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetByIdDuAnBaoLanh(idDa);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("gethopdongthuchienbyidduan")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetHopDongThucHienByIdDuAn(HttpRequestMessage request, int idDa, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _hopDongService.GetHopDongThucHienByIdDuAn(idDa, page, pageSize,out totalRow, filter);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);

                PaginationSet<HopDongViewModels> pagedSet = new PaginationSet<HopDongViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm
                };
                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);
                return response;
            });
        }
        
        [Route("getbyidgoithau")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetByIdGoiThau(HttpRequestMessage request, int idGoiThau)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _hopDongService.GetByIdGoiThau(idGoiThau);
                var modelVm = Mapper.Map<IEnumerable<HopDong>, IEnumerable<HopDongViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, modelVm);
                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "HOPDONG")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hopDongService.GetById(id);

                var responseData = Mapper.Map<HopDong, HopDongViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "HOPDONG")]
        public HttpResponseMessage Create(HttpRequestMessage request, HopDongViewModels vmHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newHopDong = new HopDong();
                    newHopDong.UpdateHopDong(vmHopDongViewModels);
                    newHopDong.CreatedDate = DateTime.Now;
                    newHopDong.CreatedBy = User.Identity.GetUserId();
                    try
                    {
                        var query = _danhMucNhaThauService.GetAll(vmHopDongViewModels.IdDuAn).Where(x =>
                            x.TenNhaThau.Contains(vmHopDongViewModels.DanhMucNhaThau.TenNhaThau));
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = vmHopDongViewModels.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = vmHopDongViewModels.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = vmHopDongViewModels.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            newHopDong.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhmuc = query.SingleOrDefault();
                            newHopDong.IdNhaThau = danhmuc.IdNhaThau;
                            danhmuc.DiaChiNhaThau = vmHopDongViewModels.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }
                    _hopDongService.Add(newHopDong);
                    _hopDongService.Save();
                    var responseData = Mapper.Map<HopDong, HopDongViewModels>(newHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "HOPDONG")]
        public HttpResponseMessage Update(HttpRequestMessage request, HopDongViewModels vmHopDongViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var updateHopDong = _hopDongService.GetById(vmHopDongViewModels.IdHopDong);
                    updateHopDong.UpdateHopDong(vmHopDongViewModels);
                    updateHopDong.CreatedDate = DateTime.Now;
                    updateHopDong.CreatedBy = User.Identity.GetUserId();
                    try
                    {
                        var query = _danhMucNhaThauService.GetAll(vmHopDongViewModels.IdDuAn)
                            .Where(x => x.TenNhaThau == vmHopDongViewModels.DanhMucNhaThau.TenNhaThau);
                        if (!query.Any())
                        {
                            DanhMucNhaThau danhMucNhaThau = new DanhMucNhaThau();
                            danhMucNhaThau.TenNhaThau = vmHopDongViewModels.DanhMucNhaThau.TenNhaThau;
                            danhMucNhaThau.DiaChiNhaThau = vmHopDongViewModels.DanhMucNhaThau.DiaChiNhaThau;
                            danhMucNhaThau.IdDuAn = vmHopDongViewModels.IdDuAn;
                            danhMucNhaThau.CreatedBy = User.Identity.GetUserId();
                            danhMucNhaThau.CreatedDate = DateTime.Now;
                            updateHopDong.DanhMucNhaThau = danhMucNhaThau;
                        }
                        else
                        {
                            var danhMuc = query.SingleOrDefault();
                            updateHopDong.IdNhaThau = danhMuc.IdNhaThau;
                            danhMuc.DiaChiNhaThau = vmHopDongViewModels.DanhMucNhaThau.DiaChiNhaThau;
                        }
                    }
                    catch (Exception e)
                    {
                        return request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message);
                    }

                    _hopDongService.Save();

                    var responseData = Mapper.Map<HopDong, HopDongViewModels>(updateHopDong);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOPDONG")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _hopDongService.Delete(id);
                    _hopDongService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "HOPDONG")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listIDHopDong = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listIDHopDong)
                    {
                        _deleteService.DeleteHopDong(item);
                    }

                    _deleteService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listIDHopDong.Count);
                }

                return response;
            });
        }
    }
}
