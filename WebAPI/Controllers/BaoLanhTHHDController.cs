﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/baolanhthhd")]
    [Authorize]
    public class BaoLanhTHHDController : ApiControllerBase
    {
        private IBaoLanhTHHDService _BaoLanhTHHDService;
        public BaoLanhTHHDController(IErrorService errorService, IBaoLanhTHHDService BaoLanhTHHDService) : base(errorService)
        {
            this._BaoLanhTHHDService = BaoLanhTHHDService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _BaoLanhTHHDService.GetAll().OrderByDescending(x => x.IdBaoLanh);

                var modelVm = Mapper.Map<IEnumerable<BaoLanhTHHD>, IEnumerable<BaoLanhTHHDViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getlistpaging")]
        [HttpGet]
        [Permission(Action = "Read", Function = "BAOLANH")]
        public HttpResponseMessage GetListPaging(HttpRequestMessage request, int idDA, int page, int pageSize, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                int totalRow = 0;

                var model = _BaoLanhTHHDService.GetByFilter(idDA, page, pageSize, out totalRow, filter);

                var modelVm = Mapper.Map<IEnumerable<BaoLanhTHHD>, IEnumerable<BaoLanhTHHDViewModels>>(model);

                PaginationSet<BaoLanhTHHDViewModels> pagedSet = new PaginationSet<BaoLanhTHHDViewModels>()
                {
                    PageIndex = page,
                    TotalRows = totalRow,
                    PageSize = pageSize,
                    Items = modelVm.OrderBy(x => x.IdHopDong)
                };

                response = request.CreateResponse(HttpStatusCode.OK, pagedSet);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        [Permission(Action = "Read", Function = "BAOLANH")]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _BaoLanhTHHDService.GetById(id);

                var responseData = Mapper.Map<BaoLanhTHHD, BaoLanhTHHDViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        [Permission(Action = "Create", Function = "BAOLANH")]
        public HttpResponseMessage Create(HttpRequestMessage request, BaoLanhTHHDViewModels BaoLanhTHHDViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newBaoLanhTHHD = new BaoLanhTHHD();
                    newBaoLanhTHHD.IdDuAn = BaoLanhTHHDViewModels.IdDuAn;
                    newBaoLanhTHHD.IdHopDong = BaoLanhTHHDViewModels.IdHopDong;
                    newBaoLanhTHHD.LoaiBaoLanh = BaoLanhTHHDViewModels.LoaiBaoLanh;
                    newBaoLanhTHHD.SoBaoLanh = BaoLanhTHHDViewModels.SoBaoLanh;
                    if (!string.IsNullOrEmpty(BaoLanhTHHDViewModels.NgayBaoLanh))
                    {
                        var a = BaoLanhTHHDViewModels.NgayBaoLanh;
                        DateTime dateTime = DateTime.ParseExact(BaoLanhTHHDViewModels.NgayBaoLanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newBaoLanhTHHD.NgayBaoLanh = dateTime;
                    }
                    else
                    {
                        newBaoLanhTHHD.NgayBaoLanh = null;
                    }
                    if (!string.IsNullOrEmpty(BaoLanhTHHDViewModels.NgayHetHan))
                    {
                        var a = BaoLanhTHHDViewModels.NgayHetHan;
                        DateTime dateTime = DateTime.ParseExact(BaoLanhTHHDViewModels.NgayHetHan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        newBaoLanhTHHD.NgayHetHan = dateTime;
                    }
                    else
                    {
                        newBaoLanhTHHD.NgayHetHan = null;
                    }
                    newBaoLanhTHHD.SoTien = BaoLanhTHHDViewModels.SoTien;
                    newBaoLanhTHHD.GhiChu = BaoLanhTHHDViewModels.GhiChu;
                    newBaoLanhTHHD.CreatedDate = DateTime.Now;
                    newBaoLanhTHHD.CreatedBy = User.Identity.GetUserId();
                    _BaoLanhTHHDService.Add(newBaoLanhTHHD);
                    _BaoLanhTHHDService.Save();

                    var responseData = Mapper.Map<BaoLanhTHHD, BaoLanhTHHDViewModels>(newBaoLanhTHHD);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [Permission(Action = "Update", Function = "BAOLANH")]
        public HttpResponseMessage Update(HttpRequestMessage request, BaoLanhTHHDViewModels BaoLanhTHHDViewModels)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbBaoLanhTHHD = _BaoLanhTHHDService.GetById(BaoLanhTHHDViewModels.IdBaoLanh);
                    dbBaoLanhTHHD.IdHopDong = BaoLanhTHHDViewModels.IdHopDong;
                    dbBaoLanhTHHD.LoaiBaoLanh = BaoLanhTHHDViewModels.LoaiBaoLanh;
                    dbBaoLanhTHHD.SoBaoLanh = BaoLanhTHHDViewModels.SoBaoLanh;
                    if (!string.IsNullOrEmpty(BaoLanhTHHDViewModels.NgayBaoLanh))
                    {
                        var a = BaoLanhTHHDViewModels.NgayBaoLanh;
                        DateTime dateTime = DateTime.ParseExact(BaoLanhTHHDViewModels.NgayBaoLanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        dbBaoLanhTHHD.NgayBaoLanh = dateTime;
                    }
                    else
                    {
                        dbBaoLanhTHHD.NgayBaoLanh = null;
                    }
                    if (!string.IsNullOrEmpty(BaoLanhTHHDViewModels.NgayHetHan))
                    {
                        var a = BaoLanhTHHDViewModels.NgayHetHan;
                        DateTime dateTime = DateTime.ParseExact(BaoLanhTHHDViewModels.NgayHetHan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                        dbBaoLanhTHHD.NgayHetHan = dateTime;
                    }
                    else
                    {
                        dbBaoLanhTHHD.NgayHetHan = null;
                    }
                    dbBaoLanhTHHD.SoTien = BaoLanhTHHDViewModels.SoTien;
                    dbBaoLanhTHHD.GhiChu = BaoLanhTHHDViewModels.GhiChu;
                    dbBaoLanhTHHD.UpdatedDate = DateTime.Now;
                    dbBaoLanhTHHD.UpdatedBy = User.Identity.GetUserId();
                    _BaoLanhTHHDService.Update(dbBaoLanhTHHD);
                    _BaoLanhTHHDService.Save();

                    var responseData = Mapper.Map<BaoLanhTHHD, BaoLanhTHHDViewModels>(dbBaoLanhTHHD);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }


        [Route("guitruongphong")]
        [HttpPut]
        [Permission(Action = "Update", Function = "THANHTOANCHIPHIKHAC")]
        public HttpResponseMessage UpdateTrangThai(HttpRequestMessage request, UpdateTrangThaiViewModels vnTrangThaiUp)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbBaoLanhTHHD = _BaoLanhTHHDService.GetById(vnTrangThaiUp.ID);
                    dbBaoLanhTHHD.TrangThaiGui = 1;
                    dbBaoLanhTHHD.UpdatedDate = DateTime.Now;
                    dbBaoLanhTHHD.UpdatedBy = User.Identity.GetUserId();
                    _BaoLanhTHHDService.Update(dbBaoLanhTHHD);
                    _BaoLanhTHHDService.Save();
                    response = request.CreateResponse(HttpStatusCode.Created);
                }
                return response;
            });
        }




        [Route("delete/")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "BAOLANH")]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _BaoLanhTHHDService.Delete(id);
                    _BaoLanhTHHDService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }

        [Route("deletemulti")]
        [HttpDelete]
        [Permission(Action = "Delete", Function = "BAOLANH")]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listid)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var listBaoLanhTHHD = new JavaScriptSerializer().Deserialize<List<int>>(listid);
                    foreach (var item in listBaoLanhTHHD)
                    {
                        _BaoLanhTHHDService.Delete(item);
                    }

                    _BaoLanhTHHDService.Save();

                    response = request.CreateResponse(HttpStatusCode.OK, listBaoLanhTHHD.Count);
                }

                return response;
            });
        }
    }
}
