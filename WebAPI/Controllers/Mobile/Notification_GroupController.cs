﻿using Model.Models.Mobile;
using Service;
using Service.Mobile;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;

namespace WebAPI.Controllers.Mobile
{
    [RoutePrefix("api/notification_group")]
    public class Notification_GroupController : ApiControllerBase
    {
        private INotification_GroupService _INotificationGroupService;
        public Notification_GroupController(IErrorService errorService, INotification_GroupService notification_GroupService) : base(errorService)
        {
            _INotificationGroupService = notification_GroupService;
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Add(HttpRequestMessage request, Notification_Group notification_Group)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                Notification_Group newEntity = new Notification_Group();
                newEntity.Name = notification_Group.Name;
                newEntity.Description = notification_Group.Description;
                newEntity.Notification_Key_Name = notification_Group.Notification_Key_Name;
                NotificationFcmService notificationFcmService = new NotificationFcmService();
                string Notification_Key = notificationFcmService.CreatingDeviceGroup(newEntity.Notification_Key_Name).Response;
                if (string.IsNullOrEmpty(Notification_Key))
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                var model = _INotificationGroupService.Add(newEntity);
                _INotificationGroupService.Save();
                response = request.CreateResponse(HttpStatusCode.Created, model);
                return response;
            });
        }

        [Route("update")]
        [HttpPost]
        public HttpResponseMessage Update(HttpRequestMessage request, Notification_Group notification_Group)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

                Notification_Group updateEntity = _INotificationGroupService.GetById(notification_Group.Id);

                updateEntity.Notification_Key_Name = notification_Group.Notification_Key_Name;
                NotificationFcmService notificationFcmService = new NotificationFcmService();
                _INotificationGroupService.Update(updateEntity);
                _INotificationGroupService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, updateEntity);
                return response;
            });
        }
    }
}
