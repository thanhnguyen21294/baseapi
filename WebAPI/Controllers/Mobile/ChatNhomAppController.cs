﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Common;
using Model.Models.Mobile;
using Service;
using Service.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Data.View.Mobile;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Models.Mobile;
using WebAPI.Providers;


namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/chatnhomapp")]
    public class ChatNhomAppController : ApiControllerBase
    {
        private IChatNhomAppService _chatNhomAppService;
        private IDeleteService _deleteService;
        private INotificationService _InotificationService;
        private INotification_UserService _Inotification_UserService;
        private IDevice_TokenService _device_TokenService;
        private INhomChatAppService _NhomChatAppService;
        private readonly ITrangThaiChatService _trangThaiChatService;
        public ChatNhomAppController(
            IErrorService errorService,
            IDeleteService deleteService,
            IChatNhomAppService chatNhomAppService,
            INhomChatAppService NhomChatAppService,
            IDevice_TokenService device_TokenService,
            INotificationService notificationService,
            INotification_UserService notification_UserService,
            ITrangThaiChatService trangThaiChatService) : base(errorService)
        {
            this._device_TokenService = device_TokenService;
            this._chatNhomAppService = chatNhomAppService;
            this._NhomChatAppService = NhomChatAppService;
            this._deleteService = deleteService;
            _InotificationService = notificationService;
            _Inotification_UserService = notification_UserService;
            _trangThaiChatService = trangThaiChatService;
        }

        [Route("GetByChatByIDUser/page/{pageIndex}/{pageSize}")]
        [HttpGet]
        public HttpResponseMessage GetByIdDuAn(HttpRequestMessage request, string IdUser1, int IdUser2, int pageIndex, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var totalItems = 0;
                var model = _chatNhomAppService.GetByChatByIDUser(IdUser1, IdUser2, out totalItems, pageIndex, pageSize);

                //var modelVm = Mapper.Map<IEnumerable<ChatNhomApp>, IEnumerable<ChatNhomAppViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, new { model, totalItems });

                var TrangThaiChat = _trangThaiChatService.GetChatByUserId1AndUserId2(IdUser1, IdUser2.ToString());
                if (TrangThaiChat != null && !TrangThaiChat.TrangThai)
                {
                    TrangThaiChat.TrangThai = true;
                    _trangThaiChatService.Update(TrangThaiChat);
                    _trangThaiChatService.Save();
                }
                return response;
            });
        }
        [Route("CapNhatTrangThaiChatNhom")]
        [HttpGet]
        public HttpResponseMessage CapNhatTrangThaiChatNhoms(HttpRequestMessage request, string IdUser1, int IdUser2)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var TrangThaiChat = _trangThaiChatService.GetChatByUserId1AndUserId2(IdUser1, IdUser2.ToString());
                if (TrangThaiChat != null && !TrangThaiChat.TrangThai)
                {
                    TrangThaiChat.TrangThai = true;
                    _trangThaiChatService.Update(TrangThaiChat);
                    _trangThaiChatService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, TrangThaiChat);
                return response;
            });
        }
        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddChat(HttpRequestMessage request, ChatNhomAppViewModels ItemChat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var newChatNhomApp = new ChatNhomApp();
                newChatNhomApp.UpdateChatNhomApp(ItemChat);
                newChatNhomApp.CreatedDate = DateTime.Now;
                newChatNhomApp.CreatedBy = User.Identity.GetUserId();
                _chatNhomAppService.Add(newChatNhomApp);
                _chatNhomAppService.Save();

                //var responseData = Mapper.Map<ChatApp, ChatAppViewModels>(newChatApp);
                response = request.CreateResponse(HttpStatusCode.Created, newChatNhomApp);

                var listTrangThaiChat = _trangThaiChatService.GetListTrangThaiChatByNhomChatId(ItemChat.IdUser2.ToString());
                UpdateListTrangThaiChat(listTrangThaiChat, ItemChat.IdUser1);
                _trangThaiChatService.Save();
                #region create notification and send all to user
                var notificationFCMService = new NotificationFcmService();
                var notification = new Notification();
                notification.Title = "Tin nhắn mới";
                notification.Body = ItemChat.NoiDung;
                notification.CreatedDate = DateTime.Now;
                notification.CreatedBy = User.Identity.GetUserId();
                var noti = _InotificationService.Add(notification);
                //_InotificationService.Save();
                if (noti != null)
                {
                    var notification_User = new Notification_User();
                    //var listUserId = ItemChat.IdUser2;
                    var listTokenDeviceAll = _device_TokenService.GetAll();
                    List<string> abc = new List<string>();
                    foreach (var item in ItemChat.ListThongTinNhom)
                    {
                        foreach (var item1 in listTokenDeviceAll)
                        {
                            if (item == item1.UserId)
                            {
                                abc.Add(item1.Token);
                            }
                        }
                    }
                    ItemChat.AppUser1 = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser1).SingleOrDefault();
                    ItemChat.AppUser2 = _NhomChatAppService.GetById(ItemChat.IdUser2);
                    //NoiDungChatNhom DataNoti = new NoiDungChatNhom();
                    var DataNoti = new MessageViewModel(
                        ItemChat.Id,
                        ItemChat.IdUser1,
                        ItemChat.IdUser2.ToString(),
                        ItemChat.NoiDung,
                        ItemChat.IsFile,
                        DateTime.Now,
                        new UserChatViewModel(ItemChat.AppUser1.Id, ItemChat.AppUser1.Avatar, ItemChat.AppUser1.FullName),
                         new UserChatViewModel(ItemChat.AppUser2.Id.ToString(), "", ItemChat.AppUser2.TenNhomChat)
                        );
                    string[] listTokenDevice = abc.ToArray();
                    //var listUserId = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser1).Select(u => u.Id).ToList();
                    _InotificationService.sendNotificationToAllUserMessgerNhom(DataNoti, ItemChat.IdUser1, ItemChat.IdUser2, ItemChat.NoiDung, listTokenDevice, ItemChat.TenNguoiGui, ItemChat.ListThongTinNhom, User.Identity.GetUserId(), "news");
                }
                #endregion

                return response;
            });
        }

        private void UpdateListTrangThaiChat(List<TrangThaiChat> listTrangThaiChat, string idUser1)
        {
            listTrangThaiChat.ForEach(item =>
            {
                if (item.IdUser1 != idUser1)
                {
                    item.TrangThai = false;
                    _trangThaiChatService.Update(item);
                    _trangThaiChatService.Save();
                }
            });
        }
    }
}
