﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models.Mobile;
using Service;
using Service.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Infrastructure.Core;
using WebAPI.Models.Mobile;

namespace WebAPI.Controllers.Mobile
{
    [RoutePrefix("api/notification")]
    public class NotificationController : ApiControllerBase
    {
        private INotificationService _InotificationService;
        private IDevice_TokenService _IDevice_TokenService;
        private INotification_UserService _Inotification_UserService;
        private NotificationFcmService _NotificationFcmService;
        private INotification_GroupService _INotificationGroupService;

        public NotificationController(IErrorService errorService) : base(errorService)
        {

        }

        public NotificationController(
            IErrorService errorService,
            INotificationService notificationService,
            IDevice_TokenService device_TokenService,
            INotification_UserService notification_UserService,
            INotification_GroupService notification_GroupService) : base(errorService)
        {
            _IDevice_TokenService = device_TokenService;
            _InotificationService = notificationService;
            _Inotification_UserService = notification_UserService;
            _NotificationFcmService = new NotificationFcmService();
            _INotificationGroupService = notification_GroupService;
        }

        [Route("devicetoken/add")]
        [HttpPost]
        public HttpResponseMessage AddDeviceToken(HttpRequestMessage request, Device_Token device_Token)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                var newEntity = new Device_Token();
                newEntity.UserId = newEntity.CreatedBy = User.Identity.GetUserId();
                newEntity.CreatedDate = DateTime.Now;
                newEntity.Token = device_Token.Token;
                _IDevice_TokenService.Add(newEntity);
                _IDevice_TokenService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Add(HttpRequestMessage request, Notification notification)
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            var newEntity = new Notification();
            newEntity.CreatedDate = DateTime.Now;
            newEntity.CreatedBy = User.Identity.GetUserId();
            newEntity.Body = notification.Body;
            newEntity.Title = notification.Title;
            _InotificationService.Add(newEntity);
            _InotificationService.Save();
            response = request.CreateResponse(HttpStatusCode.OK, true);
            return response;
        }

        [Route("getDetailUserNotiById/{Id}")]
        [HttpGet]
        public HttpResponseMessage GetDetail(HttpRequestMessage request, int Id)
        {
            return CreateHttpResponse(request, () =>
             {
                 HttpResponseMessage response = null;
                 var model = _Inotification_UserService.GetById(Id);
                 response = request.CreateResponse(HttpStatusCode.OK, model);
                 return response;
             });
        }

        [Route("getallbyuserid")]
        [HttpGet]
        public HttpResponseMessage GetAllByUserId(HttpRequestMessage request, int pageCurrent, int itemPerpage, string filter = null)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                int total;
                string UserId = User.Identity.GetUserId();
                var model = _Inotification_UserService.GetAllNotiByUserId(UserId, out total, pageCurrent, itemPerpage, filter);
                var responseData = Mapper.Map<IEnumerable<Notification_User>, IEnumerable<NotificationUserViewModel>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, new { items = responseData, totalPages = total });
                return response;
            });
        }

        [Route("getamountmessagenotread")]
        [HttpGet]
        public HttpResponseMessage GetAmountMessageNotRead(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                string UserId = User.Identity.GetUserId();
                var model = _Inotification_UserService.GetAmoutMessageNotRead(UserId);
                response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }

        [Route("getlastestmessage")]
        [HttpGet]
        public HttpResponseMessage GetLastestMessage(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                string UserId = User.Identity.GetUserId();
                var model = _Inotification_UserService.GetLastestMessage(UserId);
                var responseData = Mapper.Map<Notification_User, NotificationUserViewModel>(model);
                response = request.CreateResponse(HttpStatusCode.OK, responseData);
                return response;
            });
        }


        [Route("send/{notificationGroupId}")]
        [HttpPost]
        public HttpResponseMessage SendToGroup(HttpRequestMessage request, Notification notification, int notificationGroupId)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                var notificationGroup = _INotificationGroupService.GetById(notificationGroupId);
                var newEntity = new Notification();
                newEntity.CreatedDate = DateTime.Now;
                newEntity.Body = notification.Body;
                newEntity.Title = notification.Title;
                newEntity.CreatedBy = User.Identity.GetUserId();
                var model = _InotificationService.Add(newEntity);
                _InotificationService.Save();
                var result = _NotificationFcmService.SendNotificationToGroup(notification.Title, notification.Body, notificationGroup.Notification_Key);
                response = request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            });
        }

        [Route("sendAll")]
        [HttpPost]
        public HttpResponseMessage SendAll(HttpRequestMessage request, Notification notification)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    return request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                var result = _NotificationFcmService.SendNotificationToAllDevice(notification.Title, notification.Body, null, "news");
                response = request.CreateResponse(HttpStatusCode.OK, result);
                return response;
            });
        }

        [Route("markReadAll")]
        [HttpPut]
        public HttpResponseMessage MarkReadAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string userId = User.Identity.GetUserId();
                List<Notification_User> listNotiUser = _Inotification_UserService.GetAllByUserId(userId).ToList();
                foreach (var notiUser in listNotiUser)
                {
                    notiUser.HasRead = true;
                    _Inotification_UserService.Update(notiUser);
                    _Inotification_UserService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("markReadById/{Id}")]
        [HttpPut]
        public HttpResponseMessage MarkReadById(HttpRequestMessage request, int Id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var notiUser = _Inotification_UserService.GetById(Id);

                notiUser.HasRead = !notiUser.HasRead;
                _Inotification_UserService.Update(notiUser);
                _Inotification_UserService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }


        [Route("markMulti/{listIds}")]
        [HttpPut]
        public HttpResponseMessage MarkMulti(HttpRequestMessage request, string listIds)
        {
            string[] listMarkIds = listIds.Split(',');
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                foreach (var Id in listMarkIds)
                {
                    var notiUser = _Inotification_UserService.GetById(int.Parse(Id));
                    if (notiUser != null && !notiUser.HasRead)
                    {
                        notiUser.HasRead = !notiUser.HasRead;
                        _Inotification_UserService.Update(notiUser);
                        _Inotification_UserService.Save();
                    }
                }
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("delete/{Id}")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request, int Id)
        {

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                _Inotification_UserService.Delete(Id);
                _Inotification_UserService.Save();
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("deleteMulti/{listIds}")]
        [HttpDelete]
        public HttpResponseMessage DeleteMulti(HttpRequestMessage request, string listIds)
        {
            string[] listDeleteIds = listIds.Split(',');
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                foreach (var id in listDeleteIds)
                {
                    _Inotification_UserService.Delete(int.Parse(id));
                    _Inotification_UserService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }
        [Route("deleteAll")]
        [HttpGet]
        public HttpResponseMessage DeleteAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string UserId = User.Identity.GetUserId();
                var listDeleteIds = _Inotification_UserService.GetAllNotiByUserIdNoPage(UserId);
                foreach (var id in listDeleteIds)
                {
                    _Inotification_UserService.Delete(id);
                    _Inotification_UserService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }

        [Route("markMultiAll")]
        [HttpGet]
        public HttpResponseMessage MarkMultiAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                string UserId = User.Identity.GetUserId();
                var ListnotiUser = _Inotification_UserService.GetAllByUserId(UserId).Where(x => !x.HasRead);
                foreach (var notiUser in ListnotiUser)
                {
                    notiUser.HasRead = !notiUser.HasRead;
                    _Inotification_UserService.Update(notiUser);
                    _Inotification_UserService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, true);
                return response;
            });
        }
    }
}
