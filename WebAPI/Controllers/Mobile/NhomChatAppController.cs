﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Common;
using Model.Models.Mobile;
using Service;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.DuAn;
using WebAPI.Models.Mobile;
using WebAPI.Providers;

namespace WebAPI.Controllers
{
    [RoutePrefix("api/nhomchatapp")]
    [Authorize]
    public class NhomChatAppController : ApiControllerBase
    {
        private INhomChatAppService _nhomChatAppService;
        private readonly ITrangThaiChatService _trangThaiChatService;
        public NhomChatAppController(
            IErrorService errorService,
            INhomChatAppService nhomChatAppService,
            ITrangThaiChatService trangThaiChatService) : base(errorService)
        {
            this._nhomChatAppService = nhomChatAppService;
            _trangThaiChatService = trangThaiChatService;
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nhomChatAppService.GetAll();

                var modelVm = Mapper.Map<IEnumerable<NhomChatApp>, IEnumerable<NhomChatAppViewModels>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("getbyid/{id}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _nhomChatAppService.GetById(id);

                var responseData = Mapper.Map<NhomChatApp, NhomChatAppViewModels>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, NhomChatAppViewModels nhomChatApptVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var newNhomChatApp = new NhomChatApp();
                    newNhomChatApp.UpdateNhomChatApp(nhomChatApptVm);
                    newNhomChatApp.CreatedDate = DateTime.Now;
                    newNhomChatApp.CreatedBy = User.Identity.GetUserId();
                    var nhomChatApp = _nhomChatAppService.Add(newNhomChatApp);
                    _nhomChatAppService.Save();
                    var ListIDUser = nhomChatApptVm.ListUser.Split(new[] { "!@#$%^&*" }, StringSplitOptions.None).ToList();
                    AddTrangThaiChat(ListIDUser, nhomChatApp.Id.ToString());
                    var responseData = Mapper.Map<NhomChatApp, NhomChatAppViewModels>(newNhomChatApp);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        public HttpResponseMessage Update(HttpRequestMessage request, NhomChatAppViewModels nhomChatAppVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbNhomChatApp = _nhomChatAppService.GetById(nhomChatAppVm.Id);

                    dbNhomChatApp.UpdateNhomChatApp(nhomChatAppVm);
                    dbNhomChatApp.UpdatedDate = DateTime.Now;
                    dbNhomChatApp.UpdatedBy = User.Identity.GetUserId();
                    _nhomChatAppService.Update(dbNhomChatApp);
                    _nhomChatAppService.Save();
                    var ListIDUser = nhomChatAppVm.ListUser.Split(new[] { "!@#$%^&*" }, StringSplitOptions.None).ToList();
                    var ListIdUserInDb = _trangThaiChatService.GetListUserIdByNhomChatId(nhomChatAppVm.Id.ToString());
                    var ListUser = ListIDUser.Except(ListIdUserInDb).ToList();
                    AddTrangThaiChat(ListUser, nhomChatAppVm.Id.ToString());
                    var responseData = Mapper.Map<NhomChatApp, NhomChatAppViewModels>(dbNhomChatApp);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }
                return response;
            });
        }

        private void AddTrangThaiChat(List<string> ListIDUser, string IdNhomChat)
        {
            ListIDUser.ForEach(item =>
            {
                var TrangThaiChat = new TrangThaiChat();
                TrangThaiChat.IdUser1 = item;
                TrangThaiChat.IdUser2 = IdNhomChat;
                TrangThaiChat.IdNhom = true;
                TrangThaiChat.TrangThai = true;
                _trangThaiChatService.Add(TrangThaiChat);
                _trangThaiChatService.Save();
            });
        }

        [Route("delete/")]
        [HttpGet]
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhomChatAppService.Delete(id);
                    _nhomChatAppService.Save();

                    response = request.CreateResponse(HttpStatusCode.Created, id);
                }

                return response;
            });
        }
    }
}
