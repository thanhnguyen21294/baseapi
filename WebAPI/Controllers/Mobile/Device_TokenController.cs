﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Common;
using Model.Models.Mobile;
using Service;
using Service.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Models.Mobile;
using WebAPI.Providers;


namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/DeviceToken")]
    public class Device_TokenController : ApiControllerBase
    {
        private IDevice_TokenService _device_TokenService;
        private IDeleteService _deleteService;
        private INotificationService _InotificationService;
        private INotification_UserService _Inotification_UserService;
        public Device_TokenController(
            IErrorService errorService,
            IDeleteService deleteService,
            IDevice_TokenService device_TokenService,
            INotificationService notificationService, INotification_UserService notification_UserService) : base(errorService)
        {
            this._device_TokenService = device_TokenService;
            this._deleteService = deleteService;
            _InotificationService = notificationService;
            _Inotification_UserService = notification_UserService;
        }

        [Route("GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _device_TokenService.GetAll().OrderByDescending(x => x.CreatedDate);

                var modelVm = Mapper.Map<IEnumerable<Device_Token>, IEnumerable<Device_TokenViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, modelVm);

                return response;
            });
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddChat(HttpRequestMessage request, Device_TokenViewModels ItemChat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _device_TokenService.GetAll().OrderByDescending(x => x.CreatedDate);
                foreach(var item in model)
                {
                    if(item.Token == ItemChat.Token)
                    {
                        _device_TokenService.Delete(item.Id);
                    }
                }
                var newDevice_Token = new Device_Token();
                newDevice_Token.UpdateDevice_Token(ItemChat);
                newDevice_Token.CreatedDate = DateTime.Now;
                newDevice_Token.CreatedBy = User.Identity.GetUserId();
                _device_TokenService.Add(newDevice_Token);
                _device_TokenService.Save();

                var responseData = Mapper.Map<Device_Token, Device_TokenViewModels>(newDevice_Token);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                return response;
            });
        }
        [Route("delete")]
        [HttpPost]
        public HttpResponseMessage DeleteDeviceToken(HttpRequestMessage request, Device_TokenViewModels ItemChat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var model = _device_TokenService.GetAll().OrderByDescending(x => x.CreatedDate);
                foreach (var item in model)
                {
                    if (item.Token == ItemChat.Token)
                    {
                        _device_TokenService.Delete(item.Id);
                    }
                }
                _device_TokenService.Save();
                var newDevice_Token = new Device_Token();
                var responseData = Mapper.Map<Device_Token, Device_TokenViewModels>(newDevice_Token);
                response = request.CreateResponse(HttpStatusCode.Created, responseData);
                return response;
            });
        }

    }
}
