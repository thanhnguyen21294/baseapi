﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Model.Models.Common;
using Model.Models.Mobile;
using Service;
using Service.Mobile;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Data.View.Mobile;
using WebAPI.Infrastructure.Core;
using WebAPI.Infrastructure.Extensions;
using WebAPI.Models;
using WebAPI.Models.Common;
using WebAPI.Models.DuAn;
using WebAPI.Models.Mobile;
using WebAPI.Providers;


namespace WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/chatapp")]
    public class ChatAppController : ApiControllerBase
    {
        private IChatAppService _chatAppService;
        private IDeleteService _deleteService;
        private INotificationService _InotificationService;
        private INotification_UserService _Inotification_UserService;
        private IDevice_TokenService _device_TokenService;
        private ITrangThaiChatService _trangThaiChatService;
        private INhomChatAppService _nhomChatAppService;
        public ChatAppController(
            IErrorService errorService,
            IDeleteService deleteService,
            IChatAppService chatAppService,
            IDevice_TokenService device_TokenService,
            INotificationService notificationService,
            INotification_UserService notification_UserService,
            ITrangThaiChatService trangThaiChatService,
            INhomChatAppService nhomChatAppService) : base(errorService)
        {
            this._device_TokenService = device_TokenService;
            this._chatAppService = chatAppService;
            this._deleteService = deleteService;
            _InotificationService = notificationService;
            _Inotification_UserService = notification_UserService;
            _trangThaiChatService = trangThaiChatService;
            _nhomChatAppService = nhomChatAppService;
        }

        [Route("GetByChatByIDUser/page/{pageIndex}/{pageSize}")]
        [HttpGet]
        public HttpResponseMessage GetChatByUserId(HttpRequestMessage request, string IdUser1, string IdUser2, int pageIndex, int pageSize)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var totalItems = 0;
                var model = _chatAppService.GetByChatByIDUser(IdUser1, IdUser2, out totalItems, pageIndex, pageSize);

                //var modelVm = Mapper.Map<IEnumerable<ChatApp>, IEnumerable<ChatAppViewModels>>(model);

                response = request.CreateResponse(HttpStatusCode.OK, new { model, totalItems });
                var TrangThaiChat = _trangThaiChatService.GetChatByUserId1AndUserId2(IdUser1, IdUser2);
                if (TrangThaiChat == null)
                {
                    var trangThaiChat = new TrangThaiChat();
                    trangThaiChat.IdUser1 = IdUser1;
                    trangThaiChat.IdUser2 = IdUser2;
                    trangThaiChat.IdNhom = false;
                    trangThaiChat.TrangThai = true;
                    AddTrangThaiChat(trangThaiChat);
                    var trangThaiChat1 = new TrangThaiChat();
                    trangThaiChat1.IdUser1 = IdUser2;
                    trangThaiChat1.IdUser2 = IdUser1;
                    trangThaiChat1.IdNhom = false;
                    trangThaiChat1.TrangThai = true;
                    AddTrangThaiChat(trangThaiChat1);
                }
                else
                {
                    if (!TrangThaiChat.TrangThai)
                    {
                        TrangThaiChat.TrangThai = true;
                        _trangThaiChatService.Update(TrangThaiChat);
                        _trangThaiChatService.Save();
                    }
                }
                return response;
            });
        }
        [Route("CapNhatTrangThaiChatRieng")]
        [HttpGet]
        public HttpResponseMessage CapNhatTrangThaiChatRiengs(HttpRequestMessage request, string IdUser1, string IdUser2)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                var TrangThaiChat = _trangThaiChatService.GetChatByUserId1AndUserId2(IdUser1, IdUser2);
                if (TrangThaiChat != null && !TrangThaiChat.TrangThai)
                {
                    TrangThaiChat.TrangThai = true;
                    _trangThaiChatService.Update(TrangThaiChat);
                    _trangThaiChatService.Save();
                }
                response = request.CreateResponse(HttpStatusCode.OK, TrangThaiChat);
                return response;
            });
        }
        private void AddTrangThaiChat(TrangThaiChat trangThaiChat)
        {
            _trangThaiChatService.Add(trangThaiChat);
            _trangThaiChatService.Save();
        }

        [Route("add")]
        [HttpPost]
        public HttpResponseMessage AddChat(HttpRequestMessage request, ChatAppViewModels ItemChat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;

                var newChatApp = new ChatApp();
                newChatApp.UpdateChatApp(ItemChat);
                newChatApp.CreatedDate = DateTime.Now;
                newChatApp.CreatedBy = User.Identity.GetUserId();
                _chatAppService.Add(newChatApp);
                _chatAppService.Save();

                //var responseData = Mapper.Map<ChatApp, ChatAppViewModels>(newChatApp);
                response = request.CreateResponse(HttpStatusCode.Created, newChatApp);

                #region create notification and send all to user
                var notificationFCMService = new NotificationFcmService();
                var notification = new Notification();
                notification.Title = "Tin nhắn mới";
                notification.Body = ItemChat.NoiDung;
                notification.CreatedDate = DateTime.Now;
                notification.CreatedBy = User.Identity.GetUserId();
                var noti = _InotificationService.Add(notification);
                //_InotificationService.Save();
                if (noti != null)
                {
                    ItemChat.AppUser1 = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser1).SingleOrDefault();
                    ItemChat.AppUser2 = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser2).SingleOrDefault();
                    var notification_User = new Notification_User();
                    //var listUserId = ItemChat.IdUser2;
                    if (ItemChat.TrangThaiChatNhom == false)
                    {
                        var DataNoti = new MessageViewModel(
                       ItemChat.Id,
                       ItemChat.IdUser1,
                       ItemChat.IdUser2,
                       ItemChat.NoiDung,
                       ItemChat.IsFile,
                       DateTime.Now,
                      new UserChatViewModel(ItemChat.AppUser1.Id, ItemChat.AppUser1.Avatar, ItemChat.AppUser1.FullName),
                       new UserChatViewModel(ItemChat.AppUser2.Id, ItemChat.AppUser2.Avatar, ItemChat.AppUser2.FullName)
                      );
                        string[] listTokenDevice = _device_TokenService.GetAll().Where(x => x.UserId == ItemChat.IdUser2).Select(u => u.Token).ToArray();
                        var listUserId = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser2).Select(u => u.Id).ToList();
                        _InotificationService.sendNotificationToAllUserMessger(DataNoti, ItemChat.IdUser1, ItemChat.IdUser2, ItemChat.NoiDung, listTokenDevice, ItemChat.TenNguoiGui, listUserId, User.Identity.GetUserId(), "news");
                    }
                    else
                    {
                        var listTokenDeviceAll = _device_TokenService.GetAll();
                        List<string> abc = new List<string>();
                        foreach (var item in ItemChat.ListThongTinNhom)
                        {
                            foreach (var item1 in listTokenDeviceAll)
                            {
                                if (item == item1.UserId)
                                {
                                    abc.Add(item1.Token);
                                }
                            }
                        }
                        var DataNoti = new MessageViewModel(
                        ItemChat.Id,
                        ItemChat.IdUser1,
                        ItemChat.IdUser2,
                        ItemChat.NoiDung,
                        ItemChat.IsFile,
                        ItemChat.NgayTao,
                       new UserChatViewModel(ItemChat.AppUser1.Id, ItemChat.AppUser1.Avatar, ItemChat.AppUser1.FullName),
                        new UserChatViewModel(ItemChat.AppUser2.Id, ItemChat.AppUser2.Avatar, ItemChat.AppUser2.FullName)
                       );
                        string[] listTokenDevice = abc.ToArray();
                        var listUserId = AppUserManager.Users.Where(x => x.Id == ItemChat.IdUser2).Select(u => u.Id).ToList();
                        _InotificationService.sendNotificationToAllUserMessger(DataNoti, ItemChat.IdUser1, ItemChat.IdUser2, ItemChat.NoiDung, listTokenDevice, ItemChat.TenNguoiGui, listUserId, User.Identity.GetUserId(), "news");
                    }
                }
                #endregion
                var TrangThaiChat = _trangThaiChatService.GetChatByUserId1AndUserId2(ItemChat.IdUser2, ItemChat.IdUser1);
                TrangThaiChat.TrangThai = false;
                _trangThaiChatService.Update(TrangThaiChat);
                _trangThaiChatService.Save();
                return response;
            });
        }
        [Route("GetDanhSachNhomOrUser")]
        [HttpGet]
        public HttpResponseMessage GetDanhSachNhomOrUsers(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                var userId = User.Identity.GetUserId();
                HttpResponseMessage response = null;
                var LstUserTrangThaiChat = _trangThaiChatService.GetListTrangThaiChatByIdUser(userId);
                var LstNhomChat = _nhomChatAppService.GetNhomChatByUser(userId);
                var LstUser = AppUserManager.Users.Where(x => x.Delete == false && x.Id != userId).Include("NhomDuAnTheoUser").ToList();
                var count = 0;
                List<AppUserViewModel> LstAllUser = new List<AppUserViewModel>();
                foreach (var item in LstNhomChat)
                {
                    item.TrangThai = false;
                    if (LstUserTrangThaiChat.Contains(item.Id.ToString()))
                    {
                        item.TrangThai = true;
                        count = count + 1;
                    }
                }
                foreach (var item in LstUser)
                {
                    item.TrangThai = false;
                    var roles = AppUserManager.GetRoles(item.Id);
                    if (LstUserTrangThaiChat.Contains(item.Id.ToString()))
                    {
                        item.TrangThai = true;
                        count = count + 1;
                    }
                    var applicationUserViewModel = Mapper.Map<AppUser, AppUserViewModel>(item);
                    applicationUserViewModel.Roles = roles;
                    LstAllUser.Add(applicationUserViewModel);
                }
                //var modelVm = Mapper.Map<IEnumerable<ChatApp>, IEnumerable<ChatAppViewModels>>(model);
                response = request.CreateResponse(HttpStatusCode.OK, new { NhomChat = LstNhomChat.OrderByDescending(x => x.TrangThai), ListUser = LstAllUser.OrderByDescending(x => x.TrangThai), Count = count });
                return response;
            });
        }

    }
}
