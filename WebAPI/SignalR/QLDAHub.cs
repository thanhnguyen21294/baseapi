﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Models.GPMB;

namespace WebAPI.SignalR
{
    //Các phương thức kết nối SignalR
    [HubName("QLDAHub")]
    [Authorize]
    public class QLDAHub : Hub
    {
        private readonly static ConnectionMapping<string> _connections =
            new ConnectionMapping<string>();

        //DuAnSignalR
        public static void PushDAToAllUsers(DuAnViewModels message, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            clients.All.addDuAn(message);
        }
        //DuAnSignalR
        public static void PushCommentToAllUsers(CommentViewModels cmt, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            clients.All.newComment(cmt);
        }

        public static void PushGPMB_CommentToAllUsers(GPMB_Comment_ViewModels cmt, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            clients.All.newCommentGPMB(cmt);
        }

        public static void PushVanBanDenCham(int vanbancham, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            clients.All.vanbandencham(vanbancham);
        }
        /// <summary>
        /// Push to a specific user
        /// </summary>
        /// <param name="who"></param>
        /// <param name="message"></param>
        public static void PushDAToUser(string who, DuAnViewModels message, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            foreach (var connectionId in _connections.GetConnections(who))
            {
                clients.Client(connectionId).addChatMessageDA(message);
            }
        }

        /// <summary>
        /// Push to list users
        /// </summary>
        /// <param name="who"></param>
        /// <param name="message"></param>
        public static void PushDAToUsers(string[] whos, DuAnViewModels message, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            for (int i = 0; i < whos.Length; i++)
            {
                var who = whos[i];
                foreach (var connectionId in _connections.GetConnections(who))
                {
                    clients.Client(connectionId).addChatMessageDA(message);
                }
            }

        }

        public static void PushGPMB_File_CommentToAllUsers(GPMB_File_CommentViewModels message, QLDAHub hub)
        {
            IHubConnectionContext<dynamic> clients = GetClients(hub);
            clients.All.newCommentGPMPFile(message);
        }

        private static IHubConnectionContext<dynamic> GetClients(QLDAHub qLDAHub)
        {
            if (qLDAHub == null)
                return GlobalHost.ConnectionManager.GetHubContext<QLDAHub>().Clients;
            else
                return qLDAHub.Clients;
        }

        /// <summary>
        /// Connect user to hub
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            _connections.Add(Context.User.Identity.Name, Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            _connections.Remove(Context.User.Identity.Name, Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            if (!_connections.GetConnections(Context.User.Identity.Name).Contains(Context.ConnectionId))
            {
                _connections.Add(Context.User.Identity.Name, Context.ConnectionId);
            }

            return base.OnReconnected();
        }
    }
}