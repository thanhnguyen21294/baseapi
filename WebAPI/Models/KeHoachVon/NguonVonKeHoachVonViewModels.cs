﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.KeHoachVon
{
    public class NguonVonKeHoachVonViewModels
    {
        public int IdNguonVonKeHoachVon { get; set; }
        public int IdKeHoachVon { get; set; }
        public int IdNguonVonDuAn { get; set; }
        public double GiaTri { get; set; }
        public double GiaTriTabmis { get; set; }
        public string GhiChu { get; set; }
    }
}