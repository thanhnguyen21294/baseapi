﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.KeHoachVon
{
    public class KeHoachVonViewModel
    {
        public int IdKeHoachVon { set; get; }
        public int IdDuAn { set; get; }
        public int? IdKeHoachVonDieuChinh { set; get; }
        public string SoQuyetDinh { set; get; }
        public int? IdCoQuanPheDuyet { set; get; }
        public string NgayPheDuyet { set; get; }
        public int NienDo { set; get; }
        public string TinhChatKeHoach { set; get; }
        public double? TongGiaTri { set; get; }
        public double? XayLap { set; get; }
        public double? ThietBi { set; get; }
        public double? GiaiPhongMatBang { set; get; }
        public double? TuVan { set; get; }
        public double? QuanLyDuAn { set; get; }
        public double? Khac { set; get; }
        public double? DuPhong { set; get; }
        public string GhiChu { set; get; }
        public bool DeXuatDieuChinh { get; set; }
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }

        public virtual ICollection<NguonVonKeHoachVonViewModels> NguonVonKeHoachVons { get; set; }
    }
}