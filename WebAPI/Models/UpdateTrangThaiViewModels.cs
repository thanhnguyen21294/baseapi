﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class UpdateTrangThaiViewModels
    {
        public int ID { get; set; }
        public int TrangThai { get; set; }
    }
}