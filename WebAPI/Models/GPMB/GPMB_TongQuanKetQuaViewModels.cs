﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Models;

namespace WebAPI.Models.GPMB
{
    public class GPMB_TongQuanKetQuaViewModels
    {
        public int IdGPMBTongQuanKetQua { set; get; }
        public int IdDuAn { get; set; }

        public double? DatNNCaTheTuSDOD_DT { set; get; }
        public double? DatNNCaTheTuSDOD_SoHo { set; get; }

        public double? DatNNCong_DT { set; get; }
        public double? DatNNCong_SoHo { set; get; }

        public double? DatKhac_DT { set; get; }
        public double? DatKhac_SoHo { set; get; }

        public double? MoMa { set; get; }

        public double? Tong_DT { set; get; }
        public double? Tong_SoHo { set; get; }

        public string GhiChu { set; get; }

        public virtual Model.Models.DuAn DuAn { get; set; }
    }
}