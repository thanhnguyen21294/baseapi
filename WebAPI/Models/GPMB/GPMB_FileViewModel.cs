﻿using Model.Models;
using System;

namespace WebAPI.Models.GPMB
{
    public class GPMB_FileViewModel
    {
        public int Id { set; get; }

        public int? ParentId { set; get; }

        public string Ten { set; get; }

        public string NoiDung { set; get; }

        public string Mota { set; get; }

        public int TrangThai { set; get; }

        public int? TinhTrang { set; get; }

        public bool IsBossCreated { set; get; }

        public bool PheDuyet { get; set; }

        public DateTime? NgayTinhTrang { get; set; }

        public string FileURL { set; get; }

        public AppUser AppUser { set; get; }
    }
}