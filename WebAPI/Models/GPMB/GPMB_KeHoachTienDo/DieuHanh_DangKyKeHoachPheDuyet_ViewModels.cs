﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class DieuHanh_DangKyKeHoachPheDuyet_ViewModels
    {
        public int IDFile { get; set; }
        public string NoiDung { get; set; } 
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public bool Checked { get; set; }
    }
}