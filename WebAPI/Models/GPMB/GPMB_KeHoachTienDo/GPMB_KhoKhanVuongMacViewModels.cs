﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_KhoKhanVuongMacViewModels
    {
        public int IdKhoKhanVuongMac { set; get; }
        public int IdKeHoachGPMB { get; set; }
        public string NguyenNhanLyDo { set; get; }
        public string GiaiPhapTrienKhai { set; get; }
        public bool DaGiaiQuyet { set; get; }
        public virtual GPMB_KeHoach GPMB_KeHoach { get; set; }
    }
}