﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_KeHoachCongViecViewModels
    {
        public int IdKeHoachCongViec { set; get; }
        public int IdKeHoachGPMB { set; get; }
        public int IdCongViecGPMB { set; get; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public int LoaiCongViec { get; set; }
        public virtual GPMB_KeHoach GPMB_KeHoach { get; set; }
        public virtual GPMB_CongViec GPMB_CongViec { set; get; }
    }
}