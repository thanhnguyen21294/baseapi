﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_KeHoachViewModels
    {
        public int IdKeHoachGPMB { set; get; }
        public int? IdNhomDuAnTheoUser { set; get; }
        public int IdDuAn { get; set; }
        public string NoiDung { set; get; }
        //public string NgayGiaoLapChuTruong { get; set; }
        public bool PheDuyet { get; set; }
        public int TrangThai { get; set; }
        public int? TinhTrang { get; set; }
        public string NgayTinhTrang { get; set; }
        public string GhiChu { set; get; }
        public string CreatedBy { set; get; }
        public virtual AppUser AppUsers { get; set; }
        public virtual ICollection<GPMB_KeHoachCongViecViewModels> GPMB_KeHoachCongViecs { get; set; }
        public virtual Model.Models.DuAn DuAn { get; set; }
    }
}