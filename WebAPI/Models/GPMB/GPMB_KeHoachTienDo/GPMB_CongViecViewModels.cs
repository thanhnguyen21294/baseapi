﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_CongViecViewModels
    {
        public int IdCongViecGPMB { set; get; }
        public string TenCongViec { set; get; }
        public int LoaiCongViec { get; set; }
    }
}