﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_KeHoachTienDoChungViewModels
    {
        public int IdKeHoachTienDoChung { set; get; }
        public int IdKeHoachGPMB { set; get; }
        public string NoiDung { get; set; }
    }
}