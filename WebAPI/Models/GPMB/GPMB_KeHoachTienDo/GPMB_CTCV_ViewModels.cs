﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_CTCV_ViewModels
    {
        public int IdChiTietCongViec { set; get; }
        public int IdKeHoachCongViecGPMB { get; set; }

        public string SoQuyetDinh { get; set; }
        public string NgayPheDuyet { get; set; }

        public string VBGNV_SoVanBan { get; set; }
        public string VBGNV_Ngay { get; set; }

        public string BBBGMG_Ngay { get; set; }

        public string QDTLHD_So { get; set; }
        public string QDTLHD_Ngay { get; set; }
        public string QDTCT_So { get; set; }
        public string QDTCT_Ngay { get; set; }

        public string QDPDDT_So { get; set; }
        public string QDPDDT_NgayBanBanHanh { get; set; }

        public string HHD_Ngay { get; set; }
        public string HDU_Ngay { get; set; }

        public bool QCTDHT_Chon { get; set; }

        public string TBTHD_SoDoiTuong { get; set; }

        public bool HDPTK_Chon { get; set; }

        public string DTKKLBB_SoDoiTuong { get; set; }

        public bool CCKDBB_Chon { get; set; }

        public string LHSGPMB_So { get; set; }

        public string LTHDTT_So { get; set; }

        public string SKKTDTPA_So { get; set; }

        public string THDHTPA_So { get; set; }

        public string QDPDPA_So { get; set; }
        public string QDPDPA_TGT { get; set; }

        public string CTTNBG_So { get; set; }
        public string CTTNBG_TGT { get; set; }

        public bool CCTHD_Chon { get; set; }

        public string QTKP_So { get; set; }
        public string QTKP_Ngay { get; set; }
        public string QTKP_GT { get; set; }

        public string QDGD_So { get; set; }
        public string QDGD_Ngay { get; set; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual GPMB_KeHoachCongViec GPMB_KeHoachCongViec { get; set; }
    }
}