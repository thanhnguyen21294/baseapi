﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class DieuHanh_KeHoachTienDoPheDuyet_ViewModels
    {
        public bool Checked { get; set; }
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public IEnumerable<GPMB_KeHoachCongViec> gpmb_kehoachcongviec { get; set; }
    }
}