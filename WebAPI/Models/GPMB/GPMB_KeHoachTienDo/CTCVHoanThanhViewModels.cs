﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class CTCVHoanThanhViewModels
    {
        public int IDChiTietCV { get; set; }
        public int IDCV { get; set; }
        public string TenCV { get; set; }
        public bool Checked { get; set; }
    }
}