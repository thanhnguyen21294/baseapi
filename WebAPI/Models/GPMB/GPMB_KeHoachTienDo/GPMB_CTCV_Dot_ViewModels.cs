﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB.GPMB_KeHoachTienDo
{
    public class GPMB_CTCV_Dot_ViewModels
    {

        public int IdChiTietCongViec_Dot { set; get; }
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { get; set; }
        public string TBTHD_Ngay { get; set; }
        public string TBTHD_SoDoiTuong { get; set; }

        public string HDPTK_Ngay { get; set; }

        public string DTKT_SoDoiTuong { get; set; }

        public bool CCKDBB_Chon { get; set; }

        public string LHSGPMB_SoDoiTuong { get; set; }

        public string LTHDTT_SoDoiTuong { get; set; }

        public string CKKT_SoDoiTuong { get; set; }

        public string THDTD_SoDoiTuong { get; set; }

        public string QDPDPA_SoDoiTuong { get; set; }
        public string QDPDPA_GiaTri { get; set; }

        public string CTNBG_SoDoiTuong { get; set; }
        public string CTNBG_GiaTri { get; set; }

        public bool CCTHD_Chon { get; set; }

        public virtual GPMB_CTCV GPMB_CTCV { get; set; }
    }
}