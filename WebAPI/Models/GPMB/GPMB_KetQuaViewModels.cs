﻿using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB
{
    public class GPMB_KetQuaViewModels
    {
        public int IdGPMBKetQua { set; get; }

        public double? DHT_DT { set; get; }
        public double? DHT_SoHo { get; set; }
        public double? DHT_TongKinhPhi { get; set; }

        public double? DHT_DatNNCaThe_DT { get; set; }
        public double? DHT_DatNNCaThe_SoHo { get; set; }
        public double? DHT_DatNNCaThe_GiaTri { get; set; }

        public double? DHT_DatNNCong_DT { get; set; }
        public double? DHT_DatNNCong_SoHo { get; set; }
        public double? DHT_DatNNCong_GiaTri { get; set; }

        public double? DHT_DatKhac_DT { get; set; }
        public double? DHT_DatKhac_SoHo { get; set; }
        public double? DHT_DatKhac_GiaTri { get; set; }

        public double? DHT_MoMa { get; set; }
        public double? DHT_MoMa_GiaTri { get; set; }


        public double? CTHX_TongDT { get; set; }
        public double? CTHX_TongSoHo { get; set; }

        public double? CTHX_LapTrinhHoiDong_DuThao_DT { get; set; }
        public double? CTHX_LapTrinhHoiDong_DuThao_SoHo { get; set; }

        public double? CTHX_CongKhai_DuThao_DT { get; set; }
        public double? CTHX_CongKhai_DuThao_SoHo { get; set; }

        public double? CTHX_TrinhHoiDong_ChinhThuc_DT { get; set; }
        public double? CTHX_TrinhHoiDong_ChinhThuc_SoHo { get; set; }

        public double? CTHX_QuyetDinhThuHoi_DT { get; set; }
        public double? CTHX_QuyetDinhThuHoi_SoHo { get; set; }

        public double? CTHX_ChiTraTien_DT { get; set; }
        public double? CTHX_ChiTraTien_SoHo { get; set; }

        public double? KKVM_DT { get; set; }
        public double? KKVM_SoHo { get; set; }
        public double? KKVM_DaPheChuaNhanTien { get; set; }
        public double? KKVM_KhongHopTac { get; set; }
        public double? KKVM_ChuaDieuTra { get; set; }

        public string GhiChu { get; set; }

        public virtual GPMB_TongQuanKetQua GPMB_TongQuanKetQua { get; set; }
        //public int IdDuAn { get; set; }

        //public string TieuDe { get; set; }

        //public string LapTrinhHDKTHoanThienDTPA_DT { set; get; }
        //public double? LapTrinhHDKTHoanThienDTPA_SoHo { set; get; }
        //public double? LapTrinhHDKTHoanThienDTPA_GiaTri { set; get; }

        //public string CongKhaiKTCongKhaiDTPA_DT { set; get; }
        //public double? CongKhaiKTCongKhaiDTPA_SoHo { set; get; }
        //public double? CongKhaiKTCongKhaiDTPA_GiaTri { set; get; }

        //public string TrinhHDTDHoanThienPACT_DT { set; get; }
        //public double? TrinhHDTDHoanThienPACT_SoHo { set; get; }
        //public double? TrinhHDTDHoanThienPACT_GiaTri { set; get; }

        //public string QDThuHoiDatQuyetDinhPDPA_DT { set; get; }
        //public double? QDThuHoiDatQuyetDinhPDPA_SoHo { set; get; }
        //public double? QDThuHoiDatQuyetDinhPDPA_GiaTri { set; get; }

        //public string ChiTraTienNhanBanGiaoDat_DT { set; get; }
        //public double? ChiTraTienNhanBanGiaoDat_SoHo { set; get; }
        //public double? ChiTraTienNhanBanGiaoDat_GiaTri { set; get; }

        //public string DatNNCaTheTuSDOD_DT { set; get; }
        //public double? DatNNCaTheTuSDOD_SoHo { set; get; }
        //public double? DatNNCaTheTuSDOD_GiaTri { set; get; }
        //public string DatNNCong_DT { set; get; }
        //public double? DatNNCong_SoHo { set; get; }
        //public double? DatNNCong_GiaTri { set; get; }
        //public string DatKhac_DT { set; get; }
        //public double? DatKhac_SoHo { set; get; }
        //public double? DatKhac_GiaTri { set; get; }
        //public double? MoMa { set; get; }
        //public double? MoMa_GiaTri { set; get; }

        //public string DaPheNhungChuaNhanTien { set; get; }
        //public string KhongHopTacDieuTraKiemDem { set; get; }
        //public string ChuaDieuTraKiemDem { set; get; }

        //public double? Tong_DT { set; get; }
        //public double? Tong_SoHo { set; get; }
        //public double? Tong_GiaTri { set; get; }
    }
}