﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB
{
    public class GPMB_Comment_ViewModels
    {
        public int IdComment { get; set; }
        public int IdDuAn { get; set; }
        public string UserId { get; set; }
        public string NoiDung { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public virtual AppUserViewModel AppUsers { get; set; }
        public virtual Model.Models.DuAn DuAns { get; set; }
    }
}