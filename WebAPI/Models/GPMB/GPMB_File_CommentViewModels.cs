﻿namespace WebAPI.Models.GPMB
{
    public class GPMB_File_CommentViewModels
    {
        public int Id{ get; set; }
        public int IdFile { get; set; }
        public string UserId { get; set; }
        public string NoiDung { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public virtual AppUserViewModel AppUser { get; set; }
    }
}