﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.GPMB
{
    public class GPMB_KetQua_ChiTietViewModels
    {
        public int IdGPMBKetQuaChiTiet { set; get; }

        public int IdGPMBKetQua { get; set; }

        public string NoiDung { set; get; }

        public string ThayDoi { get; set; }

        public string CreatedBy { set; get; }

        public DateTime? CreatedDate { set; get; }

        public virtual AppUser AppUsers { get; set; }
    }
}