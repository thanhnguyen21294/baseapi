﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class TienDoChiTietViewModels
    {
        public int IdTienDoChiTiet { set; get; }
        public int IdTienDo { set; get; }
        public string NoiDung { set; get; }
        public string NgayThucHien { set; get; }
        public string TonTaiVuongMac { set; get; }
        public string GhiChu { set; get; }
    }
}