﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnQuanLyViewModels
    {
        public int Id { set; get; }
        public int IdDuAn { set; get; }
        public int IdQuanLyDA { set; get; }
        public virtual DuAnViewModels DuAn { get; set; }
        public virtual QuanLyDAViewModels QuanLyDA { get; set; }
    }
}