﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class TienDoThucHienViewModels
    {
        public int IdTienDo { set; get; }
        public int IdDuAn { set; get; }
        public int IdGoiThau { set; get; }
        public int IdHopDong { set; get; }
        public string TenCongViec { set; get; }
        public string TuNgay { set; get; }
        public string DenNgay { set; get; }
        public string GhiChu { set; get; }

        public virtual ICollection<TienDoChiTietViewModels> TienDoChiTiets { get; set; }
    }
}