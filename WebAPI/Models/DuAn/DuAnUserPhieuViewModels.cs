﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnUserPhieuViewModels
    {
        public int IdDuanUser { get; set; }
        public int IdDuAn { get; set; }
        public string UserId { get; set; }
        public bool HasRead { get; set; }
        public virtual AppUserViewModel AppUser { get; set; }
        //public virtual DuAnViewModels DuAn { get; set; }
    }
}