﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class HinhThucDauThauViewModels
    {
        public int IdHinhThucDauThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenHinhThucDauThau { get; set; }

        public string GhiChu { get; set; }

        public bool Status { get; set; }
    }
}