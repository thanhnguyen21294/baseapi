﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebAPI.Models.Common;

namespace WebAPI.Models.DuAn
{
    public class NguonVonDuAnGoiThauViewModels
    {
        public int IdNguonVonDuAnGoiThau { get; set; }

        [Required]
        public int IdGoiThau { get; set; }

        [Required]
        public int IdNguonVonDuAn { get; set; }

        public string GhiChu { get; set; }

        public virtual CommonNguonVonDuAnViewModels NguonVonDuAn { get; set; }
    }
}