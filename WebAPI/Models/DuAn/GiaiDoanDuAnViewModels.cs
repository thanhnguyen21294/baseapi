﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class GiaiDoanDuAnViewModels
    {
        public int IdGiaiDoanDuAn { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenGiaiDoanDuAn { get; set; }

        public string GhiChu { get; set; }

        public bool Status { get; set; }
    }
}