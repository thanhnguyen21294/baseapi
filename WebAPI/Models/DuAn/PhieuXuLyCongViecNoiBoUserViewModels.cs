﻿namespace WebAPI.Models.DuAn
{
    public class PhieuXuLyCongViecNoiBoUserViewModels
    {
        public int IdPhieuXuLyCongViecNoiBoUser { set; get; }
        public int IdPhieuXuLyCongViecNoiBo { set; get; }
        public int UserXLCVNoiBoEnum { get; set; }
        public string FullName { get; set; }
        public bool DaKy { get; set; }
        public string UserId { get; set; }
    }
}