﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class LienKetLinhVucViewModels
    {
        public int IdLienKetLinhVuc { get; set; }
        public string TenLienKetLinhVuc { get; set; }
        public string GhiChu { get; set; }
        public int? Order { get; set; }
    }
}