﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class NguonVonGoiThauTamUngViewModels
    {
        public int IdNguonVonGoiThauTamUng { get; set; }

        public int IdNguonVonDuAnGoiThau { get; set; }
        public int IdTamUng { get; set; }
        public double GiaTri { get; set; }

        public string GhiChu { get; set; }
    }
}