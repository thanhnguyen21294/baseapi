﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class NguonVonGoiThauThanhToanViewModels
    {
        public int IdNguonVonGoiThauThanhToan { get; set; }

       
        public int IdNguonVonDuAnGoiThau { get; set; }

       
        public int IdThanhToanHopDong { get; set; }
        
        public double GiaTri { get; set; }

            public double? GiaTriTabmis { get; set; }
        public double TamUng { get; set; }

        public string GhiChu { get; set; }
    }
}