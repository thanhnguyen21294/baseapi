﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Models;

namespace WebAPI.Models.DuAn
{
    public class ChuTruongDauTuViewModels
    {
        public int IdChuTruongDauTu { get; set; }
        public int IdDuAn { get; set; }
        public string SoVanBan { get; set; }
        public int? IdCoQuanPheDuyet { get; set; }
        public string NgayPheDuyet { get; set; }
        public string NguoiPheDuyet { get; set; }
        public double? TongMucDauTu { get; set; }
        public double? KinhPhi { get; set; }
        public string LoaiDieuChinh { get; set; }
        public string GhiChu { get; set; }
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
    }
}