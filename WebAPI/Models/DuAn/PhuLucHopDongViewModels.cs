﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class PhuLucHopDongViewModels
    {
        public int IdPhuLuc { set; get; }

        public int IdHopDong { set; get; }

        [MaxLength(255)]
        [Required]
        public string TenPhuLuc { set; get; }
        public string NgayKy { set; get; }
        public double? GiaHopDongDieuChinh { set; get; }
        public string NgayHoanThanhDieuChinh { set; get; }
        public string NoiDung { set; get; }
        public string GhiChu { set; get; }
    }
}