﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class CoQuanPheDuyetViewModels
    {
        public int IdCoQuanPheDuyet { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenCoQuanPheDuyet { get; set; }

        public string GhiChu { get; set; }

        public bool Status { get; set; }
    }
}