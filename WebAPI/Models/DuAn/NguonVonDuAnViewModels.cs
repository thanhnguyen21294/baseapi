﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class NguonVonDuAnViewModels
    {
        public int IdNguonVonDuAn { get; set; }

        public int IdDuAn { get; set; }

        public int IdNguonVon { get; set; }
        public double? GiaTriTrungHan { get; set; }

        public double? GiaTri { get; set; }

        public int? IdLapQuanLyDauTu { get; set; }

        public string GhiChu { get; set; }
    }
}