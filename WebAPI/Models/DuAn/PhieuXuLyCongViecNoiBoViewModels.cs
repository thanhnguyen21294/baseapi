﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class PhieuXuLyCongViecNoiBoViewModels
    {
        public int IdPhieuXuLyCongViecNoiBo { set; get; }
        public int? TrangThai { get; set; }
        public int? IDPhieuCha { get; set; }
        public string TenCongViec { get; set; }
        public string NoiDung { get; set; }
        public DateTime? NgayNhan { set; get; }
        public DateTime? NgayChuyen { set; get; }
        public int? IdDuAn { get; set; }
        public string GhiChu { set; get; }
        public string CreatedBy { set; get; }

        public virtual ICollection<PhieuXuLyCongViecNoiBoUserViewModels> PhieuXuLyCongViecNoiBoUsers { get; set; }
    }
}