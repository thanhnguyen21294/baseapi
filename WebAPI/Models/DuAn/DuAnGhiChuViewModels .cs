﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnGhiChuViewModels
    {
        public int IdDuAn { set; get; }
        public string GhiChuKeToan { get; set; }
    }
}