﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ThanhToanChiPhiKhacViewModels
    {
        public int IdThanhToanChiPhiKhac { set; get; }
        public int IdDuAn { set; get; }
        public string NoiDung { set; get; }
        public int? IdNhaThau { set; get; }
        public int? IdLoaiChiPhi { set; get; }
        public string LoaiThanhToan { set; get; }
        public string NgayThanhToan { set; get; }
        public string GhiChu { set; get; }
        public double TongGiaTriNguonVon { get; set; }
        public int TrangThaiGui { set; get; }
        public IEnumerable<string> TenNguonVons { get; set; }
        public virtual DanhMucNhaThau DanhMucNhaThau { set; get; }
        public virtual LoaiChiPhi LoaiChiPhi { set; get; }
        public virtual ICollection<NguonVonThanhToanChiPhiKhac> NguonVonThanhToanChiPhiKhacs { set; get; }
    }
}