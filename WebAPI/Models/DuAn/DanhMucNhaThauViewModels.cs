﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DanhMucNhaThauViewModels
    {
        public int IdNhaThau { get; set; }

        [Required]
        public string TenNhaThau { get; set; }

        public string DiaChiNhaThau { get; set; }

        public string MoTa { get; set; }

        public int IdDuAn { get; set; }
    }
}