﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnHoSoQuyetToanViewModels
    {
        public int IdDanhMucHoSoQuyetToan { get; set; }
        public int IdDuAn { get; set; }
        public int? KyThuat { get; set; }
        public int? KeToan { get; set; }
        public string HoanThien { get; set; }
    }
}