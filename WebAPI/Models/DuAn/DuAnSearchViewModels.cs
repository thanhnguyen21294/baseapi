﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnSearchViewModels
    {
        public int IdDuAn { get; set; }
        public string TenDuAn { get; set; }
        public string TenNhomDuAn { get; set; }
        public string TenGiaiDoanDuAn { get; set; }
        public int? IdGiaiDoanDuAn { get; set; }
        public string Ma { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string DuAnUsers { get; set; }
        public string DiaDiem { get; set; }
        public int? IdDuAnCha { get; set; }
        public int? IdNhomDuAnTheoUser { get; set; }
        public int? IdLinhVucNganhNghe { get; set; }
        public int? IdChuDauTu { get; set; }
        public int? IdNguonVonDuAn { get; set; }
        public int? IdHinhThucQuanLy { get; set; }
        public int? IdPhongBan { get; set; }
        public int? IdTinhTrangDuAn { get; set; }
        public string DuAnUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}