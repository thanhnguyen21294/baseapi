﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class TamUngHopDongViewModels
    {
        public int IdTamUng { set; get; }
        public int IdHopDong { set; get; }
        public string NoiDung { set; get; }
        public string NgayTamUng { set; get; }
        public string GhiChu { set; get; }
        public double TongGiaTriTamUng { get; set; }
        public IEnumerable<string> TenNguonVons { get; set; }
        public virtual ICollection<NguonVonGoiThauTamUng> NguonVonGoiThauTamUngs { set; get; }
    }
}