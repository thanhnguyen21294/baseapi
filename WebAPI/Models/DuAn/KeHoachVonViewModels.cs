﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class KeHoachVonViewModels
    {
        public int IdKeHoachVon { get; set; }

    
        public int IdDuAn { get; set; }

        public int? IdKeHoachVonDieuChinh { get; set; }

       
        public string SoQuyetDinh { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        public int? NienDo { get; set; }

       
        public string TinhChatKeHoach { get; set; }

        public double? TongGiaTri { get; set; }

        public double? XayLap { get; set; }

        public double? ThietBi { get; set; }

        public double? GiaiPhongMatBang { get; set; }

        public double? TuVan { get; set; }

        public double? QuanLyDuAn { get; set; }

        public double? Khac { get; set; }

        public double? DuPhong { get; set; }

        public string GhiChu { get; set; }

        public bool DeXuatDieuChinh { get; set; }
    }
}