﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class GroupThucHienHopDongViewModel
    {
        public string Ten { get; set; }

        public IEnumerable<ThucHienHopDongViewModels> ThucHienHopDongs { get; set; }
    }
}