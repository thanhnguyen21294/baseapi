﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class CamKetGiaiNganViewModels
    {
        public int IdCamKetGiaiNgan { get; set; }
        public int NamCK { get; set; }
        public int IdDuAn { get; set; }
        public double? Thang1 { get; set; }
        public double? Thang2 { get; set; }
        public double? Thang3 { get; set; }
        public double? Thang4 { get; set; }
        public double? Thang5 { get; set; }
        public double? Thang6 { get; set; }
        public double? Thang7 { get; set; }
        public double? Thang8 { get; set; }
        public double? Thang9 { get; set; }
        public double? Thang10 { get; set; }
        public double? Thang11 { get; set; }
        public double? Thang12 { get; set; }
        public double? Quy1 { get; set; }
        public double? Quy2 { get; set; }
        public double? Quy3 { get; set; }
        public double? Quy4 { get; set; }
        public double? TongCong { get; set; }
        public string GhiChu { get; set; }

        public int TrangThaiGui { set; get; }
    }
}