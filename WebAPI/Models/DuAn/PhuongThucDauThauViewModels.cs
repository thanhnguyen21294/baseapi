﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class PhuongThucDauThauViewModels
    {
        public int IdPhuongThucDauThau { set; get; }

        [MaxLength(255)]
        [Required]
        public string TenPhuongThucDauThau { set; get; }

        public string GhiChu { set; get; }

        public bool Status { get; set; }
    }
}