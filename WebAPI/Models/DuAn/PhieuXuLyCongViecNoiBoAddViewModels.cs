﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class PhieuXuLyCongViecNoiBoAddViewModels
    {
        public int IdPhieuXuLyCongViecNoiBo { set; get; }
        public string TenCongViec { get; set; }
        public string NoiDung { get; set; }
        public int? IdDuAn { get; set; }
        public int? TrangThai { get; set; }
        public int? IDPhieuCha { get; set; }
        public string GhiChu { set; get; }

        public List<PhieuXuLyCongViecNoiBoUserAddViewModels> Users { get; set; }

    }

    public class PhieuXuLyCongViecNoiBoUserAddViewModels {

        public string UserId { set; get; }
        public string FullName { set; get; }
        public bool? Checked { get; set; }
        public int Loai { set; get; }
    }

}