﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class QuyetToanDuAnViewModels
    {
        public int IdQuyetToanDuAn { set; get; }
        public int IdDuAn { set; get; }
        public string NoiDung { set; get; }
        public string NgayKyBienBan { set; get; }
        public string NgayTrinh { set; get; }
        public string NgayNhanDuHoSo { set; get; }
        public string NgayThamTra { set; get; }
        public string NgayPheDuyetQuyetToan { set; get; }
        public string SoPheDuyet { set; get; }
        public int? IdCoQuanPheDuyet { set; get; }
        public double? TongGiaTri { set; get; }
        public double? XayLap { set; get; }
        public double? ThietBi { set; get; }
        public double? GiaiPhongMatBang { set; get; }
        public double? TuVan { set; get; }
        public double? QuanLyDuAn { set; get; }
        public double? Khac { set; get; }
        public double? DuPhong { set; get; }
        public string GhiChu { set; get; }
        public int TrangThaiGui { set; get; }
        public virtual CoQuanPheDuyet CoQuanPheDuyet { set; get; }
        public virtual ICollection<NguonVonQuyetToanDuAn> NguonVonQuyetToanDuAns { set; get; }
    }
}