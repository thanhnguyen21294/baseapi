﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class KeHoachLuaChonNhaThauViewModels
    {
        public int IdKeHoachLuaChonNhaThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string NoiDung { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public int? IdDieuChinhKeHoachLuaChonNhaThau { get; set; }

        public string NgayTrinh { get; set; }

        [MaxLength(255)]
        public string SoVanBanTrinh { get; set; }

        public string NgayNhanDuHoSo { get; set; }

        public string NgayThamDinh { get; set; }

        public string NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        [MaxLength(50)]
        public string NguoiPheDuyet { get; set; }

        [MaxLength(50)]
        public string NgayDangTaiThongTin { get; set; }

        [MaxLength(50)]
        public string PhuongTienDangTai { get; set; }

        [MaxLength(50)]
        public string ToChucCaNhanGiamSat { get; set; }
        public string GhiChu { get; set; }

        public virtual ICollection<GoiThauViewModels> GoiThaus { get; set; }
        public virtual CoQuanPheDuyetViewModels CoQuanPheDuyet { get; set; }
    }
}