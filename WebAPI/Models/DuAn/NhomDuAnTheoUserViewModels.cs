﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Models;

namespace WebAPI.Models.DuAn
{
    public class NhomDuAnTheoUserViewModels
    {
        public int IdNhomDuAn { set; get; }
        public string TenNhomDuAn { set; get; }
        public string GhiChu { set; get; }
        public bool PhongChucNang { set; get; }
        public virtual ICollection<AppUser> AppUsers { set; get; }
        public virtual ICollection<Model.Models.DuAn> DuAns { set; get; }
        public bool Status { set; get; }
    }
}