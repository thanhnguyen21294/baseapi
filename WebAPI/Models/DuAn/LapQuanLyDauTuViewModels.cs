﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class LapQuanLyDauTuViewModels
    {
        public int IdLapQuanLyDauTu { get; set; }
        public int LoaiQuanLyDauTu { get; set; }
        public int IdDuAn { get; set; }
        public int? IdCoQuanPheDuyet { get; set; }
        public string NoiDung { get; set; }
        public string NgayTrinh { get; set; }
        public string SoToTrinh { get; set; }
        public double? GiaTriTrinh { get; set; }
        public string NgayPheDuyet { get; set; }
        public string SoQuyetDinh { get; set; }
        public string NguoiPheDuyet { get; set; }
        public int? LoaiDieuChinh { get; set; }
        public double? TongGiaTri { get; set; }
        public double? XayLap { get; set; }
        public double? ThietBi { get; set; }
        public double? GiaiPhongMatBang { get; set; }
        public double? TuVan { get; set; }
        public double? QuanLyDuAn { get; set; }
        public double? Khac { get; set; }
        public double? DuPhong { get; set; }
        public string GhiChu { get; set; }
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
        public virtual ICollection<NguonVonDuAn> NguonVonDuAns { get; set; }

    }
}