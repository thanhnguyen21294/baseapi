﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class TinhTrangDuAnViewModels
    {
        public int IdTinhTrangDuAn { get; set; }

        public int? IdTinhTrangDuAnCha { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenTinhTrangDuAn { get; set; }

        public string GhiChu { get; set; }
        public int? ThuTu { get; set; }
        public bool Status { get; set; }

        public virtual TinhTrangDuAnViewModels TinhTrangDuAnCha { set; get; }
    }
}