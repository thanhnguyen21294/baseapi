﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ThucHienHopDongViewModels
    {
        public int IdThucHien { set; get; }
        public int? IdHopDong { set; get; }
        public int? IdDuAn { set; get; }
        public int? IdLoaiChiPhi { set; get; }
        public string NoiDung { set; get; }
        public string ThoiDiemBaoCao { set; get; }
        public string UocDenNgay { set; get; }
        public double? KhoiLuong { set; get; }
        public double? UocThucHien { set; get; }
        public string NgayNghiemThu { set; get; }
        public double? GiaTriNghiemThu { set; get; }
        public int TrangThaiGui { set; get; }
        public string GhiChu { set; get; }
        public virtual HopDongViewModels HopDong { set; get; }
        public virtual LoaiChiPhiViewModels LoaiChiPhi { set; get; }
    }
}