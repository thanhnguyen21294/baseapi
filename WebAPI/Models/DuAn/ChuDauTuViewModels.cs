﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ChuDauTuViewModels
    {
        public int IdChuDauTu { set; get; }

        [Required]
        [MaxLength(255)]
        public string TenChuDauTu { set; get; }

        [MaxLength(255)]
        public string DiaChi { set; get; }

        public string GhiChu { set; get; }

        public bool Status { get; set; }
    }
}