﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class UpdateDuAnUserViewModels
    {
        public int IdDuAn { get; set; }
        public string[] lstIdUser { get; set; }
    }
}