﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class HoSoMoiThauViewModels
    {
        public int IdHoSoMoiThau { get; set; }

        public string NoiDung { get; set; }

        public int? IdDuAn { get; set; }

        public int? IdGoiThau { get; set; }

        public string NgayTrinh { get; set; }

        [MaxLength(50)]
        public string SoVanBanTrinh { get; set; }

        public string NgayNhanDu { get; set; }

        public string NgayThamDinh { get; set; }

        public string NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public string NgayDangTai { get; set; }

        [MaxLength(50)]
        public string SoDangTai { get; set; }

        [MaxLength(255)]
        public string PhuongTienDangTai { get; set; }

        public string NgayPhatHanh { get; set; }

        [MaxLength(50)]
        public string SoPhatHanh { get; set; }

        public string NgayHetHanHieuLuc { get; set; }

        public string NgayHetHanBaoLanh { get; set; }

        public string NgayLapBienBan { get; set; }

        public string GhiChu { get; set; }
    }
}