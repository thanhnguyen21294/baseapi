﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DanhMucHoSoQuyetToanViewModels
    {
        public int IdDanhMucHoSoQuyetToan { get; set; }
        public int? GiaiDoan { get; set; }
        public int? IdParent { get; set; }
        public string TenDanhMucHoSoQuyetToan { get; set; }
        public string GhiChu { get; set; }
        public int? Order { get; set; }
    }
}