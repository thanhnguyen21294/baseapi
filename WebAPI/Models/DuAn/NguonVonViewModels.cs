﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAPI.Models.DuAn
{
    public class NguonVonViewModels
    {
        public int IdNguonVon { get; set; }
        public string Ma { get; set; }
        public string TenNguonVon { get; set; }
        public string Loai { get; set; }
        public string Nhom { get; set; }
        public int? IdNguonVonCha { get; set; }
        public string Ghichu { get; set; }
        public bool Status { get; set; }
    }
}