﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ThuMucMacDinhViewModel
    {
        public int IdThuMucMacDinh { set; get; }
        public string TenThuMuc { set; get; }
        public int ? IdThuMucMacDinhCha { get; set; }
    }
}