﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ThamDinhLapQuanLyDauTuViewModels
    {
       
        public int IdThamDinh { set; get; }
        public int IdLapQuanLyDauTu { set; get; }
       
        public string NoiDung { get; set; }
        public string NgayNhanDuHoSo { set; get; }
        public string NgayPheDuyet { set; get; }
    
        public string ToChucNguoiThamDinh { set; get; }
        public string DanhGia { set; get; }
        public string GhiChu { set; get; }
    }
}