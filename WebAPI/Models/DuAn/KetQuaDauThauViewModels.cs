﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class KetQuaDauThauViewModels
    {
        public int IdKetQuaLuaChonNhaThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string NoiDung { get; set; }
        public int IdDuAn { get; set; }
        public int IdGoiThau { get; set; }

        public string NgayLapBienBanMoThau { get; set; }

        public string NgayLapBaoCaoDanhGia { get; set; }

        public string NgayLapBienBanThuongThao { get; set; }

        public string NgayLapBaoCaoThamDinh { get; set; }

        public string NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public string NgayDangTai { get; set; }

        [MaxLength(50)]
        public string SoDangTai { get; set; }

        [MaxLength(50)]
        public string PhuongTienDangTai { get; set; }

        public string NgayGuiThongBao { get; set; }

        public double? GiaDuThau { get; set; }

        public double? GiaTrungThau { get; set; }

        public int? IdNhaThau { get; set; }

        public string GhiChu { get; set; }
        public virtual DanhMucNhaThauViewModels DanhMucNhaThau { get; set; }
    }
}