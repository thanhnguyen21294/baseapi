﻿using System;

namespace WebAPI.Models.DuAn
{
    public class BaoLanhTHHDViewModels
    {
        public int IdBaoLanh { set; get; }

        public int IdDuAn { set; get; }
        public int IdHopDong { set; get; }

        public int LoaiBaoLanh { set; get; }

        public string SoBaoLanh { set; get; }

        public string NgayBaoLanh { set; get; }

        public string NgayHetHan { set; get; }

        public double? SoTien { get; set; }

        public string GhiChu { set; get; }
        public int TrangThaiGui { set; get; }

        public virtual HopDongViewModels HopDong { set; get; }
    }
}