﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class HinhThucDauTuViewModels
    {
        public int IdHinhThucDauTu { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenHinhThucDauTu { get; set; }

        public string GhiChu { get; set; }

        public bool Status { get; set; }
    }
}