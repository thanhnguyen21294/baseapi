﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class ThuMucViewModel
    {
       
        public int IdThuMuc { set; get; }
        public string TenThuMuc { set; get; }
        public int? IdThuMucCha { set; get; }
        public int SoLuongVB { get; set; }
        public int? IdThuMucMacDinh { get; set; }
        public int IdDuAn { get; set; }
    }
}