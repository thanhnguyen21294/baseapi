﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class QuyetToanHopDongViewModels
    {
        public int IdQuyetToan { set; get; }
        public int IdHopDong { set; get; }
        public string NoiDung { set; get; }
        public string NgayQuyetToan { set; get; }
        public double? GiaTriQuyetToan { set; get; }
        public int TrangThaiGui { set; get; }
        public string GhiChu { set; get; }
    }
}