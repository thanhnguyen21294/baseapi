﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class GoiThauViewModels
    {
        public int IdGoiThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenGoiThau { get; set; }

        public int IdDuAn { get; set; }

        public int IdKeHoachLuaChonNhaThau { get; set; }

        public double? GiaGoiThau { get; set; }

        public int? LoaiGoiThau { get; set; }

        public int? IdLinhVucDauThau { get; set; }

        public int? IdHinhThucLuaChon { get; set; }

        public int? IdPhuongThucDauThau { get; set; }

        public int? HinhThucDauThau { get; set; }

        public int? LoaiDauThau { get; set; }

        public int? IdLoaiHopDong { get; set; }

        [MaxLength(50)]
        public string ThoiGianThucHienHopDong { get; set; }

        public string GhiChu { set; get; }

        public virtual ICollection<NguonVonDuAnGoiThauViewModels> NguonVonDuAnGoiThaus { get; set; }
        public virtual ICollection<KetQuaDauThauViewModels> KetQuaDauThaus { set; get; }
        public virtual ICollection<HoSoMoiThauViewModels> HoSoMoiThaus { set; get; }
        public virtual HinhThucLuaChonNhaThauViewModels HinhThucLuaChonNhaThau { set; get; }
        public virtual PhuongThucDauThauViewModels PhuongThucDauThau { set; get; }
        public virtual ICollection<HopDongViewModels> HopDongs { set; get; }
    }
}