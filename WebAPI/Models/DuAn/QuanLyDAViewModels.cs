﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class QuanLyDAViewModels
    {
        public int IdQuanLyDA { set; get; }
        public string NoiDung { set; get; }
        public string GhiChu { set; get; }
    }
}