﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class LoaiChiPhiViewModels
    {
        public int IdLoaiChiPhi { get; set; }
        public int? IdLoaiChiPhiCha { get; set; }
        public string TenLoaiChiPhi { get; set; }
        public string GhiChu { get; set; }
        public int? Order { get; set; }
    }
}