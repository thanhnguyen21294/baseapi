﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class HopDongViewModels
    {
        public int IdHopDong { get; set; }
        public string TenHopDong { get; set; }
        public int IdDuAn { get; set; }
        public int IdGoiThau { get; set; }
        public string SoHopDong { get; set; }
        public string NgayKy { get; set; }
        public int? ThoiGianThucHien { get; set; }
        public int? IdLoaiHopDong { get; set; }
        public double? GiaHopDong { get; set; }
        public double? GiaHopDongDieuChinh { get; set; }
        public string NoiDungVanTat { get; set; }
        public int? IdNhaThau { get; set; }
        public string NgayBatDau { get; set; }

        public string NgayHoanThanh { get; set; }

        public string NgayHoanThanhDieuChinh { get; set; }

        public string NgayThanhLyHopDong { get; set; }

        public string GhiChu { get; set; }
        public virtual ICollection<PhuLucHopDong> PhuLucHopDongs { get; set; }
        public virtual ICollection<ThucHienHopDong> ThucHienHopDongs { get; set; }
        public virtual ICollection<TamUngHopDong> TamUngHopDongs { get; set; }
        public virtual ICollection<ThanhToanHopDong> ThanhToanHopDongs { get; set; }
        public virtual ICollection<TienDoThucHien> TienDoThucHiens { get; set; }
        public virtual DanhMucNhaThauViewModels DanhMucNhaThau { set; get; }
        public virtual ICollection<QuyetToanHopDong> QuyetToanHopDongs { get; set; }
        public virtual GoiThauViewModels GoiThau { get; set; }
    }    
}