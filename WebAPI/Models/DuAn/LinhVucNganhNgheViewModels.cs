﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class LinhVucNganhNgheViewModels
    {
        public int IdLinhVucNganhNghe { get; set; }

        [Required]
        [MaxLength(255)]
        public string Ma { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenLinhVucNganhNghe { get; set; }

        [MaxLength(255)]
        public string KhoiLinhVuc { get; set; }

        public string GhiChu { get; set; }

        public int? IdLienKetLinhVuc { set; get; }
        public int? Order { get; set; }

        public bool Status { get; set; }

        public virtual ICollection<Model.Models.DuAn> DuAns { get; set; }
    }
}