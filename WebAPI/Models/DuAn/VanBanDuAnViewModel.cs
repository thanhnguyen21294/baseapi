﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class VanBanDuAnViewModel
    {
        public int IdVanBan { set; get; }
        public int IdThuMuc { set; get; }
        public int IdDuAn { get; set; }
        public string SoVanBan { set; get; }
        
        public string TenVanBan { set; get; }
       
        public string FileName { set; get; }
        public string FileUrl { set; get; }
        public string  NgayBanHanh { set; get; }
        public string CoQuanBanHanh { set; get; }
        public string MoTa { set; get; }
        public string GhiChu { set; get; }
    }
}