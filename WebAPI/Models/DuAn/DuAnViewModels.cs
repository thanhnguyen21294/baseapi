﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class DuAnViewModels
    {
        public int IdDuAn { set; get; }
        public int? IdNhomDuAnTheoUser { set; get; }

        [MaxLength(50)]
        [Required]
        public string Ma { set; get; }

        [MaxLength(255)]
        public string TenDuAn { set; get; }

        public int? IdDuAnCha { get; set; }

        public int? IdChuDauTu { set; get; }

        [MaxLength(50)]
        public string ThoiGianThucHien { get; set; }

        public int? IdLinhVucNganhNghe { get; set; }

        public string DiaDiem { get; set; }
        public double KinhDo { set; get; }
        public double ViDo { set; get; }

        public string MucTieu { get; set; }

        public string QuyMoNangLuc { get; set; }

        public int? IdNhomDuAn { get; set; }

        public int? IdLoaiCapCongTrinh { get; set; }

        public int? IdHinhThucDauTu { get; set; }

        public int? IdGiaiDoanDuAn { get; set; }

        public int? IdHinhThucQuanLy { get; set; }

        public int? IdTinhTrangDuAn { get; set; }

        public string NgayKhoiCongKeHoach { get; set; }

        public string NgayKetThucKeHoach { get; set; }

        public string NgayKhoiCongThucTe { get; set; }

        public string NgayKetThucThucTe { get; set; }

        public double TongMucDauTu { get; set; }
        public double? TongMucDauTu_XL { get; set; }
        public double? TongMucDauTu_TB { get; set; }
        public double? TongMucDauTu_GPMB { get; set; }
        public double? TongMucDauTu_TV { get; set; }
        public double? TongMucDauTu_QLDA { get; set; }
        public double? TongMucDauTu_K { get; set; }
        public double? TongMucDauTu_DP { get; set; }

        public double? TongDuToan { get; set; }
        public double? TongDuToan_XL { get; set; }
        public double? TongDuToan_TB { get; set; }
        public double? TongDuToan_GPMB { get; set; }
        public double? TongDuToan_TV { get; set; }
        public double? TongDuToan_QLDA { get; set; }
        public double? TongDuToan_K { get; set; }
        public double? TongDuToan_DP { get; set; }

        public int? Order { get; set; }

        public int? OrderTemp { get; set; }

        public string GhiChu { get; set; }
        public string GhiChuKeToan { get; set; }
        public virtual NhomDuAnViewModels NhomDuAn { get; set; }
        public virtual GiaiDoanDuAnViewModels GiaiDoanDuAn { get; set; }
        public virtual ICollection<NguonVonDuAnViewModels> NguonVonDuAns { get; set; }
        public virtual ICollection<KeHoachVonViewModels> KeHoachVons { get; set; }
        public virtual ICollection<LapQuanLyDauTuViewModels> LapQuanLyDauTus { get; set; }
        public virtual ICollection<DuAnUserViewModels> DuAnUsers { get; set; }
        public virtual ICollection<DuAnQuanLyViewModels> DuAnQuanLys { get; set; }
        ////SignalR
        //public DuAnViewModels()
        //{
        //    DuAnUsers = new List<DuAnUserViewModels>();
        //}

        //public DateTime? CreatedDate { get; set; }

        //public string UserId { get; set; }

        //public AppUserViewModel AppUser { get; set; }

        //public bool Status { get; set; }
        public PhanHoi PhanHoi { get; set; }

    }
}