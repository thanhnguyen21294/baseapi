﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class ThanhToanHopDongViewModels
    {
        public int IdThanhToanHopDong { set; get; }
        public int IdHopDong { set; get; }
        public string NoiDung { set; get; }
        public int? IdThucHien { set; get; }
        public double? DeNghiThanhToan { set; get; }
        public string ThoiDiemThanhToan { set; get; }
        public string GhiChu { set; get; }
        public double TongGiaTriThanhToan { get; set; }
        public double TongGiaTriThuHoiUng { get; set; }
        public int TrangThaiGui { set; get; }
        public IEnumerable<string> TenNguonVons { get; set; }
        public IEnumerable<NguonVon> NguonVons { get; set; }
        public virtual HopDongViewModels HopDong { get; set; }
        public virtual ThucHienHopDong ThucHienHopDong { set; get; }
        public virtual ICollection<NguonVonGoiThauThanhToan> NguonVonGoiThauThanhToans { set; get; }
    }
}