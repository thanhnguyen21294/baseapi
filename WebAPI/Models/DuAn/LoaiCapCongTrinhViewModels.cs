﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.DuAn
{
    public class LoaiCapCongTrinhViewModels
    {
        public int IdLoaiCapCongTrinh { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenLoaiCapCongTrinh { get; set; }

        public string GhiChu { get; set; }
        public bool Status { set; get; }
    }
}