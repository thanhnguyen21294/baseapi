﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_TDTH_ChuanBiThucHienViewModels
    {
        public int IdTienDoThucHien { set; get; }
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { set; get; }

        public string TenDonViThucHien { set; get; }

        public string DonViThamDinh { set; get; }

        public string DonViPhoiHop { set; get; }

        public string CoQuanPheDuyet { set; get; }

        public string NgayBatDau { set; get; }

        public string NgayHoanThanh { set; get; }
        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt

        public string GhiChu { set; get; }

        public virtual QLTD_CTCV_ChuanBiThucHien QLTD_CTCV_ChuanBiThucHien { get; set; }
    }
}