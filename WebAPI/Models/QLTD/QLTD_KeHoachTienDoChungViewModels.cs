﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_KeHoachTienDoChungViewModels
    {
        public int IdKeHoachTienDoChung { set; get; }
        public int IdKeHoach { set; get; }
        public string NoiDung { get; set; }

        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}