﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Models.DuAn;

namespace WebAPI.Models.QLTD
{
    public class VonCapNhatKLNTViewModels
    {
        public int Id { set; get; }

        public string KLNTVonHuyen { get; set; }
        public string TDGNVonHuyen { get; set; }
        public string KLNTVonTP { get; set; }
        public string TDGNVonTP { get; set; }
        //public string TenDuAn { get; set; }
        public int? IdDuAn { get; set; }

        public int? IdFile { get; set; }

        public DuAnViewModels DuAn { get; set; }

        public CapNhatKLNTViewModels CapNhatKLNT { get; set; }
    }
}