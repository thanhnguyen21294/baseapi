﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Models;

namespace WebAPI.Models.QLTD
{
    public class QLTD_KeHoachCongViecViewModels
    {
        public int IdKeHoachCongViec { set; get; }
        public int IdKeHoach { set; get; }
        public int IdCongViec { set; get; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_CongViec QLTD_CongViec { set; get; }
    }
}