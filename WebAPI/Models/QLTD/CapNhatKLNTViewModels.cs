﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class CapNhatKLNTViewModels
    {
        public int IdFile { set; get; }

        public string FileName { set; get; }

        public string FileUrl { set; get; }

        public DateTime? CreatedDate { set; get; }

        public string CreatedBy { set; get; }
    }
}