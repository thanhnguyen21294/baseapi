﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Models;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CTCV_ChuanBiDauTuViewModels
    {
        public int IdChiTietCongViec { set; get; }

        public int IdKeHoach { get; set; }
        public int IdCongViec { get; set; }
        public int IdKeHoachCongViec { get; set; }

        public string BCPATK_NoiDung { set; get; }

        public string PDDT_SoQD { set; get; }
        public string PDDT_NgayPQ { set; get; }
        public double? PDDT_GiaTri { set; get; }

        public string PDKHLCNT_SoQD { set; get; }
        public string PDKHLCNT_NgayPD { set; get; }
        public double? PDKHLCNT_GiaTri { set; get; }

        public string KSHT_NoiDung { set; get; }

        public string CGDD_NoiDung { set; get; }

        public string SLHTKT_NoiDung { set; get; }

        public bool PDQH_KhongPheDuyet { set; get; }
        public string PDQH_SoQD_QHCT { set; get; }
        public string PDQH_NgayPD_QHCT { set; get; }
        public string PDQH_SoQD_QHTMB { set; get; }
        public string PDQH_NgayPD_QHTMB { set; get; }

        public string KSDCDH_NoiDung { set; get; }

        public string TTTDPCCC_NoiDung { set; get; }

        public string TTK_NoiDung { set; get; }

        public string LHSCBDT_KhoKhanVuongMac { set; get; }
        public string LHSCBDT_KienNghiDeXuat { set; get; }
        public string LHSCBDT_NgayHoanThanh { set; get; }
        public string LHSCBDT_SoQD { set; get; }
        public string LHSCBDT_NgayLaySo { set; get; }

        public string THSCBDT_SoToTrinh { set; get; }
        public string THSCBDT_NgayTrinh { set; get; }

        public string PDDA_BCKTKT_SoQD { set; get; }
        public string PDDA_BCKTKT_NgayPD { set; get; }
        public double? PDDA_BCKTKT_GiaTri { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual QLTD_CongViec QLTD_CongViec { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }
    }
}