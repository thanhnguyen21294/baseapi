﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CTCV_ChuTruongDauTuViewModels
    {
        public int IdChiTietCongViec { set; get; }
        public int IdKeHoach { get; set; }
        public int IdCongViec { get; set; }
        public int IdKeHoachCongViec { get; set; }

        public bool CTDD_CoSanDiaDiem { set; get; }
        public string CTDD_SoVB { set; get; }
        public string CTDD_NgayPD { set; get; }//date

        public string LCTDT_TienDoThucHien { set; get; }
        public string LCTDT_KhoKhanVuongMac { set; get; }
        public string LCTDT_KienNghiDeXuat { set; get; }
        public string LCTDT_NgayHT { set; get; }//date

        public string THSPDDA_NgayTrinhThucTe { set; get; }//date

        public string PQCTDT_SoQD { set; get; }
        public string PQCTDT_NgayLaySo { set; get; }//date
        public double? PQCTDT_GiaTri { set; get; }
        public string PQCTDT_NgayPheDuyetTT { set; get; }//date
        public int? PQCTDT_CoQuanPD { set; get; }
        public string BTGNV_SoToTrinh { set; get; }
        public string BTGNV_SoBaoCao { set; get; }
        public string BTGNV_NgayTrinh { set; get; }

        public string HGNV_SoToTrinh { set; get; }
        public string HGNV_SoBaoCao { set; get; }
        public string HGNV_NgayTrinh { set; get; }
        public string HGNV_SoVBYKien { set; get; }

        public string HTPPDGNV_SoVanBan { set; get; }
        public string HTPPDGNV_NgayNhan { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual QLTD_CongViec QLTD_CongViec { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }
    }
}