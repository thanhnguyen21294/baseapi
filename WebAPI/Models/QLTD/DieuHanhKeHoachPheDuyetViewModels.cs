﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class DieuHanhKeHoachPheDuyetViewModels
    {
        public bool Checked { get; set; }
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public int IdGiaiDoan { get; set; }
        public virtual ICollection<QLTD_KeHoachCongViecViewModels> qltd_kehoachcongviec { get; set; }
    }
}