﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_THCT_ChuTruongDauTuViewModels
    {
        public int IdThucHienChiTiet { set; get; }
        public int IdTienDoThucHien { get; set; }
        public string NoiDung { set; get; }
        public bool ThayDoi { set; get; }
        public string CreatedDate { set; get; }
        public virtual QLTD_TDTH_ChuTruongDauTu QLTD_TDTH_ChuTruongDauTu { get; set; }
    }
}