﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Models;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CTCV_QuyetToanDuAnViewModels
    {
        public int IdChiTietCongViec { set; get; }

        public int IdKeHoach { get; set; }
        public int IdCongViec { get; set; }
        public int IdKeHoachCongViec { get; set; }

        public bool HSQLCL_DaHoanThanh { set; get; }

        public string TTQT_DangThamTra { set; get; }
        public string TTQT_SoThamTra { set; get; }
        public string TTQT_NgayThamTra { set; get; }
        public double? TTQT_GiaTri { set; get; }

        public bool KTDL_KhongKiemToan { set; get; }
        public string KTDL_DangKiemToan { set; get; }
        public double? KTDL_GiaTriSauKiemToan { set; get; }

        public double? HTHSQT_GiaTriQuyetToan { set; get; }

        public string HTQT_SoQD { set; get; }
        public string HTQT_NgayPD { set; get; }
        public double? HTQT_GiaTri { set; get; }
        public double? HTQT_CongNoThu { set; get; }
        public double? HTQT_CongNoTra { set; get; }
        public double? HTQT_BoTriTraCongNo { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual QLTD_CongViec QLTD_CongViec { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }
    }
}