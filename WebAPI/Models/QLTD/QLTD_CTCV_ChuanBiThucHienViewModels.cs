﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Models;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CTCV_ChuanBiThucHienViewModels
    {
        public int IdChiTietCongViec { set; get; }
        public int IdKeHoach { get; set; }
        public int IdCongViec { get; set; }
        public int IdKeHoachCongViec { get; set; }

        public string PDDT_SoQuyetDinh { set; get; }
        public string PDDT_NgayPheDuyet { set; get; }
        public double? PDDT_GiaTri { set; get; }

        public string PDKHLCNT_SoQuyetDinh { set; get; }
        public string PDKHLCNT_NgayPheDuyet { set; get; }
        public double? PDKHLCNT_GiaTri { set; get; }

        public string LHS_KhoKhanVuongMac { set; get; }
        public string LHS_KienNghiDeXuat { set; get; }
        public string LHS_NgayHoanThanh { set; get; }

        public string THS_SoToTrinh { set; get; }
        public string LHS_NgayTrinh { set; get; }

        public string PDDA_SoQuyetDinh { set; get; }
        public string PDDA_NgayPheDuyet { set; get; }
        public double? PDDA_GiaTri { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual QLTD_CongViec QLTD_CongViec { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }
    }
}