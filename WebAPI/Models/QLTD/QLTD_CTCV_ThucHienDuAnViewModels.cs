﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.Models;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CTCV_ThucHienDuAnViewModels
    {
        public int IdChiTietCongViec { set; get; }

        public int IdKeHoach { get; set; }
        public int IdCongViec { get; set; }
        public int IdKeHoachCongViec { get; set; }

        public string GPMB_DamBaoTienDo { set; get; }
        public string GPMB_KhoKhanVuongMac { set; get; }
        public string GPMB_DeXuatGiaiPhap { set; get; }

        public string CTHLCNT_DangLapTKBVTC { set; get; }
        public string CTHLCNT_DangTrinhTKBVTC { set; get; }
        public string CTHLCNT_SoQDKHLCNT { set; get; }
        public string CTHLCNT_NgayPheDuyetKHLCNT { set; get; }
        public string CTHLCNT_SoNgayQDPD_KHLCNT { set; get; }
        public string CTHLCNT_KhoKhan { set; get; }

        public string DLCNT_SoQDKQLCNT { set; get; }
        public string DLCNT_NgayPheDuyet { set; get; }
        public double? DLCNT_GiaTri { set; get; }
        public string DLCNT_NgayPD_KQLCNT { get; set; }

        public string DTCXD_NgayBanGiaoMB { set; get; }
        public string DTCXD_ThoiGianKhoiCong { set; get; }
        public string DTCXD_KhoKhanVuongMac { set; get; }
        public string DTCXD_NgayBBNT_HoanThanhCT { set; get; }

        public string HTTC_NgayHoanThanh { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string NgayHoanThanh { set; get; }//date

        public virtual QLTD_CongViec QLTD_CongViec { get; set; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }
    }
}