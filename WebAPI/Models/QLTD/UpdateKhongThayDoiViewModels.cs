﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class UpdateKhongThayDoiViewModels
    {
        public int IdTienDoThucHien { set; get; }
        public int IdGiaiDoan { get; set; }
        public string NoiDung { get; set; }
    }
}
