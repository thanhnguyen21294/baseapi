﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_KhoKhanVuongMacViewModels
    {
        public int IdKhoKhanVuongMac { set; get; }
        public int IdKeHoach { get; set; }
        public string NguyenNhanLyDo { set; get; }
        public string GiaiPhapTrienKhai { set; get; }
        public bool DaGiaiQuyet { set; get; }
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
    }
}