﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.QLTD
{
    public class QLTD_CongViecViewModels
    {
        public int IdCongViec { set; get; }
        public int IdGiaiDoan { get; set; }
        public string TenCongViec { set; get; }
        public int LoaiCongViec { get; set; }
        public bool TienDoThucHien { set; get; }
    }
}