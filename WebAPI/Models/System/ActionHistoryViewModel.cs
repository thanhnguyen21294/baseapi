﻿using System;

namespace WebAPI.Models.System
{
    public class ActionHistoryViewModel
    {
        public int IdHistory { get; set; }

        public string AppUser { get; set; }

        public string Action { get; set; }

        public string Function { get; set; }

        public string FunctionUrl { get; set; }

        public string ParentFunction { get; set; }

        public string ParentFunctionUrl { get; set; }

        public string Content { get; set; }

        public DateTime CreateDate { get; set; }

        public string FunctionID { get; set; }
    }
}