﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.System
{
    public class ChangePasswordViewModel
    {
        public string Id { set; get; }
        public string currentPassword { set; get; }
        public string Password { set; get; }
    }
}