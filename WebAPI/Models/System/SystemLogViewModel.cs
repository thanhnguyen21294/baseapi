﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models.System
{
    public class SystemLogViewModel
    {
        public int ID { set; get; }

        [MaxLength(50)]
        public string UserId { set; get; }

        [MaxLength(50)]
        public string IPAddress { set; get; }

        [MaxLength(200)]
        public string AreaAccessed { set; get; }
        public DateTime TimeAccessed { set; get; }

        public int IDAgent { set; get; }
        public string FullName { set; get; }
        public string Avatar { set; get; }

        public string UserName { set; get; }

        public bool Checked { set; get; }
    }
}