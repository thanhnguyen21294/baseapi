﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using WebAPI.Models.DuAn;

namespace WebAPI.Models
{
    public class AppUserViewModel
    {
        public string Id { set; get; }

        public int? IdNhomDuAnTheoUser { set; get; }

        public string TenNhomNguoiDung { set; get; }

        public string FullName { set; get; }
        public string BirthDay { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string UserName { set; get; }
        public string Address { get; set; }
        public string PhoneNumber { set; get; }
        public string Avatar { get; set; }
        public bool Status { get; set; }

        public string Gender { get; set; }
        public bool Checked { set; get; }

        [NotMapped]
        public Boolean TrangThai { set; get; }

        public ICollection<string> Roles { get; set; }
        public virtual ICollection<AppRole> AppRoles { set; get; }
        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { set; get; }
    }
}