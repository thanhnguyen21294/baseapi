﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class ApplicationRoleViewModel
    {
        public string Id { set; get; }

        [Required(ErrorMessage = "Bạn phải nhập tên")]
        public string Name { set; get; }
        public string MaRole { get; set; }
        public string Description { set; get; }

    }
}