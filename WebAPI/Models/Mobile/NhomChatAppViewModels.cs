﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Mobile
{
    public class NhomChatAppViewModels
    {
        public int Id { get; set; }
        public string ListUser { get; set; }
        public string TenNhomChat { get; set; }
        public string GhiChu { set; get; }
    }
}