﻿using Model.Models;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Mobile
{
    public class ChatNhomAppViewModels
    {
        public int Id { get; set; }
        public string IdUser1 { get; set; }
        public int IdUser2 { get; set; }
        public string NoiDung { get; set; }
        public Boolean TrangThaiChatNhom { set; get; }
        public DateTime NgayTao { set; get; }
        public List<string> ListThongTinNhom { get; set; }
        public virtual AppUser AppUser1 { get; set; }
        public virtual NhomChatApp AppUser2 { get; set; }
        public string TenNguoiGui { get; set; }
        public bool IsFile { set; get; }
    }
}