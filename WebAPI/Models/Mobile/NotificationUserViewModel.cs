﻿using Model.Models;
using Model.Models.Mobile;

namespace WebAPI.Models.Mobile
{
    public class NotificationUserViewModel
    {
        public int Id { set; get; }

        public string UserId { set; get; }

        public int NotificationId { set; get; }

        public bool HasRead { set; get; }

        public virtual AppUser AppUser { set; get; }

        public virtual Notification Notification { set; get; }
    }
}