﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Mobile
{
    public class Device_TokenViewModels
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Token { get; set; }
        public virtual AppUser AppUser { get; set; }
    }
}