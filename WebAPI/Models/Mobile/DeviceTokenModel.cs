﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Mobile
{
    public class DeviceTokenModel
    {

        public string TokenId { get; set; }
        public string UserId { get; set; }

        public DeviceTokenModel(string userId, string tokenId)
        {
            TokenId = tokenId;
            UserId = userId;
        }
    }
}