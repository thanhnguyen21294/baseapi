﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models.Common
{
    public class ListDuAnViewModels
    {
        public int IdDuAn { set; get; }
        public string TenDuAn { set; get; }
        public int? IdDuAnCha { get; set; }
        public string TenGiaiDoanDuAn { get; set; }
    }
}