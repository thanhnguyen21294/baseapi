﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class VanBanDenThucHienViewModes
    {
        public int IdVanBanDenThucHien { set; get; }
        public int IdVanBanDen { set; get; }
        public string NoiDung { set; get; }
        public string CreatedDate { set; get; }
        public string CreatedBy { set; get; }

        public virtual VanBanDenViewModels VanBanDen { set; get; }
    }
}
