﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class ThuMucVanBanDenViewModels
    {
        public int IdThuMucVanBanDen { set; get; }
        public string TenThuMucVanBanDen { set; get; }
        public int? IdThuMucCha { set; get; }

        public int SoLuongVB { get; set; }
        //public ICollection<VanBanDenViewModels> VanBanDens { get; set; }
    }
}