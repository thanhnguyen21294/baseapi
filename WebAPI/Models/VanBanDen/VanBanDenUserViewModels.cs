﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class VanBanDenUserViewModels
    {
        public int IdVanBanDenUser { get; set; }

        public int IdVanBanDen { get; set; }

        public string UserId { get; set; }

        public bool HasRead { get; set; }
        public virtual AppUserViewModel AppUsers { get; set; }
        public virtual VanBanDenViewModels VanBanDens { get; set; }
    }
}