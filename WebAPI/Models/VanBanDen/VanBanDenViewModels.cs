﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class VanBanDenViewModels
    {
        public int IdVanBanDen { set; get; }
        public string SoVanBanDen { set; get; }
        public string TenVanBanDen { set; get; }
        public string FileName { set; get; }
        public string FileUrl { set; get; }
        public string NgayDen { set; get; }
        public string NoiGui { set; get; }
        public string MoTa { set; get; }
        public string NgayNhan { set; get; }
        public string ThoiHanHoanThanh { set; get; }
        public int? TrangThai { get; set; }
        public string TPYeuCau { get; set; }
        public string DeXuatTPTH { get; set; }
        public string YKienChiDaoGiamDoc { get; set; }
        public string GhiChu { set; get; }
        public int IdThuMucVanBanDen { set; get; }
        public virtual ThuMucVanBanDenViewModels ThuMucVanBanDen { set; get; }
        public virtual ICollection<VanBanDenUser> VanBanDenUsers { set; get; }
    }
}