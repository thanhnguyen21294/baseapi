﻿using System;
using System.Globalization;
using Microsoft.Ajax.Utilities;
using Model.Models;
using Model.Models.Common;
using Model.Models.Mobile;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Models.GPMB;
using WebAPI.Models.GPMB.GPMB_KeHoachTienDo;
using WebAPI.Models.Mobile;
using WebAPI.Models.QLTD;
using WebAPI.Models.System;

namespace WebAPI.Infrastructure.Extensions
{
    public static class EntityExtensions
    {

        public static void UpdateFunction(this Function function, FunctionViewModel functionVm)
        {
            function.Name = functionVm.Name;
            function.DisplayOrder = functionVm.DisplayOrder;
            function.IconCss = functionVm.IconCss;
            function.Status = functionVm.Status;
            function.ParentId = functionVm.ParentId;
            function.Status = functionVm.Status;
            function.URL = functionVm.URL;
            function.ID = functionVm.ID;
        }
        public static void UpdatePermission(this Permission permission, PermissionViewModel permissionVm)
        {
            permission.RoleId = permissionVm.RoleId;
            permission.FunctionId = permissionVm.FunctionId;
            permission.CanCreate = permissionVm.CanCreate;
            permission.CanDelete = permissionVm.CanDelete;
            permission.CanRead = permissionVm.CanRead;
            permission.CanUpdate = permissionVm.CanUpdate;
            permission.CanGet = permissionVm.CanGet;
        }

        public static void UpdateApplicationRole(this AppRole appRole, ApplicationRoleViewModel appRoleViewModel, string action = "add")
        {
            if (action == "update")
                appRole.Id = appRoleViewModel.Id;
            else
                appRole.Id = Guid.NewGuid().ToString();
            appRole.Name = appRoleViewModel.Name;
            appRole.MaRole = appRoleViewModel.MaRole;
            appRole.Description = appRoleViewModel.Description;
        }

        public static void UpdateUser(this AppUser appUser, AppUserViewModel appUserViewModel, string action = "add")
        {

            appUser.Id = appUserViewModel.Id;

            appUser.IdNhomDuAnTheoUser = appUserViewModel.IdNhomDuAnTheoUser;

            appUser.NhomDuAnTheoUser = appUserViewModel.NhomDuAnTheoUser;

            appUser.FullName = appUserViewModel.FullName;
            appUser.Email = appUserViewModel.Email;
            appUser.Address = appUserViewModel.Address;
            appUser.UserName = appUserViewModel.UserName;
            appUser.PhoneNumber = appUserViewModel.PhoneNumber;
            appUser.Gender = appUserViewModel.Gender == "True" ? true : false;
            appUser.Status = appUserViewModel.Status;
            appUser.Address = appUserViewModel.Address;
            appUser.Avatar = appUserViewModel.Avatar;

            if (appUserViewModel.Avatar == null)
            {
                appUser.Avatar = "/UploadedFiles/Avatars/avatar.png";
            }
        }

        //dung chung-----------------------------------------------------------------------------------------------
        public static void UpdateLoaiCapCongTrinh(this LoaiCapCongTrinh loaiCapCongTrinh,
            LoaiCapCongTrinhViewModels loaiCapCongTrinhViewModels)
        {
            loaiCapCongTrinh.TenLoaiCapCongTrinh = loaiCapCongTrinhViewModels.TenLoaiCapCongTrinh;
            loaiCapCongTrinh.GhiChu = loaiCapCongTrinhViewModels.GhiChu;
            loaiCapCongTrinh.Status = loaiCapCongTrinhViewModels.Status;
        }

        public static void UpdateNhomDuAnTheoUser(this NhomDuAnTheoUser nhomDuAnTheoUser, NhomDuAnTheoUserViewModels nhomDuAnTheoUserViewModels)
        {
            nhomDuAnTheoUser.TenNhomDuAn = nhomDuAnTheoUserViewModels.TenNhomDuAn;
            nhomDuAnTheoUser.GhiChu = nhomDuAnTheoUserViewModels.GhiChu;
            nhomDuAnTheoUser.Status = nhomDuAnTheoUserViewModels.Status;
            nhomDuAnTheoUser.PhongChucNang = nhomDuAnTheoUserViewModels.PhongChucNang;
        }

        public static void UpdateDuAn(this DuAn duAn, DuAnViewModels duAnViewModels)
        {
            duAn.Ma = duAnViewModels.Ma;
            duAn.TenDuAn = duAnViewModels.TenDuAn;
            duAn.IdDuAnCha = duAnViewModels.IdDuAnCha;
            duAn.IdChuDauTu = duAnViewModels.IdChuDauTu;
            duAn.ThoiGianThucHien = duAnViewModels.ThoiGianThucHien;

            duAn.IdNhomDuAnTheoUser = duAnViewModels.IdNhomDuAnTheoUser;
            duAn.IdLinhVucNganhNghe = duAnViewModels.IdLinhVucNganhNghe;
            duAn.DiaDiem = duAnViewModels.DiaDiem;
            duAn.KinhDo = duAnViewModels.KinhDo;
            duAn.ViDo = duAnViewModels.ViDo;
            duAn.TongMucDauTu = duAnViewModels.TongMucDauTu;
            duAn.MucTieu = duAnViewModels.MucTieu;
            duAn.QuyMoNangLuc = duAnViewModels.QuyMoNangLuc;
            duAn.IdNhomDuAn = duAnViewModels.IdNhomDuAn;
            duAn.IdLoaiCapCongTrinh = duAnViewModels.IdLoaiCapCongTrinh;
            duAn.IdHinhThucDauTu = duAnViewModels.IdHinhThucDauTu;
            duAn.IdGiaiDoanDuAn = duAnViewModels.IdGiaiDoanDuAn;
            duAn.IdHinhThucQuanLy = duAnViewModels.IdHinhThucQuanLy;
            duAn.IdTinhTrangDuAn = duAnViewModels.IdTinhTrangDuAn;
            duAn.TongMucDauTu = duAnViewModels.TongMucDauTu;
            duAn.TongMucDauTu_XL = duAnViewModels.TongMucDauTu_XL;
            duAn.TongMucDauTu_TB = duAnViewModels.TongMucDauTu_TB;
            duAn.TongMucDauTu_GPMB = duAnViewModels.TongMucDauTu_GPMB;
            duAn.TongMucDauTu_TV = duAnViewModels.TongMucDauTu_TV;
            duAn.TongMucDauTu_QLDA = duAnViewModels.TongMucDauTu_QLDA;
            duAn.TongMucDauTu_K = duAnViewModels.TongMucDauTu_K;
            duAn.TongMucDauTu_DP = duAnViewModels.TongMucDauTu_DP;

            duAn.TongDuToan = duAnViewModels.TongDuToan;
            duAn.TongDuToan_XL = duAnViewModels.TongDuToan_XL;
            duAn.TongDuToan_TB = duAnViewModels.TongDuToan_TB;
            duAn.TongDuToan_GPMB = duAnViewModels.TongDuToan_GPMB;
            duAn.TongDuToan_TV = duAnViewModels.TongDuToan_TV;
            duAn.TongDuToan_QLDA = duAnViewModels.TongDuToan_QLDA;
            duAn.TongDuToan_K = duAnViewModels.TongDuToan_K;
            duAn.TongDuToan_DP = duAnViewModels.TongDuToan_DP;

            if (!string.IsNullOrEmpty(duAnViewModels.NgayKhoiCongKeHoach))
            {
                var a = duAnViewModels.NgayKhoiCongKeHoach;
                DateTime dateTime = DateTime.ParseExact(duAnViewModels.NgayKhoiCongKeHoach, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                duAn.NgayKhoiCongKeHoach = dateTime;
            }
            else
            {
                duAn.NgayKhoiCongKeHoach = null;
            }
            if (!string.IsNullOrEmpty(duAnViewModels.NgayKetThucThucTe))
            {
                DateTime dateTime = DateTime.ParseExact(duAnViewModels.NgayKetThucThucTe, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                duAn.NgayKetThucThucTe = dateTime;
            }
            else
            {
                duAn.NgayKetThucThucTe = null;
            }
            if (!string.IsNullOrEmpty(duAnViewModels.NgayKetThucKeHoach))
            {
                DateTime dateTime = DateTime.ParseExact(duAnViewModels.NgayKetThucKeHoach, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                duAn.NgayKetThucKeHoach = dateTime;
            }
            else
            {
                duAn.NgayKetThucKeHoach = null;
            }
            if (!string.IsNullOrEmpty(duAnViewModels.NgayKhoiCongThucTe))
            {
                DateTime dateTime = DateTime.ParseExact(duAnViewModels.NgayKhoiCongThucTe, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                duAn.NgayKhoiCongThucTe = dateTime;
            }
            else
            {
                duAn.NgayKhoiCongThucTe = null;
            }
            duAn.Order = duAnViewModels.Order;
            duAn.OrderTemp = duAnViewModels.OrderTemp;
            duAn.GhiChu = duAnViewModels.GhiChu;
        }
        public static void UpdateNhomDuAn(this NhomDuAn nhomDuAn,
            NhomDuAnViewModels nhomDuAnViewModels)
        {
            nhomDuAn.TenNhomDuAn = nhomDuAnViewModels.TenNhomDuAn;
            nhomDuAn.GhiChu = nhomDuAnViewModels.GhiChu;
            nhomDuAn.Status = nhomDuAnViewModels.Status;
        }
        
        public static void UpdateDanhMucHoSoQuyetToan(this DanhMucHoSoQuyetToan danhMucHoSoQuyetToan,
            DanhMucHoSoQuyetToanViewModels danhMucHoSoQuyetToanViewModels)
        {
            danhMucHoSoQuyetToan.TenDanhMucHoSoQuyetToan = danhMucHoSoQuyetToanViewModels.TenDanhMucHoSoQuyetToan;
            danhMucHoSoQuyetToan.GiaiDoan = danhMucHoSoQuyetToanViewModels.GiaiDoan;
            danhMucHoSoQuyetToan.IdParent = danhMucHoSoQuyetToanViewModels.IdParent;
            danhMucHoSoQuyetToan.GhiChu = danhMucHoSoQuyetToanViewModels.GhiChu;
            danhMucHoSoQuyetToan.Order = danhMucHoSoQuyetToanViewModels.Order;
        }
        public static void UpdateLinhVucDauThau(this LinhVucDauThau linhVucDauThau,
            LinhVucDauThauViewModels linhVucDauThauViewModels)
        {
            linhVucDauThau.TenLinhVucDauThau = linhVucDauThauViewModels.TenLinhVucDauThau;
            linhVucDauThau.GhiChu = linhVucDauThauViewModels.GhiChu;
            linhVucDauThau.Status = linhVucDauThauViewModels.Status;
        }

        public static void UpdateHinhThucDauThau(this HinhThucDauThau hinhThucDauThau,
            HinhThucDauThauViewModels hinhThucDauThauViewModels)
        {
            hinhThucDauThau.TenHinhThucDauThau = hinhThucDauThauViewModels.TenHinhThucDauThau;
            hinhThucDauThau.GhiChu = hinhThucDauThauViewModels.GhiChu;
            hinhThucDauThau.Status = hinhThucDauThauViewModels.Status;
        }
        public static void UpdateChatApp(this ChatApp chatApp,
          ChatAppViewModels chatAppViewModels)
        {
            chatApp.IdUser1 = chatAppViewModels.IdUser1;
            chatApp.IdUser2 = chatAppViewModels.IdUser2;
            chatApp.NoiDung = chatAppViewModels.NoiDung;
            chatApp.TrangThaiChatNhom = chatAppViewModels.TrangThaiChatNhom;
            chatApp.NgayTao = DateTime.Now;
            chatApp.IsFile = chatAppViewModels.IsFile;
        }

        public static void UpdateLoaiChiPhi(this LoaiChiPhi loaiChiPhi, LoaiChiPhiViewModels loaiChiPhiViewModels)
        {
            loaiChiPhi.IdLoaiChiPhiCha = loaiChiPhiViewModels.IdLoaiChiPhiCha;
            loaiChiPhi.TenLoaiChiPhi = loaiChiPhiViewModels.TenLoaiChiPhi;
            loaiChiPhi.GhiChu = loaiChiPhiViewModels.GhiChu;
            loaiChiPhi.Order = loaiChiPhiViewModels.Order;
        }

        public static void UpdateCamKetGiaiNgan(this CamKetGiaiNgan camKetGiaiNgan, CamKetGiaiNganViewModels camKetGiaiNganViewModels)
        {
            camKetGiaiNgan.NamCK = camKetGiaiNganViewModels.NamCK;
            camKetGiaiNgan.IdDuAn = camKetGiaiNganViewModels.IdDuAn;
            camKetGiaiNgan.Thang1 = camKetGiaiNganViewModels.Thang1;
            camKetGiaiNgan.Thang2 = camKetGiaiNganViewModels.Thang2;
            camKetGiaiNgan.Thang3 = camKetGiaiNganViewModels.Thang3;
            camKetGiaiNgan.Thang4 = camKetGiaiNganViewModels.Thang4;
            camKetGiaiNgan.Thang5 = camKetGiaiNganViewModels.Thang5;
            camKetGiaiNgan.Thang6 = camKetGiaiNganViewModels.Thang6;
            camKetGiaiNgan.Thang7 = camKetGiaiNganViewModels.Thang7;
            camKetGiaiNgan.Thang8 = camKetGiaiNganViewModels.Thang8;
            camKetGiaiNgan.Thang9 = camKetGiaiNganViewModels.Thang9;
            camKetGiaiNgan.Thang10 = camKetGiaiNganViewModels.Thang10;
            camKetGiaiNgan.Thang11 = camKetGiaiNganViewModels.Thang11;
            camKetGiaiNgan.Thang12 = camKetGiaiNganViewModels.Thang12;
            camKetGiaiNgan.Quy1 = camKetGiaiNganViewModels.Quy1;
            camKetGiaiNgan.Quy2 = camKetGiaiNganViewModels.Quy2;
            camKetGiaiNgan.Quy3 = camKetGiaiNganViewModels.Quy3;
            camKetGiaiNgan.Quy4 = camKetGiaiNganViewModels.Quy4;
            camKetGiaiNgan.TongCong = camKetGiaiNganViewModels.TongCong;
        }

        public static void UpdateChatNhomApp(this ChatNhomApp chatNhomApp,
         ChatNhomAppViewModels chatNhomAppViewModels)
        {
            chatNhomApp.IdUser1 = chatNhomAppViewModels.IdUser1;
            chatNhomApp.IdUser2 = chatNhomAppViewModels.IdUser2;
            chatNhomApp.NoiDung = chatNhomAppViewModels.NoiDung;
            chatNhomApp.TrangThaiChatNhom = chatNhomAppViewModels.TrangThaiChatNhom;
            chatNhomApp.NgayTao = DateTime.Now;
            chatNhomApp.IsFile = chatNhomAppViewModels.IsFile;
        }
        public static void UpdateDevice_Token(this Device_Token device_Token,
          Device_TokenViewModels device_TokenViewModels)
        {
            device_Token.UserId = device_TokenViewModels.UserId;
            device_Token.Token = device_TokenViewModels.Token;
        }
        public static void UpdateNhomChatApp(this NhomChatApp nhomChatApp,
         NhomChatAppViewModels nhomChatAppViewModels)
        {
            nhomChatApp.ListUser = nhomChatAppViewModels.ListUser;
            nhomChatApp.TenNhomChat = nhomChatAppViewModels.TenNhomChat;
            nhomChatApp.GhiChu = nhomChatAppViewModels.GhiChu;
        }
        public static void UpdateHinhThucLuaChonNhaThau(this HinhThucLuaChonNhaThau hinhThucLuaChonNhaThau,
            HinhThucLuaChonNhaThauViewModels hinhThucLuaChonNhaThauViewModels)
        {
            hinhThucLuaChonNhaThau.TenHinhThucLuaChon = hinhThucLuaChonNhaThauViewModels.TenHinhThucLuaChon;
            hinhThucLuaChonNhaThau.GhiChu = hinhThucLuaChonNhaThauViewModels.GhiChu;
            hinhThucLuaChonNhaThau.Status = hinhThucLuaChonNhaThauViewModels.Status;
        }

        public static void UpdateLoaiHopDong(this LoaiHopDong loaiHopDong,
            LoaiHopDongViewModels loaiHopDongViewModels)
        {
            loaiHopDong.TenLoaiHopDong = loaiHopDongViewModels.TenLoaiHopDong;
            loaiHopDong.GhiChu = loaiHopDongViewModels.GhiChu;
            loaiHopDong.Status = loaiHopDongViewModels.Status;
        }

        public static void UpdateGiaiDoanDuAn(this GiaiDoanDuAn giaiDoanDuAn,
            GiaiDoanDuAnViewModels giaiDoanDuAnViewModels)
        {
            giaiDoanDuAn.TenGiaiDoanDuAn = giaiDoanDuAnViewModels.TenGiaiDoanDuAn;
            giaiDoanDuAn.GhiChu = giaiDoanDuAnViewModels.GhiChu;
            giaiDoanDuAn.Status = giaiDoanDuAnViewModels.Status;
        }

        public static void UpdateChuDauTu(this ChuDauTu chuDauTu,
            ChuDauTuViewModels chuDauTuViewModels)
        {
            chuDauTu.TenChuDauTu = chuDauTuViewModels.TenChuDauTu;
            chuDauTu.GhiChu = chuDauTuViewModels.GhiChu;
            chuDauTu.DiaChi = chuDauTuViewModels.DiaChi;
            chuDauTu.Status = chuDauTuViewModels.Status;
        }

        public static void UpdateTinhTrangDuAn(this TinhTrangDuAn tinhTrangDuAn,
            TinhTrangDuAnViewModels tinhTrangDuAnViewModels)
        {
            tinhTrangDuAn.TenTinhTrangDuAn = tinhTrangDuAnViewModels.TenTinhTrangDuAn;
            tinhTrangDuAn.GhiChu = tinhTrangDuAnViewModels.GhiChu;
            tinhTrangDuAn.Status = tinhTrangDuAnViewModels.Status;
            tinhTrangDuAn.IdTinhTrangDuAnCha = tinhTrangDuAnViewModels.IdTinhTrangDuAnCha;
            tinhTrangDuAn.ThuTu = tinhTrangDuAnViewModels.ThuTu;
        }

        public static void UpdatePhuongThucDauThau(this PhuongThucDauThau phuongThucDauThau,
           PhuongThucDauThauViewModels phuongThucDauThauViewModels)
        {
            phuongThucDauThau.TenPhuongThucDauThau = phuongThucDauThauViewModels.TenPhuongThucDauThau;
            phuongThucDauThau.GhiChu = phuongThucDauThauViewModels.GhiChu;
            phuongThucDauThau.Status = phuongThucDauThauViewModels.Status;
        }

        public static void UpdateLinhVucNganhNghe(this LinhVucNganhNghe linhVucNganhNghe,
           LinhVucNganhNgheViewModels linhVucNganhNgheViewModels)
        {
            linhVucNganhNghe.TenLinhVucNganhNghe = linhVucNganhNgheViewModels.TenLinhVucNganhNghe;
            linhVucNganhNghe.Ma = linhVucNganhNgheViewModels.Ma;
            linhVucNganhNghe.KhoiLinhVuc = linhVucNganhNgheViewModels.KhoiLinhVuc;
            linhVucNganhNghe.GhiChu = linhVucNganhNgheViewModels.GhiChu;
            linhVucNganhNghe.Order = linhVucNganhNgheViewModels.Order;
            linhVucNganhNghe.Status = linhVucNganhNgheViewModels.Status;
            linhVucNganhNghe.IdLienKetLinhVuc = linhVucNganhNgheViewModels.IdLienKetLinhVuc;
        }

        public static void UpdateHinhThucDauTu(this HinhThucDauTu hinhThucDauTu,
           HinhThucDauTuViewModels hinhThucDauTuViewModels)
        {
            hinhThucDauTu.TenHinhThucDauTu = hinhThucDauTuViewModels.TenHinhThucDauTu;
            hinhThucDauTu.GhiChu = hinhThucDauTuViewModels.GhiChu;
            hinhThucDauTu.Status = hinhThucDauTuViewModels.Status;
        }

        public static void UpdateHinhThucQuanLy(this HinhThucQuanLy hinhThucQuanLy,
           HinhThucQuanLyViewModels hinhThucQuanLyViewModels)
        {
            hinhThucQuanLy.TenHinhThucQuanLy = hinhThucQuanLyViewModels.TenHinhThucQuanLy;
            hinhThucQuanLy.GhiChu = hinhThucQuanLyViewModels.GhiChu;
            hinhThucQuanLy.Status = hinhThucQuanLyViewModels.Status;
        }

        public static void UpdateCoQuanPheDuyet(this CoQuanPheDuyet coQuanPheDuyet,
           CoQuanPheDuyetViewModels coQuanPheDuyetViewModels)
        {
            coQuanPheDuyet.TenCoQuanPheDuyet = coQuanPheDuyetViewModels.TenCoQuanPheDuyet;
            coQuanPheDuyet.GhiChu = coQuanPheDuyetViewModels.GhiChu;
            coQuanPheDuyet.Status = coQuanPheDuyetViewModels.Status;
        }
        public static void UpdateNguonVon(this NguonVon nguonVon,
                    NguonVonViewModels nguonVonViewModels)
        {
            nguonVon.TenNguonVon = nguonVonViewModels.TenNguonVon;
            nguonVon.Ma = nguonVonViewModels.Ma;
            nguonVon.Loai = nguonVonViewModels.Loai;
            nguonVon.Nhom = nguonVonViewModels.Nhom;
            nguonVon.IdNguonVonCha = nguonVonViewModels.IdNguonVonCha;
            nguonVon.Ghichu = nguonVonViewModels.Ghichu;
            nguonVon.Status = nguonVonViewModels.Status;
        }
        public static void UpdateNguonVonDuAn(this NguonVonDuAn nguonVonDuAn,
            NguonVonDuAnViewModels nguonVonDuAnViewModels)
        {
            nguonVonDuAn.IdDuAn = nguonVonDuAnViewModels.IdDuAn;
            nguonVonDuAn.IdNguonVon = nguonVonDuAnViewModels.IdNguonVon;
            nguonVonDuAn.GiaTri = nguonVonDuAnViewModels.GiaTri;
            nguonVonDuAn.GiaTriTrungHan = nguonVonDuAnViewModels.GiaTriTrungHan;
            nguonVonDuAn.IdLapQuanLyDauTu = nguonVonDuAnViewModels.IdLapQuanLyDauTu;
        }
        public static void UpdateThuMucMacDinh(this ThuMucMacDinh tmmd, ThuMucMacDinhViewModel tmmdVM)
        {
            tmmd.TenThuMuc = tmmdVM.TenThuMuc;
            tmmd.IdThuMucMacDinh = tmmdVM.IdThuMucMacDinh;
            tmmd.IdThuMucMacDinhCha = tmmdVM.IdThuMucMacDinhCha;
        }
        public static void UpdateThuMuc(this ThuMuc tm, ThuMucViewModel tmVM)
        {
            tm.IdThuMuc = tmVM.IdThuMuc;
            tm.TenThuMuc = tmVM.TenThuMuc;
            tm.IdThuMucCha = tmVM.IdThuMucCha;
            tm.IdThuMucMacDinh = tmVM.IdThuMucMacDinh;
            tm.IdDuAn = tmVM.IdDuAn;
        }

        public static void UpdateVanBanDuAn(this VanBanDuAn vbda, VanBanDuAnViewModel vbdaVM)
        {
            vbda.IdDuAn = vbdaVM.IdDuAn;
            vbda.IdVanBan = vbdaVM.IdVanBan;
            vbda.IdThuMuc = vbdaVM.IdThuMuc;
            vbda.TenVanBan = vbdaVM.TenVanBan;
            vbda.SoVanBan = vbdaVM.SoVanBan;
            vbda.CoQuanBanHanh = vbdaVM.CoQuanBanHanh;
            vbda.MoTa = vbdaVM.MoTa;
            vbda.GhiChu = vbdaVM.GhiChu;
            vbda.FileName = vbdaVM.FileName;
            vbda.FileUrl = vbdaVM.FileUrl;
            if (!string.IsNullOrEmpty(vbdaVM.NgayBanHanh))
            {
                DateTime dateTime = DateTime.ParseExact(vbdaVM.NgayBanHanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                vbda.NgayBanHanh = dateTime;
            }
            else
            {
                vbda.NgayBanHanh = null;
            }
        }

        public static void UpdateVanBanDen(this VanBanDen vbda, VanBanDenViewModels vbdaVM)
        {
            vbda.IdVanBanDen = vbdaVM.IdVanBanDen;
            vbda.IdThuMucVanBanDen = vbdaVM.IdThuMucVanBanDen;
            vbda.TenVanBanDen = vbdaVM.TenVanBanDen;
            vbda.SoVanBanDen = vbdaVM.SoVanBanDen;
            vbda.NoiGui = vbdaVM.NoiGui;
            vbda.MoTa = vbdaVM.MoTa;
            vbda.GhiChu = vbdaVM.GhiChu;
            vbda.FileName = vbdaVM.FileName;
            vbda.FileUrl = vbdaVM.FileUrl;
            if (!string.IsNullOrEmpty(vbdaVM.NgayDen))
            {
                DateTime dateTime = DateTime.ParseExact(vbdaVM.NgayDen, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                vbda.NgayDen = dateTime;
            }
            else
            {
                vbda.NgayDen = null;
            }
            if (!string.IsNullOrEmpty(vbdaVM.ThoiHanHoanThanh))
            {
                DateTime dateTime = DateTime.ParseExact(vbdaVM.ThoiHanHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                vbda.ThoiHanHoanThanh = dateTime;
            }
            else
            {
                vbda.ThoiHanHoanThanh = null;
            }
            if (!string.IsNullOrEmpty(vbdaVM.NgayNhan))
            {
                DateTime dateTime = DateTime.ParseExact(vbdaVM.NgayNhan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                vbda.NgayNhan = dateTime;
            }
            else
            {
                vbda.NgayNhan = null;
            }
            vbda.TrangThai = vbdaVM.TrangThai;
            vbda.TPYeuCau = vbdaVM.TPYeuCau;
            vbda.DeXuatTPTH = vbdaVM.DeXuatTPTH;
            vbda.YKienChiDaoGiamDoc = vbdaVM.YKienChiDaoGiamDoc;
        }


        public static void UpdateKeHoachLuaChonNhaThau(this KeHoachLuaChonNhaThau keHoachLuaChonNhaThau,
            KeHoachLuaChonNhaThauViewModels keHoachLuaChonNhaThayViewModels)
        {
            keHoachLuaChonNhaThau.NoiDung = keHoachLuaChonNhaThayViewModels.NoiDung;
            keHoachLuaChonNhaThau.IdDuAn = keHoachLuaChonNhaThayViewModels.IdDuAn;
            keHoachLuaChonNhaThau.IdDieuChinhKeHoachLuaChonNhaThau =
                keHoachLuaChonNhaThayViewModels.IdDieuChinhKeHoachLuaChonNhaThau;
            keHoachLuaChonNhaThau.SoVanBanTrinh = keHoachLuaChonNhaThayViewModels.SoVanBanTrinh;
            if (!string.IsNullOrEmpty(keHoachLuaChonNhaThayViewModels.NgayTrinh))
            {
                DateTime dateTime = DateTime.ParseExact(keHoachLuaChonNhaThayViewModels.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                keHoachLuaChonNhaThau.NgayTrinh = dateTime;
            }
            else
            {
                keHoachLuaChonNhaThau.NgayTrinh = null;
            }
            if (!string.IsNullOrEmpty(keHoachLuaChonNhaThayViewModels.NgayNhanDuHoSo))
            {
                DateTime dateTime = DateTime.ParseExact(keHoachLuaChonNhaThayViewModels.NgayNhanDuHoSo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                keHoachLuaChonNhaThau.NgayNhanDuHoSo = dateTime;
            }
            else
            {
                keHoachLuaChonNhaThau.NgayNhanDuHoSo = null;
            }
            if (!string.IsNullOrEmpty(keHoachLuaChonNhaThayViewModels.NgayThamDinh))
            {
                DateTime dateTime = DateTime.ParseExact(keHoachLuaChonNhaThayViewModels.NgayThamDinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                keHoachLuaChonNhaThau.NgayThamDinh = dateTime;
            }
            else
            {
                keHoachLuaChonNhaThau.NgayThamDinh = null;
            }
            if (!string.IsNullOrEmpty(keHoachLuaChonNhaThayViewModels.NgayPheDuyet))
            {
                DateTime dateTime = DateTime.ParseExact(keHoachLuaChonNhaThayViewModels.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                keHoachLuaChonNhaThau.NgayPheDuyet = dateTime;
            }
            else
            {
                keHoachLuaChonNhaThau.NgayPheDuyet = null;
            }
            keHoachLuaChonNhaThau.SoPheDuyet = keHoachLuaChonNhaThayViewModels.SoPheDuyet;
            keHoachLuaChonNhaThau.IdCoQuanPheDuyet = keHoachLuaChonNhaThayViewModels.IdCoQuanPheDuyet;
            keHoachLuaChonNhaThau.NguoiPheDuyet = keHoachLuaChonNhaThayViewModels.NguoiPheDuyet;
            if (!string.IsNullOrEmpty(keHoachLuaChonNhaThayViewModels.NgayDangTaiThongTin))
            {
                DateTime dateTime = DateTime.ParseExact(keHoachLuaChonNhaThayViewModels.NgayDangTaiThongTin, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                keHoachLuaChonNhaThau.NgayDangTaiThongTin = dateTime;
            }
            else
            {
                keHoachLuaChonNhaThau.NgayDangTaiThongTin = null;
            }
            keHoachLuaChonNhaThau.PhuongTienDangTai = keHoachLuaChonNhaThayViewModels.PhuongTienDangTai;
            keHoachLuaChonNhaThau.ToChucCaNhanGiamSat = keHoachLuaChonNhaThayViewModels.ToChucCaNhanGiamSat;
            keHoachLuaChonNhaThau.GhiChu = keHoachLuaChonNhaThayViewModels.GhiChu;
        }

        public static void UpdateGoiThau(this GoiThau goiThau, GoiThauViewModels goiThauViewModels)
        {
            goiThau.IdDuAn = goiThauViewModels.IdDuAn;
            goiThau.TenGoiThau = goiThauViewModels.TenGoiThau;
            goiThau.IdKeHoachLuaChonNhaThau = goiThauViewModels.IdKeHoachLuaChonNhaThau;
            goiThau.GiaGoiThau = goiThauViewModels.GiaGoiThau;
            goiThau.LoaiGoiThau = goiThauViewModels.LoaiGoiThau;
            goiThau.IdLinhVucDauThau = goiThauViewModels.IdLinhVucDauThau;
            goiThau.IdHinhThucLuaChon = goiThauViewModels.IdHinhThucLuaChon;
            goiThau.IdPhuongThucDauThau = goiThauViewModels.IdPhuongThucDauThau;
            goiThau.HinhThucDauThau = goiThauViewModels.HinhThucDauThau;
            goiThau.LoaiDauThau = goiThauViewModels.LoaiDauThau;
            goiThau.IdLoaiHopDong = goiThauViewModels.IdLoaiHopDong;
            goiThau.ThoiGianThucHienHopDong = goiThauViewModels.ThoiGianThucHienHopDong;
            goiThau.GhiChu = goiThauViewModels.GhiChu;
        }

        public static void UpdateHoSoMoiThau(this HoSoMoiThau hoSoMoiThau, HoSoMoiThauViewModels hoSoMoiThauViewModels)
        {
            hoSoMoiThau.NoiDung = hoSoMoiThauViewModels.NoiDung;
            hoSoMoiThau.IdDuAn = hoSoMoiThauViewModels.IdDuAn;
            hoSoMoiThau.IdGoiThau = hoSoMoiThauViewModels.IdGoiThau;
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayTrinh))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayTrinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayTrinh = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayTrinh = null;
            }
            hoSoMoiThau.SoVanBanTrinh = hoSoMoiThauViewModels.SoVanBanTrinh;
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayNhanDu))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayNhanDu, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayNhanDu = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayNhanDu = null;
            }
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayThamDinh))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayThamDinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayThamDinh = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayThamDinh = null;
            }
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayPheDuyet))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayPheDuyet = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayPheDuyet = null;
            }
            hoSoMoiThau.SoPheDuyet = hoSoMoiThauViewModels.SoPheDuyet;
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayDangTai))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayDangTai, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayDangTai = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayDangTai = null;
            }
            hoSoMoiThau.SoDangTai = hoSoMoiThauViewModels.SoDangTai;
            hoSoMoiThau.PhuongTienDangTai = hoSoMoiThauViewModels.PhuongTienDangTai;
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayPhatHanh))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayPhatHanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayPhatHanh = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayPhatHanh = null;
            }
            hoSoMoiThau.SoPhatHanh = hoSoMoiThauViewModels.SoPhatHanh;
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayHetHanHieuLuc))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayHetHanHieuLuc, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayHetHanHieuLuc = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayHetHanHieuLuc = null;
            }
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayHetHanBaoLanh))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayHetHanBaoLanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayHetHanBaoLanh = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayHetHanBaoLanh = null;
            }
            if (!string.IsNullOrEmpty(hoSoMoiThauViewModels.NgayLapBienBan))
            {
                DateTime dateTime = DateTime.ParseExact(hoSoMoiThauViewModels.NgayLapBienBan, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hoSoMoiThau.NgayLapBienBan = dateTime;
            }
            else
            {
                hoSoMoiThau.NgayLapBienBan = null;
            }
            hoSoMoiThau.GhiChu = hoSoMoiThauViewModels.GhiChu;
        }

        public static void UpdateKetQuaDauThau(this KetQuaDauThau ketQuaDauThau,
            KetQuaDauThauViewModels ketQuaDauThauViewModels)
        {
            ketQuaDauThau.NoiDung = ketQuaDauThauViewModels.NoiDung;
            ketQuaDauThau.IdDuAn = ketQuaDauThauViewModels.IdDuAn;
            ketQuaDauThau.IdGoiThau = ketQuaDauThauViewModels.IdGoiThau;
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayLapBienBanMoThau))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayLapBienBanMoThau, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayLapBienBanMoThau = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayLapBienBanMoThau = null;
            }
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayLapBaoCaoDanhGia))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayLapBaoCaoDanhGia, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayLapBaoCaoDanhGia = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayLapBaoCaoDanhGia = null;
            }
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayLapBienBanThuongThao))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayLapBienBanThuongThao, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayLapBienBanThuongThao = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayLapBienBanThuongThao = null;
            }
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayLapBaoCaoThamDinh))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayLapBaoCaoThamDinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayLapBaoCaoThamDinh = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayLapBaoCaoThamDinh = null;
            }
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayPheDuyet))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayPheDuyet = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayPheDuyet = null;
            }
            ketQuaDauThau.SoPheDuyet = ketQuaDauThauViewModels.SoPheDuyet;
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayDangTai))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayDangTai, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayDangTai = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayDangTai = null;
            }
            ketQuaDauThau.SoDangTai = ketQuaDauThauViewModels.SoDangTai;
            ketQuaDauThau.PhuongTienDangTai = ketQuaDauThauViewModels.PhuongTienDangTai;
            if (!string.IsNullOrEmpty(ketQuaDauThauViewModels.NgayGuiThongBao))
            {
                DateTime dateTime = DateTime.ParseExact(ketQuaDauThauViewModels.NgayGuiThongBao, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                ketQuaDauThau.NgayGuiThongBao = dateTime;
            }
            else
            {
                ketQuaDauThau.NgayGuiThongBao = null;
            }
            ketQuaDauThau.GiaDuThau = ketQuaDauThauViewModels.GiaDuThau;
            ketQuaDauThau.GiaTrungThau = ketQuaDauThauViewModels.GiaTrungThau;
            ketQuaDauThau.IdNhaThau = ketQuaDauThauViewModels.IdNhaThau;
            ketQuaDauThau.GhiChu = ketQuaDauThauViewModels.GhiChu;
        }

        public static void UpdateDanhMucNhaThau(this DanhMucNhaThau danhMucNhaThau,
            DanhMucNhaThauViewModels danhMucNhaThauViewModels)
        {
            danhMucNhaThau.IdDuAn = danhMucNhaThauViewModels.IdDuAn;
            danhMucNhaThau.TenNhaThau = danhMucNhaThauViewModels.TenNhaThau;
            danhMucNhaThau.DiaChiNhaThau = danhMucNhaThauViewModels.DiaChiNhaThau;
            danhMucNhaThau.MoTa = danhMucNhaThauViewModels.MoTa;
        }

        public static void UpdateHopDong(this HopDong hopDong, HopDongViewModels hopDongViewModels)
        {
            hopDong.TenHopDong = hopDongViewModels.TenHopDong;
            hopDong.IdDuAn = hopDongViewModels.IdDuAn;
            hopDong.IdGoiThau = hopDongViewModels.IdGoiThau;
            hopDong.SoHopDong = hopDongViewModels.SoHopDong;
            if (!string.IsNullOrEmpty(hopDongViewModels.NgayKy))
            {
                DateTime dateTime = DateTime.ParseExact(hopDongViewModels.NgayKy, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hopDong.NgayKy = dateTime;
            }
            else
            {
                hopDong.NgayKy = null;
            }
            hopDong.ThoiGianThucHien = hopDongViewModels.ThoiGianThucHien;
            hopDong.IdLoaiHopDong = hopDongViewModels.IdLoaiHopDong;
            hopDong.GiaHopDong = hopDongViewModels.GiaHopDong;
            hopDong.GiaHopDongDieuChinh = hopDongViewModels.GiaHopDongDieuChinh;
            hopDong.NoiDungVanTat = hopDongViewModels.NoiDungVanTat;
            if (!string.IsNullOrEmpty(hopDongViewModels.NgayBatDau))
            {
                DateTime dateTime = DateTime.ParseExact(hopDongViewModels.NgayBatDau, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hopDong.NgayBatDau = dateTime;
            }
            else
            {
                hopDong.NgayBatDau = null;
            }
            if (!string.IsNullOrEmpty(hopDongViewModels.NgayHoanThanh))
            {
                DateTime dateTime = DateTime.ParseExact(hopDongViewModels.NgayHoanThanh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hopDong.NgayHoanThanh = dateTime;
            }
            else
            {
                hopDong.NgayHoanThanh = null;
            }
            if (!string.IsNullOrEmpty(hopDongViewModels.NgayHoanThanhDieuChinh))
            {
                DateTime dateTime = DateTime.ParseExact(hopDongViewModels.NgayHoanThanhDieuChinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hopDong.NgayHoanThanhDieuChinh = dateTime;
            }
            else
            {
                hopDong.NgayHoanThanhDieuChinh = null;
            }
            if (!string.IsNullOrEmpty(hopDongViewModels.NgayThanhLyHopDong))
            {
                DateTime dateTime = DateTime.ParseExact(hopDongViewModels.NgayThanhLyHopDong, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                hopDong.NgayThanhLyHopDong = dateTime;
            }
            else
            {
                hopDong.NgayThanhLyHopDong = null;
            }
            hopDong.GhiChu = hopDongViewModels.GhiChu;
        }

        public static void UpdatePhuLucHopDong(this PhuLucHopDong phuLucHopDong,
            PhuLucHopDongViewModels phuLucHopDongViewModels)
        {
            phuLucHopDong.IdHopDong = phuLucHopDongViewModels.IdHopDong;
            phuLucHopDong.TenPhuLuc = phuLucHopDongViewModels.TenPhuLuc;
            phuLucHopDong.GiaHopDongDieuChinh = phuLucHopDongViewModels.GiaHopDongDieuChinh;
            if (!string.IsNullOrEmpty(phuLucHopDongViewModels.NgayKy))
            {
                DateTime dateTime = DateTime.ParseExact(phuLucHopDongViewModels.NgayKy, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                phuLucHopDong.NgayKy = dateTime;
            }
            else
            {
                phuLucHopDong.NgayKy = null;
            }
            if (!string.IsNullOrEmpty(phuLucHopDongViewModels.NgayHoanThanhDieuChinh))
            {
                DateTime dateTime = DateTime.ParseExact(phuLucHopDongViewModels.NgayHoanThanhDieuChinh, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                phuLucHopDong.NgayHoanThanhDieuChinh = dateTime;
            }
            else
            {
                phuLucHopDong.NgayHoanThanhDieuChinh = null;
            }
            phuLucHopDong.NoiDung = phuLucHopDongViewModels.NoiDung;
            phuLucHopDong.GhiChu = phuLucHopDongViewModels.GhiChu;
        }
        public static void UpdateThamDinhLapQuanLyDauTu(this ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu,
            ThamDinhLapQuanLyDauTuViewModels thamDinhLapQuanLyDauTuViewModels)
        {
            thamDinhLapQuanLyDauTu.IdThamDinh = thamDinhLapQuanLyDauTuViewModels.IdThamDinh;
            thamDinhLapQuanLyDauTu.NoiDung = thamDinhLapQuanLyDauTuViewModels.NoiDung;
            if (!string.IsNullOrEmpty(thamDinhLapQuanLyDauTuViewModels.NgayNhanDuHoSo))
            {
                DateTime dateTime = DateTime.ParseExact(thamDinhLapQuanLyDauTuViewModels.NgayNhanDuHoSo, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                thamDinhLapQuanLyDauTu.NgayNhanDuHoSo = dateTime;
            }
            else
            {
                thamDinhLapQuanLyDauTu.NgayNhanDuHoSo = null;
            }
            if (!string.IsNullOrEmpty(thamDinhLapQuanLyDauTuViewModels.NgayPheDuyet))
            {
                DateTime dateTime = DateTime.ParseExact(thamDinhLapQuanLyDauTuViewModels.NgayPheDuyet, "dd/MM/yyyy", new CultureInfo("vi-VN"));
                thamDinhLapQuanLyDauTu.NgayPheDuyet = dateTime;
            }
            else
            {
                thamDinhLapQuanLyDauTu.NgayPheDuyet = null;
            }
            thamDinhLapQuanLyDauTu.IdLapQuanLyDauTu = thamDinhLapQuanLyDauTuViewModels.IdLapQuanLyDauTu;
            thamDinhLapQuanLyDauTu.ToChucNguoiThamDinh = thamDinhLapQuanLyDauTuViewModels.ToChucNguoiThamDinh;
            thamDinhLapQuanLyDauTu.DanhGia = thamDinhLapQuanLyDauTuViewModels.DanhGia;
            thamDinhLapQuanLyDauTu.GhiChu = thamDinhLapQuanLyDauTuViewModels.GhiChu;

        }
        public static void UpDateHistory(this ActionHistory history, ActionHistoryViewModel historyViewModel)
        {
            history.AppUser = historyViewModel.AppUser;
            history.Action = historyViewModel.Action;
            history.Function = historyViewModel.Function;
            history.FunctionUrl = historyViewModel.FunctionUrl;
            history.ParentFunction = historyViewModel.ParentFunction;
            history.ParentFunctionUrl = historyViewModel.ParentFunctionUrl;
            history.Content = historyViewModel.Content;
            history.CreateDate = historyViewModel.CreateDate;
            history.FunctionID = historyViewModel.FunctionID;
        }

        public static void UpdateCapNhatKLNT(this CapNhatKLNT capNhat, CapNhatKLNTViewModels capNhatVM)
        {
            capNhat.IdFile = capNhatVM.IdFile;
            capNhat.FileUrl = capNhatVM.FileUrl;
            capNhat.FileName = capNhatVM.FileName;
        }

        public static void UpdateGPMBTongQuanKetQua(this GPMB_TongQuanKetQua tongQuanKetQua, GPMB_TongQuanKetQuaViewModels tongQuanKetQuaVM)
        {
            tongQuanKetQua.DatNNCaTheTuSDOD_DT = tongQuanKetQuaVM.DatNNCaTheTuSDOD_DT;
            tongQuanKetQua.DatNNCaTheTuSDOD_SoHo = tongQuanKetQuaVM.DatNNCaTheTuSDOD_SoHo;

            tongQuanKetQua.DatNNCong_DT = tongQuanKetQuaVM.DatNNCong_DT;
            tongQuanKetQua.DatNNCong_SoHo = tongQuanKetQuaVM.DatNNCong_SoHo;

            tongQuanKetQua.DatKhac_DT = tongQuanKetQuaVM.DatKhac_DT;
            tongQuanKetQua.DatKhac_SoHo = tongQuanKetQuaVM.DatKhac_SoHo;

            tongQuanKetQua.MoMa = tongQuanKetQuaVM.MoMa;
            tongQuanKetQua.Tong_DT = tongQuanKetQuaVM.Tong_DT;
            tongQuanKetQua.Tong_SoHo = tongQuanKetQuaVM.Tong_SoHo;
            tongQuanKetQua.GhiChu = tongQuanKetQuaVM.GhiChu;
        }

        public static void UpdateGPMBKetQua(this GPMB_KetQua ketQua, GPMB_KetQuaViewModels ketQuaVM)
        {
            ketQua.DHT_DT = ketQuaVM.DHT_DT;
            ketQua.DHT_SoHo = ketQuaVM.DHT_SoHo;
            ketQua.DHT_TongKinhPhi = ketQuaVM.DHT_TongKinhPhi;

            ketQua.DHT_DatNNCaThe_DT = ketQuaVM.DHT_DatNNCaThe_DT;
            ketQua.DHT_DatNNCaThe_SoHo = ketQuaVM.DHT_DatNNCaThe_SoHo;
            ketQua.DHT_DatNNCaThe_GiaTri = ketQuaVM.DHT_DatNNCaThe_GiaTri;

            ketQua.DHT_DatNNCong_DT = ketQuaVM.DHT_DatNNCong_DT;
            ketQua.DHT_DatNNCong_SoHo = ketQuaVM.DHT_DatNNCong_SoHo;
            ketQua.DHT_DatNNCong_GiaTri = ketQuaVM.DHT_DatNNCong_GiaTri;

            ketQua.DHT_DatKhac_DT = ketQuaVM.DHT_DatKhac_DT;
            ketQua.DHT_DatKhac_SoHo = ketQuaVM.DHT_DatKhac_SoHo;
            ketQua.DHT_DatKhac_GiaTri = ketQuaVM.DHT_DatKhac_GiaTri;

            ketQua.DHT_MoMa = ketQuaVM.DHT_MoMa;
            ketQua.DHT_MoMa_GiaTri = ketQuaVM.DHT_MoMa_GiaTri;

            ketQua.CTHX_TongDT = ketQuaVM.CTHX_TongDT;
            ketQua.CTHX_TongSoHo = ketQuaVM.CTHX_TongSoHo;

            ketQua.CTHX_LapTrinhHoiDong_DuThao_DT = ketQuaVM.CTHX_LapTrinhHoiDong_DuThao_DT;
            ketQua.CTHX_LapTrinhHoiDong_DuThao_SoHo = ketQuaVM.CTHX_LapTrinhHoiDong_DuThao_SoHo;

            ketQua.CTHX_CongKhai_DuThao_DT = ketQuaVM.CTHX_CongKhai_DuThao_DT;
            ketQua.CTHX_CongKhai_DuThao_SoHo = ketQuaVM.CTHX_CongKhai_DuThao_SoHo;

            ketQua.CTHX_TrinhHoiDong_ChinhThuc_DT = ketQuaVM.CTHX_TrinhHoiDong_ChinhThuc_DT;
            ketQua.CTHX_TrinhHoiDong_ChinhThuc_SoHo = ketQuaVM.CTHX_TrinhHoiDong_ChinhThuc_SoHo;

            ketQua.CTHX_QuyetDinhThuHoi_DT = ketQuaVM.CTHX_QuyetDinhThuHoi_DT;
            ketQua.CTHX_QuyetDinhThuHoi_SoHo = ketQuaVM.CTHX_QuyetDinhThuHoi_SoHo;

            ketQua.CTHX_ChiTraTien_DT = ketQuaVM.CTHX_ChiTraTien_DT;
            ketQua.CTHX_ChiTraTien_SoHo = ketQuaVM.CTHX_ChiTraTien_SoHo;

            ketQua.KKVM_DT = ketQuaVM.KKVM_DT;
            ketQua.KKVM_SoHo = ketQuaVM.KKVM_SoHo;
            ketQua.KKVM_DaPheChuaNhanTien = ketQuaVM.KKVM_DaPheChuaNhanTien;
            ketQua.KKVM_KhongHopTac = ketQuaVM.KKVM_KhongHopTac;
            ketQua.KKVM_ChuaDieuTra = ketQuaVM.KKVM_ChuaDieuTra;

            ketQua.GhiChu = ketQuaVM.GhiChu;
        }

        public static void UpdateGPMBKetQuaChiTiet(this GPMB_KetQua_ChiTiet ketQua_chiTiet, GPMB_KetQua_ChiTietViewModels ketQua_chiTietVM)
        {
            ketQua_chiTiet.IdGPMBKetQua = ketQua_chiTietVM.IdGPMBKetQua;
            ketQua_chiTiet.NoiDung = ketQua_chiTietVM.NoiDung;
        }

        //public static void UpdateGPMBKetQua(this GPMB_KetQua gpmb_ketqua, GPMB_KetQuaViewModel gpmb_ketqua)

        public static void UpdateGPMB_KhoKhanVuongMac(this GPMB_KhoKhanVuongMac kkvm, GPMB_KhoKhanVuongMacViewModels kkvm_VM)
        {
            kkvm.NguyenNhanLyDo = kkvm_VM.NguyenNhanLyDo;
            kkvm.GiaiPhapTrienKhai = kkvm_VM.GiaiPhapTrienKhai;
            kkvm.DaGiaiQuyet = kkvm_VM.DaGiaiQuyet;
        }

        public static void UpdateQuanLyDA(this QuanLyDA qlda, QuanLyDAViewModels qlda_VM)
        {
            qlda.NoiDung = qlda_VM.NoiDung;
            qlda.GhiChu = qlda_VM.GhiChu;
        }
    }
}