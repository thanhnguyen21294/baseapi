﻿using System.Collections.Generic;
using System.Linq;

namespace WebAPI.Infrastructure.Core
{
    public class PaginationSet<T>
    {
        public int PageIndex { set; get; }
        public int PageSize { get; set; }
        public int TotalRows { set; get; }
        public int? TotalFull { get; set; }
        public IEnumerable<T> Items { set; get; }
        public IEnumerable<T> AllItems { set; get; }
    }
}