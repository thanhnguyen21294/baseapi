﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Infrastructure.Core
{
    public class CongViecHangNgay<T>
    {
        public DateTime dateNow { set; get; }
        public int isCheck { set; get; }
        public IEnumerable<T> items { set; get; }
    }
}