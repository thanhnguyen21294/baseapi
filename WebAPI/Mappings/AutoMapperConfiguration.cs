﻿using AutoMapper;
using Model.Models;
using Model.Models.QLTD.GPMB;
using WebAPI.Models;
using WebAPI.Models.DuAn;
using WebAPI.Models.GPMB;
using WebAPI.Models.System;

namespace WebAPI.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AppRole, ApplicationRoleViewModel>();
                cfg.CreateMap<AppUser, AppUserViewModel>();
                cfg.CreateMap<Function, FunctionViewModel>();
                cfg.CreateMap<Permission, PermissionViewModel>();
                cfg.CreateMap<LoaiCapCongTrinh, LoaiCapCongTrinhViewModels>();
                cfg.CreateMap<NhomDuAnTheoUser, NhomDuAnTheoUserViewModels>();
                cfg.CreateMap<DuAn, DuAnViewModels>();
                cfg.CreateMap<NguonVonDuAn, NguonVonDuAnViewModels>();
                cfg.CreateMap<VanBanDuAn, VanBanDuAnViewModel>();
                cfg.CreateMap<ThuMuc, ThuMucViewModel>();
                cfg.CreateMap<ThuMucMacDinh, ThuMucMacDinhViewModel>();
                cfg.CreateMap<GoiThau, GoiThauViewModels>();
                cfg.CreateMap<HoSoMoiThau, HoSoMoiThauViewModels>();
                cfg.CreateMap<DanhMucNhaThau, DanhMucNhaThauViewModels>();
                cfg.CreateMap<KetQuaDauThau, KetQuaDauThauViewModels>();
                cfg.CreateMap<QuyetToanHopDong, QuyetToanHopDongViewModels>();
                cfg.CreateMap<NguonVonGoiThauTamUng, NguonVonGoiThauTamUngViewModels>();
                cfg.CreateMap<NguonVonGoiThauThanhToan, NguonVonGoiThauThanhToanViewModels>();
                cfg.CreateMap<ThanhToanChiPhiKhac, ThanhToanChiPhiKhacViewModels>();
                cfg.CreateMap<ThanhToanHopDong, ThanhToanHopDongViewModels>();
                cfg.CreateMap<KeHoachVon, KeHoachVonViewModels>();
                cfg.CreateMap<GiaiDoanDuAn, GiaiDoanDuAnViewModels>();
                cfg.CreateMap<NhomDuAn, NhomDuAnViewModels>();
                cfg.CreateMap<TinhTrangDuAn, TinhTrangDuAnViewModels>();
                cfg.CreateMap<NguonVon, NguonVonViewModels>();
                cfg.CreateMap<LapQuanLyDauTu, LapQuanLyDauTuViewModels>();
                cfg.CreateMap<HopDong, HopDongViewModels>();
                cfg.CreateMap<ThamDinhLapQuanLyDauTu, ThamDinhLapQuanLyDauTuViewModels>();
                cfg.CreateMap<ActionHistory, ActionHistoryViewModel>();
                cfg.CreateMap<GPMB_File, GPMB_FileViewModel>();
                cfg.CreateMap<DuAnQuanLy, DuAnQuanLyViewModels>();
            });
        }
    }
}