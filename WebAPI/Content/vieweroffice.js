$(document).ready(function () {
        SetUrlFile();
        SetHeightViewNormal();
        setInterval(function () { GetFrameset(); }, 300);
    /*
        var path = $("[id*='hdfFolder']").val();
        $(window).on("beforeunload", function () {
            $.ajax({
                type: "POST",
                url: "/_layouts/15/QLDA_ViewerOffice/Viewer.aspx/DeleteCurentFile",
                data: "{path:'" + path + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
            })
        })
        */
    });
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return decodeURI(results[1]) || 0;
    }
}
    var ifrOffice;
    var isLoadIframe = false;
    var isZoomMode = false;
    var fileExtensiton;
    var isSetEventfrmTabs = false;
    
    var sheetName = "";
    var isImageMode = false;
    function GetFrameset() {
        if (!isLoadIframe) {
            switch (fileExtensiton) {
                case ".xlsx":
                case ".xls":
                    ExcelMode();
                    break;
                case ".png": case ".gif": case ".jpg": case ".jpeg": case ".bmp": case ".tiff":
                    ImageMode();
                    break;
                default:
                    WordMode();
            }
        }
    }
    
    function ImageMode() {
        ifrOffice = $(".ifrView").contents().find("html");
        if (ifrOffice.find("body").html() != undefined) {
            isImageMode = true;
            ToggleIframe(ifrOffice);
            InitZoomIframe(ifrOffice);
            isLoadIframe = true;
        }
    }
    
    function ExcelMode() {
        ifrOffice = $(".ifrView").contents().find("html").find("frameset").find("frame[name='frSheet']").contents().find("html");
        if (!isSetEventfrmTabs) {
            $(".ifrView").contents().find("html").find("frameset").find("frame[name='frTabs']").contents().find("html").find("a[target='frSheet']").click(function () {
                isLoadIframe = false;
                isZoomMode = false;
                $("#zoom-select").val("default");
    
                sheetName = $(this).find("font").html();
                FocusTabExcel();
            })
            isSetEventfrmTabs = true;
        }
        if (ifrOffice.find("body").html() != undefined) {
            ToggleIframe(ifrOffice);
            InitZoomIframe(ifrOffice);
            isLoadIframe = true;
    
            FocusTabExcel();
        }
    }
    
    function FocusTabExcel() {
        $(".ifrView").contents().find("html").find("frameset").find("frame[name='frTabs']").contents().find("html").find("a[target='frSheet']").each(function () {
            var nude = $(this).parent().parent().parent().parent();
            if ($(this).find("font").html() == sheetName) {
                nude.attr("bgcolor", "#cccccc")
            }
            else {
                nude.attr("bgcolor", "#FFFFFF")
            }
        })
    }
    
    function WordMode() {
        ifrOffice = $(".ifrView").contents().find("html");
        if (ifrOffice.find("body").html() != undefined) {
            isLoadIframe = true;
        }
    }
    
    function ToggleIframe(ifrTemp) {
        var btnZoom = $("#spanZoom");
        if (isZoomMode) {
            isZoomMode = false;
            ifrTemp.css({ "overflow": "auto" })
            ifrTemp.css({ "cursor": "auto" })
            btnZoom.find("img").attr("src", "/Content/images/viewfile/zoom.png")
            btnZoom.find("img").attr("title", "Thu phóng tài liệu")
        }
        else {
            ifrTemp.css({ "overflow": "hidden" })
            ifrTemp.css({ "cursor": "-webkit-grab" })
            ifrTemp.css({ "cursor": "-moz-grab" })
            ifrTemp.css({ "cursor": "-ms-grab" })
            btnZoom.find("img").attr("src", "/Content/images/viewfile/cursor.png")
            btnZoom.find("img").attr("title", "Thoát chế độ thu phóng")
            isZoomMode = true;
        }
    }
    
    var hIfrView;
    //Set chiều cao cho div view và iframe
    function SetHeightViewNormal() {
        var hScreen = $("#view-office").height();
        hIfrView = hScreen - 30 - 30;
        $(".main-office").height(hIfrView + "px");
    
        var wScreen = $(window).width();
        var ifrm = wScreen - 0.02 * wScreen;
        $(".ifrView").width(ifrm + "px");
    }
    
    //Set các đường dẫn page hiển thị và file gốc để download
    function SetUrlFile() {
        var url = $.urlParam('file');//$("[id*='hdfUrl']").val();
    
        fileExtensiton = $.urlParam('ext');// $("[id*='hdfExten']").val();
        $(".ifrView").attr("src", url);
        var fileN = $.urlParam('name');// $("[id*='hdfFileName']").val();
        $("#name-office").text(fileN);
        //Đặt lại title page theo tên file
        document.title = fileN;
        var arrUrlHtm = url.split('/')
        var sheetArrName = arrUrlHtm[arrUrlHtm.length - 1].split('.');
        switch (fileExtensiton) {
            case ".xlsx": case ".xls":
                $(".zoom-toolbar").css({ "display": "block" });
                $(".zoom-dropdown").css({ "display": "block" });
                $(".zoom-dropdown option[value='fullwidth']").remove();
                sheetName = sheetArrName[0];
                break;
            case ".png": case ".gif": case ".jpg": case ".jpeg": case ".bmp": case ".tiff":
                $(".zoom-toolbar").css({ "display": "block" })
                $(".zoom-dropdown").css({ "display": "block" })
                break;
            default:
                $(".zoom-dropdown option[value='fullwidth']").remove();
                $(".zoom-toolbar").css({ "display": "none" })
                $(".zoom-dropdown").css({ "display": "none" })
        }
    }
    
    //Chuyển đổi giữu Full screen và Exit full screen
    function toggleFullscreen(id) {
        var elem = document.getElementById(id);
        if (!document.fullscreenElement && !document.mozFullScreenElement &&
            !document.webkitFullscreenElement && !document.msFullscreenElement) {
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    }
    
    //Chuyển đổi button full screen
    function toggleButtonFullScreen() {
    
        var img = $(".ic-fullscreen img");
        if (img.attr("class") == "normal-screen") {
            img.attr("class", "full-screen");
            img.attr("src", "/Content/images/viewfile/fullscreen_exit.png");
            img.attr("title", "Thoát chế độ xem toàn màn hình");
        }
        else {
            img.attr("class", "normal-screen");
            img.attr("src", "/Content/images/viewfile/fullscreen.png");
            img.attr("title", "Xem toàn màn hình")
        }
    }
    
    $(window).resize(function () {
        SetHeightViewNormal();
    })
    
    $("#spanFullScreen").click(function () {
        toggleFullscreen("view-office");
    })
    
    $("#spanZoom").click(function () {
        ToggleIframe(ifrOffice);
    })
    
    //Sự kiện fullscreen change
    $(document).on("webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange", function () {
        toggleButtonFullScreen();
    });
    
    function InitZoomIframe(ifrZoom) {
        //Event zoom
        var zoomVal = 1;
        function zoomoffice() {
            ifrZoom.css({ "transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
            ifrZoom.css({ "-ms-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
            ifrZoom.css({ "-webkit-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
        }
    
        function AutoAdjustFixZoom() {
            $("#zoom-select").val("fix");
            $("#zoom-select option[value='fix']").html((zoomVal * 100).toFixed(0) + "%");
        }
        $("#zoom-in").click(function () {
            zoomVal = zoomVal + 0.05;
            zoomoffice();
            AutoAdjustFixZoom();
        });
    
        $("#zoom-out").click(function () {
            zoomVal = zoomVal - 0.05;
            zoomoffice();
            AutoAdjustFixZoom();
        });
    
        var himg = 0;
        $("#zoom-select").change(function () {
            val = $(this).val();
            switch (val) {
                case "fix":
                    break;
                case "default":
                    if (isImageMode) {
                        ifrZoom.find("img").css({ "width": "auto" });
                    }
                    zoomVal = 1;
                    xDelta = 0;
                    yDelta = 0;
                    xDeltaOld = 0;
                    yDeltaOld = 0;
                    zoomoffice();
                    break;
                case "fullwidth":
                    if (isImageMode) {
                        ifrZoom.find("img").css({ "width": "100%" });
                        var h = $(".ifrView").height();
                        if (himg == 0) {
                            himg = ifrZoom.find("img").height();
                        }
                        yDelta = (h - himg) / 2;
                    }
                    zoomVal = 1;
                    xDelta = 0;
                    xDeltaOld = 0;
                    yDeltaOld = 0;
                    zoomoffice();
                    break;
                case "0.5":
                    zoomVal = 0.5
                    zoomoffice();
                    break;
                case "0.75":
                    zoomVal = 0.75
                    zoomoffice();
                    break;
                case "1":
                    zoomVal = 1
                    zoomoffice();
                    break;
                case "1.25":
                    zoomVal = 1.25
                    zoomoffice();
                    break;
                case "1.5":
                    zoomVal = 1.5
                    zoomoffice();
                    break;
                case "2":
                    zoomVal = 2
                    zoomoffice();
                    break;
            }
            this.blur();
        })
        //End event zoom
    
        //Mouese grab
        var xDelta = 0;
        var yDelta = 0;
        var xDeltaOld = 0;
        var yDeltaOld = 0;
    
        ifrZoom.bind('mousemove', function (e) {
            if (isZoomMode) {
                if (ifrZoom.data('clicked')) {
                    var xOld = ifrZoom.data('xOld')
                    var yOld = ifrZoom.data('yOld')
                    var xCur = e.pageX;
                    var yCur = e.pageY;
                    xDelta = xDeltaOld + xCur - xOld;
                    yDelta = yDeltaOld + yCur - yOld;
                    ifrZoom.css({ "transition": "none" });
                    ifrZoom.css({ "transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
                    ifrZoom.css({ "-ms-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
                    ifrZoom.css({ "-webkit-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
                } else {
                    xDeltaOld = xDelta;
                    yDeltaOld = yDelta;
                }
            }
        })
    
        ifrZoom.mousedown(function (e) {
            e.preventDefault();
            if (isZoomMode) {
                ifrZoom.data('clicked', true);
                ifrZoom.data('xOld', e.pageX);
                ifrZoom.data('yOld', e.pageY);
                ifrZoom.css({ "cursor": "-webkit-grabbing" })
                ifrZoom.css({ "cursor": "-moz-grabbing" })
                ifrZoom.css({ "cursor": "-ms-grabbing" })
            }
        })
        ifrZoom.mouseup(function () {
            if (isZoomMode) {
                ifrZoom.data('clicked', false);
                ifrZoom.css({ "cursor": "-webkit-grab" })
                ifrZoom.css({ "cursor": "-moz-grab" })
                ifrZoom.css({ "cursor": "-ms-grab" })
            }
        })
        //End mouese grab
    
        //Mousewheel
        var xDeltaWheelOld = 0;
        var yDeltaWheelOld = 0;
    
        ifrZoom.bind('wheel mousewheel', function (e) {
            var delta;
            if (e.originalEvent.wheelDelta !== undefined)
                delta = e.originalEvent.wheelDelta;
            else
                delta = e.originalEvent.deltaY * -1;
            if (isZoomMode) {
                if (delta > 0) {
                    zoomVal = zoomVal + 0.05;
                }
                else {
                    if (zoomVal >= 0.3) {
                        zoomVal = zoomVal - 0.05;
                    }
                }
                AutoAdjustFixZoom();
                ifrZoom.css({ "transform-origin": "" })
                ifrZoom.css({ "-ms-transform-origin": "" })
                ifrZoom.css({ "-webkit-transform-origin": "" })
                ifrZoom.css({ "transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
                ifrZoom.css({ "-ms-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
                ifrZoom.css({ "-webkit-transform": "scale(" + zoomVal + ") translate(" + (xDelta / zoomVal) + "px," + (yDelta / zoomVal) + "px)" });
            }
            else {
                ifrZoom.scrollTop(ifrZoom.scrollTop() - (delta / 2));
            }
        });
    }
    
    
    