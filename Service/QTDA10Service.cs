﻿using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IQTDA10Service
    {
      
        List<object> GetNguonVonDauTu(int idDuAn);
        List<object> GetChiPhiDauTuFromTTCPK(int idDuAn);
        List<object> GetChiPhiDauTuFromQTHD(int idDuAn);
        object GetQuyetToanByDuAn(int idDuAn);
        string GetTenQD(int idDuAn);
    }
    public class QTDA10Service : IQTDA10Service
    { 
        private IThanhToanChiPhiKhacRepository _thanhToanCPKRepository;
        private IGoiThauRepository _goiThauRepository;
        private IHopDongRepository _hopDongRepository;
        private ILapQuanLyDauTuRepository _lapQuanLyDauTuRepository;
        private INguonVonRepository _nguonVonRepository;
        private INguonVonDuAnRepository _nguonVonDuAnRepository;
        private INguonVonGoiThauTamUngRepository _nguonVonGoiThauTamUngRepository;
        private INguonVonGoiThauThanhToanRepository _nguonVonGoiThauThanhToanRepository;
        private INguonVonThanhToanChiPhiKhacRepository _nguonVonThanhToanChiPhiKhacRepository;
        private INguonVonDuAnGoiThauRepository _nguonVonDuAnGoiThauRepository;
        private IQuyetToanDuAnRepository _quyetToanDuAnRepository; 
        public QTDA10Service(INguonVonDuAnRepository nguonVonDuAnRepository, INguonVonDuAnGoiThauRepository nguonVonDuAnGoiThauRepository, INguonVonGoiThauTamUngRepository nguonVonGoiThauTamUngRepository,
            INguonVonGoiThauThanhToanRepository nguonVonGoiThauThanhToanRepository, INguonVonThanhToanChiPhiKhacRepository nguonVonThanhToanChiPhiKhacRepository, INguonVonRepository nguonVonRepository,
            ILapQuanLyDauTuRepository lapQuanLyDauTuRepository, IGoiThauRepository goiThauRepository, IHopDongRepository hopDongRepository, IThanhToanChiPhiKhacRepository thanhToanCPKRepository, IQuyetToanDuAnRepository quyetToanDuAnRepository)
        {
            _nguonVonDuAnRepository = nguonVonDuAnRepository;
            _nguonVonGoiThauTamUngRepository = nguonVonGoiThauTamUngRepository;
            _nguonVonGoiThauThanhToanRepository = nguonVonGoiThauThanhToanRepository;
            _nguonVonThanhToanChiPhiKhacRepository = nguonVonThanhToanChiPhiKhacRepository;
            _nguonVonDuAnGoiThauRepository = nguonVonDuAnGoiThauRepository;
            _nguonVonRepository = nguonVonRepository;
            _lapQuanLyDauTuRepository = lapQuanLyDauTuRepository;
            _hopDongRepository = hopDongRepository;
            _goiThauRepository = goiThauRepository;
            _thanhToanCPKRepository = thanhToanCPKRepository;
            _quyetToanDuAnRepository = quyetToanDuAnRepository;
        }
        public List<object> GetNguonVonDauTu(int idDuAn)
        {
            string[] includes = new string[] { "NguonVonDuAnGoiThaus.NguonVonGoiThauThanhToans", "NguonVon" };
           
           var chiPhiDauTus=   _nguonVonDuAnRepository.GetMulti(x => x.IdDuAn == idDuAn, includes).Select(x => new
            {
                x.IdNguonVon,
                GiaTri=x.GiaTri??0,
                GiaTriThanhToan = x.NguonVonDuAnGoiThaus.Select(nvdagt => nvdagt.NguonVonGoiThauThanhToans.Select(nvgttt => nvgttt.GiaTri).Sum()).Sum(),
               
                GiaTriThanhToanCPK = _nguonVonThanhToanChiPhiKhacRepository.GetMulti(nvttcpk => nvttcpk.IdNguonVonDuAn == x.IdNguonVonDuAn).Select(nvttcpk => nvttcpk.GiaTri).Sum(),

            }).GroupBy(x => x.IdNguonVon).Select(x => new
            {
                TenNguonVon = _nguonVonRepository.GetSingleById(x.Key).TenNguonVon,
                GiaTriDaThanhToan = x.ToList().Select(a => a.GiaTriThanhToan).Sum()+ x.ToList().Select(a => a.GiaTriThanhToanCPK).Sum(),
                GiaTriDuocDuyet= x.ToList().Select(a=>a.GiaTri).Sum(),

            }).ToList<object>();
            return chiPhiDauTus;
        }

       
        public List<object> GetChiPhiDauTuFromTTCPK(int idDuAn)
        {
            string[] includes = new string[] { "NguonVonThanhToanChiPhiKhacs" };

            var chiPhiDauTuFromTTCPK = _thanhToanCPKRepository.GetMulti(x => x.IdDuAn == idDuAn && x.LoaiThanhToan != null, includes).GroupBy(x => x.LoaiThanhToan).Select(x => new
            {
                LoaiThanhToan = x.Key,
                TongGiaTri = x.ToList().Select(ttcpk => ttcpk.NguonVonThanhToanChiPhiKhacs.Select(nvttcpk => nvttcpk.GiaTri).Sum()).Sum(),
            }).ToList<object>();

            return chiPhiDauTuFromTTCPK;
        }
        public List<object> GetChiPhiDauTuFromQTHD(int idDuAn)
        {
            string[] includes = new string[] { "QuyetToanHopDongs" };

            var chiPhiDauTuFromTTHD = _goiThauRepository.GetMulti(x => x.IdDuAn == idDuAn && x.LoaiGoiThau != null).GroupBy(x => x.LoaiGoiThau).Select(x => new
            {
                LoaiGoiThau = x.Key,
                TongGiaTri = _hopDongRepository.GetAll(includes).Where(hd => x.ToList().Select(goithau => goithau.IdGoiThau).Contains(hd.IdGoiThau)).Select(hopdong => hopdong.QuyetToanHopDongs.Where(qthd => qthd.GiaTriQuyetToan != null).Select(qthd => qthd.GiaTriQuyetToan).Sum()).Sum(),

            }).ToList<object>();
            return chiPhiDauTuFromTTHD;
        }
        public object GetQuyetToanByDuAn(int idDuAn)
        {
            var query = from lapduandautu in _lapQuanLyDauTuRepository.GetMulti(x => x.IdDuAn == idDuAn)
                        group lapduandautu by lapduandautu.IdDuAn
                        into grp
                        let lapduandautuTheoDuAnLoai1 = grp.ToList().Where(x => x.LoaiQuanLyDauTu == 1)
                        let lapduandautuTheoDuAnLoai2 = grp.ToList().Where(x => x.LoaiQuanLyDauTu == 2)
                        let lapDuAnDauTuDieuChinh = lapduandautuTheoDuAnLoai1.Where(x => x.LoaiDieuChinh == 2).OrderByDescending(x => x.NgayPheDuyet)
                        let lapDuToanDieuChinh = lapduandautuTheoDuAnLoai2.Where(x => x.LoaiDieuChinh == 2).OrderByDescending(x => x.NgayPheDuyet)
                        let flag1 = lapduandautuTheoDuAnLoai1.Count() > 0

                        let flag3 = lapDuAnDauTuDieuChinh.Count() > 0
                        let flag4 = lapDuToanDieuChinh.Count() > 0
                        select new
                        {

                            XayLap = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().XayLap :
                           lapduandautuTheoDuAnLoai1.Select(x => x.XayLap).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().XayLap :
                           lapduandautuTheoDuAnLoai2.Select(x => x.XayLap).Sum()),
                            ThietBi = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().ThietBi :
                           lapduandautuTheoDuAnLoai1.Select(x => x.ThietBi).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().ThietBi :
                           lapduandautuTheoDuAnLoai2.Select(x => x.ThietBi).Sum()),
                            QuanLyDuAn = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().QuanLyDuAn :
                           lapduandautuTheoDuAnLoai1.Select(x => x.QuanLyDuAn).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().QuanLyDuAn :
                           lapduandautuTheoDuAnLoai2.Select(x => x.QuanLyDuAn).Sum()),
                            TuVan = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().TuVan :
                           lapduandautuTheoDuAnLoai1.Select(x => x.TuVan).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().TuVan :
                           lapduandautuTheoDuAnLoai2.Select(x => x.TuVan).Sum()),
                            GiaiPhongMatBang = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().GiaiPhongMatBang :
                           lapduandautuTheoDuAnLoai1.Select(x => x.GiaiPhongMatBang).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().GiaiPhongMatBang :
                           lapduandautuTheoDuAnLoai2.Select(x => x.GiaiPhongMatBang).Sum()),
                            Khac = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().Khac :
                           lapduandautuTheoDuAnLoai1.Select(x => x.Khac).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().Khac :
                           lapduandautuTheoDuAnLoai2.Select(x => x.Khac).Sum()),

                        };
            return new
            {
                XayLap = query.Select(x => x.XayLap).Sum(),
                ThietBi = query.Select(x => x.ThietBi).Sum(),
                QuanLyDuAn = query.Select(x => x.QuanLyDuAn).Sum(),
                TuVan = query.Select(x => x.TuVan).Sum(),
                GiaiPhongMatBang = query.Select(x => x.GiaiPhongMatBang).Sum(),
                Khac = query.Select(x => x.Khac).Sum(),
            };

        }
        public string GetTenQD(int idDuAn)
        {
            string[] includes = new string[] { "CoQuanPheDuyet" };
           return  _quyetToanDuAnRepository.GetMulti(x => x.IdDuAn == idDuAn,includes).OrderByDescending(x => x.NgayPheDuyetQuyetToan).First().CoQuanPheDuyet.TenCoQuanPheDuyet;
        }


       
    }
}
