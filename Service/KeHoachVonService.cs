﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IKeHoachVonService
    {
        KeHoachVon Add(KeHoachVon iKeHoachVon);

        void Update(KeHoachVon iKeHoachVon);

        KeHoachVon Delete(int id);

        IEnumerable<KeHoachVon> GetAll();
        IEnumerable<KeHoachVon> GetAllForDieuHanhKHV();
        IEnumerable<KeHoachVon> GetAll(string filter);
        IEnumerable<KeHoachVon> GetByIdDuAn(int idDA);
        IEnumerable<KeHoachVon> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter);
        IEnumerable<KeHoachVon> GetKeHoachVonDieuChinhById(int idKeHoachVon, int page, int pageSize, string sort, out int totalRow, string filter);
        KeHoachVon GetById(int id);
        Dictionary<int, IEnumerable<NguonVonKHV>> GetKHVKeToanTheoDoi(int iNam);
        void Save();
    }
    public class KeHoachVonService : IKeHoachVonService
    {
        private IKeHoachVonRepository _KeHoachVonRepository;
        private IUnitOfWork _unitOfWork;

        public KeHoachVonService(IKeHoachVonRepository kehoachvonRepository, IUnitOfWork unitOfWork)
        {
            this._KeHoachVonRepository = kehoachvonRepository;
            this._unitOfWork = unitOfWork;
        }
        public KeHoachVon Add(KeHoachVon keHoachVon)
        {
            return _KeHoachVonRepository.Add(keHoachVon);
        }

        public KeHoachVon Delete(int id)
        {
            return _KeHoachVonRepository.Delete(id);
        }

        public IEnumerable<KeHoachVon> GetAll()
        {
            return _KeHoachVonRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonKeHoachVons" });
        }

        public IEnumerable<KeHoachVon> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _KeHoachVonRepository.GetMulti(x => x.SoQuyetDinh.Contains(filter), new string[] { "CoQuanPheDuyet", "NguonVonKeHoachVons" });
            }
            return _KeHoachVonRepository.GetKeHoachVon();
        }
        public IEnumerable<KeHoachVon> GetByIdDuAn(int idDA)
        {
            var query = _KeHoachVonRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonKeHoachVons" });
            query = query.Where(x => x.IdDuAn == idDA);
            return query.OrderBy(x => x.IdKeHoachVon);
        }
        public IEnumerable<KeHoachVon> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _KeHoachVonRepository.GetKeHoachVon();
            var q = _KeHoachVonRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonKeHoachVons" });
            if (filter != null)
            {
                query = query.Where(x => x.SoQuyetDinh.Contains(filter) && x.IdDuAn == idDA && x.IdKeHoachVonDieuChinh == null);
               
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDA && x.IdKeHoachVonDieuChinh == null);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdKeHoachVon).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public KeHoachVon GetById(int id)
        {
            return _KeHoachVonRepository.GetSingleByCondition(x => x.IdKeHoachVon == id, new string[] { "CoQuanPheDuyet", "NguonVonKeHoachVons" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(KeHoachVon updateKeHoachVon)
        {
            _KeHoachVonRepository.Update(updateKeHoachVon);
        }

        public IEnumerable<KeHoachVon> GetKeHoachVonDieuChinhById(int idKeHoachVon, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _KeHoachVonRepository.GetKeHoachVon();
          
            if (filter != null)
            {
                query = query.Where(x => x.SoQuyetDinh.Contains(filter) && x.IdKeHoachVonDieuChinh == idKeHoachVon);
            }
            else
            {
                query = query.Where(x => x.IdKeHoachVonDieuChinh==idKeHoachVon);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdKeHoachVon).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<KeHoachVon> GetAllForDieuHanhKHV()
        {
            return _KeHoachVonRepository.GetAll();
        }

        public Dictionary<int, IEnumerable<NguonVonKHV>> GetKHVKeToanTheoDoi(int iNam)
        {
            var qrKHV = _KeHoachVonRepository
                .GetMulti(x => x.NienDo == iNam && (x.IdKeHoachVonDieuChinh == null), new string[]{ "NguonVonKeHoachVons", "NguonVonKeHoachVons.NguonVonDuAn.NguonVon" })
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHV = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVon).FirstOrDefault(),
                    NguonVon = x.OrderByDescending(y => y.NgayPheDuyet).SelectMany(xx => xx.NguonVonKeHoachVons.Select(xxx => new NguonVonKHV
                    { 
                        Ma = xxx.NguonVonDuAn.NguonVon.Ma,
                        GiaTri = xxx.GiaTri,
                        GiaTriTabmis = xxx.GiaTriTabmis
                    }))
                });

            var qrKHVDC = _KeHoachVonRepository.GetMulti(x => x.NienDo == iNam && (x.IdKeHoachVonDieuChinh != null), new string[] { "NguonVonKeHoachVons", "NguonVonKeHoachVons.NguonVonDuAn.NguonVon" })
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHVDC = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVonDieuChinh).FirstOrDefault(),
                    NguonVon = x.OrderByDescending(y => y.NgayPheDuyet).SelectMany(xx => xx.NguonVonKeHoachVons.Select(xxx => new NguonVonKHV
                    {
                        Ma = xxx.NguonVonDuAn.NguonVon.Ma,
                        GiaTri = xxx.GiaTri,
                        GiaTriTabmis = xxx.GiaTriTabmis
                    }))
                });

            var query = (from item in qrKHV
                        join item1 in qrKHVDC
                        on item.IDKHV equals item1.IDKHVDC into lst
                        from item2 in lst.DefaultIfEmpty()
                        select new
                        {
                            IDDA = item.IDDA,
                            NguonVon = item2 == null ? item.NguonVon : item2.NguonVon
                        }).ToDictionary(x => x.IDDA, x=> x.NguonVon);

            return query;
        }
    }

    public class NguonVonKHV
    {
        public string Ma { get; set; }
        public double GiaTri { get; set; }
        public double GiaTriTabmis { get; set; }
    }
}
