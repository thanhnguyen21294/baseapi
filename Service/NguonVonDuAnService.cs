﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.BCModel;

namespace Service
{
    public interface INguonVonDuAnService
    {
        NguonVonDuAn Add(NguonVonDuAn nguonVonDuAn);

        void Update(NguonVonDuAn nguonVonDuAn);

        NguonVonDuAn Delete(int id);

        IEnumerable<NguonVonDuAn> GetAll();

        IEnumerable<NguonVonDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        NguonVonDuAn GetById(int id);

        void DeleteByIdDuAn(int IdDuAn);

        void DeleteByIdLapQuanLyDauTu(int IdLapQuanLyDauTu, int IdNguonVon);

        IEnumerable<NguonVonDuAn> GetByIdDuAn(int IdDuAn);
        IEnumerable<NguonVonDuAn> GetByIdLapQuanLyDauTu(int IdLapQuanLyDauTu);
        IEnumerable<NguonVonDuAn> GetByDuAn(int id);
        IEnumerable<NguonVonDuAn> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon);

        void Save();
    }

    public class NguonVonDuAnService : INguonVonDuAnService
    {
        private INguonVonDuAnRepository _nguonVonDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonDuAnService(INguonVonDuAnRepository nguonVonDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._nguonVonDuAnRepository = nguonVonDuAnRepository;
            this._unitOfWork = unitOfWork;
        }
        public NguonVonDuAn Add(NguonVonDuAn nguonVonDuAn)
        {
            return _nguonVonDuAnRepository.Add(nguonVonDuAn);
        }

        public NguonVonDuAn Delete(int id)
        {
            return _nguonVonDuAnRepository.Delete(id);
        }

        public void DeleteByIdDuAn(int idDuAn)
        {
            _nguonVonDuAnRepository.DeleteMulti(x=>x.IdDuAn == idDuAn);
        }

        public void DeleteByIdLapQuanLyDauTu(int IdLapQuanLyDauTu, int IdNguonVon)
        {
            if (IdNguonVon > 0)
            {
                _nguonVonDuAnRepository.DeleteMulti(x => x.IdLapQuanLyDauTu == IdLapQuanLyDauTu && x.IdNguonVon == IdNguonVon);
            }
            else
            {
                _nguonVonDuAnRepository.DeleteMulti(x => x.IdLapQuanLyDauTu == IdLapQuanLyDauTu);
            }
           
        }

        public IEnumerable<NguonVonDuAn> GetAll()
        {
            return _nguonVonDuAnRepository.GetAll(new[] { "NguonVon" });
        }

        public IEnumerable<NguonVonDuAn> GetByDuAn(int id)
        {
            return _nguonVonDuAnRepository.GetAll(new[] { "NguonVon" }).Where(x => x.IdDuAn == id);
        }

        public IEnumerable<NguonVonDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _nguonVonDuAnRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdNguonVonDuAn).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public NguonVonDuAn GetById(int id)
        {
            return _nguonVonDuAnRepository.GetMulti(x => x.IdNguonVonDuAn == id).SingleOrDefault();
        }
        public IEnumerable<NguonVonDuAn> GetByIdDuAn(int IdDuAn)
        {
            return _nguonVonDuAnRepository.GetMulti(x => x.IdDuAn == IdDuAn);
        }

        public IEnumerable<NguonVonDuAn> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon)
        {
            return _nguonVonDuAnRepository.GetAll().Where(x => x.IdDuAn == idduan && x.IdNguonVon == idnguonvon);
        }

        public IEnumerable<NguonVonDuAn> GetByIdLapQuanLyDauTu(int IdLapQuanLyDauTu)
        {
            return _nguonVonDuAnRepository.GetMulti(x => x.IdLapQuanLyDauTu == IdLapQuanLyDauTu);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NguonVonDuAn nguonVonDuAn)
        {
            _nguonVonDuAnRepository.Update(nguonVonDuAn);
        }

       
    }
}
