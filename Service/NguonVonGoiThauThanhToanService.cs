﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface INguonVonGoiThauThanhToanService
    {
        NguonVonGoiThauThanhToan Add(NguonVonGoiThauThanhToan nguonVonGoiThauThanhToan);
        void DeleteByIdThanhToanHopDong(int IdQTDA);
        IEnumerable<NguonVonGoiThauThanhToan> GetByThanhToanHopDong(int id);
        
        void Save();
        IEnumerable<NguonVonGoiThauThanhToan> GetAll();
    }

    public class NguonVonGoiThauThanhToanService : INguonVonGoiThauThanhToanService
    {
        private INguonVonGoiThauThanhToanRepository _NguonVonGoiThauThanhToanRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonGoiThauThanhToanService(INguonVonGoiThauThanhToanRepository nguonVonGoiThauThanhToanRepository, IUnitOfWork unitOfWork)
        {
            this._NguonVonGoiThauThanhToanRepository = nguonVonGoiThauThanhToanRepository;
            this._unitOfWork = unitOfWork;
        }

        public NguonVonGoiThauThanhToan Add(NguonVonGoiThauThanhToan nguonvongoithauThanhToan)
        {
            return _NguonVonGoiThauThanhToanRepository.Add(nguonvongoithauThanhToan);
        }

        public void DeleteByIdThanhToanHopDong(int idTTHD)
        {
            _NguonVonGoiThauThanhToanRepository.DeleteMulti(x => x.IdThanhToanHopDong == idTTHD);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
        public IEnumerable<NguonVonGoiThauThanhToan> GetAll()
        {
            return _NguonVonGoiThauThanhToanRepository.GetAll();
        }

        public IEnumerable<NguonVonGoiThauThanhToan> GetByThanhToanHopDong(int id)
        {
            return _NguonVonGoiThauThanhToanRepository.GetAll().Where(x => x.IdThanhToanHopDong == id);
        }
    }
}
