﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Service
{
    public interface INguonVonThanhToanChiPhiKhacService
    {
        NguonVonThanhToanChiPhiKhac Add(NguonVonThanhToanChiPhiKhac nguonVonQuyetToanDuAn);
        void DeleteByIdThanhToanChiPhiKhac(int IdQTDA);
        IEnumerable<NguonVonThanhToanChiPhiKhac> GetAll();
        IEnumerable<NguonVonThanhToanChiPhiKhac> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon);
        IEnumerable<NguonVonThanhToanChiPhiKhac> GetByIdDuAn(int idduan);
        void Save();
    }

    public class NguonVonThanhToanChiPhiKhacService : INguonVonThanhToanChiPhiKhacService
    {
        private INguonVonThanhToanChiPhiKhacRepository _NguonVonThanhToanChiPhiKhacRepository;
        private INguonVonDuAnService _nguonVonDuAnService;
        private IUnitOfWork _unitOfWork;

        public NguonVonThanhToanChiPhiKhacService(INguonVonThanhToanChiPhiKhacRepository nguonVonThanhToanChiPhiKhacRepository, INguonVonDuAnService nguonVonDuAnService, IUnitOfWork unitOfWork)
        {
            this._NguonVonThanhToanChiPhiKhacRepository = nguonVonThanhToanChiPhiKhacRepository;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._unitOfWork = unitOfWork;
        }

        public NguonVonThanhToanChiPhiKhac Add(NguonVonThanhToanChiPhiKhac nguonvonthanhtoanChiPhiKhac)
        {
            return _NguonVonThanhToanChiPhiKhacRepository.Add(nguonvonthanhtoanChiPhiKhac);
        }

        public void DeleteByIdThanhToanChiPhiKhac(int idTTCPK)
        {
            _NguonVonThanhToanChiPhiKhacRepository.DeleteMulti(x => x.IdThanhToanChiPhiKhac == idTTCPK);
        }

        public IEnumerable<NguonVonThanhToanChiPhiKhac>  GetAll()
        {
           return  _NguonVonThanhToanChiPhiKhacRepository.GetAll();
        }

        public IEnumerable<NguonVonThanhToanChiPhiKhac> GetByIdDuAn(int idduan)
        {
            //lay ra tat ca thanh toan chi phi khac theo nguon von
            var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdDuAn == idduan).Select(x => x.IdNguonVonDuAn);
            List<NguonVonThanhToanChiPhiKhac> nvttchks = new List<NguonVonThanhToanChiPhiKhac>();
            foreach (var item in nguonvonduans)
            {
                var nvttchk = this.GetAll().Where(x => x.IdNguonVonDuAn == item);
                foreach (var item1 in nvttchk)
                {
                    nvttchks.Add(item1);
                }
            }
            return nvttchks;
        }

        public IEnumerable<NguonVonThanhToanChiPhiKhac> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon)
        {
            //lay ra tat ca thanh toan chi phi khac theo nguon von
            var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdNguonVon == idnguonvon && x.IdDuAn == idduan).Select(x => x.IdNguonVonDuAn);
            List<NguonVonThanhToanChiPhiKhac> nvttchks = new List<NguonVonThanhToanChiPhiKhac>();
            foreach (var item in nguonvonduans)
            {
                var nvttchk = this.GetAll().Where(x => x.IdNguonVonDuAn == item);
                foreach (var item1 in nvttchk)
                {
                    nvttchks.Add(item1);
                }
            }
            return nvttchks;

        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
