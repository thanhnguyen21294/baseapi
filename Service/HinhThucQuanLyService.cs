﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IHinhThucQuanLyService
    {
        HinhThucQuanLy Add(HinhThucQuanLy hinhThucQuanLy);

        void Update(HinhThucQuanLy hinhThucQuanLy);

        HinhThucQuanLy Delete(int id);

        IEnumerable<HinhThucQuanLy> GetAll();

        IEnumerable<HinhThucQuanLy> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        HinhThucQuanLy GetById(int id);

        void Save();
    }

    public class HinhThucQuanLyService : IHinhThucQuanLyService
    {
        private IHinhThucQuanLyRepository _hinhThucQuanLyRepository;
        private IUnitOfWork _unitOfWork;

        public HinhThucQuanLyService(IHinhThucQuanLyRepository hinhThucQuanLyRepository, IUnitOfWork unitOfWork)
        {
            this._hinhThucQuanLyRepository = hinhThucQuanLyRepository;
            this._unitOfWork = unitOfWork;
        }
        public HinhThucQuanLy Add(HinhThucQuanLy hinhThucQuanLy)
        {
            return _hinhThucQuanLyRepository.Add(hinhThucQuanLy);
        }

        public HinhThucQuanLy Delete(int id)
        {
            return _hinhThucQuanLyRepository.Delete(id);
        }

        public IEnumerable<HinhThucQuanLy> GetAll()
        {
            return _hinhThucQuanLyRepository.GetAll();
        }

        public IEnumerable<HinhThucQuanLy> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _hinhThucQuanLyRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenHinhThucQuanLy.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHinhThucQuanLy).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public HinhThucQuanLy GetById(int id)
        {
            return _hinhThucQuanLyRepository.GetMulti(x => x.IdHinhThucQuanLy == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HinhThucQuanLy hinhThucQuanLy)
        {
            _hinhThucQuanLyRepository.Update(hinhThucQuanLy);
        }
    }
}
