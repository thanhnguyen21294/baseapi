﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IDuAnQuanLyService
    {
        DuAnQuanLy Add(DuAnQuanLy DuAnQuanLy);

        void Update(DuAnQuanLy DuAnQuanLy);

        DuAnQuanLy Delete(int id);

        IEnumerable<DuAnQuanLy> GetAll();

        DuAnQuanLy GetById(int id);

        IEnumerable<DuAnQuanLy> GetByIdDA(int idDA);

        void Save();
    }

    public class DuAnQuanLyService : IDuAnQuanLyService
    {
        private IDuAnQuanLyRepository _DuAnQuanLyRepository;
        private IUnitOfWork _unitOfWork;

        public DuAnQuanLyService(IDuAnQuanLyRepository duAnQuanLyRepository, IUnitOfWork unitOfWork)
        {
            this._DuAnQuanLyRepository = duAnQuanLyRepository;
            this._unitOfWork = unitOfWork;
        }
        public DuAnQuanLy Add(DuAnQuanLy DuAnQuanLy)
        {
            return _DuAnQuanLyRepository.Add(DuAnQuanLy);
        }

        public DuAnQuanLy Delete(int id)
        {
            return _DuAnQuanLyRepository.Delete(id);
        }

        public IEnumerable<DuAnQuanLy> GetAll()
        {
            return _DuAnQuanLyRepository.GetAll();
        }
        public IEnumerable<DuAnQuanLy> GetByIdDA(int idDA)
        {
            return _DuAnQuanLyRepository.GetMulti(x => x.IdDuAn == idDA);
        }
        public DuAnQuanLy GetById(int id)
        {
            return _DuAnQuanLyRepository.GetMulti(x => x.Id == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DuAnQuanLy DuAnQuanLy)
        {
            _DuAnQuanLyRepository.Update(DuAnQuanLy);
        }
    }
}
