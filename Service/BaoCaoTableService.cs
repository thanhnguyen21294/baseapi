﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service
{

    public interface IBaoCaoTableService
    {
        BaoCaoTable Add(BaoCaoTable baoCaoTable);
        BaoCaoTable AddBaoCao(string documentName, string phanLoai, string folderReport, string userName);
        void Update(BaoCaoTable BaoCaoTable);

        BaoCaoTable Delete(int id);

        IEnumerable<BaoCaoTable> GetAll();

        IEnumerable<BaoCaoTable> GetByPhanLoai(string phanloai);

        IEnumerable<BaoCaoTable> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        BaoCaoTable GetById(int id);

        void Save();
    }

    public class BaoCaoTableService : IBaoCaoTableService
    {
        private IBaoCaoTableRepository _BaoCaoTableRepository;
        private IUnitOfWork _unitOfWork;

        public BaoCaoTableService(IBaoCaoTableRepository BaoCaoTableRepository, IUnitOfWork unitOfWork)
        {
            this._BaoCaoTableRepository = BaoCaoTableRepository;
            this._unitOfWork = unitOfWork;
        }
        public BaoCaoTable Add(BaoCaoTable baoCaoTable)
        {
            return _BaoCaoTableRepository.Add(baoCaoTable);
        }
        public BaoCaoTable AddBaoCao(string documentName, string phanLoai, string folderReport, string userName)
        {
            BaoCaoTable bc = new BaoCaoTable();
            bc.Ten = documentName;
            bc.CreatedDate = DateTime.Now;
            bc.CreatedBy = userName;
            bc.PhanLoai = phanLoai;
            bc.URL = folderReport + "/" + documentName;
            return this.Add(bc);
        }

        public BaoCaoTable Delete(int id)
        {
            return _BaoCaoTableRepository.Delete(id);
        }

        public IEnumerable<BaoCaoTable> GetAll()
        {
            return _BaoCaoTableRepository.GetAll();
        }

        public IEnumerable<BaoCaoTable> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _BaoCaoTableRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.Ten.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdBaoCao).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public BaoCaoTable GetById(int id)
        {
            return _BaoCaoTableRepository.GetMulti(x => x.IdBaoCao == id).SingleOrDefault();
        }

        public IEnumerable<BaoCaoTable> GetByPhanLoai(string phanloai)
        {
            var query = _BaoCaoTableRepository.GetAll().Where(x => x.PhanLoai == phanloai).OrderByDescending(x => x.CreatedDate);
            //totalRow = query.Count();
            //return query.Skip((page - 1) * pageSize).Take(pageSize);
            return query;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(BaoCaoTable BaoCaoTable)
        {
            _BaoCaoTableRepository.Update(BaoCaoTable);
        }
    }
}
