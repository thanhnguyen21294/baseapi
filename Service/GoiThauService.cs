﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IGoiThauService
    {
        GoiThau Add(GoiThau goiThau);

        void Update(GoiThau goiThau);

        GoiThau Delete(int id);

        IEnumerable<GoiThau> GetAll();

        IEnumerable<int> GetForLocBaoCao(List<int> idLoais);

        IEnumerable<GoiThau> GetAllID(int idKH);

        IEnumerable<GoiThau> GetByIdDuAn(int idDuAn);

        IEnumerable<GoiThau> GetByFilter(int idKeHoach, int page, int pageSize, out int totalRow, string filter);
        IEnumerable<GoiThau> GetListByIdKeHoach(int idKeHoach);
        GoiThau GetById(int id);
        IEnumerable<GoiThau> GetByHoSoMoiThau(int idDuAn);

        void DeleteByIdKeHoach(int id);

        void Save();
    }

    public class GoiThauService : IGoiThauService
    {
        private IGoiThauRepository _GoiThauRepository;
        private IUnitOfWork _unitOfWork;
        private IKeHoachLuaChonNhaThauService _keHoachLuaChonNhaThauService;

        public GoiThauService(IGoiThauRepository goiThauRepository, IUnitOfWork unitOfWork, IKeHoachLuaChonNhaThauService keHoachLuaChonNhaThauService)
        {
            this._GoiThauRepository = goiThauRepository;
            this._unitOfWork = unitOfWork;
            this._keHoachLuaChonNhaThauService = keHoachLuaChonNhaThauService;
        }
        public GoiThau Add(GoiThau goiThau)
        {
            return _GoiThauRepository.Add(goiThau);
        }

        public GoiThau Delete(int id)
        {
            return _GoiThauRepository.Delete(id);
        }
        public IEnumerable<int> GetForLocBaoCao(List<int> idLoais)
        {
            return _GoiThauRepository.GetMulti(x => x.LoaiGoiThau != null).Where(x=> idLoais.Contains(Convert.ToInt32(x.LoaiGoiThau))).Select(x=> x.IdDuAn);
        }
        public IEnumerable<GoiThau> GetAll()
        {
            return _GoiThauRepository.GetAll();
        }

        public IEnumerable<GoiThau> GetAllID(int idKH)
        {
            return _GoiThauRepository.GetAll(new[] { "PhuongThucDauThau", "HinhThucLuaChonNhaThau" }).Where(x=>x.IdKeHoachLuaChonNhaThau == idKH);


        }

        public IEnumerable<GoiThau> GetByIdDuAn(int idDuAn)
        {
            var query = _GoiThauRepository.GetAll();
            query = query.Where(x => x.IdDuAn == idDuAn);
            return query.OrderBy(x => x.IdGoiThau);
        }

        public IEnumerable<GoiThau> GetByFilter(int idKeHoach, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _GoiThauRepository.GetMulti(x=>x.IdKeHoachLuaChonNhaThau == idKeHoach, new []{ "PhuongThucDauThau", "HinhThucLuaChonNhaThau" });
            if (filter != null)
            {
                query = query.Where(x => x.TenGoiThau.Contains(filter));
            }
            totalRow = query.Count();
            var goiThau = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!goiThau.Any() && page > 1)
            //{
            //    page = page - 1;
            //    goiThau = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            return goiThau;
        }

        public IEnumerable<GoiThau> GetListByIdKeHoach(int idKeHoach)
        {
            return _GoiThauRepository.GetMulti(x=>x.IdKeHoachLuaChonNhaThau == idKeHoach);
        }

        public GoiThau GetById(int id)
        {
            return _GoiThauRepository.GetSingleByCondition(x=>x.IdGoiThau == id, new []{ "NguonVonDuAnGoiThaus" });
        }

        public IEnumerable<GoiThau> GetByHoSoMoiThau(int idDuAn)
        {
            var query = from kh in _keHoachLuaChonNhaThauService.GetChild(idDuAn)
                join goiThau in _GoiThauRepository.GetMulti(x => x.IdDuAn == idDuAn) on kh.IdKeHoachLuaChonNhaThau
                    equals goiThau.IdKeHoachLuaChonNhaThau
                select goiThau;
            return query;
        }

        public void DeleteByIdKeHoach(int id)
        {
            _GoiThauRepository.DeleteMulti(x=>x.IdKeHoachLuaChonNhaThau == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GoiThau goiThau)
        {
            _GoiThauRepository.Update(goiThau);
        }
    }
}
