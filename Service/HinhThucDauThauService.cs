﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IHinhThucDauThauService
    {
        HinhThucDauThau Add(HinhThucDauThau hinhThucDauThau);

        void Update(HinhThucDauThau loaiCapCongTrinh);

        HinhThucDauThau Delete(int id);

        IEnumerable<HinhThucDauThau> GetAll();

        IEnumerable<HinhThucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        HinhThucDauThau GetById(int id);

        void Save();
    }
    public class HinhThucDauThauService : IHinhThucDauThauService
    {
        private IHinhThucDauThauRepository _hinhThucDauThauRepository;
        private IUnitOfWork _unitOfWork;

        public HinhThucDauThauService(IHinhThucDauThauRepository hinhThucDauThauRepository, IUnitOfWork unitOfWork)
        {
            this._hinhThucDauThauRepository = hinhThucDauThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public HinhThucDauThau Add(HinhThucDauThau hinhThucDauThau)
        {
            return _hinhThucDauThauRepository.Add(hinhThucDauThau);
        }

        public HinhThucDauThau Delete(int id)
        {
            return _hinhThucDauThauRepository.Delete(id);
        }

        public IEnumerable<HinhThucDauThau> GetAll()
        {
            return _hinhThucDauThauRepository.GetAll();
        }

        public IEnumerable<HinhThucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _hinhThucDauThauRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenHinhThucDauThau.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHinhThucDauThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public HinhThucDauThau GetById(int id)
        {
            return _hinhThucDauThauRepository.GetMulti(x => x.IdHinhThucDauThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HinhThucDauThau hinhThucDauThau)
        {
            _hinhThucDauThauRepository.Update(hinhThucDauThau);
        }
    }
}
