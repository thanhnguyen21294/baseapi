﻿using Common;
using Data;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Service
{
    public interface IActionHistoryService
    {
        ActionHistory Add(ActionHistory history);

        void Update(ActionHistory history);

        IEnumerable<ActionHistory> GetAll(int page, int pageSize, out int totalRow, string filter, int loaitimkiem, DateTime? begindate = null, DateTime? enddate = null);

        IEnumerable<ActionHistory> GetByIdUser(string appuser, int page, int pageSize, out int totalRow, string filter, int loaitimkiem, DateTime? begindate = null, DateTime? enddate = null);

        ActionHistory GetById(int id);

        string RemoveSpace(string str);

        List<string> ToList(string str);

        ActionHistory DeleteByID(int id);

        DateTime GetNgayGanNhat(DateTime date, int loai, int so, DateTime datenow);

        void DeleteHistory(int kieuxoa, int chuky, int thang, int thu, int ngay, int gio, int phut, DateTime? ngayxoa, int sosanh, string filePath, int checkcontinue, int checkauto);

        void Save();
    }

    public class HistoryService : IActionHistoryService
    {
        private IActionHistoryRepository _historyRepository;
        private IUnitOfWork _unitOfWork;
        private IDuAnService _duAnService;
        private IFunctionService _functionService;
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        //CancellationTokenSource canceltoken;

        public HistoryService(IActionHistoryRepository historyRepository, IUnitOfWork unitOfWork, IDuAnService duAnService, IFunctionService functionService)
        {
            this._historyRepository = historyRepository;
            this._duAnService = duAnService;
            this._functionService = functionService;
            this._unitOfWork = unitOfWork;
            //var _cancellation = new CancellationTokenSource();
            //this.canceltoken = _cancellation;
        }

        public ActionHistory Add(ActionHistory history)
        {
            return _historyRepository.Add(history);
        }

        public ActionHistory DeleteByID(int id)
        {
            return _historyRepository.Delete(id);
        }

        public void DeleteHistory(int kieuxoa, int chuky, int thang, int thu, int ngay, int gio, int phut, DateTime? ngayxoa, int sosanh, string filePath, int checkcontinue, int checkauto)
        {
            using (var context = new DataContext())
            {
                //xóa bằng tay
                string str2 = "auto.log";

                var path3 = Path.Combine(filePath, str2);
                if (kieuxoa == 2)
                {
                    //xóa hàng ngày
                    if (chuky == 0)
                    {
                        DateTime today = DateTime.Today;
                        today = today.AddDays(-1).AddHours(gio).AddMinutes(phut);

                        //var path = today.Day.ToString() + today.Month.ToString() + today.Year.ToString() +today.Hour.ToString() + today.Minute.ToString() + today.Second.ToString() + ".log";
                        var path = kieuxoa + "." + DateTime.Now.Ticks + ".log";

                        var path2 = Path.Combine(filePath, path);
                        // serialize JSON to a string and then write string to a file

                        if (sosanh == 0)
                        {
                            TimeSpan ts = DateTime.Today.AddHours(gio).AddMinutes(phut) - DateTime.Now;
                            int tsnumber = (int) ts.TotalMilliseconds;
                            if (tsnumber < 0)
                            {
                                ts = (DateTime.Today.AddDays(1) - DateTime.Today) + ts;
                            }

                            Task.Delay(ts, cancellationTokenSource.Token).ContinueWith(x =>
                            {
                                if (checkauto == 0)
                                {
                                    cancellationTokenSource.Cancel();
                                }
                                else
                                {
                                    if (File.Exists(path3))
                                    {
                                        string jsonString2 = File.ReadAllText(path3);
                                        var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                        int icheck = sponsors2.Id;
                                        if (icheck == checkauto)
                                        {
                                            sosanh++;
                                            using (var context1 = new DataContext())
                                            {
                                                var query = context1.ActionHistories.Where(z => z.CreateDate <= today);
                                                File.WriteAllText(path2, JsonConvert.SerializeObject(query));
                                                // serialize JSON directly to a file
                                                using (StreamWriter file = File.CreateText(path2))
                                                {
                                                    JsonSerializer serializer = new JsonSerializer();
                                                    serializer.Serialize(file, query);
                                                }
                                                foreach (var item in query)
                                                {
                                                    context1.ActionHistories.Remove(item);
                                                }
                                                context1.SaveChanges();
                                            }

                                            this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                        }
                                        else
                                        {
                                            cancellationTokenSource.Cancel();
                                        }
                                    }
                                }
                            });
                        }
                        else
                        {
                            Task.Delay(TimeSpan.FromSeconds(86400), cancellationTokenSource.Token).ContinueWith(x =>
                            {
                                if (checkauto == 0)
                                {
                                    cancellationTokenSource.Cancel();
                                }
                                else
                                {
                                    if (File.Exists(path3))
                                    {
                                        string jsonString2 = File.ReadAllText(path3);
                                        var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                        int icheck = sponsors2.Id;
                                        if (icheck == checkauto)
                                        {
                                            sosanh++;
                                            using (var context1 = new DataContext())
                                            {
                                                var query = context1.ActionHistories.Where(z => z.CreateDate <= today);
                                                File.WriteAllText(path2, JsonConvert.SerializeObject(query));
                                                // serialize JSON directly to a file
                                                using (StreamWriter file = File.CreateText(path2))
                                                {
                                                    JsonSerializer serializer = new JsonSerializer();
                                                    serializer.Serialize(file, query);
                                                }
                                                foreach (var item in query)
                                                {
                                                    context1.ActionHistories.Remove(item);
                                                }
                                                context1.SaveChanges();
                                            }

                                            this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                        }
                                        else
                                        {
                                            cancellationTokenSource.Cancel();
                                        }
                                    }
                                }
                            });
                        }
                    }
                    //Xóa hàng tuần
                    else if (chuky == 1)
                    {
                        DateTime nextweek = DateTime.Today;
                        var ngaytrongtuan = DateTime.Today.DayOfWeek;
                        int numberngaytrogntuan = 0;
                        if (ngaytrongtuan.ToString() == "Monday")
                        {
                            numberngaytrogntuan = 2;
                        }
                        else if (ngaytrongtuan.ToString() == "Tuesday")
                        {
                            numberngaytrogntuan = 3;
                        }
                        else if (ngaytrongtuan.ToString() == "Wednesday")
                        {
                            numberngaytrogntuan = 4;
                        }
                        else if (ngaytrongtuan.ToString() == "Thursday")
                        {
                            numberngaytrogntuan = 5;
                        }
                        else if (ngaytrongtuan.ToString() == "Friday")
                        {
                            numberngaytrogntuan = 6;
                        }
                        else if (ngaytrongtuan.ToString() == "Saturday")
                        {
                            numberngaytrogntuan = 7;
                        }
                        else if (ngaytrongtuan.ToString() == "Sunday")
                        {
                            numberngaytrogntuan = 8;
                        }
                        int songaycanthem = 0;
                        if (thu > numberngaytrogntuan)
                        {
                            songaycanthem = thu - numberngaytrogntuan;
                        }
                        else if (thu < numberngaytrogntuan)
                        {
                            songaycanthem = thu + 7 - numberngaytrogntuan;
                        }
                        else
                        {
                            songaycanthem = 0;
                        }
                        nextweek = nextweek.AddDays(songaycanthem).AddHours(gio).AddMinutes(phut);
                        DateTime lastweek = nextweek.AddDays(-7);
                        //var path = today.Day.ToString() + today.Month.ToString() + today.Year.ToString() +today.Hour.ToString() + today.Minute.ToString() + today.Second.ToString() + ".log";
                        var path = kieuxoa + "." + DateTime.Now.Ticks + ".log";

                        var path2 = Path.Combine(filePath, path);
                        // serialize JSON to a string and then write string to a file

                        if (sosanh == 0)
                        {
                            TimeSpan timetuancanthemlandau = nextweek - DateTime.Now;
                            int i = (int)timetuancanthemlandau.TotalSeconds;
                            Task.Delay(TimeSpan.FromSeconds(i)).ContinueWith(x =>
                              {
                                  if (checkauto == 0)
                                  {
                                      cancellationTokenSource.Cancel();
                                  }
                                  else
                                  {
                                      if (File.Exists(path3))
                                      {
                                          string jsonString2 = File.ReadAllText(path3);
                                          var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                          int icheck = sponsors2.Id;
                                          if (icheck == checkauto)
                                          {
                                              sosanh++;
                                              using (var context1 = new DataContext())
                                              {
                                                  var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastweek);
                                                  File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                  // serialize JSON directly to a file
                                                  using (StreamWriter file = File.CreateText(path2))
                                                  {
                                                      JsonSerializer serializer = new JsonSerializer();
                                                      serializer.Serialize(file, query1);
                                                  }
                                                  foreach (var item in query1)
                                                  {
                                                      context1.ActionHistories.Remove(item);
                                                  }
                                                  context1.SaveChanges();
                                              }
                                              this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                          }
                                          else
                                          {
                                              cancellationTokenSource.Cancel();
                                          }
                                      }
                                  }
                              });
                        }
                        else
                        {
                            Task.Delay(7 * 86400000).ContinueWith(x =>
                              {
                                  if (checkauto == 0)
                                  {
                                      cancellationTokenSource.Cancel();
                                  }
                                  else
                                  {
                                      if (File.Exists(path3))
                                      {
                                          string jsonString2 = File.ReadAllText(path3);
                                          var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                          int icheck = sponsors2.Id;
                                          if (icheck == checkauto)
                                          {
                                              sosanh++;
                                              using (var context1 = new DataContext())
                                              {
                                                  var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastweek);
                                                  File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                  // serialize JSON directly to a file
                                                  using (StreamWriter file = File.CreateText(path2))
                                                  {
                                                      JsonSerializer serializer = new JsonSerializer();
                                                      serializer.Serialize(file, query1);
                                                  }
                                                  foreach (var item in query1)
                                                  {
                                                      context1.ActionHistories.Remove(item);
                                                  }
                                                  context1.SaveChanges();
                                              }
                                              this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                          }
                                          else
                                          {
                                              cancellationTokenSource.Cancel();
                                          }
                                      }
                                  }
                              });
                        }
                    }
                    //Xóa hàng tháng
                    else if (chuky == 2)
                    {
                        DateTime nextmonth = DateTime.Today;
                        var tongngaytrongthang = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);
                        if (tongngaytrongthang < ngay)
                        {
                            ngay = tongngaytrongthang;
                        }
                        var ngaycuathang = DateTime.Today.Day;
                        int songaycanthem = 0;
                        if (ngay > ngaycuathang)
                        {
                            songaycanthem = ngay - ngaycuathang;
                        }
                        else if (ngay < ngaycuathang)
                        {
                            songaycanthem = tongngaytrongthang + ngay - ngaycuathang;
                        }
                        else
                        {
                            songaycanthem = 0;
                        }
                        nextmonth = nextmonth.AddDays(songaycanthem).AddHours(gio).AddMinutes(phut);
                        DateTime lastmonth = nextmonth.AddMonths(-1);
                        //var query = context.ActionHistories.Where(x => x.CreateDate <= lastmonth);
                        //var path = today.Day.ToString() + today.Month.ToString() + today.Year.ToString() +today.Hour.ToString() + today.Minute.ToString() + today.Second.ToString() + ".log";
                        var path = kieuxoa + "." + DateTime.Now.Ticks + ".log";

                        var path2 = Path.Combine(filePath, path);
                        context.SaveChanges();
                        if (sosanh == 0)
                        {
                            TimeSpan ts2 = nextmonth - DateTime.Now;
                            int giaycanthem = (int)ts2.TotalSeconds;
                            int dem = (int)giaycanthem / (500 * 3600);
                            int thapphan = giaycanthem - (dem * 500 * 3600);

                            Task.Delay(TimeSpan.FromMilliseconds(thapphan * 1000)).ContinueWith(x =>
                                  {
                                      if (dem > 0)
                                      {
                                          for (var i = 0; i < dem; i++)
                                          {
                                              Thread.Sleep(500 * 3600 * 1000);
                                          }
                                      }
                                      if (checkauto == 0)
                                      {
                                          cancellationTokenSource.Cancel();
                                      }
                                      else
                                      {
                                          if (File.Exists(path3))
                                          {
                                              string jsonString2 = File.ReadAllText(path3);
                                              var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                              int icheck = sponsors2.Id;
                                              if (icheck == checkauto)
                                              {
                                                  sosanh++;
                                                  using (var context1 = new DataContext())
                                                  {
                                                      var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastmonth);
                                                      File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                      // serialize JSON directly to a file
                                                      using (StreamWriter file = File.CreateText(path2))
                                                      {
                                                          JsonSerializer serializer = new JsonSerializer();
                                                          serializer.Serialize(file, query1);
                                                      }
                                                      foreach (var item in query1)
                                                      {
                                                          context1.ActionHistories.Remove(item);
                                                      }
                                                      context1.SaveChanges();
                                                  }
                                                  this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                              }
                                              else
                                              {
                                                  cancellationTokenSource.Cancel();
                                              }
                                          }
                                      }
                                  });
                        }
                        else
                        {
                            var tongngaytrongthangsau = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);
                            int giaycanthem = tongngaytrongthangsau * 24 * 3600;
                            int dem = (int)giaycanthem / (500 * 3600);
                            int thapphan = giaycanthem - (dem * 500 * 3600);

                            Task.Delay(TimeSpan.FromMilliseconds(thapphan * 1000)).ContinueWith(x =>
                                  {
                                      if (dem > 0)
                                      {
                                          for (var i = 0; i < dem; i++)
                                          {
                                              Thread.Sleep(500 * 3600 * 1000);
                                          }
                                      }
                                      if (checkauto == 0)
                                      {
                                          cancellationTokenSource.Cancel();
                                      }
                                      else
                                      {
                                          if (File.Exists(path3))
                                          {
                                              string jsonString2 = File.ReadAllText(path3);
                                              var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                              int icheck = sponsors2.Id;
                                              if (icheck == checkauto)
                                              {
                                                  sosanh++;
                                                  using (var context1 = new DataContext())
                                                  {
                                                      var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastmonth);
                                                      File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                      // serialize JSON directly to a file
                                                      using (StreamWriter file = File.CreateText(path2))
                                                      {
                                                          JsonSerializer serializer = new JsonSerializer();
                                                          serializer.Serialize(file, query1);
                                                      }
                                                      foreach (var item in query1)
                                                      {
                                                          context1.ActionHistories.Remove(item);
                                                      }
                                                      context1.SaveChanges();
                                                  }
                                                  this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                              }
                                              else
                                              {
                                                  cancellationTokenSource.Cancel();
                                              }
                                          }
                                      }
                                  });
                        }
                    }
                    else
                    {
                        DateTime nextyear = DateTime.Today;
                        var thangtrongnam = DateTime.Today.Month;
                        var tongngaytrongthang = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);
                        if (tongngaytrongthang < ngay)
                        {
                            ngay = tongngaytrongthang;
                        }
                        var ngaycuathang = DateTime.Today.Day;
                        int sothangcanthem = 0;
                        int songaycanthem = 0;
                        if (thang > thangtrongnam)
                        {
                            sothangcanthem = thang - thangtrongnam;
                            songaycanthem = ngay - ngaycuathang;
                        }
                        else if (thang < thangtrongnam)
                        {
                            sothangcanthem = thang + 12 - thangtrongnam;
                            songaycanthem = ngay - ngaycuathang;
                        }
                        else
                        {
                            sothangcanthem = 0;
                            songaycanthem = ngay - ngaycuathang;
                        }
                        //if (songaycanthem < 0)
                        //{
                        //    songaycanthem = (-1) * songaycanthem;
                        //    sothangcanthem = sothangcanthem - 1;
                        //}
                        nextyear = nextyear.AddMonths(sothangcanthem).AddDays(songaycanthem).AddHours(gio).AddMinutes(phut).AddYears(-1);
                        DateTime lastyear = nextyear.AddYears(-1);
                        //var query = context.ActionHistories.Where(x => x.CreateDate <= lastyear);
                        //var path = today.Day.ToString() + today.Month.ToString() + today.Year.ToString() +today.Hour.ToString() + today.Minute.ToString() + today.Second.ToString() + ".log";
                        var path = kieuxoa + "." + DateTime.Now.Ticks + ".log";

                        var path2 = Path.Combine(filePath, path);

                        if (sosanh == 0)
                        {
                            var a = DateTime.Today.AddMonths(sothangcanthem).AddDays(songaycanthem).AddHours(gio).AddMinutes(phut);
                            TimeSpan timeSpan = DateTime.Today.AddMonths(sothangcanthem).AddDays(songaycanthem).AddHours(gio).AddMinutes(phut) - DateTime.Today;
                            int index = (int)timeSpan.TotalMinutes;
                            int giaycanthem = index * 60;
                            int dem = (int)giaycanthem / (500 * 3600);
                            int thapphan = giaycanthem - (dem * 500 * 3600);
                            Task.Delay(TimeSpan.FromMilliseconds(thapphan * 1000)).ContinueWith(x =>
                              {
                                  if (dem > 0)
                                  {
                                      for (var i = 0; i < dem; i++)
                                      {
                                          Thread.Sleep(500 * 3600 * 1000);
                                      }
                                  }
                                  if (checkauto == 0)
                                  {
                                      cancellationTokenSource.Cancel();
                                  }
                                  else
                                  {
                                      if (File.Exists(path3))
                                      {
                                          string jsonString2 = File.ReadAllText(path3);
                                          var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                          int icheck = sponsors2.Id;
                                          if (icheck == checkauto)
                                          {
                                              sosanh++;
                                              using (var context1 = new DataContext())
                                              {
                                                  var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastyear);
                                                  File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                  // serialize JSON directly to a file
                                                  using (StreamWriter file = File.CreateText(path2))
                                                  {
                                                      JsonSerializer serializer = new JsonSerializer();
                                                      serializer.Serialize(file, query1);
                                                  }
                                                  foreach (var item in query1)
                                                  {
                                                      context1.ActionHistories.Remove(item);
                                                  }
                                                  context1.SaveChanges();
                                              }
                                              this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                          }
                                          else
                                          {
                                              cancellationTokenSource.Cancel();
                                          }
                                      }
                                  }
                              });
                        }
                        else
                        {
                            DateTime dec31 = new DateTime(2010, 12, 31);
                            DateTime dateToDisplay = dec31.AddYears(DateTime.Today.Year - 2010);
                            var a = dateToDisplay.DayOfYear;
                            int giaycanthem = a * 24 * 60 * 60;
                            int dem = (int)giaycanthem / (500 * 3600);
                            int thapphan = giaycanthem - (dem * 500 * 3600);
                            Task.Delay(TimeSpan.FromMilliseconds(thapphan * 1000)).ContinueWith(x =>
                                    {
                                        if (dem > 0)
                                        {
                                            for (var i = 0; i <= dem; i++)
                                            {
                                                Thread.Sleep(500 * 3600 * 1000);
                                            }
                                        }
                                        if (checkauto == 0)
                                        {
                                            cancellationTokenSource.Cancel();
                                        }
                                        else
                                        {
                                            if (File.Exists(path3))
                                            {
                                                string jsonString2 = File.ReadAllText(path3);
                                                var sponsors2 = JsonConvert.DeserializeObject<AutoDeleteHistory>(jsonString2);
                                                int icheck = sponsors2.Id;
                                                if (icheck == checkauto)
                                                {
                                                    sosanh++;
                                                    using (var context1 = new DataContext())
                                                    {
                                                        var query1 = context1.ActionHistories.Where(z => z.CreateDate <= lastyear);
                                                        File.WriteAllText(path2, JsonConvert.SerializeObject(query1));
                                                        // serialize JSON directly to a file
                                                        using (StreamWriter file = File.CreateText(path2))
                                                        {
                                                            JsonSerializer serializer = new JsonSerializer();
                                                            serializer.Serialize(file, query1);
                                                        }
                                                        foreach (var item in query1)
                                                        {
                                                            context1.ActionHistories.Remove(item);
                                                        }
                                                        context1.SaveChanges();
                                                    }
                                                    this.DeleteHistory(kieuxoa, chuky, thang, thu, ngay, gio, phut, ngayxoa, sosanh, filePath, checkcontinue, checkauto);
                                                }
                                                else
                                                {
                                                    cancellationTokenSource.Cancel();
                                                }
                                            }
                                        }
                                    });
                        }
                    }
                }
                else if (kieuxoa == 0)
                {
                    ngayxoa = ngayxoa.Value.AddDays(1);
                    var query = context.ActionHistories.Where(x => x.CreateDate <= ngayxoa);
                    var path = kieuxoa.ToString() + "." + DateTime.Now.Ticks.ToString() + ".log";
                    var path2 = Path.Combine(filePath, path);
                    // serialize JSON to a string and then write string to a file

                    File.WriteAllText(path2, JsonConvert.SerializeObject(query));
                    // serialize JSON directly to a file
                    using (StreamWriter file = File.CreateText(path2))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, query);
                    }
                    foreach (var item in query)
                    {
                        context.ActionHistories.Remove(item);
                    }
                    context.SaveChanges();
                }
                else if (kieuxoa == 1)
                {
                }
            }
        }

        public IEnumerable<ActionHistory> GetAll(int page, int pageSize, out int totalRow, string filter, int loaitimkiem, DateTime? begindate = null, DateTime? enddate = null)
        {
            var query = _historyRepository.GetAll();

            if (filter != null)
            {
                filter = Common.ClearnStringToSearch.ClearnString(filter);
                if (loaitimkiem == 1)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.AppUser).Contains(filter));
                }
                else if (loaitimkiem == 2)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Action).Contains(filter));
                }
                else if (loaitimkiem == 3)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Content).Contains(filter));
                }
                else if (loaitimkiem == 4)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Function).Contains(filter));
                }
                else if (loaitimkiem == 5)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.ParentFunction).Contains(filter));
                }
                else
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.AppUser).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Content).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Function).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.ParentFunction).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Action).Contains(filter));
                }
            }
            if (begindate != null)
            {
                query = query.Where(x => x.CreateDate >= begindate);
                if (enddate != null)
                {
                    if (enddate > begindate)
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                    else if (enddate == begindate)
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                    else
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                }
            }
            else
            {
                if (enddate != null)
                {
                    query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                }
            }
            totalRow = query.Count();
            if (query.Count() > 0)
            {
                return query.OrderByDescending(x => x.IdHistory).Skip((page - 1) * pageSize).Take(pageSize);
            }
            else
            {
                return query;
            }
        }

        public ActionHistory GetById(int id)
        {
            return _historyRepository.GetSingleById(id);
        }

        public IEnumerable<ActionHistory> GetByIdUser(string appuser, int page, int pageSize, out int totalRow, string filter, int loaitimkiem, DateTime? begindate = null, DateTime? enddate = null)
        {
            var query = _historyRepository.GetAll().Where(x => x.AppUser == appuser);
            if (filter != null)
            {
                filter = Common.ClearnStringToSearch.ClearnString(filter);
                if (loaitimkiem == 1)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.AppUser).Contains(filter));
                }
                else if (loaitimkiem == 2)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Action).Contains(filter));
                }
                else if (loaitimkiem == 3)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Content).Contains(filter));
                }
                else if (loaitimkiem == 4)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.Function).Contains(filter));
                }
                else if (loaitimkiem == 5)
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.ParentFunction).Contains(filter));
                }
                else
                {
                    query = query.Where(x => Common.ClearnStringToSearch.ClearnString(x.AppUser).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Content).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Function).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.ParentFunction).Contains(filter) || Common.ClearnStringToSearch.ClearnString(x.Action).Contains(filter));
                }
            }
            if (begindate != null)
            {
                query = query.Where(x => x.CreateDate >= begindate);
                if (enddate != null)
                {
                    if (enddate > begindate)
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                    else if (enddate == begindate)
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                    else
                    {
                        query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                    }
                }
            }
            else
            {
                if (enddate != null)
                {
                    query = query.Where(x => x.CreateDate < enddate.Value.AddDays(1));
                }
            }
            totalRow = query.Count();
            if (query.Count() > 0)
            {
                return query.OrderByDescending(x => x.IdHistory).Skip((page - 1) * pageSize).Take(pageSize);
            }
            else
            {
                return query;
            }
        }

        public DateTime GetNgayGanNhat(DateTime date, int loai, int so, DateTime datenow)
        {
            try
            {
                if (date >= datenow)
                {
                    return date;
                }
                else
                {
                    var d = datenow - date;
                    if (loai == 0)
                    {
                        var x = d.TotalDays / so;
                        int y = (int)x;
                        date = date.AddDays(so * y - so);
                    }
                    if (loai == 1)
                    {
                        var x = d.TotalDays / 31 / so;
                        int y = (int)x;
                        date = date.AddMonths(so * y - so);
                    }
                    if (loai == 2)
                    {
                        var x = d.TotalDays / 365 / so;
                        int y = (int)x;
                        date = date.AddYears(so * y - so);
                    }
                    return date;
                }
            }
            catch
            {
                return date;
            }
        }

        public List<string> Recheck(string item, string item2, string parenturl)
        {
            List<string> Lst = new List<string>();
            string functionId = "";
            try
            {
                if (item.IndexOf("/api/function/") != -1)
                {
                    if (item == "/api/function/add" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", Name : ") + 9);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                    }
                    if (item == "/api/function/update" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", Name : ") + 9);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                    }
                    item = "Chức năng";
                    parenturl = "/main/hethong/function";
                }
                else if (item.IndexOf("/api/chutruongdautu/") != -1)
                {
                    item = "Chủ trương đầu tư";
                    parenturl = "/main/quanlyduan/quanlydautu/chutruongdautu";
                }
                else if (item.IndexOf("/api/phuluchopdong/") != -1)
                {
                    item = "Phụ lục hợp đồng";
                }
                else if (item.IndexOf("/api/thanhtoanchiphikhac/") != -1)
                {
                    item = "Thanh toán chi phí khác";
                    parenturl = "/main/quanlyduan/taichinh/thanhtoanchiphikhac";
                }
                else if (item.IndexOf("/api/lapquanlydautu/") != -1)
                {
                    item = "Lập quản lý đầu tư";
                    parenturl = "/main/quanlyduan/quanlydautu/lapduandautu";
                }
                else if (item.IndexOf("/api/loaicapcongtrinh/") != -1)
                {
                    if (item == "/api/loaicapcongtrinh/add" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", TenLoaiCapCongTrinh : ") + 24);
                        item2 = item2.Substring(0, item2.IndexOf(" }"));
                    }
                    if (item == "/api/loaicapcongtrinh/update" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", TenLoaiCapCongTrinh : ") + 9);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                    }
                    item = "Loại cấp công trình";
                    parenturl = "/main/dungchung/loaicapcongtrinh";
                }
                else if (item.IndexOf("/api/hinhthucdauthau/") != -1)
                {
                    item = "Hình thức đấu thầu";
                    parenturl = "/main/dungchung/hinhthucdauthau";
                }
                else if (item.IndexOf("/api/thanhtoanhopdong/") != -1)
                {
                    item = "Thanh toán hợp đồng";
                    parenturl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                }
                else if (item.IndexOf("/api/tiendochitiet/") != -1)
                {
                    item = "Tiến độ chi tiết";
                    parenturl = "/main/quanlyduan/quanlyxaydung/tiendothuchienhopdong";
                }
                else if (item.IndexOf("/api/nhomduan/") != -1)
                {
                    if (item == "/api/nhomduan/add" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", TenNhomDuAn : ") + 16);
                        var index = item2.IndexOf(" ,");
                        if (index > -1)
                        {
                            item2 = item2.Substring(0, index);
                        }
                        else
                        {
                            item2 = item2.Substring(0, item2.IndexOf(" }"));
                        }
                    }
                    if (item == "/api/nhomduan/update" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", TenNhomDuAn : ") + 16);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                    }
                    item = "Nhóm dự án";
                    parenturl = "/main/dungchung/nhomduan";
                    functionId = "DMDC";
                }
                else if (item.IndexOf("/api/chudautu/") != -1)
                {
                    item = "Chủ đầu tư";
                    parenturl = "/main/dungchung/chudautu";
                }
                else if (item.IndexOf("/api/appUser/") != -1)
                {
                    item = "User";
                    parenturl = "/main/hethong/user";
                }
                else if (item.IndexOf("/api/quyettoanduan/") != -1)
                {
                    item = "Quyết toán dự án";
                    parenturl = "/main/quanlyduan/taichinh/quyettoanduan";
                }
                else if (item.IndexOf("/api/hopdong/") != -1)
                {
                    item = "Hợp đồng";
                    parenturl = "/main/quanlyduan/quanlydauthau/thongtinhopdong";
                }
                else if (item.IndexOf("/api/loaihopdong/") != -1)
                {
                    item = "Loại hợp đồng";
                    parenturl = "/main/dungchung/loaihopdong";
                }
                else if (item.IndexOf("/api/hinhthucquanly/") != -1)
                {
                    item = "Hình thức quản lý dự án";
                    parenturl = "/main/dungchung/hinhthucquanly";
                }
                else if (item.IndexOf("/api/tamunghopdong/") != -1)
                {
                    item = "Tạm ứng hợp đồng";
                    parenturl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                }
                else if (item.IndexOf("/api/thuchienhopdong/") != -1)
                {
                    item = "Thực hiện hợp đồng";
                    parenturl = "/main/quanlyduan/quanlyxaydung/thuchienhopdong";
                }
                else if (item.IndexOf("/api/quyettoanhopdong/") != -1)
                {
                    item = "Quyết toán hợp đồng";
                    parenturl = "/main/quanlyduan/taichinh/thanhtoanhopdong";
                }
                else if (item.IndexOf("/api/linhvucnganhnghe/") != -1)
                {
                    item = "Lĩnh vực ngành nghề";
                    parenturl = "/main/dungchung/linhvucnganhnghe";
                }
                else if (item.IndexOf("/api/ketquadauthau/") != -1)
                {
                    item = "Kết quả đấu thầu";
                    parenturl = "/main/quanlyduan/quanlydauthau/ketquadauthau";
                }
                else if (item.IndexOf("/api/kehoachluachonnhathau/") != -1)
                {
                    item = "Kế hoạch lựa chọn nhà thầu";
                    parenturl = "/main/quanlyduan/quanlydauthau/kehoachluachonnhathau";
                }
                else if (item.IndexOf("/api/duan/") != -1)
                {
                    item = "Dự án";
                    parenturl = "/main/quanlyduan/duan";
                }
                else if (item.IndexOf("/api/tm/") != -1)
                {
                    item = "Thư mục";
                }
                else if (item.IndexOf("/api/coquanpheduyet/") != -1)
                {
                    item = "Cơ quan phê duyệt";
                    parenturl = "/main/dungchung/coquanpheduyet";
                }
                else if (item.IndexOf("/api/phuongthucdauthau/") != -1)
                {
                    item = "Phương thức đấu thầu";
                }
                else if (item.IndexOf("/api/thamdinhlapquanlydautu/") != -1)
                {
                    item = "Thẩm định lập quản lý đầu tư";
                }
                else if (item.IndexOf("/api/tinhtrangduan/") != -1)
                {
                    item = "Tình trạng dự án";
                }
                else if (item.IndexOf("/api/nguonvon/") != -1)
                {
                    item = "Nguồn vốn";
                }
                else if (item.IndexOf("/api/hinhthucdautu/") != -1)
                {
                    item = "Hình thức đầu tư";
                }
                else if (item.IndexOf("/api/dieuhanh/") != -1)
                {
                    item = "Điều hành";
                }
                else if (item.IndexOf("/api/linhvucdauthau/") != -1)
                {
                    item = "Lĩnh vực đấu thầu";
                }
                else if (item.IndexOf("/api/tiendothuchienhopdong/") != -1)
                {
                    item = "Tiến độ thực hiện hợp đồng";
                }
                else if (item.IndexOf("/api/danhmucnhathau/") != -1)
                {
                    item = "Danh mục nhà thầu";
                }
                else if (item.IndexOf("/api/upload/") != -1)
                {
                    item = "Upload";
                }
                else if (item.IndexOf("/api/goithau/") != -1)
                {
                    item = "Gói thầu";
                }
                else if (item.IndexOf("/api/kehoachvon/") != -1)
                {
                    string strkhv = item2;
                    int idduan = 0;
                    if (item == "/api/kehoachvon/add" && item2.Length > 0)
                    {
                        strkhv = strkhv.Replace('"', ' ');
                        strkhv = strkhv.Substring(strkhv.IndexOf(", IdDuAn :") + 10);
                        strkhv = strkhv.Substring(0, strkhv.IndexOf(","));
                        idduan = Int32.Parse(strkhv);
                    }
                    var tenduan = _duAnService.GetById(idduan).TenDuAn;
                    if (item == "/api/kehoachvon/add" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", SoQuyetDinh : ") + 17);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                        item2 = tenduan + "/" + item2;
                    }
                    if (item == "/api/kehoachvon/update" && item2.Length > 0)
                    {
                        item2 = item2.Replace('"', ' ');
                        item2 = item2.Substring(item2.IndexOf(", SoQuyetDinh : ") + 17);
                        item2 = item2.Substring(0, item2.IndexOf(" ,"));
                        item2 = tenduan + "/" + item2;
                    }
                    item = "Kế hoạch vốn";
                    parenturl = "/main/quanlyduan/kehoachvon";
                }
                else if (item.IndexOf("/api/hinhthucluachonnhathau/") != -1)
                {
                    item = "Hình thức lựa chọn nhà thầu";
                }
                else if (item.IndexOf("/api/vbda/") != -1)
                {
                    item = "Văn bản dự án";
                }
                else if (item.IndexOf("/api/nguonvonduan/") != -1)
                {
                    item = "Nguồn vốn dự án";
                }
                else if (item.IndexOf("/api/hosomoithau/") != -1)
                {
                    item = "Hồ sơ mời thầu";
                }
                else if (item.IndexOf("/api/nguonvonduangoithau/") != -1)
                {
                    item = "Nguồn vốn dự án gói thầu";
                }
                else if (item.IndexOf("/api/appRole/") != -1)
                {
                    item = "Nhóm người dùng";
                }
                else if (item.IndexOf("/api/tmmd/") != -1)
                {
                    item = "Thư mục mặc định";
                }
                else if (item.IndexOf("/api/giaidoanduan/") != -1)
                {
                    item = "Giai đoạn dự án";
                }
                else if (item.IndexOf("/api/tiendothuchien/") != -1)
                {
                    item = "Tiến độ thực hiện";
                }
                else if (item.IndexOf("/api/tiendodauthau/") != -1)
                {
                    item = "Tiến độ đấu thầu";
                }
                else if (item.IndexOf("/api/NhomDuAnTheoUser/") != -1)
                {
                    item = "Nhóm dự án theo user";
                }
                else if (item.IndexOf("/api/kehoachvoncacduan/") != -1)
                {
                    item = "Kế hoạch vốn các dự án";
                }
                else
                {

                }
                Lst.Add(item);
                Lst.Add(item2);
                Lst.Add(parenturl);
                Lst.Add(functionId);
                return Lst;
            }
            catch
            {
                Lst.Add(item);
                Lst.Add(item2);
                Lst.Add(parenturl);
                Lst.Add(functionId);
                return Lst;
            }
        }

        public string RemoveSpace(string str)
        {
            if (str.Length > 0 && str.IndexOf(',') != -1)
            {
                str = str.Substring(0, str.IndexOf(','));
            }
            if (str.Length > 0 && str.IndexOf(" ") != -1)
            {
                str = str.Replace(" ", "");
                if (str.Length > 0 && str.IndexOf(' ') != -1)
                {
                    this.RemoveSpace(str);
                }
            }
            if (str.Length > 0 && str.IndexOf(',') != -1)
            {
                str = str.Replace(",", "");
                if (str.Length > 0 && str.IndexOf(",") != -1)
                {
                    this.RemoveSpace(str);
                }
            }
            return str;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public List<string> ToList(string str)
        {
            List<string> list = new List<string>();
            string[] lst = str.Split(',');
            foreach (var item in lst)
            {
                list.Add(item);
            }
            return list;
        }

        public void Update(ActionHistory history)
        {
            _historyRepository.Update(history);
        }
    }
}