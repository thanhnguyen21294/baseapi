﻿using Common;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IPhieuXuLyCongViecNoiBoService
    {
        PhieuXuLyCongViecNoiBo Add(PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBo);

        void Update(PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBo);

        PhieuXuLyCongViecNoiBo Delete(int id);

        IEnumerable<PhieuXuLyCongViecNoiBo> GetAll();
        IEnumerable<PhieuXuLyCongViecNoiBo> GetPhieuCha(string userID);

        IEnumerable<PhieuXuLyCongViecNoiBo> GetByFilter(int idDa, int page, int pageSize, string userID, List<string> userIdNhanVien, out int totalRow, string filter = null);

        IEnumerable<PhieuForMobiles> GetAllForMobile(int page, int pageSize, string userID, out int totalRow, string filter = null);
        int GetCountForMobile(string userID);
        IEnumerable<PhieuXuLyCongViecNoiBo> GetForExport(int idDa, string userID, List<string> userIdNhanVien, string filter = null);

        PhieuXuLyCongViecNoiBo GetById(int id);

        object GetByIdForMobile(int id);
        void Save();
    }

    public class PhieuXuLyCongViecNoiBoService : IPhieuXuLyCongViecNoiBoService
    {
        private IPhieuXuLyCongViecNoiBoRepository _PhieuXuLyCongViecNoiBoRepository;
        private IAppUserRepository _AppUserRepository;
        private IUnitOfWork _unitOfWork;

        public PhieuXuLyCongViecNoiBoService(IPhieuXuLyCongViecNoiBoRepository PhieuXuLyCongViecNoiBoRepository, IAppUserRepository AppUserRepository, IUnitOfWork unitOfWork)
        {
            _PhieuXuLyCongViecNoiBoRepository = PhieuXuLyCongViecNoiBoRepository;
            _AppUserRepository = AppUserRepository;
            _unitOfWork = unitOfWork;
        }
        public PhieuXuLyCongViecNoiBo Add(PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBo)
        {
            return _PhieuXuLyCongViecNoiBoRepository.Add(PhieuXuLyCongViecNoiBo);
        }

        public PhieuXuLyCongViecNoiBo Delete(int id)
        {
            return _PhieuXuLyCongViecNoiBoRepository.Delete(id);
        }

        public IEnumerable<PhieuXuLyCongViecNoiBo> GetAll()
        {
            return _PhieuXuLyCongViecNoiBoRepository.GetAll();
        }

        public IEnumerable<PhieuXuLyCongViecNoiBo> GetPhieuCha(string userID)
        {
            return _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.IDPhieuCha == null && x.PhieuXuLyCongViecNoiBoUsers.Select(y => y.UserId).Contains(userID));
        }
        public IEnumerable<PhieuXuLyCongViecNoiBo> GetByFilter(int idDA, int page, int pageSize, string userID, List<string> userIdNhanVien, out int totalRow, string filter = null)
        {
            var queryAllForUser = _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.IdDuAn == idDA && x.PhieuXuLyCongViecNoiBoUsers.Select(y => y.UserId).Contains(userID), new string[] { "PhieuXuLyCongViecNoiBoUsers" }).ToList();
            var daDuyet = queryAllForUser.Where(x => x.TrangThai == 1).ToList();
            var choDuyet = new List<PhieuXuLyCongViecNoiBo>();
            if (userIdNhanVien.Count() > 0)
            {
                choDuyet = queryAllForUser.Where(x => x.TrangThai == 0 && userIdNhanVien.Contains(x.CreatedBy)).ToList();
            }
            else
            {
                choDuyet = queryAllForUser.Where(x => x.TrangThai == 0 && x.CreatedBy == userID).ToList();
            }
            var query = choDuyet.Union(daDuyet);
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdPhieuXuLyCongViecNoiBo).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<PhieuForMobiles> GetAllForMobile(int page, int pageSize, string userID, out int totalRow, string filter = null)
        {
            var queryAll = _PhieuXuLyCongViecNoiBoRepository.GetListTree().Where(x => x.TrangThai == 0 && x.PhieuXuLyCongViecNoiBoUsers.Select(y => y.UserId).Contains(userID)).ToList();
            var query = queryAll.GroupBy(x => new { x.IdDuAn, x.DuAn.TenDuAn }).Select(x => new PhieuForMobiles
            {
                IdDuAn = x.Key.IdDuAn,
                TenDuAn = x.Key.TenDuAn,
                Phieus = x.Select(y => new PhieuMinis
                {
                    IdPhieuXuLyCongViecNoiBo = y.IdPhieuXuLyCongViecNoiBo,
                    TenCongViec = y.TenCongViec,
                    NoiDung = y.NoiDung,
                    NgayChuyen = y.NgayChuyen,
                    NgayNhan = y.NgayNhan,
                    Childrends = y.Childrens.Select(z => new PhieuMinis
                    {
                        IdPhieuXuLyCongViecNoiBo = z.IdPhieuXuLyCongViecNoiBo,
                        TenCongViec = z.TenCongViec,
                        NoiDung = z.NoiDung,
                        NgayChuyen = z.NgayChuyen,
                        NgayNhan = z.NgayNhan
                    })
                }).OrderBy(y => y.IdPhieuXuLyCongViecNoiBo)
            });
            totalRow = query.Count();
            return query.Skip((page - 1) * pageSize).Take(pageSize);
        }
        public int GetCountForMobile(string userID)
        {
           var reData =  _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.TrangThai == 0 && x.PhieuXuLyCongViecNoiBoUsers.Select(y => y.UserId).Contains(userID), new string[] { "PhieuXuLyCongViecNoiBoUsers" }).Count();
            return reData;
        }
        public IEnumerable<PhieuXuLyCongViecNoiBo> GetForExport(int idDA, string userID, List<string> userIdNhanVien, string filter = null)
        {
            var queryAllForUser = _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.IdDuAn == idDA && x.PhieuXuLyCongViecNoiBoUsers.Select(y => y.UserId).Contains(userID), new string[] { "PhieuXuLyCongViecNoiBoUsers" }).ToList();
            var daDuyet = queryAllForUser.Where(x => x.TrangThai == 1).ToList();
            var choDuyet = new List<PhieuXuLyCongViecNoiBo>();
            if(userIdNhanVien.Count() > 0)
            {
                choDuyet = queryAllForUser.Where(x => x.TrangThai == 0 && userIdNhanVien.Contains(x.CreatedBy)).ToList();
            }
            else
            {
                choDuyet = queryAllForUser.Where(x => x.TrangThai == 0 && x.CreatedBy == userID).ToList();
            }
            var query = choDuyet.Union(daDuyet);
            if(filter != null)
            {
                query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()));
            }
            return query.OrderBy(x => x.IdPhieuXuLyCongViecNoiBo);
        }

        public PhieuXuLyCongViecNoiBo GetById(int id)
        {
            return _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.IdPhieuXuLyCongViecNoiBo == id, new string[] { "DuAn", "PhieuXuLyCongViecNoiBoUsers" }).SingleOrDefault();
        }

        public object GetByIdForMobile(int id)
        {
            var users = _AppUserRepository.GetAllUser();
            var model = _PhieuXuLyCongViecNoiBoRepository.GetMulti(x => x.IdPhieuXuLyCongViecNoiBo == id, new string[] { "DuAn", "PhieuXuLyCongViecNoiBoUsers" }).SingleOrDefault();
            return new
            {
                model.IdPhieuXuLyCongViecNoiBo,
                model.TenCongViec,
                model.NoiDung,
                model.NgayNhan,
                model.NgayChuyen,
                model.DuAn.TenDuAn,
                model.TrangThai,
                NguoiTao = users.SingleOrDefault(x => x.Id == model.CreatedBy).FullName
            };
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBo)
        {
            _PhieuXuLyCongViecNoiBoRepository.Update(PhieuXuLyCongViecNoiBo);
        }
    }
}
