﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ILienKetLinhVucService
    {
        LienKetLinhVuc Add(LienKetLinhVuc LienKetLinhVuc);

        void Update(LienKetLinhVuc LienKetLinhVuc);

        LienKetLinhVuc Delete(int id);

        IEnumerable<LienKetLinhVuc> GetAll();

        IEnumerable<LienKetLinhVuc> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        LienKetLinhVuc GetById(int id);

        void Save();
    }

    public class LienKetLinhVucService : ILienKetLinhVucService
    {
        private ILienKetLinhVucRepository _LienKetLinhVucRepository;
        private IUnitOfWork _unitOfWork;

        public LienKetLinhVucService(ILienKetLinhVucRepository LienKetLinhVucRepository, IUnitOfWork unitOfWork)
        {
            this._LienKetLinhVucRepository = LienKetLinhVucRepository;
            this._unitOfWork = unitOfWork;
        }
        public LienKetLinhVuc Add(LienKetLinhVuc LienKetLinhVuc)
        {
            return _LienKetLinhVucRepository.Add(LienKetLinhVuc);
        }

        public LienKetLinhVuc Delete(int id)
        {
            return _LienKetLinhVucRepository.Delete(id);
        }

        public IEnumerable<LienKetLinhVuc> GetAll()
        {
            return _LienKetLinhVucRepository.GetAll();
        }

        public IEnumerable<LienKetLinhVuc> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _LienKetLinhVucRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenLienKetLinhVuc.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLienKetLinhVuc).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LienKetLinhVuc GetById(int id)
        {
            return _LienKetLinhVucRepository.GetMulti(x => x.IdLienKetLinhVuc == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LienKetLinhVuc LienKetLinhVuc)
        {
            _LienKetLinhVucRepository.Update(LienKetLinhVuc);
        }
    }
}
