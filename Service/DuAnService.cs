﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System.Linq.Expressions;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using Microsoft.Office.Interop.Excel;
using System.Web.Script.Serialization;

namespace Service
{
    public interface IDuAnService
    {
        DuAn Add(DuAn duAn);

        void Update(DuAn duAn);
        IEnumerable<DuAn> GetTMDTForBaoCao(List<int> lstIDDuAn);
        DuAn Delete(int id);
        IEnumerable<DuAn> FilterByNameDuAn(string filter, List<DuAn> duans);
        IEnumerable<DuAn> FilterByNameDuAn(string filter, int nam);
        IEnumerable<DuAn> GetAll();
        IEnumerable<DuAn> GetForLocDuAn();
        //IEnumerable<DuAn> GetForLocDuAnNew();
        IEnumerable<DuAn> GetAllKHV();
        IEnumerable<DuAn> GetAllTienDoDauThau();
        List<DuAnSearch> GetListDuAns(int? idNhomDuAnTheoUser, string idUser);
        IEnumerable<DuAn> GetByFilter(int? idNhomDuAnTheoUser, int page, int pageSize, out int totalRow, string filter);
        List<IEnumerable<DuAnSearch>> GetByFilter(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, int IdGiaiDoan);
        //List<IEnumerable<DuAnSearch>> GetByNewFilter(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, int? idChuDauTu, int? idNhomDuAn, int? idTinhTrangDuAn,
        //    int? idLinhVuc, int? idQuanLy, int? idNguonVon, int? idPhongBan, string filter, string diadiem, string thoigianthuchien, int IdGiaiDoan);
        List<IEnumerable<DuAnSearch>> GetByNewFilter(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, string diadiem, string thoigianthuchien, string strIdChuDauTu, string strIdNhomDuAn, string strIdTinhTrangDuAn, string strIdLinhVucNganhNghe, string strIdNguonVon, string strIdGiaiDoan, string strIdQuanLy, string strIdPhongBan, string tenDA, int IdGiaiDoan);
        List<IEnumerable<DuAnSearch>> GetPhanQuyen(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter,string diadiem, int? IdGiaiDoan, string uid, int pid, int lvid, Dictionary<string, string> dicUser);
        List<IEnumerable<DuAnSearch>> GetPhanQuyenNew(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, string diadiem, int? IdGiaiDoan, string uid, int pid, int lvid, Dictionary<string, string> dicUser, string username);
        IEnumerable<DuAn> DuAnByYearName(string year, string filter, int? IdNhomDuAnTheoUser);
        IEnumerable<DuAn> DuAnNguonVonByYearName(int year, string filter);
        IEnumerable<DuAn> GetAllTienDoThucHienHopDong(int? IdNhomDuAnTheoUser);
        IEnumerable<DuAn> GetAllDuAnCham(int? IdNhomDuAnTheoUser);

        IEnumerable<DuAn> DuAnNguonVonKHV(int? IdNhomDuAnTheoUser);

        IEnumerable<DuAn> Filter(Expression<Func<DuAn, bool>> predicate, string[] includes = null);
        IEnumerable<DuAn> GetAllNguonVonDuAn();
        IEnumerable<DuAn> GetAllQuanLy();
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanHoanThanh(int donvibaocao, string loaibaocao, int year);
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanNhanHoSo(int donvibaocao, string loaibaocao, int year);
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanChuaNhanHoSo(int donvibaocao, string loaibaocao, int year);
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanHoanThanhByChuDauTu(int donvibaocao, string loaibaocao, int year);
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanNhanHoSoByChuDauTu(int donvibaocao, string loaibaocao, int year);
        IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanChuaNhanHoSoByChuDauTu(int donvibaocao, string loaibaocao, int year);
        void GetChildByIdDuAn(int id, List<DuAn> duancontheoids, List<DuAn> duancontheoids2);

        DuAn GetById(int id);
        DuAn GetByIdForPhieu(int id);
        DuAn GetByIdForBaoCao(int id);
        IEnumerable<DuAnSearch> FilterForTheoDoiHoSoQT(string filter, string idUser);
        void Save();
        IEnumerable<DuAnSearch> GetListChonDuAns(int? idNhomDuAnTheoUser, string idUser);
        IEnumerable<BC_LocDuAn_PhanQuyen_PhongCT> GetList_LocDuAn_For_BaoCao(IEnumerable<int> idsIDDA, Dictionary<string, string> lstUserNhanVien);
        IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(string IDDuAns, Dictionary<string, string> lstUserNhanVien);
        //SignalR DuAn

        //DuAn Create(DuAn announcement);

        //List<DuAn> GetListByUserId(string userId, int pageIndex, int pageSize, out int totalRow);

        //List<DuAn> GetListByUserId(string userId, int top);

        //void DeleteDASignalR(int notificationId);

        //void MarkAsRead(string userId, int notificationId);

        //DuAn GetDetail(int id);

        //List<DuAn> GetListAll(int pageIndex, int pageSize, out int totalRow);

        //List<DuAn> ListAllUnread(string userId, int pageIndex, int pageSize, out int totalRow);

    }



    public class DuAnService : IDuAnService
    {
        private IDuAnRepository _duAnRepository;
        private IUnitOfWork _unitOfWork;
        private IQuyetToanDuAnService _quyetToanDuAnService;
        private IDuAnUserRepository _duAnUserRepository;
        private INhomDuAnRepository _nhomDuAnRepository;
        private IDanhMucNhaThauService _danhMucNhaThauService;

        //private IChuDauTuRepository _chuDauTuRepository;
        //private ITinhTrangDuAnRepository _tinhTrangDuAnRepository;
        //private ILinhVucNganhNgheRepository _linhVucNganhNgheRepository;
        //private IGiaiDoanDuAnRepository _giaiDoanDuAnRepository;
        //private IDuAnQuanLyRepository _duAnQuanLyRepository;


        public DuAnService(IQuyetToanDuAnService quyetToanDuAnService, IDuAnRepository duAnRepository, IDuAnUserRepository duAnUserRepository, INhomDuAnRepository nhomDuAnRepository, IDanhMucNhaThauService danhMucNhaThauService, IUnitOfWork unitOfWork)
        {
            _duAnRepository = duAnRepository;
            _duAnUserRepository = duAnUserRepository;
            _unitOfWork = unitOfWork;
            _quyetToanDuAnService = quyetToanDuAnService;
            _nhomDuAnRepository = nhomDuAnRepository;
            _danhMucNhaThauService = danhMucNhaThauService;
            //_chuDauTuRepository = chuDauTuRepository;
            //_tinhTrangDuAnRepository = tinhTrangDuAnRepository;
            //_linhVucNganhNgheRepository = linhVucNganhNgheRepository;
            //_giaiDoanDuAnRepository = giaiDoanDuAnRepository;
            //_duAnQuanLyRepository = duAnQuanLyRepository;
        }

        public DuAn Add(DuAn duAn)
        {
            return _duAnRepository.Add(duAn);
        }

        public DuAn Delete(int id)
        {
            return _duAnRepository.Delete(id);
        }

        public IEnumerable<DuAn> GetForLocDuAn()
        {
            return _duAnRepository.GetAll(new[] { "NhomDuAn", "GiaiDoanDuAn", "DuAnUsers" , "NguonVonDuAns" });
        }
         
        public IEnumerable<DuAn> GetAll()
        {
            return _duAnRepository.GetAll(new[] { "NhomDuAn", "GiaiDoanDuAn", "DuAnUsers" });
        }
        
        public IEnumerable<DuAn> GetTMDTForBaoCao(List<int> lstIDDuAn)
        {
            return _duAnRepository.GetMulti(x => lstIDDuAn.Contains(x.IdDuAn) == false);
        }
        public List<DuAnSearch> GetListDuAns(int? idNhomDuAnTheoUser, string idUser)
        {
            List<DuAnSearch> lstReturn = new List<DuAnSearch>();
            if (idNhomDuAnTheoUser != null)
            {
                var query = _duAnRepository.GetAll(new[] { "DuAnUsers" });
                if (idUser != "")
                {
                    lstReturn = query.Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).Select(x => new DuAnSearch
                    {
                        IdDuAn = x.IdDuAn,
                        TenDuAn = x.TenDuAn,
                        IdDuAnCha = x.IdDuAnCha
                    }).ToList();
                }
            }
            else
            {
                var query = _duAnRepository.GetAll(new[] { "DuAnUsers" });
                if (idUser != "")
                {
                    lstReturn = query.Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).Select(x => new DuAnSearch
                    {
                        IdDuAn = x.IdDuAn,
                        TenDuAn = x.TenDuAn,
                        IdDuAnCha = x.IdDuAnCha
                    }).ToList(); ;
                }
                
            }
            return lstReturn;
        }

        public IEnumerable<DuAnSearch> GetListChonDuAns(int? idNhomDuAnTheoUser, string idUser)
        {
            IEnumerable<DuAnSearch> lstReturn = new List<DuAnSearch>();
            //if (idNhomDuAnTheoUser != null)
            //{
                if (idUser != "")
                {
                    lstReturn = _duAnRepository.GetMulti(x => x.DuAnUsers.Select(y => y.UserId).Contains(idUser), new[] { "DuAnUsers", "GiaiDoanDuAn" }).Select(x => new DuAnSearch
                    {
                        IdDuAn = x.IdDuAn,
                        TenDuAn = x.TenDuAn,
                        IdDuAnCha = x.IdDuAnCha,
                        TenGiaiDoanDuAn = x.IdGiaiDoanDuAn != null ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : ""
                    });
                }
            //}
            //else
            //{
            //    //var query = _duAnRepository.GetAll(new[] { "DuAnUsers" });
            //    if (idUser != "")
            //    {
            //        lstReturn = query.Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0).Select(x => new DuAnSearch
            //        {
            //            IdDuAn = x.IdDuAn,
            //            TenDuAn = x.TenDuAn,
            //            IdDuAnCha = x.IdDuAnCha
            //        }).ToList(); ;
            //    }

            //}
            return lstReturn;
        }
        public IEnumerable<DuAnSearch> FilterForTheoDoiHoSoQT(string filter, string idUser)
        {
            IEnumerable<DuAnSearch> lstReturn = new List<DuAnSearch>();
            if (filter != null)
            {
                if (idUser != "")
                {
                    lstReturn = _duAnRepository.GetMulti(x => x.DuAnUsers.Select(y => y.UserId).Contains(idUser) 
                    && x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.ToLower().Contains(filter.ToLower()), new[] { "DuAnUsers" }).Select(x => new DuAnSearch
                    {
                        IdDuAn = x.IdDuAn,
                        TenDuAn = x.TenDuAn,
                    });
                }
            }
            else
            {
                if (idUser != "")
                {
                    lstReturn = _duAnRepository.GetMulti(x => x.DuAnUsers.Select(y => y.UserId).Contains(idUser), new[] { "DuAnUsers" }).Select(x => new DuAnSearch
                    {
                        IdDuAn = x.IdDuAn,
                        TenDuAn = x.TenDuAn,
                    });
                }
            }
            return lstReturn;
        }
        public IEnumerable<DuAn> FilterByNameDuAn(string filter, List<DuAn> duans)
        {
            if (filter != null)
            {
                List<DuAn> search = new List<DuAn>();
                var listSearch = duans.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.ToLower().Contains(filter.ToLower()));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParents(duans.ToList(), duAn.IdDuAnCha)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDuAnCha == null);

                var resual = parents.OrderBy(x => x.IdDuAn);

                foreach (var duAn in resual)
                {
                    //parents = parents.Union(GetChild(search, duAn));
                }
                return parents;

            }
            else
                return duans;

        }
        public IEnumerable<DuAn> FilterByNameDuAn(string filter, int nam)
        {
            IEnumerable<DuAn> query;
            if (filter != null)
            {
                query = _duAnRepository.GetMulti(x => (x.KeHoachVons.Where(v => v.NienDo == nam).Count() > 0) 
                && (x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.ToLower().Contains(filter.ToLower())), 
                new string[]{ "NhomDuAn", "GiaiDoanDuAn", "ChuDauTu", "GoiThaus", "KeHoachVons", "GoiThaus.HopDongs.DanhMucNhaThau" });
            }
            else
            {
                query = _duAnRepository.GetMulti(x =>(x.KeHoachVons.Where(v => v.NienDo == nam).Count() > 0), 
                new string[] { "NhomDuAn", "GiaiDoanDuAn", "ChuDauTu", "GoiThaus", "KeHoachVons", "GoiThaus.HopDongs.DanhMucNhaThau" });
            }
            return query;

        }
        public IEnumerable<DuAn> GetByFilter(int? idNhomDuAnTheoUser, int page, int pageSize, out int totalRow, string filter)
        {

            //Cần phải viết lại thuật toán tìm kiếm và phân trang.
            IEnumerable<DuAn> query;
            List<DuAn> search = new List<DuAn>();
            if (idNhomDuAnTheoUser == null)
            {
                query = _duAnRepository.GetAll(new[] { "NhomDuAn", "GiaiDoanDuAn" });
            }
            else
            {
                query = _duAnRepository.GetMulti(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser,
                    new[] { "NhomDuAn", "GiaiDoanDuAn" });
            }

            if (filter != null)
            {
                var listSearch = query.Where(x => x.TenDuAn.Contains(filter) || x.Ma.Contains(filter));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParents(query.ToList(), duAn.IdDuAnCha)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                return parents;
            }
            var parent = query.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = query.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = query.Count(x => x.IdDuAnCha == null);
            return parent;
        }      

        public List<IEnumerable<DuAnSearch>> GetByFilter(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, int IdGiaiDoan)
        {
            List<IEnumerable<DuAnSearch>> reLst = new List<IEnumerable<DuAnSearch>>();
            IEnumerable<DuAn> lstDuAn;
            List<DuAnSearch> search = new List<DuAnSearch>();
            var lstDuAnUser = _duAnUserRepository.GetMulti(x => x.UserId == idUser, new string[] { "AppUsers" }).Select(x => x.IdDuAn).Distinct().ToList();
            if (idNhomDuAnTheoUser == null)
            {
                lstDuAn = _duAnRepository.GetMulti(x => lstDuAnUser.Contains(x.IdDuAn), new string[] { "NhomDuAn", "GiaiDoanDuAn" });
            }
            else
            {
                lstDuAn = _duAnRepository.GetMulti(x => lstDuAnUser.Contains(x.IdDuAn) && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser, new string[] { "NhomDuAn", "GiaiDoanDuAn" }); 
            }

            var qLst = lstDuAn.Select(x => new DuAnSearch
            {
                IdDuAn = x.IdDuAn,
                TenDuAn = x.TenDuAn,
                TenNhomDuAn = x.NhomDuAn != null ? x.NhomDuAn.TenNhomDuAn : "",
                IdGiaiDoanDuAn = x.IdGiaiDoanDuAn,
                TenGiaiDoanDuAn = x.GiaiDoanDuAn != null ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
                Ma = x.Ma,
                ThoiGianThucHien = x.ThoiGianThucHien,
                IdDuAnCha = x.IdDuAnCha
            }).ToList();
            if(IdGiaiDoan != -1)
            {
                qLst = qLst.Where(x => x.IdGiaiDoanDuAn == IdGiaiDoan).ToList();
            }
           
            if (filter != null)
            {
                var listSearch = qLst.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.Contains(filter));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                //Lấy 20 bản ghi cha đầu tiên sau khi sắp xếp các cha
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    //search: danh sách tất cả trong dự án
                    //duAn: danh sách các cha trong dự án
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                return reLst;
            }
            var parent = qLst.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = qLst.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = qLst.Count(x => x.IdDuAnCha == null);
            foreach (var item in parent)
            {
                parent = parent.Union(GetChild(qLst, item));
            }
            reLst.Add(parent);
            reLst.Add(qLst);
            return reLst;
        }        

        public List<IEnumerable<DuAnSearch>> GetPhanQuyenNew(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, string diadiem, int? IdGiaiDoan, string uid, int pid, int lvid, Dictionary<string, string> dicUser, string username)
        {
            List<IEnumerable<DuAnSearch>> reLst = new List<IEnumerable<DuAnSearch>>();
            List<DuAnSearch> search = new List<DuAnSearch>();
            var lstDuAn = _duAnRepository.GetAll(new string[] { "NhomDuAn", "GiaiDoanDuAn" }).ToList();
            var lstDuAnUser = _duAnUserRepository.GetAll(new string[] { "AppUsers" }).ToList();

            var dDuAnUser = lstDuAnUser.GroupBy(x => x.IdDuAn).Select(x => new
            {
                x.Key,
                IdUsers = string.Join("; ", x.Select(y => y.UserId).Distinct())
            }).ToDictionary(x => x.Key, x => x.IdUsers);

            var dDuAnUserName = lstDuAnUser.GroupBy(x => x.IdDuAn).Select(x => new
            {
                x.Key,
                UserNames = string.Join("; ", x.Select(y => y.AppUsers.FullName).Distinct()) // (x.Select(y => dicUser.Keys.Where(h => h == y.UserId).FirstOrDefault())).Select(z => z != null ? dicUser[z] : null).Where(k => k != null)
            }).ToDictionary(x => x.Key, x => x.UserNames);

            var lstAll = lstDuAn.Select(x => new DuAnSearch
            {
                IdDuAn = x.IdDuAn,
                TenDuAn = x.TenDuAn,
                TenNhomDuAn = x.NhomDuAn != null ? x.NhomDuAn.TenNhomDuAn : "",
                TenGiaiDoanDuAn = x.GiaiDoanDuAn != null ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
                Ma = x.Ma,
                IdGiaiDoanDuAn = x.IdGiaiDoanDuAn,
                ThoiGianThucHien = x.ThoiGianThucHien,
                DiaDiem = x.DiaDiem,
                IdDuAnCha = x.IdDuAnCha,
                CreatedDate = x.CreatedDate,
                IdLinhVucNganhNghe = x.IdLinhVucNganhNghe,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUserID = dDuAnUser.ContainsKey(x.IdDuAn) ? dDuAnUser[x.IdDuAn] : "",
                DuAnUsers = dDuAnUserName.ContainsKey(x.IdDuAn) ? dDuAnUserName[x.IdDuAn] : ""
            }).ToList();
            if (username != null && username != "null")
            {
                username = username.ToLower();
                lstAll = lstAll.Where(x => x.DuAnUsers.ToLower().Contains(username)).ToList();
            }
            if (idNhomDuAnTheoUser == null)
            {
                lstAll = lstAll.Where(x => x.DuAnUserID.Contains(idUser)).ToList();
            }
            else
            {
                lstAll = lstAll.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser && x.DuAnUserID.Contains(idUser)).ToList();
            }
            if (IdGiaiDoan != -1)
            {
                lstAll = lstAll.Where(x => x.IdGiaiDoanDuAn == IdGiaiDoan).ToList();
            }
            if (uid != "null")
            {
                lstAll = lstAll.Where(x => x.DuAnUserID.Contains(uid)).ToList();
            }
            if (pid > 0)
            {
                lstAll = lstAll.Where(x => x.IdNhomDuAnTheoUser == pid).ToList();
            }
            if (lvid > 0)
            {
                lstAll = lstAll.Where(x => x.IdLinhVucNganhNghe == lvid).ToList();
            }
            if (diadiem != null)
            {
                lstAll = lstAll.Where(x => x.DiaDiem != null && x.DiaDiem.ToLower().Contains(diadiem.ToLower())).ToList();
            }

            if (filter != null)
            {
                var listSearch = lstAll.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.Contains(filter));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParentNews(lstAll, duAn.IdDuAnCha)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(lstAll);
                return reLst;
            }
            var parent = lstAll.Where(y => y.IdDuAnCha == null).OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = lstAll.Where(y => y.IdDuAnCha == null).OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = lstAll.Count(x => x.IdDuAnCha == null);
            foreach (var item in parent)
            {
                parent = parent.Union(GetChild(lstAll, item));
            }
            reLst.Add(parent);
            reLst.Add(lstAll);
            return reLst;
        }


        public List<IEnumerable<DuAnSearch>> GetPhanQuyen(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, string diadiem, int? IdGiaiDoan, string uid, int pid, int lvid, Dictionary<string, string> dicUser)
        {
            List<IEnumerable<DuAnSearch>> reLst = new List<IEnumerable<DuAnSearch>>();
            List<DuAnSearch> search = new List<DuAnSearch>();
            var lstDuAn = _duAnRepository.GetAll(new string[] { "NhomDuAn", "GiaiDoanDuAn" }).ToList();
            var lstDuAnUser = _duAnUserRepository.GetAll(new string[] { "AppUsers" }).ToList();

            var dDuAnUser = lstDuAnUser.GroupBy(x => x.IdDuAn).Select(x => new
            {
                x.Key,
                IdUsers = string.Join("; ", x.Select(y => y.UserId).Distinct())
            }).ToDictionary(x => x.Key, x => x.IdUsers);

            var dDuAnUserName = lstDuAnUser.GroupBy(x => x.IdDuAn).Select(x => new
            {
                x.Key,
                UserNames = string.Join("; ", x.Select(y => y.AppUsers.FullName).Distinct()) // (x.Select(y => dicUser.Keys.Where(h => h == y.UserId).FirstOrDefault())).Select(z => z != null ? dicUser[z] : null).Where(k => k != null)
            }).ToDictionary(x => x.Key, x => x.UserNames);

            var lstAll = lstDuAn.Select(x => new DuAnSearch
            {
                IdDuAn = x.IdDuAn,
                TenDuAn = x.TenDuAn,
                TenNhomDuAn = x.NhomDuAn != null ? x.NhomDuAn.TenNhomDuAn : "",
                TenGiaiDoanDuAn = x.GiaiDoanDuAn != null ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
                Ma = x.Ma,
                IdGiaiDoanDuAn = x.IdGiaiDoanDuAn,
                ThoiGianThucHien = x.ThoiGianThucHien,
                DiaDiem = x.DiaDiem,
                IdDuAnCha = x.IdDuAnCha,
                CreatedDate = x.CreatedDate,
                IdLinhVucNganhNghe = x.IdLinhVucNganhNghe,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUserID = dDuAnUser.ContainsKey(x.IdDuAn) ? dDuAnUser[x.IdDuAn] : "",
                DuAnUsers = dDuAnUserName.ContainsKey(x.IdDuAn) ? dDuAnUser[x.IdDuAn] : ""
            }); 

            if (idNhomDuAnTheoUser == null)
            {
                lstAll = lstAll.Where(x => x.DuAnUserID.Contains(idUser));
            }
            else
            {
                lstAll = lstAll.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser && x.DuAnUserID.Contains(idUser));
            }
            if (IdGiaiDoan != -1)
            {
                lstAll = lstAll.Where(x => x.IdGiaiDoanDuAn == IdGiaiDoan);
            }
            if(uid != "null")
            {
                lstAll = lstAll.Where(x => x.DuAnUserID.Contains(uid));
            }
            if(pid > 0)
            {
                lstAll = lstAll.Where(x => x.IdNhomDuAnTheoUser == pid);
            }
            if(lvid > 0)
            {
                lstAll = lstAll.Where(x => x.IdLinhVucNganhNghe == lvid);
            }
            if (diadiem != null)
            {
                lstAll = lstAll.Where(x => x.DiaDiem != null && x.DiaDiem.ToLower().Contains(diadiem.ToLower()));
            }

            if (filter != null)
            {
                var listSearch = lstAll.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.Contains(filter));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParentNews(lstAll, duAn.IdDuAnCha)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(lstAll);
                return reLst;
            }
            var parent = lstAll.Where(y => y.IdDuAnCha == null).OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = lstAll.Where(y => y.IdDuAnCha == null).OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = lstAll.Count(x => x.IdDuAnCha == null);
            foreach (var item in parent)
            {
                parent = parent.Union(GetChild(lstAll, item));
            }
            reLst.Add(parent);
            reLst.Add(lstAll);
            return reLst;
        }

        public DuAn GetById(int id)
        {
            return _duAnRepository.GetSingleByCondition(x => x.IdDuAn == id, new string[] { "NguonVonDuAns", "QuyetToanDuAns", "DuAnQuanLys", "DuAnQuanLys.QuanLyDA" });
        }

        public DuAn GetByIdForPhieu(int id)
        {
            return _duAnRepository.GetSingleByCondition(x => x.IdDuAn == id, new string[] { "ChuDauTu" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DuAn duAn)
        {
            _duAnRepository.Update(duAn);
        }

        private IEnumerable<DuAnSearch> GetChild(IEnumerable<DuAnSearch> duAns, DuAnSearch duAn)
        {
            return duAns.Where(x => x.IdDuAnCha == duAn.IdDuAn)
                .Union(duAns.Where(x => x.IdDuAnCha == duAn.IdDuAn).SelectMany(y => GetChild(duAns, y)));// || x.IdDuAn == duAn.IdDuAn
        }

        public IEnumerable<DuAn> FindAllChilds(List<DuAn> all_data, int toplevelid)
        {
            List<DuAn> inner = new List<DuAn>();
            foreach (var t in all_data.Where(item => item.IdDuAnCha == toplevelid))
            {
                inner.Add(t);
                inner = inner.Union(FindAllChilds(all_data, t.IdDuAn)).ToList();
            }

            return inner;
        }

        private IEnumerable<DuAn> FindAllParents(List<DuAn> all_data, int? child)
        {
            var parent = all_data.FirstOrDefault(x => x.IdDuAn == child);

            if (parent == null)
                return Enumerable.Empty<DuAn>();

            return new[] { parent }.Concat(FindAllParents(all_data, parent.IdDuAnCha));
        }

        private IEnumerable<DuAnSearch> FindAllParentNews(IEnumerable<DuAnSearch> all_data, int? child)
        {
            var parent = all_data.FirstOrDefault(x => x.IdDuAn == child);
            if (parent == null)
                return Enumerable.Empty<DuAnSearch>();
            return new[] { parent }.Concat(FindAllParentNews(all_data, parent.IdDuAnCha));
        }
        public IEnumerable<DuAn> DuAnByYearName(string year, string filter, int? IdNhomDuAnTheoUser)
        {

            //Cần phải viết lại thuật toán tìm kiếm và phân trang.
            IEnumerable<DuAn> query;

            if (IdNhomDuAnTheoUser == null)
            {
                query = _duAnRepository.GetAll(new[] { "NhomDuAn", "TinhTrangDuAn" });
                if (filter != null)
                {
                    query = query.Where(x => (x.CreatedDate.ToString().Contains(year) || x.UpdatedDate.ToString().Contains(year)) && x.TenDuAn.Contains(filter));
                }
                else
                {
                    query = query.Where(x => x.CreatedDate.ToString().Contains(year) || x.UpdatedDate.ToString().Contains(year));
                }
            }
            else
            {
                query = _duAnRepository.GetAll(new[] { "NhomDuAn", "TinhTrangDuAn" });
                if (filter != null)
                {
                    query = query.Where(x => (x.CreatedDate.ToString().Contains(year) || x.UpdatedDate.ToString().Contains(year)) && x.TenDuAn.Contains(filter) && x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser);
                }
                else
                {
                    query = query.Where(x => (x.CreatedDate.ToString().Contains(year) || x.UpdatedDate.ToString().Contains(year)) && x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser);
                }
            }
            return query.OrderByDescending(x => x.IdDuAn);
        }

        public IEnumerable<DuAn> DuAnNguonVonByYearName(int year, string filter)
        {
            IEnumerable<DuAn> query;
            List<DuAn> query1 = new List<DuAn>();
            query = _duAnRepository.GetAll(new[] { "NguonVonDuAns.NguonVon", "KeHoachVons", "LapQuanLyDauTus" });
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    if (item.KeHoachVons.Count() > 0)
                    {
                        var a = 0;
                        foreach (var item1 in item.KeHoachVons)
                        {
                            if (item1.NienDo == year)
                            {
                                a = 1;
                            }
                        }
                        if (a > 0)
                        {
                            query1.Add(item);
                        }
                    }
                }
            }

            if (filter != null)
            {
                return query1.Where(x => x.TenDuAn.Contains(filter)).OrderByDescending(x => x.IdDuAn);
            }
            else
            {
                return query1.OrderByDescending(x => x.IdDuAn);
            }
        }

        public IEnumerable<DuAn> DuAnNguonVonKHV(int? IdNhomDuAnTheoUser)
        {
            IEnumerable<DuAn> query;

            if (IdNhomDuAnTheoUser == null)
            {
                query = _duAnRepository.GetAll(new[] { "NguonVonDuAns.NguonVon", "KeHoachVons", "LapQuanLyDauTus" });
            }
            else
            {
                query = _duAnRepository.GetAll(new[] { "NguonVonDuAns.NguonVon", "KeHoachVons", "LapQuanLyDauTus" }).Where(x => x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser);
            }
            return query.OrderByDescending(x => x.IdDuAn);
        }

        public IEnumerable<DuAn> GetAllKHV()
        {
            return _duAnRepository.GetAllKHV();
        }

        public IEnumerable<DuAn> GetAllTienDoDauThau()
        {
            return _duAnRepository.GetAllTienDoDauThau();
        }

        public IEnumerable<DuAn> GetAllTienDoThucHienHopDong(int? IdNhomDuAnTheoUser)
        {
            if (IdNhomDuAnTheoUser == null)
            {
                return _duAnRepository.GetAllTienDoThucHienHopDong();
            }
            else
            {
                return _duAnRepository.GetAllTienDoThucHienHopDong().Where(x => x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser);
            }   
        }
        public IEnumerable<DuAn> GetAllDuAnCham(int? IdNhomDuAnTheoUser)
        {
            if (IdNhomDuAnTheoUser == null)
            {
                return _duAnRepository.GetAllDuAnCham();
            }
            else
            {
                return _duAnRepository.GetAllDuAnCham().Where(x => x.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser).ToList();
            }
        }

        public IEnumerable<DuAn> Filter(Expression<Func<DuAn, bool>> predicate, string[] includes = null)
        {
            return _duAnRepository.GetMulti(predicate, includes);

        }


        public IEnumerable<DuAn> GetAllNguonVonDuAn()
        {
            return _duAnRepository.GetAllNguonVonDuAn();
        }

        public IEnumerable<DuAn> GetAllQuanLy()
        {
            return _duAnRepository.GetAll(new string[] { "DuAnQuanLys" });
        }

        public DuAn GetByIdForBaoCao(int id)
        {
            return _duAnRepository.GetSingleByCondition(x => x.IdDuAn == id, new string[] { "NguonVonDuAns", "QuyetToanDuAns", "ChuDauTu", "NhomDuAn" });
        }

        public void GetChildByIdDuAn(int id, List<DuAn> duancontheoids, List<DuAn> duancontheoids2)
        {
            duancontheoids2 = new List<DuAn>();
            var duancons = _duAnRepository.GetAll().Where(x => x.IdDuAnCha != null && x.IdDuAnCha == id);
            foreach (var item in duancons)
            {
                int index = 0;
                if (duancontheoids.Count > 0)
                {
                    foreach (var item1 in duancontheoids)
                    {
                        if (item1.IdDuAn == item.IdDuAn)
                        {
                            index = 1;
                        }
                    }
                    if (index != 1)
                    {
                        duancontheoids.Add(item);
                        duancontheoids2.Add(item);
                    }
                }
                else
                {
                    duancontheoids.Add(item);
                    duancontheoids2.Add(item);
                }
            }
            if (duancontheoids2.Count > 0)
            {
                foreach (var item in duancontheoids2)
                {
                    this.GetChildByIdDuAn(item.IdDuAn, duancontheoids, duancontheoids2);
                }
            }
        }
        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanHoanThanh(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanhoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan != null && x.IdCoQuanPheDuyet == donvibaocao);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanhoanthanhs)
            {
                if (loaibaocao == "6 tháng đầu năm")
                {
                    if (item.NgayPheDuyetQuyetToan.Value.Month <= 6 && item.NgayPheDuyetQuyetToan.Value.Day <= 31 && item.NgayPheDuyetQuyetToan.Value.Year == year)
                    {
                        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                        duans.Add(duan);
                    }
                }
                if (loaibaocao == "cả năm")
                {
                    if (item.NgayPheDuyetQuyetToan.Value.Month <= 12 && item.NgayPheDuyetQuyetToan.Value.Day <= 31 && item.NgayPheDuyetQuyetToan.Value.Year == year)
                    {
                        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                        duans.Add(duan);
                    }
                }
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans;
        }

        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanNhanHoSo(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanchuahoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo != null && x.IdCoQuanPheDuyet == donvibaocao);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanchuahoanthanhs)
            {
                //if (loaibaocao == "6 tháng đầu năm")
                //{
                //    if (item.NgayNhanDuHoSo.Value.Month <= 6 && item.NgayNhanDuHoSo.Value.Day <= 31 && item.NgayNhanDuHoSo.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                //if (loaibaocao == "cả năm")
                //{
                //    if (item.NgayNhanDuHoSo.Value.Month <= 12 && item.NgayNhanDuHoSo.Value.Day <= 31 && item.NgayNhanDuHoSo.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                var duan = this.GetByIdForBaoCao(item.IdDuAn);
                duans.Add(duan);
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans;
        }

        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanChuaNhanHoSo(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanchuahoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo == null && x.NgayKyBienBan != null && x.IdCoQuanPheDuyet == donvibaocao);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanchuahoanthanhs)
            {
                //if (loaibaocao == "6 tháng đầu năm")
                //{
                //    if (item.NgayKyBienBan.Value.Month <= 6 && item.NgayKyBienBan.Value.Day <= 31 && item.NgayKyBienBan.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                //if (loaibaocao == "cả năm")
                //{
                //    if (item.NgayKyBienBan.Value.Month <= 12 && item.NgayKyBienBan.Value.Day <= 31 && item.NgayKyBienBan.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                var duan = this.GetByIdForBaoCao(item.IdDuAn);
                duans.Add(duan);
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans;
        }
        //mau 12
        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanHoanThanhByChuDauTu(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanhoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan != null);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanhoanthanhs)
            {
                if (loaibaocao == "6 tháng đầu năm")
                {
                    if (item.NgayPheDuyetQuyetToan.Value.Month <= 6 && item.NgayPheDuyetQuyetToan.Value.Day <= 31 && item.NgayPheDuyetQuyetToan.Value.Year == year)
                    {
                        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                        duans.Add(duan);
                    }
                }
                if (loaibaocao == "cả năm")
                {
                    if (item.NgayPheDuyetQuyetToan.Value.Month <= 12 && item.NgayPheDuyetQuyetToan.Value.Day <= 31 && item.NgayPheDuyetQuyetToan.Value.Year == year)
                    {
                        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                        duans.Add(duan);
                    }
                }
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans.Where(x => x.IdChuDauTu == donvibaocao);
        }

        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanNhanHoSoByChuDauTu(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanchuahoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo != null);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanchuahoanthanhs)
            {
                //if (loaibaocao == "6 tháng đầu năm")
                //{
                //    if (item.NgayNhanDuHoSo.Value.Month <= 6 && item.NgayNhanDuHoSo.Value.Day <= 31 && item.NgayNhanDuHoSo.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                //if (loaibaocao == "cả năm")
                //{
                //    if (item.NgayNhanDuHoSo.Value.Month <= 12 && item.NgayNhanDuHoSo.Value.Day <= 31 && item.NgayNhanDuHoSo.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                var duan = this.GetByIdForBaoCao(item.IdDuAn);
                duans.Add(duan);
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans.Where(x => x.IdChuDauTu == donvibaocao);
        }

        public IEnumerable<DuAn> GetByLoaiBaoCaoByYearByQuyetToanChuaNhanHoSoByChuDauTu(int donvibaocao, string loaibaocao, int year)
        {
            var quyettoanduanchuahoanthanhs = _quyetToanDuAnService.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo == null && x.NgayKyBienBan != null);
            List<DuAn> duans = new List<DuAn>();
            foreach (var item in quyettoanduanchuahoanthanhs)
            {
                //if (loaibaocao == "6 tháng đầu năm")
                //{
                //    if (item.NgayKyBienBan.Value.Month <= 6 && item.NgayKyBienBan.Value.Day <= 31 && item.NgayKyBienBan.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                //if (loaibaocao == "cả năm")
                //{
                //    if (item.NgayKyBienBan.Value.Month <= 12 && item.NgayKyBienBan.Value.Day <= 31 && item.NgayKyBienBan.Value.Year == year)
                //    {
                //        var duan = this.GetByIdForBaoCao(item.IdDuAn);
                //        duans.Add(duan);
                //    }
                //}
                var duan = this.GetByIdForBaoCao(item.IdDuAn);
                duans.Add(duan);
            }
            for (var i = 0; i < duans.Count; i++)
            {
                for (var j = i + 1; j < duans.Count; j++)
                {
                    if (duans[i] == duans[j])
                    {
                        duans.Remove(duans[j]);
                    }
                }
            }
            return duans.Where(x => x.IdChuDauTu == donvibaocao);
        }


        public IEnumerable<BC_LocDuAn_PhanQuyen_PhongCT> GetList_LocDuAn_For_BaoCao(IEnumerable<int> idsIDDA, Dictionary<string, string> lstUserNhanVien)
        {
            var qr = _duAnRepository
                .GetMulti(x => idsIDDA.Contains(x.IdDuAn), new string[] { "NhomDuAn", "LinhVucNganhNghe", "GiaiDoanDuAn", "DuAnUsers" })
                .Select(x => new
                {
                    IDPhong = x.IdNhomDuAnTheoUser,
                    IDDA = x.IdDuAn,
                    IDDACha = x.IdDuAnCha,
                    TenDuAn = x.TenDuAn,
                    MaDuAn = x.Ma,
                    TenNhomDuAn = x.NhomDuAn == null ? "" : x.NhomDuAn.TenNhomDuAn,
                    TenLVNN = x.LinhVucNganhNghe == null ? "" : x.LinhVucNganhNghe.TenLinhVucNganhNghe,
                    TenGD = x.GiaiDoanDuAn == null ? "" : x.GiaiDoanDuAn.TenGiaiDoanDuAn,
                    ThoiGianThucHien = x.ThoiGianThucHien,
                    Users = x.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                });

            var abc = qr.ToList();

            var query = qr.GroupBy(x => new { x.IDPhong })
                 .Select(x => new BC_LocDuAn_PhanQuyen_PhongCT()
                 {
                     IDPhong = x.Key.IDPhong,
                     grpDuAnCha = x.Where(y => y.IDDACha == null).GroupBy(y => new { y.IDDA, y.TenDuAn })
                     .Select(y => new BC_LocDuAn_PhanQuyen_DuAn_Cha()
                     {
                         IDDA = y.Key.IDDA,
                         TenDuAnCha = y.Key.TenDuAn,
                         MaDuAnCha = y.Select(z => z.MaDuAn).FirstOrDefault(),
                         TenNhomDuAnCha = y.Select(z => z.TenNhomDuAn).FirstOrDefault(),
                         TenLVNNCha = y.Select(z => z.TenLVNN).FirstOrDefault(),
                         TenGDCha = y.Select(z => z.TenGD).FirstOrDefault(),
                         ThoiGianThucHienCha = y.Select(z => z.ThoiGianThucHien).FirstOrDefault(),
                         QuanLyCha = "",
                         Users = y.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                         grpDuAnCon = y.Where(z => z.IDDACha == y.Key.IDDA).GroupBy(z => new { z.IDDA, z.TenDuAn })
                         .Select(z => new BC_LocDuAn_PhanQuyen_DuAn()
                         {
                             IDDA = z.Key.IDDA,
                             TenDuAn = z.Key.TenDuAn,
                             MaDuAn = z.Select(z1 => z1.MaDuAn).FirstOrDefault(),
                             TenNhomDuAn = z.Select(z1 => z1.TenNhomDuAn).FirstOrDefault(),
                             TenLVNN = z.Select(z1 => z1.TenLVNN).FirstOrDefault(),
                             TenGD = z.Select(z1 => z1.TenGD).FirstOrDefault(),
                             ThoiGianThucHien = z.Select(z1 => z1.ThoiGianThucHien).FirstOrDefault(),
                             QuanLy = "",
                             Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                         })
                     })
                 });

            return query;
        }

        public IEnumerable<BC_TienDo_Phong_CanBo_QuanLy> GetList_DuAn_Phong_CanBo_QuanLy_For_BaoCao_TienDo(string IDDuAns, Dictionary<string, string> lstUserNhanVien)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var query = _duAnRepository.GetMulti(x => arrDuAn.Contains(x.IdDuAn.ToString()), new string[] { "DuAnUsers", "NhomDuAnTheoUser" })
                .Select(x => new 
                {
                    IDDA = x.IdDuAn,
                    TenPhong = x.NhomDuAnTheoUser.TenNhomDuAn,
                    Users = x.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                });

            var lst = query.GroupBy(x => new { x.IDDA })
                .Select(x => new BC_TienDo_Phong_CanBo_QuanLy()
                {
                    IDDA = x.Key.IDDA,
                    TenPhong = x.Select(y => y.TenPhong).FirstOrDefault(),
                    Users = x.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                });
            return lst;
        }

        //public IEnumerable<DuAn> GetForLocDuAnNew()
        //{
        //    return _duAnRepository.GetAll(new[] { "NhomDuAn", "GiaiDoanDuAn", "DuAnUsers", "NguonVonDuAns" });
        //}
                                                    
        public List<IEnumerable<DuAnSearch>> GetByNewFilter(int? idNhomDuAnTheoUser, string idUser, int page, int pageSize, out int totalRow, string filter, string thoigianthuchien, string diadiem, string strIdChuDauTu, string strIdNhomDuAn, string strIdTinhTrangDuAn, string strIdLinhVucNganhNghe, string strIdNguonVon, string strIdGiaiDoan, string strIdQuanLy, string strIdPhongBan, string tenDA, int IdGiaiDoan)
        {
            List<IEnumerable<DuAnSearch>> reLst = new List<IEnumerable<DuAnSearch>>();
            IEnumerable<DuAn> lstDuAn;
            //Danh sách dự án tìm kiếm
            List<DuAnSearch> search = new List<DuAnSearch>();
            var lstDuAnUser = _duAnUserRepository.GetMulti(x => x.UserId == idUser, new string[] { "AppUsers" }).Select(x => x.IdDuAn).Distinct().ToList();
            List<DuAn> allDuAns = new List<DuAn>();    

            if (idNhomDuAnTheoUser == null)
            {
                lstDuAn = _duAnRepository.GetMulti(x => lstDuAnUser.Contains(x.IdDuAn), new string[] { "NhomDuAn", "GiaiDoanDuAn", "ChuDauTu"});
            }
            else
            {
                lstDuAn = _duAnRepository.GetMulti(x => lstDuAnUser.Contains(x.IdDuAn) && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser, new string[] { "NhomDuAn", "GiaiDoanDuAn" });
            }                      


            //Trả về danh sách dự án gồm các thông tin bên dưới
            var qLst = lstDuAn.Select(x => new DuAnSearch
            {
                IdDuAn = x.IdDuAn,
                TenDuAn = x.TenDuAn,
                TenNhomDuAn = x.NhomDuAn != null ? x.NhomDuAn.TenNhomDuAn : "",
                IdGiaiDoanDuAn = x.IdGiaiDoanDuAn,
                TenGiaiDoanDuAn = x.GiaiDoanDuAn != null ? x.GiaiDoanDuAn.TenGiaiDoanDuAn : "",
                Ma = x.Ma,
                ThoiGianThucHien = x.ThoiGianThucHien,
                IdDuAnCha = x.IdDuAnCha,
                DiaDiem = x.DiaDiem,
                IdLinhVucNganhNghe = x.IdLinhVucNganhNghe,
                IdTinhTrangDuAn = x.IdTinhTrangDuAn,
                IdHinhThucQuanLy = x.IdHinhThucQuanLy,
                IdChuDauTu = x.IdChuDauTu,
                IdNhomDuAn = x.IdNhomDuAn,
                IdPhongBan = x.IdNhomDuAnTheoUser
            }).ToList();
            
            List<DuAnSearch> listSearchByFilter = qLst;
            List<DuAnSearch> listSearchByThoiGian = qLst;
            List<DuAnSearch> listSearchDiaDiem = qLst;
            List<DuAnSearch> listDuAnByChuDauTu = qLst;
            List<DuAnSearch> listDuAnByNhomDuAn = qLst;
            List<DuAnSearch> listDuAnByIdTinhTrangDuAn = qLst;
            List<DuAnSearch> listDuAnByIdLinhVucNganhNghe = qLst;
            List<DuAnSearch> listDuAnByIdNguonVon = qLst;
            List<DuAnSearch> listDuAnByIdGiaiDoan = qLst;
            List<DuAnSearch> listDuAnByIdQuanLy = qLst;
            List<DuAnSearch> listDuAnByIdPhongBan = qLst;

            if (IdGiaiDoan != -1)
            {
                qLst = qLst.Where(x => x.IdGiaiDoanDuAn == IdGiaiDoan).ToList();
            }

            if (filter != null)
            {
                //Danh sách các dự án có tên chứa filter hoặc mã chứa filter
                listSearchByFilter = qLst.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()) || x.Ma.Contains(filter)).ToList();
                foreach (var duAn in listSearchByFilter)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }
                }
                //Tìm dự án cha
                var parents = listSearchByFilter.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }

            if (thoigianthuchien != null)
            {
                //Danh sách các dự án có tên chứa
                listSearchByThoiGian = qLst.Where(x => x.ThoiGianThucHien != null && x.ThoiGianThucHien.ToLower().Contains(thoigianthuchien.ToLower())).ToList();
                foreach (var duAn in listSearchByThoiGian)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = search.Where(x => x.IdDuAn == duAn.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }
                }
                //Tìm dự án cha
                var parents = listSearchByThoiGian.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }

            if (diadiem != null)
            {              
                listSearchDiaDiem = qLst.Where(x =>x.DiaDiem != null && x.DiaDiem.ToLower().Contains(diadiem.ToLower())).ToList();
                foreach (var duAnByDiaDiem in listSearchDiaDiem)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = search.Where(x => x.IdDuAn == duAnByDiaDiem.IdDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAnByDiaDiem);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAnByDiaDiem.IdDuAnCha)).ToList();
                    }
                }
                //Tìm dự án cha
                var parents = listSearchDiaDiem.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAnByDiaDiem in resual)
                {
                    parents = parents.Union(GetChild(search, duAnByDiaDiem));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdChuDauTu != null)
            {
                var listIdChuDauTu = new JavaScriptSerializer().Deserialize<List<int>>(strIdChuDauTu);
                //listDuAnByChuDauTu = lstDuAn.Where(x => x.IdChuDauTu != null && listIdChuDauTu.Any(y => y == x.IdChuDauTu)).ToList();
                listDuAnByChuDauTu = qLst.Where(x => x.IdChuDauTu != null && listIdChuDauTu.Any(y => y == x.IdChuDauTu)).ToList();
                foreach (var duAn in listDuAnByChuDauTu)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdChuDauTu == duAn.IdChuDauTu);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByChuDauTu.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdNhomDuAn != null)
            {
                var listIdNhomDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdNhomDuAn);
                listDuAnByNhomDuAn = qLst.Where(x => x.IdNhomDuAn != null && listIdNhomDuAn.Any(y => y == x.IdNhomDuAn)).ToList();
                foreach (var duAn in listDuAnByNhomDuAn)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdNhomDuAn == duAn.IdNhomDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByNhomDuAn.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdTinhTrangDuAn != null)
            {
                var listIdTinhTrangDuAn = new JavaScriptSerializer().Deserialize<List<int>>(strIdTinhTrangDuAn);
                listDuAnByIdTinhTrangDuAn = qLst.Where(x => x.IdTinhTrangDuAn != null && listIdTinhTrangDuAn.Any(y => y == x.IdTinhTrangDuAn)).ToList();
                foreach (var duAn in listDuAnByIdTinhTrangDuAn)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdTinhTrangDuAn == duAn.IdTinhTrangDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdTinhTrangDuAn.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdLinhVucNganhNghe != null)
            {
                var listIdLinhVucNganhNghe = new JavaScriptSerializer().Deserialize<List<int>>(strIdLinhVucNganhNghe);
                listDuAnByIdLinhVucNganhNghe = qLst.Where(x => x.IdLinhVucNganhNghe != null && listIdLinhVucNganhNghe.Any(y => y == x.IdLinhVucNganhNghe)).ToList();
                foreach (var duAn in listDuAnByIdLinhVucNganhNghe)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdLinhVucNganhNghe == duAn.IdLinhVucNganhNghe);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdLinhVucNganhNghe.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdNguonVon != null)
            {
                var listIdNguonVon = new JavaScriptSerializer().Deserialize<List<int>>(strIdNguonVon);
                listDuAnByIdNguonVon = qLst.Where(x => x.IdNguonVonDuAn != null && listIdNguonVon.Any(y => y == x.IdNguonVonDuAn)).ToList();
                foreach (var duAn in listDuAnByIdNguonVon)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdNguonVonDuAn == duAn.IdNguonVonDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdNguonVon.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdGiaiDoan != null)
            {
                var listIdGiaiDoan = new JavaScriptSerializer().Deserialize<List<int>>(strIdGiaiDoan);
                listDuAnByIdGiaiDoan = qLst.Where(x => x.IdGiaiDoanDuAn != null && listIdGiaiDoan.Any(y => y == x.IdGiaiDoanDuAn)).ToList();
                foreach (var duAn in listDuAnByIdGiaiDoan)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdGiaiDoanDuAn == duAn.IdGiaiDoanDuAn);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdGiaiDoan.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdQuanLy != null)
            {
                var listIdQuanLy = new JavaScriptSerializer().Deserialize<List<int>>(strIdQuanLy);
                listDuAnByIdQuanLy = qLst.Where(x => x.IdHinhThucQuanLy != null && listIdQuanLy.Any(y => y == x.IdHinhThucQuanLy)).ToList();
                foreach (var duAn in listDuAnByIdQuanLy)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdHinhThucQuanLy == duAn.IdHinhThucQuanLy);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdQuanLy.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }
            if (strIdPhongBan != null)
            {
                var listIdPhongBan = new JavaScriptSerializer().Deserialize<List<int>>(strIdPhongBan);
                listDuAnByIdPhongBan = qLst.Where(x => x.IdPhongBan != null && listIdPhongBan.Any(y => y == x.IdPhongBan)).ToList();
                foreach (var duAn in listDuAnByIdPhongBan)
                {
                    //Chọn ra các dự án tìm kiếm trùng idduan
                    var items = qLst.Where(x => x.IdPhongBan == duAn.IdPhongBan);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        //Tìm các dự án không thỏa mãn điều kiện tìm kiếm
                        search = search.Union(FindAllParentNews(qLst, duAn.IdDuAnCha)).ToList();
                    }

                }
                //Tìm dự án cha
                var parents = listDuAnByIdPhongBan.Where(x => x.IdDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                reLst.Add(parents);
                reLst.Add(qLst);
                //return reLst;
            }

            var locduans = qLst.Join(listSearchByFilter,
                a => a.IdDuAn,
                b => b.IdDuAn,
                (a, b) => new
                {
                    a
                }).Join(listSearchDiaDiem,
                ab => ab.a.IdDuAn,
                c => c.IdDuAn,
                (ab, c) => new
                {
                    c
                }).Join(listSearchByThoiGian,
                abc => abc.c.IdDuAn,
                d => d.IdDuAn,
                (abc, d) => new {
                    d
                }).Join(listDuAnByNhomDuAn,
                abcd => abcd.d.IdDuAn,
                e => e.IdDuAn,
                (abcd, e) => new {
                    e
                }).Join(listDuAnByIdTinhTrangDuAn,
                abcde => abcde.e.IdDuAn,
                f => f.IdDuAn,
                (abcde, f) => new {
                    f
                }).Join(listDuAnByChuDauTu,
                abcdef => abcdef.f.IdDuAn,
                g => g.IdDuAn,
                (abcdef, g) => new
                {
                    g
                }).Join(listDuAnByIdLinhVucNganhNghe,
                abcdefg => abcdefg.g.IdDuAn,
                h => h.IdDuAn,
                (abcdefg, h) => new
                {
                    h
                }).Join(listDuAnByIdNguonVon,
                abcdefgh => abcdefgh.h.IdDuAn,
                i => i.IdDuAn,
                (abcdefgh, i) => new
                {
                    i
                }).Join(listDuAnByIdQuanLy,
                abcdefghi => abcdefghi.i.IdDuAn,
                j => j.IdDuAn,
                (abcdefghi, j) => new
                {
                    j
                }).Join(listDuAnByIdGiaiDoan,
                abcdefghij => abcdefghij.j.IdDuAn,
                k => k.IdDuAn,
                (abcdefghij, k) => new
                {
                    k
                }).Join(listDuAnByIdPhongBan,
                abcdefghijk => abcdefghijk.k.IdDuAn,
                l => l.IdDuAn,
                (abcdefghijk, l) => new DuAnSearch
                {
                    IdDuAn = l.IdDuAn,
                    Ma = l.Ma,
                    TenDuAn = l.TenDuAn,
                    IdDuAnCha = l.IdDuAnCha,
                    IdChuDauTu = l.IdChuDauTu,
                    ThoiGianThucHien = l.ThoiGianThucHien,
                    IdLinhVucNganhNghe = l.IdLinhVucNganhNghe,
                    DiaDiem = l.DiaDiem,
                    IdNhomDuAn = l.IdNhomDuAn,
                    IdGiaiDoanDuAn = l.IdGiaiDoanDuAn,
                    IdHinhThucQuanLy = l.IdHinhThucQuanLy,
                    IdTinhTrangDuAn = l.IdTinhTrangDuAn,
                    IdNhomDuAnTheoUser = l.IdPhongBan,
                    TenNhomDuAn = l.TenNhomDuAn,
                    TenGiaiDoanDuAn = l.TenGiaiDoanDuAn
                }).ToList();

            totalRow = locduans.Count();
            var result = from s in locduans
                         select s;
            //var parent = qLst.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!parent.Any() && page > 1)
            //{
            //    page = page - 1;
            //    parent = qLst.Where(y => y.IdDuAnCha == null).OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            //totalRow = qLst.Count(x => x.IdDuAnCha == null);
            //foreach (var item in result)
            //{
            //    parent = parent.Union(GetChild(qLst, item));
            //}
            List<IEnumerable<DuAnSearch>> newreLst = new List<IEnumerable<DuAnSearch>>();

            //foreach(var item in result)
            //{
            //    newreLst.Add(result);
            //}
            newreLst.Add(result);

            return newreLst;

        }

        //SignalR DuAn Service

        //public DuAn Create(DuAn duan)
        //{
        //    return _duAnRepository.Add(duan);
        //}

        //public void DeleteDASignalR(int notificationId)
        //{
        //    _duAnRepository.Delete(notificationId);
        //}

        //public List<DuAn> GetListAll(int pageIndex, int pageSize, out int totalRow)
        //{
        //    var query = _duAnRepository.GetAll(new string[] { "AppUser" });
        //    totalRow = query.Count();
        //    return query.OrderByDescending(x => x.CreatedDate)
        //        .Skip(pageSize * (pageIndex - 1))
        //        .Take(pageSize).ToList();
        //}

        //public List<DuAn> GetListByUserId(string userId, int pageIndex, int pageSize, out int totalRow)
        //{
        //    var query = _duAnRepository.GetMulti(x => x.UserId == userId);
        //    totalRow = query.Count();
        //    return query.OrderByDescending(x => x.CreatedDate)
        //        .Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
        //}

        //public List<DuAn> GetListByUserId(string userId, int top)
        //{
        //    return _duAnRepository.GetMulti(x => x.UserId == userId)
        //        .OrderByDescending(x => x.CreatedDate)
        //        .Take(top).ToList();
        //}

        //public DuAn GetDetail(int id)
        //{
        //    return _duAnRepository.GetSingleByCondition(x => x.IdDuAn == id, new string[] { "AppUser" });
        //}

        //public List<DuAn> ListAllUnread(string userId, int pageIndex, int pageSize, out int totalRow)
        //{
        //    var query = _duAnRepository.GetAllUnread(userId);
        //    totalRow = query.Count();
        //    return query.OrderByDescending(x => x.CreatedDate).Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
        //}

        //public void MarkAsRead(string userId, int notificationId)
        //{
        //    var announ = _duAnUserRepository.GetSingleByCondition(x => x.IdDuAn == notificationId && x.UserId == userId);
        //    if (announ == null)
        //    {
        //        _duAnUserRepository.Add(new DuAnUser()
        //        {
        //            IdDuAn = notificationId,
        //            UserId = userId,
        //            HasRead = true
        //        });
        //    }
        //    else
        //    {
        //        announ.HasRead = true;
        //    }
        //}

    }

    public class DuAnSearch
    {
        public int IdDuAn { get; set; }
        public string TenDuAn { get; set; }
        public string TenNhomDuAn { get; set; }
        public string TenGiaiDoanDuAn { get; set; }
        public int? IdGiaiDoanDuAn { get; set; }
        public string Ma { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string DuAnUsers { get; set; }
        public string DiaDiem { get; set; }
        public int? IdDuAnCha { get; set; }
        public int? IdDanhMucNhaThau { get; set; }
        public int? IdNhomDuAnTheoUser { get; set; }
        public int? IdLinhVucNganhNghe { get; set; }
        public int? IdChuDauTu { get; set; }
        public int? IdNguonVonDuAn { get; set; }
        public int? IdHinhThucQuanLy { get; set; }
        public int? IdPhongBan { get; set; }
        public int? IdTinhTrangDuAn { get; set; }
        public int? IdNhomDuAn { get; set; }
        public string DuAnUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
