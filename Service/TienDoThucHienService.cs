﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ITienDoThucHienService
    {
        TienDoThucHien Add(TienDoThucHien iTienDoThucHien);

        void Update(TienDoThucHien iTienDoThucHien);

        TienDoThucHien Delete(int id);

        IEnumerable<TienDoThucHien> GetAll();

        IEnumerable<TienDoThucHien> GetAll(string filter);

        IEnumerable<HopDong> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter);

        TienDoThucHien GetById(int id);

        void Save();
    }
    public class TienDoThucHienService : ITienDoThucHienService
    {
        private ITienDoThucHienRepository _TienDoThucHienRepository;
        private IUnitOfWork _unitOfWork;

        public TienDoThucHienService(ITienDoThucHienRepository tienDoThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._TienDoThucHienRepository = tienDoThucHienRepository;
            this._unitOfWork = unitOfWork;
        }
        public TienDoThucHien Add(TienDoThucHien chuTruongDauTu)
        {
            return _TienDoThucHienRepository.Add(chuTruongDauTu);
        }

        public TienDoThucHien Delete(int id)
        {
            return _TienDoThucHienRepository.Delete(id);
        }

        public IEnumerable<TienDoThucHien> GetAll()
        {
            return _TienDoThucHienRepository.GetAll();
        }

        public IEnumerable<TienDoThucHien> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _TienDoThucHienRepository.GetMulti(x => x.TenCongViec.Contains(filter));
            }
            return _TienDoThucHienRepository.GetAll();
        }

        public IEnumerable<HopDong> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            IEnumerable<HopDong> query = null;
           
          
                query = _TienDoThucHienRepository.GetThucHienHopDongs(idDA,filter);
            
         
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHopDong).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public TienDoThucHien GetById(int id)
        {
            return _TienDoThucHienRepository.GetSingleByCondition(x => x.IdTienDo == id, new string[] { "TienDoChiTiets"});
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(TienDoThucHien updateTienDoThucHien)
        {
            _TienDoThucHienRepository.Update(updateTienDoThucHien);
        }

      
    }
}
