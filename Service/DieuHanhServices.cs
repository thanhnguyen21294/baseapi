﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IDieuHanhServices
    {
        object GetAllCTCVChuaHT();
    }
    public class DieuHanhServices : IDieuHanhServices
    {
        private IQLTD_TDTH_ChuanBiDauTuRepository _QLTD_TDTH_ChuanBiDauTuRepository;
        private IQLTD_THCT_ChuanBiDauTuRepository _QLTD_THCT_ChuanBiDauTuRepository;

        private IQLTD_TDTH_ChuanBiThucHienRepository _QLTD_TDTH_ChuanBiThucHienRepository;
        private IQLTD_THCT_ChuanBiThucHienRepository _QLTD_THCT_ChuanBiThucHienRepository;

        private IQLTD_TDTH_ChuTruongDauTuRepository _QLTD_TDTH_ChuTruongDauTuRepository;
        private IQLTD_THCT_ChuTruongDauTuRepository _QLTD_THCT_ChuTruongDauTuRepository;

        private IQLTD_TDTH_ThucHienDuAnRepository _QLTD_TDTH_ThucHienDuAnRepository;
        private IQLTD_THCT_ThucHienDuAnRepository _QLTD_THCT_ThucHienDuAnRepository;

        private IQLTD_TDTH_QuyetToanDuAnRepository _QLTD_TDTH_QuyetToanDuAnRepository;
        private IQLTD_THCT_QuyetToanDuAnRepository _QLTD_THCT_QuyetToanDuAnRepository;

        private IUnitOfWork _unitOfWork;

        public DieuHanhServices(
            IQLTD_TDTH_ChuanBiDauTuRepository qltd_ChuanBiDauTuRepository,
            IQLTD_THCT_ChuanBiDauTuRepository qltd_THCT_ChuanBiDauTuRepository,
            IQLTD_TDTH_ChuanBiThucHienRepository qltd_TDTH_ChuanBiThucHienRepository,
            IQLTD_THCT_ChuanBiThucHienRepository qltd_THCT_ChuanBiThucHienRepository,
            IQLTD_TDTH_ChuTruongDauTuRepository qltd_TDTH_ChuTruongDauTuRepository,
            IQLTD_THCT_ChuTruongDauTuRepository qltd_THCT_ChuTruongDauTuRepository,
            IQLTD_TDTH_ThucHienDuAnRepository qltd_TDTH_ThucHienDuAnRepository,
            IQLTD_THCT_ThucHienDuAnRepository qltd_THCT_ThucHienDuAnRepository,
            IQLTD_TDTH_QuyetToanDuAnRepository qltd_TDTH_QuyetToanDuAnRepository,
            IQLTD_THCT_QuyetToanDuAnRepository qltd_THCT_QuyetToanDuAnRepository,
            IUnitOfWork unitOfWork)
        {
            _QLTD_TDTH_ChuanBiDauTuRepository = qltd_ChuanBiDauTuRepository;
            _QLTD_THCT_ChuanBiDauTuRepository = qltd_THCT_ChuanBiDauTuRepository;

            _QLTD_TDTH_ChuanBiThucHienRepository = qltd_TDTH_ChuanBiThucHienRepository;
            _QLTD_THCT_ChuanBiThucHienRepository = qltd_THCT_ChuanBiThucHienRepository;

            _QLTD_TDTH_ChuTruongDauTuRepository = qltd_TDTH_ChuTruongDauTuRepository;
            _QLTD_THCT_ChuTruongDauTuRepository = qltd_THCT_ChuTruongDauTuRepository;

            _QLTD_TDTH_ThucHienDuAnRepository = qltd_TDTH_ThucHienDuAnRepository;
            _QLTD_THCT_ThucHienDuAnRepository = qltd_THCT_ThucHienDuAnRepository;

            _QLTD_TDTH_QuyetToanDuAnRepository = qltd_TDTH_QuyetToanDuAnRepository;
            _QLTD_THCT_QuyetToanDuAnRepository = qltd_THCT_QuyetToanDuAnRepository;

            _unitOfWork = unitOfWork;
        }

        public object GetAllCTCVChuaHT()
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstCTDTDaBam = _QLTD_THCT_ChuTruongDauTuRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCTDT = _QLTD_TDTH_ChuTruongDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuTruongDauTu", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec" })
                .Where(x => !lstCTDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuTruongDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.TrangThai == 4)
                .GroupBy(g => new { g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new { g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.IdCongViec, g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();

            var lstCBDTDaBam = _QLTD_THCT_ChuanBiDauTuRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBDT = _QLTD_TDTH_ChuanBiDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiDauTu", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec" })
                .Where(x => !lstCBDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.TrangThai == 4)
                .GroupBy(g => new { g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new { g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.IdCongViec, g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 3
                        }).ToList()
                    }).ToList()
                }).ToList();

            var lstCBTHDaBam = _QLTD_THCT_ChuanBiThucHienRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBTH = _QLTD_TDTH_ChuanBiThucHienRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiThucHien", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec" })
                .Where(x => !lstCBTHDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiThucHien.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.TrangThai == 4)
                .GroupBy(g => new { g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new { g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.IdCongViec, g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();

            var lstTHDADaBam = _QLTD_THCT_ThucHienDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstTHDA = _QLTD_TDTH_ThucHienDuAnRepository.GetAll(new string[] { "QLTD_CTCV_ThucHienDuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_CongViec" })
                .Where(x => !lstTHDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ThucHienDuAn.NgayHoanThanh == null && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.TrangThai == 4)
                .GroupBy(g => new { g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new { g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.IdCongViec, g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();

            var lstQTDADaBam = _QLTD_THCT_QuyetToanDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstQTDA = _QLTD_TDTH_QuyetToanDuAnRepository.GetAll(new string[] { "QLTD_CTCV_QuyetToanDuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec" })
                .Where(x => !lstQTDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_QuyetToanDuAn.NgayHoanThanh == null && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.TrangThai == 4)
                .GroupBy(g => new { g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new { g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.IdCongViec, g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();
            var re = lstCTDT.Union(lstCBDT).Union(lstCBTH).Union(lstTHDA).Union(lstQTDA);
            return lstCTDT;
        }
    }
}
