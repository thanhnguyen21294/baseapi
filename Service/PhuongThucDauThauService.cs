﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IPhuongThucDauThauService
    {
        PhuongThucDauThau Add(PhuongThucDauThau phuongThucDauThau);

        void Update(PhuongThucDauThau phuongThucDauThau);

        PhuongThucDauThau Delete(int id);

        IEnumerable<PhuongThucDauThau> GetAll();

        IEnumerable<PhuongThucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        PhuongThucDauThau GetById(int id);

        void Save();
    }

    public class PhuongThucDauThauService : IPhuongThucDauThauService
    {
        private IPhuongThucDauThauRepository _phuongThucDauThauRepository;
        private IUnitOfWork _unitOfWork;

        public PhuongThucDauThauService(IPhuongThucDauThauRepository phuongThucDauThauRepository, IUnitOfWork unitOfWork)
        {
            this._phuongThucDauThauRepository = phuongThucDauThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public PhuongThucDauThau Add(PhuongThucDauThau phuongThucDauThau)
        {
            return _phuongThucDauThauRepository.Add(phuongThucDauThau);
        }

        public PhuongThucDauThau Delete(int id)
        {
            return _phuongThucDauThauRepository.Delete(id);
        }

        public IEnumerable<PhuongThucDauThau> GetAll()
        {
            return _phuongThucDauThauRepository.GetAll();
        }

        public IEnumerable<PhuongThucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _phuongThucDauThauRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenPhuongThucDauThau.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdPhuongThucDauThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public PhuongThucDauThau GetById(int id)
        {
            return _phuongThucDauThauRepository.GetMulti(x => x.IdPhuongThucDauThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PhuongThucDauThau phuongThucDauThau)
        {
            _phuongThucDauThauRepository.Update(phuongThucDauThau);
        }
    }
}
