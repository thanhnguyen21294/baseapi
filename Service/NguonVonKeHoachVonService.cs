﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface INguonVonKeHoachVonService
    {
        NguonVonKeHoachVon Add(NguonVonKeHoachVon nguonVonKeHoachVon);
        void DeleteByIdKeHoachVon(int IdDuAn);

        void Save();
    }

    public class NguonVonKeHoachVonService : INguonVonKeHoachVonService
    {
        private INguonVonKeHoachVonRepository _nguonVoKeHoachVonRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonKeHoachVonService(INguonVonKeHoachVonRepository nguonVonKeHoachVonRepository, IUnitOfWork unitOfWork)
        {
            this._nguonVoKeHoachVonRepository = nguonVonKeHoachVonRepository;
            this._unitOfWork = unitOfWork;
        }

        public NguonVonKeHoachVon Add(NguonVonKeHoachVon nguonVonKeHoachVon)
        {
            return _nguonVoKeHoachVonRepository.Add(nguonVonKeHoachVon);
        }

        public void DeleteByIdKeHoachVon(int idKHV)
        {
            _nguonVoKeHoachVonRepository.DeleteMulti(x => x.IdKeHoachVon == idKHV);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}

