﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_KhoKhanVuongMacService
    {
        QLTD_KhoKhanVuongMac Add(QLTD_KhoKhanVuongMac qltd_KhoKhanVuongMac);

        void Update(QLTD_KhoKhanVuongMac qltd_KhoKhanVuongMac);

        QLTD_KhoKhanVuongMac Delete(int id);

        IEnumerable<QLTD_KhoKhanVuongMac> GetByIdKeHoach(int idKH);

        IEnumerable<BC_KhoKhanVuongMac> GetBCKhoKhanDieuHanh(string userId, int idNhomDuAnTheoUser, Dictionary<string, string> dicUser);

        QLTD_KhoKhanVuongMac GetById(int id);

        object GetKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser);
        object GetPageKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser, int Numberpage, int page, string filter, Dictionary<string, string> dicUser);
        object GetGroupAllKeHoachByDuAn(string userId, int Numberpage, int page, string filter, Dictionary<string, string> dicUser);
        object GetCountKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser); void Save();
        object GetCountKeHoachByDuAn(string userId);
        IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_LCTDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_CBDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_CBTH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_TH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_TienDo_KK_QT> Get_KK_GP_QT_For_BaoCao_TienDo(string IDDuAns);

        IEnumerable<BC_Uyban_PL1_KKGP> Get_NoiDung_KK_GP_For_BaoCao(int idkh);

        IEnumerable<BC_Uyban_PL1_KKGP_New> Get_NoiDung_KK_GP_For_BaoCao_New();
    }

    public class QLTD_KhoKhanVuongMacService : IQLTD_KhoKhanVuongMacService
    {
        private IQLTD_KhoKhanVuongMacRepository _QLTD_KhoKhanVuongMacRepository;
        private IQLTD_KeHoachRepository _IQLTD_KeHoachRepository;
        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IDuAnRepository _DuAnRepository;
        private IUnitOfWork _unitOfWork;
        public QLTD_KhoKhanVuongMacService(IQLTD_KhoKhanVuongMacRepository qltd_KhoKhanVuongMacRepository,
            IQLTD_KeHoachRepository qltd_KeHoachRepository,
            IQLTD_KeHoachService QLTD_KeHoachService,
            IDuAnRepository DuAnRepository,
            IUnitOfWork unitOfWork)
        {
            this._QLTD_KhoKhanVuongMacRepository = qltd_KhoKhanVuongMacRepository;
            this._IQLTD_KeHoachRepository = qltd_KeHoachRepository;
            this._QLTD_KeHoachService = QLTD_KeHoachService;
            this._unitOfWork = unitOfWork;
            _DuAnRepository = DuAnRepository;
        }

        public QLTD_KhoKhanVuongMac Add(QLTD_KhoKhanVuongMac qltd_KhoKhanVuongMac)
        {
            return _QLTD_KhoKhanVuongMacRepository.Add(qltd_KhoKhanVuongMac);
        }

        public QLTD_KhoKhanVuongMac Delete(int id)
        {
            return _QLTD_KhoKhanVuongMacRepository.Delete(id);
        }

        public IEnumerable<QLTD_KhoKhanVuongMac> GetByIdKeHoach(int idKH)
        {
            return _QLTD_KhoKhanVuongMacRepository.GetQLTD_KhoKhanVuongMac().Where(x => x.IdKeHoach == idKH);
        }

        public QLTD_KhoKhanVuongMac GetById(int id)
        {
            return _QLTD_KhoKhanVuongMacRepository.GetSingleByCondition(x => x.IdKhoKhanVuongMac == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_KhoKhanVuongMac qltd_KhoKhanVuongMac)
        {
            _QLTD_KhoKhanVuongMacRepository.Update(qltd_KhoKhanVuongMac);
        }

        public object GetKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser)
        {
            var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
            {
                IDKeHoach = x.IdKeHoach,
                IDDA = x.IdDuAn,
                IDGD = x.IdGiaiDoan
            });
            if (idNhomDuAnTheoUser == 13)
            {
                khPD = khPD.Where(w => w.IDGD == 6);
            }
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH).ToList();

            var datas = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => x.DaGiaiQuyet == false && grpKHPD.Contains(x.IdKeHoach), new string[] { "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.QLTD_GiaiDoan" })
                .Where(x => x.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0).GroupBy(x => new { x.QLTD_KeHoach.IdDuAn, x.QLTD_KeHoach.DuAn.TenDuAn }).Select(s => new
                {
                    IdDuAn = s.Key.IdDuAn.ToString(),
                    TenDuAn = s.Key.TenDuAn.ToString(),
                    grpGiaiDoan = s.GroupBy(g => new
                    {
                        g.IdKeHoach,
                        g.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan
                    }).Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        total = y.Count()
                    }).ToList()
                }).ToList();

            return datas;
        }

        public IEnumerable<BC_KhoKhanVuongMac> GetBCKhoKhanDieuHanh(string userId, int idNhomDuAnTheoUser, Dictionary<string, string> lstUserNhanVien)
        {
            var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
            {
                IDKeHoach = x.IdKeHoach,
                IDDA = x.IdDuAn,
                IDGD = x.IdGiaiDoan
            });
            if (idNhomDuAnTheoUser == 13)
            {
                khPD = khPD.Where(w => w.IDGD == 6);
            }
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH).ToList();

            var query = _QLTD_KhoKhanVuongMacRepository.GetMulti(x =>
            ((x.DaGiaiQuyet == false)
            && (grpKHPD.Contains(x.IdKeHoach))
            && (x.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0))
            , new string[] { "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.QLTD_GiaiDoan" })
            .Select(x => new
            {
                IDDA = x.QLTD_KeHoach.IdDuAn,
                TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                IdPhongQuanLy = x.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                {
                    ID = u.UserId,
                    Name = lstUserNhanVien[u.UserId]
                }).ToList().Distinct(),
                IDKH = x.IdKeHoach,
                IDGD = x.QLTD_KeHoach.IdGiaiDoan,
                TenGD = x.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan
            }).ToList();

            var datas = query.GroupBy(x => new { x.IdPhongQuanLy })
                .Select(x => new BC_KhoKhanVuongMac()
                {
                    IDPhong = x.Key.IdPhongQuanLy,
                    grpDA = x.GroupBy(y => new { y.IDDA, y.TenDuAn }).Select(y => new BC_KhoKhanVuongMac_DuAn()
                    {
                        IDDA = y.Key.IDDA,
                        TenDuAn = y.Key.TenDuAn,
                        Users = y.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        grpGiaiDoan = y.GroupBy(z => new { z.IDGD, z.TenGD }).Select(z => new BC_KhoKhanVuongMac_GiaiDoan()
                        {
                            IDGD = z.Key.IDGD,
                            TenGiaiDoan = z.Key.TenGD,
                            grpKH = z.Select(z1 => new BC_KhoKhanVuongMac_Kehoach()
                            {
                                IDKH = z1.IDKH
                            })
                        })
                    })
                });

            return datas;
        }


        // Duy
        public object GetCountKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser)
        {
            var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
            {
                IDKeHoach = x.IdKeHoach,
                IDDA = x.IdDuAn,
                IDGD = x.IdGiaiDoan
            });
            if (idNhomDuAnTheoUser == 13)
            {
                khPD = khPD.Where(w => w.IDGD == 6);
            }
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH);

            var datas = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => x.DaGiaiQuyet == false && grpKHPD.Contains(x.IdKeHoach), new string[] { "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.QLTD_GiaiDoan" })
                .Where(x => x.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0).GroupBy(x => new { x.QLTD_KeHoach.IdDuAn, x.QLTD_KeHoach.DuAn.TenDuAn }).Select(s => new
                {
                    IdDuAn = s.Key.IdDuAn.ToString(),
                    TenDuAn = s.Key.TenDuAn.ToString(),
                });
            var count = datas.Count();
            return count;
        }
        public object GetPageKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser, int Numberpage, int page, string filter, Dictionary<string, string> dicUser)
        {
            var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
            {
                IDKeHoach = x.IdKeHoach,
                IDDA = x.IdDuAn,
                IDGD = x.IdGiaiDoan
            });
            if (idNhomDuAnTheoUser == 13)
            {
                khPD = khPD.Where(w => w.IDGD == 6);
            }
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH);

            var datas = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => x.DaGiaiQuyet == false && grpKHPD.Contains(x.IdKeHoach),
                new string[] { "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers",
                    "QLTD_KeHoach.QLTD_GiaiDoan", "QLTD_KeHoach.QLTD_KeHoachTienDoChungs" })
                .Where(x => x.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0).GroupBy(x => new { x.QLTD_KeHoach.IdDuAn, x.QLTD_KeHoach.DuAn.TenDuAn, x.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser, x.QLTD_KeHoach.DuAn.DuAnUsers }).Select(s => new
                {
                    IdDuAn = s.Key.IdDuAn.ToString(),
                    TenDuAn = s.Key.TenDuAn.ToString(),
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = string.Join("; ", dicUser.Keys.Select(x => ((s.Key.DuAnUsers.ToList().
                     Select(y => y.UserId)).Where(a => a == x).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                    grpGiaiDoan = s.GroupBy(g => new
                    {
                        g.IdKeHoach,
                        g.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                    }).Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        NoiDung = y.Select(z=>z.QLTD_KeHoach.QLTD_KeHoachTienDoChungs.First().NoiDung),
                        total = y.Count()
                    })
                });
            if (filter != null)
            {
                datas = datas.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            var results = (datas.OrderBy(x => x.IdDuAn)
             .Skip((page - 1) * Numberpage)
             .Take(Numberpage));
            Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
            dictionary2.Add("CurrentPage", page);
            dictionary2.Add("TotalItemPages", results.Count());
            dictionary2.Add("TotalPages", datas.Count() % Numberpage > 0 ? ((datas.Count() / Numberpage) + 1) : (datas.Count() / Numberpage));
            dictionary2.Add("TotalItems", datas.Count());

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("Data", results);
            dictionary.Add("Page", dictionary2);
            return dictionary;
            // return datas;
        }

        public object GetPageTienDoChung(string userId, int idNhomDuAnTheoUser, int Numberpage, int page, string filter, Dictionary<string, string> dicUser)
        {
            var khPD = _IQLTD_KeHoachRepository.GetAll().Select(x => new
            {
                IDKeHoach = x.IdKeHoach,
                IDDA = x.IdDuAn,
                IDGD = x.IdGiaiDoan
            });
            if (idNhomDuAnTheoUser == 13)
            {
                khPD = khPD.Where(w => w.IDGD == 6);
            }
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH);

            var datas = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => grpKHPD.Contains(x.IdKeHoach),
                new string[] { "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers",
                    "QLTD_KeHoach.QLTD_GiaiDoan", "QLTD_KeHoach.QLTD_KeHoachTienDoChungs" })
                .Where(x => x.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0).GroupBy(x => new { x.QLTD_KeHoach.IdDuAn, x.QLTD_KeHoach.DuAn.TenDuAn, x.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser, x.QLTD_KeHoach.DuAn.DuAnUsers }).Select(s => new
                {
                    IdDuAn = s.Key.IdDuAn.ToString(),
                    TenDuAn = s.Key.TenDuAn.ToString(),
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = string.Join("; ", dicUser.Keys.Select(x => ((s.Key.DuAnUsers.ToList().
                     Select(y => y.UserId)).Where(a => a == x).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                    grpGiaiDoan = s.GroupBy(g => new
                    {
                        g.IdKeHoach,
                        g.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                    }).Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        NoiDung = y.Select(z => z.QLTD_KeHoach.QLTD_KeHoachTienDoChungs.First().NoiDung),
                        total = y.Count()
                    })
                });
            if (filter != null)
            {
                datas = datas.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            var results = (datas.OrderBy(x => x.IdDuAn)
             .Skip((page - 1) * Numberpage)
             .Take(Numberpage));
            Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
            dictionary2.Add("CurrentPage", page);
            dictionary2.Add("TotalItemPages", results.Count());
            dictionary2.Add("TotalPages", datas.Count() % Numberpage > 0 ? ((datas.Count() / Numberpage) + 1) : (datas.Count() / Numberpage));
            dictionary2.Add("TotalItems", datas.Count());

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("Data", results);
            dictionary.Add("Page", dictionary2);
            return dictionary;
            // return datas;
        }
        public IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_LCTDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            if (loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 2, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 2);
            }
            var lst = Get_NoiDung_KK_GP_For_BaoCao(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_CBDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            if (loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 3);
            }
            var lst = Get_NoiDung_KK_GP_For_BaoCao(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_CBTH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            if (loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 4, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 4);
            }
            var lst = Get_NoiDung_KK_GP_For_BaoCao(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP> Get_KK_GP_TH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            if (loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 5);
            }

            var lst = Get_NoiDung_KK_GP_For_BaoCao(idskehoach);
            return lst;
        }

        public IEnumerable<BC_TienDo_KK_QT> Get_KK_GP_QT_For_BaoCao_TienDo(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 6);

            var query = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => ((idskehoach.Contains(x.IdKeHoach)) && (x.DaGiaiQuyet == false))).ToList();

            var lst = query.GroupBy(x => new { x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_TienDo_KK_QT()
            {
                IDDA = x.Key.IdDuAn,
                grpKKVM = x.Select(y => new BC_TienDo_KK_QT_NoiDung()
                {
                    KKVM = y.NguyenNhanLyDo + " , " + y.GiaiPhapTrienKhai + "; "
                })
            });
            var a = lst.ToList();
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP> Get_NoiDung_KK_GP_For_BaoCao(IEnumerable<int> idskehoach)
        {
            var query = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => ((idskehoach.Contains(x.IdKeHoach)) && (x.DaGiaiQuyet == false))).ToList();

            var lst = query.GroupBy(x => new { x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_Uyban_PL1_KKGP()
            {
                IDDA = x.Key.IdDuAn,
                KK = x.Select(y => y.NguyenNhanLyDo),
                GP = x.Select(y => y.GiaiPhapTrienKhai),
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP> Get_NoiDung_KK_GP_For_BaoCao(int idkh)
        {
            var query = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => ((x.IdKeHoach == idkh) && (x.DaGiaiQuyet == false))).ToList();

            var lst = query.GroupBy(x => new { x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_Uyban_PL1_KKGP()
            {
                IDDA = x.Key.IdDuAn,
                KK = x.Select(y => y.NguyenNhanLyDo),
                GP = x.Select(y => y.GiaiPhapTrienKhai),
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KKGP_New> Get_NoiDung_KK_GP_For_BaoCao_New()
        {
            var lst = _QLTD_KhoKhanVuongMacRepository.GetMulti(x => x.DaGiaiQuyet == false)
                .GroupBy(x => new { x.IdKeHoach, x.QLTD_KeHoach.IdDuAn })
                .Select(x => new BC_Uyban_PL1_KKGP_New()
            {
                    IDKH = x.Key.IdKeHoach,
                    IDDA = x.Key.IdDuAn,
                    KK = x.Select(y => y.NguyenNhanLyDo),
                    GP = x.Select(y => y.GiaiPhapTrienKhai),
            });

            return lst;
        }

        public object GetGroupAllKeHoachByDuAn(string userId, int Numberpage, int page, string filter, Dictionary<string, string> dicUser)
        {
            var listDuAn = _DuAnRepository.GetMulti(x => x.DuAnUsers.Where(y => y.UserId == userId).Count() > 0,new string[] { "QLTD_KeHoachs", "DuAnUsers",
                "QLTD_KeHoachs.QLTD_KeHoachTienDoChungs", "QLTD_KeHoachs.QLTD_GiaiDoan" }).Where(x=>x.QLTD_KeHoachs.Count() > 0).ToList();
            var listGroup = listDuAn.Select(s => new
            {
                s.IdDuAn,
                s.TenDuAn,
                s.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(x => ((s.DuAnUsers.ToList().
                 Select(y => y.UserId)).Where(a => a == x).FirstOrDefault()))
                   .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpGiaiDoan = s.QLTD_KeHoachs
               .Select(y => new
               {
                   IDKH = y.IdKeHoach,
                   TenGiaiDoan = y.QLTD_GiaiDoan.TenGiaiDoan,
                   NoiDung = string.Join("", y.QLTD_KeHoachTienDoChungs.Where(a => a.NoiDung != null && a.NoiDung != "").Select(a => (a.CreatedDate != null ? ("(" + Convert.ToDateTime(a.CreatedDate).ToString("dd/MM/yyyy HH:mm") + ") - ") : "") + a.NoiDung)),
                   Ngay = y.NgayTinhTrang,
                   GiaiDoan = y.IdGiaiDoan,
                   total = s.QLTD_KeHoachs.Count()
               }).Where(t=> t.Ngay != null).OrderByDescending(h=> h.Ngay).OrderBy(u=>u.GiaiDoan)
            }).ToList();
            if (filter != null)
            {
                listGroup = listGroup.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower())).ToList();
            }
            var results = (listGroup.OrderBy(x => x.IdDuAn)
             .Skip((page - 1) * Numberpage)
             .Take(Numberpage));
            Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
            dictionary2.Add("CurrentPage", page);
            dictionary2.Add("TotalItemPages", results.Count());
            dictionary2.Add("TotalPages", listGroup.Count() % Numberpage > 0 ? ((listGroup.Count() / Numberpage) + 1) : (listGroup.Count() / Numberpage));
            dictionary2.Add("TotalItems", listGroup.Count());

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            dictionary.Add("Data", results);
            dictionary.Add("Page", dictionary2);
            return dictionary;
        }
        public object GetCountKeHoachByDuAn(string userId)
        {
            var listDuAn = _DuAnRepository.GetAll(new string[] { "QLTD_KeHoachs", "DuAnUsers",
                "QLTD_KeHoachs.QLTD_KeHoachTienDoChungs", "QLTD_KeHoachs.QLTD_GiaiDoan" }).Where(x => x.QLTD_KeHoachs.Count() > 0 && x.DuAnUsers.Where(y => y.UserId == userId).Count() > 0);
            var count = listDuAn.Count();
            return count;
        }
    }
}
