﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.KeToanModel;
using Model.Models.BCModel.BaocaoTiendo;
using Model.Models.DieuHanhModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_KeHoachService
    {
        QLTD_KeHoach Add(QLTD_KeHoach qltd_KeHoach);

        void Update(QLTD_KeHoach qltd_KeHoach);

        QLTD_KeHoach Delete(int id);

        IEnumerable<QLTD_KeHoach> GetAll(int idDuAn, int? idNhomDuAnTheoUser, int idGiaiDoan, string filter);

        IEnumerable<QLTD_KeHoach> GetAll(string filter);

        IEnumerable<QLTD_KeHoach> GetByFilter(int idDA, int? idNhomDuAnTheoUser, int idGiaiDoan, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_KeHoach GetById(int id);

        IEnumerable<QLTD_KeHoach> GetAll();

        IEnumerable<BanLVKeHoachMoiNhat> Get_KeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter, Dictionary<string, string> dicUser);

        IEnumerable<BanLVKeHoachMoiNhat> Get_CountKeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter);
        IEnumerable<BCKeHoachTrienKhaiDuAn> Get_BC_KeHoachTrienKhaiCacDuAn(string IDDuAns, Dictionary<string, string> dicUser);

        //IEnumerable<BC_TienDo_LCTDT> Get_KH_For_BC_TienDo(string IDDuAns);

        IEnumerable<BC_TienDo_LCTDT_ThoiGianHoanThanh> Get_For_BaoCao_TienDo_LCTDT_ThoiGianHoanThanh(string IDDuAns);
        IEnumerable<BC_TienDo_LCTDT_NgayGiaoNhiemVu> Get_For_BaoCao_TienDo_LCTDT_NgayGiaoNhiemVu(string IDDuAns);
        IEnumerable<BC_TienDo_LCTDT_ThongTin_LCTDT> Get_For_BaoCao_TienDo_LCTDT_ThongTinLCTDT(string IDDuAns);
        IEnumerable<KeToanModel> GetKPPheDuyet();
        IEnumerable<int> Get_IdsKeHoach(string[] arSz, int iGD, int iTT);
        IEnumerable<int> Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(string[] arSz, int iGD);

        string Get_NoiDungCV_For_BC_ThucHien_KeHoachTrienKhaiDA(int IDKH, int IDCV, int dDim);
        void Save();
    }

    public class QLTD_KeHoachService : IQLTD_KeHoachService
    {
        private IQLTD_KeHoachRepository _QLTD_KeHoachRepository;
        private IQLTD_CTCV_ChuTruongDauTuRepository _QLTD_CTCV_ChuTruongDauTuRepository;
        private IQLTD_CTCV_ChuanBiDauTuRepository _QLTD_CTCV_ChuanBiDauTuRepository;
        private IQLTD_CTCV_ChuanBiThucHienRepository _QLTD_CTCV_ChuanBiThucHienRepository;
        private IQLTD_CTCV_ThucHienDuAnRepository _QLTD_CTCV_ThucHienDuAnRepository;
        private IQLTD_CTCV_QuyetToanDuAnRepository _QLTD_CTCV_QuyetToanDuAnRepository;
        private IQLTD_KeHoachCongViecRepository _QLTD_KeHoachCongViecRepository;
        private IQLTD_TDTH_ChuTruongDauTuRepository _QLTD_TDTH_ChuTruongDauTuRepository;
        private IQLTD_TDTH_ChuanBiDauTuRepository _QLTD_TDTH_ChuanBiDauTuRepository;
        private IQLTD_TDTH_ChuanBiThucHienRepository _QLTD_TDTH_ChuanBiThucHienRepository;
        private IQLTD_TDTH_ThucHienDuAnRepository _QLTD_TDTH_ThucHienDuAnRepository;
        private IQLTD_TDTH_QuyetToanDuAnRepository _QLTD_TDTH_QuyetToanDuAnRepository;
        private IQLTD_KeHoachTienDoChungRepository _QLTD_KeHoachTienDoChungRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_KeHoachService(IQLTD_KeHoachRepository qltd_KeHoachRepository,
            IQLTD_CTCV_ChuTruongDauTuRepository QLTD_CTCV_ChuTruongDauTuRepository,
            IQLTD_CTCV_ChuanBiDauTuRepository QLTD_CTCV_ChuanBiDauTuRepository,
            IQLTD_CTCV_ChuanBiThucHienRepository QLTD_CTCV_ChuanBiThucHienRepository,
            IQLTD_CTCV_ThucHienDuAnRepository QLTD_CTCV_ThucHienDuAnRepository,
            IQLTD_CTCV_QuyetToanDuAnRepository QLTD_CTCV_QuyetToanDuAnRepository,
            IQLTD_KeHoachCongViecRepository QLTD_KeHoachCongViecRepository,
            IQLTD_TDTH_ChuTruongDauTuRepository QLTD_TDTH_ChuTruongDauTuRepository,
            IQLTD_TDTH_ChuanBiDauTuRepository QLTD_TDTH_ChuanBiDauTuRepository,
            IQLTD_TDTH_ChuanBiThucHienRepository QLTD_TDTH_ChuanBiThucHienRepository,
            IQLTD_TDTH_ThucHienDuAnRepository QLTD_TDTH_ThucHienDuAnRepository,
            IQLTD_TDTH_QuyetToanDuAnRepository QLTD_TDTH_QuyetToanDuAnRepository,
            IQLTD_KeHoachTienDoChungRepository QLTD_KeHoachTienDoChungRepository,
            IUnitOfWork unitOfWork)
        {
            this._QLTD_KeHoachRepository = qltd_KeHoachRepository;
            this._QLTD_CTCV_ChuTruongDauTuRepository = QLTD_CTCV_ChuTruongDauTuRepository;
            this._QLTD_CTCV_ChuanBiDauTuRepository = QLTD_CTCV_ChuanBiDauTuRepository;
            this._QLTD_CTCV_ChuanBiThucHienRepository = QLTD_CTCV_ChuanBiThucHienRepository;
            this._QLTD_CTCV_ThucHienDuAnRepository = QLTD_CTCV_ThucHienDuAnRepository;
            this._QLTD_CTCV_QuyetToanDuAnRepository = QLTD_CTCV_QuyetToanDuAnRepository;
            this._QLTD_KeHoachCongViecRepository = QLTD_KeHoachCongViecRepository;
            this._QLTD_TDTH_ChuTruongDauTuRepository = QLTD_TDTH_ChuTruongDauTuRepository;
            this._QLTD_TDTH_ChuanBiDauTuRepository = QLTD_TDTH_ChuanBiDauTuRepository;
            this._QLTD_TDTH_ChuanBiThucHienRepository = QLTD_TDTH_ChuanBiThucHienRepository;
            this._QLTD_TDTH_ThucHienDuAnRepository = QLTD_TDTH_ThucHienDuAnRepository;
            this._QLTD_TDTH_QuyetToanDuAnRepository = QLTD_TDTH_QuyetToanDuAnRepository;
            this._QLTD_KeHoachTienDoChungRepository = QLTD_KeHoachTienDoChungRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_KeHoach Add(QLTD_KeHoach qltd_KeHoach)
        {
            return _QLTD_KeHoachRepository.Add(qltd_KeHoach);
        }

        public QLTD_KeHoach Delete(int id)
        {
            _QLTD_KeHoachTienDoChungRepository.DeleteMulti(x => x.IdKeHoach == id);
            _QLTD_KeHoachCongViecRepository.DeleteMulti(x => x.IdKeHoach == id);
            return _QLTD_KeHoachRepository.Delete(id);
        }

        public IEnumerable<QLTD_KeHoach> GetAll(int idDuAn, int? idNhomDuAnTheoUser, int idGiaiDoan, string filter)
        {
            var query = _QLTD_KeHoachRepository.GetQLTD_KeHoach();
            //if(idNhomDuAnTheoUser != null)
            //{
            //    query = query.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
            //}
            if(idGiaiDoan != -1)
            {
                if (filter != null)
                {
                    query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan)
                        .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang);
                }
                else
                {
                    query = query.Where(x => x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan)
                        .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang); ;
                }
                return query;
            }
            else
            {
                if (filter != null)
                {
                    query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.IdDuAn == idDuAn)
                        .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang);
                }
                else
                {
                    query = query.Where(x => x.IdDuAn == idDuAn)
                        .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang); ;
                }
                return query;
            }
           
        }

        public IEnumerable<QLTD_KeHoach> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _QLTD_KeHoachRepository.GetMulti(x => x.NoiDung.Contains(filter));
            }
            return _QLTD_KeHoachRepository.GetQLTD_KeHoach();
        }

        public IEnumerable<QLTD_KeHoach> GetAll()
        {
            return _QLTD_KeHoachRepository.GetQLTD_KeHoach();
        }

        public IEnumerable<KeToanModel> GetKPPheDuyet()
        {
            var chuTruongDauTu = _QLTD_CTCV_ChuTruongDauTuRepository.GetMulti(x => x.IdCongViec == 14 && x.NgayHoanThanh != null).Select(x => new
            {
                ID = x.IdKeHoach,
                SoQD = x.PQCTDT_SoQD + (x.PQCTDT_NgayPheDuyetTT.ToString() != "" ? ", " + x.PQCTDT_NgayPheDuyetTT?.ToString("dd/MM/yyyy") : ""),
                GiaTri = x.PQCTDT_GiaTri
            }).GroupBy(x => x.ID).Select(y => y.First()).ToDictionary(x => x.ID, x => new PheDuyetInVewModel
            {
                SoQD = x.SoQD,
                GiaTri = x.GiaTri
            });
            var chuanBiDauTu = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => x.IdCongViec == 27 && x.NgayHoanThanh != null).Select(x => new
            {
                ID = x.IdKeHoach,
                SoQD = x.PDDA_BCKTKT_SoQD + (x.PDDA_BCKTKT_NgayPD.ToString() != "" ? ", " + x.PDDA_BCKTKT_NgayPD?.ToString("dd/MM/yyyy") : ""),
                GiaTri = x.PDDA_BCKTKT_GiaTri
            }).GroupBy(x => x.ID).Select(y => y.First()).ToDictionary(x => x.ID, x => new PheDuyetInVewModel
            {
                SoQD = x.SoQD,
                GiaTri = x.GiaTri
            });
            var chuanBiThucHien = _QLTD_CTCV_ChuanBiThucHienRepository.GetMulti(x => x.IdCongViec == 33 && x.NgayHoanThanh != null).Select(x => new
            {
                ID = x.IdKeHoach,
                SoQD = x.PDDA_SoQuyetDinh + (x.PDDA_NgayPheDuyet.ToString() != "" ? ", " + x.PDDA_NgayPheDuyet?.ToString("dd/MM/yyyy") : ""),
                GiaTri = x.PDDA_GiaTri
            }).GroupBy(x => x.ID).Select(y => y.First()).ToDictionary(x => x.ID, x => new PheDuyetInVewModel
            {
                SoQD = x.SoQD,
                GiaTri = x.GiaTri
            });

            var lstKeHoachPheDuyet = _QLTD_KeHoachRepository.GetMulti(x => x.TinhTrang == 4 && x.IdGiaiDoan < 5).Select(x => new
            {
                x.IdDuAn,
                x.IdGiaiDoan,
                x.IdKeHoach,
                x.UpdatedDate
            }).GroupBy(x => x.IdDuAn).Select(x => new
            {
                x.Key,
                KH_CTDT = (x.Where(y => y.IdGiaiDoan == 2).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()) != null ? (x.Where(y => y.IdGiaiDoan == 2).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()).IdKeHoach : 0,
                KH_CBDT = (x.Where(y => y.IdGiaiDoan == 3).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()) != null ? (x.Where(y => y.IdGiaiDoan == 3).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()).IdKeHoach : 0,
                KH_CBTH = (x.Where(y => y.IdGiaiDoan == 4).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()) != null ? (x.Where(y => y.IdGiaiDoan == 4).Select(yy => new
                {
                    yy.IdKeHoach,
                    yy.UpdatedDate
                }).OrderByDescending(yyy => yyy.UpdatedDate).FirstOrDefault()).IdKeHoach : 0
                }
                ).Select(a => new KeToanModel
                {
                    IdDuAn = a.Key,
                    CTDT = chuTruongDauTu.ContainsKey(a.KH_CTDT) ? chuTruongDauTu[a.KH_CTDT] : null,
                    CBDT = chuanBiDauTu.ContainsKey(a.KH_CBDT) ? chuanBiDauTu[a.KH_CBDT] : null,
                    CBTH = chuanBiThucHien.ContainsKey(a.KH_CBTH) ? chuanBiThucHien[a.KH_CBTH] : null
                });

            return lstKeHoachPheDuyet;
        }


        public IEnumerable<QLTD_KeHoach> GetByFilter(int idDuAn, int? idNhomDuAnTheoUser, int idGiaiDoan, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_KeHoachRepository.GetQLTD_KeHoach();
            if (filter != null)
            {
                //query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan);
            }
            else
            {
                //query = query.Where(x => x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
                query = query.Where(x => x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdKeHoach).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public QLTD_KeHoach GetById(int id)
        {
            return _QLTD_KeHoachRepository.GetSingleByCondition(x => x.IdKeHoach == id, new string[] { "QLTD_KeHoachCongViecs", "DuAn" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_KeHoach qltd_KeHoach)
        {
            _QLTD_KeHoachRepository.Update(qltd_KeHoach);
        }

        public IEnumerable<BanLVKeHoachMoiNhat> Get_KeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter, Dictionary<string, string> dicUser)
        {
            

            var qltd_KeHoachCongViec = _QLTD_KeHoachCongViecRepository.GetAll().Select(x => new QLTD_KeHoachCongViec
            {
                IdKeHoach = x.IdKeHoach,
                IdKeHoachCongViec = x.IdKeHoachCongViec,
                IdCongViec = x.IdCongViec
            }).ToList();

            var lstKH = _QLTD_KeHoachRepository.GetAll(new[] { "DuAn.DuAnUsers" }).Select(x => new {
                IdGiaiDoan = x.IdGiaiDoan,
                IdKeHoach = x.IdKeHoach,
                NoiDung = x.NoiDung,
                IdDuAn = x.IdDuAn,
                TenDuAn = x.DuAn.TenDuAn,
                TinhTrang = x.TinhTrang,
                TrangThai = x.TrangThai,
                QLTD_KeHoachCongViecs = qltd_KeHoachCongViec.Where(y => y.IdKeHoach == x.IdKeHoach).ToList(),
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = x.DuAn.DuAnUsers,
                NameUser = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAn.DuAnUsers.
                 Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
            }).ToList();

            var query = lstKH.Where(x => x.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0);
            if (filter != null)
            {
                query = query.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            IEnumerable<int> idskehoach;
            if (idNhomDuAnTheoUser != 13)
            {
                idskehoach = lstKH.Select(y => new
                {
                    IdKeHoach = y.IdKeHoach,
                    IdGiaiDoan = y.IdGiaiDoan,
                    IdDuAn = y.IdDuAn,
                }).GroupBy(x => new { x.IdDuAn, x.IdGiaiDoan }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());
            }
            else
            {
                idskehoach = lstKH.Where(w => w.IdGiaiDoan == 6).Select(y => new
                {
                    IdKeHoach = y.IdKeHoach,
                    IdGiaiDoan = y.IdGiaiDoan,
                    IdDuAn = y.IdDuAn
                }).GroupBy(x => new { x.IdDuAn, x.IdGiaiDoan }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());
            }

            var lst = query.Where(x => idskehoach.Contains(x.IdKeHoach) && x.TrangThai < 4 && x.TrangThai == nTrangThai)
                .Select(x => new BanLVKeHoachMoiNhat()
                {
                    IDGiaiDoan = x.IdGiaiDoan,
                    IDKH = x.IdKeHoach,
                    KH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    DuAn = x.TenDuAn,
                    TinhTrang = x.TinhTrang,
                    TrangThai = x.TrangThai,
                    qltd_kehoachcongviec = x.QLTD_KeHoachCongViecs,
                    IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                    DuAnUsers = x.NameUser
                });

            return lst;
        }

        public IEnumerable<BCKeHoachTrienKhaiDuAn> Get_BC_KeHoachTrienKhaiCacDuAn(string IDDuAns, Dictionary<string, string> lstUserNhanVien)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            IEnumerable<int> idskehoach;

            idskehoach = _QLTD_KeHoachRepository.GetAll().Where(x => x.TinhTrang == 4 && x.TrangThai == 4 && arrDuAn.Contains(x.IdDuAn.ToString())).Select(y => new
            {
                IdKeHoach = y.IdKeHoach,
                IdGiaiDoan = y.IdGiaiDoan,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn, x.IdGiaiDoan }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());

            var lstCTDT = _QLTD_CTCV_ChuTruongDauTuRepository.
                GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_CongViec", "QLTD_CongViec.QLTD_GiaiDoan", "QLTD_KeHoachCongViec", "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.DuAn.NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDDA = x.QLTD_KeHoach.IdDuAn,
                    Order = x.QLTD_KeHoach.DuAn.Order,
                    OrderTemp = x.QLTD_KeHoach.DuAn.OrderTemp,
                    TenPhong = x.QLTD_KeHoach.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                    IDNhomGiaiDoan = 1,
                    TenNhomGiaiDoan = "Chuẩn bị đầu tư",
                    TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                    IDGiaiDoan = x.QLTD_KeHoach.IdGiaiDoan,
                    TenGiaiDoan = x.QLTD_CongViec.QLTD_GiaiDoan.TenGiaiDoan,
                    TenCongViec = x.QLTD_CongViec.TenCongViec,
                    TuNgay = x.QLTD_KeHoachCongViec.TuNgay,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay,
                    IDCV = x.IdCongViec,
                    HoanTHanh = x.HoanThanh,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            var lstCBDT = _QLTD_CTCV_ChuanBiDauTuRepository.
                GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_CongViec", "QLTD_CongViec.QLTD_GiaiDoan", "QLTD_KeHoachCongViec", "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.DuAn.NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDDA = x.QLTD_KeHoach.IdDuAn,
                    Order = x.QLTD_KeHoach.DuAn.Order,
                    OrderTemp = x.QLTD_KeHoach.DuAn.OrderTemp,
                    TenPhong = x.QLTD_KeHoach.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                    IDNhomGiaiDoan = 1,
                    TenNhomGiaiDoan = "Chuẩn bị đầu tư",
                    TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                    IDGiaiDoan = x.QLTD_KeHoach.IdGiaiDoan,
                    TenGiaiDoan = x.QLTD_CongViec.QLTD_GiaiDoan.TenGiaiDoan,
                    TenCongViec = x.QLTD_CongViec.TenCongViec,
                    TuNgay = x.QLTD_KeHoachCongViec.TuNgay,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay,
                    IDCV = x.IdCongViec,
                    HoanTHanh = x.HoanThanh,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            var lstCBTH = _QLTD_CTCV_ChuanBiThucHienRepository.
                GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_CongViec", "QLTD_CongViec.QLTD_GiaiDoan", "QLTD_KeHoachCongViec", "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.DuAn.NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDDA = x.QLTD_KeHoach.IdDuAn,
                    Order = x.QLTD_KeHoach.DuAn.Order,
                    OrderTemp = x.QLTD_KeHoach.DuAn.OrderTemp,
                    TenPhong = x.QLTD_KeHoach.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                    IDNhomGiaiDoan = 2,
                    TenNhomGiaiDoan = "Thực hiện đầu tư",
                    TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                    IDGiaiDoan = x.QLTD_KeHoach.IdGiaiDoan,
                    TenGiaiDoan = x.QLTD_CongViec.QLTD_GiaiDoan.TenGiaiDoan,
                    TenCongViec = x.QLTD_CongViec.TenCongViec,
                    TuNgay = x.QLTD_KeHoachCongViec.TuNgay,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay,
                    IDCV = x.IdCongViec,
                    HoanTHanh = x.HoanThanh,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            var lstTH = _QLTD_CTCV_ThucHienDuAnRepository.
                GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_CongViec", "QLTD_CongViec.QLTD_GiaiDoan", "QLTD_KeHoachCongViec", "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.DuAn.NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDDA = x.QLTD_KeHoach.IdDuAn,
                    Order = x.QLTD_KeHoach.DuAn.Order,
                    OrderTemp = x.QLTD_KeHoach.DuAn.OrderTemp,
                    TenPhong = x.QLTD_KeHoach.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                    IDNhomGiaiDoan = 2,
                    TenNhomGiaiDoan = "Thực hiện đầu tư",
                    TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                    IDGiaiDoan = x.QLTD_KeHoach.IdGiaiDoan,
                    TenGiaiDoan = x.QLTD_CongViec.QLTD_GiaiDoan.TenGiaiDoan,
                    TenCongViec = x.QLTD_CongViec.TenCongViec,
                    TuNgay = x.QLTD_KeHoachCongViec.TuNgay,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay,
                    IDCV = x.IdCongViec,
                    HoanTHanh = x.HoanThanh,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            var lstQT = _QLTD_CTCV_QuyetToanDuAnRepository.
                GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_CongViec", "QLTD_CongViec.QLTD_GiaiDoan", "QLTD_KeHoachCongViec", "QLTD_KeHoach", "QLTD_KeHoach.DuAn", "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoach.DuAn.NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDDA = x.QLTD_KeHoach.IdDuAn,
                    Order = x.QLTD_KeHoach.DuAn.Order,
                    OrderTemp = x.QLTD_KeHoach.DuAn.OrderTemp,
                    TenPhong = x.QLTD_KeHoach.DuAn.NhomDuAnTheoUser.TenNhomDuAn,
                    IDNhomGiaiDoan = 3,
                    TenNhomGiaiDoan = "Kết thúc đầu tư",
                    TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                    IDGiaiDoan = x.QLTD_KeHoach.IdGiaiDoan,
                    TenGiaiDoan = x.QLTD_CongViec.QLTD_GiaiDoan.TenGiaiDoan,
                    TenCongViec = x.QLTD_CongViec.TenCongViec,
                    TuNgay = x.QLTD_KeHoachCongViec.TuNgay,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay,
                    IDCV = x.IdCongViec,
                    HoanTHanh = x.HoanThanh,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct(),
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            var lst = lstCTDT.Union(lstCBDT).Union(lstCBTH).Union(lstTH).Union(lstQT).OrderBy(x => x.IDDA);
            var lstAll = lst
                .GroupBy(g => new { g.IDDA, g.TenDuAn })
                .Select(s => new BCKeHoachTrienKhaiDuAn()
                {
                    IDDA = s.Key.IDDA,
                    TenDuAn = s.Key.TenDuAn,
                    OrderTemp = s.Select(x => x.OrderTemp).FirstOrDefault(),
                    Order = s.Select(x => x.Order).FirstOrDefault(),
                    TenPhong = s.Select(x => x.TenPhong).FirstOrDefault(),
                    Users = s.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                    grpNhomGiaiDoan = s.GroupBy(g1 => new { g1.IDNhomGiaiDoan, g1.TenNhomGiaiDoan }).Select(s1 => new BCNhomGiaiDoanTrienKhaiDA()
                    {
                        TenNhomGiaiDoan = s1.Key.TenNhomGiaiDoan,
                        grpGiaiDoan = s1.GroupBy(g2 => new { g2.IDGiaiDoan, g2.TenGiaiDoan }).Select(s2 => new BCGiaiDoanTrienKhaiDA()
                        {
                            IDGiaiDoan = s2.Key.IDGiaiDoan,
                            TenGiaiDoan = s2.Key.TenGiaiDoan,
                            grpCongViec = s2.Select(s3 => new BCCongViecTrienKhaiDA()
                            {
                                TenCongViec = s3.TenCongViec,
                                TuNgay = s3.TuNgay == null? "" : s3.TuNgay.ToString(),
                                DenNgay = s3.DenNgay == null? "" : s3.DenNgay.ToString(),
                                IDCV = s3.IDCV,
                                IDKH = s3.IDKH,
                                HoanThanh = s3.HoanTHanh
                            })
                        })
                    })
                }).OrderBy(x => x.OrderTemp).ThenBy(x => x.Order).ToList();

            return lstAll;
        }
        // Duy
        public IEnumerable<BanLVKeHoachMoiNhat> Get_CountKeHoachMoiNhat_DuoiMucGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter)
        {
            var query = _QLTD_KeHoachRepository.GetMulti(x => x.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0,
                new[] { "DuAn", "DuAn.DuAnUsers", "QLTD_KeHoachCongViecs" });
            if (filter != null)
            {
                query = query.Where(x => x.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            IEnumerable<int> idskehoach;


            if (idNhomDuAnTheoUser != 13)
            {
                idskehoach = _QLTD_KeHoachRepository.GetAll().Select(y => new
                {
                    IdKeHoach = y.IdKeHoach,
                    IdGiaiDoan = y.IdGiaiDoan,
                    IdDuAn = y.IdDuAn
                }).GroupBy(x => new { x.IdDuAn, x.IdGiaiDoan }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());
            }
            else
            {
                idskehoach = _QLTD_KeHoachRepository.GetMulti(w => w.IdGiaiDoan == 6).Select(y => new
                {
                    IdKeHoach = y.IdKeHoach,
                    IdGiaiDoan = y.IdGiaiDoan,
                    IdDuAn = y.IdDuAn
                }).GroupBy(x => new { x.IdDuAn, x.IdGiaiDoan }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());
            }

            var lst = query.Where(x => idskehoach.Contains(x.IdKeHoach) && x.TrangThai < 4 && x.TrangThai == nTrangThai)
                .Select(x => new BanLVKeHoachMoiNhat()
                {
                    IDGiaiDoan = x.IdGiaiDoan,
                    IDKH = x.IdKeHoach,
                    KH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    DuAn = x.DuAn.TenDuAn,
                    TinhTrang = x.TinhTrang,
                    TrangThai = x.TrangThai,
                    qltd_kehoachcongviec = x.QLTD_KeHoachCongViecs
                });

            return lst;
        }

        public IEnumerable<int> Get_IdsKeHoach(string[] arSz, int iGD, int iTT)
        {
            var idskehoach = _QLTD_KeHoachRepository.GetAll().Where(x => ((arSz.Contains(x.IdDuAn.ToString())) && (x.IdGiaiDoan == iGD) && (x.TinhTrang == iTT) && (x.TrangThai == iTT))).Select(y => new
            {
                IdKeHoach = y.IdKeHoach,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());

            return idskehoach.ToList();
        }

        public IEnumerable<BC_TienDo_LCTDT_ThoiGianHoanThanh> Get_For_BaoCao_TienDo_LCTDT_ThoiGianHoanThanh(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 2);

            var query = _QLTD_KeHoachCongViecRepository.GetMulti(x => idskehoach.Contains(x.IdKeHoach), new[] { "QLTD_KeHoach" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new BC_TienDo_LCTDT_ThoiGianHoanThanh()
                {
                    IDDA = x.Key.IdDuAn,
                    ThoiGianHoanThanh_Nam = x.Where(y => y.IdCongViec == 27).Select(z => z.DenNgay.Value.Year).FirstOrDefault()
                });

            return query;
        }


        public IEnumerable<BC_TienDo_LCTDT_NgayGiaoNhiemVu> Get_For_BaoCao_TienDo_LCTDT_NgayGiaoNhiemVu(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 2);

            var query = _QLTD_KeHoachRepository.GetMulti(x => idskehoach.Contains(x.IdKeHoach)).GroupBy(x => new { x.IdDuAn })
                .Select(x => new BC_TienDo_LCTDT_NgayGiaoNhiemVu()
                {
                    IDDA = x.Key.IdDuAn,
                    NgayGiaoNVu_LCTDT = x.Select(y => y.NgayGiaoLapChuTruong).FirstOrDefault()
                });

            return query;
        }

        public IEnumerable<BC_TienDo_LCTDT_ThongTin_LCTDT> Get_For_BaoCao_TienDo_LCTDT_ThongTinLCTDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 2);

            var query = _QLTD_CTCV_ChuTruongDauTuRepository.GetMulti(x => ((idskehoach.Contains(x.IdKeHoach)) && (x.QLTD_KeHoach.IdGiaiDoan == 2)), new[] { "QLTD_KeHoach", "QLTD_TDTH_ChuTruongDauTu" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new BC_TienDo_LCTDT_ThongTin_LCTDT()
                {
                    IDDA = x.Key.IdDuAn,
                    ChapThuanDD_CoSanDiaDiem = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_CoSanDiaDiem).FirstOrDefault(),
                    ChapThuanDD_SoVB = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_SoVB).FirstOrDefault(),
                    ChapThuanDD_NgayPD = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_NgayPD).FirstOrDefault(),
                    LCTDT_TienDoThucHien = x.Select(y => y.QLTD_TDTH_ChuTruongDauTu.Select(z => z.NoiDung)).FirstOrDefault(),
                    LCTDT_KKVM = x.Where(y => y.IdCongViec == 11).Select(y => y.LCTDT_KhoKhanVuongMac).FirstOrDefault(),
                    LCTDT_KNDX = x.Where(y => y.IdCongViec == 11).Select(y => y.LCTDT_KienNghiDeXuat).FirstOrDefault(),
                    NgayTrinhHS = x.Where(y => y.IdCongViec == 13).Select(y => y.THSPDDA_NgayTrinhThucTe).FirstOrDefault(),
                    PDCTDT_SoVB = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_SoQD).FirstOrDefault(),
                    PDCTDT_NgayPD = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_NgayLaySo).FirstOrDefault(),
                    PDCTDT_TongMucDauTu = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_GiaTri).FirstOrDefault(),
                    PDCTDT_NgayPDTT = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_NgayPheDuyetTT).FirstOrDefault()
                });

            return query;
        }
        
        public IEnumerable<int> Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(string[] arrDuAn, int iGD)
        {
            var idskehoach = _QLTD_KeHoachRepository.GetAll().Where(x => ((arrDuAn.Contains(x.IdDuAn.ToString()))&&(x.IdGiaiDoan == iGD))).Select(y => new
            {
                IdKeHoach = y.IdKeHoach,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());

            return idskehoach;
        }

        public string Get_NoiDungCV_For_BC_ThucHien_KeHoachTrienKhaiDA(int IDKH, int IDCV, int dDim)
        {
            string sz = "";
            
            #region Chủ trương đầu tư
            var qrCTDT = _QLTD_CTCV_ChuTruongDauTuRepository.GetMulti(x => (IDKH == x.IdKeHoach), new[] { "QLTD_KeHoach" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new
                {
                    //IDDA = x.Key.IdDuAn,
                    ChapThuanDD_CoSanDiaDiem = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_CoSanDiaDiem).FirstOrDefault(),
                    ChapThuanDD_SoVB = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_SoVB).FirstOrDefault(),
                    ChapThuanDD_NgayPD = x.Where(y => y.IdCongViec == 10).Select(y => y.CTDD_NgayPD).FirstOrDefault(),
                    NgayTrinhThucTe = x.Where(y => y.IdCongViec == 13).Select(y => y.THSPDDA_NgayTrinhThucTe).FirstOrDefault(),
                    SoQD = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_SoQD).FirstOrDefault(),
                    NgayLaySo = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_NgayLaySo).FirstOrDefault(),
                    GiaTri = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_GiaTri).FirstOrDefault(),
                    NgayPD = x.Where(y => y.IdCongViec == 14).Select(y => y.PQCTDT_NgayPheDuyetTT).FirstOrDefault()
                });

            if (IDCV == 10)
            {
                if(qrCTDT.Select(x => x.ChapThuanDD_CoSanDiaDiem).FirstOrDefault() == true)
                {
                    sz = "Đã có sẵn địa điểm";                    
                }
                else
                {
                    sz = "Số văn bản chấp thuận địa điểm: " + qrCTDT.Select(x => x.ChapThuanDD_SoVB).FirstOrDefault() 
                        + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCTDT.Select(x => x.ChapThuanDD_NgayPD).FirstOrDefault());
                }
            }

            if (IDCV == 11)
            {
                var query = _QLTD_TDTH_ChuTruongDauTuRepository.GetMulti(x => x.QLTD_CTCV_ChuTruongDauTu.IdKeHoach == IDKH, new string[] { "QLTD_CTCV_ChuTruongDauTu" })
                    .GroupBy(x => new { x.QLTD_CTCV_ChuTruongDauTu.IdKeHoach })
                    .Select(x => new
                    {
                        ND = x.Select(y => y.NoiDung)
                    });

                var qr = query.Select(x => x.ND).FirstOrDefault();

                if (qr!=null)
                {
                    foreach(var item in qr)
                    {
                        sz += item + ";";
                    }
                }
            }

            if (IDCV == 13)
            {
                sz = "Ngày trình thực tế: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCTDT.Select(x => x.NgayTrinhThucTe).FirstOrDefault());
            }

            if (IDCV == 14)
            {
                sz = "Số quyết định phê duyệt: " + qrCTDT.Select(x => x.SoQD).FirstOrDefault() 
                    + "; Ngày lấy số: " + qrCTDT.Select(x => x.NgayLaySo).FirstOrDefault()
                    + "; Giá trị: " + (qrCTDT.Select(x => x.GiaTri).FirstOrDefault() / dDim)
                    + "; Ngày phê duyệt thực tế: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCTDT.Select(x => x.NgayPD).FirstOrDefault());
            }
            #endregion

            #region Chuẩn bị đầu tư
            var qrCBDT = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => (IDKH == x.IdKeHoach), new[] { "QLTD_KeHoach", "QLTD_TDTH_ChuanBiDauTu" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new
                {
                    //BCPATK
                    BCPATK = x.Where(y => y.IdCongViec == 15).Select(y => y.BCPATK_NoiDung).FirstOrDefault(),
                    //PDDT
                    PDDT_SoQD = x.Where(y => y.IdCongViec == 16).Select(y => y.PDDT_SoQD).FirstOrDefault(),
                    PDDT_NgayPD = x.Where(y => y.IdCongViec == 16).Select(y => y.PDDT_NgayPQ).FirstOrDefault(),
                    PDDT_GiaTri = x.Where(y => y.IdCongViec == 16).Select(y => y.PDDT_GiaTri).FirstOrDefault(),
                    //PDKHLCNT
                    PDKHLCNT_SoQD = x.Where(y => y.IdCongViec == 17).Select(y => y.PDKHLCNT_SoQD).FirstOrDefault(),
                    PDKHLCNT_NgayPD = x.Where(y => y.IdCongViec == 17).Select(y => y.PDKHLCNT_NgayPD).FirstOrDefault(),
                    PDKHLCNT_GiaTri = x.Where(y => y.IdCongViec == 17).Select(y => y.PDKHLCNT_GiaTri).FirstOrDefault(),
                    //KSHT
                    KSHT_NoiDung = x.Where(y => y.IdCongViec == 18).Select(y => y.KSHT_NoiDung).FirstOrDefault(),
                    //CGDD
                    CGDD_NoiDung = x.Where(y => y.IdCongViec == 19).Select(y => y.CGDD_NoiDung).FirstOrDefault(),
                    //SLHTKT
                    SLHTKT_NoiDung = x.Where(y => y.IdCongViec == 20).Select(y => y.SLHTKT_NoiDung).FirstOrDefault(),
                    //PDQH
                    PDQH_KhongPheDuyet = x.Where(y => y.IdCongViec == 21).Select(y => y.PDQH_KhongPheDuyet).FirstOrDefault(),
                    PDQH_SoQD_QHCT = x.Where(y => y.IdCongViec == 21).Select(y => y.PDQH_SoQD_QHCT).FirstOrDefault(),
                    PDQH_NgayPD_QHCT = x.Where(y => y.IdCongViec == 21).Select(y => y.PDQH_NgayPD_QHCT).FirstOrDefault(),
                    PDQH_SoQD_QHTMB = x.Where(y => y.IdCongViec == 21).Select(y => y.PDQH_SoQD_QHTMB).FirstOrDefault(),
                    PDQH_NgayPD_QHTMB = x.Where(y => y.IdCongViec == 21).Select(y => y.PDQH_NgayPD_QHTMB).FirstOrDefault(),
                    //KSDC
                    KSDC_NoiDung = x.Where(y => y.IdCongViec == 22).Select(y => y.KSDCDH_NoiDung).FirstOrDefault(),
                    //PCCC
                    ThamDuyet_PCCC = x.Where(y => y.IdCongViec == 23).Select(y => y.TTTDPCCC_NoiDung).FirstOrDefault(),
                    //TTK
                    TTK = x.Where(y => y.IdCongViec == 24).Select(y => y.TTK_NoiDung).FirstOrDefault(),
                    //THS
                    THSCBDT_SoToTrinh = x.Where(y => y.IdCongViec == 26).Select(y => y.THSCBDT_SoToTrinh).FirstOrDefault(),
                    THSCBDT_NgayTrinh = x.Where(y => y.IdCongViec == 26).Select(y => y.THSCBDT_NgayTrinh).FirstOrDefault(),
                    //PDDA
                    PDDA_BCKTKT_SoQD = x.Where(y => y.IdCongViec == 27).Select(y => y.PDDA_BCKTKT_SoQD).FirstOrDefault(),
                    PDDA_BCKTKT_NgayPD = x.Where(y => y.IdCongViec == 27).Select(y => y.PDDA_BCKTKT_NgayPD).FirstOrDefault(),
                    PDDA_BCKTKT_GiaTri = x.Where(y => y.IdCongViec == 27).Select(y => y.PDDA_BCKTKT_GiaTri).FirstOrDefault()
                });

            if (IDCV == 15)
            {
                sz = "Báo cáo phương án thiết kế: " + qrCBDT.Select(x => x.BCPATK).FirstOrDefault();
            }

            if (IDCV == 16)
            {
                sz = "Số quyết định: " + qrCBDT.Select(x => x.PDDT_SoQD).FirstOrDefault() 
                    +"; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.PDDT_NgayPD).FirstOrDefault())
                    +"; Giá trị: " + (qrCBDT.Select(x => x.PDDT_GiaTri).FirstOrDefault() / dDim);
            }

            if (IDCV == 17)
            {
                sz = "Số quyết định: " + qrCBDT.Select(x => x.PDKHLCNT_SoQD).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.PDKHLCNT_NgayPD).FirstOrDefault())
                    + "; Giá trị: " + (qrCBDT.Select(x => x.PDKHLCNT_GiaTri).FirstOrDefault()/dDim);
            }

            if (IDCV == 18)
            {
                sz = "Nội dung: " + qrCBDT.Select(x => x.KSHT_NoiDung).FirstOrDefault();
            }

            if (IDCV == 19)
            {
                sz = "Nội dung: " + qrCBDT.Select(x => x.CGDD_NoiDung).FirstOrDefault();
            }

            if (IDCV == 20)
            {
                sz = "Nội dung: " + qrCBDT.Select(x => x.SLHTKT_NoiDung).FirstOrDefault();
            }

            if (IDCV == 21)
            {
                if (qrCBDT.Select(x => x.PDQH_KhongPheDuyet).FirstOrDefault() == true)
                {
                    sz = "Không phải phê duyệt quy hoạch";
                }
                else
                {
                    sz = "Số quyết định QHCT: " + qrCBDT.Select(x => x.PDQH_SoQD_QHCT).FirstOrDefault()
                        + "; Ngày phê duyệt QHCT: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.PDQH_NgayPD_QHCT).FirstOrDefault())
                        + "; Số quyết định QHTMB: " + qrCBDT.Select(x => x.PDQH_SoQD_QHTMB).FirstOrDefault()
                        + "; Ngày phê duyệt QHTMB: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.PDQH_NgayPD_QHTMB).FirstOrDefault());
                }

                //sz = "Nội dung: " + query.Select(x => x.ND).FirstOrDefault();
            }

            if (IDCV == 22)
            {
                sz = "Nội dung: " + qrCBDT.Select(x => x.KSDC_NoiDung).FirstOrDefault();
            }

            if (IDCV == 23)
            {
                sz = "Nội dung: " + qrCBDT.Select(x => x.ThamDuyet_PCCC).FirstOrDefault();
            }

            if (IDCV == 24)
            {
                sz = "Nội dung: " + qrCBDT.Select(y => y.TTK).FirstOrDefault();
            }

            if (IDCV == 25)
            {
                var query = _QLTD_TDTH_ChuanBiDauTuRepository.GetMulti(x => x.QLTD_CTCV_ChuanBiDauTu.IdKeHoach == IDKH, new string[] { "QLTD_CTCV_ChuanBiDauTu" })
                .GroupBy(x => new { x.QLTD_CTCV_ChuanBiDauTu.IdKeHoach })
                .Select(x => new
                {
                    ND = x.Select(y => y.NoiDung)
                });

                var qr = query.Select(x => x.ND).FirstOrDefault();
                if (qr != null)
                {
                    foreach (var item in qr)
                    {
                        sz += item + ";";
                    }
                }
            }

            if (IDCV == 26)
            {
                sz = "Số tờ trình: " + qrCBDT.Select(x => x.THSCBDT_SoToTrinh).FirstOrDefault()
                     + "; Ngày trình: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.THSCBDT_NgayTrinh).FirstOrDefault());
            }

            if (IDCV == 27)
            {
                sz = "Số quyết định: " + qrCBDT.Select(x => x.PDDA_BCKTKT_SoQD).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBDT.Select(x => x.PDDA_BCKTKT_NgayPD).FirstOrDefault())
                    + "; Giá trị: " + (qrCBDT.Select(x => x.PDDA_BCKTKT_GiaTri).FirstOrDefault()/dDim);
            }
            #endregion

            #region Chuẩn bị thực hiện
            var qrCBTH = _QLTD_CTCV_ChuanBiThucHienRepository.GetMulti(x => (IDKH == x.IdKeHoach), new[] { "QLTD_KeHoach" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new
                {
                    //PDDT
                    PDDT_SoQuyetDinh = x.Where(y => y.IdCongViec == 29).Select(y => y.PDDT_SoQuyetDinh).FirstOrDefault(),
                    PDDT_NgayPheDuyet = x.Where(y => y.IdCongViec == 29).Select(y => y.PDDT_NgayPheDuyet).FirstOrDefault(),
                    PDDT_GiaTri = x.Where(y => y.IdCongViec == 29).Select(y => y.PDDT_GiaTri).FirstOrDefault(),
                    //PD KHLCNT
                    PDKHLCNT_SoQuyetDinh = x.Where(y => y.IdCongViec == 30).Select(y => y.PDKHLCNT_SoQuyetDinh).FirstOrDefault(),
                    PDKHLCNT_NgayPheDuyet = x.Where(y => y.IdCongViec == 30).Select(y => y.PDKHLCNT_NgayPheDuyet).FirstOrDefault(),
                    PDKHLCNT_GiaTri = x.Where(y => y.IdCongViec == 30).Select(y => y.PDKHLCNT_GiaTri).FirstOrDefault(),
                    //THS
                    THS_SoToTrinh = x.Where(y => y.IdCongViec == 32).Select(y => y.THS_SoToTrinh).FirstOrDefault(),
                    THS_NgayTrinh = x.Where(y => y.IdCongViec == 32).Select(y => y.LHS_NgayTrinh).FirstOrDefault(),
                    //PDDA
                    PDDA_SoQuyetDinh = x.Where(y => y.IdCongViec == 33).Select(y => y.PDDA_SoQuyetDinh).FirstOrDefault(),
                    PDDA_NgayPheDuyet = x.Where(y => y.IdCongViec == 33).Select(y => y.PDDA_NgayPheDuyet).FirstOrDefault(),
                    PDDA_GiaTri = x.Where(y => y.IdCongViec == 33).Select(y => y.PDDA_GiaTri).FirstOrDefault(),
                });

            if (IDCV == 29)
            {
                sz = "Số quyết định: " + qrCBTH.Select(x => x.PDDT_SoQuyetDinh).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBTH.Select(x => x.PDDT_NgayPheDuyet).FirstOrDefault())
                    + "; Giá trị: " + (qrCBTH.Select(x => x.PDDT_GiaTri).FirstOrDefault() / dDim);

            }

            if (IDCV == 30)
            {
                sz = "Số quyết định: " + qrCBTH.Select(x => x.PDKHLCNT_SoQuyetDinh).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBTH.Select(x => x.PDKHLCNT_NgayPheDuyet).FirstOrDefault())
                    + "; Giá trị: " + (qrCBTH.Select(x => x.PDKHLCNT_GiaTri).FirstOrDefault() / dDim);
            }

            if (IDCV == 31)
            {
                var query = _QLTD_TDTH_ChuanBiThucHienRepository.GetMulti(x => x.QLTD_CTCV_ChuanBiThucHien.IdKeHoach == IDKH, new string[] { "QLTD_CTCV_ChuanBiThucHien" })
                    .GroupBy(x => new { x.QLTD_CTCV_ChuanBiThucHien.IdKeHoach })
                    .Select(x => new
                    {
                        ND = x.Select(y => y.NoiDung)
                    });

                var qr = query.Select(x => x.ND).FirstOrDefault();

                if(qr != null)
                {
                    foreach (var item in qr)
                    {
                        sz += item + ";";
                    }
                }
            }

            if (IDCV == 32)
            {
                sz = "Số tờ trình: " + qrCBTH.Select(x => x.THS_SoToTrinh).FirstOrDefault()
                    + "; Ngày trình: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBTH.Select(x => x.THS_NgayTrinh).FirstOrDefault());
            }

            if (IDCV == 33)
            {
                sz = "Số quyết định: " + qrCBTH.Select(x => x.PDDA_SoQuyetDinh).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrCBTH.Select(x => x.PDDA_NgayPheDuyet).FirstOrDefault())
                    + "; Giá trị: " + (qrCBTH.Select(x => x.PDDA_GiaTri).FirstOrDefault() / dDim);
            }
            #endregion

            #region Thực hiện
            var qrTH = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => (IDKH == x.IdKeHoach), new[] { "QLTD_KeHoach" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new
                {
                    //CB_DK_THLCNT
                    CTHLCNT_DangLapTKBVTC = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_DangLapTKBVTC).FirstOrDefault(),
                    CTHLCNT_DangTrinhTKBVTC = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_DangTrinhTKBVTC).FirstOrDefault(),
                    CTHLCNT_SoQDKHLCNT = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_SoQDKHLCNT).FirstOrDefault(),
                    CTHLCNT_NgayPheDuyetKHLCNT = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_NgayPheDuyetKHLCNT).FirstOrDefault(),
                    CTHLCNT_SoNgayQDPD_KHLCNT = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_SoNgayQDPD_KHLCNT).FirstOrDefault(),
                    CTHLCNT_KhoKhan = x.Where(y => y.IdCongViec == 35).Select(y => y.CTHLCNT_KhoKhan).FirstOrDefault(),
                    //DLCNT
                    DLCNT_SoQDKQLCNT = x.Where(y => y.IdCongViec == 36).Select(y => y.DLCNT_SoQDKQLCNT).FirstOrDefault(),
                    DLCNT_NgayPheDuyet = x.Where(y => y.IdCongViec == 36).Select(y => y.DLCNT_NgayPheDuyet).FirstOrDefault(),
                    DLCNT_GiaTri = x.Where(y => y.IdCongViec == 36).Select(y => y.DLCNT_GiaTri).FirstOrDefault(),
                    DLCNT_NgayPD_KQLCNT = x.Where(y => y.IdCongViec == 36).Select(y => y.DLCNT_NgayPD_KQLCNT).FirstOrDefault(),
                    //BG_CT_DV_SD
                    HTTC_NgayHoanThanh = x.Where(y => y.IdCongViec == 38).Select(y => y.HTTC_NgayHoanThanh).FirstOrDefault(),
                });

            var qrTH_IDCTCV_GPMB = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => ((IDKH == x.IdKeHoach) && (x.IdCongViec == 34)), new[] { "QLTD_KeHoach" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDCTCV = x.IdChiTietCongViec
                }).GroupBy(x => new { x.IDKH }).Select(x => x.OrderByDescending(y => y.IDCTCV).Select(y => y.IDCTCV).FirstOrDefault());

            var qrTH_IDCTCV_DTCXD = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => ((IDKH == x.IdKeHoach) && (x.IdCongViec == 37)), new[] { "QLTD_KeHoach" })
                .Select(x => new
                {
                    IDKH = x.IdKeHoach,
                    IDCTCV = x.IdChiTietCongViec
                }).GroupBy(x => new { x.IDKH }).Select(x => x.OrderByDescending(y => y.IDCTCV).Select(y => y.IDCTCV).FirstOrDefault());

            if (IDCV == 34)
            {
                var query = _QLTD_TDTH_ThucHienDuAnRepository
                    .GetMulti(x => ((x.QLTD_CTCV_ThucHienDuAn.IdKeHoach == IDKH)&&(qrTH_IDCTCV_GPMB.Contains(x.IdChiTietCongViec))), new string[] { "QLTD_CTCV_ThucHienDuAn" })
                    .GroupBy(x => new { x.QLTD_CTCV_ThucHienDuAn.IdKeHoach })
                    .Select(x => new
                    {
                        ND = x.Select(y => y.NoiDung)
                    });

                var qr = query.Select(x => x.ND).FirstOrDefault();

                if(qr != null)
                {
                    foreach (var item in qr)
                    {
                        sz += item + ";";
                    }
                }
            }

            if (IDCV == 35)
            {
                sz = "Đang lập hoặc điều chỉnh thiết kế BVTC-DT: " + qrTH.Select(x => x.CTHLCNT_DangLapTKBVTC).FirstOrDefault()
                    + "; Đang trình thiết kế hoặc điều chỉnh thiết kế BVTC-DT: " + qrTH.Select(x => x.CTHLCNT_DangTrinhTKBVTC).FirstOrDefault()
                    + "; Số, ngày tháng tờ trình KHLCNT: " + qrTH.Select(x => x.CTHLCNT_SoQDKHLCNT).FirstOrDefault()
                    + "; Ngày trình KHLCNT: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrTH.Select(x => x.CTHLCNT_NgayPheDuyetKHLCNT).FirstOrDefault())
                    + "; Số, ngày tháng QĐPD KHLCNT: " + qrTH.Select(x => x.CTHLCNT_SoNgayQDPD_KHLCNT).FirstOrDefault()
                    + "; Khó khăn, vướng mắc, kiến nghị, đề xuất: " + qrTH.Select(x => x.CTHLCNT_KhoKhan).FirstOrDefault();
            }

            if (IDCV == 36)
            {
                sz = "Số quyết định KQLCNT: " + qrTH.Select(x => x.DLCNT_SoQDKQLCNT).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrTH.Select(x => x.DLCNT_NgayPheDuyet).FirstOrDefault())
                    + "; Giá trị: " + (qrTH.Select(x => x.DLCNT_GiaTri).FirstOrDefault() / dDim)
                    + "; Ngày phê duyệt KQLCNT gói thầu cuối cùng: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrTH.Select(x => x.DLCNT_NgayPD_KQLCNT).FirstOrDefault());
            }

            if (IDCV == 37)
            {
                var query = _QLTD_TDTH_ThucHienDuAnRepository
                    .GetMulti(x => ((x.QLTD_CTCV_ThucHienDuAn.IdKeHoach == IDKH) && (qrTH_IDCTCV_DTCXD.Contains(x.IdChiTietCongViec))), new string[] { "QLTD_CTCV_ThucHienDuAn" })
                    .GroupBy(x => new { x.QLTD_CTCV_ThucHienDuAn.IdKeHoach })
                    .Select(x => new
                    {
                        ND = x.Select(y => y.NoiDung)
                    });

                var qr = query.Select(x => x.ND).FirstOrDefault();

                if(qr != null)
                {
                    foreach (var item in qr)
                    {
                        sz += item + ";";
                    }
                }

            }

            if (IDCV == 38)
            {
                sz = "Ngày hoàn thành: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrTH.Select(x => x.HTTC_NgayHoanThanh).FirstOrDefault());
            }
            #endregion

            #region Quyết toán dự án
            var qrQT = _QLTD_CTCV_QuyetToanDuAnRepository.GetMulti(x => (IDKH == x.IdKeHoach), new[] { "QLTD_KeHoach" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new
                {
                    //HS_QLCL
                    HSQLCL_DaHoanThanh = x.Where(y => y.IdCongViec == 39).Select(y => y.HSQLCL_DaHoanThanh).FirstOrDefault(),
                    //Lập Hồ sơ Trình thẩm tra quyết toán
                    TTQT_DangThamTra = x.Where(y => y.IdCongViec == 41).Select(y => y.TTQT_DangThamTra).FirstOrDefault(),
                    TTQT_SoThamTra = x.Where(y => y.IdCongViec == 41).Select(y => y.TTQT_SoThamTra).FirstOrDefault(),
                    TTQT_NgayThamTra = x.Where(y => y.IdCongViec == 41).Select(y => y.TTQT_NgayThamTra).FirstOrDefault(),
                    TTQT_GiaTri = x.Where(y => y.IdCongViec == 41).Select(y => y.TTQT_GiaTri).FirstOrDefault(),
                    //Nhận QĐ phê duyệt quyết toán
                    HTHSQT_GiaTriQuyetToan = x.Where(y => y.IdCongViec == 42).Select(y => y.HTHSQT_GiaTriQuyetToan).FirstOrDefault(),
                    //Thanh toán công nợ(Hoàn thành quyết toán)
                    HTQT_SoQD = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_SoQD).FirstOrDefault(),
                    HTQT_NgayPD = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_NgayPD).FirstOrDefault(),
                    HTQT_GiaTri = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_GiaTri).FirstOrDefault(),
                    HTQT_CongNoThu = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_CongNoThu).FirstOrDefault(),
                    HTQT_CongNoTra = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_CongNoTra).FirstOrDefault(),
                    HTQT_BoTriTraCongNo = x.Where(y => y.IdCongViec == 43).Select(y => y.HTQT_BoTriTraCongNo).FirstOrDefault()
                });


            if (IDCV == 39)
            {
                if(qrQT.Select(x => x.HSQLCL_DaHoanThanh).FirstOrDefault() == true)
                {
                    sz = "Đã hoàn thành hồ sơ QLCL";
                }
            }

            if (IDCV == 40)
            {
                var query = _QLTD_TDTH_QuyetToanDuAnRepository.GetMulti(x => x.QLTD_CTCV_QuyetToanDuAn.IdKeHoach == IDKH, new string[] { "QLTD_CTCV_QuyetToanDuAn" })
                    .GroupBy(x => new { x.QLTD_CTCV_QuyetToanDuAn.IdKeHoach })
                    .Select(x => new
                    {
                        ND = x.Select(y => y.NoiDung)
                    });

                var qr = query.Select(x => x.ND).FirstOrDefault();

                if(qr != null)
                {
                    foreach (var item in qr)
                    {
                        sz += item + ";";
                    }
                }
            }

            if (IDCV == 41)
            {
                sz = "Đang lập hồ sơ: " + qrQT.Select(x => x.TTQT_DangThamTra).FirstOrDefault()
                    + "; Số tờ trình: " + qrQT.Select(x => x.TTQT_SoThamTra).FirstOrDefault()
                    + "; Ngày trình: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrQT.Select(x => x.TTQT_NgayThamTra).FirstOrDefault())
                    + "; Giá trị: " + (qrQT.Select(x => x.TTQT_GiaTri).FirstOrDefault() / dDim);
            }

            if (IDCV == 42)
            {
                sz = "Giá trị quyết toán: " + (qrQT.Select(x => x.HTHSQT_GiaTriQuyetToan).FirstOrDefault() / dDim);
            }

            if (IDCV == 43)
            {
                sz = "Số quyết định: " + qrQT.Select(x => x.HTQT_SoQD).FirstOrDefault()
                    + "; Ngày phê duyệt: " + Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(qrQT.Select(x => x.HTQT_NgayPD).FirstOrDefault())
                    + "; Giá trị: " + (qrQT.Select(x => x.HTQT_GiaTri).FirstOrDefault() / dDim)
                    + "; Công nợ còn phải thu: " + (qrQT.Select(x => x.HTQT_CongNoThu).FirstOrDefault() / dDim)
                    + "; Công nợ còn phải trả: " + (qrQT.Select(x => x.HTQT_CongNoTra).FirstOrDefault() / dDim)
                    + "; Cần bố trí cốn trả công nợ: " + (qrQT.Select(x => x.HTQT_BoTriTraCongNo).FirstOrDefault() / dDim);
            }
            #endregion

            return sz;
        }

        public string Get_String_DateTime_For_BC_ThucHien_KeHoachTrienKhaiDA(DateTime? dt)
        {
            var szDT = Convert.ToDateTime(dt).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            if (szDT == "01/01/0001")
            {
                szDT = "";
            }
            return szDT;
        }

    }
}
