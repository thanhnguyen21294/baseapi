﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CongViecService
    {
        QLTD_CongViec Add(QLTD_CongViec qltd_CongViec);

        void Update(QLTD_CongViec qltd_CongViec);

        QLTD_CongViec Delete(int id);

        IEnumerable<QLTD_CongViec> GetAll(int idGiaiDoan);

        QLTD_CongViec GetById(int id);
        List<int> GetCVKetThuc();
        void Save();
    }

    public class QLTD_CongViecService : IQLTD_CongViecService
    {
        private IQLTD_CongViecRepository _QLTD_CongViecRepository;
        private IQLTD_KeHoachCongViecRepository _QLTD_KeHoachCongViecRepository;
        private IQLTD_CTCV_ThucHienDuAnRepository _QLTD_CTCV_ThucHienDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_CongViecService(IQLTD_CongViecRepository qltd_CongViecRepository, IQLTD_KeHoachCongViecRepository QLTD_KeHoachCongViecRepository,
            IQLTD_CTCV_ThucHienDuAnRepository QLTD_CTCV_ThucHienDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_CongViecRepository = qltd_CongViecRepository;
            _QLTD_KeHoachCongViecRepository = QLTD_KeHoachCongViecRepository;
            _QLTD_CTCV_ThucHienDuAnRepository = QLTD_CTCV_ThucHienDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_CongViec Add(QLTD_CongViec qltd_CongViec)
        {
            return _QLTD_CongViecRepository.Add(qltd_CongViec);
        }

        public QLTD_CongViec Delete(int id)
        {
            return _QLTD_CongViecRepository.Delete(id);
        }
        
        public List<int> GetCVKetThuc()
        {
            var IDEnd = _QLTD_CongViecRepository.GetMulti(x => x.IdGiaiDoan == 5).OrderByDescending(x => x.Order).FirstOrDefault().IdCongViec;
            var lstKH = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => x.IdCongViec == IDEnd && x.HoanThanh == 2, new string[] { "QLTD_KeHoach" }).Select(x => x.QLTD_KeHoach.IdDuAn).Distinct().ToList();
            return lstKH;
        }
        public IEnumerable<QLTD_CongViec> GetAll(int idGiaiDoan)
        {
            return _QLTD_CongViecRepository.GetQLTD_CongViec().Where(x => x.IdGiaiDoan == idGiaiDoan).OrderBy(o => o.Order);
        }

        public QLTD_CongViec GetById(int id)
        {
            return _QLTD_CongViecRepository.GetMulti(x => x.IdCongViec == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CongViec qltd_CongViec)
        {
            _QLTD_CongViecRepository.Update(qltd_CongViec);
        }
    }
}
