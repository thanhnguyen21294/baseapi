﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_THCT_ChuanBiThucHienService
    {
        QLTD_THCT_ChuanBiThucHien Add(QLTD_THCT_ChuanBiThucHien qltd_THCT_ChuanBiThucHien);

        void Update(QLTD_THCT_ChuanBiThucHien qltd_THCT_ChuanBiThucHien);

        QLTD_THCT_ChuanBiThucHien Delete(int id);

        IEnumerable<QLTD_THCT_ChuanBiThucHien> GetAll();

        IEnumerable<QLTD_THCT_ChuanBiThucHien> GetByIDTienDoThucHien(int idTDTH);

        IEnumerable<QLTD_THCT_ChuanBiThucHien> GetCongViecHangNgay(int idCTCV);

        IEnumerable<QLTD_THCT_ChuanBiThucHien> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_THCT_ChuanBiThucHien GetById(int id);

        void Save();
    }
    class QLTD_THCT_ChuanBiThucHienService : IQLTD_THCT_ChuanBiThucHienService
    {
        private IQLTD_THCT_ChuanBiThucHienRepository _QLTD_THCT_ChuanBiThucHienRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_THCT_ChuanBiThucHienService(IQLTD_THCT_ChuanBiThucHienRepository qltd_THCT_ChuanBiThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_THCT_ChuanBiThucHienRepository = qltd_THCT_ChuanBiThucHienRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_THCT_ChuanBiThucHien Add(QLTD_THCT_ChuanBiThucHien qltd_THCT_ChuanBiThucHien)
        {
            return _QLTD_THCT_ChuanBiThucHienRepository.Add(qltd_THCT_ChuanBiThucHien);
        }

        public QLTD_THCT_ChuanBiThucHien Delete(int id)
        {
            return _QLTD_THCT_ChuanBiThucHienRepository.Delete(id);
        }

        public IEnumerable<QLTD_THCT_ChuanBiThucHien> GetAll()
        {
            return _QLTD_THCT_ChuanBiThucHienRepository.GetQLTD_THCT_ChuanBiThucHien();
        }

        public QLTD_THCT_ChuanBiThucHien GetById(int id)
        {
            return _QLTD_THCT_ChuanBiThucHienRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_THCT_ChuanBiThucHien> GetByIDTienDoThucHien(int idTDTH)
        {
            return _QLTD_THCT_ChuanBiThucHienRepository.GetMulti(x => x.IdTienDoThucHien == idTDTH);
        }

        public IEnumerable<QLTD_THCT_ChuanBiThucHien> GetCongViecHangNgay(int idCTCV)
        {
            DateTime now = DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0);

            var query = _QLTD_THCT_ChuanBiThucHienRepository.
                GetMulti(x => x.QLTD_TDTH_ChuanBiThucHien.IdChiTietCongViec == idCTCV
                && x.CreatedDate > startDate
                && x.CreatedDate < endDate, new string[] { "QLTD_TDTH_ChuanBiThucHien" }).
                OrderByDescending(x => x.CreatedDate);
            return query;
        }

        public IEnumerable<QLTD_THCT_ChuanBiThucHien> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_THCT_ChuanBiThucHienRepository.GetQLTD_THCT_ChuanBiThucHien();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDoThucHien == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDoThucHien == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_THCT_ChuanBiThucHien qltd_THCT_ChuanBiThucHien)
        {
            _QLTD_THCT_ChuanBiThucHienRepository.Update(qltd_THCT_ChuanBiThucHien);
        }
    }
}
