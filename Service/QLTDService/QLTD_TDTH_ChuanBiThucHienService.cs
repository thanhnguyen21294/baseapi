﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_TDTH_ChuanBiThucHienService
    {
        QLTD_TDTH_ChuanBiThucHien Add(QLTD_TDTH_ChuanBiThucHien qltd_ChuanBiThucHien);

        void Update(QLTD_TDTH_ChuanBiThucHien qltd_ChuanBiThucHien);

        QLTD_TDTH_ChuanBiThucHien Delete(int id);

        IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetAll();

        IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetByIDChiTietCongViec(int idCTCV);

        IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_TDTH_ChuanBiThucHien GetById(int id);

        void Save();
    }
    class QLTD_TDTH_ChuanBiThucHienService : IQLTD_TDTH_ChuanBiThucHienService
    {
        private IQLTD_TDTH_ChuanBiThucHienRepository _QLTD_TDTH_ChuanBiThucHienRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_TDTH_ChuanBiThucHienService(IQLTD_TDTH_ChuanBiThucHienRepository qltd_ChuanBiThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_TDTH_ChuanBiThucHienRepository = qltd_ChuanBiThucHienRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_TDTH_ChuanBiThucHien Add(QLTD_TDTH_ChuanBiThucHien qltd_ChuanBiThucHien)
        {
            return _QLTD_TDTH_ChuanBiThucHienRepository.Add(qltd_ChuanBiThucHien);
        }

        public QLTD_TDTH_ChuanBiThucHien Delete(int id)
        {
            return _QLTD_TDTH_ChuanBiThucHienRepository.Delete(id);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetAll()
        {
            return _QLTD_TDTH_ChuanBiThucHienRepository.GetQLTD_TDTH_ChuanBiThucHien();
        }

        public QLTD_TDTH_ChuanBiThucHien GetById(int id)
        {
            return _QLTD_TDTH_ChuanBiThucHienRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetByIDChiTietCongViec(int idCTCV)
        {
            return _QLTD_TDTH_ChuanBiThucHienRepository.GetMulti(x => x.IdChiTietCongViec == idCTCV);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_TDTH_ChuanBiThucHienRepository.GetQLTD_TDTH_ChuanBiThucHien();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdChiTietCongViec == idCTCV);
            }
            else
            {
                query = query.Where(x => x.IdChiTietCongViec == idCTCV);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_TDTH_ChuanBiThucHien qltd_ChuanBiThucHien)
        {
            _QLTD_TDTH_ChuanBiThucHienRepository.Update(qltd_ChuanBiThucHien);
        }
    }
}
