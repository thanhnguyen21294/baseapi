﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Model.Models.BCModel;

namespace Service.QLTDService
{
    public interface IQLTD_KeHoachCongViecService
    {
        QLTD_KeHoachCongViec Add(QLTD_KeHoachCongViec qltd_KeHoachCongViec);

        void Update(QLTD_KeHoachCongViec qltd_KeHoachCongViec);

        QLTD_KeHoachCongViec Delete(int id);

        IEnumerable<QLTD_KeHoachCongViec> GetAll();

        IEnumerable<QLTD_KeHoachCongViec> GetAllID(int idKH);

        IEnumerable<QLTD_KeHoachCongViec> GetByFilter(int idKH, int page, int pageSize, out int totalRow, string filter);

        IEnumerable<QLTD_KeHoachCongViec> GetListByIdKeHoach(int idKH);

        QLTD_KeHoachCongViec GetById(int id);

        void DeleteByIdKeHoach(int idKH);

        IEnumerable<KetQuaKhongNhapLieu> GetAllKHCVTheoNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay);

        IEnumerable<BaoCaoKetQuaThucHien> GetCVDaThucHienNgayDenNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay);

        IEnumerable<KetQuaHoanThanhNhungCham> GetCVHoanThanhChamNgayDenNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay);

        IEnumerable<KetQuaChuaHoanThanh> GetCVChamChuaHoanThanhNgayDenNgay(Dictionary<string, string> lstUserNhanVien);

        void Save();
    }
    public class QLTD_KeHoachCongViecService : IQLTD_KeHoachCongViecService
    {
        private IQLTD_KeHoachCongViecRepository _QLTD_KeHoachCongViecRepository;
        private IDuAnRepository _DuAnRepository;

        private IQLTD_CTCV_ChuTruongDauTuRepository _QLTD_CTCV_ChuTruongDauTuRepository;
        private IQLTD_TDTH_ChuTruongDauTuRepository _QLTD_TDTH_ChuTruongDauTuRepository;
        private IQLTD_THCT_ChuTruongDauTuRepository _QLTD_THCT_ChuTruongDauTuRepository;

        private IQLTD_CTCV_ChuanBiDauTuRepository _QLTD_CTCV_ChuanBiDauTuRepository;
        private IQLTD_TDTH_ChuanBiDauTuRepository _QLTD_TDTH_ChuanBiDauTuRepository;
        private IQLTD_THCT_ChuanBiDauTuRepository _QLTD_THCT_ChuanBiDauTuRepository;

        private IQLTD_CTCV_ChuanBiThucHienRepository _QLTD_CTCV_ChuanBiThucHienRepository;
        private IQLTD_TDTH_ChuanBiThucHienRepository _QLTD_TDTH_ChuanBiThucHienRepository;
        private IQLTD_THCT_ChuanBiThucHienRepository _QLTD_THCT_ChuanBiThucHienRepository;

        private IQLTD_CTCV_ThucHienDuAnRepository _QLTD_CTCV_ThucHienDuAnRepository;
        private IQLTD_TDTH_ThucHienDuAnRepository _QLTD_TDTH_ThucHienDuAnRepository;
        private IQLTD_THCT_ThucHienDuAnRepository _QLTD_THCT_ThucHienDuAnRepository;

        private IQLTD_CTCV_QuyetToanDuAnRepository _QLTD_CTCV_QuyetToanDuAnRepository;
        private IQLTD_TDTH_QuyetToanDuAnRepository _QLTD_TDTH_QuyetToanDuAnRepository;
        private IQLTD_THCT_QuyetToanDuAnRepository _QLTD_THCT_QuyetToanDuAnRepository;

        private IQLTD_KeHoachRepository _IQLTD_KeHoachRepository;
        private IUnitOfWork _unitOfWork;
        private IQLTD_KeHoachService _QLTD_KeHoachService;

        public QLTD_KeHoachCongViecService(IQLTD_KeHoachCongViecRepository qLTD_KeHoachCongViecRepository,
            IDuAnRepository duAnRepository,
            IQLTD_KeHoachRepository qltd_KeHoachRepository,
            IQLTD_CTCV_ChuTruongDauTuRepository qltd_CTCV_ChuTruongDauTuRepository,
            IQLTD_TDTH_ChuTruongDauTuRepository qltd_TDTH_ChuTruongDauTuRepository,
            IQLTD_THCT_ChuTruongDauTuRepository qltd_THCT_ChuTruongDauTuRepository,
            IQLTD_CTCV_ChuanBiDauTuRepository qltd_CTCV_ChuanBiDauTuRepository,
            IQLTD_TDTH_ChuanBiDauTuRepository qltd_TDTH_ChuanBiDauTuRepository,
            IQLTD_THCT_ChuanBiDauTuRepository qltd_THCT_ChuanBiDauTuRepository,
            IQLTD_CTCV_ChuanBiThucHienRepository qltd_CTCV_ChuanBiThucHienRepository,
            IQLTD_TDTH_ChuanBiThucHienRepository qltd_TDTH_ChuanBiThucHienRepository,
            IQLTD_THCT_ChuanBiThucHienRepository qltd_THCT_ChuanBiThucHienRepository,
            IQLTD_CTCV_ThucHienDuAnRepository qltd_CTCV_ThucHienDuAnRepository,
            IQLTD_TDTH_ThucHienDuAnRepository qltd_TDTH_ThucHienDuAnRepository,
            IQLTD_THCT_ThucHienDuAnRepository qltd_THCT_ThucHienDuAnRepository,
            IQLTD_CTCV_QuyetToanDuAnRepository qltd_CTCV_QuyetToanDuAnRepository,
            IQLTD_TDTH_QuyetToanDuAnRepository qltd_TDTH_QuyetToanDuAnRepository,
            IQLTD_THCT_QuyetToanDuAnRepository qltd_THCT_QuyetToanDuAnRepository,
        IUnitOfWork unitOfWork,
            IQLTD_KeHoachService qLTD_KeHoachService)
        {
            _QLTD_KeHoachCongViecRepository = qLTD_KeHoachCongViecRepository;
            _DuAnRepository = duAnRepository;

            _QLTD_CTCV_ChuTruongDauTuRepository = qltd_CTCV_ChuTruongDauTuRepository;
            _QLTD_TDTH_ChuTruongDauTuRepository = qltd_TDTH_ChuTruongDauTuRepository;
            _QLTD_THCT_ChuTruongDauTuRepository = qltd_THCT_ChuTruongDauTuRepository;

            _QLTD_CTCV_ChuanBiDauTuRepository = qltd_CTCV_ChuanBiDauTuRepository;
            _QLTD_TDTH_ChuanBiDauTuRepository = qltd_TDTH_ChuanBiDauTuRepository;
            _QLTD_THCT_ChuanBiDauTuRepository = qltd_THCT_ChuanBiDauTuRepository;

            _QLTD_CTCV_ChuanBiThucHienRepository = qltd_CTCV_ChuanBiThucHienRepository;
            _QLTD_TDTH_ChuanBiThucHienRepository = qltd_TDTH_ChuanBiThucHienRepository;
            _QLTD_THCT_ChuanBiThucHienRepository = qltd_THCT_ChuanBiThucHienRepository;

            _QLTD_CTCV_ThucHienDuAnRepository = qltd_CTCV_ThucHienDuAnRepository;
            _QLTD_TDTH_ThucHienDuAnRepository = qltd_TDTH_ThucHienDuAnRepository;
            _QLTD_THCT_ThucHienDuAnRepository = qltd_THCT_ThucHienDuAnRepository;

            _QLTD_CTCV_QuyetToanDuAnRepository = qltd_CTCV_QuyetToanDuAnRepository;
            _QLTD_TDTH_QuyetToanDuAnRepository = qltd_TDTH_QuyetToanDuAnRepository;
            _QLTD_THCT_QuyetToanDuAnRepository = qltd_THCT_QuyetToanDuAnRepository;

            this._unitOfWork = unitOfWork;
            this._QLTD_KeHoachService = qLTD_KeHoachService;
            _IQLTD_KeHoachRepository = qltd_KeHoachRepository;
        }

        public QLTD_KeHoachCongViec Add(QLTD_KeHoachCongViec qltd_KeHoachCongViec)
        {
            return _QLTD_KeHoachCongViecRepository.Add(qltd_KeHoachCongViec);
        }

        public QLTD_KeHoachCongViec Delete(int id)
        {
            return _QLTD_KeHoachCongViecRepository.Delete(id);
        }

        public void DeleteByIdKeHoach(int idKH)
        {
            _QLTD_KeHoachCongViecRepository.DeleteMulti(x => x.IdKeHoach == idKH);
        }

        public IEnumerable<QLTD_KeHoachCongViec> GetAll()
        {
            return _QLTD_KeHoachCongViecRepository.GetAll(new[] { "QLTD_CongViec" });
        }

        public IEnumerable<QLTD_KeHoachCongViec> GetAllID(int idKH)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<QLTD_KeHoachCongViec> GetByFilter(int idKeHoach, int page, int pageSize, out int totalRow, string filter)
        {
            throw new NotImplementedException();
        }

        public QLTD_KeHoachCongViec GetById(int id)
        {
            return _QLTD_KeHoachCongViecRepository.GetSingleByCondition(x => x.IdKeHoachCongViec == id, new[] { "QLTD_CongViec" });
        }

        public IEnumerable<QLTD_KeHoachCongViec> GetListByIdKeHoach(int idKH)
        {
            return _QLTD_KeHoachCongViecRepository.GetQLTD_KeHoachCongViec().Where(x => x.IdKeHoach == idKH);
        }

        public IEnumerable<KetQuaKhongNhapLieu> GetAllKHCVTheoNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay)
        {
            try
            {
                // lấy các kế hoạch đã phê duyệt
                var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
                {
                    IDKeHoach = x.IdKeHoach,
                    TenKH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    IDGD = x.IdGiaiDoan,
                    NgayTinhTrang = x.NgayTinhTrang
                }).ToList();
                var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                        .Select(x => new
                        {
                            KH = x.Select(y => new
                            {
                                IDKH = y.IDKeHoach,
                                NgayTinhTrang = y.NgayTinhTrang
                            }).OrderByDescending(a => a.NgayTinhTrang).FirstOrDefault()
                        }).Select(x => x.KH.IDKH).ToList();
                //-------------------------------------------------------------------------------------
                DateTime sTuNgay = DateTime.ParseExact(tungay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime sDenNgay = DateTime.ParseExact(denngay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                //Chủ trương đầu tư ----------------------------------------------------------------------------------
                var lstAll_CTDT = _QLTD_CTCV_ChuTruongDauTuRepository.GetAll(new string[]
                { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                    "QLTD_KeHoach.DuAn.DuAnUsers",
                    "QLTD_KeHoachCongViec", "QLTD_TDTH_ChuTruongDauTu",
                    "QLTD_TDTH_ChuTruongDauTu.QLTD_THCT_ChuTruongDauTu" })
                .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                && w.QLTD_KeHoachCongViec.TuNgay != null 
                && w.QLTD_KeHoachCongViec.DenNgay != null
                && (w.QLTD_KeHoachCongViec.TuNgay < sDenNgay)
                && w.QLTD_CongViec.TienDoThucHien
                && w.QLTD_TDTH_ChuTruongDauTu.Count > 0)
                .Select(x => new
                {
                    IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                    TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    IDKH = x.QLTD_KeHoach.IdKeHoach,
                    TenKH = x.QLTD_KeHoach.NoiDung,
                    TenCV = x.QLTD_CongViec.TenCongViec,
                    TuNgayKH = new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                    TuNgayTT = x.QLTD_KeHoachCongViec.TuNgay <= sTuNgay ? new DateTime(sTuNgay.Year, sTuNgay.Month, sTuNgay.Day, 0, 0, 0) : new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                    DenNgayKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0),
                    NgayHoanThanh = (x.NgayHoanThanh == null || x.NgayHoanThanh > sDenNgay) ? new DateTime(sDenNgay.Year, sDenNgay.Month, sDenNgay.Day, 0, 0, 0) : new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    CoTienDo = x.QLTD_CongViec.TienDoThucHien,
                    grpTDTH = x.QLTD_TDTH_ChuTruongDauTu.Select(y => new
                    {
                        IdTienDoThucHien = y.IdTienDoThucHien,
                        NoiDung = y.NoiDung,
                        grpTHCT = y.QLTD_THCT_ChuTruongDauTu.Select(z => new
                        {
                            NoiDung = z.NoiDung,
                            ThayDoi = z.ThayDoi,
                            Created = new DateTime(z.CreatedDate.Value.Year, z.CreatedDate.Value.Month, z.CreatedDate.Value.Day, 0, 0, 0)
                        })
                    }).ToList()
                }).ToList();
                //Chuẩn bị đầu tư
                var lstAll_CBDT = _QLTD_CTCV_ChuanBiDauTuRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers",
                        "QLTD_KeHoachCongViec", "QLTD_TDTH_ChuanBiDauTu",
                        "QLTD_TDTH_ChuanBiDauTu.QLTD_THCT_ChuanBiDauTu" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.QLTD_KeHoachCongViec.TuNgay != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (w.QLTD_KeHoachCongViec.TuNgay < sDenNgay)
                    && w.QLTD_CongViec.TienDoThucHien
                    && w.QLTD_TDTH_ChuanBiDauTu.Count > 0)
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        TuNgayKH = new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        TuNgayTT = x.QLTD_KeHoachCongViec.TuNgay <= sTuNgay ? new DateTime(sTuNgay.Year, sTuNgay.Month, sTuNgay.Day, 0, 0, 0) : new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        DenNgayKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0),
                        NgayHoanThanh = (x.NgayHoanThanh == null || x.NgayHoanThanh > sDenNgay) ? new DateTime(sDenNgay.Year, sDenNgay.Month, sDenNgay.Day, 0, 0, 0) : new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        CoTienDo = x.QLTD_CongViec.TienDoThucHien,
                        grpTDTH = x.QLTD_TDTH_ChuanBiDauTu.Select(y => new
                        {
                            IdTienDoThucHien = y.IdTienDoThucHien,
                            NoiDung = y.NoiDung,
                            grpTHCT = y.QLTD_THCT_ChuanBiDauTu.Select(z => new
                            {
                                NoiDung = z.NoiDung,
                                ThayDoi = z.ThayDoi,
                                Created = new DateTime(z.CreatedDate.Value.Year, z.CreatedDate.Value.Month, z.CreatedDate.Value.Day, 0, 0, 0)
                            })
                        }).ToList()
                    }).ToList();

                //Chuẩn bị thực hiện
                var lstAll_CBTH = _QLTD_CTCV_ChuanBiThucHienRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers",
                        "QLTD_KeHoachCongViec", "QLTD_TDTH_ChuanBiThucHien",
                        "QLTD_TDTH_ChuanBiThucHien.QLTD_THCT_ChuanBiThucHien" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.QLTD_KeHoachCongViec.TuNgay != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (w.QLTD_KeHoachCongViec.TuNgay < sDenNgay)
                    && w.QLTD_CongViec.TienDoThucHien
                    && w.QLTD_TDTH_ChuanBiThucHien.Count > 0)
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        TuNgayKH = new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        TuNgayTT = x.QLTD_KeHoachCongViec.TuNgay <= sTuNgay ? new DateTime(sTuNgay.Year, sTuNgay.Month, sTuNgay.Day, 0, 0, 0) : new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        DenNgayKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0),
                        NgayHoanThanh = (x.NgayHoanThanh == null || x.NgayHoanThanh > sDenNgay) ? new DateTime(sDenNgay.Year, sDenNgay.Month, sDenNgay.Day, 0, 0, 0) : new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        CoTienDo = x.QLTD_CongViec.TienDoThucHien,
                        grpTDTH = x.QLTD_TDTH_ChuanBiThucHien.Select(y => new
                        {
                            IdTienDoThucHien = y.IdTienDoThucHien,
                            NoiDung = y.NoiDung,
                            grpTHCT = y.QLTD_THCT_ChuanBiThucHien.Select(z => new
                            {
                                NoiDung = z.NoiDung,
                                ThayDoi = z.ThayDoi,
                                Created = new DateTime(z.CreatedDate.Value.Year, z.CreatedDate.Value.Month, z.CreatedDate.Value.Day, 0, 0, 0)
                            })
                        }).ToList()
                    }).ToList();

                //Thực hiện dự án
                var lstAll_THDA = _QLTD_CTCV_ThucHienDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers",
                        "QLTD_KeHoachCongViec", "QLTD_TDTH_ThucHienDuAn",
                        "QLTD_TDTH_ThucHienDuAn.QLTD_THCT_ThucHienDuAn" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.QLTD_KeHoachCongViec.TuNgay != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (w.QLTD_KeHoachCongViec.TuNgay < sDenNgay)
                    && w.QLTD_CongViec.TienDoThucHien
                    && w.QLTD_TDTH_ThucHienDuAn.Count > 0)
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        TuNgayKH = new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        TuNgayTT = x.QLTD_KeHoachCongViec.TuNgay <= sTuNgay ? new DateTime(sTuNgay.Year, sTuNgay.Month, sTuNgay.Day, 0, 0, 0) : new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        DenNgayKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0),
                        NgayHoanThanh = (x.NgayHoanThanh == null || x.NgayHoanThanh > sDenNgay) ? new DateTime(sDenNgay.Year, sDenNgay.Month, sDenNgay.Day, 0, 0, 0) : new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        CoTienDo = x.QLTD_CongViec.TienDoThucHien,
                        grpTDTH = x.QLTD_TDTH_ThucHienDuAn.Select(y => new
                        {
                            IdTienDoThucHien = y.IdTienDoThucHien,
                            NoiDung = y.NoiDung,
                            grpTHCT = y.QLTD_THCT_ThucHienDuAn.Select(z => new
                            {
                                NoiDung = z.NoiDung,
                                ThayDoi = z.ThayDoi,
                                Created = new DateTime(z.CreatedDate.Value.Year, z.CreatedDate.Value.Month, z.CreatedDate.Value.Day, 0, 0, 0)
                            })
                        }).ToList()
                    }).ToList();

                //Quyết toán dự án
                var lstAll_QTDA = _QLTD_CTCV_QuyetToanDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers",
                        "QLTD_KeHoachCongViec", "QLTD_TDTH_QuyetToanDuAn",
                        "QLTD_TDTH_QuyetToanDuAn.QLTD_THCT_QuyetToanDuAn" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.QLTD_KeHoachCongViec.TuNgay != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (w.QLTD_KeHoachCongViec.TuNgay < sDenNgay)
                    && w.QLTD_CongViec.TienDoThucHien
                    && w.QLTD_TDTH_QuyetToanDuAn.Count > 0)
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        TuNgayKH = new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        TuNgayTT = x.QLTD_KeHoachCongViec.TuNgay <= sTuNgay ? new DateTime(sTuNgay.Year, sTuNgay.Month, sTuNgay.Day, 0, 0, 0) : new DateTime(x.QLTD_KeHoachCongViec.TuNgay.Value.Year, x.QLTD_KeHoachCongViec.TuNgay.Value.Month, x.QLTD_KeHoachCongViec.TuNgay.Value.Day, 0, 0, 0),
                        DenNgayKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0),
                        NgayHoanThanh = (x.NgayHoanThanh == null || x.NgayHoanThanh > sDenNgay) ? new DateTime(sDenNgay.Year, sDenNgay.Month, sDenNgay.Day, 0, 0, 0) : new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        CoTienDo = x.QLTD_CongViec.TienDoThucHien,
                        grpTDTH = x.QLTD_TDTH_QuyetToanDuAn.Select(y => new
                        {
                            IdTienDoThucHien = y.IdTienDoThucHien,
                            NoiDung = y.NoiDung,
                            grpTHCT = y.QLTD_THCT_QuyetToanDuAn.Select(z => new
                            {
                                NoiDung = z.NoiDung,
                                ThayDoi = z.ThayDoi,
                                Created = new DateTime(z.CreatedDate.Value.Year, z.CreatedDate.Value.Month, z.CreatedDate.Value.Day, 0, 0, 0)
                            })
                        }).ToList()
                    }).ToList();

                var lstAll = lstAll_CTDT.Union(lstAll_CBDT).Union(lstAll_CBTH).Union(lstAll_THDA).Union(lstAll_QTDA).ToList();

                var lstAll_Bam = lstAll.Where(w => w.NgayHoanThanh >= w.TuNgayTT)
                    .GroupBy(g => new { g.IDDuAn, g.TenDA })
                    .Select(s => new KetQuaKhongNhapLieu()
                    {
                        IDDuAn = s.Key.IDDuAn,
                        TenDA = s.Key.TenDA,
                        Users = s.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        grpKH = s.GroupBy(g1 => new { g1.IDKH, g1.TenKH, g1.TenCV, g1.TuNgayTT, g1.NgayHoanThanh }).Select(s1 => new KHKhongNhapLieu()
                        {
                            IDKH = s1.Key.IDKH,
                            TenKH = s1.Key.TenKH + " -> " + s1.Key.TenCV,
                            TuNgay = s1.Key.TuNgayTT,
                            NgayHT = s1.Key.NgayHoanThanh,
                            lstDate = listDateFromTo(s1.Key.TuNgayTT, s1.Key.NgayHoanThanh),
                            grpTDTH = s1.FirstOrDefault().grpTDTH.Select(s2 => new TDTH()
                            {
                                IDTienDoThucHien = s2.IdTienDoThucHien,
                                NoiDung = s2.NoiDung,
                                grpNgayBam = s2.grpTHCT.GroupBy(g2 => g2.Created).Select(s4 => s4.Key).ToList()
                            }).ToList()
                        }).ToList()
                    }).ToList();

                return lstAll_Bam;
            }
            catch (Exception e)
            {
                string aaa = e.Message;
                return null;
            }
        }

        public IEnumerable<BaoCaoKetQuaThucHien> GetCVDaThucHienNgayDenNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay)
        {
            try
            {
                // lấy các kế hoạch đã phê duyệt
                var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
                {
                    IDKeHoach = x.IdKeHoach,
                    TenKH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    IDGD = x.IdGiaiDoan,
                    NgayTinhTrang = x.NgayTinhTrang
                }).ToList();
                var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                        .Select(x => new
                        {
                            KH = x.Select(y => new
                            {
                                IDKH = y.IDKeHoach,
                                NgayTinhTrang = y.NgayTinhTrang
                            }).OrderByDescending(a => a.NgayTinhTrang).FirstOrDefault()
                        }).Select(x => x.KH.IDKH).ToList();
                //-------------------------------------------------------------------------------------
                DateTime sTuNgay = DateTime.ParseExact(tungay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime sDenNgay = DateTime.ParseExact(denngay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                //Chủ trương đầu tư ----------------------------------------------------------------------------------
                var lstAll_CTDT = _QLTD_CTCV_ChuTruongDauTuRepository.GetAll(new string[]
                { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                    "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec"})
                .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                && w.NgayHoanThanh != null
                && w.QLTD_KeHoachCongViec.DenNgay != null
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                .Select(x => new
                {
                    IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                    TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    IDKH = x.QLTD_KeHoach.IdKeHoach,
                    TenKH = x.QLTD_KeHoach.NoiDung,
                    TenCV = x.QLTD_CongViec.TenCongViec,
                    NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                }).ToList();
                //Chuẩn bị đầu tư
                var lstAll_CBDT = _QLTD_CTCV_ChuanBiDauTuRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    }).ToList();

                //Chuẩn bị thực hiện
                var lstAll_CBTH = _QLTD_CTCV_ChuanBiThucHienRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    }).ToList();

                //Thực hiện dự án
                var lstAll_THDA = _QLTD_CTCV_ThucHienDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    }).ToList();

                //Quyết toán dự án
                var lstAll_QTDA = _QLTD_CTCV_QuyetToanDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    }).ToList();

                var lstAllUnion = lstAll_CTDT.Union(lstAll_CBDT).Union(lstAll_CBTH).Union(lstAll_THDA).Union(lstAll_QTDA).ToList();

                var lstAll = lstAllUnion
                    .GroupBy(g => new { g.IDDuAn, g.TenDA })
                    .Select(s => new BaoCaoKetQuaThucHien()
                    {
                        IDDuAn = s.Key.IDDuAn,
                        TenDA = s.Key.TenDA,

                        Users = s.Select(s1 => s1.Users).FirstOrDefault().Select(s2=>new Users() { ID = s2.ID,Name=s2.Name}),
                        grpKH = s.GroupBy(g1 => new { g1.IDKH, g1.TenKH, }).Select(s1 => new KH()
                        {
                            IDKH = s1.Key.IDKH,
                            TenKH = s1.Key.TenKH,
                            grpCV = s1.Select(s2 => new CV()
                            {
                                TenCV = s2.TenCV,
                                NgayHoanThanh = s2.NgayHoanThanh
                            }).ToList()
                        }).ToList()
                    }).ToList();

                return lstAll;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<KetQuaHoanThanhNhungCham> GetCVHoanThanhChamNgayDenNgay(Dictionary<string, string> lstUserNhanVien, string tungay, string denngay)
        {
            try
            {
                // lấy các kế hoạch đã phê duyệt
                var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
                {
                    IDKeHoach = x.IdKeHoach,
                    TenKH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    IDGD = x.IdGiaiDoan,
                    NgayTinhTrang = x.NgayTinhTrang
                }).ToList();
                var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                        .Select(x => new
                        {
                            KH = x.Select(y => new
                            {
                                IDKH = y.IDKeHoach,
                                NgayTinhTrang = y.NgayTinhTrang
                            }).OrderByDescending(a => a.NgayTinhTrang).FirstOrDefault()
                        }).Select(x => x.KH.IDKH).ToList();
                //-------------------------------------------------------------------------------------
                DateTime sTuNgay = DateTime.ParseExact(tungay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime sDenNgay = DateTime.ParseExact(denngay, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                //Chủ trương đầu tư ----------------------------------------------------------------------------------
                var lstAll_CTDT = _QLTD_CTCV_ChuTruongDauTuRepository.GetAll(new string[]
                { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                    "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec"})
                .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                && w.NgayHoanThanh != null
                && w.QLTD_KeHoachCongViec.DenNgay != null 
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                .Select(x => new
                {
                    IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                    TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    IDKH = x.QLTD_KeHoach.IdKeHoach,
                    TenKH = x.QLTD_KeHoach.NoiDung,
                    TenCV = x.QLTD_CongViec.TenCongViec,
                    NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                    NgayHoanThanhKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)
                }).ToList();
                //Chuẩn bị đầu tư
                var lstAll_CBDT = _QLTD_CTCV_ChuanBiDauTuRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        NgayHoanThanhKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)
                    }).ToList();

                //Chuẩn bị thực hiện
                var lstAll_CBTH = _QLTD_CTCV_ChuanBiThucHienRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        NgayHoanThanhKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)
                    }).ToList();

                //Thực hiện dự án
                var lstAll_THDA = _QLTD_CTCV_ThucHienDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        NgayHoanThanhKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)
                    }).ToList();

                //Quyết toán dự án
                var lstAll_QTDA = _QLTD_CTCV_QuyetToanDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh != null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0))
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) >= sTuNgay)
                    && (new DateTime(w.NgayHoanThanh.Value.Year, w.NgayHoanThanh.Value.Month, w.NgayHoanThanh.Value.Day, 0, 0, 0) <= sDenNgay))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        NgayHoanThanh = new DateTime(x.NgayHoanThanh.Value.Year, x.NgayHoanThanh.Value.Month, x.NgayHoanThanh.Value.Day, 0, 0, 0),
                        NgayHoanThanhKH = new DateTime(x.QLTD_KeHoachCongViec.DenNgay.Value.Year, x.QLTD_KeHoachCongViec.DenNgay.Value.Month, x.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)
                    }).ToList();

                var lstAllUnion = lstAll_CTDT.Union(lstAll_CBDT).Union(lstAll_CBTH).Union(lstAll_THDA).Union(lstAll_QTDA).ToList();

                var lstAll = lstAllUnion
                    .GroupBy(g => new { g.IDDuAn, g.TenDA })
                    .Select(s => new KetQuaHoanThanhNhungCham()
                    {
                        IDDuAn = s.Key.IDDuAn,
                        TenDA = s.Key.TenDA,
                        Users = s.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        grpKH = s.GroupBy(g1 => new { g1.IDKH, g1.TenKH, }).Select(s1 => new KHHoanThanhNhungCham()
                        {
                            IDKH = s1.Key.IDKH,
                            TenKH = s1.Key.TenKH,
                            grpCV = s1.Select(s2 => new CVHoanThanhNhungCham()
                            {
                                TenCV = s2.TenCV,
                                NgayHoanThanh = s2.NgayHoanThanh,
                                NgayHoanThanhKH = s2.NgayHoanThanhKH
                            }).ToList()
                        }).ToList()
                    }).ToList();

                return lstAll;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<KetQuaChuaHoanThanh> GetCVChamChuaHoanThanhNgayDenNgay(Dictionary<string, string> lstUserNhanVien)
        {
            try
            {
                // lấy các kế hoạch đã phê duyệt
                var khPD = _IQLTD_KeHoachRepository.GetMulti(w => w.TinhTrang == 4).Select(x => new
                {
                    IDKeHoach = x.IdKeHoach,
                    TenKH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    IDGD = x.IdGiaiDoan,
                    NgayTinhTrang = x.NgayTinhTrang
                }).ToList();
                var grpKHPD = khPD.GroupBy(g => new { g.IDDA, g.IDGD })
                        .Select(x => new
                        {
                            KH = x.Select(y => new
                            {
                                IDKH = y.IDKeHoach,
                                NgayTinhTrang = y.NgayTinhTrang
                            }).OrderByDescending(a => a.NgayTinhTrang).FirstOrDefault()
                        }).Select(x => x.KH.IDKH).ToList();
                //-------------------------------------------------------------------------------------
                DateTime sNgayHienTai = new DateTime(DateTime.Now.Year,DateTime.Now.Month, DateTime.Now.Day,0,0,0);

                //Chủ trương đầu tư ----------------------------------------------------------------------------------
                var lstAll_CTDT = _QLTD_CTCV_ChuTruongDauTuRepository.GetAll(new string[]
                { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                    "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec"})
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh == null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (sNgayHienTai > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)))
                .Select(x => new
                {
                    IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                    TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                    Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    IDKH = x.QLTD_KeHoach.IdKeHoach,
                    TenKH = x.QLTD_KeHoach.NoiDung,
                    TenCV = x.QLTD_CongViec.TenCongViec,
                    DenNgay = x.QLTD_KeHoachCongViec.DenNgay != null ? x.QLTD_KeHoachCongViec.DenNgay.Value.ToString() : ""
                }).ToList();
                //Chuẩn bị đầu tư
                var lstAll_CBDT = _QLTD_CTCV_ChuanBiDauTuRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh == null 
                    && w.QLTD_KeHoachCongViec.DenNgay != null 
                    && (sNgayHienTai > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        DenNgay = x.QLTD_KeHoachCongViec.DenNgay != null ? x.QLTD_KeHoachCongViec.DenNgay.Value.ToString() : ""
                    }).ToList();

                //Chuẩn bị thực hiện
                var lstAll_CBTH = _QLTD_CTCV_ChuanBiThucHienRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh == null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (sNgayHienTai > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        DenNgay = x.QLTD_KeHoachCongViec.DenNgay != null ? x.QLTD_KeHoachCongViec.DenNgay.Value.ToString() : ""
                    }).ToList();

                //Thực hiện dự án
                var lstAll_THDA = _QLTD_CTCV_ThucHienDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh == null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (sNgayHienTai > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        DenNgay = x.QLTD_KeHoachCongViec.DenNgay != null ? x.QLTD_KeHoachCongViec.DenNgay.Value.ToString() : ""
                    }).ToList();

                //Quyết toán dự án
                var lstAll_QTDA = _QLTD_CTCV_QuyetToanDuAnRepository.GetAll(new string[]
                    { "QLTD_KeHoach", "QLTD_CongViec", "QLTD_KeHoach.DuAn",
                        "QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_KeHoachCongViec" })
                    .Where(w => grpKHPD.Contains(w.QLTD_KeHoach.IdKeHoach)
                    && w.NgayHoanThanh == null
                    && w.QLTD_KeHoachCongViec.DenNgay != null
                    && (sNgayHienTai > new DateTime(w.QLTD_KeHoachCongViec.DenNgay.Value.Year, w.QLTD_KeHoachCongViec.DenNgay.Value.Month, w.QLTD_KeHoachCongViec.DenNgay.Value.Day, 0, 0, 0)))
                    .Select(x => new
                    {
                        IDDuAn = x.QLTD_KeHoach.DuAn.IdDuAn,
                        TenDA = x.QLTD_KeHoach.DuAn.TenDuAn,
                        Users = x.QLTD_KeHoach.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                        {
                            ID = u.UserId,
                            Name = lstUserNhanVien[u.UserId]
                        }).ToList().Distinct(),
                        IDKH = x.QLTD_KeHoach.IdKeHoach,
                        TenKH = x.QLTD_KeHoach.NoiDung,
                        TenCV = x.QLTD_CongViec.TenCongViec,
                        DenNgay = x.QLTD_KeHoachCongViec.DenNgay != null ? x.QLTD_KeHoachCongViec.DenNgay.Value.ToString() : ""
                    }).ToList();

                var lstAllUnion = lstAll_CTDT.Union(lstAll_CBDT).Union(lstAll_CBTH).Union(lstAll_THDA).Union(lstAll_QTDA).ToList();

                var lstAll = lstAllUnion
                    .GroupBy(g => new { g.IDDuAn, g.TenDA })
                    .Select(s => new KetQuaChuaHoanThanh()
                    {
                        IDDuAn = s.Key.IDDuAn,
                        TenDA = s.Key.TenDA,
                        Users = s.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        grpKH = s.GroupBy(g1 => new { g1.IDKH, g1.TenKH, }).Select(s1 => new KHChuaHoanThanh()
                        {
                            IDKH = s1.Key.IDKH,
                            TenKH = s1.Key.TenKH,
                            grpCV = s1.Select(s2 => new CVChuaHoanThanh()
                            {
                                TenCV = s2.TenCV,
                                DenNgay = s2.DenNgay
                            }).ToList()
                        }).ToList()
                    }).ToList();

                return lstAll;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private List<DateTime> listDateFromTo(DateTime startDate, DateTime endDate)
        {
            List<DateTime> reLstDate = new List<DateTime>();
            for (DateTime d = startDate; d <= endDate; d = d.AddDays(1))
            {
                reLstDate.Add(d);
            }
            return reLstDate;
        }
        private void getFullNameUser()
        {

        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_KeHoachCongViec qltd_KeHoachCongViec)
        {
            _QLTD_KeHoachCongViecRepository.Update(qltd_KeHoachCongViec);
        }
    }
}
