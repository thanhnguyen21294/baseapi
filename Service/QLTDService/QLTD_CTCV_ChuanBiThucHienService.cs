﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CTCV_ChuanBiThucHienService
    {
        QLTD_CTCV_ChuanBiThucHien Add(QLTD_CTCV_ChuanBiThucHien qltd_CTCV_ChuanBiThucHien);

        void Update(QLTD_CTCV_ChuanBiThucHien qltd_CTCV_ChuanBiThucHien);

        QLTD_CTCV_ChuanBiThucHien Delete(int id);

        IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAll(int idKHCV);
        

        QLTD_CTCV_ChuanBiThucHien GetByID(int id);

        IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAllByIDKH(int idKH, string filter = null);

        QLTD_CTCV_ChuanBiThucHien GetByIdKHCV(int idKHCV);

        void Save();

        IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAll();
    }

    class QLTD_CTCV_ChuanBiThucHienService : IQLTD_CTCV_ChuanBiThucHienService
    {
        private IQLTD_CTCV_ChuanBiThucHienRepository _QLTD_CTCV_ChuanBiDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_CTCV_ChuanBiThucHienService(IQLTD_CTCV_ChuanBiThucHienRepository qLTD_CTCV_ChuanBiThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_CTCV_ChuanBiDauTuRepository = qLTD_CTCV_ChuanBiThucHienRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_CTCV_ChuanBiThucHien Add(QLTD_CTCV_ChuanBiThucHien qltd_CTCV_ChuanBiThucHien)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.Add(qltd_CTCV_ChuanBiThucHien);
        }
        
        public QLTD_CTCV_ChuanBiThucHien Delete(int id)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAll(int idKHCV)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetQLTD_CTCV_ChuanBiThucHien().Where(x => x.IdKeHoachCongViec == idKHCV);
        }

        public IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => x.IdKeHoach == idKH, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
            if (filter != null)
            {
                query = query.Where(x => x.QLTD_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public QLTD_CTCV_ChuanBiThucHien GetByIdKHCV(int idKHCV)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetQLTD_CTCV_ChuanBiThucHien().Where(x => x.IdKeHoachCongViec == idKHCV).SingleOrDefault();
        }
        public QLTD_CTCV_ChuanBiThucHien GetByID(int id)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CTCV_ChuanBiThucHien qltd_CTCV_ChuanBiThucHien)
        {
            _QLTD_CTCV_ChuanBiDauTuRepository.Update(qltd_CTCV_ChuanBiThucHien);
        }
        
        public IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetAll()
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetAll();
        }
    }
}
