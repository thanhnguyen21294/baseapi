﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_THCT_QuyetToanDuAnService
    {
        QLTD_THCT_QuyetToanDuAn Add(QLTD_THCT_QuyetToanDuAn qltd_THCT_QuyetToanDuAn);

        void Update(QLTD_THCT_QuyetToanDuAn qltd_THCT_QuyetToanDuAn);

        QLTD_THCT_QuyetToanDuAn Delete(int id);

        IEnumerable<QLTD_THCT_QuyetToanDuAn> GetAll();

        IEnumerable<QLTD_THCT_QuyetToanDuAn> GetByIDTienDoThucHien(int idTDTH);

        IEnumerable<QLTD_THCT_QuyetToanDuAn> GetCongViecHangNgay(int idCTCV);

        IEnumerable<QLTD_THCT_QuyetToanDuAn> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_THCT_QuyetToanDuAn GetById(int id);

        void Save();
    }
    class QLTD_THCT_QuyetToanDuAnService : IQLTD_THCT_QuyetToanDuAnService
    {
        private IQLTD_THCT_QuyetToanDuAnRepository _QLTD_THCT_QuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_THCT_QuyetToanDuAnService(IQLTD_THCT_QuyetToanDuAnRepository qltd_THCT_QuyetToanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_THCT_QuyetToanDuAnRepository = qltd_THCT_QuyetToanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_THCT_QuyetToanDuAn Add(QLTD_THCT_QuyetToanDuAn qltd_THCT_QuyetToanDuAn)
        {
            return _QLTD_THCT_QuyetToanDuAnRepository.Add(qltd_THCT_QuyetToanDuAn);
        }

        public QLTD_THCT_QuyetToanDuAn Delete(int id)
        {
            return _QLTD_THCT_QuyetToanDuAnRepository.Delete(id);
        }

        public IEnumerable<QLTD_THCT_QuyetToanDuAn> GetAll()
        {
            return _QLTD_THCT_QuyetToanDuAnRepository.GetQLTD_THCT_QuyetToanDuAn();
        }

        public QLTD_THCT_QuyetToanDuAn GetById(int id)
        {
            return _QLTD_THCT_QuyetToanDuAnRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_THCT_QuyetToanDuAn> GetByIDTienDoThucHien(int idTDTH)
        {
            return _QLTD_THCT_QuyetToanDuAnRepository.GetMulti(x => x.IdTienDoThucHien == idTDTH);
        }

        public IEnumerable<QLTD_THCT_QuyetToanDuAn> GetCongViecHangNgay(int idCTCV)
        {
            DateTime now = DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0);

            var query = _QLTD_THCT_QuyetToanDuAnRepository.
                GetMulti(x => x.QLTD_TDTH_QuyetToanDuAn.IdChiTietCongViec == idCTCV
                && x.CreatedDate > startDate
                && x.CreatedDate < endDate, new string[] { "QLTD_TDTH_QuyetToanDuAn" }).
                OrderByDescending(x => x.CreatedDate);
            return query;
        }

        public IEnumerable<QLTD_THCT_QuyetToanDuAn> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_THCT_QuyetToanDuAnRepository.GetQLTD_THCT_QuyetToanDuAn();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDoThucHien == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDoThucHien == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_THCT_QuyetToanDuAn qltd_THCT_QuyetToanDuAn)
        {
            _QLTD_THCT_QuyetToanDuAnRepository.Update(qltd_THCT_QuyetToanDuAn);
        }
    }
}
