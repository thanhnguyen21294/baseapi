﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_THCT_ThucHienDuAnService
    {
        QLTD_THCT_ThucHienDuAn Add(QLTD_THCT_ThucHienDuAn qltd_THCT_ThucHienDuAn);

        void Update(QLTD_THCT_ThucHienDuAn qltd_THCT_ThucHienDuAn);

        QLTD_THCT_ThucHienDuAn Delete(int id);

        IEnumerable<QLTD_THCT_ThucHienDuAn> GetAll();

        IEnumerable<QLTD_THCT_ThucHienDuAn> GetByIDTienDoThucHien(int idTDTH);

        IEnumerable<QLTD_THCT_ThucHienDuAn> GetCongViecHangNgay(int idCTCV);

        IEnumerable<QLTD_THCT_ThucHienDuAn> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_THCT_ThucHienDuAn GetById(int id);

        void Save();
    }
    class QLTD_THCT_ThucHienDuAnService : IQLTD_THCT_ThucHienDuAnService
    {
        private IQLTD_THCT_ThucHienDuAnRepository _QLTD_THCT_ThucHienDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_THCT_ThucHienDuAnService(IQLTD_THCT_ThucHienDuAnRepository qltd_THCT_ThucHienDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_THCT_ThucHienDuAnRepository = qltd_THCT_ThucHienDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_THCT_ThucHienDuAn Add(QLTD_THCT_ThucHienDuAn qltd_THCT_ThucHienDuAn)
        {
            return _QLTD_THCT_ThucHienDuAnRepository.Add(qltd_THCT_ThucHienDuAn);
        }

        public QLTD_THCT_ThucHienDuAn Delete(int id)
        {
            return _QLTD_THCT_ThucHienDuAnRepository.Delete(id);
        }

        public IEnumerable<QLTD_THCT_ThucHienDuAn> GetAll()
        {
            return _QLTD_THCT_ThucHienDuAnRepository.GetQLTD_THCT_ThucHienDuAn();
        }

        public QLTD_THCT_ThucHienDuAn GetById(int id)
        {
            return _QLTD_THCT_ThucHienDuAnRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_THCT_ThucHienDuAn> GetByIDTienDoThucHien(int idTDTH)
        {
            return _QLTD_THCT_ThucHienDuAnRepository.GetMulti(x => x.IdTienDoThucHien == idTDTH);
        }

        public IEnumerable<QLTD_THCT_ThucHienDuAn> GetCongViecHangNgay(int idCTCV)
        {
            DateTime now = DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0);

            var query = _QLTD_THCT_ThucHienDuAnRepository.
                GetMulti(x => x.QLTD_TDTH_ThucHienDuAn.IdChiTietCongViec == idCTCV
                && x.CreatedDate > startDate
                && x.CreatedDate < endDate, new string[] { "QLTD_TDTH_ThucHienDuAn" }).
                OrderByDescending(x => x.CreatedDate);
            return query;
        }

        public IEnumerable<QLTD_THCT_ThucHienDuAn> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_THCT_ThucHienDuAnRepository.GetQLTD_THCT_ThucHienDuAn();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDoThucHien == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDoThucHien == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_THCT_ThucHienDuAn qltd_THCT_ThucHienDuAn)
        {
            _QLTD_THCT_ThucHienDuAnRepository.Update(qltd_THCT_ThucHienDuAn);
        }
    }
}
