﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_TDTH_ThucHienDuAnService
    {
        QLTD_TDTH_ThucHienDuAn Add(QLTD_TDTH_ThucHienDuAn qltd_ThucHienDuAn);

        void Update(QLTD_TDTH_ThucHienDuAn qltd_ThucHienDuAn);

        QLTD_TDTH_ThucHienDuAn Delete(int id);

        IEnumerable<QLTD_TDTH_ThucHienDuAn> GetAll();

        IEnumerable<QLTD_TDTH_ThucHienDuAn> GetByIDChiTietCongViec(int idCTCV);

        IEnumerable<QLTD_TDTH_ThucHienDuAn> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_TDTH_ThucHienDuAn GetById(int id);

        void Save();
    }
    class QLTD_TDTH_ThucHienDuAnService : IQLTD_TDTH_ThucHienDuAnService
    {
        private IQLTD_TDTH_ThucHienDuAnRepository _QLTD_TDTH_ThucHienDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_TDTH_ThucHienDuAnService(IQLTD_TDTH_ThucHienDuAnRepository qltd_ThucHienDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_TDTH_ThucHienDuAnRepository = qltd_ThucHienDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_TDTH_ThucHienDuAn Add(QLTD_TDTH_ThucHienDuAn qltd_ThucHienDuAn)
        {
            return _QLTD_TDTH_ThucHienDuAnRepository.Add(qltd_ThucHienDuAn);
        }

        public QLTD_TDTH_ThucHienDuAn Delete(int id)
        {
            return _QLTD_TDTH_ThucHienDuAnRepository.Delete(id);
        }

        public IEnumerable<QLTD_TDTH_ThucHienDuAn> GetAll()
        {
            return _QLTD_TDTH_ThucHienDuAnRepository.GetQLTD_TDTH_ThucHienDuAn();
        }

        public QLTD_TDTH_ThucHienDuAn GetById(int id)
        {
            return _QLTD_TDTH_ThucHienDuAnRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_TDTH_ThucHienDuAn> GetByIDChiTietCongViec(int idCTCV)
        {
            return _QLTD_TDTH_ThucHienDuAnRepository.GetMulti(x => x.IdChiTietCongViec == idCTCV);
        }

        public IEnumerable<QLTD_TDTH_ThucHienDuAn> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_TDTH_ThucHienDuAnRepository.GetQLTD_TDTH_ThucHienDuAn();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdChiTietCongViec == idCTCV);
            }
            else
            {
                query = query.Where(x => x.IdChiTietCongViec == idCTCV);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_TDTH_ThucHienDuAn qltd_ThucHienDuAn)
        {
            _QLTD_TDTH_ThucHienDuAnRepository.Update(qltd_ThucHienDuAn);
        }
    }
}
