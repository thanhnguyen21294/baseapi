﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CTCV_QuyetToanDuAnService
    {
        QLTD_CTCV_QuyetToanDuAn Add(QLTD_CTCV_QuyetToanDuAn qltd_CTCV_QuyetToanDuAn);

        void Update(QLTD_CTCV_QuyetToanDuAn qltd_CTCV_QuyetToanDuAn);

        QLTD_CTCV_QuyetToanDuAn Delete(int id);

        IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetAll(int idKHCV);

        QLTD_CTCV_QuyetToanDuAn GetByID(int id);

        IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetAllByIDKH(int idKH, string filter = null);

        QLTD_CTCV_QuyetToanDuAn GetByIdKHCV(int idKHCV);

        void Save();
    }

    class QLTD_CTCV_QuyetToanDuAnService : IQLTD_CTCV_QuyetToanDuAnService
    {
        private IQLTD_CTCV_QuyetToanDuAnRepository _QLTD_CTCV_QuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_CTCV_QuyetToanDuAnService(IQLTD_CTCV_QuyetToanDuAnRepository qltd_CTCV_QuyetToanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_CTCV_QuyetToanDuAnRepository = qltd_CTCV_QuyetToanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_CTCV_QuyetToanDuAn Add(QLTD_CTCV_QuyetToanDuAn qltd_CTCV_QuyetToanDuAn)
        {
            return _QLTD_CTCV_QuyetToanDuAnRepository.Add(qltd_CTCV_QuyetToanDuAn);
        }

        public QLTD_CTCV_QuyetToanDuAn Delete(int id)
        {
            return _QLTD_CTCV_QuyetToanDuAnRepository.Delete(id);
        }

        public IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetAll(int idKHCV)
        {
            return _QLTD_CTCV_QuyetToanDuAnRepository.GetQLTD_CTCV_QuyetToanDuAn().Where(x => x.IdKeHoachCongViec == idKHCV);
        }

        public IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _QLTD_CTCV_QuyetToanDuAnRepository.GetMulti(x => x.IdKeHoach == idKH, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
            if (filter != null)
            {
                query = query.Where(x => x.QLTD_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public QLTD_CTCV_QuyetToanDuAn GetByIdKHCV(int idKHCV)
        {
            return _QLTD_CTCV_QuyetToanDuAnRepository.GetQLTD_CTCV_QuyetToanDuAn().Where(x => x.IdKeHoachCongViec == idKHCV).SingleOrDefault();
        }
        public QLTD_CTCV_QuyetToanDuAn GetByID(int id)
        {
            return _QLTD_CTCV_QuyetToanDuAnRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CTCV_QuyetToanDuAn qltd_CTCV_QuyetToanDuAn)
        {
            _QLTD_CTCV_QuyetToanDuAnRepository.Update(qltd_CTCV_QuyetToanDuAn);
        }
    }
}
