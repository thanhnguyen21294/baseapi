﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_TienDoThucHienService
    {
        QLTD_TDTH_ChuTruongDauTu Add(QLTD_TDTH_ChuTruongDauTu qltd_TienDoThucHien);

        void Update(QLTD_TDTH_ChuTruongDauTu qltd_TienDoThucHien);

        QLTD_TDTH_ChuTruongDauTu Delete(int id);

        IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetAll();

        IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetByIDChiTietCongViec(int idCTCV);

        IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetByFilter(int idCTCV,int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_TDTH_ChuTruongDauTu GetById(int id);

        void Save();
    }
    class QLTD_TDTH_ChuTruongDauTuService : IQLTD_TienDoThucHienService
    {
        private IQLTD_TDTH_ChuTruongDauTuRepository _QLTD_TienDoThucHienRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_TDTH_ChuTruongDauTuService(IQLTD_TDTH_ChuTruongDauTuRepository qltd_TienDoThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_TienDoThucHienRepository = qltd_TienDoThucHienRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_TDTH_ChuTruongDauTu Add(QLTD_TDTH_ChuTruongDauTu qltd_TienDoThucHien)
        {
            return _QLTD_TienDoThucHienRepository.Add(qltd_TienDoThucHien);
        }

        public QLTD_TDTH_ChuTruongDauTu Delete(int id)
        {
            return _QLTD_TienDoThucHienRepository.Delete(id);
        }

        public IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetAll()
        {
            return _QLTD_TienDoThucHienRepository.GetQLTD_TienDoThucHien();
        }

        public QLTD_TDTH_ChuTruongDauTu GetById(int id)
        {
            return _QLTD_TienDoThucHienRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetByIDChiTietCongViec(int idCTCV)
        {
            return _QLTD_TienDoThucHienRepository.GetMulti(x => x.IdChiTietCongViec == idCTCV);
        }

        public IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_TienDoThucHienRepository.GetQLTD_TienDoThucHien();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdChiTietCongViec == idCTCV);
            }
            else
            {
                query = query.Where(x => x.IdChiTietCongViec == idCTCV);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_TDTH_ChuTruongDauTu qltd_TienDoThucHien)
        {
            _QLTD_TienDoThucHienRepository.Update(qltd_TienDoThucHien);
        }
    }
}
