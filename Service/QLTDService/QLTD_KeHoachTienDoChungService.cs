﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_KeHoachTienDoChungService
    {
        QLTD_KeHoachTienDoChung Add(QLTD_KeHoachTienDoChung qltd_KeHoachTienDoChung);

        void Update(QLTD_KeHoachTienDoChung qltd_KeHoachTienDoChung);

        QLTD_KeHoachTienDoChung Delete(int id);

        IEnumerable<object> GetByIdKeHoach(int idKH);

        QLTD_KeHoachTienDoChung GetById(int id);

        void Save();

        IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_LCTDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_CBDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_CBTH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_TH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai);
        IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_QT_For_BaoCao_TienDo(string IDDuAns);

        IEnumerable<BC_Uyban_PL1_KHTDC> Get_ND_TDC_For_BC_ThucHien_KeHoachTrienKhaiDA(int IDKH);

        IEnumerable<BC_Uyban_PL1_KHTDC_New> Get_ND_TDC_For_BC_ThucHien_KeHoachTrienKhaiDA_New(string szIDDuAn);
    }

    public class QLTD_KeHoachTienDoChungService : IQLTD_KeHoachTienDoChungService
    {
        private IQLTD_KeHoachTienDoChungRepository _QLTD_KeHoachTienDoChungRepository;
        private IQLTD_KeHoachRepository _QLTD_KeHoachRepository;
        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IAppUserService _appUserService;
        private IUnitOfWork _unitOfWork;
        public QLTD_KeHoachTienDoChungService(IQLTD_KeHoachTienDoChungRepository qltd_KeHoachTienDoChungRepository,
            IQLTD_KeHoachRepository QLTD_KeHoachRepository,
            IQLTD_KeHoachService QLTD_KeHoachService,
            IAppUserService appUserService,
            IUnitOfWork unitOfWork)
        {
            this._QLTD_KeHoachTienDoChungRepository = qltd_KeHoachTienDoChungRepository;
            this._QLTD_KeHoachRepository = QLTD_KeHoachRepository;
            this._QLTD_KeHoachService = QLTD_KeHoachService;
            _appUserService = appUserService;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_KeHoachTienDoChung Add(QLTD_KeHoachTienDoChung qltd_KeHoachTienDoChung)
        {
            return _QLTD_KeHoachTienDoChungRepository.Add(qltd_KeHoachTienDoChung);
        }

        public QLTD_KeHoachTienDoChung Delete(int id)
        {
            return _QLTD_KeHoachTienDoChungRepository.Delete(id);
        }

        public IEnumerable<object> GetByIdKeHoach(int idKH)
        {
            var users = _appUserService.GetAllUser().Select(x => new
            {
                x.Id,
                x.FullName
            }).ToList();
            var tdcs = _QLTD_KeHoachTienDoChungRepository.GetQLTD_KeHoachTienDoChung().Where(x => x.IdKeHoach == idKH).OrderByDescending(x => x.CreatedDate).ToList();
            var query = from s in tdcs
                        join u in users on s.CreatedBy equals u.Id
                        select new
                        {
                            s.IdKeHoach,
                            s.IdKeHoachTienDoChung,
                            s.NoiDung,
                            s.CreatedDate,
                            s.CreatedBy,
                            u.FullName
                        };
            return query;
        }

        public QLTD_KeHoachTienDoChung GetById(int id)
        {
            return _QLTD_KeHoachTienDoChungRepository.GetSingleByCondition(x => x.IdKeHoachTienDoChung == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_KeHoachTienDoChung qltd_KeHoachTienDoChung)
        {
            _QLTD_KeHoachTienDoChungRepository.Update(qltd_KeHoachTienDoChung);
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_LCTDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            IEnumerable<int> idskehoach;

            if(loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 2, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 2);
            }

            var lst = Get_NoiDung_TienDoChung(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_CBDT_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;

            if(loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 3);
            }

            var lst = Get_NoiDung_TienDoChung(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_CBTH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            IEnumerable<int> idskehoach;

            if(loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 4, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 4);
            }
            var lst = Get_NoiDung_TienDoChung(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_TH_For_BaoCao_TienDo_PL1(string IDDuAns, int loai)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;

            if(loai == 1)
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);
            }
            else
            {
                idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 5);
            }

            var lst = Get_NoiDung_TienDoChung(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_KHTDC_QT_For_BaoCao_TienDo(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            IEnumerable<int> idskehoach;
            idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 6);
            var lst = Get_NoiDung_TienDoChung(idskehoach);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_NoiDung_TienDoChung(IEnumerable<int> idskehoach)
        {
            var query = _QLTD_KeHoachTienDoChungRepository.GetMulti(x => idskehoach.Contains(x.IdKeHoach)).ToList();

            var lst = query.GroupBy(x => new { x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_Uyban_PL1_KHTDC()
            {
                IDDA = x.Key.IdDuAn,
                NoiDung = x.Select(y => y.NoiDung).FirstOrDefault()
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC> Get_ND_TDC_For_BC_ThucHien_KeHoachTrienKhaiDA(int IDKH)
        {
            var query = _QLTD_KeHoachTienDoChungRepository.GetMulti(x => x.IdKeHoach == IDKH).ToList();

            var lst = query.GroupBy(x => new { x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_Uyban_PL1_KHTDC()
            {
                IDDA = x.Key.IdDuAn,
                NoiDung = x.Select(y => y.NoiDung).FirstOrDefault()
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_KHTDC_New> Get_ND_TDC_For_BC_ThucHien_KeHoachTrienKhaiDA_New(string szIDDuAn)
        {
            string[] arrDuAn = szIDDuAn.Split(',');

            var lst = _QLTD_KeHoachTienDoChungRepository.GetMulti(x => arrDuAn.Contains(x.QLTD_KeHoach.IdDuAn.ToString())).
                GroupBy(x => new { x.IdKeHoach, x.QLTD_KeHoach.IdDuAn }).Select(x => new BC_Uyban_PL1_KHTDC_New()
                {
                    IDKH = x.Key.IdKeHoach,
                    IDDA = x.Key.IdDuAn,
                    NoiDung = x.Select(y => y.NoiDung).FirstOrDefault()
                });

            return lst;
        }
    }
}
