﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.BCModel.BaocaoTiendo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CTCV_ChuTruongDauTuService
    {
        QLTD_CTCV_ChuTruongDauTu Add(QLTD_CTCV_ChuTruongDauTu qltd_CTCV_ChuTruongDauTu);

        void Update(QLTD_CTCV_ChuTruongDauTu qltd_CTCV_ChuTruongDauTu);

        QLTD_CTCV_ChuTruongDauTu Delete(int id);

        IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetAll(int idKHCV);

        QLTD_CTCV_ChuTruongDauTu GetByID(int id);

        IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetAllByIDKH(int idKH, string filter = null);

        QLTD_CTCV_ChuTruongDauTu GetByIdKHCV(int idKHCV);

        IEnumerable<BC_Uyban_PL1_PDDA> Get_PDCTDT_ForBC_TienDo_PL1(string IDDuAns);

        void Save();

        #region Báo cáo tiến độ
        //IEnumerable<BC_TienDo_LCTDT> Get_LCTDT_ForBC_TienDo(string IDDuAns);
        #endregion
    }

    class QLTD_CTCV_ChuTruongDauTuService : IQLTD_CTCV_ChuTruongDauTuService
    {
        private IQLTD_CTCV_ChuTruongDauTuRepository _QLTD_CTCV_ChuTruongDauTuRepository;
        private IQLTD_KeHoachRepository _qLTD_KeHoachRepository;
        private IQLTD_KeHoachService _qLTD_KeHoachService;
        private IUnitOfWork _unitOfWork;

        public QLTD_CTCV_ChuTruongDauTuService(IQLTD_CTCV_ChuTruongDauTuRepository qltd_CTCV_ChuTruongDauTuRepository,
            IQLTD_KeHoachRepository qLTD_KeHoachRepository,
            IQLTD_KeHoachService qLTD_KeHoachService,
            IUnitOfWork unitOfWork)
        {
            this._QLTD_CTCV_ChuTruongDauTuRepository = qltd_CTCV_ChuTruongDauTuRepository;
            this._qLTD_KeHoachRepository = qLTD_KeHoachRepository;
            this._qLTD_KeHoachService = qLTD_KeHoachService;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_CTCV_ChuTruongDauTu Add(QLTD_CTCV_ChuTruongDauTu qltd_CTCV_ChuTruongDauTu)
        {
            return _QLTD_CTCV_ChuTruongDauTuRepository.Add(qltd_CTCV_ChuTruongDauTu);
        }

        public QLTD_CTCV_ChuTruongDauTu Delete(int id)
        {
            return _QLTD_CTCV_ChuTruongDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetAll(int idKHCV)
        {
            return _QLTD_CTCV_ChuTruongDauTuRepository.GetQLTD_CTCV_ChuTruongDauTu().Where(x => x.IdKeHoachCongViec == idKHCV);
        }

        public IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _QLTD_CTCV_ChuTruongDauTuRepository.GetMulti(x => x.IdKeHoach == idKH, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
            if(filter != null)
            {
                query = query.Where(x => x.QLTD_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public QLTD_CTCV_ChuTruongDauTu GetByIdKHCV(int idKHCV)
        {
            return _QLTD_CTCV_ChuTruongDauTuRepository.GetQLTD_CTCV_ChuTruongDauTu().Where(x => x.IdKeHoachCongViec == idKHCV).SingleOrDefault();
        }
        public QLTD_CTCV_ChuTruongDauTu GetByID(int id)
        {
            return _QLTD_CTCV_ChuTruongDauTuRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
        }
        
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CTCV_ChuTruongDauTu qltd_CTCV_ChuTruongDauTu)
        {
            _QLTD_CTCV_ChuTruongDauTuRepository.Update(qltd_CTCV_ChuTruongDauTu);
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_PDCTDT_ForBC_TienDo_PL1(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _qLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 2, 4);
            
            var query = _QLTD_CTCV_ChuTruongDauTuRepository
                .GetMulti(x =>
                ((idskehoach.Contains(x.IdKeHoach))
                && (x.HoanThanh == 2)
                && (arrDuAn.Contains(x.QLTD_KeHoach.IdDuAn.ToString()))
                && (x.IdCongViec == 14)
                )
                , new[] { "QLTD_KeHoach" })
                .Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                });
            return query;
        }
    }
}
