﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Service.QLTDService
{
    public interface IQLTD_TDTH_QuyetToanDuAnService
    {
        QLTD_TDTH_QuyetToanDuAn Add(QLTD_TDTH_QuyetToanDuAn qltd_QuyetToanDuAn);

        void Update(QLTD_TDTH_QuyetToanDuAn qltd_QuyetToanDuAn);

        QLTD_TDTH_QuyetToanDuAn Delete(int id);

        IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetAll();

        IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetByIDChiTietCongViec(int idCTCV);

        IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_TDTH_QuyetToanDuAn GetById(int id);

        void Save();
    }
    class QLTD_TDTH_QuyetToanDuAnService : IQLTD_TDTH_QuyetToanDuAnService
    {
        private IQLTD_TDTH_QuyetToanDuAnRepository _QLTD_TDTH_QuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_TDTH_QuyetToanDuAnService(IQLTD_TDTH_QuyetToanDuAnRepository qltd_QuyetToanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_TDTH_QuyetToanDuAnRepository = qltd_QuyetToanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_TDTH_QuyetToanDuAn Add(QLTD_TDTH_QuyetToanDuAn qltd_QuyetToanDuAn)
        {
            return _QLTD_TDTH_QuyetToanDuAnRepository.Add(qltd_QuyetToanDuAn);
        }

        public QLTD_TDTH_QuyetToanDuAn Delete(int id)
        {
            return _QLTD_TDTH_QuyetToanDuAnRepository.Delete(id);
        }

        public IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetAll()
        {
            return _QLTD_TDTH_QuyetToanDuAnRepository.GetQLTD_TDTH_QuyetToanDuAn();
        }

        public QLTD_TDTH_QuyetToanDuAn GetById(int id)
        {
            return _QLTD_TDTH_QuyetToanDuAnRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetByIDChiTietCongViec(int idCTCV)
        {
            return _QLTD_TDTH_QuyetToanDuAnRepository.GetMulti(x => x.IdChiTietCongViec == idCTCV);
        }

        public IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_TDTH_QuyetToanDuAnRepository.GetQLTD_TDTH_QuyetToanDuAn();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdChiTietCongViec == idCTCV);
            }
            else
            {
                query = query.Where(x => x.IdChiTietCongViec == idCTCV);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_TDTH_QuyetToanDuAn qltd_QuyetToanDuAn)
        {
            _QLTD_TDTH_QuyetToanDuAnRepository.Update(qltd_QuyetToanDuAn);
        }
    }
}
