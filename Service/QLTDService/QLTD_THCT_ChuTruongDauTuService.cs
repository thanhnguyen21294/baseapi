﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_THCT_ChuTruongDauTuService
    {
        QLTD_THCT_ChuTruongDauTu Add(QLTD_THCT_ChuTruongDauTu qltd_THCT_ChuTruongDauTu);

        void Update(QLTD_THCT_ChuTruongDauTu qltd_THCT_ChuTruongDauTu);

        QLTD_THCT_ChuTruongDauTu Delete(int id);

        IEnumerable<QLTD_THCT_ChuTruongDauTu> GetAll();

        IEnumerable<QLTD_THCT_ChuTruongDauTu> GetByIDTienDoThucHien(int idTDTH);

        IEnumerable<QLTD_THCT_ChuTruongDauTu> GetCongViecHangNgay(int idCTCV);

        IEnumerable<QLTD_THCT_ChuTruongDauTu> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_THCT_ChuTruongDauTu GetById(int id);

        void Save();
    }
    class QLTD_THCT_ChuTruongDauTuService : IQLTD_THCT_ChuTruongDauTuService
    {
        private IQLTD_THCT_ChuTruongDauTuRepository _QLTD_THCT_ChuTruongDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_THCT_ChuTruongDauTuService(IQLTD_THCT_ChuTruongDauTuRepository qltd_THCT_ChuTruongDauTuRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_THCT_ChuTruongDauTuRepository = qltd_THCT_ChuTruongDauTuRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_THCT_ChuTruongDauTu Add(QLTD_THCT_ChuTruongDauTu qltd_THCT_ChuTruongDauTu)
        {
            return _QLTD_THCT_ChuTruongDauTuRepository.Add(qltd_THCT_ChuTruongDauTu);
        }

        public QLTD_THCT_ChuTruongDauTu Delete(int id)
        {
            return _QLTD_THCT_ChuTruongDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_THCT_ChuTruongDauTu> GetAll()
        {
            return _QLTD_THCT_ChuTruongDauTuRepository.GetQLTD_THCT_ChuTruongDauTu();
        }

        public IEnumerable<QLTD_THCT_ChuTruongDauTu> GetCongViecHangNgay(int idCTCV)
        {
            DateTime now = DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0);

            var query = _QLTD_THCT_ChuTruongDauTuRepository.
                GetMulti(x => x.QLTD_TDTH_ChuTruongDauTu.IdChiTietCongViec == idCTCV 
                && x.CreatedDate > startDate 
                && x.CreatedDate < endDate, new string[] { "QLTD_TDTH_ChuTruongDauTu" }).
                OrderByDescending(x => x.CreatedDate);
            return query;
        }

        public QLTD_THCT_ChuTruongDauTu GetById(int id)
        {
            return _QLTD_THCT_ChuTruongDauTuRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_THCT_ChuTruongDauTu> GetByIDTienDoThucHien(int idTDTH)
        {
            return _QLTD_THCT_ChuTruongDauTuRepository.GetMulti(x => x.IdTienDoThucHien == idTDTH);
        }

        public IEnumerable<QLTD_THCT_ChuTruongDauTu> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_THCT_ChuTruongDauTuRepository.GetQLTD_THCT_ChuTruongDauTu();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDoThucHien == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDoThucHien == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_THCT_ChuTruongDauTu qltd_THCT_ChuTruongDauTu)
        {
            _QLTD_THCT_ChuTruongDauTuRepository.Update(qltd_THCT_ChuTruongDauTu);
        }
    }
}
