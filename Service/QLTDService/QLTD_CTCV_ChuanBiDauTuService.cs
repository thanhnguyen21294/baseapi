﻿using Common;
using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CTCV_ChuanBiDauTuService
    {
        QLTD_CTCV_ChuanBiDauTu Add(QLTD_CTCV_ChuanBiDauTu qltd_CTCV_ChuanBiDauTu);

        void Update(QLTD_CTCV_ChuanBiDauTu qltd_CTCV_ChuanBiDauTu);

        QLTD_CTCV_ChuanBiDauTu Delete(int id);

        IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAll(int idKHCV);

        QLTD_CTCV_ChuanBiDauTu GetByID(int id);

        IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAllByIDKH(int idKH, string filter = null);

        QLTD_CTCV_ChuanBiDauTu GetByIdKHCV(int idKHCV);

        IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAllCTCVChuaHT();

        void Save();

        IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAll();

        IEnumerable<BC_Uyban_PL1_PDDA> Get_PDDA_ForBC_TienDo_PL1(string IDDuAns);

        IEnumerable<BC_Uyban_PL1_PDDA> Get_TrinhHoSo_ForBC_TienDo_PL1(string IDDuAns);

        IEnumerable<BC_Uyban_PL1_PDDA> Get_DamBaoTienDo_ForBC_TienDo_PL1(string IDDuAns);
        Dictionary<int, DuAnTMDT> GetTMDT();
        IEnumerable<BC_Uyban_PL1_PDDA> Get_ThucHienTruoc3110_ForBC_TienDo_PL1(string IDDuAns, int nam);
        Dictionary<int, DuAnHoanThanh> GetDuAnHoanThanhCBDT(DateTime ngaySoSanh);
        IEnumerable<BC_Uyban_PL1_PDDA> Get_CBDT_DaPheDuyet_ForBC_TienDo_PL1(string IDDuAns);
    }

    class QLTD_CTCV_ChuanBiDauTuService : IQLTD_CTCV_ChuanBiDauTuService
    {
        private IQLTD_CTCV_ChuanBiDauTuRepository _QLTD_CTCV_ChuanBiDauTuRepository;
        private IQLTD_KeHoachRepository _QLTD_KeHoachRepository;
        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IQLTD_KeHoachCongViecRepository _QLTD_KeHoachCongViecRepository;
        private IUnitOfWork _unitOfWork;
        private IDuAnRepository _duAnRepository;

        public QLTD_CTCV_ChuanBiDauTuService(IQLTD_CTCV_ChuanBiDauTuRepository qltd_CTCV_ChuanBiDauTuRepository,
            IQLTD_KeHoachRepository QLTD_KeHoachRepository,
            IQLTD_KeHoachService QLTD_KeHoachService,
            IQLTD_KeHoachCongViecRepository QLTD_KeHoachCongViecRepository,
            IUnitOfWork unitOfWork,
            IDuAnRepository duAnRepository)
        {
            this._QLTD_CTCV_ChuanBiDauTuRepository = qltd_CTCV_ChuanBiDauTuRepository;
            this._QLTD_KeHoachRepository = QLTD_KeHoachRepository;
            this._QLTD_KeHoachService = QLTD_KeHoachService;
            this._QLTD_KeHoachCongViecRepository = QLTD_KeHoachCongViecRepository;
            this._unitOfWork = unitOfWork;
            _duAnRepository = duAnRepository;
        }
        public Dictionary<int,DuAnTMDT> GetTMDT()
        {
            var model = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => x.IdCongViec == 27 && x.QLTD_KeHoach.NgayTinhTrang != null, new string[] { "QLTD_KeHoach.DuAn" }).Select(x => new DuAnTMDT
            {
                IdDuAn = x.QLTD_KeHoach.IdDuAn,
                TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                IdKeHoach = x.IdKeHoach,
                NgayKeHoach = x.QLTD_KeHoach.NgayTinhTrang,
                SoQD = x.PDDA_BCKTKT_SoQD,
                TMDTPD = x.PDDA_BCKTKT_GiaTri,
                TMDTDA = x.QLTD_KeHoach.DuAn.TongMucDauTu
            }).OrderByDescending(x => x.IdDuAn).OrderByDescending(x => Convert.ToDateTime(x.NgayKeHoach)).ToList();

            List<DuAnTMDT> lstDAPD = new List<DuAnTMDT>();
            List<int> lstCheck = new List<int>();
            foreach (var item in model)
            {
                if (!lstCheck.Contains(item.IdDuAn))
                {
                    lstCheck.Add(item.IdDuAn);
                    lstDAPD.Add(item);
                }
            }
            var modelDA = _duAnRepository.GetMulti(x => lstCheck.Contains(x.IdDuAn) == false).Select(x => new DuAnTMDT
            {
                IdDuAn = x.IdDuAn,
                TenDuAn = x.TenDuAn,
                IdKeHoach = 0,
                NgayKeHoach = null,
                SoQD = "",
                TMDTPD = 0,
                TMDTDA = x.TongMucDauTu
            }).ToList();
            var dicReturn = lstDAPD.Union(modelDA).ToDictionary(x => x.IdDuAn, x => x);
            return dicReturn;
        }

        public Dictionary<int, DuAnHoanThanh> GetDuAnHoanThanhCBDT(DateTime ngaySoSanh)
        {
            var model = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => x.IdCongViec == 27 && x.NgayHoanThanh != null && x.HoanThanh ==2, new string[] { "QLTD_KeHoach.DuAn" }).Select(x => new DuAnHoanThanh
            {
                IdDuAn = x.QLTD_KeHoach.IdDuAn,
                TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                IdKeHoach = x.IdKeHoach,
                NgayKeHoach = x.QLTD_KeHoach.NgayTinhTrang,
                NgayHoanThanh = x.NgayHoanThanh,
                SoSanh = x.NgayHoanThanh < ngaySoSanh ? 1: 0
            }).OrderByDescending(x => x.IdDuAn).OrderByDescending(x => Convert.ToDateTime(x.NgayKeHoach)).ToList();

            List<DuAnHoanThanh> lstDAPD = new List<DuAnHoanThanh>();
            List<int> lstCheck = new List<int>();
            foreach (var item in model)
            {
                if (!lstCheck.Contains(item.IdDuAn))
                {
                    lstCheck.Add(item.IdDuAn);
                    lstDAPD.Add(item);
                }
            }
            var dicReturn = lstDAPD.ToDictionary(x => x.IdDuAn, x => x);
            return dicReturn;
        }
        public QLTD_CTCV_ChuanBiDauTu Add(QLTD_CTCV_ChuanBiDauTu qltd_CTCV_ChuanBiDauTu)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.Add(qltd_CTCV_ChuanBiDauTu);
        }

        public QLTD_CTCV_ChuanBiDauTu Delete(int id)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAllCTCVChuaHT()
        {
            var query = _QLTD_CTCV_ChuanBiDauTuRepository.GetAll();
            query = query.Where(x => x.NgayHoanThanh == null);
            return query;
        }

        public IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAll(int idKHCV)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetQLTD_CTCV_ChuanBiDauTu().Where(x => x.IdKeHoachCongViec == idKHCV);
        }

        public IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _QLTD_CTCV_ChuanBiDauTuRepository.GetMulti(x => x.IdKeHoach == idKH, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
            if (filter != null)
            {
                query = query.Where(x => x.QLTD_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public QLTD_CTCV_ChuanBiDauTu GetByIdKHCV(int idKHCV)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetQLTD_CTCV_ChuanBiDauTu().Where(x => x.IdKeHoachCongViec == idKHCV).SingleOrDefault();
        }
        public QLTD_CTCV_ChuanBiDauTu GetByID(int id)
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CTCV_ChuanBiDauTu qltd_CTCV_ChuanBiDauTu)
        {
            _QLTD_CTCV_ChuanBiDauTuRepository.Update(qltd_CTCV_ChuanBiDauTu);
        }

        public IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetAll()
        {
            return _QLTD_CTCV_ChuanBiDauTuRepository.GetAll();
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_CBDT_DaPheDuyet_ForBC_TienDo_PL1(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);

            var query = _QLTD_CTCV_ChuanBiDauTuRepository
                .GetMulti(x =>
                ((idskehoach.Contains(x.IdKeHoach)))
                , new[] { "QLTD_KeHoach" })
                .Select(x => new
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                }).ToList();

            var lst = query.GroupBy(x => new { x.IDDA }).Select(x => new BC_Uyban_PL1_PDDA()
            {
                IDDA = x.Key.IDDA
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_PDDA_ForBC_TienDo_PL1(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);
            
            var query = _QLTD_CTCV_ChuanBiDauTuRepository
                .GetMulti(x => 
                ((idskehoach.Contains(x.IdKeHoach)) 
                && (x.HoanThanh == 2) 
                && (arrDuAn.Contains(x.QLTD_KeHoach.IdDuAn.ToString()))
                && (x.PDDA_BCKTKT_SoQD != null)
                && (x.PDDA_BCKTKT_NgayPD != null)
                && (x.PDDA_BCKTKT_GiaTri != null)
                )
                , new[] { "QLTD_KeHoach" })
                .Select(x => new 
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                }).ToList();

            var lst = query.GroupBy(x => new { x.IDDA }).Select(x => new BC_Uyban_PL1_PDDA()
            {
                IDDA = x.Key.IDDA
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_TrinhHoSo_ForBC_TienDo_PL1(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);

            var query = _QLTD_CTCV_ChuanBiDauTuRepository
                .GetMulti(x =>
                ((idskehoach.Contains(x.IdKeHoach))
                && (x.HoanThanh == 2)
                && (arrDuAn.Contains(x.QLTD_KeHoach.IdDuAn.ToString()))
                && (x.IdCongViec == 26)
                )
                , new[] { "QLTD_KeHoach" })
                .Select(x => new
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                }).ToList();

            var lst = query.GroupBy(x => new { x.IDDA }).Select(x => new BC_Uyban_PL1_PDDA()
            {
                IDDA = x.Key.IDDA
            });

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DamBaoTienDo_ForBC_TienDo_PL1(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);

            DateTime now = DateTime.Now;
            DateTime compDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var q1 = _QLTD_KeHoachCongViecRepository.GetMulti(x =>
                (
                (idskehoach.Contains(x.IdKeHoach))
                && (x.IdCongViec == 27)
                && (x.DenNgay != null)
                )).GroupBy(x => new { x.IdKeHoach }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());

            var query = _QLTD_CTCV_ChuanBiDauTuRepository
                .GetMulti(x =>
                (
                (q1.Contains(x.IdKeHoach))
                && (((x.NgayHoanThanh == null) && (DateTime.Now < x.QLTD_KeHoachCongViec.DenNgay))
                || ((x.NgayHoanThanh != null) && (x.NgayHoanThanh < x.QLTD_KeHoachCongViec.DenNgay)))
                )
                , new[] { "QLTD_KeHoach", "QLTD_KeHoachCongViec" })
                .GroupBy(x => new { x.QLTD_KeHoach.IdDuAn })
                .Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.Key.IdDuAn
                });

            var a = query.ToList();

            return query;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_ThucHienTruoc3110_ForBC_TienDo_PL1(string IDDuAns, int nam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 3, 4);
            DateTime compDate = new DateTime(nam, 11, 1, 0, 0, 0);

            var query = _QLTD_KeHoachCongViecRepository.GetMulti(x => (
                (idskehoach.Contains(x.IdKeHoach))
                && (x.IdCongViec == 27)
                && (x.DenNgay != null)
                && (x.DenNgay > compDate)
                ), new[] { "QLTD_KeHoach" }).Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                });

            return query;
        }

    }
}
