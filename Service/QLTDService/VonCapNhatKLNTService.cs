﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.BCModel;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IVonCapNhatKLNTService
    {
        VonCapNhatKLNT Add(VonCapNhatKLNT vonCapNhat);

        IEnumerable<VonCapNhatKLNT> GetAll();

        void Save();

        IEnumerable<VonCapNhatKLNT> GetFileById(int idFile, int page, int pageSize, string sort, out int totalRow, string filter);

        VonCapNhatKLNT GetById(int idDA, int idFile);

        void Update(VonCapNhatKLNT vonCapNhatKLNT);

        List<int> ListIntFile(int id);

        Dictionary<int, List<object>> GetGiaiNganVon(string IDDuAns);

        VonCapNhatKLNT Delete(int id);

        Dictionary<int, List<object>> GetKLTH(string IDDuAns);

        IEnumerable<BC_ThucHien_KeHoachTrienKhaiDuAn_NguonVon> Get_CayBaoCao_For_BaoCao_ThucHien_KeHoach_TrienKhaiDuAn(string IDDuAns, int iNam, Dictionary<string, string> lstUserNhanVien);

        IEnumerable<BC_KeHoachTrienKhaiDuAn_TDGN> GetGiaTriAll(string IDDuAns, int iNam, Dictionary<string, string> lstUserNhanVien);
    }

    public class VonCapNhatKLNTService : IVonCapNhatKLNTService
    {
        private IVonCapNhatKLNTRepository _vonCapNhatKLNTRepository;
        private IUnitOfWork _unitOfWork;

        public VonCapNhatKLNTService(IVonCapNhatKLNTRepository vonCapNhatKLNTRepository, IUnitOfWork unitOfWork)
        {
            this._vonCapNhatKLNTRepository = vonCapNhatKLNTRepository;
            this._unitOfWork = unitOfWork;
        }

        public VonCapNhatKLNT Add(VonCapNhatKLNT vonCapNhat)
        {
            return _vonCapNhatKLNTRepository.Add(vonCapNhat);
        }

        public IEnumerable<VonCapNhatKLNT> GetAll()
        {
            return _vonCapNhatKLNTRepository.GetAll(new string[] {"DuAn"});
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<VonCapNhatKLNT> GetFileById(int idFile, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _vonCapNhatKLNTRepository.GetVonCapNhatKLNT();

            if (filter != null)
            {
                query = query.Where(x => x.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()));
            }

            query = query.Where(x => x.IdFile == idFile);
            totalRow = query.Count();
            return query.OrderBy(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public VonCapNhatKLNT GetById(int idDA, int idFile)
        {
            var a = _vonCapNhatKLNTRepository.GetAll().Where(x => x.IdDuAn == idDA && x.IdFile == idFile).FirstOrDefault();
            return _vonCapNhatKLNTRepository.GetAll().Where(x => x.IdDuAn == idDA && x.IdFile == idFile).FirstOrDefault();
        }

        public void Update(VonCapNhatKLNT vonCapNhatKLNT)
        {
            _vonCapNhatKLNTRepository.Update(vonCapNhatKLNT);
        }


        public List<int> ListIntFile(int id)
        {
            return _vonCapNhatKLNTRepository.GetAll().Where(x => x.IdFile == id).Select(x => x.Id).ToList<int>();
        }

        public VonCapNhatKLNT Delete(int id)
        {
            return _vonCapNhatKLNTRepository.Delete(id);
        }

        public Dictionary<int, List<object>> GetGiaiNganVon(string IDDuAns)
        {
            var lst = _vonCapNhatKLNTRepository.GetGiaiNganVon(IDDuAns);
            string[] szAr = { "GiaiNganVonHuyen", "GiaiNganVonTP" };
            Dictionary<int, List<object>> dic = GetDicIntList(lst, szAr);

            //var dic = (from item in
            //              (from item in lst
            //               select new
            //               {
            //                   IDDA = (int)GetValueObject(item, "IDDA"),
            //                   sz1 = GetValueObject(item, "GiaiNganVonHuyen"),
            //                   sz2 = GetValueObject(item, "GiaiNganVonTP")
            //               })
            //           group item by new { item.IDDA }
            //          into item1
            //           select new
            //           {
            //               IDDA = item1.Key.IDDA,
            //               grp = item1.Select(x => new
            //               {
            //                   str1 = x.sz1,
            //                   str2 = x.sz2,
            //               }).ToList<object>()
            //           }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetKLTH(string IDDuAns)
        {
            var lst = _vonCapNhatKLNTRepository.GetKLTH(IDDuAns);
            string[] szAr = { "KLTHVonHuyen", "KLTHVonTP" };
            Dictionary<int, List<object>> dic = GetDicIntList(lst, szAr);

            return dic;
        }

        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }

        public Dictionary<int, List<object>> GetDicIntList(List<object> lst, string[] szAr)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, szAr[0]),
                               sz2 = GetValueObject(item, szAr[1])
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public IEnumerable<BC_ThucHien_KeHoachTrienKhaiDuAn_NguonVon> Get_CayBaoCao_For_BaoCao_ThucHien_KeHoach_TrienKhaiDuAn(string IDDuAns, int iNam, Dictionary<string, string> lstUserNhanVien)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var q1 = _vonCapNhatKLNTRepository
                .GetMulti(x =>
                ((IDDuAns.Contains(x.IdDuAn.ToString()))
                && (x.CapNhatKLNT.CreatedDate.Value.Year == iNam)
                && (x.isVonTP))
                , new string[] { "CapNhatKLNT", "DuAn", "DuAn.NhomDuAnTheoUser", "DuAn.DuAnUsers" })
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    //NVon = "Vốn Ngân sách Huyện",
                    NVon = "Vốn Tập trung thành phố",
                    STT = 0,
                    IDDA = x.Key.IdDuAn,
                    TenDuAn = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.TenDuAn).FirstOrDefault(),
                    TenPhong = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.NhomDuAnTheoUser.TenNhomDuAn).FirstOrDefault(),
                    GiaTri = x.OrderByDescending(y => y.CreateDateTime).Select(y => y.TDGNVonTP).FirstOrDefault(),
                    Users = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct()).FirstOrDefault(),
                });

            var q2 = _vonCapNhatKLNTRepository
                .GetMulti(x =>
                ((IDDuAns.Contains(x.IdDuAn.ToString()))
                && (x.CapNhatKLNT.CreatedDate.Value.Year == iNam)
                && (x.isVonHuyen))
                , new string[] { "CapNhatKLNT", "DuAn", "DuAn.NhomDuAnTheoUser", "DuAn.DuAnUsers" })
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    NVon = "Vốn Ngân sách Huyện",
                    //NVon = "Vốn Tập trung thành phố",
                    STT = 1,
                    IDDA = x.Key.IdDuAn,
                    TenDuAn = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.TenDuAn).FirstOrDefault(),
                    TenPhong = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.NhomDuAnTheoUser.TenNhomDuAn).FirstOrDefault(),
                    GiaTri = x.OrderByDescending(y => y.CreateDateTime).Select(y => y.TDGNVonHuyen).FirstOrDefault(),
                    Users = x.OrderByDescending(y => y.IdFile).Select(y => y.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).Distinct()).FirstOrDefault(),
                });

            var qr = q2.Union(q1);

            var query = qr.GroupBy(x => new { x.NVon, x.STT })
                .Select(x => new BC_ThucHien_KeHoachTrienKhaiDuAn_NguonVon()
                {
                    TenNV = x.Key.NVon,
                    STT = x.Key.STT,
                    grpDuAn = x.GroupBy(y => new {  y.IDDA, y.TenDuAn }).Select(y => new BC_ThucHien_KeHoachTrienKhaiDuAn_DuAn()
                    {
                        IDDA = y.Key.IDDA,
                        TenDuAn = y.Key.TenDuAn,
                        TenPhong = y.Select(z => z.TenPhong).FirstOrDefault(),
                        Users = y.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        GiaTriNV = y.Select(z => z.GiaTri).FirstOrDefault()
                    })
                }).OrderBy(o => o.STT);

            return query;
        }

        public IEnumerable<BC_KeHoachTrienKhaiDuAn_TDGN> GetGiaTriAll(string IDDuAns, int iNam, Dictionary<string, string> lstUserNhanVien)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var query = _vonCapNhatKLNTRepository
                .GetMulti(x =>
                ((IDDuAns.Contains(x.IdDuAn.ToString()))
                && (x.CapNhatKLNT.CreatedDate.Value.Year == iNam))
                , new string[] { "CapNhatKLNT" })
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new BC_KeHoachTrienKhaiDuAn_TDGN
                {
                    IDDA = x.Key.IdDuAn,
                    MaDuAn = x.OrderByDescending(y => y.IdFile).Select(y => y.MaDuAn).FirstOrDefault(),
                    TDGNHuyen = x.OrderByDescending(y => y.CreateDateTime).Select(y => y.TDGNVonHuyen).FirstOrDefault(),
                    TDGNTP = x.OrderByDescending(y => y.CreateDateTime).Select(y => y.TDGNVonTP).FirstOrDefault()
                });

            return query;
        }
    }
}
