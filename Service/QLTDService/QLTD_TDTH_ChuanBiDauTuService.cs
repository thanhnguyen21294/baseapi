﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Service.QLTDService
{
    public interface IQLTD_TDTH_ChuanBiDauTuService
    {
        QLTD_TDTH_ChuanBiDauTu Add(QLTD_TDTH_ChuanBiDauTu qltd_ChuanBiDauTu);

        void Update(QLTD_TDTH_ChuanBiDauTu qltd_ChuanBiDauTu);

        QLTD_TDTH_ChuanBiDauTu Delete(int id);

        IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetAll();

        IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetByIDChiTietCongViec(int idCTCV);

        IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_TDTH_ChuanBiDauTu GetById(int id);

        IEnumerable<BC_ThucHien_KeHoachTrienKhaiDuAn_TDTH> Get_TenTienDo_CBDT_For_BaoCaoThucHienTrienKhaiDA(int IDKH);

        object GetAllCTCVChuaHT(string idUser,int idNhomDuAnTheoUser);
        object GetPageCTCVChuaHT(string idUser, int idNhomDuAnTheoUser, int Numberpage, int page, string filter, Dictionary<string, string> dicUser);
        object GetCountAllCTCVChuaHT(string idUser, int idNhomDuAnTheoUser);
        void Save();
    }
    class QLTD_TDTH_ChuanBiDauTuService : IQLTD_TDTH_ChuanBiDauTuService
    {
        private IQLTD_TDTH_ChuanBiDauTuRepository _QLTD_TDTH_ChuanBiDauTuRepository;
        private IQLTD_THCT_ChuanBiDauTuRepository _QLTD_THCT_ChuanBiDauTuRepository;
        private IQLTD_TDTH_ChuanBiThucHienRepository _QLTD_TDTH_ChuanBiThucHienRepository;
        private IQLTD_THCT_ChuanBiThucHienRepository _QLTD_THCT_ChuanBiThucHienRepository;
        private IQLTD_TDTH_ChuTruongDauTuRepository _QLTD_TDTH_ChuTruongDauTuRepository;
        private IQLTD_THCT_ChuTruongDauTuRepository _QLTD_THCT_ChuTruongDauTuRepository;
        private IQLTD_TDTH_ThucHienDuAnRepository _QLTD_TDTH_ThucHienDuAnRepository;
        private IQLTD_THCT_ThucHienDuAnRepository _QLTD_THCT_ThucHienDuAnRepository;
        private IQLTD_TDTH_QuyetToanDuAnRepository _QLTD_TDTH_QuyetToanDuAnRepository;
        private IQLTD_THCT_QuyetToanDuAnRepository _QLTD_THCT_QuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_TDTH_ChuanBiDauTuService(
            IQLTD_TDTH_ChuanBiDauTuRepository qltd_ChuanBiDauTuRepository,
            IQLTD_THCT_ChuanBiDauTuRepository qltd_THCT_ChuanBiDauTuRepository,
            IQLTD_TDTH_ChuanBiThucHienRepository qltd_TDTH_ChuanBiThucHienRepository,
            IQLTD_THCT_ChuanBiThucHienRepository qltd_THCT_ChuanBiThucHienRepository,
            IQLTD_TDTH_ChuTruongDauTuRepository qltd_TDTH_ChuTruongDauTuRepository,
            IQLTD_THCT_ChuTruongDauTuRepository qltd_THCT_ChuTruongDauTuRepository,
            IQLTD_TDTH_ThucHienDuAnRepository qltd_TDTH_ThucHienDuAnRepository,
            IQLTD_THCT_ThucHienDuAnRepository qltd_THCT_ThucHienDuAnRepository,
            IQLTD_TDTH_QuyetToanDuAnRepository qltd_TDTH_QuyetToanDuAnRepository,
            IQLTD_THCT_QuyetToanDuAnRepository qltd_THCT_QuyetToanDuAnRepository,
            IUnitOfWork unitOfWork)
        {
            _QLTD_TDTH_ChuanBiDauTuRepository = qltd_ChuanBiDauTuRepository;
            _QLTD_THCT_ChuanBiDauTuRepository = qltd_THCT_ChuanBiDauTuRepository;
            _QLTD_TDTH_ChuanBiThucHienRepository = qltd_TDTH_ChuanBiThucHienRepository;
            _QLTD_THCT_ChuanBiThucHienRepository = qltd_THCT_ChuanBiThucHienRepository;

            _QLTD_TDTH_ChuTruongDauTuRepository = qltd_TDTH_ChuTruongDauTuRepository;
            _QLTD_THCT_ChuTruongDauTuRepository = qltd_THCT_ChuTruongDauTuRepository;

            _QLTD_TDTH_ThucHienDuAnRepository = qltd_TDTH_ThucHienDuAnRepository;
            _QLTD_THCT_ThucHienDuAnRepository = qltd_THCT_ThucHienDuAnRepository;

            _QLTD_TDTH_QuyetToanDuAnRepository = qltd_TDTH_QuyetToanDuAnRepository;
            _QLTD_THCT_QuyetToanDuAnRepository = qltd_THCT_QuyetToanDuAnRepository;
            _unitOfWork = unitOfWork;
        }

        public QLTD_TDTH_ChuanBiDauTu Add(QLTD_TDTH_ChuanBiDauTu qltd_ChuanBiDauTu)
        {
            return _QLTD_TDTH_ChuanBiDauTuRepository.Add(qltd_ChuanBiDauTu);
        }

        public QLTD_TDTH_ChuanBiDauTu Delete(int id)
        {
            return _QLTD_TDTH_ChuanBiDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetAll()
        {
            return _QLTD_TDTH_ChuanBiDauTuRepository.GetQLTD_TDTH_ChuanBiDauTu();
        }

        public object GetAllCTCVChuaHT(string idUser, int idNhomDuAnTheoUser)
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstCTDTDaBam = _QLTD_THCT_ChuTruongDauTuRepository.GetAll().Where(x => 
            DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, 
            ((DateTime)x.CreatedDate).Month, 
            ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();

            var lstCTDTs = _QLTD_TDTH_ChuTruongDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuTruongDauTu", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec" })
                .Where(x => !lstCTDTDaBam.Contains(x.IdTienDoThucHien) 
                && x.QLTD_CTCV_ChuTruongDauTu.NgayHoanThanh == null 
                && x.HoanThanh != 2
                && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuTruongDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 2,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            t.NgayBatDau,
                            t.NgayHoanThanh,
                            TinhTrang = t.NgayHoanThanh < now ? 1:0,
                            SubDate = t.NgayHoanThanh != null ? (now.Date - t.NgayHoanThanh.Value.Date).Days : 0,
                            Ngay1 = t.NgayBatDau != null ? (now.Date - t.NgayBatDau.Value.Date).Days : 0,
                            Ngay2 = (t.NgayHoanThanh != null && t.NgayBatDau != null) ? (t.NgayHoanThanh.Value.Date - t.NgayBatDau.Value.Date).Days : 0,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 2
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCTDT = lstCTDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 2,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        z.NgayBatDau,
                        z.NgayHoanThanh,
                        z.TinhTrang,
                        z.SubDate,
                        z.Ngay1,
                        z.Ngay2,
                        IDGiaiDoan = 2
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBDTDaBam = _QLTD_THCT_ChuanBiDauTuRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBDTs = _QLTD_TDTH_ChuanBiDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiDauTu", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec" })
                .Where(x => !lstCBDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new {
                    
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 3,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            t.NgayBatDau,
                            t.NgayHoanThanh,
                            TinhTrang = t.NgayHoanThanh < DateTime.Now ? 1 : 0,
                            SubDate = t.NgayHoanThanh != null ? (now.Date - t.NgayHoanThanh.Value.Date).Days : 0,
                            Ngay1 = t.NgayBatDau != null ? (now.Date - t.NgayBatDau.Value.Date).Days : 0,
                            Ngay2 = (t.NgayHoanThanh != null && t.NgayBatDau != null) ? (t.NgayHoanThanh.Value.Date - t.NgayBatDau.Value.Date).Days : 0,
                            IDGiaiDoan = 3
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBDT = lstCBDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 3,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        z.NgayBatDau,
                        z.NgayHoanThanh,
                        z.TinhTrang,
                        z.SubDate,
                        z.Ngay1,
                        z.Ngay2,
                        IDGiaiDoan = 3
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBTHDaBam = _QLTD_THCT_ChuanBiThucHienRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBTHs = _QLTD_TDTH_ChuanBiThucHienRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiThucHien", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec" })
                .Where(x => !lstCBTHDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiThucHien.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiThucHien.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 4,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            t.NgayBatDau,
                            t.NgayHoanThanh,
                            TinhTrang = t.NgayHoanThanh < DateTime.Now ? 1 : 0,
                            SubDate = t.NgayHoanThanh != null ? (now.Date - t.NgayHoanThanh.Value.Date).Days : 0,
                            Ngay1 = t.NgayBatDau != null ? (now.Date - t.NgayBatDau.Value.Date).Days : 0,
                            Ngay2 = (t.NgayHoanThanh != null && t.NgayBatDau != null) ? (t.NgayHoanThanh.Value.Date - t.NgayBatDau.Value.Date).Days : 0,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBTH = lstCBTHs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 4,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        z.NgayBatDau,
                        z.NgayHoanThanh,
                        z.TinhTrang,
                        z.SubDate,
                        z.Ngay1,
                        z.Ngay2,
                        IDGiaiDoan = 4
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstTHDADaBam = _QLTD_THCT_ThucHienDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstTHDAs = _QLTD_TDTH_ThucHienDuAnRepository.GetAll(new string[] { "QLTD_CTCV_ThucHienDuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ThucHienDuAn.QLTD_CongViec" })
                .Where(x => !lstTHDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ThucHienDuAn.NgayHoanThanh == null && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ThucHienDuAn.IdKeHoach,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 5,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            t.NgayBatDau,
                            t.NgayHoanThanh,
                            TinhTrang = t.NgayHoanThanh < DateTime.Now ? 1 : 0,
                            SubDate = t.NgayHoanThanh != null ? (now.Date - t.NgayHoanThanh.Value.Date).Days : 0,
                            Ngay1 = t.NgayBatDau != null ? (now.Date - t.NgayBatDau.Value.Date).Days : 0,
                            Ngay2 = (t.NgayHoanThanh != null && t.NgayBatDau != null) ? (t.NgayHoanThanh.Value.Date - t.NgayBatDau.Value.Date).Days : 0,
                            IDGiaiDoan = 5
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstTHDA = lstTHDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 5,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        z.NgayBatDau,
                        z.NgayHoanThanh,
                        z.TinhTrang,
                        z.SubDate,
                        z.Ngay1,
                        z.Ngay2,
                        IDGiaiDoan = 5
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstQTDADaBam = _QLTD_THCT_QuyetToanDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstQTDAs = _QLTD_TDTH_QuyetToanDuAnRepository.GetAll(new string[] { "QLTD_CTCV_QuyetToanDuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec" })
                .Where(x => !lstQTDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_QuyetToanDuAn.NgayHoanThanh == null && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_QuyetToanDuAn.IdKeHoach,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.TenCongViec })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 6,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            t.NgayBatDau,
                            t.NgayHoanThanh,
                            TinhTrang = t.NgayHoanThanh < DateTime.Now ? 1 : 0,
                            SubDate = t.NgayHoanThanh != null ? (now.Date - t.NgayHoanThanh.Value.Date).Days : 0,
                            Ngay1 = t.NgayBatDau != null ? (now.Date - t.NgayBatDau.Value.Date).Days : 0,
                            Ngay2 = (t.NgayHoanThanh != null && t.NgayBatDau != null) ? (t.NgayHoanThanh.Value.Date - t.NgayBatDau.Value.Date).Days : 0,
                            IDGiaiDoan = 6
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstQTDA = lstQTDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 6,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        z.NgayBatDau,
                        z.NgayHoanThanh,
                        z.TinhTrang,
                        z.SubDate,
                        z.Ngay1,
                        z.Ngay2,
                        IDGiaiDoan = 6
                    }).ToList()
                }).ToList()
            }).ToList();
            if (idNhomDuAnTheoUser != 13)
            {
                var re = lstCTDT.Union(lstCBDT).Union(lstCBTH).Union(lstTHDA).Union(lstQTDA).GroupBy(x => new { x.IDDA, x.TenDA }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                return re;
            }
            else
            {
                var re = lstQTDA.GroupBy(x => new { x.IDDA, x.TenDA }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                return re;
            }
        }

        public QLTD_TDTH_ChuanBiDauTu GetById(int id)
        {
            return _QLTD_TDTH_ChuanBiDauTuRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetByIDChiTietCongViec(int idCTCV)
        {
            return _QLTD_TDTH_ChuanBiDauTuRepository.GetMulti(x => x.IdChiTietCongViec == idCTCV);
        }

        public IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetByFilter(int idCTCV, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_TDTH_ChuanBiDauTuRepository.GetQLTD_TDTH_ChuanBiDauTu();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdChiTietCongViec == idCTCV);
            }
            else
            {
                query = query.Where(x => x.IdChiTietCongViec == idCTCV);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_TDTH_ChuanBiDauTu qltd_ChuanBiDauTu)
        {
            _QLTD_TDTH_ChuanBiDauTuRepository.Update(qltd_ChuanBiDauTu);
        }

        // Duy
        public object GetCountAllCTCVChuaHT(string idUser, int idNhomDuAnTheoUser)
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstCTDTDaBam = _QLTD_THCT_ChuTruongDauTuRepository.GetAll().Where(x =>
            DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year,
            ((DateTime)x.CreatedDate).Month,
            ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();

            var lstCTDTs = _QLTD_TDTH_ChuTruongDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuTruongDauTu", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec" })
                .Where(x => !lstCTDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuTruongDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuTruongDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 2,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 2
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCTDT = lstCTDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 2,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 2
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBDTDaBam = _QLTD_THCT_ChuanBiDauTuRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBDTs = _QLTD_TDTH_ChuanBiDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiDauTu", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec" })
                .Where(x => !lstCBDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new {

                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.TenDuAn
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 3,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 3
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBDT = lstCBDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 3,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 3
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBTHDaBam = _QLTD_THCT_ChuanBiThucHienRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBTHs = _QLTD_TDTH_ChuanBiThucHienRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiThucHien", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec" })
                .Where(x => !lstCBTHDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiThucHien.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiThucHien.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 4,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBTH = lstCBTHs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 4,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 4
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstTHDADaBam = _QLTD_THCT_ThucHienDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstTHDAs = _QLTD_TDTH_ThucHienDuAnRepository.GetAll(new string[] { "QLTD_CTCV_ThucHienDuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ThucHienDuAn.QLTD_CongViec" })
                .Where(x => !lstTHDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ThucHienDuAn.NgayHoanThanh == null && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ThucHienDuAn.IdKeHoach,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 5,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 5
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstTHDA = lstTHDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 5,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 5
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstQTDADaBam = _QLTD_THCT_QuyetToanDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstQTDAs = _QLTD_TDTH_QuyetToanDuAnRepository.GetAll(new string[] { "QLTD_CTCV_QuyetToanDuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec" })
                .Where(x => !lstQTDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_QuyetToanDuAn.NgayHoanThanh == null && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.IdDuAn, g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.TenDuAn })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_QuyetToanDuAn.IdKeHoach,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 6,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 6
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstQTDA = lstQTDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 6,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 6
                    }).ToList()
                }).ToList()
            }).ToList();
            if (idNhomDuAnTheoUser != 13)
            {
                var re = lstCTDT.Union(lstCBDT).Union(lstCBTH).Union(lstTHDA).Union(lstQTDA).GroupBy(x => new { x.IDDA, x.TenDA }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                var count = re.Count();
                return count;
            }
            else
            {
                var re = lstQTDA.GroupBy(x => new { x.IDDA, x.TenDA }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                var count = re.Count();
                return count;
            }
        }
        public object GetPageCTCVChuaHT(string idUser, int idNhomDuAnTheoUser, int Numberpage, int page, string filter, Dictionary<string, string> dicUser)
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstCTDTDaBam = _QLTD_THCT_ChuTruongDauTuRepository.GetAll().Where(x =>
            DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year,
            ((DateTime)x.CreatedDate).Month,
            ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();

            var lstCTDTs = _QLTD_TDTH_ChuTruongDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuTruongDauTu", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec" })
                .Where(x => !lstCTDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuTruongDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.TenDuAn,
                    g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                    g.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.DuAn.DuAnUsers
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuTruongDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuTruongDauTu.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 2,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 2
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCTDT = lstCTDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAnUsers.ToList().
                      Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 2,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 2
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBDTDaBam = _QLTD_THCT_ChuanBiDauTuRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBDTs = _QLTD_TDTH_ChuanBiDauTuRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiDauTu", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec" })
                .Where(x => !lstCBDTDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiDauTu.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new {
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.TenDuAn,
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                    g.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.DuAn.DuAnUsers
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiDauTu.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiDauTu.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 3,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 3
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBDT = lstCBDTs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAnUsers.ToList().
                      Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 3,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 3
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstCBTHDaBam = _QLTD_THCT_ChuanBiThucHienRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstCBTHs = _QLTD_TDTH_ChuanBiThucHienRepository.GetAll(new string[] { "QLTD_CTCV_ChuanBiThucHien", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn", "QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec" })
                .Where(x => !lstCBTHDaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ChuanBiThucHien.NgayHoanThanh == null && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.TenDuAn,
                    g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                    g.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.DuAn.DuAnUsers
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ChuanBiThucHien.IdKeHoach,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ChuanBiThucHien.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 4,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 4
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstCBTH = lstCBTHs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAnUsers.ToList().
                     Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 4,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 4
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstTHDADaBam = _QLTD_THCT_ThucHienDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstTHDAs = _QLTD_TDTH_ThucHienDuAnRepository.GetAll(new string[] { "QLTD_CTCV_ThucHienDuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_ThucHienDuAn.QLTD_CongViec" })
                .Where(x => !lstTHDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_ThucHienDuAn.NgayHoanThanh == null && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.TenDuAn,
                    g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                    g.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.DuAn.DuAnUsers
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers= s.Key.DuAnUsers,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_ThucHienDuAn.IdKeHoach,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_ThucHienDuAn.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 5,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 5
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstTHDA = lstTHDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAnUsers.ToList()
                .Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 5,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 5
                    }).ToList()
                }).ToList()
            }).ToList();
            var lstQTDADaBam = _QLTD_THCT_QuyetToanDuAnRepository.GetAll().Where(x => DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year, ((DateTime)x.CreatedDate).Month, ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdTienDoThucHien).ToList().Distinct();
            var lstQTDAs = _QLTD_TDTH_QuyetToanDuAnRepository.GetAll(new string[] { "QLTD_CTCV_QuyetToanDuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn", "QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers", "QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec" })
                .Where(x => !lstQTDADaBam.Contains(x.IdTienDoThucHien) && x.QLTD_CTCV_QuyetToanDuAn.NgayHoanThanh == null && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.TrangThai == 4
                && x.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0)
                .GroupBy(g => new { g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.IdDuAn,
                    g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.TenDuAn,
                    g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                    g.QLTD_CTCV_QuyetToanDuAn.QLTD_KeHoach.DuAn.DuAnUsers
                })
                .Select(s => new
                {
                    IDDA = s.Key.IdDuAn,
                    TenDA = s.Key.TenDuAn,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.GroupBy(g => new {
                        g.QLTD_CTCV_QuyetToanDuAn.IdKeHoach,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.IdCongViec,
                        g.QLTD_CTCV_QuyetToanDuAn.QLTD_CongViec.TenCongViec
                    })
                    .Select(y => new
                    {
                        IDKH = y.Key.IdKeHoach,
                        IDCV = y.Key.IdCongViec,
                        TenCV = y.Key.TenCongViec,
                        IDGiaiDoan = 6,
                        grpThucHien = y.Select(t => new
                        {
                            IdTienDoThucHien = t.IdTienDoThucHien,
                            NoiDung = t.NoiDung,
                            IDGiaiDoan = 6
                        }).ToList()
                    }).ToList()
                }).ToList();
            var lstQTDA = lstQTDAs.Select(x => new
            {
                IDDA = x.IDDA,
                TenDA = x.TenDA,
                IdNhomDuAnTheoUser = x.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.DuAnUsers.ToList().
                      Select(y => y.UserId)).Where(a => a == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                grpKH = x.grpKH.Select(y => new
                {
                    IDKH = y.IDKH,
                    IDCV = y.IDCV,
                    TenCV = y.TenCV,
                    IDGiaiDoan = 6,
                    grpThucHien = y.grpThucHien.Select(z => new
                    {
                        IdTienDoThucHien = z.IdTienDoThucHien,
                        NoiDung = z.NoiDung,
                        IDGiaiDoan = 6
                    }).ToList()
                }).ToList()
            }).ToList();
            if (idNhomDuAnTheoUser != 13)
            {
                var re = lstCTDT.Union(lstCBDT).Union(lstCBTH).Union(lstTHDA).Union(lstQTDA).GroupBy(x => new { x.IDDA, x.TenDA, x.IdNhomDuAnTheoUser, x.DuAnUsers }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                if (filter != null)
                {
                    re = re.Where(x => x.TenDA.ToLower().Contains(filter.ToLower()));
                }
                var results = (re.OrderBy(x => x.IDDA)
             .Skip((page - 1) * Numberpage)
             .Take(Numberpage));
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", re.Count() % Numberpage > 0 ? ((re.Count() / Numberpage) + 1) : (re.Count() / Numberpage));
                dictionary2.Add("TotalItems", re.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", results);
                dictionary.Add("Page", dictionary2);
                return dictionary;
            }
            else
            {
                var re = lstQTDA.GroupBy(x => new { x.IDDA, x.TenDA, x.IdNhomDuAnTheoUser, x.DuAnUsers }).Select(s => new
                {
                    IDDA = s.Key.IDDA,
                    TenDA = s.Key.TenDA,
                    IdNhomDuAnTheoUser = s.Key.IdNhomDuAnTheoUser,
                    DuAnUsers = s.Key.DuAnUsers,
                    grpKH = s.Select(a => a.grpKH).ToList()
                });
                if (filter != null)
                {
                    re = re.Where(x => x.TenDA.ToLower().Contains(filter.ToLower()));
                }
                var results = (re.OrderBy(x => x.IDDA)
              .Skip((page - 1) * Numberpage)
              .Take(Numberpage));
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", re.Count() % Numberpage > 0 ? ((re.Count() / Numberpage) + 1) : (re.Count() / Numberpage));
                dictionary2.Add("TotalItems", re.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", results);
                dictionary.Add("Page", dictionary2);
                return dictionary;
                //return re;
            }
        }


        public IEnumerable<BC_ThucHien_KeHoachTrienKhaiDuAn_TDTH> Get_TenTienDo_CBDT_For_BaoCaoThucHienTrienKhaiDA(int IDKH)
        {
            var qr = _QLTD_TDTH_ChuanBiDauTuRepository
                .GetMulti(x => x.QLTD_CTCV_ChuanBiDauTu.IdKeHoach == IDKH, new string[] { "QLTD_CTCV_ChuanBiDauTu" })
                .GroupBy(x => new { x.QLTD_CTCV_ChuanBiDauTu.IdKeHoach })
                .Select(x => new BC_ThucHien_KeHoachTrienKhaiDuAn_TDTH()
                {
                    ND = x.Select(y => y.NoiDung).FirstOrDefault()
                });
            return qr;
        }
    }
}
