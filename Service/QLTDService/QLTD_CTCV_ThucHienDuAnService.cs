﻿using Common;
using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_CTCV_ThucHienDuAnService
    {
        QLTD_CTCV_ThucHienDuAn Add(QLTD_CTCV_ThucHienDuAn qltd_CTCV_ThucHienDuAn);

        void Update(QLTD_CTCV_ThucHienDuAn qltd_CTCV_ThucHienDuAn);

        QLTD_CTCV_ThucHienDuAn Delete(int id);

        IEnumerable<QLTD_CTCV_ThucHienDuAn> GetAll(int idKHCV);

        QLTD_CTCV_ThucHienDuAn GetByID(int id);
        IEnumerable<QLTD_CTCV_ThucHienDuAn> GetThongKe();

        IEnumerable<QLTD_CTCV_ThucHienDuAn> GetAllByIDKH(int idKH, string filter = null);

        QLTD_CTCV_ThucHienDuAn GetByIdKHCV(int idKHCV);

        IEnumerable<BC_Uyban_PL1_PDDA> Get_DangThucHienGPMB_For_BC_Uyban_PL2(string IDDuAns);

        IEnumerable<BC_Uyban_PL1_PDDA> Get_DangLuaChonNhaThau_For_BC_Uyban_PL2(string IDDuAns);
        IEnumerable<BC_Uyban_PL1_PDDA> Get_DangTrienKhaiThiCong_For_BC_Uyban_PL2(string IDDuAns);
        IEnumerable<BC_Uyban_PL1_PDDA> Get_DaHoanThanhThiCong_For_BC_Uyban_PL2(string IDDuAns);
        Dictionary<int, DuAnHoanThanh> GetDuAnHoanThanhTHDT(DateTime ngaySoSanh);
        IEnumerable<BC_Uyban_PL1_PDDA> Get_DanhSo_For_CV_CTHLCNT_For_BC_TienDo_ThucHien(string IDDuAns);
        void Save();
    }

    class QLTD_CTCV_ThucHienDuAnService : IQLTD_CTCV_ThucHienDuAnService
    {
        private IQLTD_CTCV_ThucHienDuAnRepository _QLTD_CTCV_ThucHienDuAnRepository;
        private IQLTD_KeHoachRepository _QLTD_KeHoachRepository;
        private IQLTD_KeHoachService _QLTD_KeHoachService;
        private IUnitOfWork _unitOfWork;

        public QLTD_CTCV_ThucHienDuAnService(IQLTD_CTCV_ThucHienDuAnRepository qltd_CTCV_ThucHienDuAnRepository,
            IQLTD_KeHoachRepository QLTD_KeHoachRepository,
            IQLTD_KeHoachService QLTD_KeHoachService,
            IUnitOfWork unitOfWork)
        {
            this._QLTD_CTCV_ThucHienDuAnRepository = qltd_CTCV_ThucHienDuAnRepository;
            this._QLTD_KeHoachRepository = QLTD_KeHoachRepository;
            this._QLTD_KeHoachService = QLTD_KeHoachService;
            this._unitOfWork = unitOfWork;
        }
        public IEnumerable<QLTD_CTCV_ThucHienDuAn> GetThongKe()
        {
            var query = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => x.IdCongViec == 44 && x.NgayHoanThanh != null, new[] { "QLTD_KeHoachCongViec.QLTD_KeHoach.DuAn", "QLTD_CongViec" });
            return query;
        }
        public QLTD_CTCV_ThucHienDuAn Add(QLTD_CTCV_ThucHienDuAn qltd_CTCV_ThucHienDuAn)
        {
            return _QLTD_CTCV_ThucHienDuAnRepository.Add(qltd_CTCV_ThucHienDuAn);
        }

        public QLTD_CTCV_ThucHienDuAn Delete(int id)
        {
            return _QLTD_CTCV_ThucHienDuAnRepository.Delete(id);
        }
        public Dictionary<int, DuAnHoanThanh> GetDuAnHoanThanhTHDT(DateTime ngaySoSanh)
        {
            var model = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => x.IdCongViec == 44 && x.NgayHoanThanh != null && x.HoanThanh == 2, new string[] { "QLTD_KeHoach.DuAn" }).Select(x => new DuAnHoanThanh
            {
                IdDuAn = x.QLTD_KeHoach.IdDuAn,
                TenDuAn = x.QLTD_KeHoach.DuAn.TenDuAn,
                IdKeHoach = x.IdKeHoach,
                NgayKeHoach = x.QLTD_KeHoach.NgayTinhTrang,
                NgayHoanThanh = x.NgayHoanThanh,
                SoSanh = x.NgayHoanThanh < ngaySoSanh ? 1 : 0
            }).OrderByDescending(x => x.IdDuAn).OrderByDescending(x => Convert.ToDateTime(x.NgayKeHoach)).ToList();

            List<DuAnHoanThanh> lstDAPD = new List<DuAnHoanThanh>();
            List<int> lstCheck = new List<int>();
            foreach (var item in model)
            {
                if (!lstCheck.Contains(item.IdDuAn))
                {
                    lstCheck.Add(item.IdDuAn);
                    lstDAPD.Add(item);
                }
            }
            var dicReturn = lstDAPD.ToDictionary(x => x.IdDuAn, x => x);
            return dicReturn;
        }
        public IEnumerable<QLTD_CTCV_ThucHienDuAn> GetAll(int idKHCV)
        {
            return _QLTD_CTCV_ThucHienDuAnRepository.GetQLTD_CTCV_ThucHienDuAn().Where(x => x.IdKeHoachCongViec == idKHCV);
        }

        public IEnumerable<QLTD_CTCV_ThucHienDuAn> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => x.IdKeHoach == idKH, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
            if (filter != null)
            {
                query = query.Where(x => x.QLTD_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public QLTD_CTCV_ThucHienDuAn GetByIdKHCV(int idKHCV)
        {
            return _QLTD_CTCV_ThucHienDuAnRepository.GetQLTD_CTCV_ThucHienDuAn().Where(x => x.IdKeHoachCongViec == idKHCV).SingleOrDefault();
        }
        public QLTD_CTCV_ThucHienDuAn GetByID(int id)
        {
            return _QLTD_CTCV_ThucHienDuAnRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "QLTD_KeHoachCongViec", "QLTD_CongViec" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_CTCV_ThucHienDuAn qltd_CTCV_ThucHienDuAn)
        {
            _QLTD_CTCV_ThucHienDuAnRepository.Update(qltd_CTCV_ThucHienDuAn);
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DangThucHienGPMB_For_BC_Uyban_PL2(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);
            var lst = _QLTD_CTCV_ThucHienDuAnRepository.GetMulti(x => ((idskehoach.Contains(x.IdKeHoach)) && (x.IdCongViec == 34) && (x.GPMB_DamBaoTienDo != null) && x.HoanThanh < 2), new string[] { "QLTD_KeHoach" })
                .Select(x => new BC_Uyban_PL1_PDDA() { IDDA = x.QLTD_KeHoach.IdDuAn });
            //var lst = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 34, 2, 1);
            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DangLuaChonNhaThau_For_BC_Uyban_PL2(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);
            
            var q1 = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 35, 2, 2);
            var q2 = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 36, 2, 1);

            var lst = from item in q1
                      join item1 in q2
                      on item.IDDA equals item1.IDDA
                      select new BC_Uyban_PL1_PDDA()
                      {
                          IDDA = item.IDDA
                      };

            var a = lst.ToList();

            return lst;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DangTrienKhaiThiCong_For_BC_Uyban_PL2(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);

            var query = _QLTD_CTCV_ThucHienDuAnRepository
                .GetMulti(x =>
                ((idskehoach.Contains(x.IdKeHoach))
                && (x.IdCongViec == 37)
                && (x.DTCXD_NgayBanGiaoMB != null)
                && (x.HoanThanh < 1))
                , new string[] { "QLTD_KeHoach" })
                .Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                });

            //var q1 = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 36, 2, 2);
            //var q2 = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 37, 2, 1);

            //var lst = from item in q1
            //          join item1 in q2
            //          on item.IDDA equals item1.IDDA
            //          select new BC_Uyban_PL1_PDDA()
            //          {
            //              IDDA = item.IDDA
            //          };

            //var a = lst.ToList();

            return query;
        }

        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DaHoanThanhThiCong_For_BC_Uyban_PL2(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach(arrDuAn, 5, 4);
            var lst = Get_IDDA_For_BaoCao_Uyban_PL2(idskehoach, 38, 2, 2);
            var a = lst.ToList();
            return lst;
        }



        public IEnumerable<BC_Uyban_PL1_PDDA> Get_IDDA_For_BaoCao_Uyban_PL2(IEnumerable<int> idskehoach, int IDCV, int iHT, int loai)
        {
            IEnumerable<BC_Uyban_PL1_PDDA> query = new List<BC_Uyban_PL1_PDDA>();

            if (loai == 1)
            {
                query = _QLTD_CTCV_ThucHienDuAnRepository
                    .GetMulti(x =>
                    ((idskehoach.Contains(x.IdKeHoach))
                    && (x.IdCongViec == IDCV)
                    && (x.HoanThanh < iHT)
                    )
                    , new[] { "QLTD_KeHoach" })
                    .Select(x => new BC_Uyban_PL1_PDDA()
                    {
                        IDDA = x.QLTD_KeHoach.IdDuAn
                    });
            }
            else
            {
                query = _QLTD_CTCV_ThucHienDuAnRepository
                    .GetMulti(x =>
                    ((idskehoach.Contains(x.IdKeHoach))
                    && (x.IdCongViec == IDCV)
                    && (x.HoanThanh == iHT)
                    )
                    , new[] { "QLTD_KeHoach" })
                    .Select(x => new BC_Uyban_PL1_PDDA()
                    {
                        IDDA = x.QLTD_KeHoach.IdDuAn
                    });
            }

            return query;
        }


        public IEnumerable<BC_Uyban_PL1_PDDA> Get_DanhSo_For_CV_CTHLCNT_For_BC_TienDo_ThucHien(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var idskehoach = _QLTD_KeHoachService.Get_IdsKeHoach_For_BaoCao_TienDo_TheoGiaiDoan(arrDuAn, 5);
            var query = _QLTD_CTCV_ThucHienDuAnRepository
                .GetMulti(x => (
                (idskehoach.Contains(x.IdKeHoach))
                && (x.IdCongViec == 35)
                && (x.HoanThanh < 1)
                && (
                (x.CTHLCNT_DangLapTKBVTC != null) 
                || (x.CTHLCNT_DangTrinhTKBVTC != null) 
                || (x.CTHLCNT_KhoKhan != null) 
                || (x.CTHLCNT_NgayPheDuyetKHLCNT != null) 
                || (x.CTHLCNT_SoNgayQDPD_KHLCNT != null) 
                || (x.CTHLCNT_SoQDKHLCNT != null))
                )
                , new string[] { "QLTD_KeHoach" })
                .Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                });
            return query;
        }
    }
}
