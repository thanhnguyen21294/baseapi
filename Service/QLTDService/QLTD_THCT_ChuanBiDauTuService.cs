﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface IQLTD_THCT_ChuanBiDauTuService
    {
        QLTD_THCT_ChuanBiDauTu Add(QLTD_THCT_ChuanBiDauTu qltd_THCT_ChuanBiDauTu);

        void Update(QLTD_THCT_ChuanBiDauTu qltd_THCT_ChuanBiDauTu);

        QLTD_THCT_ChuanBiDauTu Delete(int id);

        IEnumerable<QLTD_THCT_ChuanBiDauTu> GetAll();

        IEnumerable<QLTD_THCT_ChuanBiDauTu> GetByIDTienDoThucHien(int idTDTH);

        IEnumerable<QLTD_THCT_ChuanBiDauTu> GetCongViecHangNgay(int idCTCV);

        IEnumerable<QLTD_THCT_ChuanBiDauTu> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        QLTD_THCT_ChuanBiDauTu GetById(int id);

        void Save();
    }
    class QLTD_THCT_ChuanBiDauTuService : IQLTD_THCT_ChuanBiDauTuService
    {
        private IQLTD_THCT_ChuanBiDauTuRepository _QLTD_THCT_ChuanBiDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_THCT_ChuanBiDauTuService(IQLTD_THCT_ChuanBiDauTuRepository qltd_THCT_ChuanBiDauTuRepository, IUnitOfWork unitOfWork)
        {
            this._QLTD_THCT_ChuanBiDauTuRepository = qltd_THCT_ChuanBiDauTuRepository;
            this._unitOfWork = unitOfWork;
        }

        public QLTD_THCT_ChuanBiDauTu Add(QLTD_THCT_ChuanBiDauTu qltd_THCT_ChuanBiDauTu)
        {
            return _QLTD_THCT_ChuanBiDauTuRepository.Add(qltd_THCT_ChuanBiDauTu);
        }

        public QLTD_THCT_ChuanBiDauTu Delete(int id)
        {
            return _QLTD_THCT_ChuanBiDauTuRepository.Delete(id);
        }

        public IEnumerable<QLTD_THCT_ChuanBiDauTu> GetAll()
        {
            return _QLTD_THCT_ChuanBiDauTuRepository.GetQLTD_THCT_ChuanBiDauTu();
        }

        public QLTD_THCT_ChuanBiDauTu GetById(int id)
        {
            return _QLTD_THCT_ChuanBiDauTuRepository.GetSingleByCondition(x => x.IdTienDoThucHien == id);
        }

        public IEnumerable<QLTD_THCT_ChuanBiDauTu> GetByIDTienDoThucHien(int idTDTH)
        {
            return _QLTD_THCT_ChuanBiDauTuRepository.GetMulti(x => x.IdTienDoThucHien == idTDTH);
        }

        public IEnumerable<QLTD_THCT_ChuanBiDauTu> GetCongViecHangNgay(int idCTCV)
        {
            DateTime now = DateTime.Today;
            DateTime startDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            DateTime endDate = new DateTime(now.Year, now.Month, now.Day, 23, 0, 0);

            var query = _QLTD_THCT_ChuanBiDauTuRepository.
                GetMulti(x => x.QLTD_TDTH_ChuanBiDauTu.IdChiTietCongViec == idCTCV
                && x.CreatedDate > startDate
                && x.CreatedDate < endDate, new string[] { "QLTD_TDTH_ChuanBiDauTu" }).
                OrderByDescending(x => x.CreatedDate);
            return query;
        }

        public IEnumerable<QLTD_THCT_ChuanBiDauTu> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QLTD_THCT_ChuanBiDauTuRepository.GetQLTD_THCT_ChuanBiDauTu();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDoThucHien == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDoThucHien == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QLTD_THCT_ChuanBiDauTu qltd_THCT_ChuanBiDauTu)
        {
            _QLTD_THCT_ChuanBiDauTuRepository.Update(qltd_THCT_ChuanBiDauTu);
        }
    }
}
