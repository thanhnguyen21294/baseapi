﻿using Data.Infrastructure;
using Data.Repositories.QLTDRepository;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.QLTDService
{
    public interface ICapNhatKLNTService
    {
        CapNhatKLNT Add(CapNhatKLNT capnhat);

        IEnumerable<CapNhatKLNT> GetAll();

        void Save();

        CapNhatKLNT GetById(int id);

        CapNhatKLNT Delete(int id);
    }

    public class CapNhatKLNTService : ICapNhatKLNTService
    {
        private ICapNhatKLNTRepository _CapNhatKLNTRepository;
        private IUnitOfWork _unitOfWork;

        public CapNhatKLNTService(ICapNhatKLNTRepository CapNhatKLNTRepository, IUnitOfWork unitOfWork)
        {
            this._CapNhatKLNTRepository = CapNhatKLNTRepository;
            this._unitOfWork = unitOfWork;
        }

        public CapNhatKLNT Add(CapNhatKLNT capnhat)
        {
            return _CapNhatKLNTRepository.Add(capnhat);
        }

        public IEnumerable<CapNhatKLNT> GetAll()
        {
            return _CapNhatKLNTRepository.GetCapNhatKLNT();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public CapNhatKLNT GetById(int id)
        {
            return _CapNhatKLNTRepository.GetMulti(x => x.IdFile == id).SingleOrDefault();
        }

        public CapNhatKLNT Delete(int id)
        {
            return _CapNhatKLNTRepository.Delete(id);
        }
    }
}
