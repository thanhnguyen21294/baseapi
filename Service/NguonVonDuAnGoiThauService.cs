﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface INguonVonDuAnGoiThauService
    {
        NguonVonDuAnGoiThau Add(NguonVonDuAnGoiThau nguonVonDuAnGoiThau);

        void Update(NguonVonDuAnGoiThau loaiCapCongTrinh);

        NguonVonDuAnGoiThau Delete(int id);

        IEnumerable<NguonVonDuAnGoiThau> GetAll();

        IEnumerable<NguonVonDuAnGoiThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);
        IEnumerable<NguonVonDuAnGoiThau> GetListByIdGoiThau(int idGoiThau);
        NguonVonDuAnGoiThau GetById(int id);

        void Save();
    }
    public class NguonVonDuAnGoiThauService : INguonVonDuAnGoiThauService
    {
        private INguonVonDuAnGoiThauRepository _nguonVonDuAnGoiThauRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonDuAnGoiThauService(INguonVonDuAnGoiThauRepository nguonVonDuAnGoiThauRepository, IUnitOfWork unitOfWork)
        {
            this._nguonVonDuAnGoiThauRepository = nguonVonDuAnGoiThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public NguonVonDuAnGoiThau Add(NguonVonDuAnGoiThau nguonVonDuAnGoiThau)
        {
            return _nguonVonDuAnGoiThauRepository.Add(nguonVonDuAnGoiThau);
        }

        public NguonVonDuAnGoiThau Delete(int id)
        {
            return _nguonVonDuAnGoiThauRepository.Delete(id);
        }

        public IEnumerable<NguonVonDuAnGoiThau> GetAll()
        {
            return _nguonVonDuAnGoiThauRepository.GetAll();
        }

        public IEnumerable<NguonVonDuAnGoiThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _nguonVonDuAnGoiThauRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdNguonVonDuAnGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<NguonVonDuAnGoiThau> GetListByIdGoiThau(int idGoiThau)
        {
            return _nguonVonDuAnGoiThauRepository.GetMulti(x => x.IdGoiThau == idGoiThau, new string[] {"NguonVonDuAn" });
        }

        public NguonVonDuAnGoiThau GetById(int id)
        {
            return _nguonVonDuAnGoiThauRepository.GetMulti(x => x.IdNguonVonDuAnGoiThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NguonVonDuAnGoiThau nguonVonDuAnGoiThau)
        {
            _nguonVonDuAnGoiThauRepository.Update(nguonVonDuAnGoiThau);
        }
    }
}
