﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IBaoLanhTHHDService
    {
        BaoLanhTHHD Add(BaoLanhTHHD BaoLanhTHHD);

        void Update(BaoLanhTHHD BaoLanhTHHD);

        BaoLanhTHHD Delete(int id);

        IEnumerable<BaoLanhTHHD> GetAll();

        IEnumerable<BaoLanhTHHD> GetByIDDuAn(int idDA);

        IEnumerable<BaoLanhTHHD> GetByFilter(int idDA, int page, int pageSize, out int totalRow, string filter = null);

        BaoLanhTHHD GetById(int id);

        IEnumerable<BaoLanhTHHD> GetByNgayHetHan(string IDDuAns, string szNgayHetHan);

        void Save();
    }

    public class BaoLanhTHHDService : IBaoLanhTHHDService
    {
        private IBaoLanhTHHDRepository _BaoLanhTHHDRepository;
        private IUnitOfWork _unitOfWork;

        public BaoLanhTHHDService(IBaoLanhTHHDRepository BaoLanhTHHDRepository, IUnitOfWork unitOfWork)
        {
            this._BaoLanhTHHDRepository = BaoLanhTHHDRepository;
            this._unitOfWork = unitOfWork;
        }
        public BaoLanhTHHD Add(BaoLanhTHHD BaoLanhTHHD)
        {
            return _BaoLanhTHHDRepository.Add(BaoLanhTHHD);
        }

        public BaoLanhTHHD Delete(int id)
        {
            return _BaoLanhTHHDRepository.Delete(id);
        }

        public IEnumerable<BaoLanhTHHD> GetAll()
        {
            return _BaoLanhTHHDRepository.GetAll();
        }
        public IEnumerable<BaoLanhTHHD> GetByIDDuAn(int idDA)
        {
            return _BaoLanhTHHDRepository.GetMulti(x => x.IdDuAn == idDA);
        }

        public IEnumerable<BaoLanhTHHD> GetByNgayHetHan(string IDDuAns, string szNgayHetHan)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            DateTime dt = Convert.ToDateTime(szNgayHetHan);
            return _BaoLanhTHHDRepository.GetMulti(x => x.NgayHetHan != null && (arrDuAn.Contains(x.HopDong.IdDuAn.ToString()) && x.NgayHetHan <= dt), new string[] { "HopDong.GoiThau.DuAn.NhomDuAnTheoUser", "HopDong.GoiThau.DuAn.DuAnUsers.AppUsers" });
        }
        public IEnumerable<BaoLanhTHHD> GetByFilter(int idDA, int page, int pageSize, out int totalRow, string filter = null)
        {
            var query = _BaoLanhTHHDRepository.GetMulti(x => x.IdDuAn == idDA, new string[]{"HopDong"});
            if (filter != null)
            {
                query = query.Where(x => x.SoBaoLanh.ToLower().Contains(filter.ToLower()));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdBaoLanh).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public BaoLanhTHHD GetById(int id)
        {
            return _BaoLanhTHHDRepository.GetMulti(x => x.IdBaoLanh == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(BaoLanhTHHD BaoLanhTHHD)
        {
            _BaoLanhTHHDRepository.Update(BaoLanhTHHD);
        }
    }
}
