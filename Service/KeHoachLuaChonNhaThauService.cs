﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface IKeHoachLuaChonNhaThauService
    {
        KeHoachLuaChonNhaThau Add(KeHoachLuaChonNhaThau keHoachLuaChonNhaThau);

        void Update(KeHoachLuaChonNhaThau keHoachLuaChonNhaThau);

        KeHoachLuaChonNhaThau Delete(int id);

        IEnumerable<KeHoachLuaChonNhaThau> GetAll(int idDuAn);

        IEnumerable<KeHoachLuaChonNhaThau> GetAllForHistory();

        IEnumerable<KeHoachLuaChonNhaThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, out int totalRowfull, string filter);

        KeHoachLuaChonNhaThau GetById(int id);
        IEnumerable<KeHoachLuaChonNhaThau> GetChild(int idDuAn);

        void Save();
    }

    public class KeHoachLuaChonNhaThauService : IKeHoachLuaChonNhaThauService
    {
        private IKeHoachLuaChonNhaThauRepository _keHoachLuaChonNhaThauRepository;
        private IUnitOfWork _unitOfWork;

        public KeHoachLuaChonNhaThauService(IKeHoachLuaChonNhaThauRepository keHoachLuaChonNhaThauRepository, IUnitOfWork unitOfWork)
        {
            this._keHoachLuaChonNhaThauRepository = keHoachLuaChonNhaThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public KeHoachLuaChonNhaThau Add(KeHoachLuaChonNhaThau keHoachLuaChonNhaThau)
        {
            return _keHoachLuaChonNhaThauRepository.Add(keHoachLuaChonNhaThau);
        }

        public KeHoachLuaChonNhaThau Delete(int id)
        {
            return _keHoachLuaChonNhaThauRepository.Delete(id);
        }

        public IEnumerable<KeHoachLuaChonNhaThau> GetAll(int idDuAn)
        {
            return _keHoachLuaChonNhaThauRepository.GetMulti(x=>x.IdDuAn == idDuAn, new string[] { "CoQuanPheDuyet" });
        }

        public IEnumerable<KeHoachLuaChonNhaThau> GetAllForHistory()
        {
            return _keHoachLuaChonNhaThauRepository.GetAll().OrderByDescending(x=>x.IdKeHoachLuaChonNhaThau);
        }

        public IEnumerable<KeHoachLuaChonNhaThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow,out int totalRowfull, string filter)
        {
            List<KeHoachLuaChonNhaThau> search = new List<KeHoachLuaChonNhaThau>();
            var query = _keHoachLuaChonNhaThauRepository.GetMulti(x=>x.IdDuAn == idDuAn,new string[]{ "CoQuanPheDuyet"});
            if (filter != null)
            {
                //query = query.Where(x => x.NoiDung.Contains(filter));
                var listSearch = query.Where(x => x.NoiDung.Contains(filter));
                foreach (var duAn in listSearch)
                {
                    var items = search.Where(x => x.IdKeHoachLuaChonNhaThau == duAn.IdKeHoachLuaChonNhaThau);
                    if (!items.Any())
                    {
                        search.Add(duAn);
                        search = search.Union(FindAllParents(query.ToList(), duAn.IdDieuChinhKeHoachLuaChonNhaThau)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdDieuChinhKeHoachLuaChonNhaThau == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdDuAn).Skip((page - 1) * pageSize).Take(pageSize);

                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                totalRowfull = parents.Count();
                return parents;
            }
            else
            {
                //totalRow = query.Count();
                //var keHoach = query.Where(x=>x.IdDieuChinhKeHoachLuaChonNhaThau==null).OrderBy(x => x.IdKeHoachLuaChonNhaThau).Skip((page - 1) * pageSize).Take(pageSize);
                //if (!keHoach.Any() && page > 1)
                //{
                //    page = page - 1;
                //    keHoach = query.Where(x => x.IdDieuChinhKeHoachLuaChonNhaThau == null).OrderBy(x => x.IdKeHoachLuaChonNhaThau).Skip((page - 1) * pageSize).Take(pageSize);
                //}
                totalRow = query.Where(x=>x.IdDieuChinhKeHoachLuaChonNhaThau==null).Count();
                totalRowfull = query.Count();
                var parent = query.Where(y => y.IdDieuChinhKeHoachLuaChonNhaThau == null).OrderBy(x => x.IdKeHoachLuaChonNhaThau).Skip((page - 1) * pageSize).Take(pageSize);
                if (!parent.Any() && page > 1)
                {
                    page = page - 1;
                    parent = query.Where(y => y.IdDieuChinhKeHoachLuaChonNhaThau == null).OrderBy(x => x.IdKeHoachLuaChonNhaThau).Skip((page - 1) * pageSize).Take(pageSize);
                }
                //totalRow = query.Count(x => x.IdDieuChinhKeHoachLuaChonNhaThau == null);
                
                foreach (var item in parent)
                {
                    parent = parent.Union(GetChild(query, item));
                }
                return parent;
            }
        }
        private IEnumerable<KeHoachLuaChonNhaThau> FindAllParents(List<KeHoachLuaChonNhaThau> all_data, int? child)
        {
            var parent = all_data.FirstOrDefault(x => x.IdKeHoachLuaChonNhaThau == child);

            if (parent == null)
                return Enumerable.Empty<KeHoachLuaChonNhaThau>();

            return new[] { parent }.Concat(FindAllParents(all_data, parent.IdDieuChinhKeHoachLuaChonNhaThau));
        }

        private IEnumerable<KeHoachLuaChonNhaThau> GetChild(IEnumerable<KeHoachLuaChonNhaThau> duAns, KeHoachLuaChonNhaThau duAn)
        {
            return duAns.Where(x => x.IdDieuChinhKeHoachLuaChonNhaThau == duAn.IdKeHoachLuaChonNhaThau || x.IdKeHoachLuaChonNhaThau == duAn.IdKeHoachLuaChonNhaThau)
                .Union(duAns.Where(x => x.IdDieuChinhKeHoachLuaChonNhaThau == duAn.IdKeHoachLuaChonNhaThau).SelectMany(y => GetChild(duAns, y)));
        }

        public KeHoachLuaChonNhaThau GetById(int id)
        {
            return _keHoachLuaChonNhaThauRepository.GetSingleById(id);
        }

        public IEnumerable<KeHoachLuaChonNhaThau> GetChild(int idDuAn)
        {
            var query = _keHoachLuaChonNhaThauRepository.GetMulti(x => x.IdDuAn == idDuAn).ToList();
            List<KeHoachLuaChonNhaThau> resual = new List<KeHoachLuaChonNhaThau>();
            foreach (var item in query.Where(x=>x.IdDieuChinhKeHoachLuaChonNhaThau == null))
            {
                resual = resual.Union(FindChild(query, item).ToList()).ToList() ;
            }
            return resual;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(KeHoachLuaChonNhaThau keHoachLuaChonNhaThau)
        {
            _keHoachLuaChonNhaThauRepository.Update(keHoachLuaChonNhaThau);
        }

        IEnumerable<KeHoachLuaChonNhaThau> FindChild(List<KeHoachLuaChonNhaThau> allData, KeHoachLuaChonNhaThau keHoach)
        {
            List<KeHoachLuaChonNhaThau> list = new List<KeHoachLuaChonNhaThau>();
            var child = allData.Where(x => x.IdKeHoachLuaChonNhaThau == keHoach.IdDieuChinhKeHoachLuaChonNhaThau);
            if (child.Count() == 0)
            {
                list.Add(keHoach);
            }
            else
            {
                foreach (var item in child)
                {
                    list.Union(FindChild(allData, item));
                }
            }
            return list;
        }
    }
}
