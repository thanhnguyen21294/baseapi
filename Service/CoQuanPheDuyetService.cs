﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ICoQuanPheDuyetService
    {
        CoQuanPheDuyet Add(CoQuanPheDuyet coQuanPheDuyet);

        void Update(CoQuanPheDuyet coQuanPheDuyet);

        CoQuanPheDuyet Delete(int id);

        IEnumerable<CoQuanPheDuyet> GetAll();

        IEnumerable<CoQuanPheDuyet> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        CoQuanPheDuyet GetById(int id);

        void Save();
    }

    public class CoQuanPheDuyetService : ICoQuanPheDuyetService
    {
        private ICoQuanPheDuyetRepository _coQuanPheDuyetRepository;
        private IUnitOfWork _unitOfWork;

        public CoQuanPheDuyetService(ICoQuanPheDuyetRepository coQuanPheDuyetRepository, IUnitOfWork unitOfWork)
        {
            this._coQuanPheDuyetRepository = coQuanPheDuyetRepository;
            this._unitOfWork = unitOfWork;
        }
        public CoQuanPheDuyet Add(CoQuanPheDuyet coQuanPheDuyet)
        {
            return _coQuanPheDuyetRepository.Add(coQuanPheDuyet);
        }

        public CoQuanPheDuyet Delete(int id)
        {
            return _coQuanPheDuyetRepository.Delete(id);
        }

        public IEnumerable<CoQuanPheDuyet> GetAll()
        {
            return _coQuanPheDuyetRepository.GetAll();
        }

        public IEnumerable<CoQuanPheDuyet> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _coQuanPheDuyetRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenCoQuanPheDuyet.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdCoQuanPheDuyet).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public CoQuanPheDuyet GetById(int id)
        {
            return _coQuanPheDuyetRepository.GetMulti(x => x.IdCoQuanPheDuyet == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(CoQuanPheDuyet coQuanPheDuyet)
        {
            _coQuanPheDuyetRepository.Update(coQuanPheDuyet);
        }
    }
}
