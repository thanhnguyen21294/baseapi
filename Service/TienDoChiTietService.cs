﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ITienDoChiTietService
    {
        TienDoChiTiet Add(TienDoChiTiet iTienDoThucHien);

        void Update(TienDoChiTiet iTienDoThucHien);

        TienDoChiTiet Delete(int id);
        void DeleteByIdTienDoThucHien(int idTDTH);

        IEnumerable<TienDoChiTiet> GetAll();

        IEnumerable<TienDoChiTiet> GetAll(string filter);

        IEnumerable<TienDoChiTiet> GetbyIdTienDoThucHien(int idTDTH);
        IEnumerable<TienDoChiTiet> GetbyIdTienDoThucHien(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);
        IEnumerable<TienDoChiTiet> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter);

        TienDoChiTiet GetById(int id);

        void Save();
    }
    public class TienDoChiTietService : ITienDoChiTietService
    {
        private ITienDoChiTietRepository _TienDoChiTietRepository;
        private IUnitOfWork _unitOfWork;

        public TienDoChiTietService(ITienDoChiTietRepository tienDoChiTietRepository, IUnitOfWork unitOfWork)
        {
            this._TienDoChiTietRepository = tienDoChiTietRepository;
            this._unitOfWork = unitOfWork;
        }
        public TienDoChiTiet Add(TienDoChiTiet chuTruongDauTu)
        {
            return _TienDoChiTietRepository.Add(chuTruongDauTu);
        }

        public TienDoChiTiet Delete(int id)
        {
            return _TienDoChiTietRepository.Delete(id);
        }
        public void DeleteByIdTienDoThucHien(int idIdTDTH)
        {
            var lstDel = _TienDoChiTietRepository.GetMulti(x => x.IdTienDo == idIdTDTH);
            foreach(var del in lstDel)
            {
                _TienDoChiTietRepository.Delete(del.IdTienDoChiTiet);
            }
        }
        
        public IEnumerable<TienDoChiTiet> GetAll()
        {
            return _TienDoChiTietRepository.GetAll();
        }

        public IEnumerable<TienDoChiTiet> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _TienDoChiTietRepository.GetMulti(x => x.NoiDung.Contains(filter));
            }
            return _TienDoChiTietRepository.GetAll();
        }
        public IEnumerable<TienDoChiTiet> GetbyIdTienDoThucHien(int idTDTH)
        {
            var query = _TienDoChiTietRepository.GetAll();
            query = query.Where(x => x.IdTienDo == idTDTH);
            return query.OrderBy(x => x.IdTienDoChiTiet);
        }
        public IEnumerable<TienDoChiTiet> GetbyIdTienDoThucHien(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _TienDoChiTietRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDo == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDo == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoChiTiet).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<TienDoChiTiet> GetByFilter(int idTDTH, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _TienDoChiTietRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdTienDo == idTDTH);
            }
            else
            {
                query = query.Where(x => x.IdTienDo == idTDTH);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdTienDoChiTiet).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public TienDoChiTiet GetById(int id)
        {
            return _TienDoChiTietRepository.GetMulti(x => x.IdTienDoChiTiet == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(TienDoChiTiet updateTienDoChiTiet)
        {
            _TienDoChiTietRepository.Update(updateTienDoChiTiet);
        }
    }
}
