﻿using Data.Infrastructure;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Models;

namespace Service
{
    public interface ICommentService
    {
        Comment Add(Comment Comments);

        void Update(Comment id);

        Comment Delete(int id);

        IEnumerable<Comment> GetAll();

        IEnumerable<Comment> GetByIdDuAn(int idDuAn);

        Comment GetById(int id);

        void Save();
    }
    public class CommentService : ICommentService
    {
        private ICommentsRepository _CommentsRepository;
        private IUnitOfWork _unitOfWork;

        public CommentService(ICommentsRepository CommentsRepository, IUnitOfWork unitOfWork)
        {
            this._CommentsRepository = CommentsRepository;
            this._unitOfWork = unitOfWork;
        }
        public Comment Add(Comment Comments)
        {
            return _CommentsRepository.Add(Comments);
        }

        public Comment Delete(int id)
        {
            return _CommentsRepository.Delete(id);
        }

        public IEnumerable<Comment> GetAll()
        {
            return _CommentsRepository.GetAll();
        }

        public IEnumerable<Comment> GetByIdDuAn(int idDuAn)
        {
            var query = _CommentsRepository.GetAll(new string[] { "AppUsers" }).Where(x => x.IdDuAn == idDuAn);
            return query;
        }

        public Comment GetById(int id)
        {
            return _CommentsRepository.GetMulti(x => x.IdComment == id, new string[] { "AppUsers" }).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(Comment Comments)
        {
            _CommentsRepository.Update(Comments);
        }
    }
}
