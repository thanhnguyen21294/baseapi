﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface IPhuLucHopDongService
    {
        PhuLucHopDong Add(PhuLucHopDong phuLucHopDong);

        void Update(PhuLucHopDong phuLucHopDong);

        PhuLucHopDong Delete(int id);

        IEnumerable<PhuLucHopDong> GetAll();

        IEnumerable<PhuLucHopDong> GetByFilter(int idHopDong, int page, int pageSize, out int totalRow, string filter);

        PhuLucHopDong GetById(int id);

        void Save();
    }



    public class PhuLucHopDongService : IPhuLucHopDongService
    {
        private IPhuLucHopDongRepository _phuLucHopDongRepository;
        private IUnitOfWork _unitOfWork;

        public PhuLucHopDongService(IPhuLucHopDongRepository phuLucHopDongRepository, IUnitOfWork unitOfWork)
        {
            this._phuLucHopDongRepository = phuLucHopDongRepository;
            this._unitOfWork = unitOfWork;
        }
        public PhuLucHopDong Add(PhuLucHopDong phuLucHopDong)
        {
            return _phuLucHopDongRepository.Add(phuLucHopDong);
        }

        public PhuLucHopDong Delete(int id)
        {
            return _phuLucHopDongRepository.Delete(id);
        }

        public IEnumerable<PhuLucHopDong> GetAll()
        {
            return _phuLucHopDongRepository.GetAll();
        }

        public IEnumerable<PhuLucHopDong> GetByFilter(int idHopDong, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _phuLucHopDongRepository.GetMulti(x=>x.IdHopDong == idHopDong);
            if (filter != null)
            {
                query = query.Where(x => x.TenPhuLuc.Contains(filter));
            }
            totalRow = query.Count();
            var phuluc = query.OrderBy(x => x.IdPhuLuc).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!phuluc.Any() && page > 1)
            //{
            //    page = page - 1;
            //    phuluc = query.OrderBy(x => x.IdPhuLuc).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            return phuluc;
        }

        public PhuLucHopDong GetById(int id)
        {
            return _phuLucHopDongRepository.GetMulti(x => x.IdPhuLuc == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PhuLucHopDong phuLucHopDong)
        {
            _phuLucHopDongRepository.Update(phuLucHopDong);
        }
    }
}
