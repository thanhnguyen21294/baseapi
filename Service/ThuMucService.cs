﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IThuMucService
    {
        IEnumerable<ThuMuc> GetTMsByDA(int idDA);
        IEnumerable<ThuMuc> GetAllForHistory();
        void AddThuMucWhenDuAnCreated(int idDA);
        ThuMuc Add(ThuMuc tm);
        void Update(ThuMuc tm);
        void Delete(int idTM);
        void Save();
    }
    public class ThuMucService : IThuMucService
    {
        public IThuMucRepository _tmRepository;
        public IUnitOfWork _unitOfWork;
        public ThuMucService(IThuMucRepository  tmRepository, IUnitOfWork unitOfWork)
        {
            _tmRepository = tmRepository;
            _unitOfWork = unitOfWork;
        }
        public ThuMuc Add(ThuMuc tm)
        {
            _tmRepository.Add(tm);
            return tm;

        }
        public void Delete(int idTM)
        {
           _tmRepository.Delete(idTM);
        }

       
        public void Update(ThuMuc tm)
        {
         _tmRepository.Update(tm);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<ThuMuc> GetTMsByDA(int idDA)
        {
          return   _tmRepository.GetTMsByDA(idDA);
        }

        public void AddThuMucWhenDuAnCreated(int idDA)
        {
            _tmRepository.AddThuMucWhenDuAnCreated(idDA);
        }

        public IEnumerable<ThuMuc> GetAllForHistory()
        {
            return _tmRepository.GetAll();
        }
    }
}
