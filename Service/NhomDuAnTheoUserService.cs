﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface INhomDuAnTheoUserService
    {
        NhomDuAnTheoUser Add(NhomDuAnTheoUser nhomDuAnTheoUser);

        void Update(NhomDuAnTheoUser nhomDuAnTheoUser);

        NhomDuAnTheoUser Delete(int id);

        IEnumerable<NhomDuAnTheoUser> GetAll();

        IEnumerable<NhomDuAnTheoUser> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        NhomDuAnTheoUser GetById(int id);

        void Save();
    }



    public class NhomDuAnTheoUserService : INhomDuAnTheoUserService
    {
        private INhomDuAnTheoUserRepository _nhomDuAnTheoUserRepository;
        private IUnitOfWork _unitOfWork;

        public NhomDuAnTheoUserService(INhomDuAnTheoUserRepository nhomDuAnTheoUserRepository, IUnitOfWork unitOfWork)
        {
            this._nhomDuAnTheoUserRepository = nhomDuAnTheoUserRepository;
            this._unitOfWork = unitOfWork;
        }
        public NhomDuAnTheoUser Add(NhomDuAnTheoUser nhomDuAnTheoUser)
        {
            return _nhomDuAnTheoUserRepository.Add(nhomDuAnTheoUser);
        }

        public NhomDuAnTheoUser Delete(int id)
        {
            return _nhomDuAnTheoUserRepository.Delete(id);
        }

        public IEnumerable<NhomDuAnTheoUser> GetAll()
        {
            return _nhomDuAnTheoUserRepository.GetAll();
        }

        public IEnumerable<NhomDuAnTheoUser> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _nhomDuAnTheoUserRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenNhomDuAn.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdNhomDuAn).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public NhomDuAnTheoUser GetById(int id)
        {
            return _nhomDuAnTheoUserRepository.GetMulti(x => x.IdNhomDuAn == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NhomDuAnTheoUser nhomDuAnTheoUser)
        {
            _nhomDuAnTheoUserRepository.Update(nhomDuAnTheoUser);
        }
    }
}
