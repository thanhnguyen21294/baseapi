﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface INguonVonQuyetToanDuAnService
    {
        NguonVonQuyetToanDuAn Add(NguonVonQuyetToanDuAn nguonVonQuyetToanDuAn);
        void DeleteByIdQuyetToanDuAn(int IdQTDA);
        IEnumerable<NguonVonQuyetToanDuAn> GetAll();

        void Save();
    }

    public class NguonVonQuyetToanDuAnService : INguonVonQuyetToanDuAnService
    {
        private INguonVonQuyetToanDuAnRepository _NguonVonQuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonQuyetToanDuAnService(INguonVonQuyetToanDuAnRepository nguonVonQuyetToanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._NguonVonQuyetToanDuAnRepository = nguonVonQuyetToanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }

        public NguonVonQuyetToanDuAn Add(NguonVonQuyetToanDuAn nguonvonquyettoanDuAn)
        {
            return _NguonVonQuyetToanDuAnRepository.Add(nguonvonquyettoanDuAn);
        }

        public void DeleteByIdQuyetToanDuAn(int idQTDA)
        {
            _NguonVonQuyetToanDuAnRepository.DeleteMulti(x => x.IdQuyetToanDuAn == idQTDA);
        }

        public IEnumerable<NguonVonQuyetToanDuAn> GetAll()
        {
            return _NguonVonQuyetToanDuAnRepository.GetAll();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
