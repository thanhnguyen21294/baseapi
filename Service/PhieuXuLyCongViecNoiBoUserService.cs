﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IPhieuXuLyCongViecNoiBoUserService
    {
        PhieuXuLyCongViecNoiBoUser Add(PhieuXuLyCongViecNoiBoUser PhieuXuLyCongViecNoiBoUser);

        void AddMulti(List<PhieuXuLyCongViecNoiBoUser> PhieuXuLyCongViecNoiBoUsers);

        void Update(PhieuXuLyCongViecNoiBoUser PhieuXuLyCongViecNoiBoUser);

        PhieuXuLyCongViecNoiBoUser Delete(int id);

        PhieuXuLyCongViecNoiBoUser GetById(int id);

        void Save();

        void DeleteByPhieu(int id);
    }

    public class PhieuXuLyCongViecNoiBoUserService : IPhieuXuLyCongViecNoiBoUserService
    {
        private IPhieuXuLyCongViecNoiBoUserRepository _PhieuXuLyCongViecNoiBoUserRepository;
        private IUnitOfWork _unitOfWork;

        public PhieuXuLyCongViecNoiBoUserService(IPhieuXuLyCongViecNoiBoUserRepository PhieuXuLyCongViecNoiBoUserRepository, IUnitOfWork unitOfWork)
        {
            this._PhieuXuLyCongViecNoiBoUserRepository = PhieuXuLyCongViecNoiBoUserRepository;
            this._unitOfWork = unitOfWork;
        }
        public PhieuXuLyCongViecNoiBoUser Add(PhieuXuLyCongViecNoiBoUser PhieuXuLyCongViecNoiBoUser)
        {
            return _PhieuXuLyCongViecNoiBoUserRepository.Add(PhieuXuLyCongViecNoiBoUser);
        }

        public void AddMulti(List<PhieuXuLyCongViecNoiBoUser> PhieuXuLyCongViecNoiBoUsers)
        {
            foreach(PhieuXuLyCongViecNoiBoUser p in PhieuXuLyCongViecNoiBoUsers)
            {
                _PhieuXuLyCongViecNoiBoUserRepository.Add(p);
            }
            this.Save();
        }

        public void DeleteByPhieu(int id)
        {
            _PhieuXuLyCongViecNoiBoUserRepository.DeleteMulti(x => x.IdPhieuXuLyCongViecNoiBo == id);
        }

        public PhieuXuLyCongViecNoiBoUser Delete(int id)
        {
            return _PhieuXuLyCongViecNoiBoUserRepository.Delete(id);
        }

        public IEnumerable<PhieuXuLyCongViecNoiBoUser> GetAll()
        {
            return _PhieuXuLyCongViecNoiBoUserRepository.GetAll();
        }

        public IEnumerable<PhieuXuLyCongViecNoiBoUser> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _PhieuXuLyCongViecNoiBoUserRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdPhieuXuLyCongViecNoiBoUser).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public PhieuXuLyCongViecNoiBoUser GetById(int id)
        {
            return _PhieuXuLyCongViecNoiBoUserRepository.GetMulti(x => x.IdPhieuXuLyCongViecNoiBoUser == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PhieuXuLyCongViecNoiBoUser PhieuXuLyCongViecNoiBoUser)
        {
            _PhieuXuLyCongViecNoiBoUserRepository.Update(PhieuXuLyCongViecNoiBoUser);
        }
    }
}
