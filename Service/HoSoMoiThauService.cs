﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface IHoSoMoiThauService
    {
        HoSoMoiThau Add(HoSoMoiThau hoSoMoiThau);

        void Update(HoSoMoiThau hoSoMoiThau);

        HoSoMoiThau Delete(int id);

        IEnumerable<HoSoMoiThau> GetAll();

        IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);
        HoSoMoiThau GetById(int id);

        void Save();
    }

    public class HoSoMoiThauService : IHoSoMoiThauService
    {
        private IHoSoMoiThauRepository _HoSoMoiThauRepository;
        private IUnitOfWork _unitOfWork;

        public HoSoMoiThauService(IHoSoMoiThauRepository hoSoMoiThauRepository, IUnitOfWork unitOfWork)
        {
            this._HoSoMoiThauRepository = hoSoMoiThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public HoSoMoiThau Add(HoSoMoiThau hoSoMoiThau)
        {
            return _HoSoMoiThauRepository.Add(hoSoMoiThau);
        }

        public HoSoMoiThau Delete(int id)
        {
            return _HoSoMoiThauRepository.Delete(id);
        }

        public IEnumerable<HoSoMoiThau> GetAll()
        {
            return _HoSoMoiThauRepository.GetAll();
        }

        public IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _HoSoMoiThauRepository.GetGoiThaus(idDuAn, filter);
            totalRow = query.Count();
            var  hoSo = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!hoSo.Any() && page > 1)
            //{
            //    page = page - 1;
            //    hoSo = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            return hoSo;
        }


        public HoSoMoiThau GetById(int id)
        {
            return _HoSoMoiThauRepository.GetMulti(x => x.IdHoSoMoiThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HoSoMoiThau hoSoMoiThau)
        {
            _HoSoMoiThauRepository.Update(hoSoMoiThau);
        }
    }
}
