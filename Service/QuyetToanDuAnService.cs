﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IQuyetToanDuAnService
    {
        QuyetToanDuAn Add(QuyetToanDuAn iQuyetToanDuAn);

        void Update(QuyetToanDuAn iQuyetToanDuAn);

        QuyetToanDuAn Delete(int id);

        IEnumerable<QuyetToanDuAn> GetAll();

        IEnumerable<QuyetToanDuAn> GetAll(string filter);
        IEnumerable<QuyetToanDuAn> GetYear(int year);
        IEnumerable<QuyetToanDuAn> GetByIdDuAn(int idDA);
        QuyetToanDuAn GetNewestByIdDuAn(int idduan);
        IEnumerable<QuyetToanDuAn> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter);
        QuyetToanDuAn GetById(int id);
        IEnumerable<QuyetToanDuAn> GetHoanThanhByIdDuAn(int id);
        IEnumerable<QuyetToanDuAn> GetNhanDuHoSoByIdDuAn(int id);
        IEnumerable<QuyetToanDuAn> GetChuaNhanDuHoSoByIdDuAn(int id);
        void Save();
    }
    public class QuyetToanDuAnService : IQuyetToanDuAnService
    {
        private IQuyetToanDuAnRepository _QuyetToanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public QuyetToanDuAnService(IQuyetToanDuAnRepository QuyetToanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._QuyetToanDuAnRepository = QuyetToanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }
        public QuyetToanDuAn Add(QuyetToanDuAn quyetToanDuAn)
        {
            return _QuyetToanDuAnRepository.Add(quyetToanDuAn);
        }

        public QuyetToanDuAn Delete(int id)
        {
            return _QuyetToanDuAnRepository.Delete(id);
        }

        public IEnumerable<QuyetToanDuAn> GetAll()
        {
            return _QuyetToanDuAnRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
        }

        public IEnumerable<QuyetToanDuAn> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _QuyetToanDuAnRepository.GetMulti(x => x.NoiDung.Contains(filter), new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
            }
            
            return _QuyetToanDuAnRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
        }

        public IEnumerable<QuyetToanDuAn> GetYear(int year)
        {
            return _QuyetToanDuAnRepository.GetAll().Where(x => (x.NgayPheDuyetQuyetToan != null && x.NgayPheDuyetQuyetToan.Value.Year <= year));
        }

        public IEnumerable<QuyetToanDuAn> GetByIdDuAn(int idDA)
        {
            var query = _QuyetToanDuAnRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
            query = query.Where(x => x.IdDuAn == idDA);
            return query.OrderBy(x => x.IdQuyetToanDuAn);
        }
        public IEnumerable<QuyetToanDuAn> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _QuyetToanDuAnRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDA);
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDA);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdQuyetToanDuAn).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public QuyetToanDuAn GetById(int id)
        {
            return _QuyetToanDuAnRepository.GetSingleByCondition(x => x.IdQuyetToanDuAn == id, new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QuyetToanDuAn updateQuyetToanDuAn)
        {
            _QuyetToanDuAnRepository.Update(updateQuyetToanDuAn);
        }

        public QuyetToanDuAn GetNewestByIdDuAn(int idduan)
        {
            return _QuyetToanDuAnRepository.GetAll(new string[] { "CoQuanPheDuyet", "NguonVonQuyetToanDuAns"}).Where(x => x.IdDuAn == idduan && x.NgayPheDuyetQuyetToan != null).OrderByDescending(x => x.NgayPheDuyetQuyetToan).FirstOrDefault();
        }

        public IEnumerable<QuyetToanDuAn> GetHoanThanhByIdDuAn(int id)
        {
            return _QuyetToanDuAnRepository.GetAll().Where(x => x.NgayPheDuyetQuyetToan != null && x.IdDuAn ==id);
        }

        public IEnumerable<QuyetToanDuAn> GetNhanDuHoSoByIdDuAn(int id)
        {
            return _QuyetToanDuAnRepository.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo != null && x.IdDuAn == id);
        }

        public IEnumerable<QuyetToanDuAn> GetChuaNhanDuHoSoByIdDuAn(int id)
        {
            return _QuyetToanDuAnRepository.GetAll().Where(x => x.NgayPheDuyetQuyetToan == null && x.NgayNhanDuHoSo == null && x.NgayKyBienBan !=null && x.IdDuAn == id);
        }
    }
}
