﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IThanhToanHopDongService
    {
        ThanhToanHopDong Add(ThanhToanHopDong hopDong);

        void Update(ThanhToanHopDong hopDong);

        ThanhToanHopDong Delete(int id);

        IEnumerable<ThanhToanHopDong> GetAll();

        IEnumerable<ThanhToanHopDong> GetForKeToanView();

        IEnumerable<ThanhToanHopDong> GetByIdHopDong(int idHopDong);
        IEnumerable<ThanhToanHopDong> GetByIdThucHien(int idThucHien);
        IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);
        IEnumerable<ThanhToanHopDong> GetByNienDo(int niendo,bool flag);
        IEnumerable<ThanhToanHopDong> GetByYearDuAn(string year);
        IEnumerable<ThanhToanHopDong> GetAllNguonVonGoiThauThanhToanByYear(string year);
        ThanhToanHopDong GetById(int id);
        IEnumerable<ThanhToanHopDong> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon);
        IEnumerable<ThanhToanHopDong> GetByIdDuAnIdNguonVonIdHopDong(int idduan, int idnguonvon, int idhopdong);

        void Save();
    }

    public class ThanhToanHopDongService : IThanhToanHopDongService
    {
        private IThanhToanHopDongRepository _ThanhToanHopDongRepository;
        private IHopDongRepository _hopDongRepository;
        private IUnitOfWork _unitOfWork;
        private INguonVonDuAnService _nguonVonDuAnService;
        private INguonVonDuAnGoiThauService _nguonVonDuAnGoiThauService;
        private INguonVonGoiThauThanhToanService _nguonVonGoiThauThanhToanService;

        public ThanhToanHopDongService(IThanhToanHopDongRepository thanhToanHopDongRepository, IUnitOfWork unitOfWork, IHopDongRepository hopDongRepository, INguonVonDuAnService nguonVonDuAnService, INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService, INguonVonGoiThauThanhToanService nguonVonGoiThauThanhToanService)
        {
            this._ThanhToanHopDongRepository = thanhToanHopDongRepository;
            this._hopDongRepository = hopDongRepository;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._nguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            this._nguonVonGoiThauThanhToanService = nguonVonGoiThauThanhToanService;
            this._unitOfWork = unitOfWork;
        }
        public ThanhToanHopDong Add(ThanhToanHopDong thanhToanHopDong)
        {
            return _ThanhToanHopDongRepository.Add(thanhToanHopDong);
        }

        public ThanhToanHopDong Delete(int id)
        {
            return _ThanhToanHopDongRepository.Delete(id);
        }

        public IEnumerable<ThanhToanHopDong> GetAll()
        {
            return _ThanhToanHopDongRepository.GetAll();
        }

        
        public IEnumerable<ThanhToanHopDong> GetForKeToanView()
        {
            var query = _ThanhToanHopDongRepository.GetMulti(x => x.ThoiDiemThanhToan != null ,new[] { "HopDong", "NguonVonGoiThauThanhToans", "NguonVonGoiThauThanhToans.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon" });
            return query.OrderBy(x => x.ThoiDiemThanhToan);
        }

        public IEnumerable<ThanhToanHopDong> GetByIdHopDong(int idHopDong)
        {
            var query = _ThanhToanHopDongRepository.GetAll(new[] { "HopDong", "NguonVonGoiThauThanhToans" });
            query = query.Where(x => x.IdHopDong == idHopDong);
            return query.OrderBy(x => x.IdThanhToanHopDong);
        }

        public IEnumerable<ThanhToanHopDong> GetByIdThucHien(int idThucHien)
        {
            var query = _ThanhToanHopDongRepository.GetAll();
            query = query.Where(x => x.IdThucHien == idThucHien);
            return query.OrderBy(x => x.IdThanhToanHopDong);
        }
        public IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _ThanhToanHopDongRepository.GetThanhToanHopDongs(idDuAn, filter);
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHopDong).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ThanhToanHopDong GetById(int id)
        {
            return _ThanhToanHopDongRepository.GetMulti(x => x.IdThanhToanHopDong == id, new string[] { "NguonVonGoiThauThanhToans" }).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ThanhToanHopDong thucHienHopDong)
        {
            _ThanhToanHopDongRepository.Update(thucHienHopDong);
        }

        public IEnumerable<ThanhToanHopDong> GetByNienDo(int niendo,bool flag)
        {
            IEnumerable<ThanhToanHopDong> lst;
          
          if (flag==true)
            {
               lst = _ThanhToanHopDongRepository.GetAll().Where(x => x.ThoiDiemThanhToan.Value.Year==niendo);

            }
          else
            {
                lst = _ThanhToanHopDongRepository.GetAll().Where(x => x.ThoiDiemThanhToan.Value.Year <= niendo);
            }
            return lst;
        }

        public IEnumerable<ThanhToanHopDong> GetByYearDuAn(string year)
        {
            List<ThanhToanHopDong> thanhToanHopDong = new List<ThanhToanHopDong>();
            var hopdong = _hopDongRepository.GetAll();
            foreach (var item in hopdong)
            {
                var query = _ThanhToanHopDongRepository.GetAll(new[] { "HopDong" }).Where(x => (x.ThoiDiemThanhToan.ToString().Contains(year) || x.CreatedDate.ToString().Contains(year)) && x.IdHopDong == item.IdHopDong);
                if (query.Count() > 0)
                {
                    foreach (var itemm in query)
                    {
                        thanhToanHopDong.Add(itemm);
                    }
                }
            }
            return thanhToanHopDong;
        }

        public IEnumerable<ThanhToanHopDong> GetAllNguonVonGoiThauThanhToanByYear(string year)
        {
            return _ThanhToanHopDongRepository.GetAll(new[] { "NguonVonGoiThauThanhToans" }).Where(x => x.ThoiDiemThanhToan.ToString().Contains(year) || x.CreatedDate.ToString().Contains(year));
        }

        public IEnumerable<ThanhToanHopDong> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon)
        {
            //lay ra tat ca thanh toan hop dong theo idduan
            var hopdong = _hopDongRepository.GetAll().Where(x => x.IdDuAn == idduan);

            List<ThanhToanHopDong> tthds = new List<ThanhToanHopDong>();
            foreach(var item in hopdong)
            {
                var tthd = this.GetByIdHopDong(item.IdHopDong);
                foreach(var item1 in tthd)
                {
                    tthds.Add(item1);
                }
            }
            // lay ra cac thanh toan hd theo id nguon von
            var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdNguonVon == idnguonvon).Select(x => x.IdNguonVonDuAn);
            List<NguonVonDuAnGoiThau> nvdagts = new List<NguonVonDuAnGoiThau>();
            foreach (var item in nguonvonduans)
            {
                var nvdagt = _nguonVonDuAnGoiThauService.GetAll().Where(x=>x.IdNguonVonDuAn==item);
                foreach(var item1 in nvdagt)
                {
                    nvdagts.Add(item1);
                }
            }
            List<NguonVonGoiThauThanhToan> nvgttts = new List<NguonVonGoiThauThanhToan>();
            foreach(var item in nvdagts)
            {
                var nvgttt = _nguonVonGoiThauThanhToanService.GetAll().Where(x => x.IdNguonVonDuAnGoiThau == item.IdNguonVonDuAnGoiThau);
                foreach(var item1 in nvgttt)
                {
                    nvgttts.Add(item1);
                }
            }
            List<ThanhToanHopDong> tthdnvs = new List<ThanhToanHopDong>();
            foreach(var item in nvgttts)
            {
                var tthdnv = this.GetAll().Where(x => x.IdThanhToanHopDong == item.IdThanhToanHopDong);
                foreach(var item1 in tthdnv)
                {
                    tthdnvs.Add(item1);
                }
            }
            //xét thanhtoanhopdong theo duan va theo nguonvon
            List<ThanhToanHopDong> thanhtoanhopdong = new List<ThanhToanHopDong>();
            foreach(var item in tthds)
            {
                var index = 0;
                foreach(var item1 in tthdnvs)
                {
                        if (item.IdThanhToanHopDong == item1.IdThanhToanHopDong)
                        {
                            index = 1;
                        }
                    
                }
                if (index == 1)
                {
                    var index1 = 0;
                    foreach (var item2 in thanhtoanhopdong)
                    {
                        if(item2.IdThanhToanHopDong == item.IdThanhToanHopDong)
                        {
                            index1 = 1;
                        }
                    }
                    if(index1 != 1)
                    {
                        thanhtoanhopdong.Add(item);
                    }
                }
            }
            return thanhtoanhopdong;
            
        }

        public IEnumerable<ThanhToanHopDong> GetByIdDuAnIdNguonVonIdHopDong(int idduan, int idnguonvon, int idhopdong)
        {
            //lay ra tat ca thanh toan hop dong theo idduan
            var hopdong = _hopDongRepository.GetAll().Where(x => x.IdDuAn == idduan);

            List<ThanhToanHopDong> tthds = new List<ThanhToanHopDong>();
            foreach (var item in hopdong)
            {
                var tthd = this.GetByIdHopDong(item.IdHopDong);
                foreach (var item1 in tthd)
                {
                    tthds.Add(item1);
                }
            }
            // lay ra cac thanh toan hd theo id nguon von
            var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdNguonVon == idnguonvon).Select(x => x.IdNguonVonDuAn);
            List<NguonVonDuAnGoiThau> nvdagts = new List<NguonVonDuAnGoiThau>();
            foreach (var item in nguonvonduans)
            {
                var nvdagt = _nguonVonDuAnGoiThauService.GetAll().Where(x => x.IdNguonVonDuAn == item);
                foreach (var item1 in nvdagt)
                {
                    nvdagts.Add(item1);
                }
            }
            List<NguonVonGoiThauThanhToan> nvgttts = new List<NguonVonGoiThauThanhToan>();
            foreach (var item in nvdagts)
            {
                var nvgttt = _nguonVonGoiThauThanhToanService.GetAll().Where(x => x.IdNguonVonDuAnGoiThau == item.IdNguonVonDuAnGoiThau);
                foreach (var item1 in nvgttt)
                {
                    nvgttts.Add(item1);
                }
            }
            List<ThanhToanHopDong> tthdnvs = new List<ThanhToanHopDong>();
            foreach (var item in nvgttts)
            {
                var tthdnv = this.GetAll().Where(x => x.IdThanhToanHopDong == item.IdThanhToanHopDong);
                foreach (var item1 in tthdnv)
                {
                    tthdnvs.Add(item1);
                }
            }
            //xét thanhtoanhopdong theo duan va theo nguonvon
            List<ThanhToanHopDong> thanhtoanhopdong = new List<ThanhToanHopDong>();
            foreach (var item in tthds)
            {
                var index = 0;
                foreach (var item1 in tthdnvs)
                {
                    if (item.IdThanhToanHopDong == item1.IdThanhToanHopDong)
                    {
                        index = 1;
                    }

                }
                if (index == 1)
                {
                    var index1 = 0;
                    foreach (var item2 in thanhtoanhopdong)
                    {
                        if (item2.IdThanhToanHopDong == item.IdThanhToanHopDong)
                        {
                            index1 = 1;
                        }
                    }
                    if (index1 != 1)
                    {
                        thanhtoanhopdong.Add(item);
                    }
                }
            }
            return thanhtoanhopdong.Where(x=>x.IdHopDong==idhopdong);
        }
    }
}
