﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Service
{
    public interface ITamUngHopDongService
    {
        TamUngHopDong Add(TamUngHopDong hopDong);

        void Update(TamUngHopDong hopDong);

        TamUngHopDong Delete(int id);

        IEnumerable<TamUngHopDong> GetAll();

        IEnumerable<TamUngHopDong> GetByIdHopDong(int idHopDong);

        IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);

        IEnumerable<TamUngHopDong> GetAllNguonVonGoiThauTamUngByYear(string year);

        TamUngHopDong GetById(int id);

        IEnumerable<TamUngHopDong> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon);

        IEnumerable<TamUngHopDong> GetByIdDuAn(int id);
       
        void Save();
    }

    public class TamUngHopDongService : ITamUngHopDongService
    {
        private ITamUngHopDongRepository _TamUngHopDongRepository;
        private IHopDongRepository _hopDongRepository;
        private INguonVonDuAnService _nguonVonDuAnService;
        private INguonVonDuAnGoiThauService _nguonVonDuAnGoiThauService;
        private INguonVonGoiThauTamUngService _nguonVonGoiThauTamUngService;
        private IUnitOfWork _unitOfWork;

        public TamUngHopDongService(INguonVonGoiThauTamUngService nguonVonGoiThauTamUngService, ITamUngHopDongRepository tamUngHopDongRepository,INguonVonDuAnGoiThauService nguonVonDuAnGoiThauService, IHopDongRepository hopDongRepository, INguonVonDuAnService nguonVonDuAnService, IUnitOfWork unitOfWork)
        {
            this._TamUngHopDongRepository = tamUngHopDongRepository;
            this._hopDongRepository = hopDongRepository;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._nguonVonDuAnGoiThauService = nguonVonDuAnGoiThauService;
            this._nguonVonGoiThauTamUngService = nguonVonGoiThauTamUngService;
            this._unitOfWork = unitOfWork;
        }
        public TamUngHopDong Add(TamUngHopDong thucHienHopDong)
        {
            return _TamUngHopDongRepository.Add(thucHienHopDong);
        }

        public TamUngHopDong Delete(int id)
        {
            return _TamUngHopDongRepository.Delete(id);
        }

        public IEnumerable<TamUngHopDong> GetAll()
        {
            return _TamUngHopDongRepository.GetAll();
        }

        public IEnumerable<TamUngHopDong> GetByIdHopDong(int idHopDong)
        {
            var query = _TamUngHopDongRepository.GetAll(new[] { "HopDong" });
            query = query.Where(x => x.IdHopDong == idHopDong);
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _TamUngHopDongRepository.GetTamUngHopDongs(idDuAn, filter);
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHopDong).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public TamUngHopDong GetById(int id)
        {
            return _TamUngHopDongRepository.GetMulti(x => x.IdTamUng == id, new string[] { "NguonVonGoiThauTamUngs" }).SingleOrDefault();
        }

        public IEnumerable<TamUngHopDong> GetAllNguonVonGoiThauTamUngByYear(string year)
        {
            return _TamUngHopDongRepository.GetAll(new[] { "NguonVonGoiThauTamUngs" }).Where(x => x.NgayTamUng.ToString().Contains(year) || x.CreatedDate.ToString().Contains(year));
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(TamUngHopDong thucHienHopDong)
        {
            _TamUngHopDongRepository.Update(thucHienHopDong);
        }

        public IEnumerable<TamUngHopDong> GetByIdDuAnIdNguonVon(int idduan, int idnguonvon)
        {
            //lay ra tat ca thanh toan hop dong theo idduan
            var hopdong = _hopDongRepository.GetAll().Where(x => x.IdDuAn == idduan);

            List<TamUngHopDong> tthds = new List<TamUngHopDong>();
            foreach (var item in hopdong)
            {
                var tthd = this.GetByIdHopDong(item.IdHopDong);
                foreach (var item1 in tthd)
                {
                    tthds.Add(item1);
                }
            }
            // lay ra cac thanh toan hd theo id nguon von
            var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdNguonVon == idnguonvon).Select(x => x.IdNguonVonDuAn);
            List<NguonVonDuAnGoiThau> nvdagts = new List<NguonVonDuAnGoiThau>();
            foreach (var item in nguonvonduans)
            {
                var nvdagt = _nguonVonDuAnGoiThauService.GetAll().Where(x => x.IdNguonVonDuAn == item);
                foreach (var item1 in nvdagt)
                {
                    nvdagts.Add(item1);
                }
            }
            List<NguonVonGoiThauTamUng> nvgttts = new List<NguonVonGoiThauTamUng>();
            foreach (var item in nvdagts)
            {
                var nvgttt = _nguonVonGoiThauTamUngService.GetAll().Where(x => x.IdNguonVonDuAnGoiThau == item.IdNguonVonDuAnGoiThau);
                foreach (var item1 in nvgttt)
                {
                    nvgttts.Add(item1);
                }
            }
            List<TamUngHopDong> tthdnvs = new List<TamUngHopDong>();
            foreach (var item in nvgttts)
            {
                var tthdnv = this.GetAll().Where(x => x.IdTamUng == item.IdTamUng);
                foreach (var item1 in tthdnv)
                {
                    tthdnvs.Add(item1);
                }
            }
            //xét thanhtoanhopdong theo duan va theo nguonvon
            List<TamUngHopDong> tamunghopdong = new List<TamUngHopDong>();
            foreach (var item in tthds)
            {
                var index = 0;
                foreach (var item1 in tthdnvs)
                {
                    if (item.IdTamUng == item1.IdTamUng)
                    {
                        index = 1;
                    }

                }
                if (index == 1)
                {
                    var index1 = 0;
                    foreach (var item2 in tamunghopdong)
                    {
                        if (item2.IdTamUng == item.IdTamUng)
                        {
                            index1 = 1;
                        }
                    }
                    if (index1 != 1)
                    {
                        tamunghopdong.Add(item);
                    }
                }
            }
            return tamunghopdong;

        }

        public IEnumerable<TamUngHopDong> GetByIdDuAn(int id)
        {
            List<TamUngHopDong> tamunghopdongs = new List<TamUngHopDong>();
            var hopdong = _hopDongRepository.GetAll().Where(x => x.IdDuAn == id);
            foreach(var item in hopdong)
            {
                var tamung = _TamUngHopDongRepository.GetAll().Where(x => x.IdHopDong == item.IdHopDong);
                foreach(var item1 in tamung)
                {
                    tamunghopdongs.Add(item1);
                }
            }
            return tamunghopdongs;
        }
    }
}
