﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IGiaiDoanDuAnService
    {
        GiaiDoanDuAn Add(GiaiDoanDuAn giaiDoanDuAn);

        void Update(GiaiDoanDuAn giaiDoanDuAn);

        GiaiDoanDuAn Delete(int id);

        IEnumerable<GiaiDoanDuAn> GetAll();

        IEnumerable<GiaiDoanDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        GiaiDoanDuAn GetById(int id);

        void Save();
    }

    public class GiaiDoanDuAnService : IGiaiDoanDuAnService
    {
        private IGiaiDoanDuAnRepository _giaiDoanDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public GiaiDoanDuAnService(IGiaiDoanDuAnRepository giaiDoanDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._giaiDoanDuAnRepository = giaiDoanDuAnRepository;
            this._unitOfWork = unitOfWork;
        }
        public GiaiDoanDuAn Add(GiaiDoanDuAn giaiDoanDuAn)
        {
            return _giaiDoanDuAnRepository.Add(giaiDoanDuAn);
        }

        public GiaiDoanDuAn Delete(int id)
        {
            return _giaiDoanDuAnRepository.Delete(id);
        }

        public IEnumerable<GiaiDoanDuAn> GetAll()
        {
            return _giaiDoanDuAnRepository.GetAll();
        }

        public IEnumerable<GiaiDoanDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _giaiDoanDuAnRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenGiaiDoanDuAn.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdGiaiDoanDuAn).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public GiaiDoanDuAn GetById(int id)
        {
            return _giaiDoanDuAnRepository.GetMulti(x => x.IdGiaiDoanDuAn == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GiaiDoanDuAn giaiDoanDuAn)
        {
            _giaiDoanDuAnRepository.Update(giaiDoanDuAn);
        }
    }
}
