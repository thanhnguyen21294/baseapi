﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IHinhThucDauTuService
    {
        HinhThucDauTu Add(HinhThucDauTu hinhThucDauTu);

        void Update(HinhThucDauTu hinhThucDauTu);

        HinhThucDauTu Delete(int id);

        IEnumerable<HinhThucDauTu> GetAll();

        IEnumerable<HinhThucDauTu> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        HinhThucDauTu GetById(int id);

        void Save();
    }

    public class HinhThucDauTuService : IHinhThucDauTuService
    {
        private IHinhThucDauTuRepository _hinhThucDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public HinhThucDauTuService(IHinhThucDauTuRepository hinhThucDauTuRepository, IUnitOfWork unitOfWork)
        {
            this._hinhThucDauTuRepository = hinhThucDauTuRepository;
            this._unitOfWork = unitOfWork;
        }
        public HinhThucDauTu Add(HinhThucDauTu hinhThucDauTu)
        {
            return _hinhThucDauTuRepository.Add(hinhThucDauTu);
        }

        public HinhThucDauTu Delete(int id)
        {
            return _hinhThucDauTuRepository.Delete(id);
        }

        public IEnumerable<HinhThucDauTu> GetAll()
        {
            return _hinhThucDauTuRepository.GetAll();
        }

        public IEnumerable<HinhThucDauTu> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _hinhThucDauTuRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenHinhThucDauTu.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHinhThucDauTu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public HinhThucDauTu GetById(int id)
        {
            return _hinhThucDauTuRepository.GetMulti(x => x.IdHinhThucDauTu == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HinhThucDauTu hinhThucDauTu)
        {
            _hinhThucDauTuRepository.Update(hinhThucDauTu);
        }
    }
}
