﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IHopDongService
    {
        HopDong Add(HopDong hopDong);

        void Update(HopDong hopDong);

        HopDong Delete(int id);

        IEnumerable<HopDong> GetAll();

        IEnumerable<int> GetForLocBaoCao(DateTime? tuNgay, DateTime? denNgay);
        IEnumerable<HopDong> GetByIdDuAn(int idDuAn);
        IEnumerable<HopDong> GetByIdDuAnBaoLanh(int idDuAn);
        IEnumerable<HopDong> GetByIdDuAnGroupLoaiGoiThau(int idDuAn, string year);
        IEnumerable<HopDong> GetHopDongThucHienByIdDuAn(int idDuAn, int page, int pageSize, out int totalRow, string filter);
        IEnumerable<HopDong> GetByIdGoiThau(int idGoiThau);
        IEnumerable<HopDong> GetByIdDuAnLoaiGoiThau(int idDuAn, int idLoaiGoiThau);
        IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);
        IEnumerable<HopDong> GetByYear(string year);
        IEnumerable<HopDong> GetByIdNhaThau(int idnhathau);

        IEnumerable<DanhSachLuaChonNhaThau> GetHopDong(Dictionary<string, string> lstUserNhanVien, string IDDuAns);

        IEnumerable<DanhSachLuaChonNhaThau> GetHopDong_TuVan(Dictionary<string, string> lstUserNhanVien, string IDDuAns);
        IEnumerable<DanhSachLuaChonNhaThau> GetHopDong_ThietBi(Dictionary<string, string> lstUserNhanVien, string IDDuAns);

        HopDong GetById(int id);

        void Save();
    }

    public class HopDongService : IHopDongService
    {
        private IHopDongRepository _HopDongRepository;
        private IDuAnUserRepository _DuAnUserRepository;
        private IGoiThauRepository _GoiThauRepository;
        private IKeHoachLuaChonNhaThauRepository _KeHoachLuaChonNhaThauRepository;
        private IUnitOfWork _unitOfWork;

        public HopDongService(IHopDongRepository hopDongRepository,
            IGoiThauRepository GoiThauRepository,
            IKeHoachLuaChonNhaThauRepository KeHoachLuaChonNhaThauRepository,
            IDuAnUserRepository DuAnUserRepository,
            IUnitOfWork unitOfWork)
        {
            this._HopDongRepository = hopDongRepository;
            this._GoiThauRepository = GoiThauRepository;
            this._KeHoachLuaChonNhaThauRepository = KeHoachLuaChonNhaThauRepository;
            _DuAnUserRepository = DuAnUserRepository;
            this._unitOfWork = unitOfWork;
        }
        public HopDong Add(HopDong hopDong)
        {
            return _HopDongRepository.Add(hopDong);
        }

        public HopDong Delete(int id)
        {
            return _HopDongRepository.Delete(id);
        }

        public IEnumerable<int> GetForLocBaoCao(DateTime? tuNgay, DateTime? denNgay)
        {
            var hds = _HopDongRepository.GetMulti(x => (tuNgay != null ? x.NgayBatDau <= tuNgay : true) && (denNgay != null ? x.NgayHoanThanh >= denNgay : true)).Select(x => x.IdDuAn);
            return hds;

        }
        public IEnumerable<HopDong> GetAll()
        {
            return _HopDongRepository.GetAll();
        }

        public IEnumerable<HopDong> GetByYear(string year)
        {
            return _HopDongRepository.GetAll().Where(x => x.CreatedDate.ToString().Contains(year));
        }

        public IEnumerable<HopDong> GetByIdDuAnGroupLoaiGoiThau(int idDuAn, string year)
        {
            var query = _HopDongRepository.GetAll(new[] { "GoiThau", "DanhMucNhaThau", "ThanhToanHopDongs", "QuyetToanHopDongs", "TamUngHopDongs" });
            query = query.Where(x => x.IdDuAn == idDuAn && x.CreatedDate.ToString().Contains(year));
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<HopDong> GetByIdDuAnLoaiGoiThau(int idDuAn, int idLoaiGoiThau)
        {
            var query = _HopDongRepository.GetAll(new[] { "GoiThau", "QuyetToanHopDongs" });
            query = query.Where(x => x.IdDuAn == idDuAn && x.GoiThau.LoaiGoiThau == idLoaiGoiThau);
            return query;
        }
        public IEnumerable<HopDong> GetHopDongThucHienByIdDuAn(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _HopDongRepository.GetAll(new string[] { "ThucHienHopDongs", "QuyetToanHopDongs" });
            if (filter != null)
            {
                query = query.Where(x => x.TenHopDong.Contains(filter) && x.IdDuAn == idDuAn);
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDuAn);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHopDong).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<HopDong> GetByIdDuAn(int idDuAn)
        {
            var query = _HopDongRepository.GetAll();
            query = query.Where(x => x.IdDuAn == idDuAn);
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<HopDong> GetByIdDuAnBaoLanh(int idDuAn)
        {
            var query = _HopDongRepository.GetMulti(x => x.IdDuAn == idDuAn, new string[]{ "BaoLanhTHHDs" }).OrderBy(x => x.IdHopDong).ToList();
            query = query.Where(x => x.BaoLanhTHHDs.Count() < 2).ToList();
            return query;
        }

        public IEnumerable<HopDong> GetByIdGoiThau(int idDuAn)
        {
            var query = _HopDongRepository.GetAll();
            query = query.Where(x => x.IdGoiThau == idDuAn);
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            //var query = _HopDongRepository.GetMulti(x=>x.IdDuAn == idDuAn);
            //if (filter != null)
            //{
            //    query = query.Where(x => x.TenHopDong.Contains(filter));
            //}
            var query = _HopDongRepository.GetByFilter(idDuAn, filter);
            totalRow = query.Count();
            var hopDong = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!hopDong.Any() && page > 1)
            //{
            //    page = page - 1;
            //    hopDong = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            return hopDong;
        }

        public HopDong GetById(int id)
        {
            return _HopDongRepository.GetMulti(x => x.IdHopDong == id, new[] { "DanhMucNhaThau" }).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HopDong hopDong)
        {
            _HopDongRepository.Update(hopDong);
        }

        public IEnumerable<HopDong> GetByIdNhaThau(int idnhathau)
        {
            return this.GetAll().Where(x => x.IdNhaThau != null && x.IdNhaThau == idnhathau);
        }

        public IEnumerable<DanhSachLuaChonNhaThau> GetHopDong(Dictionary<string, string> lstUserNhanVien, string IDDuAns)
        {
            string[] sarrDuAn = IDDuAns.Split(',');
            int[] arrDuAn = sarrDuAn.Select(int.Parse).ToArray();
            IEnumerable<int?> idsKH;
            IEnumerable<int> idsHD;
            idsKH = _KeHoachLuaChonNhaThauRepository.GetAll().Select(x => new
            {
                IDKH = x.IdDieuChinhKeHoachLuaChonNhaThau == null ? x.IdKeHoachLuaChonNhaThau : x.IdDieuChinhKeHoachLuaChonNhaThau,
                IDDA = x.IdDuAn
            }).GroupBy(x => new { x.IDDA, x.IDKH }).Distinct().Select(x => x.Key.IDKH);

            var a = idsKH.ToList();

            idsHD = _HopDongRepository.GetMulti(x => idsKH.Contains(x.GoiThau.IdKeHoachLuaChonNhaThau), new[] { "GoiThau" }).Select(x => new
            {
                IDGT = x.IdGoiThau,
                IDHD = x.IdHopDong
            }).GroupBy(x => new { x.IDGT }).Select(x => x.OrderByDescending(y => y.IDHD).Select(y => y.IDHD).FirstOrDefault());

            var lstDuAnUser1 = _DuAnUserRepository.GetMulti(x => x.UserId != null && arrDuAn.Contains(x.IdDuAn)).ToList();
            var lstDuAnUser = lstDuAnUser1.Where(x => lstUserNhanVien.ContainsKey(x.UserId) == true).Select(u => new
            {
                u.IdDuAn,
                ID = u.UserId,
                Name = lstUserNhanVien[u.UserId]
            }).ToList().Distinct();

            var queryAll = _HopDongRepository.GetAll(new[] { "GoiThau", "GoiThau.KeHoachLuaChonNhaThau", "DanhMucNhaThau", "DuAn", "DuAn.DuAnUsers", "DuAn.LinhVucNganhNghe", "DuAn.GiaiDoanDuAn" })
                .Where(x=> arrDuAn.Contains(x.IdDuAn))
                .Select(x => new
               {
                    x.GoiThau.IdKeHoachLuaChonNhaThau,
                    x.IdHopDong,
                   IDLinhVuc = x.DuAn.IdLinhVucNganhNghe,
                   TenLinhVuc = x.DuAn.LinhVucNganhNghe.TenLinhVucNganhNghe,
                   Users = lstDuAnUser.Where(y =>y.IdDuAn == x.IdDuAn),
                   IDGiaiDoan = x.DuAn.IdGiaiDoanDuAn,
                   TenGiaiDoan = x.DuAn.GiaiDoanDuAn.TenGiaiDoanDuAn,
                   IDDA = x.IdDuAn,
                   TenDuAn = x.DuAn.TenDuAn,
                   OrderDuAn = x.DuAn.Order,
                   OrderTemp = x.DuAn.OrderTemp != null ? x.DuAn.OrderTemp : 0,
                   TenNhaThau = x.DanhMucNhaThau.TenNhaThau,
                   TuNgay = x.NgayBatDau == null ? "" : x.NgayBatDau.ToString(),
                   DenNgay = x.NgayHoanThanh == null ? "" : x.NgayHoanThanh.ToString(),
                   LoaiGoiThau = x.GoiThau.LoaiGoiThau,
                x.DuAn.DuAnUsers
            }).ToList();

            var query = queryAll.Where(x => idsKH.Contains(x.IdKeHoachLuaChonNhaThau)
                && idsHD.Contains(x.IdHopDong)
                && x.IDLinhVuc != null
                && x.IDGiaiDoan != null
                && x.LoaiGoiThau != null)
                .Select(x => new
                {
                    x.IDLinhVuc,
                    x.TenLinhVuc,
                    x.Users,
                    x.IDGiaiDoan,
                    x.TenGiaiDoan,
                    x.IDDA,
                    x.TenDuAn,
                    x.OrderDuAn,
                    x.OrderTemp,
                    x.TenNhaThau,
                    x.TuNgay,
                    x.DenNgay,
                    x.LoaiGoiThau
                }).ToList();

            IEnumerable<DanhSachLuaChonNhaThau> lst;

            lst = query.GroupBy(x => new { x.IDLinhVuc, x.TenLinhVuc })
                .Select(x => new DanhSachLuaChonNhaThau()
                {
                    IDLinhVuc = x.Key.IDLinhVuc,
                    TenLinhVuc = x.Key.TenLinhVuc,
                    grpGiaiDoan = x.GroupBy(y => new { y.IDGiaiDoan, y.TenGiaiDoan }).Select(y => new GiaiDoanLCNT()
                    {
                        IDGiaiDoan = y.Key.IDGiaiDoan,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        grpDuAn = y.GroupBy(z => new { z.IDDA, z.TenDuAn }).Select(z => new DuAnLuaChonNhaThau()
                        {
                            IDDA = z.Key.IDDA,
                            TenDuAn = z.Key.TenDuAn,
                            OrderDuAn = z.Select(s1 => s1.OrderDuAn).FirstOrDefault(),
                            OrderTemp = z.Select(s1 => s1.OrderTemp).FirstOrDefault(),
                            Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                            grpGoiThau = z.Select(z1 => new GoiThauLuaChonNhaThau()
                            {
                                NhaThauThucHien = z1.TenNhaThau,
                                TuNgay = z1.TuNgay,
                                DenNgay = z1.DenNgay,
                                LoaiGoiThau = z1.LoaiGoiThau
                            })
                        }).OrderBy(o => o.OrderTemp).ThenBy(o => o.OrderDuAn )
                    }).OrderBy(y => y.IDGiaiDoan)
                });

            return lst.ToList();
        }

        public IEnumerable<DanhSachLuaChonNhaThau> GetHopDong_TuVan(Dictionary<string, string> lstUserNhanVien, string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            IEnumerable<int?> idsKH;
            IEnumerable<int> idsHD;
            idsKH = _KeHoachLuaChonNhaThauRepository.GetAll().Select(x => new
            {
                IDKH = x.IdDieuChinhKeHoachLuaChonNhaThau == null ? x.IdKeHoachLuaChonNhaThau : x.IdDieuChinhKeHoachLuaChonNhaThau,
                IDDA = x.IdDuAn
            }).GroupBy(x => new { x.IDDA, x.IDKH }).Distinct().Select(x => x.Key.IDKH);

            var a = idsKH.ToList();

            idsHD = _HopDongRepository.GetMulti(x => idsKH.Contains(x.GoiThau.IdKeHoachLuaChonNhaThau), new[] { "GoiThau" }).Select(x => new
            {
                IDGT = x.IdGoiThau,
                IDHD = x.IdHopDong
            }).GroupBy(x => new { x.IDGT }).Select(x => x.OrderByDescending(y => y.IDHD).Select(y => y.IDHD).FirstOrDefault());

            var query = _HopDongRepository.GetAll(new[] { "GoiThau", "GoiThau.KeHoachLuaChonNhaThau", "DanhMucNhaThau", "DuAn", "DuAn.DuAnUsers", "DuAn.LinhVucNganhNghe", "DuAn.GiaiDoanDuAn" })
                .Where(x => idsKH.Contains(x.GoiThau.IdKeHoachLuaChonNhaThau)
                && arrDuAn.Contains(x.IdDuAn.ToString())
                && idsHD.Contains(x.IdHopDong)
                && x.DuAn.IdLinhVucNganhNghe != null
                && x.DuAn.IdGiaiDoanDuAn != null
                && x.DuAn.IdGiaiDoanDuAn <= 2
                && x.GoiThau.LoaiGoiThau != null)
                .Select(x => new
                {
                    IDLinhVuc = x.DuAn.IdLinhVucNganhNghe,
                    TenLinhVuc = x.DuAn.LinhVucNganhNghe.TenLinhVucNganhNghe,
                    IDGiaiDoan = x.DuAn.IdGiaiDoanDuAn,
                    TenGiaiDoan = x.DuAn.GiaiDoanDuAn.TenGiaiDoanDuAn,
                    IDDA = x.IdDuAn,
                    TenDuAn = x.DuAn.TenDuAn,
                    Users = x.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    TenNhaThau = x.DanhMucNhaThau.TenNhaThau,
                    TuNgay = x.NgayBatDau == null ? "" : x.NgayBatDau.ToString(),
                    DenNgay = x.NgayHoanThanh == null ? "" : x.NgayHoanThanh.ToString(),
                    LoaiGoiThau = x.GoiThau.LoaiGoiThau
                }).ToList();

            IEnumerable<DanhSachLuaChonNhaThau> lst;

            lst = query.GroupBy(x => new { x.IDLinhVuc, x.TenLinhVuc })
                .Select(x => new DanhSachLuaChonNhaThau()
                {
                    IDLinhVuc = x.Key.IDLinhVuc,
                    TenLinhVuc = x.Key.TenLinhVuc,
                    grpGiaiDoan = x.GroupBy(y => new { y.IDGiaiDoan, y.TenGiaiDoan }).Select(y => new GiaiDoanLCNT()
                    {
                        IDGiaiDoan = y.Key.IDGiaiDoan,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        //grpGoiThau = y.Select(z => new GoiThauLuaChonNhaThau()
                        //{
                        //    IDDA = z.IDDA,
                        //    TenDuAn = z.TenDuAn,
                        //    Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        //    NhaThauThucHien = z.TenNhaThau,
                        //    TuNgay = z.TuNgay,
                        //    DenNgay = z.DenNgay,
                        //    LoaiGoiThau = z.LoaiGoiThau
                        //}).ToList()
                        grpDuAn = y.GroupBy(z => new { z.IDDA, z.TenDuAn }).Select(z => new DuAnLuaChonNhaThau()
                        {
                            IDDA = z.Key.IDDA,
                            TenDuAn = z.Key.TenDuAn,
                            Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                            grpGoiThau = z.Select(z1 => new GoiThauLuaChonNhaThau()
                            {
                                NhaThauThucHien = z1.TenNhaThau,
                                TuNgay = z1.TuNgay,
                                DenNgay = z1.DenNgay,
                                LoaiGoiThau = z1.LoaiGoiThau
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

            return lst;
        }

        public IEnumerable<DanhSachLuaChonNhaThau> GetHopDong_ThietBi(Dictionary<string, string> lstUserNhanVien, string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            IEnumerable<int?> idsKH;
            IEnumerable<int> idsHD;
            idsKH = _KeHoachLuaChonNhaThauRepository.GetAll().Select(x => new
            {
                IDKH = x.IdDieuChinhKeHoachLuaChonNhaThau == null ? x.IdKeHoachLuaChonNhaThau : x.IdDieuChinhKeHoachLuaChonNhaThau,
                IDDA = x.IdDuAn
            }).GroupBy(x => new { x.IDDA, x.IDKH }).Distinct().Select(x => x.Key.IDKH);

            var a = idsKH.ToList();

            idsHD = _HopDongRepository.GetMulti(x => idsKH.Contains(x.GoiThau.IdKeHoachLuaChonNhaThau), new[] { "GoiThau" }).Select(x => new
            {
                IDGT = x.IdGoiThau,
                IDHD = x.IdHopDong
            }).GroupBy(x => new { x.IDGT }).Select(x => x.OrderByDescending(y => y.IDHD).Select(y => y.IDHD).FirstOrDefault());

            var query = _HopDongRepository.GetAll(new[] { "GoiThau", "GoiThau.KeHoachLuaChonNhaThau", "DanhMucNhaThau", "DuAn", "DuAn.DuAnUsers", "DuAn.LinhVucNganhNghe", "DuAn.GiaiDoanDuAn" })
                .Where(x => idsKH.Contains(x.GoiThau.IdKeHoachLuaChonNhaThau)
                && arrDuAn.Contains(x.IdDuAn.ToString())
                && idsHD.Contains(x.IdHopDong)
                && x.DuAn.IdLinhVucNganhNghe != null
                && x.DuAn.IdGiaiDoanDuAn != null
                && x.DuAn.IdGiaiDoanDuAn >= 3
                && x.GoiThau.LoaiGoiThau != null)
                .Select(x => new
                {
                    IDLinhVuc = x.DuAn.IdLinhVucNganhNghe,
                    TenLinhVuc = x.DuAn.LinhVucNganhNghe.TenLinhVucNganhNghe,
                    IDGiaiDoan = x.DuAn.IdGiaiDoanDuAn,
                    TenGiaiDoan = x.DuAn.GiaiDoanDuAn.TenGiaiDoanDuAn,
                    IDDA = x.IdDuAn,
                    TenDuAn = x.DuAn.TenDuAn,
                    Users = x.DuAn.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    TenNhaThau = x.DanhMucNhaThau.TenNhaThau,
                    TuNgay = x.NgayBatDau == null ? "" : x.NgayBatDau.ToString(),
                    DenNgay = x.NgayHoanThanh == null ? "" : x.NgayHoanThanh.ToString(),
                    LoaiGoiThau = x.GoiThau.LoaiGoiThau
                }).ToList();

            IEnumerable<DanhSachLuaChonNhaThau> lst;

            lst = query.GroupBy(x => new { x.IDLinhVuc, x.TenLinhVuc })
                .Select(x => new DanhSachLuaChonNhaThau()
                {
                    IDLinhVuc = x.Key.IDLinhVuc,
                    TenLinhVuc = x.Key.TenLinhVuc,
                    grpGiaiDoan = x.GroupBy(y => new { y.IDGiaiDoan, y.TenGiaiDoan }).Select(y => new GiaiDoanLCNT()
                    {
                        IDGiaiDoan = y.Key.IDGiaiDoan,
                        TenGiaiDoan = y.Key.TenGiaiDoan,
                        //grpGoiThau = y.Select(z => new GoiThauLuaChonNhaThau()
                        //{
                        //    IDDA = z.IDDA,
                        //    TenDuAn = z.TenDuAn,
                        //    Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                        //    NhaThauThucHien = z.TenNhaThau,
                        //    TuNgay = z.TuNgay,
                        //    DenNgay = z.DenNgay,
                        //    LoaiGoiThau = z.LoaiGoiThau
                        //}).ToList()
                        grpDuAn = y.GroupBy(z => new { z.IDDA, z.TenDuAn }).Select(z => new DuAnLuaChonNhaThau()
                        {
                            IDDA = z.Key.IDDA,
                            TenDuAn = z.Key.TenDuAn,
                            Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                            grpGoiThau = z.Select(z1 => new GoiThauLuaChonNhaThau()
                            {
                                NhaThauThucHien = z1.TenNhaThau,
                                TuNgay = z1.TuNgay,
                                DenNgay = z1.DenNgay,
                                LoaiGoiThau = z1.LoaiGoiThau
                            }).ToList()
                        }).ToList()
                    }).ToList()
                }).ToList();

            return lst;
        }
    }
}
