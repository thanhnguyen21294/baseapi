﻿using Common;
using Data.Infrastructure;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IAppUserService
    {

        //Dictionary<string, string> GetUserRole();
        IEnumerable<AppUser> GetAllUser();
    }
    public class AppUserService : IAppUserService
    {
        public IAppUserRepository _AppUserRepository;
        private IUnitOfWork _unitOfWork;
        public AppUserService(IAppUserRepository appUserRepository, IUnitOfWork unitOfWork)
        {
            _AppUserRepository = appUserRepository;
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<AppUser> GetAllUser()
        {
            return _AppUserRepository.GetAllUser();
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
