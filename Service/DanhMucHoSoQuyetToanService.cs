﻿using Data.Infrastructure;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Models;

namespace Service
{
    public interface IDanhMucHoSoQuyetToanService
    {
        DanhMucHoSoQuyetToan Add(DanhMucHoSoQuyetToan DanhMucHoSoQuyetToans);

        void Update(DanhMucHoSoQuyetToan id);

        DanhMucHoSoQuyetToan Delete(int id);

        IEnumerable<DanhMucHoSoQuyetToan> GetAll();

        DanhMucHoSoQuyetToan GetById(int id);

        IEnumerable<DanhMucHoSoQuyetToan> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        void Save();
    }
    public class DanhMucHoSoQuyetToanService : IDanhMucHoSoQuyetToanService
    {
        private IDanhMucHoSoQuyetToanRepository _DanhMucHoSoQuyetToanRepository;
        private IUnitOfWork _unitOfWork;

        public DanhMucHoSoQuyetToanService(IDanhMucHoSoQuyetToanRepository DanhMucHoSoQuyetToanRepository, IUnitOfWork unitOfWork)
        {
            this._DanhMucHoSoQuyetToanRepository = DanhMucHoSoQuyetToanRepository;
            this._unitOfWork = unitOfWork;
        }
        public DanhMucHoSoQuyetToan Add(DanhMucHoSoQuyetToan DanhMucHoSoQuyetToans)
        {
            return _DanhMucHoSoQuyetToanRepository.Add(DanhMucHoSoQuyetToans);
        }
        public IEnumerable<DanhMucHoSoQuyetToan> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _DanhMucHoSoQuyetToanRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.Order).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public DanhMucHoSoQuyetToan Delete(int id)
        {
            return _DanhMucHoSoQuyetToanRepository.Delete(id);
        }

        public IEnumerable<DanhMucHoSoQuyetToan> GetAll()
        {
            return _DanhMucHoSoQuyetToanRepository.GetAll();
        }

        public DanhMucHoSoQuyetToan GetById(int id)
        {
            return _DanhMucHoSoQuyetToanRepository.GetMulti(x => x.IdDanhMucHoSoQuyetToan == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DanhMucHoSoQuyetToan DanhMucHoSoQuyetToans)
        {
            _DanhMucHoSoQuyetToanRepository.Update(DanhMucHoSoQuyetToans);
        }
    }
}
