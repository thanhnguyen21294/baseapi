﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ICamKetGiaiNganService
    {
        CamKetGiaiNgan Add(CamKetGiaiNgan CamKetGiaiNgan);

        void Update(CamKetGiaiNgan CamKetGiaiNgan);

        CamKetGiaiNgan Delete(int id);

        IEnumerable<CamKetGiaiNgan> GetAll();

        IEnumerable<CamKetGiaiNgan> GetByFilter(int page, int pageSize, out int totalRow);

        CamKetGiaiNgan GetById(int id);

        void Save();
    }

    public class CamKetGiaiNganService : ICamKetGiaiNganService
    {
        private ICamKetGiaiNganRepository _CamKetGiaiNganRepository;
        private IUnitOfWork _unitOfWork;

        public CamKetGiaiNganService(ICamKetGiaiNganRepository CamKetGiaiNganRepository, IUnitOfWork unitOfWork)
        {
            this._CamKetGiaiNganRepository = CamKetGiaiNganRepository;
            this._unitOfWork = unitOfWork;
        }
        public CamKetGiaiNgan Add(CamKetGiaiNgan CamKetGiaiNgan)
        {
            return _CamKetGiaiNganRepository.Add(CamKetGiaiNgan);
        }

        public CamKetGiaiNgan Delete(int id)
        {
            return _CamKetGiaiNganRepository.Delete(id);
        }

        public IEnumerable<CamKetGiaiNgan> GetAll()
        {
            return _CamKetGiaiNganRepository.GetAll();
        }

        public IEnumerable<CamKetGiaiNgan> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _CamKetGiaiNganRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdCamKetGiaiNgan).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public CamKetGiaiNgan GetById(int id)
        {
            return _CamKetGiaiNganRepository.GetMulti(x => x.IdCamKetGiaiNgan == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(CamKetGiaiNgan CamKetGiaiNgan)
        {
            _CamKetGiaiNganRepository.Update(CamKetGiaiNgan);
        }
    }
}
