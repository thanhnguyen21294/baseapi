﻿using Data.Infrastructure;
using Data.Repositories.BCRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
//using Z.EntityFramework.Plus;
using Model.Models.BCModel;
using System.Text;
using System.Threading.Tasks;
using Data.Repositories;
using Data.Repositories.QLTDRepository;
using Model.Models.BCModel.BaocaoTiendo;

namespace Service.BCService
{
    public interface IBaoCaoTienDoService
    {
        #region Báo cáo tiến độ kế hoạch xây dựng cơ bản
        #region Cây dự án
        List<object> GetListDACbiDTu(string IDDuAns);
        List<object> GetListDA(string IDDuAns);

        Dictionary<int, List<object>> GetDuAnCon(string IDDuAns);
        #endregion
        #region Chủ trương đầu tư
        Dictionary<int, int> Getdic_CTDT_TGHTCT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_NGNV(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_CTDD(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_LCTDT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_NTHSPD(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_PDCTDT(string IDDuAns);
        Dictionary<int, string> Getdic_CTDT_KK(string IDDuAns);
        Dictionary<int, string> Getdic_CTDT_GP(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CTDT_TDC(string IDDuAns);
        #endregion

        #region Chuẩn bị đầu tư
        Dictionary<int, List<object>> Getdic_CBDT_BCPATK(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_PDT_CBDT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_PKHKCNT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_KSHT_CGDD_HTKT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_PDQH(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_KSDC(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_HTPDDA(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_TTDPDDA(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_PCCC_TTK(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_DLHS(string IDDuAns);
        //Dictionary<int, List<object>> Getdic_CBDT_KKGP(string IDDuAns);
        Dictionary<int, string> Getdic_CBDT_KK(string IDDuAns);
        Dictionary<int, string> Getdic_CBDT_GP(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBDT_TDC(string IDDuAns);
        #endregion

        #region Chuẩn bị thực hiện
        Dictionary<int, List<object>> Getdic_CBTH_PDT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBTH_PKHLCNT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBTH_HTPD_THBV(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBTH_TTD_TKBV(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBTH_DLHS(string IDDuAns);
        //Dictionary<int, List<object>> Getdic_CBTH_KKGP(string IDDuAns);
        Dictionary<int, string> Getdic_CBTH_KK(string IDDuAns);
        Dictionary<int, string> Getdic_CBTH_GP(string IDDuAns);
        Dictionary<int, List<object>> Getdic_CBTH_TDC(string IDDuAns);
        #endregion

        #region Thực hiện
        Dictionary<int, List<object>> Getdic_THDT_HTTC(string IDDuAns);
        Dictionary<int, List<object>> Getdic_THDT_DTCXD(string IDDuAns);
        Dictionary<int, List<object>> Getdic_THDT_DLCNT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_THDT_CTHLCNT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_THDT_GPMB(string IDDuAns);
        //Dictionary<int, List<object>> Getdic_THDT_KKGP(string IDDuAns);
        Dictionary<int, string> Getdic_TH_KK(string IDDuAns);
        Dictionary<int, string> Getdic_TH_GP(string IDDuAns);
        Dictionary<int, List<object>> Getdic_TH_TDC(string IDDuAns);
        #endregion

        #region Quyết toán dự án
        Dictionary<int, List<object>> Getdic_QT_HTQT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_QT_HTHS_QLCL(string IDDuAns);
        Dictionary<int, List<object>> Getdic_QT_HTHS_HSQT(string IDDuAns);
        Dictionary<int, List<object>> Getdic_QT_HTHS_KTDL(string IDDuAns);
        Dictionary<int, List<object>> Getdic_QT_HTHS_TTQT(string IDDuAns);
        Dictionary<int, string> Getdic_QT_KKGP(string IDDuAns);
        Dictionary<int, List<object>> Getdic_QT_TDC(string IDDuAns);
        #endregion

        #region Kế hoạch vốn
        Dictionary<int, List<object>> Getdic_KHV_BD(string IDDuAns, int iNam);
        Dictionary<int, List<object>> Getdic_KHV_SDC(string IDDuAns, int iNam);
        Dictionary<int, List<object>> Getdic_KHV_TTCPK(string IDDuAns, int iNam);
        Dictionary<int, List<object>> Getdic_KHV_TTHD(string IDDuAns, int iNam);
        #endregion
        #endregion

        #region Báo cáo tiến độ dự án chuẩn bị đầu tư --- Báo cáo ủy ban PL1
        #region Cây dự án
        List<object> GetListTD_DA_CBDT_CDA(string IDDuAns);
        Dictionary<int, List<object>> GetdicTD_DA_DAC(string IDDuAns);
        #endregion
        BC_UyBan_PL1_Them GetDACbiDTu(int IDDuAn);

        #region Kế hoạch vốn
        Dictionary<int, double> GetDic_TD_DA_CBDT_KHV(string IDDuAns, int iNam);
        #endregion

        #region Giải ngân
        Dictionary<int, double> GetDic_TD_DA_CBDT_GN(string IDDuAns, int iNam);
        #endregion

        #region Hoàn thành trình duyệt
        Dictionary<int, int> Getdic_DA_CBDT_TDDA(string IDDuAns);
        #endregion

        #region Hoàn thành lập chủ trương đầu tư
        Dictionary<int, List<object>> Getdic_DA_CBDT_HT_LCTDT(string IDDuAns);
        #endregion
        #endregion

        #region kế hoạch vốn đề xuất điều chỉnh
        Dictionary<int, double> Getdic_DA_KHV_DXDC(string IDDuAns, int iNam);
        #endregion

        #region Tiến độ thực hiện
        #region Lập chủ trương đầu tư
        Dictionary<int, string> GetDic_TDTH_LCTDT(string IDDuAns);
        #endregion
        #region Chuẩn bị đầu tư
        Dictionary<int, string> GetDic_TDTH_CBDT(string IDDuAns);
        #endregion
        #region Chuẩn bị thực hiện
        Dictionary<int, string> GetDic_TDTH_CBTH(string IDDuAns);
        #endregion
        #region Thực hiện - DTCXD
        Dictionary<int, string> GetDic_TDTH_TH_DTCXD(string IDDuAns);
        #endregion
        #region Thực hiện - GPMB
        Dictionary<int, string> GetDic_TDTH_TH_GPMB(string IDDuAns);
        #endregion
        #endregion

        #region Báo cáo tiến độ dự án thực hiện -- Báo cáo ủy ban PL2
        #region Cây dự án
        List<object> GetListTD_DA_TH_CDA(string IDDuAns);
        #endregion
        #region Khối lượng thực hiện
        Dictionary<int, double> GetDic_KLTH(string IDDuAns, int iNam);
        #endregion
        #endregion

        #region Cây dự án
        #region Quyết toán dự án
        List<object> GetList_QT_DA(string IDDuAns);
        #endregion
        #region Thực hiện dự án
        List<object> GetList_TH_DA(string IDDuAns);
        #endregion
        #endregion

        #region Điều hành
        #region Chủ trương đầu tư
        List<object> GetList_DH_TDDA_CTDT_CDA(string idUser, string filter);
        #endregion
        #region Chuẩn bị đầu tư
        List<object> GetList_DH_TDDA_CBDT_CDA(string idUser,string filter);
        #endregion
        #region Chuẩn bị thực hiện
        List<object> GetList_DH_TDDA_CBTH_CDA(string idUser, string filter);
        #endregion
        #region Thực hiện
        List<object> GetList_DH_TDDA_TH_CDA(string idUser, string filter);
        #endregion
        #region Quyết toán
        List<object> GetList_DH_TDDA_QT_CDA(string idUser, string filter);
        #endregion
        #region Kế hoạch mới nhất
        List<object> GetList_DH_TDDA_KHMN(string idUser);
        #endregion
        #endregion

        #region Kế hoạch mới nhất, trạng thái hoàn thành dưới mức 4
        List<object> KHMN_DGDPD(string idUser, string filter);
        List<object> KHMN(string idUser);
        #endregion

        #region Điều hành - Công việc chưa hoàn thành
        List<object> GetLst_CV_CHT_CTDT(string idUser);
        List<object> GetLst_CV_CHT_CBDT(string idUser);
        List<object> GetLst_CV_CHT_CBTH(string idUser);
        List<object> GetLst_CV_CHT_TH(string idUser);
        List<object> GetLst_CV_CHT_QT(string idUser);
        #endregion

        #region Điều hành - Công việc chưa hoàn thành
        List<object> GetLst_CVChuyenQT(string idUser);
        #endregion

        #region Báo cáo tổng hợp QLTD
        #region Cây dự án
        //IEnumerable<BaoCaoTongHopQLTD> Get_CayDuAn_TongHop_QLTD(string IDDuAns);
        #endregion
        #endregion

        #region Thông tin cây dự án
        //IEnumerable<object> GetCayDuAn(string IDDuAns);
        #endregion


        #region Báo cáo Phụ lục 1 _ Sửa
        IEnumerable<BC_TienDo_UB_PL1_CayDuAn> Get_BC_UB_PL1_CayDA(string IDDuAns, Dictionary<string, string> lstUserNhanVien);
        IEnumerable<BC_Uyban_PL1_PDDA> GetDic_PDDA_CBDT(string IDDuAns);
        IEnumerable<BC_TienDo_KHV> Get_KHV_For_BaoCao(string IDDuAns, int iNam);
        IEnumerable<BC_TienDo_KHV> Get_KHV_For_BaoCaoAll(string IDDuAns, int iNam);
        IEnumerable<BC_TienDo_KHV_New> Get_KHV_For_BaoCao_KeHoachGN(string IDDuAns, int iNam);
        
        IEnumerable<BC_TienDo_GN> Get_GN_For_BaoCao_PL1(string IDDuAns, int iNam);
        IEnumerable<BC_ListID> GetListIDDA_For_BC_TienDo(string IDDuAns);
        #endregion
    }

    public class BaoCaoTienDoService : IBaoCaoTienDoService//, IRepository<BC_TienDo_UB_PL1_CayDuAn>
    {
        private IUnitOfWork _unitOfWork;
        private IQLTD_TTDA_BCRepository _QLTD_TTDA_BCRepository;
        private IDuAnRepository _duAnRepository;
        private IKeHoachVonRepository _keHoachVonRepository;
        private IQLTD_CTCV_ChuanBiDauTuRepository _qLTD_CTCV_ChuanBiDauTuRepository;
        private IQLTD_KeHoachRepository _qLTD_KeHoachRepository;
        private IVonCapNhatKLNTRepository _vonCapNhatKLNTRepository;

        public BaoCaoTienDoService(IUnitOfWork unitOfWork,
            IDuAnRepository duAnRepository,
            IKeHoachVonRepository keHoachVonRepository,
            IQLTD_CTCV_ChuanBiDauTuRepository qLTD_CTCV_ChuanBiDauTuRepository,
            IQLTD_KeHoachRepository qLTD_KeHoachRepository,
            IQLTD_TTDA_BCRepository QLTD_TTDA_BCRepository,
            IVonCapNhatKLNTRepository vonCapNhatKLNTRepository)
        {
            this._unitOfWork = unitOfWork;
            this._QLTD_TTDA_BCRepository = QLTD_TTDA_BCRepository;
            this._keHoachVonRepository = keHoachVonRepository;
            this._qLTD_CTCV_ChuanBiDauTuRepository = qLTD_CTCV_ChuanBiDauTuRepository;
            this._qLTD_KeHoachRepository = qLTD_KeHoachRepository;
            this._duAnRepository = duAnRepository;
            this._vonCapNhatKLNTRepository = vonCapNhatKLNTRepository;
        }
        #region Báo cáo tiến độ kế hoạch xây dựng cơ bản
        #region Cây dự án
        public List<object> GetListDA(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetListDA(IDDuAns);
        }
        public BC_UyBan_PL1_Them GetDACbiDTu(int IDDuAn)
        {
            return _QLTD_TTDA_BCRepository.GetDACbiDTu(IDDuAn);
        }
        public List<object> GetListDACbiDTu(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetListDACbiDTu(IDDuAns);
        }

        public Dictionary<int, List<object>> GetDuAnCon(string IDDuAns)
        {
            List<object> lstDuAnCon = _QLTD_TTDA_BCRepository.GetDuAnCon(IDDuAns);

            Dictionary<int, List<object>> dicDuAnCon = new Dictionary<int, List<object>>();

            dicDuAnCon = (from item in
                             (from item in lstDuAnCon
                              select new
                              {
                                  IDDA = (int)GetValueObject(item, "IDDA"),
                                  IDDACha = (int)GetValueObject(item, "IDDACha"),
                                  TenDuAn = GetValueObject(item, "TenDuAn"),
                                  NhomDuAn = GetValueObject(item, "NhomDuAn"),
                                  DiaDiemXayDung = GetValueObject(item, "DiaDiemXayDung"),
                                  NangLucThietKe = GetValueObject(item, "NangLucThietKe"),
                                  ThoiGianThucHien = GetValueObject(item, "ThoiGianThucHien"),
                                  MaSoDuAn = GetValueObject(item, "MaSoDuAn"),
                                  TenChuDauTu = GetValueObject(item, "TenChuDauTu"),
                                  PDDA_BCKTKT_SoQD = GetValueObject(item, "PDDA_BCKTKT_SoQD"),
                                  PDDA_BCKTKT_NgayPD = GetValueObject(item, "PDDA_BCKTKT_NgayPD"),
                                  TMDT_ChuaPD = GetValueObject(item, "TMDT_ChuaPD")
                              })
                          group item by new { item.IDDACha }
                         into item1
                          select new
                          {
                              IDDACha = item1.Key.IDDACha,
                              grpDuAnCon = item1.Select(x => new
                              {
                                  IDDA = x.IDDA,
                                  TenDuAn = x.TenDuAn,
                                  NhomDuAn = x.NhomDuAn,
                                  DiaDiemXayDung = x.DiaDiemXayDung,
                                  NangLucThietKe = x.NangLucThietKe,
                                  ThoiGianThucHien = x.ThoiGianThucHien,
                                  MaSoDuAn = x.MaSoDuAn,
                                  TenChuDauTu = x.TenChuDauTu,
                                  PDDA_BCKTKT_SoQD = x.PDDA_BCKTKT_SoQD,
                                  PDDA_BCKTKT_NgayPD = x.PDDA_BCKTKT_NgayPD,
                                  TMDT_ChuaPD = x.TMDT_ChuaPD
                              }).ToList<object>()
                          }).ToDictionary(x => x.IDDACha, x => x.grpDuAnCon);

            return dicDuAnCon;
        }
        #endregion

        #region Chủ trương đầu tư
        public Dictionary<int, int> Getdic_CTDT_TGHTCT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_TGHTCT(IDDuAns);
            var dic = (from item in lst
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           ThoiGian = (int)GetValueObject(item, "ThoiGian")
                       }).ToDictionary(x => x.IDDA, x => x.ThoiGian);
            return dic;

        }
        public Dictionary<int, List<object>> Getdic_CTDT_NGNV(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_NGNV(IDDuAns);
            return GetDic(lst, "NgayGiaoNVu");
        }
        public Dictionary<int, List<object>> Getdic_CTDT_CTDD(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_CTDD(IDDuAns);
            string[] szAr = { "CoSanDiaDiem", "SoQDChapThuan", "NgayQDChapThuan" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CTDT_LCTDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_LCTDT(IDDuAns);
            string[] szAr = { "KhoKhanVuongMac", "KienNghi" };
            return GetDic_2(lst, szAr);

        }
        public Dictionary<int, List<object>> Getdic_CTDT_NTHSPD(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_NTHSPD(IDDuAns);
            return GetDic(lst, "NgayTrinhHoSoPD");
        }
        public Dictionary<int, List<object>> Getdic_CTDT_PDCTDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_PDCTDT(IDDuAns);
            string[] szAr = { "SoVBPheDuyet", "NgayPheDuyet", "GiaTri", "NgayPheDuyetThucTe" };
            return GetDic_4(lst, szAr);
        }
        public Dictionary<int, string> Getdic_CTDT_KK(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_KK(IDDuAns);
            string[] ar = { "grpKKVM_LCTDT", "KKVM_LCTDT" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, string> Getdic_CTDT_GP(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_GP(IDDuAns);
            string[] ar = { "grpGP_LCTDT", "GP_LCTDT" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, List<object>> Getdic_CTDT_TDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CTDT_TDC(IDDuAns);
            return GetDic(lst, "NoiDung");
        }

        #endregion

        #region Chuẩn bị đầu tư 
        public Dictionary<int, List<object>> Getdic_CBDT_BCPATK(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_BCPATK(IDDuAns);
            return GetDic(lst, "BaoCaoPAnTKe");
        }
        public Dictionary<int, List<object>> Getdic_CBDT_PDT_CBDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_PDT_CBDT(IDDuAns);
            string[] szAr = { "SoPheDToan", "NgayPheDToan", "GiaTriPheDToan" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_PKHKCNT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_PKHKCNT(IDDuAns);
            string[] szAr = { "SoPheKH", "NgayPheKH", "GiaTriPheKH" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_KSHT_CGDD_HTKT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_KSHT_CGDD_HTKT(IDDuAns);
            string[] szAr = { "KhaoSatHienTrang", "ChiGioiDuongDo", "SoLieuHTKT" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_PDQH(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_PDQH(IDDuAns);
            string[] szAr = { "KhongPheDuyet", "SoPheDuyetQHCT", "NgayPheDuyetQHCT", "SoPheDuyetQHTMB", "NgayPheDuyetQHTMB" };
            return GetDic_5(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_KSDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_KSDC(IDDuAns);
            return GetDic(lst, "KhaoSatDiaChat");
        }
        public Dictionary<int, List<object>> Getdic_CBDT_HTPDDA(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_HTPDDA(IDDuAns);
            string[] szAr = { "SoPheDuyetBCKTKT", "NgayPheDuyetBCKTKT", "GiaTriPheDuyetBCKTKT" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_TTDPDDA(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_TTDPDDA(IDDuAns);
            string[] szAr = { "SoToTrinhHoSoBCKTKT", "NgayTrinhHoSoBCKTKT" };
            return GetDic_2(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_PCCC_TTK(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_PCCC_TTK(IDDuAns);
            string[] szAr = { "PCCC", "TTK" };
            return GetDic_2(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBDT_DLHS(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_DLHS(IDDuAns);
            string[] szAr = { "LHSCBDT_SoQD", "LHSCBDT_NgayLaySo", "LHSCBDT_NgayHoanThanh", "LHSCBDT_KhoKhanVuongMac", "LHSCBDT_KienNghiDeXuat" };
            return GetDic_5(lst, szAr);
        }
        //public Dictionary<int, List<object>> Getdic_CBDT_KKGP(string IDDuAns)
        //{
        //    List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_KKGP(IDDuAns);
        //    string[] szAr = { "NguyenNhan", "GiaiPhap" };
        //    return GetDic_2(lst, szAr);
        //}
        public Dictionary<int, string> Getdic_CBDT_KK(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_KK(IDDuAns);
            string[] ar = { "grpKKVM_CBDT", "KKVM_CBDT" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, string> Getdic_CBDT_GP(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_GP(IDDuAns);
            string[] ar = { "grpGP_CBDT", "GP_CBDT" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, List<object>> Getdic_CBDT_TDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_TDC(IDDuAns);
            return GetDic(lst, "NoiDung");
        }
        #endregion

        #region Chuẩn bị thực hiện
        public Dictionary<int, List<object>> Getdic_CBTH_PDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_PDT(IDDuAns);
            string[] szAr = { "PDDT_SoQuyetDinh", "PDDT_NgayPheDuyet", "PDDT_GiaTri" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBTH_PKHLCNT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_PKHLCNT(IDDuAns);
            string[] szAr = { "PDKHLCNT_SoQuyetDinh", "PDKHLCNT_NgayPheDuyet", "PDKHLCNT_GiaTri" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBTH_HTPD_THBV(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_HTPD_THBV(IDDuAns);
            string[] szAr = { "PDDA_SoQuyetDinh", "PDDA_NgayPheDuyet", "PDDA_GiaTri" };
            return GetDic_3(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBTH_TTD_TKBV(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_TTD_TKBV(IDDuAns);
            string[] szAr = { "THS_SoToTrinh", "LHS_NgayTrinh" };
            return GetDic_2(lst, szAr);
        }
        public Dictionary<int, List<object>> Getdic_CBTH_DLHS(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_DLHS(IDDuAns);
            string[] szAr = { "LHSCBDT_KhoKhanVuongMac", "LHSCBDT_KienNghiDeXuat" };
            return GetDic_2(lst, szAr);
        }

        //public Dictionary<int, List<object>> Getdic_CBTH_KKGP(string IDDuAns)
        //{
        //    List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_KKGP(IDDuAns);
        //    string[] szAr = { "NguyenNhan", "GiaiPhap" };
        //    return GetDic_2(lst, szAr);
        //}

        public Dictionary<int, string> Getdic_CBTH_KK(string IDDuAns)
        {
            //List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_KK(IDDuAns);
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_KK(IDDuAns);
            string[] ar = { "grpKKVM_CBTH", "KKVM_CBTH" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, string> Getdic_CBTH_GP(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBTH_GP(IDDuAns);
            //List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_GP(IDDuAns);
            string[] ar = { "grpGP_CBTH", "GP_CBTH" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, List<object>> Getdic_CBTH_TDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_CBDT_TDC(IDDuAns);
            return GetDic(lst, "NoiDung");
        }
        #endregion

        #region Thực hiện
        public Dictionary<int, List<object>> Getdic_THDT_HTTC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_HTTC(IDDuAns);
            return GetDic(lst, "NgayHoanThanh");
        }

        public Dictionary<int, List<object>> Getdic_THDT_DTCXD(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_DTCXD(IDDuAns);
            string[] arsz = { "NgayBanGiao", "ThoiGianKhoiCongHoanThanh", "KhoKhanVuongMac" };
            return GetDic_3(lst, arsz);
        }

        public Dictionary<int, List<object>> Getdic_THDT_DLCNT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_DLCNT(IDDuAns);
            string[] arsz = { "SoQDPD", "NgayQDPD", "GiaTri" };
            return GetDic_3(lst, arsz);
        }

        public Dictionary<int, List<object>> Getdic_THDT_CTHLCNT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_CTHLCNT(IDDuAns);
            string[] arsz = { "SoQDKH", "NgayQDKH", "NgayTrinhKH", "NgayTrinhBVTC", "DangLapTK", "KhoKhan" };
            return GetDic_6(lst, arsz);
        }

        public Dictionary<int, List<object>> Getdic_THDT_GPMB(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_GPMB(IDDuAns);
            string[] arsz = { "DamBaoTienDo", "KhoKhanVuongMac", "DeXuatGiaiPhap" };
            return GetDic_3(lst, arsz);
        }

        //public Dictionary<int, List<object>> Getdic_THDT_KKGP(string IDDuAns)
        //{
        //    List<object> lst = _QLTD_TTDA_BCRepository.GetList_THDT_KKGP(IDDuAns);
        //    string[] arsz = { "NguyenNhan", "GiaiPhap" };
        //    return GetDic_2(lst, arsz);
        //}
        public Dictionary<int, string> Getdic_TH_KK(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TH_KK(IDDuAns);
            string[] ar = { "grpKKVM_TH", "KKVM_TH" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, string> Getdic_TH_GP(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TH_GP(IDDuAns);
            string[] ar = { "grpGP_TH", "GP_TH" };
            return GetDic_Int_String(lst, ar);
        }

        public Dictionary<int, List<object>> Getdic_TH_TDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TH_TDC(IDDuAns);
            return GetDic(lst, "NoiDung");
        }
        #endregion

        #region Quyết toán dự án
        public Dictionary<int, List<object>> Getdic_QT_HTQT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_HTQT(IDDuAns);
            string[] szAr = { "SoQD", "NgayQD", "GiaTriQT", "CongNoThu", "CongNoTra", "BoTriVonTra" };
            return GetDic_6(lst, szAr);
        }

        public Dictionary<int, List<object>> Getdic_QT_HTHS_QLCL(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_HTHS_QLCL(IDDuAns);
            return GetDic(lst, "HoSoQLCL");
        }

        public Dictionary<int, List<object>> Getdic_QT_HTHS_HSQT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_HTHS_HSQT(IDDuAns);
            return GetDic(lst, "GiaTri");
        }

        public Dictionary<int, List<object>> Getdic_QT_HTHS_KTDL(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_HTHS_KTDL(IDDuAns);
            string[] szAr = { "KhongKiemToan", "DangKiemToan", "GiaTriQT" };
            return GetDic_3(lst, szAr);
        }

        public Dictionary<int, List<object>> Getdic_QT_HTHS_TTQT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_HTHS_TTQT(IDDuAns);
            string[] szAr = { "DangThamTra", "SoThamTra", "NgayThamTra", "GiaTri" };
            return GetDic_4(lst, szAr);
        }

        public Dictionary<int, string> Getdic_QT_KKGP(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_KKGP(IDDuAns);
            string[] szAr = { "grpKKVM_QT", "KKVM_QT", "GP_QT" };
            return GetDic_Int_String_KKGP_QT(lst, szAr);
        }

        public Dictionary<int, List<object>> Getdic_QT_TDC(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_QT_TDC(IDDuAns);
            return GetDic(lst, "NoiDung");
        }
        #endregion

        #region Kế hoạch vốn
        public Dictionary<int, List<object>> Getdic_KHV_BD(string IDDuAns, int iNam)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_KHV_BD(IDDuAns, iNam);
            return GetDic(lst, "GiaTri");
        }
        public Dictionary<int, List<object>> Getdic_KHV_SDC(string IDDuAns, int iNam)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_KHV_SDC(IDDuAns, iNam);
            return GetDic(lst, "GiaTri");
        }
        public Dictionary<int, List<object>> Getdic_KHV_TTCPK(string IDDuAns, int iNam)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_KHV_TTCPK(IDDuAns, iNam);
            return GetDic(lst, "GiaTri");
        }
        public Dictionary<int, List<object>> Getdic_KHV_TTHD(string IDDuAns, int iNam)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_KHV_TTHD(IDDuAns, iNam);
            return GetDic(lst, "GiaTri");
        }
        #endregion
        #endregion

        #region Báo cáo tiến độ dự án chuẩn bị đầu tư -- Báo cáo ủy ban PL1
        #region Cây dự án
        public List<object> GetListTD_DA_CBDT_CDA(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetListTD_DA_CBDT_CDA(IDDuAns);
        }
        public Dictionary<int, List<object>> GetdicTD_DA_DAC(string IDDuAns)
        {
            List<object> lstDuAnCon = _QLTD_TTDA_BCRepository.GetListTD_DA_DAC(IDDuAns);

            Dictionary<int, List<object>> dicDuAnCon = new Dictionary<int, List<object>>();

            dicDuAnCon = (from item in
                             (from item in lstDuAnCon
                              select new
                              {
                                  IDDA = (int)GetValueObject(item, "IDDA"),
                                  IDDACha = (int)GetValueObject(item, "IDDACha"),
                                  TenDuAn = GetValueObject(item, "TenDuAn"),
                                  NangLucThietKe = GetValueObject(item, "NangLucThietKe"),
                                  ThoiGianThucHien = GetValueObject(item, "ThoiGianThucHien"),
                                  TenChuDauTu = GetValueObject(item, "TenChuDauTu")
                              })
                          group item by new { item.IDDACha }
                          into item1
                          select new
                          {
                              IDDACha = item1.Key.IDDACha,
                              grpDuAnCon = item1.Select(x => new
                              {
                                  IDDA = x.IDDA,
                                  TenDuAn = x.TenDuAn,
                                  NangLucThietKe = x.NangLucThietKe,
                                  ThoiGianThucHien = x.ThoiGianThucHien,
                                  TenChuDauTu = x.TenChuDauTu
                              }).ToList<object>()
                          }).ToDictionary(x => x.IDDACha, x => x.grpDuAnCon);

            return dicDuAnCon;
        }
        #endregion

        #region Kế hoạch vốn
        public Dictionary<int, double> GetDic_TD_DA_CBDT_KHV(string IDDuAns, int iNam)
        {
            var lstKHV = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_KHV(IDDuAns, iNam);
            var lstKHVDC = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_KHVDC(IDDuAns, iNam);

            var lst = (from item in lstKHV
                       join item1 in lstKHVDC on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lst1
                       from item2 in lst1.DefaultIfEmpty()
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           GiaTri = item2 == null ? (double)GetValueObject(item, "GiaTri") : (double)GetValueObject(item2, "GiaTri")
                       }).ToDictionary(x => x.IDDA, x => x.GiaTri);
            return lst;
        }
        #endregion

        #region Giải ngân
        public Dictionary<int, double> GetDic_TD_DA_CBDT_GN(string IDDuAns, int iNam)
        {
            var lstTTHD = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_TTHD(IDDuAns, iNam);
            var lstTTCPK = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_TTCPK(IDDuAns, iNam);

            var lst = (from item in lstTTHD
                       join item1 in lstTTCPK
                       on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           GiaTri = (double)GetValueObject(item, "GiaTriTTHD") + (double)GetValueObject(item1, "GiaTriCPK")
                       }).ToDictionary(x => x.IDDA, x => x.GiaTri);
            return lst;
        }
        #endregion

        #region Hoàn thành trình duyệt
        public Dictionary<int, int> Getdic_DA_CBDT_TDDA(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_TDDA(IDDuAns);

            var dic = (from item in lst
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           TrangThaiTrinh = (int)GetValueObject(item, "TrangThaiTrinh")
                       }).ToDictionary(x => x.IDDA, x => x.TrangThaiTrinh);
            return dic;
        }
        #endregion

        #region Hoàn thành lập chủ trương đầu tư
        public Dictionary<int, List<object>> Getdic_DA_CBDT_HT_LCTDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_DA_CBDT_HT_LCTDT(IDDuAns);
            string[] szAr = { "TrangThaiTrinh", "KhoKhanVuongMac", "KienNghi" };
            return GetDic_3(lst, szAr);
        }
        #endregion
        #endregion

        #region Kế hoạch vốn đề xuất điều chỉnh
        public Dictionary<int, double> Getdic_DA_KHV_DXDC(string IDDuAns, int iNam)
        {
            var lst = _QLTD_TTDA_BCRepository.GetList_DA_KHV_DXDC(IDDuAns, iNam);

            var dic = (from item in lst
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           GiaTri = (double)GetValueObject(item, "GiaTri")
                       }).ToDictionary(x => x.IDDA, x => x.GiaTri);

            return dic;
        }
        #endregion

        #region Tiến độ thực hiện
        #region Lập chủ trương đầu tư
        public Dictionary<int, string> GetDic_TDTH_LCTDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TDTH_LCTDT(IDDuAns);
            string[] ar = { "grpTD_LCTDT", "TD_LCTDT" };
            return GetDic_Int_String(lst, ar);
        }
        #endregion

        #region Chuẩn bị đầu tư
        public Dictionary<int, string> GetDic_TDTH_CBDT(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TDTH_CBDT(IDDuAns);
            string[] ar = { "grpTD_CBDT", "TD_CBDT" };
            return GetDic_Int_String(lst, ar);
        }
        #endregion

        #region Chuẩn bị thực hiện
        public Dictionary<int, string> GetDic_TDTH_CBTH(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TDTH_CBTH(IDDuAns);
            string[] ar = { "grpTD_CBTH", "TD_CBTH" };
            return GetDic_Int_String(lst, ar);
        }
        #endregion

        #region Thực hiện - DTCXD
        public Dictionary<int, string> GetDic_TDTH_TH_DTCXD(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TDTH_TH_DTCXD(IDDuAns);
            string[] ar = { "grpTD_TH", "TD_TH" };
            return GetDic_Int_String(lst, ar);
        }
        #endregion

        #region Thực hiện - GPMB
        public Dictionary<int, string> GetDic_TDTH_TH_GPMB(string IDDuAns)
        {
            List<object> lst = _QLTD_TTDA_BCRepository.GetList_TDTH_TH_GPMB(IDDuAns);
            string[] ar = { "grpTD_TH", "TD_TH" };
            return GetDic_Int_String(lst, ar);
        }
        #endregion
        //#region Chuẩn bị thực hiện
        //public Dictionary<int, string> dic_TDTH_CBTH(string IDDuAns)
        //{

        //}
        //#endregion
        #endregion

        #region Báo cáo tiến độ dự án thực hiện --- Báo cáo ủy ban PL2
        #region Cây dự án
        public List<object> GetListTD_DA_TH_CDA(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetListTD_DA_TH_CDA(IDDuAns);
        }
        #endregion
        #region Khối lượng thực hiện
        public Dictionary<int, double> GetDic_KLTH(string IDDuAns, int iNam)
        {
            var lst = _QLTD_TTDA_BCRepository.GetLstKhoiLuongThucHien(IDDuAns, iNam);

            var dic = (from item in lst
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           KLTH = (double)GetValueObject(item, "KLTH")
                       }).ToDictionary(x => x.IDDA, x => x.KLTH);
            return dic;
        }
        #endregion
        #endregion

        #region Cây dự án
        #region Quyết toán dự án
        public List<object> GetList_QT_DA(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetList_QTDA_CDA(IDDuAns);
        }
        #endregion

        #region Thực hiện dự án
        public List<object> GetList_TH_DA(string IDDuAns)
        {
            return _QLTD_TTDA_BCRepository.GetList_CDA_THDT(IDDuAns);
        }
        #endregion
        #endregion

        #region  Dùng chung
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }

        public Dictionary<int, List<object>> GetDic(List<object> lst, string sz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz = GetValueObject(item, sz),
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str = x.sz,
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetDic_2(List<object> lst, string[] arsz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, arsz[0]),
                               sz2 = GetValueObject(item, arsz[1])
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetDic_3(List<object> lst, string[] arsz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, arsz[0]),
                               sz2 = GetValueObject(item, arsz[1]),
                               sz3 = GetValueObject(item, arsz[2])
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                               str3 = x.sz3
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetDic_4(List<object> lst, string[] arsz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, arsz[0]),
                               sz2 = GetValueObject(item, arsz[1]),
                               sz3 = GetValueObject(item, arsz[2]),
                               sz4 = GetValueObject(item, arsz[3]),
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                               str3 = x.sz3,
                               str4 = x.sz4,
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetDic_5(List<object> lst, string[] arsz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, arsz[0]),
                               sz2 = GetValueObject(item, arsz[1]),
                               sz3 = GetValueObject(item, arsz[2]),
                               sz4 = GetValueObject(item, arsz[3]),
                               sz5 = GetValueObject(item, arsz[4])
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                               str3 = x.sz3,
                               str4 = x.sz4,
                               str5 = x.sz5
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, List<object>> GetDic_6(List<object> lst, string[] arsz)
        {
            var dic = (from item in
                          (from item in lst
                           select new
                           {
                               IDDA = (int)GetValueObject(item, "IDDA"),
                               sz1 = GetValueObject(item, arsz[0]),
                               sz2 = GetValueObject(item, arsz[1]),
                               sz3 = GetValueObject(item, arsz[2]),
                               sz4 = GetValueObject(item, arsz[3]),
                               sz5 = GetValueObject(item, arsz[4]),
                               sz6 = GetValueObject(item, arsz[5])
                           })
                       group item by new { item.IDDA }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grp = item1.Select(x => new
                           {
                               str1 = x.sz1,
                               str2 = x.sz2,
                               str3 = x.sz3,
                               str4 = x.sz4,
                               str5 = x.sz5,
                               str6 = x.sz6,
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grp);
            return dic;
        }

        public Dictionary<int, string> GetDic_Int_String(List<object> lst, string[] arsz)
        {
            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            string szNoiDung = "";
            foreach (var item in lst)
            {
                int IDDA = (int)GetValueObject(item, "IDDA");

                List<object> grp = (List<object>)GetValueObject(item, arsz[0]);

                string sz = "";
                foreach (var obj in grp)
                {
                    string sz1 = (string)GetValueObject(obj, arsz[1]);

                    sz += "-" + sz1 + ";";
                }
                szNoiDung = sz;
                dicRe.Add(IDDA, szNoiDung);
            }
            return dicRe;
        }

        public Dictionary<int, string> GetDic_Int_String_KKGP_QT(List<object> lst, string[] arsz)
        {
            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            string szNoiDung = "";
            foreach (var item in lst)
            {
                int IDDA = (int)GetValueObject(item, "IDDA");

                List<object> grp = (List<object>)GetValueObject(item, arsz[0]);

                string sz = "";
                foreach (var obj in grp)
                {
                    string sz1 = (string)GetValueObject(obj, arsz[1]);
                    string sz2 = (string)GetValueObject(obj, arsz[2]);
                    sz += "-" + sz1 + "," + sz2 + ";";
                }
                szNoiDung = sz;
                dicRe.Add(IDDA, szNoiDung);
            }
            return dicRe;
        }

        public Dictionary<int, string> GetDic_Int_String_TDC(List<object> lst)
        {
            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            string szNoiDung = "";
            foreach (var item in lst)
            {
                int IDDA = (int)GetValueObject(item, "IDDA");
                string sz = (string)GetValueObject(item, "NoiDung");
                //List<object> grp = (List<object>)GetValueObject(item, arsz[0]);

                //string sz = "";
                //foreach (var obj in grp)
                //{
                //    string sz1 = (string)GetValueObject(obj, arsz[1]);

                //    sz += "-" + sz1 + ";";
                //}
                szNoiDung = sz;
                dicRe.Add(IDDA, szNoiDung);
            }
            return dicRe;
        }
        #endregion

        #region Điều hành
        #region Chủ trương đầu tư
        public List<object> GetList_DH_TDDA_CTDT_CDA(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_CTDT_CDA(idUser, filter);
        }
        #endregion
        #region Chuẩn bị đầu tư
        public List<object> GetList_DH_TDDA_CBDT_CDA(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_CBDT_CDA(idUser, filter);
        }
        #endregion
        #region Chuẩn bị thực hiện
        public List<object> GetList_DH_TDDA_CBTH_CDA(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_CBTH_CDA(idUser, filter);
        }
        #endregion
        #region Thực hiện
        public List<object> GetList_DH_TDDA_TH_CDA(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_TH_CDA(idUser, filter);
        }
        #endregion
        #region Quyết toán
        public List<object> GetList_DH_TDDA_QT_CDA(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_QT_CDA(idUser, filter);
        }
        #endregion
        #region Kế hoạch mới nhất
        public List<object> GetList_DH_TDDA_KHMN(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetList_DH_TDDA_KHMN(idUser);
        }
        #endregion

        #endregion

        #region Kế hoạch mới nhất, trạng thái hoàn thành dưới mức 4
        public List<object> KHMN_DGDPD(string idUser, string filter)
        {
            return _QLTD_TTDA_BCRepository.KHMN_DGDPD(idUser, filter);
        }

        public List<object> KHMN(string idUser)
        {
            return _QLTD_TTDA_BCRepository.KHMN(idUser);
        }
        #endregion

        #region Điều hành - Công việc chưa hoàn thành
        public List<object> GetLst_CV_CHT_CTDT(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CV_CHT_CTDT(idUser);
        }
        public List<object> GetLst_CV_CHT_CBDT(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CV_CHT_CBDT(idUser);

        }

        public List<object> GetLst_CVChuyenQT(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CVChuyenQT(idUser);

        }

        public List<object> GetLst_CV_CHT_CBTH(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CV_CHT_CBTH(idUser);

        }
        public List<object> GetLst_CV_CHT_TH(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CV_CHT_TH(idUser);

        }
        public List<object> GetLst_CV_CHT_QT(string idUser)
        {
            return _QLTD_TTDA_BCRepository.GetLst_CV_CHT_QT(idUser);

        }
        #endregion

        #region Báo cáo tổng hợp QLTD
        #region Cây dự án
        //public IEnumerable<BaoCaoTongHopQLTD> Get_CayDuAn_TongHop_QLTD_CBDT(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var a = _duAnRepository
        //        .GetMulti(x =>
        //        (x.IdGiaiDoanDuAn == 1 &&
        //        x.IdTinhTrangDuAn != null &&
        //        x.IdLinhVucNganhNghe != null &&
        //        arrDuAn.Contains(x.IdDuAn.ToString())), new[] { "LinhVucNganhNghe", "GiaiDoanDuAn", "NhomDuAn" })
        //        .Select(a1 => new
        //        {
        //            IDLVNN = a1.IdLinhVucNganhNghe,
        //            LVNN = a1.LinhVucNganhNghe.TenLinhVucNganhNghe,
        //            OrderLVNN = a1.LinhVucNganhNghe.Order,
        //            IDGDDA = a1.IdGiaiDoanDuAn,
        //            GDDA = a1.GiaiDoanDuAn.TenGiaiDoanDuAn,
        //            IDTinhTrangDuAn = a1.IdTinhTrangDuAn,
        //            TTDA = a1.TinhTrangDuAn,
        //            IDDA = a1.IdDuAn,
        //            IDDACha = a1.IdDuAnCha,
        //            TenDuAn = a1.TenDuAn,
        //            NhomDuAn = a1.NhomDuAn.TenNhomDuAn,
        //            OrderDA = a1.Order,

        //        }).ToList();

        //}
        #endregion
        #endregion
        #region Thông tin cây dự án BC_UB_PL1

        //        var lBaoCao = from item in DbContext.DuAns
        //                      where item.IdGiaiDoanDuAn == 1 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
        //                      && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString())
        //                      select new
        //                      {
        //                          IDDA = item.IdDuAn,
        //                          IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
        //                          LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
        //                          OrderLinhVuc = item.LinhVucNganhNghe.Order,
        //                          IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
        //                          GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
        //                          IDTinhTrangDuAn = item.IdTinhTrangDuAn,
        //                          TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
        //                          TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
        //                          NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
        //                          OrderDuAn = item.Order,
        //                          DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
        //                          NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
        //                          ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
        //                          MaSoDuAn = item.Ma == null ? "" : item.Ma,
        //                          TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu
        //                      };

        //        lstBaoCao = (from item in lBaoCao
        //                     group item by new { item.IDLinhVucNganhNghe, item.LinhVucNganhNghe, item.OrderLinhVuc
        //    }
        //    into item1
        //                         select new
        //                         {
        //                             IDLinhVucNganhNghe = item1.Key.IDLinhVucNganhNghe,
        //                             TenLinhVucNganhNghe = item1.Key.LinhVucNganhNghe,
        //                             OrderLinhVuc = item1.Key.OrderLinhVuc,
        //                             grpGDDT = item1.GroupBy(x => new { x.IDGiaiDoanDuAn, x.GiaiDoanDuAn
        //}).Select(x => new
        //                             {
        //                                 IDGiaiDoanDuAn = x.Key.IDGiaiDoanDuAn,
        //                                 TenGiaiDoanDuAn = x.Key.GiaiDoanDuAn,
        //                                 grpTTDA = item1.Where(xx => xx.IDGiaiDoanDuAn == x.Key.IDGiaiDoanDuAn).GroupBy(y => new { y.IDTinhTrangDuAn, y.TinhTrangDuAn }).Select(y => new
        //                                 {
        //                                     IDTinhTrangDuAn = y.Key.IDTinhTrangDuAn,
        //                                     TinhTrangDuAn = y.Key.TinhTrangDuAn,
        //                                     grpDuAn = item1.Where(yy => yy.IDTinhTrangDuAn == y.Key.IDTinhTrangDuAn).Select(z => new
        //                                     {
        //                                         IDDA = z.IDDA,
        //                                         TenDuAn = z.TenDuAn,
        //                                         NhomDuAn = z.NhomDuAn,
        //                                         DiaDiemXayDung = z.DiaDiemXayDung,
        //                                         NangLucThietKe = z.NangLucThietKe,
        //                                         ThoiGianThucHien = z.ThoiGianThucHien,
        //                                         MaSoDuAn = z.MaSoDuAn,
        //                                         TenChuDauTu = z.TenChuDauTu,
        //                                         OrderDuAn = z.OrderDuAn
        //                                     }).OrderBy(z => z.OrderDuAn).ToList<object>()
        //                                 }).ToList<object>()
        //                             }).ToList<object>()
        //                         }).OrderBy(x => x.OrderLinhVuc).ToList<object>();

        //public IEnumerable<object> GetCayDuAn(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var a = _duAnRepository
        //        .GetMulti(x => (arrDuAn.Contains(x.IdDuAn.ToString())
        //        && x.IdGiaiDoanDuAn == 1
        //        && x.IdTinhTrangDuAn != null
        //        && x.IdLinhVucNganhNghe != null), new[] { "LinhVucNganhNghe", "TinhTrangDuAn",  "ChuDauTu" })
        //        .Select(x => new
        //        {
        //            IDLinhVucNganhNghe = x.IdLinhVucNganhNghe,
        //            TenLinhVucNganhNghe = x.LinhVucNganhNghe.TenLinhVucNganhNghe,
        //            OrderLinhVuc = x.LinhVucNganhNghe.Order,
        //            IDTinhTrangDuAn = x.IdTinhTrangDuAn,
        //            TenTinhTrangDuAn = x.TinhTrangDuAn.TenTinhTrangDuAn,
        //            IDDuAnCha = x.IdDuAnCha,
        //            IDDuAn = x.IdDuAn,
        //            TenDuAn = x.TenDuAn,
        //            OrderDA = x.Order,
        //            ThoiGianThucHien = x.ThoiGianThucHien,
        //            TenChuDauTu = x.ChuDauTu == null ? "" : x.ChuDauTu.TenChuDauTu
        //        }).ToList();

        //    var b = a
        //        .GroupBy(a1 => new { a1.IDLinhVucNganhNghe, a1.TenLinhVucNganhNghe })
        //        .Select(a2 => new
        //        {
        //            IDLVNN = a2.Key.IDLinhVucNganhNghe,
        //            LVNN = a2.Key.TenLinhVucNganhNghe,
        //            grpTTDA = a2.GroupBy(a3 => new { a3.IDTinhTrangDuAn, a3.TenTinhTrangDuAn })
        //            .Select(a3 => new
        //            {
        //                IDTTDA = a3.Key.IDTinhTrangDuAn,
        //                TTDA = a3.Key.TenTinhTrangDuAn,
        //                grpDuAnCha = a3.Where(x => x.IDDuAnCha == null)
        //                .GroupBy(a4 => new { a4.IDDuAn, a4.TenDuAn, a4.ThoiGianThucHien, a4.OrderDA, a4.TenChuDauTu })
        //                .Select(a4 => new
        //                {
        //                    IDDACha = a4.Key.IDDuAn,
        //                    TenDACha = a4.Key.TenDuAn,
        //                    TgianThienCha = a4.Key.ThoiGianThucHien,
        //                    OrderDACha = a4.Key.OrderDA,
        //                    TenChuDauTuCha = a4.Key.TenChuDauTu,
        //                    grpDA = a4.Where(x => x.IDDuAnCha == a4.Key.IDDuAn).Select(a5 => new
        //                    {
        //                        IDDA = a5.IDDuAn,
        //                        TenDA = a5.TenDuAn,
        //                        TGTH = a5.ThoiGianThucHien,
        //                        OrderDA = a5.OrderDA,
        //                        TenCDT = a5.TenChuDauTu
        //                    }).ToList()
        //                }).ToList()
        //            }).ToList()
        //        }).ToList();

        //    return b;
        //}

        //public Dictionary<int, double?> GetKHV(int IDDA, int iNam)
        //{
        //    var a = _keHoachVonRepository.
        //        GetMulti(x => (x.IdKeHoachVonDieuChinh == null && x.NienDo == iNam && x.IdDuAn == IDDA))
        //        .GroupBy(a1 => new { a1.IdDuAn })
        //        .Select(a1 => new
        //        {
        //            IDDA = a1.Key.IdDuAn,
        //            IDKHV = a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.IdKeHoachVon).FirstOrDefault(),
        //            GiaTri = a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
        //        });

        //    var b = _keHoachVonRepository.
        //        GetMulti(x => (x.IdKeHoachVonDieuChinh != null && x.NienDo == iNam && x.IdDuAn == IDDA))
        //        .GroupBy(a1 => new { a1.IdDuAn })
        //        .Select(a1 => new
        //        {
        //            IDDA = a1.Key.IdDuAn,
        //            IDKHVDC = a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.IdKeHoachVonDieuChinh).FirstOrDefault(),
        //            GiaTri = a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : a1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
        //        });

        //    var c = (from item in a
        //             join item1 in b
        //             on item.IDKHV equals item1.IDKHVDC into lst1
        //             from item2 in lst1.DefaultIfEmpty()
        //             select new
        //             {
        //                 IDDA = item.IDDA,
        //                 GiaTri = item2 == null ? item.GiaTri : item2.GiaTri
        //             }).ToDictionary(x => x.IDDA, x => x.GiaTri);

        //    return c;
        //}

        #endregion


        #region Báo cáo Ủy ban phụ lục 1 _ Sửa
        public IEnumerable<BC_Uyban_PL1_PDDA> GetDic_PDDA_CBDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var idskehoach = _qLTD_KeHoachRepository.GetMulti(x => (x.IdGiaiDoan == 3 && x.TrangThai == 4 && arrDuAn.Contains(x.IdDuAn.ToString()))).Select(y => new
            {
                IdKeHoach = y.IdKeHoach,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn }).Select(x => x.OrderByDescending(y => y.IdKeHoach).Select(y => y.IdKeHoach).FirstOrDefault());

            var a12 = idskehoach.ToList();

            var query = _qLTD_CTCV_ChuanBiDauTuRepository
                .GetMulti(x =>
                ((idskehoach.Contains(x.IdKeHoach))
                && (x.HoanThanh == 2)
                && (x.IdCongViec == 27)
                )
                , new[] { "QLTD_KeHoach" })
                .Select(x => new BC_Uyban_PL1_PDDA()
                {
                    IDDA = x.QLTD_KeHoach.IdDuAn
                });
            var a = query.ToList();
            return query;
        }

        public IEnumerable<BC_TienDo_UB_PL1_CayDuAn> Get_BC_UB_PL1_CayDA(string IDDuAns, Dictionary<string, string> lstUserNhanVien)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            
            var qr = _duAnRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.IdLinhVucNganhNghe != null) && (x.IdTinhTrangDuAn != null) && (x.IdGiaiDoanDuAn == 1)),
                new string[] { "LinhVucNganhNghe", "TinhTrangDuAn", "ChuDauTu", "DuAnUsers", "NhomDuAnTheoUser" })
                .Select(x => new
                {
                    IDLVNN = x.IdLinhVucNganhNghe,
                    TenLVNN = x.LinhVucNganhNghe.TenLinhVucNganhNghe,
                    x.DiaDiem,
                    Order = x.LinhVucNganhNghe.Order,
                    IDTTDA = x.IdTinhTrangDuAn,
                    TenTTDA = x.TinhTrangDuAn.TenTinhTrangDuAn,
                    IDDA = x.IdDuAn,
                    IDDACha = x.IdDuAnCha,
                    TenDA = x.TenDuAn,
                    ThoiGianThucHien = x.ThoiGianThucHien,
                    TenChuDauTu = x.ChuDauTu == null ? "" : x.ChuDauTu.TenChuDauTu,
                    TenPhong = x.NhomDuAnTheoUser.TenNhomDuAn,
                    Users = x.DuAnUsers.Where(c => c.UserId != null && lstUserNhanVien.ContainsKey(c.UserId)).Select(u => new
                    {
                        ID = u.UserId,
                        Name = lstUserNhanVien[u.UserId]
                    }).ToList().Distinct(),
                    OrderDA = x.Order
                }).ToList();
                
            var query = qr
                .GroupBy(x => new { x.IDLVNN, x.TenLVNN })
                .Select(x => new BC_TienDo_UB_PL1_CayDuAn()
                {
                    IDLVNN = x.Key.IDLVNN,
                    TenLVNN = x.Key.TenLVNN,
                    Order = x.Select(y => y.Order).FirstOrDefault(),
                    grpTTDA = x.GroupBy(y => new { y.IDTTDA, y.TenTTDA })
                    .Select(y => new BC_TienDo_UB_PL1_TTDA()
                    {
                        IDTTDA = y.Key.IDTTDA,
                        TenTTDA = y.Key.TenTTDA,
                        grpDACha = y.Where(z => z.IDDACha == null).GroupBy(z => new { z.IDDA, z.TenDA, z.DiaDiem })
                        .Select(z => new BC_TienDo_UB_PL1_DACha()
                        {
                            IDDACha = z.Key.IDDA,
                            TenDACha = z.Key.TenDA,
                            DiaDiem = z.Key.DiaDiem,
                            OrderDACha = z.Select(z1 => z1.OrderDA).FirstOrDefault(),
                            ThoiGianThucHienCha = z.Select(z1 => z1.ThoiGianThucHien).FirstOrDefault(),
                            TenChuDauTuCha = z.Select(z1 => z1.TenChuDauTu).FirstOrDefault(),
                            TenPhongCha = z.Select(z1 => z1.TenPhong).FirstOrDefault(),
                            Users = z.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                            grpDACon = z.Where(z1 => z1.IDDACha == z.Key.IDDA).GroupBy(z1 => new { z1.IDDA, z1.TenDA, z1.DiaDiem }).Select(z1 => new BC_TienDo_UB_PL1_DACon()
                            {
                                IDDA = z1.Key.IDDA,
                                TenDA = z1.Key.TenDA,
                                DiaDiem = z1.Key.DiaDiem,
                                OrderDA = z1.Select(z2 => z2.OrderDA).FirstOrDefault(),
                                ThoiGianThucHien = z1.Select(z2 => z2.ThoiGianThucHien).FirstOrDefault(),
                                TenChuDauTu = z1.Select(z2 => z2.TenChuDauTu).FirstOrDefault(),
                                TenPhong = z1.Select(z2 => z2.TenPhong).FirstOrDefault(),
                                Users = z1.Select(s1 => s1.Users).FirstOrDefault().Select(s2 => new Users() { ID = s2.ID, Name = s2.Name }),
                            }).OrderBy(z1 => z1.OrderDA)
                        }).OrderBy(z => z.OrderDACha)
                    })
                }).OrderBy(x => x.Order);

            var a = query.ToList();
            //var b = from q in query
            //        join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
            //        join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
            return query;
        }

        public IEnumerable<BC_TienDo_KHV> Get_KHV_For_BaoCao(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var qrKHV = _keHoachVonRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.NienDo == iNam)
                && (x.IdKeHoachVonDieuChinh == null)))
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHV = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVon).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault(),
                }).ToList();

            var qrKHVDC = _keHoachVonRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.NienDo == iNam)
                && (x.IdKeHoachVonDieuChinh != null)))
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHVDC = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVonDieuChinh).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault()
                }).ToList();

            var query = from item in qrKHV
                        join item1 in qrKHVDC
                        on item.IDKHV equals item1.IDKHVDC into lst
                        from item2 in lst.DefaultIfEmpty()
                        select new BC_TienDo_KHV()
                        {
                            IDDA = item.IDDA,
                            dGiaTri = item2 == null ? item.GT : item2.GT
                        };

            return query;
        }
        public IEnumerable<BC_TienDo_KHV> Get_KHV_For_BaoCaoAll(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var qrKHV = _keHoachVonRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.NienDo == iNam)
                && (x.IdKeHoachVonDieuChinh == null)))
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHV = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVon).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault(),
                }).ToList();

            var qrKHVDC = _keHoachVonRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.NienDo == iNam)
                && (x.IdKeHoachVonDieuChinh != null)))
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHVDC = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVonDieuChinh).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault()
                }).ToList();

            var query = from item in qrKHV
                        join item1 in qrKHVDC
                        on item.IDKHV equals item1.IDKHVDC into lst
                        from item2 in lst.DefaultIfEmpty()
                        select new BC_TienDo_KHV()
                        {
                            IDDA = item.IDDA,
                            dGiaTri = item2 == null ? item.GT : item2.GT
                        };

            return query;
        }
        public IEnumerable<BC_TienDo_KHV_New> Get_KHV_For_BaoCao_KeHoachGN(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var originList = _keHoachVonRepository.GetAll(new string[] { "NguonVonKeHoachVons", "NguonVonKeHoachVons.NguonVonDuAn", "NguonVonKeHoachVons.NguonVonDuAn.NguonVon" }).Select(s => new
            {
                IdDuAn = s.IdDuAn,
                NienDo = s.NienDo,
                IdKeHoachVonDieuChinh = s.IdKeHoachVonDieuChinh,
                NguonVonKeHoachVons = s.NguonVonKeHoachVons,
                NgayPheDuyet = s.NgayPheDuyet,
                IdKeHoachVon = s.IdKeHoachVon,
                TongGiaTri = s.TongGiaTri
            });
            var qrKHV = originList.Where(x =>
                            (arrDuAn.Contains(x.IdDuAn.ToString()))
                            && (x.NienDo == iNam)
                            && (x.IdKeHoachVonDieuChinh == null)
                            && (x.NguonVonKeHoachVons.Count() > 0)
            )
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHV = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVon).FirstOrDefault(),
                    NguonVonHuyen = x.Select(y => y.NguonVonKeHoachVons.Where(z => z.NguonVonDuAn.NguonVon.IdNguonVon == 8).Select(s => s.GiaTri).Sum()).FirstOrDefault(),
                    NguonVonTP = x.Select(y => y.NguonVonKeHoachVons.Where(z => z.NguonVonDuAn.NguonVon.IdNguonVon == 1012).Select(s => s.GiaTri).Sum()).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault(),
                });

            var qrKHVDC = originList.Where(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.NienDo == iNam)
                && (x.IdKeHoachVonDieuChinh != null))
                && (x.NguonVonKeHoachVons.Count() > 0))
                .GroupBy(x => new { x.IdDuAn })
                .Select(x => new
                {
                    IDDA = x.Key.IdDuAn,
                    IDKHVDC = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => y.IdKeHoachVonDieuChinh).FirstOrDefault(),
                    NguonVonHuyen = x.Select(y => y.NguonVonKeHoachVons.Where(z => z.NguonVonDuAn.NguonVon.IdNguonVon == 8).Select(s => s.GiaTri).ToList().Sum()).FirstOrDefault(),
                    NguonVonTP = x.Select(y => y.NguonVonKeHoachVons.Where(z => z.NguonVonDuAn.NguonVon.IdNguonVon == 1012).Select(s => s.GiaTri).ToList().Sum()).FirstOrDefault(),
                    GT = x.OrderByDescending(y => y.NgayPheDuyet).Select(y => (double?)y.TongGiaTri).FirstOrDefault()
                });

            var query = from item in qrKHV
                        join item1 in qrKHVDC
                        on item.IDKHV equals item1.IDKHVDC into lst
                        from item2 in lst.DefaultIfEmpty()
                        select new BC_TienDo_KHV_New()
                        {
                            IDDA = item.IDDA,
                            dGiaTriHuyen = item2 == null ? item.NguonVonHuyen : item2.NguonVonHuyen,
                            dGiaTriTP = item2 == null ? item.NguonVonTP : item2.NguonVonTP,
                            dGiaTri = item2 == null ? item.GT : item2.GT
                        };
            return query;
        }

    
        public IEnumerable<BC_TienDo_GN> Get_GN_For_BaoCao_PL1(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var query = _vonCapNhatKLNTRepository
                .GetMulti(x => ((arrDuAn.Contains(x.MaDuAn)) && (x.CapNhatKLNT.CreatedDate.Value.Year == iNam))
                , new string[] { "CapNhatKLNT" })
                .GroupBy(x => x.IdDuAn)
                .Select(x => new BC_TienDo_GN()
                {
                    IDDA = x.OrderByDescending(y => y.IdFile).Select(y => y.IdDuAn.ToString()).FirstOrDefault(),
                    dGiaTriGN = x.OrderByDescending(y => y.IdFile).Select(y => y.TDGNVonHuyen) == null ? "0" : x.OrderByDescending(y => y.IdFile).Select(y => y.TDGNVonHuyen).FirstOrDefault()
                });
            return query;
        }

        public IEnumerable<BC_ListID> GetListIDDA_For_BC_TienDo(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var qr = _duAnRepository
                .GetMulti(x =>
                ((arrDuAn.Contains(x.IdDuAn.ToString()))
                && (x.IdLinhVucNganhNghe != null)
                && (x.IdTinhTrangDuAn != null)
                && (x.IdGiaiDoanDuAn == 1))
                , new string[] { "LinhVucNganhNghe", "TinhTrangDuAn", "ChuDauTu", "DuAnUsers", "NhomDuAnTheoUser" })
                .Select(x => new BC_ListID()
                {
                   IDDA = x.IdDuAn
                });

            var a = qr.ToList();

            return qr;
        }

        
        #endregion
    }
}

