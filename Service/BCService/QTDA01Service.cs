﻿using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BCService
{
    public interface IQTDA01Service
    {
        List<object> GetNguonVonDauTu(int idDuAn);
        List<object> GetChiPhiDauTuFromTTCPK(int idDuAn);
        List<object> GetChiPhiDauTuFromQTHD(int idDuAn);
        object GetQuyetToanByDuAn(int idDuAn);
    }

    public class QTDA01Service:IQTDA01Service
    {
        private IKeHoachVonRepository _kehoachVonRepository;
        private INguonVonRepository _nguonVonRepository;
        private INguonVonDuAnGoiThauRepository _nguonvonDuAnGoiThauRepository;
        private INguonVonKeHoachVonRepository _nguonVonKeHoachVonRepository;
        private INguonVonDuAnRepository _nguonvonDuAnRepository;
        public INguonVonThanhToanChiPhiKhacRepository _nguonVonThanhToanCPKRepository;
        public IThanhToanChiPhiKhacRepository _thanhToanCPKRepository;
        private IGoiThauRepository _goiThauRepository;
        private IHopDongRepository _hopDongRepository;
        private INguonVonGoiThauThanhToanRepository _nguonvonGoiThauThanhToanRepository;
        private IThanhToanHopDongRepository _thanhToanHopDongRepository;
        private ILapQuanLyDauTuRepository _lapQuanLyDauTuRepository;
        public QTDA01Service(INguonVonDuAnRepository nguonvonDuAnRepository, INguonVonKeHoachVonRepository nguonVonKeHoachVonRepository, INguonVonThanhToanChiPhiKhacRepository nguonvonThanhToanCPKRepository, INguonVonDuAnGoiThauRepository nguonvonDuAnGoiThauRepository, INguonVonRepository nguonVonRepository,
            IThanhToanChiPhiKhacRepository thanhToanCPKRepository, IGoiThauRepository goiThauRepository, IHopDongRepository hopDongRepository, INguonVonGoiThauThanhToanRepository nguonvonGoiThauThanhToanRepository, IThanhToanHopDongRepository thanhToanHopDongRepository, ILapQuanLyDauTuRepository lapQuanLyDauTuRepository, IKeHoachVonRepository kehoachVonRepository)
        {
            _nguonvonDuAnRepository = nguonvonDuAnRepository;
            _nguonVonKeHoachVonRepository = nguonVonKeHoachVonRepository;
            _nguonVonThanhToanCPKRepository = nguonvonThanhToanCPKRepository;
            _nguonvonDuAnGoiThauRepository = nguonvonDuAnGoiThauRepository;
            _thanhToanCPKRepository = thanhToanCPKRepository;
            _goiThauRepository = goiThauRepository;
            _hopDongRepository = hopDongRepository;
            _nguonvonGoiThauThanhToanRepository = nguonvonGoiThauThanhToanRepository;
            _thanhToanHopDongRepository = thanhToanHopDongRepository;
            _nguonVonRepository = nguonVonRepository;
            _lapQuanLyDauTuRepository=lapQuanLyDauTuRepository;
            _kehoachVonRepository = kehoachVonRepository;
        }
        public List<object> GetNguonVonDauTu(int idDuAn)
        {
            string[] includes = new string[] { "NguonVonGoiThauThanhToans" };
            string[] includes1 = new string[] { "NguonVonKeHoachVons" };
            var q5 = _kehoachVonRepository.GetAll().Where(x => x.IdKeHoachVonDieuChinh != null && x.IdDuAn==idDuAn).GroupBy(x => x.IdKeHoachVonDieuChinh).Select(x => new
            {
                IdKeHoachVonDieuChinh = x.Key,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).FirstOrDefault().IdKeHoachVon,
            }).ToList();
            
            var q6 = from khv1 in _kehoachVonRepository.GetMulti(x=>x.IdDuAn==idDuAn&&x.IdKeHoachVonDieuChinh==null)
                     where!(from x in q5 select x.IdKeHoachVonDieuChinh).Contains(khv1.IdKeHoachVon)
                     select new
                     {
                         IdKeHoachVonDieuChinh = khv1.IdKeHoachVonDieuChinh,
                         IdKeHoachVon = khv1.IdKeHoachVon,
                     };
           
            var q56 = q5.Union(q6).ToList();
            var nguonVonKHVs= _nguonVonKeHoachVonRepository.GetAll().Where(nvkh => q56.Select(x => x.IdKeHoachVon).Contains(nvkh.IdKeHoachVon)).ToList();
            
          foreach(var item in nguonVonKHVs)
            {
                var a = item.IdKeHoachVon;
            }
            var nguonVonDauTus = (from nvda in _nguonvonDuAnRepository.GetAll().Where(x => x.IdDuAn == idDuAn && x.IdNguonVon != null)
                       
                        select new
                        {
                            nvda.IdNguonVon,
                            GiaTri = nvda.GiaTri ?? 0,
                            NguonVonKeHoach = nguonVonKHVs.Where(nvkhv=>nvkhv.IdNguonVonDuAn==nvda.IdNguonVonDuAn).Select(nvkhv=>nvkhv.GiaTri).Sum(),
                            NguonVonThanhToanCPK = _nguonVonThanhToanCPKRepository.GetMulti(nvttcpk => nvttcpk.IdNguonVonDuAn == nvda.IdNguonVonDuAn).Select(nvttcpk => nvttcpk.GiaTri).Sum(),
                            NguonVonTTHD = _nguonvonDuAnGoiThauRepository.GetMulti(nvdagt => nvdagt.IdNguonVonDuAn == nvda.IdNguonVonDuAn, includes).Select(nvdagt => nvdagt.NguonVonGoiThauThanhToans.Select(nvgtt => nvgtt.GiaTri).Sum()).Sum(),
                        }).GroupBy(x => x.IdNguonVon).Join(_nguonVonRepository.GetAll(), a => a.Key, b => b.IdNguonVon, (a, b) => new
                        {
                            TenNguonVon = b.TenNguonVon,
                            GiaTriNV = a.ToList().Select(a1 => a1.GiaTri).Sum(),
                            GiaTriKH = a.ToList().Select(a1 => a1.NguonVonKeHoach).Sum(),
                            GiaTriTT = a.ToList().Select(a1 => a1.NguonVonThanhToanCPK).Sum() + a.ToList().Select(a1 => a1.NguonVonTTHD).Sum(),
                        }).ToList<object>();

           

            return nguonVonDauTus;
        }
        public List<object> GetChiPhiDauTuFromTTCPK(int idDuAn)
        {
            string[] includes =new string[] {"NguonVonThanhToanChiPhiKhacs"};
            
            var chiPhiDauTuFromTTCPK= _thanhToanCPKRepository.GetMulti(x => x.IdDuAn == idDuAn&&x.LoaiThanhToan!=null, includes).GroupBy(x => x.LoaiThanhToan).Select(x => new
            {
                LoaiThanhToan = x.Key,
                TongGiaTri = x.ToList().Select(ttcpk => ttcpk.NguonVonThanhToanChiPhiKhacs.Select(nvttcpk => nvttcpk.GiaTri).Sum()).Sum(),
            }).ToList<object>();
            
            return chiPhiDauTuFromTTCPK;
        }
        public List<object> GetChiPhiDauTuFromQTHD(int idDuAn)
        {
            string[] includes = new string[] { "QuyetToanHopDongs" };
          
            var chiPhiDauTuFromTTHD= _goiThauRepository.GetMulti(x => x.IdDuAn == idDuAn&&x.LoaiGoiThau!=null).GroupBy(x => x.LoaiGoiThau).Select(x => new
            {
                LoaiGoiThau =x.Key,
                TongGiaTri=  _hopDongRepository.GetAll(includes).Where(hd=>x.ToList().Select(goithau=> goithau.IdGoiThau).Contains(hd.IdGoiThau)).Select(hopdong=>hopdong.QuyetToanHopDongs.Where(qthd=>qthd.GiaTriQuyetToan!=null).Select(qthd=>qthd.GiaTriQuyetToan).Sum()).Sum(),
                
            }).ToList<object>();
            return chiPhiDauTuFromTTHD;
        }
        public object GetQuyetToanByDuAn(int idDuAn)
        {
            var query = from lapduandautu in _lapQuanLyDauTuRepository.GetMulti(x => x.IdDuAn == idDuAn)
                        group lapduandautu by lapduandautu.IdDuAn
                        into grp
                        let lapduandautuTheoDuAnLoai1 = grp.ToList().Where(x => x.LoaiQuanLyDauTu == 1)
                        let lapduandautuTheoDuAnLoai2 = grp.ToList().Where(x => x.LoaiQuanLyDauTu == 2)
                        let lapDuAnDauTuDieuChinh = lapduandautuTheoDuAnLoai1.Where(x => x.LoaiDieuChinh == 2).OrderByDescending(x => x.NgayPheDuyet)
                        let lapDuToanDieuChinh = lapduandautuTheoDuAnLoai2.Where(x => x.LoaiDieuChinh == 2).OrderByDescending(x => x.NgayPheDuyet)
                        let flag1 = lapduandautuTheoDuAnLoai1.Count()>0 
                       
                        let flag3=lapDuAnDauTuDieuChinh.Count()>0
                        let flag4 = lapDuToanDieuChinh.Count() > 0
                        select new
                        {
                           
                           XayLap =(flag1==true)?((flag3==true)?lapDuAnDauTuDieuChinh.First().XayLap:
                           lapduandautuTheoDuAnLoai1.Select(x=>x.XayLap).Sum()): ((flag4 == true) ? lapDuToanDieuChinh.First().XayLap :
                           lapduandautuTheoDuAnLoai2.Select(x => x.XayLap).Sum()),
                           ThietBi= (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().ThietBi :
                           lapduandautuTheoDuAnLoai1.Select(x => x.ThietBi).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().ThietBi :
                           lapduandautuTheoDuAnLoai2.Select(x => x.ThietBi).Sum()),
                           QuanLyDuAn = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().QuanLyDuAn :
                           lapduandautuTheoDuAnLoai1.Select(x => x.QuanLyDuAn).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().QuanLyDuAn :
                           lapduandautuTheoDuAnLoai2.Select(x => x.QuanLyDuAn).Sum()),
                           TuVan = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().TuVan :
                           lapduandautuTheoDuAnLoai1.Select(x => x.TuVan).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().TuVan :
                           lapduandautuTheoDuAnLoai2.Select(x => x.TuVan).Sum()),
                           GiaiPhongMatBang = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().GiaiPhongMatBang :
                           lapduandautuTheoDuAnLoai1.Select(x => x.GiaiPhongMatBang).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().GiaiPhongMatBang :
                           lapduandautuTheoDuAnLoai2.Select(x => x.GiaiPhongMatBang).Sum()),
                           Khac = (flag1 == true) ? ((flag3 == true) ? lapDuAnDauTuDieuChinh.First().Khac:
                           lapduandautuTheoDuAnLoai1.Select(x => x.Khac).Sum()) : ((flag4 == true) ? lapDuToanDieuChinh.First().Khac :
                           lapduandautuTheoDuAnLoai2.Select(x => x.Khac).Sum()),
                          
                        };
            return new
            {
                XayLap = query.Select(x => x.XayLap).Sum(),
                ThietBi = query.Select(x => x.ThietBi).Sum(),
                QuanLyDuAn = query.Select(x => x.QuanLyDuAn).Sum(),
                TuVan = query.Select(x => x.TuVan).Sum(),
                GiaiPhongMatBang = query.Select(x => x.GiaiPhongMatBang).Sum(),
                Khac = query.Select(x => x.Khac).Sum(),
            };

        }
    }
}
