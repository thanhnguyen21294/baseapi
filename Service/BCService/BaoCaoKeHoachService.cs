﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.BCRepository;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BCService
{
    public interface IBaoCaoKeHoachService
    {
        #region VonDTXDCBNam
        List<object> GetBCVonDauTuXDCBNam(string IDDuAns);

        Dictionary<int, List<object>> GetDuAnCon(string IDDuAns);

        Dictionary<int, string> GetTMDTLDADTSoQD();
        Dictionary<int, string> GetTMDTLTDTSoQD();

        Dictionary<int, double> GetTMDTLDADTTongGiaTri();
        Dictionary<int, double> GetTMDTLTDTTongGiaTri();
        
        Dictionary<int, List<object>> GetDicKhoiLuongThucHien(int iNam);

        //List<object> GetKeHoachVonNamDieuChinhXDCB(int iNam);

        //List<object> GetKeHoachVonNamXDCB(int iNam);

        Dictionary<int, List<object>> GetDicKHVN_1XDCB(int iNam);

        Dictionary<int, double> GetDicGNN_2(int iNam);

        Dictionary<int, List<object>> GetDicGNN_1XDCB(int iNam);
        //List<object> GetThanhToanChiPhiKhacN_1(int iNam, string szDonViTinh);

        //List<object> GetThanhToanHopDongN_1(int iNam, string szDonViTinh);

        Dictionary<int, List<object>> GetDicKHVN_XDCB(int iNam);

        #endregion

        #region Vốn TPCP
        List<object> GetThongTinDuAnCha(string IDDuAns);
        List<object> GetThongTinDuAnCon(string IDDuAns);

        List<object> GetKHVNNSNN(int iNam);
        List<object> GetKHVNDCNSNN(int iNam);

        List<object> GetLuyKeKeHoachVonNSNN(int iNam);
        List<object> GetLuyKeKeHoachVonDieuChinhNSNN(int iNam);

        Dictionary<int, List<object>> GetDicKLTH(int iNam);

        List<object> GetUocThanhToanChiPhiKhacs(int iNam);
        List<object> GetUocThanhToanHopDong(int iNam);

        Dictionary<int, double> GetDicKHGN(int iNam);

        #endregion

        #region TinhHinhThucHienODAVonXDCB
        List<object> GetDuAnChaODA(string IDDuAns);
        List<object> GetDuAnConODA(string IDDuAns);

        Dictionary<int, string> GetTMDTLDADTSoQDBanDau();
        Dictionary<int, string> GetTMDTLTDTSoQDBanDau();

        Dictionary<int, double> GetTMDTLDADTTongGiaTriBanDau();
        Dictionary<int, double> GetTMDTLTDTTongGiaTriBanDau();

        Dictionary<int, string> GetTMDTLDADTSoQDSuaDoi();
        Dictionary<int, string> GetTMDTLTDTSoQDSuaDoi();

        Dictionary<int, double> GetTMDTLDADTTongGiaTriSuaDoi();
        Dictionary<int, double> GetTMDTLTDTTongGiaTriSuaDoi();

        List<object> GetLuyKeVonThanhToanCPKs(int iNam);
        List<object> GetLuyKeVonThanhToanHD(int iNam);

        List<object> GetDicKHVN_ODA(int iNam);
        List<object> GetDicKHVNDC_ODA(int iNam);

        List<object> GetUocThanhToanCPKs(int iNam);
        List<object> GetUocThanhToanHD(int iNam);

        List<object> GetThanhToanCPKsNam(int iNam);
        List<object> GetThanhToanHDNam(int iNam);
        #endregion
    }

    public class BaoCaoKeHoachService : IBaoCaoKeHoachService
    {
        private IUnitOfWork _unitOfWork;
        private IKeHoachVonDTXDCBNamRepository _KeHoachVonDTXDCBNamRepository;
        private IKeHoachVonDauTuNSNNRepository _KeHoachVonDauTuNSNNRepository;
        private ITinhHinhThucHienODAVonXDCBRepository _TinhHinhThucHienODAVonXDCBRepository;

        public BaoCaoKeHoachService(IUnitOfWork unitOfWork, IKeHoachVonDTXDCBNamRepository _KeHoachVonDTXDCBNamRepository, IKeHoachVonDauTuNSNNRepository _KeHoachVonDauTuNSNNRepository, ITinhHinhThucHienODAVonXDCBRepository _TinhHinhThucHienODAVonXDCBRepository)
        {
            this._unitOfWork = unitOfWork;
            this._KeHoachVonDTXDCBNamRepository = _KeHoachVonDTXDCBNamRepository;
            this._KeHoachVonDauTuNSNNRepository = _KeHoachVonDauTuNSNNRepository;
            this._TinhHinhThucHienODAVonXDCBRepository = _TinhHinhThucHienODAVonXDCBRepository;
        }

        #region Vốn ĐTXDCB Năm
        public List<object> GetBCVonDauTuXDCBNam(string IDDuAns)
        {
            return _KeHoachVonDTXDCBNamRepository.GetBCVonDauTuXDCBNam(IDDuAns);
        }

        public Dictionary<int, List<object>> GetDuAnCon(string IDDuAns)
        {
            List<object> lstDuAnCon = _KeHoachVonDTXDCBNamRepository.GetDuAnCon(IDDuAns);

            Dictionary<int, List<object>> dicDuAnCon = new Dictionary<int, List<object>>();

            dicDuAnCon = (from item in
                             (from item in lstDuAnCon
                              select new
                              {
                                  IDDA = (int)GetValueObject(item, "IDDA"),
                                  IDDACha = (int)GetValueObject(item, "IDDACha"),
                                  TenDuAn = GetValueObject(item, "TenDuAn"),
                                  DiaDiemXayDung = GetValueObject(item, "DiaDiemXayDung"),
                                  NangLucThietKe = GetValueObject(item, "NangLucThietKe"),
                                  ThoiGianKhoiCongHoanThanh = GetValueObject(item, "ThoiGianKhoiCongHoanThanh")
                              })
                          group item by new { item.IDDACha }
                         into item1
                          select new
                          {
                              IDDACha = item1.Key.IDDACha,
                              grpDuAnCon = item1.Select(x => new
                              {
                                  IDDA = x.IDDA,
                                  TenDuAn = x.TenDuAn,
                                  DiaDiemXayDung = x.DiaDiemXayDung,
                                  NangLucThietKe = x.NangLucThietKe,
                                  ThoiGianKhoiCongHoanThanh = x.ThoiGianKhoiCongHoanThanh
                              }).ToList<object>()
                          }).ToDictionary(x => x.IDDACha, x => x.grpDuAnCon);

            return dicDuAnCon;
        }

        public Dictionary<int, string> GetTMDTLDADTSoQD()
        {
            return _KeHoachVonDTXDCBNamRepository.GetTMDTLDADTSoQD();
        }

        public Dictionary<int, string> GetTMDTLTDTSoQD()
        {
            return _KeHoachVonDTXDCBNamRepository.GetTMDTLTDTSoQD();
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTri()
        {
            return _KeHoachVonDTXDCBNamRepository.GetTMDTLDADTTongGiaTri();
        }

        public Dictionary<int, double> GetTMDTLTDTTongGiaTri()
        {
            return _KeHoachVonDTXDCBNamRepository.GetTMDTLTDTTongGiaTri();
        }
        
        public Dictionary<int, List<object>> GetDicKhoiLuongThucHien(int iNam)
        {
            List<object> lstKhoiLuongThucHien = _KeHoachVonDTXDCBNamRepository.GetLstKhoiLuongThucHien(iNam);

            Dictionary<int, List<object>> DicKhoiLuongThucHien = new Dictionary<int, List<object>>();

            DicKhoiLuongThucHien = (from item in
                                        (from item in lstKhoiLuongThucHien
                                         select new
                                         {
                                             IDDA = (int)GetValueObject(item, "IDDA"),

                                             KLTHDenNgay = GetValueObject(item, "KLTHDenNgay"),

                                             KLTHCaNam = GetValueObject(item, "KLTHCaNam")
                                         })
                                    group item by new { item.IDDA }
                                    into item1
                                    select new
                                    {
                                        IDDA = item1.Key.IDDA,
                                        grpKhoiLuong = item1.Select(x => new
                                        {
                                            KLTHDenNgay = x.KLTHDenNgay,

                                            KLTHCaNam = x.KLTHCaNam
                                        }).ToList<object>()
                                    }).ToDictionary(x => x.IDDA, x => x.grpKhoiLuong);

            return DicKhoiLuongThucHien;
        }
        
        public Dictionary<int, List<object>> GetDicKHVN_1XDCB(int iNam)
        {
            int iNam_1 = iNam - 1;

            List<object> lstKHVN_1BD = _KeHoachVonDTXDCBNamRepository.GetKeHoachVonNamXDCB(iNam_1);

            List<object> lstKHVN_1DC = _KeHoachVonDTXDCBNamRepository.GetKeHoachVonNamDieuChinh(iNam_1);

            Dictionary<int, List<object>> dicKHVN_1 = new Dictionary<int, List<object>>();

            dicKHVN_1 = (from item in
                            (from item in lstKHVN_1BD
                             join item1 in lstKHVN_1DC on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                             from item2 in lstDCKHV.DefaultIfEmpty()
                             select new
                             {
                                 IDDA = (int)GetValueObject(item, "IDDA"),

                                 KeHoachVonNam_NSTP = item2 == null ? GetValueObject(item, "KeHoachVonNam_NSTP") : GetValueObject(item2, "KeHoachVonNam_NSTP"),

                                 KeHoachVonNam_SDDDD = item2 == null ? GetValueObject(item, "KeHoachVonNam_SDDDD") : GetValueObject(item2, "KeHoachVonNam_SDDDD"),

                                 KeHoachVonNam_NSTT = item2 == null ? GetValueObject(item, "KeHoachVonNam_NSTT") : GetValueObject(item2, "KeHoachVonNam_NSTT"),

                                 KeHoachVonNam_NST = item2 == null ? GetValueObject(item, "KeHoachVonNam_NST") : GetValueObject(item2, "KeHoachVonNam_NST"),

                                 KeHoachVonNam_ODA = item2 == null ? GetValueObject(item, "KeHoachVonNam_ODA") : GetValueObject(item2, "KeHoachVonNam_ODA"),

                                 KeHoachVonNam_TPCP = item2 == null ? GetValueObject(item, "KeHoachVonNam_TPCP") : GetValueObject(item2, "KeHoachVonNam_TPCP"),
                             })
                         group item by new { item.IDDA }
                         into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,

                             grpGiaTri = item1.Select(x => new
                             {
                                 KeHoachVonNam_NSTP = x.KeHoachVonNam_NSTP,

                                 KeHoachVonNam_SDDDD = x.KeHoachVonNam_SDDDD,

                                 KeHoachVonNam_NSTT = x.KeHoachVonNam_NSTT,

                                 KeHoachVonNam_NST = x.KeHoachVonNam_NST,

                                 KeHoachVonNam_ODA = x.KeHoachVonNam_ODA,

                                 KeHoachVonNam_TPCP = x.KeHoachVonNam_TPCP
                             }).ToList<object>()
                         }).ToDictionary(x => x.IDDA, x => x.grpGiaTri);

            return dicKHVN_1;
        }

        public Dictionary<int, double> GetDicGNN_2(int iNam)
        {
            List<object> lstThanhToanCPK = _KeHoachVonDTXDCBNamRepository.GetThanhToanChiPhiKhacN_2(iNam);
            List<object> lstThanhToanHD = _KeHoachVonDTXDCBNamRepository.GetThanhToanHopDongN_2(iNam);

            Dictionary<int, double> dicGNN_2 = new Dictionary<int, double>();

            dicGNN_2 = (from item in lstThanhToanCPK
                        join item1 in lstThanhToanHD
                        on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                        select new
                        {
                            IDDA = (int)GetValueObject(item, "IDDA"),

                            GiaTri = (double)GetValueObject(item, "GiaTriCPKN_2") + (double)GetValueObject(item1, "GiaTriTTHDN_2")
                        }).ToDictionary(x => x.IDDA, x => x.GiaTri);

            return dicGNN_2;
        }

        public Dictionary<int, List<object>> GetDicGNN_1XDCB(int iNam)
        {
            List<object> lstThanhToanCPK = _KeHoachVonDTXDCBNamRepository.GetThanhToanChiPhiKhacN_1(iNam);
            List<object> lstThanhToanHD = _KeHoachVonDTXDCBNamRepository.GetThanhToanHopDongN_1(iNam);

            Dictionary<int, List<object>> dicGNN_1 = new Dictionary<int, List<object>>();

            dicGNN_1 = (from item in
                           (from item in lstThanhToanCPK
                            join item1 in lstThanhToanHD
                            on GetValueObject(item, "IDDA") equals GetValueObject(item1, "IDDA")
                            select new
                            {
                                IDDA = (int)GetValueObject(item, "IDDA"),

                                GiaTriNguonSDD = (double)GetValueObject(item, "GiaTriNguonSDD") + (double)GetValueObject(item1, "GiaTriNguonSDD"),

                                GiaTriNguonSDDDD = (double)GetValueObject(item, "GiaTriNguonSDDDD") + (double)GetValueObject(item1, "GiaTriNguonSDDDD"),

                                GiaTriNguonTinh = (double)GetValueObject(item, "GiaTriNguonTinh") + (double)GetValueObject(item1, "GiaTriNguonTinh"),

                                GiaTriNguonTT = (double)GetValueObject(item, "GiaTriNguonTT") + (double)GetValueObject(item1, "GiaTriNguonTT"),

                                GiaTriNguonODA = (double)GetValueObject(item, "GiaTriNguonODA") + (double)GetValueObject(item1, "GiaTriNguonODA"),

                                GiaTriNguonTPCP = (double)GetValueObject(item, "GiaTriNguonTPCP") + (double)GetValueObject(item1, "GiaTriNguonTPCP"),
                            })
                        group item by new { item.IDDA }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IDDA,
                            grpGNN_1 = item1.Select(x => new
                            {
                                GiaTriNguonSDD = x.GiaTriNguonSDD,
                                GiaTriNguonSDDDD = x.GiaTriNguonSDDDD,
                                GiaTriNguonTinh = x.GiaTriNguonTinh,
                                GiaTriNguonTT = x.GiaTriNguonTT,
                                GiaTriNguonODA = x.GiaTriNguonODA,
                                GiaTriNguonTPCP = x.GiaTriNguonTPCP
                            }).ToList<object>()
                        }).ToDictionary(x => x.IDDA, x => x.grpGNN_1);

            return dicGNN_1;

        }

        public Dictionary<int, List<object>> GetDicKHVN_XDCB(int iNam)
        {
            List<object> lstKHVNBD = _KeHoachVonDTXDCBNamRepository.GetKeHoachVonNamXDCB(iNam);

            List<object> lstKHVNDC = _KeHoachVonDTXDCBNamRepository.GetKeHoachVonNamDieuChinh(iNam);

            Dictionary<int, List<object>> dicKHVN = new Dictionary<int, List<object>>();

            dicKHVN = (from item in
                            (from item in lstKHVNBD
                             join item1 in lstKHVNDC on GetValueObject(item, "IDKHV") equals GetValueObject(item1, "IDKHVDC") into lstDCKHV
                             from item2 in lstDCKHV.DefaultIfEmpty()
                             select new
                             {
                                 IDDA = (int)GetValueObject(item, "IDDA"),

                                 KeHoachVonNam_NSTP = item2 == null ? GetValueObject(item, "KeHoachVonNam_NSTP") : GetValueObject(item2, "KeHoachVonNam_NSTP"),

                                 KeHoachVonNam_SDDDD = item2 == null ? GetValueObject(item, "KeHoachVonNam_SDDDD") : GetValueObject(item2, "KeHoachVonNam_SDDDD"),

                                 KeHoachVonNam_NSTT = item2 == null ? GetValueObject(item, "KeHoachVonNam_NSTT") : GetValueObject(item2, "KeHoachVonNam_NSTT"),

                                 KeHoachVonNam_NST = item2 == null ? GetValueObject(item, "KeHoachVonNam_NST") : GetValueObject(item2, "KeHoachVonNam_NST"),

                                 KeHoachVonNam_ODA = item2 == null ? GetValueObject(item, "KeHoachVonNam_ODA") : GetValueObject(item2, "KeHoachVonNam_ODA"),

                                 KeHoachVonNam_TPCP = item2 == null ? GetValueObject(item, "KeHoachVonNam_TPCP") : GetValueObject(item2, "KeHoachVonNam_TPCP"),
                             })
                         group item by new { item.IDDA }
                         into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,

                             grpGiaTri = item1.Select(x => new
                             {
                                 KeHoachVonNam_NSTP = x.KeHoachVonNam_NSTP,

                                 KeHoachVonNam_SDDDD = x.KeHoachVonNam_SDDDD,

                                 KeHoachVonNam_NSTT = x.KeHoachVonNam_NSTT,

                                 KeHoachVonNam_NST = x.KeHoachVonNam_NST,

                                 KeHoachVonNam_ODA = x.KeHoachVonNam_ODA,

                                 KeHoachVonNam_TPCP = x.KeHoachVonNam_TPCP
                             }).ToList<object>()
                         }).ToDictionary(x => x.IDDA, x => x.grpGiaTri);

            return dicKHVN;
        }
        #endregion

        #region Vốn TPCP
        public List<object> GetThongTinDuAnCha(string IDDuAns)
        {
            return _KeHoachVonDauTuNSNNRepository.GetThongTinDuAnCha(IDDuAns);
        }
        public List<object> GetThongTinDuAnCon(string IDDuAns)
        {
            return _KeHoachVonDauTuNSNNRepository.GetThongTinDuAnCon(IDDuAns);
        }


        public List<object> GetKHVNNSNN(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetKHVNNSNN(iNam);
        }
        public List<object> GetKHVNDCNSNN(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetKHVNDCNSNN(iNam);
        }

        public List<object> GetLuyKeKeHoachVonNSNN(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetLuyKeKeHoachVonNSNN(iNam);
        }
        public List<object> GetLuyKeKeHoachVonDieuChinhNSNN(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetLuyKeKeHoachVonDieuChinhNSNN(iNam);
        }

        public Dictionary<int, List<object>> GetDicKLTH(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetDicKLTH(iNam);
        }
        public List<object> GetUocThanhToanChiPhiKhacs(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetUocThanhToanChiPhiKhacs(iNam);
        }
        public List<object> GetUocThanhToanHopDong(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetUocThanhToanHopDong(iNam);
        }


        public Dictionary<int, double> GetDicKHGN(int iNam)
        {
            return _KeHoachVonDauTuNSNNRepository.GetDicKHGN(iNam);
        }

        #endregion

        #region TinhHinhThucHienODAVonXDCB
        public List<object> GetDuAnChaODA(string IDDuAns)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetDuAnCha(IDDuAns);
        }
        public List<object> GetDuAnConODA(string IDDuAns)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetDuAnCon(IDDuAns);
        }

        public Dictionary<int, string> GetTMDTLDADTSoQDBanDau()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLDADTSoQDBanDau();
        }

        public Dictionary<int, string> GetTMDTLTDTSoQDBanDau()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLTDTSoQDBanDau();
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTriBanDau()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLDADTTongGiaTriBanDau();
        }
        public Dictionary<int, double> GetTMDTLTDTTongGiaTriBanDau()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLTDTTongGiaTriBanDau();
        }

        public Dictionary<int, string> GetTMDTLDADTSoQDSuaDoi()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLDADTSoQDSuaDoi();
        }
        public Dictionary<int, string> GetTMDTLTDTSoQDSuaDoi()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLTDTSoQDSuaDoi();
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTriSuaDoi()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLDADTTongGiaTriSuaDoi();
        }
        public Dictionary<int, double> GetTMDTLTDTTongGiaTriSuaDoi()
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetTMDTLTDTTongGiaTriSuaDoi();
        }


        public List<object> GetLuyKeVonThanhToanCPKs(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetLuyKeVonThanhToanCPKs(iNam);
        }
        public List<object> GetLuyKeVonThanhToanHD(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetLuyKeVonThanhToanHD(iNam);
        }

        public List<object> GetDicKHVN_ODA(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetDicKHVN_ODA(iNam);
        }

        public List<object> GetDicKHVNDC_ODA(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetDicKHVNDC_ODA(iNam);
        }

        public List<object> GetUocThanhToanCPKs(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetUocThanhToanCPKs(iNam);
        }
        public List<object> GetUocThanhToanHD(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetUocThanhToanHD(iNam);
        }

        public List<object> GetThanhToanCPKsNam(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetThanhToanCPKsNam(iNam);
        }

        public List<object> GetThanhToanHDNam(int iNam)
        {
            return _TinhHinhThucHienODAVonXDCBRepository.GetThanhToanHDNam(iNam);
        }
        #endregion

        #region  Dung chung
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }
        #endregion

    }
}