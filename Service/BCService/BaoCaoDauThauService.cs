﻿
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BCService
{
    public interface IBaoCaoDauThauService
    {
         List<object> GetKetQuaDauThauTheoLinhVuc_NguonVon(int idNguonVon,int nam);
         List<object>  GetKetQuaDauThauTheoHinhThuc_NguonVon(int idNguonVon,int nam);
         List<object> GetKetQuaDauThauTheoLinhVuc_CoQuanPheDuyet(int idCQPD,int nam);
        List<object> GetKetQuaDauThauTheoHinhThuc_CoQuanPheDuyet(int idCQPD,int nam);
    }
    public class BaoCaoDauThauService:IBaoCaoDauThauService
    {
        private IKeHoachLuaChonNhaThauRepository _keHoachLuaChonNhaThauRepository;
        private IDuAnRepository _duAnRepository;
        private INhomDuAnRepository _nhomduanRepository;
        private IGoiThauRepository _goithauRepository;
        private ILinhVucDauThauRepository _linhVucDauThauRepository;
        private INguonVonDuAnRepository _nguonVonDuAnRepository;
        private IHinhThucLuaChonNhaThauRepository _hinhThucLuaChonNhaThauRepository;
        public BaoCaoDauThauService(INguonVonDuAnRepository nguonVonDuAnRepository, ILinhVucDauThauRepository linhVucDauThauRepository, INhomDuAnRepository nhomduanRepository, IGoiThauRepository goithauRepository, IDuAnRepository duAnRepository, IHinhThucLuaChonNhaThauRepository hinhThucLuaChonNhaThauRepository, IKeHoachLuaChonNhaThauRepository keHoachLuaChonNhaThauRepository)
        {
            _nguonVonDuAnRepository = nguonVonDuAnRepository;
            _linhVucDauThauRepository = linhVucDauThauRepository;
            _nhomduanRepository = nhomduanRepository;
            _goithauRepository = goithauRepository;
            _duAnRepository = duAnRepository;
            _hinhThucLuaChonNhaThauRepository = hinhThucLuaChonNhaThauRepository;
            _keHoachLuaChonNhaThauRepository = keHoachLuaChonNhaThauRepository;
        }
        public List<object>  GetKetQuaDauThauTheoLinhVuc_NguonVon(int idNguonVon,int nam)
        {

            var listIdDuAnTheoNguonVon = _nguonVonDuAnRepository.GetMulti(x => x.IdNguonVon == idNguonVon).Select(x =>   x.IdDuAn);
            var query = GetKetQuaDauThauTheoLinhVuc(listIdDuAnTheoNguonVon,nam);
            return query;
        }
        
        public List<object> GetKetQuaDauThauTheoHinhThuc_NguonVon(int idNguonVon,int nam)
        {
           
            var listIdDuAnTheoNguonVon = _nguonVonDuAnRepository.GetMulti(x => x.IdNguonVon == idNguonVon).Select(x => x.IdDuAn);
            var query = GetKetQuaDauThauTheoHinhthuc(listIdDuAnTheoNguonVon,nam);
            return query;
        }
        public List<object> GetKetQuaDauThauTheoLinhVuc_CoQuanPheDuyet(int idCQPD, int nam)
        {
            var q5 = _keHoachLuaChonNhaThauRepository.GetMulti(x => x.IdDieuChinhKeHoachLuaChonNhaThau != null&&x.IdCoQuanPheDuyet==idCQPD).GroupBy(x => x.IdDieuChinhKeHoachLuaChonNhaThau).Select(x => new
            {
                IdDieuChinhKeHoachLuaChonNhaThau = x.Key,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).FirstOrDefault().IdKeHoachLuaChonNhaThau,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).FirstOrDefault().IdDuAn,
            }).ToList();

            var q6 = from khv1 in _keHoachLuaChonNhaThauRepository.GetMulti(x=>x.IdDieuChinhKeHoachLuaChonNhaThau == null&& x.IdCoQuanPheDuyet == idCQPD)
                     where !(from x in q5 select x.IdDieuChinhKeHoachLuaChonNhaThau).Contains(khv1.IdKeHoachLuaChonNhaThau)
                     select new
                     {
                         IdDieuChinhKeHoachLuaChonNhaThau = khv1.IdDieuChinhKeHoachLuaChonNhaThau,
                         IdKeHoachLuaChonNhaThau = khv1.IdKeHoachLuaChonNhaThau,
                         IdDuAn=khv1.IdDuAn,
                     };

            var q56 = q5.Union(q6);
            var lstIdDuAn = q56.Select(x => x.IdDuAn);
            var query=   GetKetQuaDauThauTheoLinhVuc(lstIdDuAn,nam);
            return query;
        }
        public List<object> GetKetQuaDauThauTheoHinhThuc_CoQuanPheDuyet(int idCQPD, int nam)
        {
            var q5 = _keHoachLuaChonNhaThauRepository.GetMulti(x => x.IdDieuChinhKeHoachLuaChonNhaThau != null&&x.IdCoQuanPheDuyet==idCQPD).GroupBy(x => x.IdDieuChinhKeHoachLuaChonNhaThau).Select(x => new
            {
                IdDieuChinhKeHoachLuaChonNhaThau = x.Key,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).FirstOrDefault().IdKeHoachLuaChonNhaThau,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).FirstOrDefault().IdDuAn,
            }).ToList();

            var q6 = from khv1 in _keHoachLuaChonNhaThauRepository.GetMulti(x => x.IdDieuChinhKeHoachLuaChonNhaThau == null&&x.IdCoQuanPheDuyet==idCQPD)
                     where !(from x in q5 select x.IdDieuChinhKeHoachLuaChonNhaThau).Contains(khv1.IdKeHoachLuaChonNhaThau)
                     select new
                     {
                         IdDieuChinhKeHoachLuaChonNhaThau = khv1.IdDieuChinhKeHoachLuaChonNhaThau,
                         IdKeHoachLuaChonNhaThau = khv1.IdKeHoachLuaChonNhaThau,
                         IdDuAn = khv1.IdDuAn,
                     };

            var q56 = q5.Union(q6);
            var lstIdDuAn = q56.Select(x => x.IdDuAn);
            var query = GetKetQuaDauThauTheoHinhthuc(lstIdDuAn,nam);
            return query;
        }
        public List<object> GetKetQuaDauThauTheoLinhVuc(IEnumerable<int> lstTempt, int nam)
        {
            var  listDuAn = _duAnRepository.GetMulti(x => lstTempt.Contains(x.IdDuAn) && x.IdNhomDuAn != null);
            string[] includes = new string[] { "KetQuaDauThaus", "DuAn" };
            var query = _goithauRepository.GetMulti(goithau => goithau.IdLinhVucDauThau != null, includes).Join(listDuAn, goithau => goithau.IdDuAn, duan => duan.IdDuAn, (goithau, duan) => new
            {
                GiaGoiThau = goithau.GiaGoiThau,
                IdNhomDuAn = duan.IdNhomDuAn,
                KetQuaDauThau = goithau.KetQuaDauThaus.Where(x => (x.NgayPheDuyet != null) ? x.NgayPheDuyet.Value.Year == nam : false).OrderByDescending(x => x.NgayPheDuyet).FirstOrDefault(),
                IdLinhVucDauThau = goithau.IdLinhVucDauThau,
            }).Where(x => x.KetQuaDauThau != null).GroupBy(x => new { x.IdLinhVucDauThau, x.IdNhomDuAn }).Select(grpGoiThauTheoLinhVuc => new
            {
                IdLinhVucDauThau = grpGoiThauTheoLinhVuc.Key.IdLinhVucDauThau,
                IdNhomDuAn = grpGoiThauTheoLinhVuc.Key.IdNhomDuAn,
                TenLinhVuc = _linhVucDauThauRepository.GetSingleById(grpGoiThauTheoLinhVuc.Key.IdLinhVucDauThau ?? 0).TenLinhVucDauThau,
                TenNhomDuAn = _nhomduanRepository.GetSingleById(grpGoiThauTheoLinhVuc.Key.IdNhomDuAn ?? 0).TenNhomDuAn,
                GiaGoiThau = grpGoiThauTheoLinhVuc.ToList().Select(x => x.GiaGoiThau).Sum(),
                GiaTrungThau = grpGoiThauTheoLinhVuc.ToList().Select(a => a.KetQuaDauThau.GiaTrungThau).Sum(),
                SoGoiThau = grpGoiThauTheoLinhVuc.ToList().Count,
            }).ToList<object>();

            return query;
        }
        public List<object> GetKetQuaDauThauTheoHinhthuc(IEnumerable<int> lstTempt, int nam)
        {
            var listDuAn = _duAnRepository.GetMulti(x => lstTempt.Contains(x.IdDuAn) && x.IdNhomDuAn != null);
            string[] includes = new string[] { "KetQuaDauThaus", "DuAn" };
            var query = _goithauRepository.GetMulti(goithau => goithau.IdHinhThucLuaChon != null, includes).Join(listDuAn, goithau => goithau.IdDuAn, duan => duan.IdDuAn, (goithau, duan) => new
            {
                GiaGoiThau=goithau.GiaGoiThau,
                IdNhomDuAn = duan.IdNhomDuAn,
                KetQuaDauThau = goithau.KetQuaDauThaus.Where(x => x.NgayPheDuyet.Value.Year == nam).OrderByDescending(x => x.NgayPheDuyet).FirstOrDefault(),
                IdHinhThucLuaChon = goithau.IdHinhThucLuaChon,
            }).Where(x => x.KetQuaDauThau != null).GroupBy(x => new { x.IdHinhThucLuaChon, x.IdNhomDuAn }).Select(grpGoiThauTheoHinhThuc => new
            {
                IdHinhThucLuaChon = grpGoiThauTheoHinhThuc.Key.IdHinhThucLuaChon,
                IdNhomDuAn = grpGoiThauTheoHinhThuc.Key.IdNhomDuAn,
                TenHinhThuc = _hinhThucLuaChonNhaThauRepository.GetSingleById(grpGoiThauTheoHinhThuc.Key.IdHinhThucLuaChon ?? 0).TenHinhThucLuaChon,
                TenNhomDuAn = _nhomduanRepository.GetSingleById(grpGoiThauTheoHinhThuc.Key.IdNhomDuAn ?? 0).TenNhomDuAn,
                GiaGoiThau = grpGoiThauTheoHinhThuc.ToList().Select(x => x.GiaGoiThau).Sum(),
                GiaTrungThau = grpGoiThauTheoHinhThuc.ToList().Select(a => a.KetQuaDauThau.GiaTrungThau).Sum(),
                SoGoiThau = grpGoiThauTheoHinhThuc.ToList().Count,
            }).ToList<object>();
            return query;
        }
        


    }
   
}