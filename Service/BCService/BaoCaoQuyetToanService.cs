﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.BCService
{
    public interface IBaoCaoQuyetToanService
    {

    }

    public class BaoCaoQuyetToanService : IBaoCaoQuyetToanService
    {
        private IUnitOfWork _unitOfWork;

        public BaoCaoQuyetToanService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
    }
}
