﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ILoaiHopDongService
    {
        LoaiHopDong Add(LoaiHopDong loaiHopDong);

        void Update(LoaiHopDong loaiHopDong);

        LoaiHopDong Delete(int id);

        IEnumerable<LoaiHopDong> GetAll();

        IEnumerable<LoaiHopDong> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        LoaiHopDong GetById(int id);

        void Save();
    }

    public class LoaiHopDongService : ILoaiHopDongService
    {
        private ILoaiHopDongRepository _loaiHopDongRepository;
        private IUnitOfWork _unitOfWork;

        public LoaiHopDongService(ILoaiHopDongRepository loaiHopDongRepository, IUnitOfWork unitOfWork)
        {
            this._loaiHopDongRepository = loaiHopDongRepository;
            this._unitOfWork = unitOfWork;
        }
        public LoaiHopDong Add(LoaiHopDong loaiHopDong)
        {
            return _loaiHopDongRepository.Add(loaiHopDong);
        }

        public LoaiHopDong Delete(int id)
        {
            return _loaiHopDongRepository.Delete(id);
        }

        public IEnumerable<LoaiHopDong> GetAll()
        {
            return _loaiHopDongRepository.GetAll();
        }

        public IEnumerable<LoaiHopDong> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _loaiHopDongRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenLoaiHopDong.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLoaiHopDong).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LoaiHopDong GetById(int id)
        {
            return _loaiHopDongRepository.GetMulti(x => x.IdLoaiHopDong == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LoaiHopDong loaiHopDong)
        {
            _loaiHopDongRepository.Update(loaiHopDong);
        }
    }
}
