﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ILinhVucDauThauService
    {
        LinhVucDauThau Add(LinhVucDauThau linhVucDauThau);

        void Update(LinhVucDauThau linhVucDauThau);

        LinhVucDauThau Delete(int id);

        IEnumerable<LinhVucDauThau> GetAll();

        IEnumerable<LinhVucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        LinhVucDauThau GetById(int id);

        void Save();
    }

    public class LinhVucDauThauService : ILinhVucDauThauService
    {
        private ILinhVucDauThauRepository _linhVucDauThauRepository;
        private IUnitOfWork _unitOfWork;

        public LinhVucDauThauService(ILinhVucDauThauRepository linhVucDauThauRepository, IUnitOfWork unitOfWork)
        {
            this._linhVucDauThauRepository = linhVucDauThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public LinhVucDauThau Add(LinhVucDauThau linhVucDauThau)
        {
            return _linhVucDauThauRepository.Add(linhVucDauThau);
        }

        public LinhVucDauThau Delete(int id)
        {
            return _linhVucDauThauRepository.Delete(id);
        }

        public IEnumerable<LinhVucDauThau> GetAll()
        {
            return _linhVucDauThauRepository.GetAll();
        }

        public IEnumerable<LinhVucDauThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _linhVucDauThauRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenLinhVucDauThau.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLinhVucDauThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LinhVucDauThau GetById(int id)
        {
            return _linhVucDauThauRepository.GetMulti(x => x.IdLinhVucDauThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LinhVucDauThau linhVucDauThau)
        {
            _linhVucDauThauRepository.Update(linhVucDauThau);
        }
    }
}
