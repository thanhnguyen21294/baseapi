﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IHinhThucLuaChonNhaThauService
    {
        HinhThucLuaChonNhaThau Add(HinhThucLuaChonNhaThau hinhThucLuaChonNhaThau);

        void Update(HinhThucLuaChonNhaThau hinhThucLuaChonNhaThau);

        HinhThucLuaChonNhaThau Delete(int id);

        IEnumerable<HinhThucLuaChonNhaThau> GetAll();

        IEnumerable<HinhThucLuaChonNhaThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        HinhThucLuaChonNhaThau GetById(int id);

        void Save();
    }

    public class HinhThucLuaChonNhaThauService : IHinhThucLuaChonNhaThauService
    {
        private IHinhThucLuaChonNhaThauRepository _hinhThucLuaChonNhaThauRepository;
        private IUnitOfWork _unitOfWork;

        public HinhThucLuaChonNhaThauService(IHinhThucLuaChonNhaThauRepository hinhThucLuaChonNhaThauRepository, IUnitOfWork unitOfWork)
        {
            this._hinhThucLuaChonNhaThauRepository = hinhThucLuaChonNhaThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public HinhThucLuaChonNhaThau Add(HinhThucLuaChonNhaThau hinhThucLuaChonNhaThau)
        {
            return _hinhThucLuaChonNhaThauRepository.Add(hinhThucLuaChonNhaThau);
        }

        public HinhThucLuaChonNhaThau Delete(int id)
        {
            return _hinhThucLuaChonNhaThauRepository.Delete(id);
        }

        public IEnumerable<HinhThucLuaChonNhaThau> GetAll()
        {
            return _hinhThucLuaChonNhaThauRepository.GetAll();
        }

        public IEnumerable<HinhThucLuaChonNhaThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _hinhThucLuaChonNhaThauRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenHinhThucLuaChon.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdHinhThucLuaChon).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public HinhThucLuaChonNhaThau GetById(int id)
        {
            return _hinhThucLuaChonNhaThauRepository.GetMulti(x => x.IdHinhThucLuaChon == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(HinhThucLuaChonNhaThau hinhThucLuaChonNhaThau)
        {
            _hinhThucLuaChonNhaThauRepository.Update(hinhThucLuaChonNhaThau);
        }
    }
}
