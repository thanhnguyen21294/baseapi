﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IDuAnUserService
    {
        DuAnUser Add(DuAnUser DuAnUser);

        void Update(DuAnUser DuAnUser);

        IEnumerable<DuAnUser> GetByIdDuAn(int idDA);
        IEnumerable<DuAnUser> GetByIdDuAnPhieu(int idDA);

        IEnumerable<DuAnUser> GetAll();

        IEnumerable<DuAnUser> GetByIdDuAnIDUser(int idDA, string userID);
        IEnumerable<DuAnUser> GetDuAnTenUser();
        DuAnUser Delete(int id);

        void Save();
    }

    public class DuAnUserService : IDuAnUserService
    {
        private IDuAnUserRepository _DuAnUserRepository;
        private IUnitOfWork _unitOfWork;

        public DuAnUserService(IDuAnUserRepository DuAnUserRepository, IUnitOfWork unitOfWork)
        {
            this._DuAnUserRepository = DuAnUserRepository;
            this._unitOfWork = unitOfWork;
        }
        public DuAnUser Add(DuAnUser DuAnUser)
        {
            return _DuAnUserRepository.Add(DuAnUser);
        }
        public IEnumerable<DuAnUser> GetByIdDuAn(int idDA)
        {
            return _DuAnUserRepository.GetMulti(x => x.IdDuAn == idDA, new string[] {"AppUsers"});
        }

        public IEnumerable<DuAnUser> GetDuAnTenUser()
        {
            return _DuAnUserRepository.GetAll(new string[] { "AppUsers" });
        }

        public IEnumerable<DuAnUser> GetByIdDuAnPhieu(int idDA)
        {
            return _DuAnUserRepository.GetMulti(x => x.IdDuAn == idDA && x.AppUsers.IdNhomDuAnTheoUser != 14, new string[] { "AppUsers.AppRoles", "AppUsers.NhomDuAnTheoUser" });
        }

        public DuAnUser Delete(int id)
        {
            return _DuAnUserRepository.Delete(id);
        }

        public IEnumerable<DuAnUser> GetAll()
        {
            return _DuAnUserRepository.GetAll();
        }

        public IEnumerable<DuAnUser> GetByIdDuAnIDUser(int idDA, string userID)
        {
            return _DuAnUserRepository.GetMulti(x => x.IdDuAn == idDA && x.UserId == userID);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DuAnUser DuAnUser)
        {
            _DuAnUserRepository.Update(DuAnUser);
        }
    }
}
