﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IQuanLyDAService
    {
        QuanLyDA Add(QuanLyDA QuanLyDA);

        void Update(QuanLyDA QuanLyDA);

        QuanLyDA Delete(int id);

        IEnumerable<QuanLyDA> GetAll();

        IEnumerable<QuanLyDA> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        QuanLyDA GetById(int id);

        void Save();
    }

    public class QuanLyDAService : IQuanLyDAService
    {
        private IQuanLyDARepository _QuanLyDARepository;
        private IUnitOfWork _unitOfWork;

        public QuanLyDAService(IQuanLyDARepository QuanLyDARepository, IUnitOfWork unitOfWork)
        {
            this._QuanLyDARepository = QuanLyDARepository;
            this._unitOfWork = unitOfWork;
        }
        public QuanLyDA Add(QuanLyDA QuanLyDA)
        {
            return _QuanLyDARepository.Add(QuanLyDA);
        }

        public QuanLyDA Delete(int id)
        {
            return _QuanLyDARepository.Delete(id);
        }

        public IEnumerable<QuanLyDA> GetAll()
        {
            return _QuanLyDARepository.GetAll();
        }

        public IEnumerable<QuanLyDA> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _QuanLyDARepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdQuanLyDA).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public QuanLyDA GetById(int id)
        {
            return _QuanLyDARepository.GetMulti(x => x.IdQuanLyDA == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QuanLyDA QuanLyDA)
        {
            _QuanLyDARepository.Update(QuanLyDA);
        }
    }
}
