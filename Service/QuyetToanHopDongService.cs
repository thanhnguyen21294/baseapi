﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IQuyetToanHopDongService
    {
        QuyetToanHopDong Add(QuyetToanHopDong hopDong);

        void Update(QuyetToanHopDong hopDong);

        QuyetToanHopDong Delete(int id);

        IEnumerable<QuyetToanHopDong> GetAll();

        IEnumerable<QuyetToanHopDong> GetByIdHopDong(int idHopDong);

        IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);

        QuyetToanHopDong GetById(int id);

        void Save();
    }

    public class QuyetToanHopDongService : IQuyetToanHopDongService
    {
        private IQuyetToanHopDongRepository _QuyetToanHopDongRepository;
        private IUnitOfWork _unitOfWork;

        public QuyetToanHopDongService(IQuyetToanHopDongRepository quyetToanHopDongRepository, IUnitOfWork unitOfWork)
        {
            this._QuyetToanHopDongRepository = quyetToanHopDongRepository;
            this._unitOfWork = unitOfWork;
        }
        public QuyetToanHopDong Add(QuyetToanHopDong QuyetToanHopDong)
        {
            return _QuyetToanHopDongRepository.Add(QuyetToanHopDong);
        }

        public QuyetToanHopDong Delete(int id)
        {
            return _QuyetToanHopDongRepository.Delete(id);
        }

        public IEnumerable<QuyetToanHopDong> GetAll()
        {
            return _QuyetToanHopDongRepository.GetAll();
        }

        public IEnumerable<QuyetToanHopDong> GetByIdHopDong(int idHopDong)
        {
            var query = _QuyetToanHopDongRepository.GetAll();
            query = query.Where(x => x.IdHopDong == idHopDong);
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _QuyetToanHopDongRepository.GetQuyetToanHopDongs(idDuAn, filter);
            totalRow = query.Count();
            return query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public QuyetToanHopDong GetById(int id)
        {
            return _QuyetToanHopDongRepository.GetMulti(x => x.IdQuyetToan == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(QuyetToanHopDong QuyetToanHopDong)
        {
            _QuyetToanHopDongRepository.Update(QuyetToanHopDong);
        }
    }
}
