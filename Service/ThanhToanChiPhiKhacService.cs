﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Service
{
    public interface IThanhToanChiPhiKhacService
    {
        ThanhToanChiPhiKhac Add(ThanhToanChiPhiKhac iQuyetToanDuAn);

        void Update(ThanhToanChiPhiKhac iQuyetToanDuAn);

        ThanhToanChiPhiKhac Delete(int id);

        IEnumerable<ThanhToanChiPhiKhac> GetAll();
        IEnumerable<ThanhToanChiPhiKhac> GetForViewKeToan(int nam);
        IEnumerable<ThanhToanChiPhiKhac> GetAll(string filter);
        IEnumerable<ThanhToanChiPhiKhac> GetByIdDuAn(int idDA);
        IEnumerable<ThanhToanChiPhiKhac> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter);
        IEnumerable<ThanhToanChiPhiKhac> GetThanhToan_NguonVonThanhToanCPK();
        IEnumerable<ThanhToanChiPhiKhac> GetYear(int year);
        IEnumerable<ThanhToanChiPhiKhac> GetByIdNhaThau(int idnhathau);
        ThanhToanChiPhiKhac GetById(int id);

        void Save();
    }
    public class ThanhToanChiPhiKhacService : IThanhToanChiPhiKhacService
    {
        private IThanhToanChiPhiKhacRepository _ThanhToanChiPhiKhacRepository;
        private IUnitOfWork _unitOfWork;

        public ThanhToanChiPhiKhacService(IThanhToanChiPhiKhacRepository ThanhToanChiPhiKhacRepository, IUnitOfWork unitOfWork)
        {
            this._ThanhToanChiPhiKhacRepository = ThanhToanChiPhiKhacRepository;
            this._unitOfWork = unitOfWork;
        }
        public ThanhToanChiPhiKhac Add(ThanhToanChiPhiKhac quyetToanDuAn)
        {
            return _ThanhToanChiPhiKhacRepository.Add(quyetToanDuAn);
        }

        public ThanhToanChiPhiKhac Delete(int id)
        {
            return _ThanhToanChiPhiKhacRepository.Delete(id);
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetAll()
        {
            return _ThanhToanChiPhiKhacRepository.GetAll(new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs" });
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _ThanhToanChiPhiKhacRepository.GetMulti(x => x.NoiDung.Contains(filter), new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs" });
            }

            return _ThanhToanChiPhiKhacRepository.GetAll(new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs" });
        }
        public IEnumerable<ThanhToanChiPhiKhac> GetByIdDuAn(int idDA)
        {
            var query = _ThanhToanChiPhiKhacRepository.GetAll(new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs", "LoaiChiPhi" });
            query = query.Where(x => x.IdDuAn == idDA);
            return query.OrderBy(x => x.IdThanhToanChiPhiKhac);
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetYear(int year)
        {
            var query = _ThanhToanChiPhiKhacRepository.GetAll(new string[] { "NguonVonThanhToanChiPhiKhacs" });
            query = query.Where(x => (x.NgayThanhToan != null && x.NgayThanhToan.Value.Year <= year));
            return query.OrderBy(x => x.IdThanhToanChiPhiKhac);
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetForViewKeToan(int nam)
        {
            var query = _ThanhToanChiPhiKhacRepository.GetMulti(x => x.NgayThanhToan != null && x.IdLoaiChiPhi != null,new string[] { "NguonVonThanhToanChiPhiKhacs.NguonVonDuAn.NguonVon" }).ToList();
            return query.OrderBy(x => x.IdThanhToanChiPhiKhac);
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _ThanhToanChiPhiKhacRepository.GetAll(new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs", "LoaiChiPhi" });
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDA);
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDA);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdThanhToanChiPhiKhac).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ThanhToanChiPhiKhac GetById(int id)
        {
            return _ThanhToanChiPhiKhacRepository.GetSingleByCondition(x => x.IdThanhToanChiPhiKhac == id, new string[] { "DanhMucNhaThau", "NguonVonThanhToanChiPhiKhacs" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ThanhToanChiPhiKhac updateQuyetToanDuAn)
        {
            _ThanhToanChiPhiKhacRepository.Update(updateQuyetToanDuAn);
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetThanhToan_NguonVonThanhToanCPK()
        {
            return _ThanhToanChiPhiKhacRepository.GetThanhToan_NguonVonThanhToanCPK();
        }

        public IEnumerable<ThanhToanChiPhiKhac> GetByIdNhaThau(int idnhathau)
        {
           return _ThanhToanChiPhiKhacRepository.GetAll(new [] { "NguonVonThanhToanChiPhiKhacs" }).Where(x => x.IdNhaThau == idnhathau);
        }
    }
}
