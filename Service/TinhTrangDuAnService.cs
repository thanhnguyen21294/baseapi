﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ITinhTrangDuAnService
    {
        TinhTrangDuAn Add(TinhTrangDuAn tinhTrangDuAn);

        void Update(TinhTrangDuAn tinhTrangDuAn);

        TinhTrangDuAn Delete(int id);

        IEnumerable<TinhTrangDuAn> GetAll();

        IEnumerable<TinhTrangDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        TinhTrangDuAn GetById(int id);

        void Save();
    }

    public class TinhTrangDuAnService : ITinhTrangDuAnService
    {
        private ITinhTrangDuAnRepository _tinhTrangDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public TinhTrangDuAnService(ITinhTrangDuAnRepository tinhTrangDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._tinhTrangDuAnRepository = tinhTrangDuAnRepository;
            this._unitOfWork = unitOfWork;
        }
        public TinhTrangDuAn Add(TinhTrangDuAn tinhTrangDuAn)
        {
            return _tinhTrangDuAnRepository.Add(tinhTrangDuAn);
        }

        public TinhTrangDuAn Delete(int id)
        {
            return _tinhTrangDuAnRepository.Delete(id);
        }

        public IEnumerable<TinhTrangDuAn> GetAll()
        {
            return _tinhTrangDuAnRepository.GetAll().OrderBy(x => x.ThuTu);
        }

        public IEnumerable<TinhTrangDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _tinhTrangDuAnRepository.GetAll().OrderBy(x => x.ThuTu);
            List<TinhTrangDuAn> search = new List<TinhTrangDuAn>();
            if (filter != null)
            {
                var listSearch = query.Where(x => x.TenTinhTrangDuAn.Contains(filter));
                foreach (var nguonVon in listSearch)
                {
                    var items = search.Where(x => x.IdTinhTrangDuAn == nguonVon.IdTinhTrangDuAn);
                    if (!items.Any())
                    {
                        search.Add(nguonVon);
                        search = search.Union(FindAllParents(query.ToList(), nguonVon.IdTinhTrangDuAn)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdTinhTrangDuAnCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdTinhTrangDuAn).Skip((page - 1) * pageSize).Take(pageSize);

                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                return parents;

            }
            var parent = query.Where(y => y.IdTinhTrangDuAnCha == null).OrderBy(x => x.IdTinhTrangDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = query.Where(y => y.IdTinhTrangDuAnCha == null).OrderBy(x => x.IdTinhTrangDuAn).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = query.Count(x => x.IdTinhTrangDuAnCha == null);
            foreach (var item in parent)
            {
                parent = parent.Union(GetChild(query, item));
            }
            return parent;
        }
        private IEnumerable<TinhTrangDuAn> GetChild(IEnumerable<TinhTrangDuAn> tinhTrangDuAns, TinhTrangDuAn tinhTrangDuAn)
        {
            return tinhTrangDuAns.Where(x => x.IdTinhTrangDuAnCha == tinhTrangDuAn.IdTinhTrangDuAn || x.IdTinhTrangDuAn == tinhTrangDuAn.IdTinhTrangDuAn)
                .Union(tinhTrangDuAns.Where(x => x.IdTinhTrangDuAnCha == tinhTrangDuAn.IdTinhTrangDuAn).SelectMany(y => GetChild(tinhTrangDuAns, y)));
        }

        private IEnumerable<TinhTrangDuAn> FindAllParents(List<TinhTrangDuAn> all_data, int? child)
        {
            var parent = all_data.FirstOrDefault(x => x.IdTinhTrangDuAn == child);

            if (parent == null)
                return Enumerable.Empty<TinhTrangDuAn>();

            return new[] { parent }.Concat(FindAllParents(all_data, parent.IdTinhTrangDuAnCha));
        }

        public TinhTrangDuAn GetById(int id)
        {
            return _tinhTrangDuAnRepository.GetMulti(x => x.IdTinhTrangDuAn == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(TinhTrangDuAn tinhTrangDuAn)
        {
            _tinhTrangDuAnRepository.Update(tinhTrangDuAn);
        }
    }
}
