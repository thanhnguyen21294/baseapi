﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface INhomDuAnService
    {
        NhomDuAn Add(NhomDuAn nhomDuAn);

        void Update(NhomDuAn nhomDuAn);

        NhomDuAn Delete(int id);

        IEnumerable<NhomDuAn> GetAll();

        IEnumerable<NhomDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        NhomDuAn GetById(int id);

        void Save();
    }
    public class NhomDuAnService : INhomDuAnService
    {
        private INhomDuAnRepository _nhomDuAnRepository;
        private IUnitOfWork _unitOfWork;

        public NhomDuAnService(INhomDuAnRepository nhomDuAnRepository, IUnitOfWork unitOfWork)
        {
            this._nhomDuAnRepository = nhomDuAnRepository;
            this._unitOfWork = unitOfWork;
        }
        public NhomDuAn Add(NhomDuAn nhomDuAn)
        {
            return _nhomDuAnRepository.Add(nhomDuAn);
        }

        public NhomDuAn Delete(int id)
        {
            return _nhomDuAnRepository.Delete(id);
        }

        public IEnumerable<NhomDuAn> GetAll()
        {
            return _nhomDuAnRepository.GetAll();
        }

        public IEnumerable<NhomDuAn> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _nhomDuAnRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenNhomDuAn.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdNhomDuAn).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public NhomDuAn GetById(int id)
        {
            return _nhomDuAnRepository.GetMulti(x => x.IdNhomDuAn == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NhomDuAn nhomDuAn)
        {
            _nhomDuAnRepository.Update(nhomDuAn);
        }
    }
}
