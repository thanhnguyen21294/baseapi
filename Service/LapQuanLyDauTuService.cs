﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ILapQuanLyDauTuService
    {
        LapQuanLyDauTu Add(LapQuanLyDauTu iLapQuanLyDauTu);

        void Update(LapQuanLyDauTu iLapQuanLyDauTu);

        LapQuanLyDauTu Delete(int id);
        IEnumerable<LapQuanLyDauTu> GetAll();
        IEnumerable<LapQuanLyDauTu> GetAll(int iLoai);
        LapQuanLyDauTu GetNewestTheoDuAn(int id);
        IEnumerable<LapQuanLyDauTu> GetAll(string filter, int iLoai);
        IEnumerable<LapQuanLyDauTu> GetAllByIdDuAn(int iLoai, int idDuAn);
        LapQuanLyDauTu GetFirstTheoDuAn(int id);
        LapQuanLyDauTu GetLastTheoDieuChinhTheoDuAn(int idduan);
        IEnumerable<LapQuanLyDauTu> GetByFilter(int idDA, int iLoai, int page, int pageSize, string sort, out int totalRow, string filter);

        LapQuanLyDauTu GetById(int id);

        void Save();
    }
    public class LapQuanLyDauTuService : ILapQuanLyDauTuService
    {
        private ILapQuanLyDauTuRepository _LapQuanLyDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public LapQuanLyDauTuService(ILapQuanLyDauTuRepository lapquanlydautuRepository, IUnitOfWork unitOfWork)
        {
            this._LapQuanLyDauTuRepository = lapquanlydautuRepository;
            this._unitOfWork = unitOfWork;
        }
        public LapQuanLyDauTu Add(LapQuanLyDauTu lapQuanLyDauTu)
        {
            return _LapQuanLyDauTuRepository.Add(lapQuanLyDauTu);
        }

        public LapQuanLyDauTu Delete(int id)
        {
            return _LapQuanLyDauTuRepository.Delete(id);
        }

        public IEnumerable<LapQuanLyDauTu> GetAll(int iLoai)
        {
            return _LapQuanLyDauTuRepository.GetAll().Where(x => x.LoaiQuanLyDauTu == iLoai);
        }

        public IEnumerable<LapQuanLyDauTu> GetAll(string filter, int iLoai)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _LapQuanLyDauTuRepository.GetMulti(x => x.SoQuyetDinh.Contains(filter));
            }
            return _LapQuanLyDauTuRepository.GetLapQuanLyDauTu().Where(x => x.LoaiQuanLyDauTu == iLoai);
        }

        public IEnumerable<LapQuanLyDauTu> GetAll()
        {
            return _LapQuanLyDauTuRepository.GetAll();
        }

        public IEnumerable<LapQuanLyDauTu> GetAllByIdDuAn(int iLoai, int idDuAn)
        {
            return _LapQuanLyDauTuRepository.GetAll().Where(x => x.LoaiQuanLyDauTu == iLoai && x.IdDuAn == idDuAn);
        }

        public IEnumerable<LapQuanLyDauTu> GetByFilter(int idDA, int iLoai, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _LapQuanLyDauTuRepository.GetLapQuanLyDauTu();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter) && x.LoaiQuanLyDauTu == iLoai && x.IdDuAn == idDA);
            }
            else
            {
                query = query.Where(x => x.LoaiQuanLyDauTu == iLoai && x.IdDuAn == idDA);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLapQuanLyDauTu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LapQuanLyDauTu GetById(int id)
        {
            return _LapQuanLyDauTuRepository.GetLapQuanLyDauTu().Where(x => x.IdLapQuanLyDauTu == id).SingleOrDefault();
        }

        public LapQuanLyDauTu GetFirstTheoDuAn(int id)
        {
            var query = _LapQuanLyDauTuRepository.GetAll().Where(x => x.IdDuAn == id && x.NgayPheDuyet != null);
            LapQuanLyDauTu lapQuanLyDauTu = new LapQuanLyDauTu();
            if (query.Where(x => x.LoaiQuanLyDauTu == 1).Count() > 0)
            {
                lapQuanLyDauTu = query.Where(x => x.LoaiQuanLyDauTu == 1).OrderBy(x => x.NgayPheDuyet).First();
            }
            if (query.Where(x => x.LoaiQuanLyDauTu == 2).Count() > 0 && query.Where(x => x.LoaiQuanLyDauTu == 1).Count() == 0)
            {
                lapQuanLyDauTu = query.Where(x => x.LoaiQuanLyDauTu == 2).OrderBy(x => x.NgayPheDuyet).First();
            }
            return lapQuanLyDauTu;
        }

        public LapQuanLyDauTu GetLastTheoDieuChinhTheoDuAn(int idduan)
        {
            var query = this.GetAll().Where(x => x.IdDuAn == idduan && x.NgayPheDuyet != null && x.LoaiDieuChinh !=null);
            LapQuanLyDauTu lapQuanLyDauTu = new LapQuanLyDauTu();
            if (query.Count()>0)
            {
                if (query.Where(x => x.LoaiQuanLyDauTu == 1).Count() > 0)
                {
                    var query1 = query.Where(x => x.LoaiQuanLyDauTu == 1 && x.LoaiDieuChinh != null && x.LoaiDieuChinh == 2);
                    if (query1.Count() > 0)
                    {
                        var query2 = query1.OrderByDescending(x => x.NgayPheDuyet).First();
                        lapQuanLyDauTu = query2;
                    }
                    else
                    {
                        var query2 = query.Where(x => x.LoaiQuanLyDauTu == 1 && x.LoaiDieuChinh != null && x.LoaiDieuChinh == 1).OrderByDescending(x => x.NgayPheDuyet).First();
                        if (query2 !=null)
                        {
                            lapQuanLyDauTu = query2;
                        }
                        //else
                        //{
                        //    var query3 = query.Where(x => x.LoaiQuanLyDauTu == 1 && x.LoaiDieuChinh == null).OrderByDescending(x => x.NgayPheDuyet).First();
                        //    lapQuanLyDauTu = query3;
                        //}
                    }
                }
                else
                {
                    if (query.Where(x => x.LoaiQuanLyDauTu == 2).Count() > 0){
                        var query1 = query.Where(x => x.LoaiQuanLyDauTu == 2 && x.LoaiDieuChinh != null && x.LoaiDieuChinh == 2);
                        if (query1.Count() > 0)
                        {
                            var query2 = query1.OrderByDescending(x => x.NgayPheDuyet).First();
                            lapQuanLyDauTu = query2;
                        }
                        else
                        {
                            var query2 = query.Where(x => x.LoaiQuanLyDauTu == 2 && x.LoaiDieuChinh != null && x.LoaiDieuChinh == 1).OrderByDescending(x => x.NgayPheDuyet).First();
                            if (query2 != null)
                            {
                                lapQuanLyDauTu = query2;
                            }
                            //else
                            //{
                            //    var query3 = query.Where(x => x.LoaiQuanLyDauTu == 2 && x.LoaiDieuChinh == null).OrderByDescending(x => x.NgayPheDuyet).First();
                            //    lapQuanLyDauTu = query3;
                            //}
                        }
                    }
                    
                }
                return lapQuanLyDauTu;
            }
            else
            {
                return null;
            }
            
  
        }

        public LapQuanLyDauTu GetNewestTheoDuAn(int id)
        {
            var query = _LapQuanLyDauTuRepository.GetAll().Where(x => x.IdDuAn == id && x.NgayPheDuyet !=null);
            LapQuanLyDauTu lapQuanLyDauTu = new LapQuanLyDauTu();
            if (query.Where(x => x.LoaiQuanLyDauTu == 1).Count() > 0)
            {
                lapQuanLyDauTu= query.Where(x=>x.LoaiQuanLyDauTu==1).OrderByDescending(x => x.NgayPheDuyet).First();
            }
            else
            {
                if (query.Where(x => x.LoaiQuanLyDauTu == 2).Count() > 0)
                {
                    lapQuanLyDauTu = query.Where(x => x.LoaiQuanLyDauTu == 2).OrderByDescending(x => x.NgayPheDuyet).First();
                }
                    
            }
            return lapQuanLyDauTu;

        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LapQuanLyDauTu updateLapQuanLyDauTu)
        {
            _LapQuanLyDauTuRepository.Update(updateLapQuanLyDauTu);
        }
       
    }
}
