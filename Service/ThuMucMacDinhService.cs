﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
   public  interface IThuMucMacDinhService
    {
        IEnumerable<ThuMucMacDinh> GetAll();
        IEnumerable<ThuMucMacDinh> GetByFilter(string filter);
        ThuMucMacDinh GetTMMDById(int id);
        ThuMucMacDinh Add(ThuMucMacDinh tmmd);
        void Update(ThuMucMacDinh tmmd);
        void Delete(int id);
        void Save();
    }
   public  class ThuMucMacDinhService : IThuMucMacDinhService
    {
        IThuMucMacDinhRepository _tmmdRepository;
        IUnitOfWork _unitOfWork;
        public ThuMucMacDinhService(IThuMucMacDinhRepository tmmdRepository, IUnitOfWork unitOfWork)
        {
            _tmmdRepository = tmmdRepository;
            _unitOfWork = unitOfWork;
        }

        public ThuMucMacDinh Add(ThuMucMacDinh tmmd)
        {
           tmmd= _tmmdRepository.Add(tmmd);
        
            return tmmd;
        }

        public void Delete(int id)
        {
            _tmmdRepository.Delete(id);
          
        }

        public IEnumerable<ThuMucMacDinh> GetByFilter(string filter)
        {
            IEnumerable<ThuMucMacDinh> tmmds = _tmmdRepository.GetByFilter(filter);
            return tmmds;
        }

        public void Update(ThuMucMacDinh tmmd)
        {
            _tmmdRepository.Update(tmmd);
            
        }
        public void Save()
        {

            _unitOfWork.Commit();
        }

        public ThuMucMacDinh GetTMMDById(int id)
        {
            return _tmmdRepository.GetSingleById(id);
        }

        public IEnumerable<ThuMucMacDinh> GetAll()
        {
           return  _tmmdRepository.GetAll();
        }
    }
}
