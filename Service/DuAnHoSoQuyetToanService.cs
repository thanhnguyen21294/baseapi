﻿using Data.Infrastructure;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Models;

namespace Service
{
    public interface IDuAnHoSoQuyetToanService
    {
        DuAnHoSoQuyetToan Add(DuAnHoSoQuyetToan DuAnHoSoQuyetToans);

        void Update(DuAnHoSoQuyetToan id);

        DuAnHoSoQuyetToan Delete(int id);

        IEnumerable<DuAnHoSoQuyetToan> GetAll();
        IEnumerable<DuAnHoSoQuyetToan> GetByIdDA(int idDA);

        DuAnHoSoQuyetToan GetById(int id);

        DuAnHoSoQuyetToan GetByIdTwoKey(int idDA, int IdDMHS);

        bool CheckExistedDuAn(int idDA);

        void Save();

        IEnumerable<DuAnHoSoQuyetToan> GetForView();
    }
    public class DuAnHoSoQuyetToanService : IDuAnHoSoQuyetToanService
    {
        private IDuAnHoSoQuyetToanRepository _DuAnHoSoQuyetToanRepository;
        private IUnitOfWork _unitOfWork;

        public DuAnHoSoQuyetToanService(IDuAnHoSoQuyetToanRepository DuAnHoSoQuyetToanRepository, IUnitOfWork unitOfWork)
        {
            this._DuAnHoSoQuyetToanRepository = DuAnHoSoQuyetToanRepository;
            this._unitOfWork = unitOfWork;
        }
        public DuAnHoSoQuyetToan Add(DuAnHoSoQuyetToan DuAnHoSoQuyetToans)
        {
            return _DuAnHoSoQuyetToanRepository.Add(DuAnHoSoQuyetToans);
        }

        public IEnumerable<DuAnHoSoQuyetToan> GetForView()
        {
            var lst = _DuAnHoSoQuyetToanRepository.GetMulti(x => x.KyThuat == 1, new string[] { "DanhMucHoSoQuyetToan" });
            return lst;
        }
        public DuAnHoSoQuyetToan Delete(int id)
        {
            return _DuAnHoSoQuyetToanRepository.Delete(id);
        }
        public DuAnHoSoQuyetToan GetByIdTwoKey(int idDA, int IdDMHS)
        {
            return _DuAnHoSoQuyetToanRepository.GetMulti(x => x.IdDuAn == idDA && x.IdDanhMucHoSoQuyetToan == IdDMHS).SingleOrDefault();
        }
        public IEnumerable<DuAnHoSoQuyetToan> GetAll()
        {
            return _DuAnHoSoQuyetToanRepository.GetAll();
        }
        public IEnumerable<DuAnHoSoQuyetToan> GetByIdDA(int idDA)
        {
            return _DuAnHoSoQuyetToanRepository.GetMulti(x => x.IdDuAn == idDA, new string[] { "DanhMucHoSoQuyetToan" });
        }
        public DuAnHoSoQuyetToan GetById(int id)
        {
            return _DuAnHoSoQuyetToanRepository.GetMulti(x => x.IdDuAnHoSoQuyetToan == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public bool CheckExistedDuAn(int idDA)
        {
            return _DuAnHoSoQuyetToanRepository.GetMulti(x => x.IdDuAn == idDA).ToList().Count > 0 ? true : false;
        }

        public void Update(DuAnHoSoQuyetToan DuAnHoSoQuyetToans)
        {
            _DuAnHoSoQuyetToanRepository.Update(DuAnHoSoQuyetToans);
        }
    }
}
