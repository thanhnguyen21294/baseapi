﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface IKetQuaDauThauService
    {
        KetQuaDauThau Add(KetQuaDauThau ketQuaDauThau);

        void Update(KetQuaDauThau ketQuaDauThau);

        KetQuaDauThau Delete(int id);

        IEnumerable<KetQuaDauThau> GetAll();

        IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);

        KetQuaDauThau GetById(int id);

        void Save();
    }



    public class KetQuaDauThauService : IKetQuaDauThauService
    {
        private IKetQuaDauThauRepository _ketQuaDauThauRepository;
        private IUnitOfWork _unitOfWork;

        public KetQuaDauThauService(IKetQuaDauThauRepository ketQuaDauThauRepository, IUnitOfWork unitOfWork)
        {
            this._ketQuaDauThauRepository = ketQuaDauThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public KetQuaDauThau Add(KetQuaDauThau ketQuaDauThau)
        {
            return _ketQuaDauThauRepository.Add(ketQuaDauThau);
        }

        public KetQuaDauThau Delete(int id)
        {
            return _ketQuaDauThauRepository.Delete(id);
        }

        public IEnumerable<KetQuaDauThau> GetAll()
        {
            return _ketQuaDauThauRepository.GetAll();
        }

        public IEnumerable<GoiThau> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _ketQuaDauThauRepository.GetKetQuaByGoiThaus(idDuAn, filter);
            totalRow = query.Count();
            var ketQua = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //if (!ketQua.Any() && page > 1)
            //{
            //    page = page - 1;
            //    ketQua = query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
            //}
            return ketQua;
        }

        public KetQuaDauThau GetById(int id)
        {
            return _ketQuaDauThauRepository.GetSingleByCondition(x=>x.IdKetQuaLuaChonNhaThau == id, new []{"DanhMucNhaThau"});
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(KetQuaDauThau ketQuaDauThau)
        {
            _ketQuaDauThauRepository.Update(ketQuaDauThau);
        }
    }
}
