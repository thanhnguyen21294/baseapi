﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IThucHienHopDongService
    {
        ThucHienHopDong Add(ThucHienHopDong hopDong);

        void Update(ThucHienHopDong hopDong);

        ThucHienHopDong Delete(int id);

        IEnumerable<ThucHienHopDong> GetAll();

        IEnumerable<ThucHienHopDong> GetForKeToanViewTheoDoi();

        IEnumerable<ThucHienHopDong> GetByIdHopDong(int idHopDong);

        IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter);

        IEnumerable<ThucHienHopDong> GetByFilterNew(int idDuAn, int page, int pageSize, out int totalRow, string filter);

        ThucHienHopDong GetById(int id);

        void Save();
    }

    public class ThucHienHopDongService : IThucHienHopDongService
    {
        private IThucHienHopDongRepository _ThucHienHopDongRepository;
        private IUnitOfWork _unitOfWork;

        public ThucHienHopDongService(IThucHienHopDongRepository thucHienHopDongRepository, IUnitOfWork unitOfWork)
        {
            this._ThucHienHopDongRepository = thucHienHopDongRepository;
            this._unitOfWork = unitOfWork;
        }
        public ThucHienHopDong Add(ThucHienHopDong thucHienHopDong)
        {
            return _ThucHienHopDongRepository.Add(thucHienHopDong);
        }

        public ThucHienHopDong Delete(int id)
        {
            return _ThucHienHopDongRepository.Delete(id);
        }

        public IEnumerable<ThucHienHopDong> GetAll()
        {
            return _ThucHienHopDongRepository.GetAll();
        }

        public IEnumerable<ThucHienHopDong> GetByIdHopDong(int idHopDong)
        {
            var query = _ThucHienHopDongRepository.GetAll();
            query = query.Where(x => x.IdHopDong == idHopDong);
            return query.OrderBy(x => x.IdHopDong);
        }

        public IEnumerable<ThucHienHopDong> GetForKeToanViewTheoDoi()
        {
            var query = _ThucHienHopDongRepository.GetAll(new string[] { "HopDong", "LoaiChiPhi"});
            return query;
        }
        public IEnumerable<HopDong> GetByFilter(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
            var query = _ThucHienHopDongRepository.GetThucHienHopDongs(idDuAn, filter);
            totalRow = query.Count();
            return query.OrderBy(x => x.IdGoiThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<ThucHienHopDong> GetByFilterNew(int idDuAn, int page, int pageSize, out int totalRow, string filter)
        {
           
            var query = _ThucHienHopDongRepository.GetMulti(x => x.IdDuAn == idDuAn && (filter != null ? x.NoiDung.Contains(filter) : true) , new string[] { "HopDong", "LoaiChiPhi" });
            totalRow = query.Count();
            return query.OrderBy(x => x.NgayNghiemThu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ThucHienHopDong GetById(int id)
        {
            return _ThucHienHopDongRepository.GetMulti(x => x.IdThucHien == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ThucHienHopDong thucHienHopDong)
        {
            _ThucHienHopDongRepository.Update(thucHienHopDong);
        }
    }
}
