﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Repositories;
using Model.Models;
using Data.Infrastructure;

namespace Service
{
    public interface IThamDinhLapQuanLyDauTuService
    {
        IEnumerable<ThamDinhLapQuanLyDauTu> GetByFilter(int idLapQuanLyDauTu, int page, int pageSize, string sort, out int totalRow, string filter);
        IEnumerable<ThamDinhLapQuanLyDauTu> GetAllForHistory();
        ThamDinhLapQuanLyDauTu Add(ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu);
        ThamDinhLapQuanLyDauTu GetById(int idThamDinhLapquanLyDauTu);
        void Update(ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu);
        bool Delete(int id);
        void Save();
    }
    public class ThamDinhLapQuanLyDauTuService:IThamDinhLapQuanLyDauTuService
    {
        public IThamDinhLapQuanLyDauTuRepository _thamDinhLapQuanLyDauTuRepository;
        public IUnitOfWork _unitOfWork;
        public ThamDinhLapQuanLyDauTuService(IThamDinhLapQuanLyDauTuRepository thamDinhLapQuanLyDauTuRepository,
          IUnitOfWork unitOfWork)
        {
            this._thamDinhLapQuanLyDauTuRepository = thamDinhLapQuanLyDauTuRepository;
            this._unitOfWork = unitOfWork;

        }

        public ThamDinhLapQuanLyDauTu Add(ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu)
        {
            _thamDinhLapQuanLyDauTuRepository.Add(thamDinhLapQuanLyDauTu);
           
            return thamDinhLapQuanLyDauTu;
        }

        public bool Delete(int id)
        {
            try
            {
                _thamDinhLapQuanLyDauTuRepository.Delete(id);
              
                return true;
            }
            catch
            {

            }
            return false;
           
        }

        public IEnumerable<ThamDinhLapQuanLyDauTu> GetAllForHistory()
        {
            return _thamDinhLapQuanLyDauTuRepository.GetAll();
        }

        public IEnumerable<ThamDinhLapQuanLyDauTu> GetByFilter(int id, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _thamDinhLapQuanLyDauTuRepository.GetByIdLapQuanLyDauTu(id);
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.Contains(filter));
            }
          
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLapQuanLyDauTu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ThamDinhLapQuanLyDauTu GetById(int idThamDinhLapquanLyDauTu)
        {
           ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu= _thamDinhLapQuanLyDauTuRepository.GetSingleById(idThamDinhLapquanLyDauTu);
            return thamDinhLapQuanLyDauTu;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ThamDinhLapQuanLyDauTu thamDinhLapQuanLyDauTu)
        {
            _thamDinhLapQuanLyDauTuRepository.Update(thamDinhLapQuanLyDauTu);
            
        }
    }
}
