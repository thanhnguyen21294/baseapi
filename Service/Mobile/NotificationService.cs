﻿using Data.Infrastructure;
using Data.Repositories.Mobile;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;

namespace Service.Mobile
{
    public interface INotificationService
    {
        IEnumerable<Notification> GetAll();
        Notification Add(Notification noti);
        Notification Update(Notification noti);
        Notification Delete(int Id);
        IEnumerable<Notification> GetNotiByUserId(int UserId);
        void sendNotificationToAllUser(Notification notification, List<string> listUserId, string idUserCurrent, string topic);
        void SendNotiToListUser(List<string> listUserId, string title, string body, string createdBy);
        void sendNotificationToAllUserMessger(object ItemChat, string IdUser1, string IdUser2, string NoiDung, string[] listTokenDevice, string notification, List<string> listUserId, string idUserCurrent, string topic);
        void sendNotificationToAllUserMessgerNhom(object ItemChat, string IdUser1, int IdUser2, string NoiDung, string[] listTokenDevice, string notification, List<string> listUserId, string idUserCurrent, string topic);
        void Save();
    }

    public class NotificationService : INotificationService
    {
        private INotificationRepository _INotificationRepository;
        private INotification_UserService _Inotification_UserService;
        private IDevice_TokenService _DeviceToKenService;
        private IUnitOfWork _IUnitOfWork;

        public NotificationService(
            INotificationRepository notificationRepository,
            IUnitOfWork unitOfWork,
            INotification_UserService notification_UserService,
            IDevice_TokenService deviceToKenService)
        {
            _DeviceToKenService = deviceToKenService;
            _Inotification_UserService = notification_UserService;
            _INotificationRepository = notificationRepository;
            _IUnitOfWork = unitOfWork;
        }

        public Notification Add(Notification noti)
        {
            return _INotificationRepository.Add(noti);
        }

        public Notification Delete(int Id)
        {
            return _INotificationRepository.Delete(Id);
        }

        public IEnumerable<Notification> GetAll()
        {
            return _INotificationRepository.GetAll();
        }

        public IEnumerable<Notification> GetNotiByUserId(int UserId)
        {
            throw new NotImplementedException();
        }

        public Notification Update(Notification noti)
        {
            throw new NotImplementedException();
        }

        public void sendNotificationToAllUser(Notification notification, List<string> listUserId, string idUserCurrent, string topic)
        {

            #region create notification and send all to user
            var notificationFCMService = new NotificationFcmService();
            var dataUserNoti = new Notification_User();
            if (notification != null)
            {
                var notification_User = new Notification_User();
                foreach (var userId in listUserId)
                {
                    notification_User.NotificationId = notification.Id;
                    notification_User.UserId = userId;
                    if (userId == idUserCurrent)
                    {
                        dataUserNoti = _Inotification_UserService.Add(notification_User);
                    }
                    else
                    {
                        _Inotification_UserService.Add(notification_User);
                    }
                    _Inotification_UserService.Save();
                }
            }
            var data = new
            {
                Id = dataUserNoti.Id,
                Title = dataUserNoti.Notification.Title,
                Body = dataUserNoti.Notification.Body,
                CreatedDate = dataUserNoti.Notification.CreatedDate,
                HasRead = dataUserNoti.HasRead
            };

            notificationFCMService.SendNotificationToAllDevice(notification.Title, notification.Body, data, topic);
            #endregion
        }

        public void SendNotiToListUser(List<string> listUserId, string title, string body, string createdBy)
        {
            NotificationFcmService fcmService = new NotificationFcmService();
            var listDeviceToKen = GetListDeviceToken(listUserId);
            if (listDeviceToKen.Count > 0)
            {
                listDeviceToKen.ForEach(item =>
                {
                    Notification notification = new Notification();
                    notification.CreatedBy = createdBy;
                    notification.CreatedDate = DateTime.Now;
                    notification.Title = title;
                    notification.Body = body;
                    Notification_User notification_User = new Notification_User();
                    var noti = _INotificationRepository.Add(notification);
                    Save();
                    notification_User.UserId = item.UserId;
                    notification_User.NotificationId = noti.Id;
                    _Inotification_UserService.Add(notification_User);
                    _Inotification_UserService.Save();
                    fcmService.SendNotification(title, body, item.TokenId,
                        new { UserRecivedId = item.UserId, UserSenderId = createdBy, Title = title, Body = body, CreatedDate = DateTime.Now, IsNotification = true });
                });
            }
        }


        public void sendNotificationToAllUserMessger(object ItemChat, string IdUser1, string IdUser2, string NoiDung, string[] listTokenDevice, string notification, List<string> listUserId, string idUserCurrent, string topic)
        {

            #region create notification and send all to user
            var notificationFCMService = new NotificationFcmService();
            var dataUserNoti = new Notification_User();
            //if (notification != null)
            //{
            //    var notification_User = new Notification_User();
            //    foreach (var userId in listUserId)
            //    {
            //        notification_User.NotificationId = notification.Id;
            //        notification_User.UserId = userId;
            //        dataUserNoti = _Inotification_UserService.Add(notification_User);
            //        //_Inotification_UserService.Save();
            //    }
            //}
            //var data = new
            //{
            //    Id = dataUserNoti.Id,
            //    Title = dataUserNoti.Notification.Title,
            //    Body = dataUserNoti.Notification.Body,
            //    CreatedDate = dataUserNoti.Notification.CreatedDate,
            //    HasRead = dataUserNoti.HasRead
            //};

            //notificationFCMService.AddMultiDeviceToGroupMessger(listTokenDevice, NoiDung);
            foreach (var item in listTokenDevice)
            {
                notificationFCMService.SendNotificationMessger(ItemChat, IdUser1, IdUser2, notification, NoiDung, "news", item);
            }
            //notificationFCMService.SendNotification(data.Title, NoiDung, "news");
            #endregion
        }
        public void sendNotificationToAllUserMessgerNhom(object ItemChat, string IdUser1, int IdUser2, string NoiDung, string[] listTokenDevice, string notification, List<string> listUserId, string idUserCurrent, string topic)
        {

            #region create notification and send all to user
            var notificationFCMService = new NotificationFcmService();
            var dataUserNoti = new Notification_User();
            //if (notification != null)
            //{
            //    var notification_User = new Notification_User();
            //    foreach (var userId in listUserId)
            //    {
            //        notification_User.NotificationId = notification.Id;
            //        notification_User.UserId = userId;
            //        dataUserNoti = _Inotification_UserService.Add(notification_User);
            //        _Inotification_UserService.Save();
            //    }
            //}
            //var data = new
            //{
            //    Id = dataUserNoti.Id,
            //    Title = dataUserNoti.Notification.Title,
            //    Body = dataUserNoti.Notification.Body,
            //    CreatedDate = dataUserNoti.Notification.CreatedDate,
            //    HasRead = dataUserNoti.HasRead
            //};

            //notificationFCMService.AddMultiDeviceToGroupMessger(listTokenDevice, NoiDung);
            foreach (var item in listTokenDevice)
            {
                notificationFCMService.SendNotificationMessgerNhom(ItemChat, IdUser1, IdUser2, notification, NoiDung, "news", item);
            }
            //notificationFCMService.SendNotification(data.Title, NoiDung, "news");
            #endregion
        }
        public void Save()
        {
            _IUnitOfWork.Commit();
        }

        private List<DeviceToken> GetListDeviceToken(List<string> listUserId)
        {
            List<DeviceToken> listDeviceToKen = new List<DeviceToken>();
            listUserId.ForEach(item =>
            {
                var tokenDevice = _DeviceToKenService.GetByUserId(item);
                if (tokenDevice != null)
                {
                    listDeviceToKen.Add(new DeviceToken(item, tokenDevice.Token));
                }
            });
            return listDeviceToKen;
        }
    }

    public class DeviceToken
    {

        public string TokenId { get; set; }
        public string UserId { get; set; }

        public DeviceToken(string userId, string tokenId)
        {
            TokenId = tokenId;
            UserId = userId;
        }
    }

}
