﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface INhomChatAppService
    {
        NhomChatApp Add(NhomChatApp NhomChatApp);

        void Update(NhomChatApp NhomChatApp);

        IEnumerable<NhomChatApp> GetByIdNhom(int id);

        IEnumerable<NhomChatApp> GetAll();

        IEnumerable<NhomChatApp> GetNhomChatByUser(string IdUser);

        NhomChatApp Delete(int id);

        NhomChatApp GetById(int id);
        void Save();
    }

    public class NhomChatAppService : INhomChatAppService
    {
        private INhomChatAppRepository _NhomChatAppRepository;
        private IUnitOfWork _unitOfWork;

        public NhomChatAppService(INhomChatAppRepository NhomChatAppRepository, IUnitOfWork unitOfWork)
        {
            this._NhomChatAppRepository = NhomChatAppRepository;
            this._unitOfWork = unitOfWork;
        }
        public NhomChatApp Add(NhomChatApp NhomChatApp)
        {
            return _NhomChatAppRepository.Add(NhomChatApp);
        }
        public IEnumerable<NhomChatApp> GetByIdNhom(int Id)
        {
            return _NhomChatAppRepository.GetMulti(x => x.Id == Id);
        }

        public NhomChatApp Delete(int id)
        {
            return _NhomChatAppRepository.Delete(id);
        }

        public IEnumerable<NhomChatApp> GetAll()
        {
            return _NhomChatAppRepository.GetAll();
        }
        public IEnumerable<NhomChatApp> GetNhomChatByUser(string IdUser)
        {
            return _NhomChatAppRepository.GetMulti(x => x.ListUser.Contains(IdUser));
        }
        public NhomChatApp GetById(int id)
        {
            return _NhomChatAppRepository.GetSingleByCondition(x => x.Id == id);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NhomChatApp ChatApp)
        {
            _NhomChatAppRepository.Update(ChatApp);
        }
    }
}
