﻿using Data.Infrastructure;
using Data.Repositories.Mobile;
using Model.Models.Mobile;
using System.Collections.Generic;
using System.Linq;

namespace Service.Mobile
{
    public interface INotification_UserService
    {
        IEnumerable<Notification_User> GetAllNotiByUserId(string UserId, out int total, int pageCurrent, int itemPerpage, string filter);
        IEnumerable<Notification_User> GetAllByUserId(string UserId);
        Notification_User Add(Notification_User notification_User);
        void Update(Notification_User notification_User);
        Notification_User Delete(int Id);
        Notification_User GetById(int Id);
        int GetAmoutMessageNotRead(string UserId);
        Notification_User GetLastestMessage(string UserId);
        List<int> GetAllNotiByUserIdNoPage(string UserId);
        void Save();
    }

    public class Notification_UserService : INotification_UserService
    {
        private INotification_UserRepository _INotification_UserRepository;
        private IUnitOfWork _IunitOfWork;

        public Notification_UserService(IUnitOfWork unitOfWork, INotification_UserRepository notification_UserRepository)
        {
            _INotification_UserRepository = notification_UserRepository;
            _IunitOfWork = unitOfWork;
        }
        public List<int> GetAllNotiByUserIdNoPage (string UserID)
        {
            List<int> ListIdNoti = _INotification_UserRepository.GetMulti(x => x.UserId == UserID).Select(y => y.Id).ToList();
            return ListIdNoti;


        }
        public IEnumerable<Notification_User> GetAllNotiByUserId(string UserId, out int total, int pageCurrent, int itemPerpage, string filter)
        {
            var listUserNoti = _INotification_UserRepository.GetMulti(x => x.UserId == UserId, new string[] { "Notification", "Notification.AppUser" });
            if (filter != "null")
            {
                listUserNoti = listUserNoti.Where(x => x.Notification.Body.Contains(filter) || x.Notification.Title.Contains(filter) || x.Notification.AppUser.UserName.Contains(filter));
            }
            total = listUserNoti.Count() / itemPerpage;
            if ((listUserNoti.Count() % itemPerpage) > 0)
            {
                total++;
            }

            return listUserNoti.OrderByDescending(x => x.Notification.CreatedDate).Skip((pageCurrent - 1) * itemPerpage).Take(itemPerpage);
        }

        public int GetAmoutMessageNotRead(string UserId)
        {
            return _INotification_UserRepository.Count(x => !x.HasRead && x.UserId == UserId);
        }

        public Notification_User GetLastestMessage(string UserId)
        {
            return _INotification_UserRepository.GetMulti(x => x.UserId == UserId, new string[] { "Notification", "Notification.AppUser" })
                .OrderByDescending(x => x.Notification.CreatedDate).FirstOrDefault();
        }

        public IEnumerable<Notification_User> GetAllByUserId(string UserId)
        {
            return _INotification_UserRepository.GetMulti(x => x.UserId == UserId);
        }

        public Notification_User Add(Notification_User notification_User)
        {
            return _INotification_UserRepository.Add(notification_User);
        }

        public void Update(Notification_User notification_User)
        {
            _INotification_UserRepository.Update(notification_User);
        }

        public Notification_User Delete(int Id)
        {
            return _INotification_UserRepository.Delete(Id);
        }

        public Notification_User GetById(int Id)
        {
            return _INotification_UserRepository.GetSingleByCondition(x => x.Id == Id, new string[] { "Notification" });
        }

        public void Save()
        {
            _IunitOfWork.Commit();
        }
    }
}
