﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Service.Mobile
{
    public class NotificationFcmService
    {
        public NotificationFcmService()
        {
        }

        public bool Successful
        {
            get;
            set;
        }

        public string Response
        {
            get;
            set;
        }
        public Exception Error
        {
            get;
            set;
        }

        public NotificationFcmService SendNotification(string _title, string _message, string _tokenDevice, object dataObj)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";
                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    to = _tokenDevice,
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        showWhenInForeground = true
                        //icon="myicon"
                    },
                    data = dataObj,
                    priority = "High"
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService CreatingDeviceGroup(string deviceGroupName)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("project_id:{0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    operation = "create",
                    notification_key_name = deviceGroupName,
                    registration_ids = new string[] { "c7e2nle7YXE:APA91bGm6zuTJJQIhsf70AMsgW2AQrPCVJ0b_xgFzLKy0lR7YkuW75hIcXkMAkDMAjpMnaDpzleWwC_0qAbdsVzkRwElPCVwrTc9mVYZidj6ysMWh0DdzFHxq8LwScP3kMx-Uxt6slgv" }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService GetKeyGroup(string deviceGroupName)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification?notification_key_name=" + deviceGroupName;

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "GET";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("project_id:{0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new { };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }


        public NotificationFcmService AddDeviceToGroup(string _group_Push_Qldada, string _notification_key, string deviceToken)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    Operation = "add",
                    notification_key_name = _group_Push_Qldada,
                    notification_key = _notification_key,
                    registration_ids = new string[1] { deviceToken }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService AddMultiDeviceToGroup(string notificationKeyName, string[] arrayDeviceToken)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    Operation = "add",
                    notification_key_name = notificationKeyName,
                    notification_key = "notification_key",
                    registration_ids = arrayDeviceToken
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }


        public NotificationFcmService RemoveDeviceToGroup(string _title, string _message, string _topic)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    Operation = "remove",
                    notification_key_name = "group_Push_Qldada",
                    notification_key = "notification_key",
                    registration_ids = new string[1] { "1" }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService SendNotificationToGroup(string _title, string _message, string grouKey)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    to = grouKey,
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        //icon="myicon"
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService SendNotificationToAllDevice(string _title, string _message, object obj, string _topic)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    to = "/topics/" + _topic,
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        data = obj,
                        showWhenInForeground = true,
                        icon = System.Web.HttpContext.Current.Server.MapPath("~" + "UploadedFiles/Mobile/icon/icon.png"),
                        sound = "default",
                        badge = "1"
                    },
                    priority = "High"
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService CreatingDeviceGroupMessger(List<string> listTokenDevice, string deviceGroupName)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("project_id:{0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    priority = "High",
                    operation = "Tin nhắn mới",
                    notification_key_name = deviceGroupName,
                    registration_ids = listTokenDevice

                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService AddMultiDeviceToGroupMessger(string[] arrayDeviceToken, string notificationKeyName)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/notification";

                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    Operation = "add",
                    notification_key_name = notificationKeyName,
                    notification_key = "notification_key",
                    registration_ids = new string[] { "d0GxL1wPEkk:APA91bH3ToWeXLlnU0281i-mjPjS2vKnjTg4OXPP6LzigS-o00q4p4uwP3f7qMuIybS8QOxnqPtKg5KU9RtC_a_B77ONYWybq2EJVBkJgFKnvsa1T0CYc6JhpqQksyEILLVdVBG-YP-9" }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }

        public NotificationFcmService SendNotificationMessger(object ItemChat, string _IdUser1, string _IdUser2, string _title, string _message, string _topic, string tokendevice)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";
                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";
                var data = new
                {
                    to = tokendevice, // Uncoment this if you want to test for single device
                    //to = "/topics/" + _topic, // this is for topic 
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        showWhenInForeground = true,
                        notification_key_name = _IdUser1,
                        notification_key = _IdUser2,
                    },
                    data = ItemChat,
                    priority = "High"
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }
        public NotificationFcmService SendNotificationMessgerNhom(object ItemChat, string _IdUser1, int _IdUser2, string _title, string _message, string _topic, string tokendevice)
        {
            NotificationFcmService result = new NotificationFcmService();
            try
            {
                string FCMKey = ConfigurationManager.AppSettings["FCMKey"];
                string FCMSenderId = ConfigurationManager.AppSettings["FCMSenderId"];
                result.Successful = true;
                result.Error = null;
                // var value = message;
                var requestUri = "https://fcm.googleapis.com/fcm/send";
                WebRequest webRequest = WebRequest.Create(requestUri);
                webRequest.Method = "POST";
                webRequest.Headers.Add(string.Format("Authorization: key={0}", FCMKey));
                webRequest.Headers.Add(string.Format("Sender: id={0}", FCMSenderId));
                webRequest.ContentType = "application/json";

                var data = new
                {
                    to = tokendevice, // Uncoment this if you want to test for single device
                    //to = "/topics/" + _topic, // this is for topic 
                    notification = new
                    {
                        title = _title,
                        body = _message,
                        showWhenInForeground = true,
                        notification_key_name = _IdUser1,
                        notification_key = _IdUser2,
                    },
                    data = ItemChat,
                    priority = "High"
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                webRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse webResponse = webRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = webResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                result.Response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Successful = false;
                result.Response = null;
                result.Error = ex;
            }
            return result;
        }
    }
}