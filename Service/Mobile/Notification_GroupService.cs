﻿using Data.Infrastructure;
using Data.Repositories.Mobile;
using Model.Models.Mobile;

namespace Service.Mobile
{
    public interface INotification_GroupService
    {
        Notification_Group Add(Notification_Group Notification_Group);
        void Update(Notification_Group Notification_Group);
        Notification_Group Delte(int Id);
        string GetNotificationKeyByName(string Name);
        Notification_Group GetById(int Id);
        void Save();
    }

    public class Notification_GroupService : INotification_GroupService
    {
        private INotification_GroupRepository _INotification_GroupRepository;
        private IUnitOfWork _IunitOfWork;

        public Notification_GroupService(IUnitOfWork unitOfWork, INotification_GroupRepository Notification_GroupRepository)
        {
            _INotification_GroupRepository = Notification_GroupRepository;
            _IunitOfWork = unitOfWork;
        }

        public Notification_Group Add(Notification_Group Notification_Group)
        {
            return _INotification_GroupRepository.Add(Notification_Group);
        }

        public void Update(Notification_Group Notification_Group)
        {
            _INotification_GroupRepository.Update(Notification_Group);
        }

        public Notification_Group Delte(int Id)
        {
            return _INotification_GroupRepository.Delete(Id);
        }

        public string GetNotificationKeyByName(string Name)
        {
            return _INotification_GroupRepository.GetSingleByCondition(x => x.Notification_Key_Name == Name).Notification_Key;
        }

        public Notification_Group GetById(int Id)
        {
            return _INotification_GroupRepository.GetSingleById(Id);
        }

        public void Save()
        {
            _IunitOfWork.Commit();
        }
    }
}
