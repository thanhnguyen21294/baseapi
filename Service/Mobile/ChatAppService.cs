﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Data.View.Mobile;

namespace Service
{
    public interface IChatAppService
    {
        ChatApp Add(ChatApp ChatApp);

        void Update(ChatApp ChatApp);

        IEnumerable<ChatApp> GetByIdDuAn(int idDA);

        IEnumerable<ChatApp> GetAll();

        IEnumerable<MessageViewModel> GetByChatByIDUser(string IdUser1, string IdUser2, out int totalItems, int pageIndex, int pageSize);

        ChatApp Delete(int id);

        void Save();
    }

    public class ChatAppService : IChatAppService
    {
        private IChatAppRepository _ChatAppRepository;
        private IUnitOfWork _unitOfWork;

        public ChatAppService(IChatAppRepository ChatAppRepository, IUnitOfWork unitOfWork)
        {
            this._ChatAppRepository = ChatAppRepository;
            this._unitOfWork = unitOfWork;
        }
        public ChatApp Add(ChatApp ChatApp)
        {
            return _ChatAppRepository.Add(ChatApp);
        }
        public IEnumerable<ChatApp> GetByIdDuAn(int Id)
        {
            return _ChatAppRepository.GetMulti(x => x.Id == Id, new string[] { "AppUsers" });
        }

        public ChatApp Delete(int id)
        {
            return _ChatAppRepository.Delete(id);
        }

        public IEnumerable<ChatApp> GetAll()
        {
            return _ChatAppRepository.GetAll();
        }

        public IEnumerable<MessageViewModel> GetByChatByIDUser(string IdUser1, string IdUser2, out int totalItems, int pageIndex, int pageSize)
        {
            var listMsg = _ChatAppRepository.GetMulti(x =>
            (x.IdUser1 == IdUser1 && x.IdUser2 == IdUser2) || (x.IdUser1 == IdUser2 && x.IdUser2 == IdUser1),
            new string[] { "AppUser1", "AppUser2" }).OrderByDescending(z => z.CreatedDate)
               .Select(msg => new MessageViewModel(
                    msg.Id,
                    msg.IdUser1,
                    msg.IdUser2,
                    msg.NoiDung,
                     msg.IsFile,
                    msg.CreatedDate,
                    new UserChatViewModel(msg.IdUser1, msg.AppUser1.Avatar, msg.AppUser1.FullName),
                     new UserChatViewModel(msg.IdUser2, msg.AppUser2.Avatar, msg.AppUser2.FullName)
                    ));
            totalItems = listMsg.Count();
            return listMsg.Skip((pageIndex - 1) * pageSize).Take(pageSize).OrderBy(z => z.NgayTao);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ChatApp ChatApp)
        {
            _ChatAppRepository.Update(ChatApp);
        }
    }
}
