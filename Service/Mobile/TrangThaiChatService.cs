﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ITrangThaiChatService
    {
        TrangThaiChat Add(TrangThaiChat TrangThaiChat);

        void Update(TrangThaiChat TrangThaiChat);

        IEnumerable<TrangThaiChat> GetAll();

        IEnumerable<TrangThaiChat> GetByChatByIDUser(string IdUser1, string IdUser2);

        List<string> GetListUserIdByNhomChatId(string IdNhomChat);

        TrangThaiChat Delete(int id);

        TrangThaiChat GetChatByUserId1AndUserId2(string IdUser1, string IdUser2);

        List<TrangThaiChat> GetListTrangThaiChatByNhomChatId(string IdNhomChat);

        List<string> GetListTrangThaiChatByIdUser(string IdUser);

        void Save();
    }

    public class TrangThaiChatService : ITrangThaiChatService
    {
        private ITrangThaiChatRepository _TrangThaiChatRepository;
        private IUnitOfWork _unitOfWork;

        public TrangThaiChatService(ITrangThaiChatRepository TrangThaiChatRepository, IUnitOfWork unitOfWork)
        {
            this._TrangThaiChatRepository = TrangThaiChatRepository;
            this._unitOfWork = unitOfWork;
        }
        public TrangThaiChat Add(TrangThaiChat TrangThaiChat)
        {
            return _TrangThaiChatRepository.Add(TrangThaiChat);
        }
        public IEnumerable<TrangThaiChat> GetByIdDuAn(int Id)
        {
            return _TrangThaiChatRepository.GetMulti(x => x.Id == Id, new string[] { "AppUsers" });
        }

        public TrangThaiChat Delete(int id)
        {
            return _TrangThaiChatRepository.Delete(id);
        }

        public IEnumerable<TrangThaiChat> GetAll()
        {
            return _TrangThaiChatRepository.GetAll();
        }

        public IEnumerable<TrangThaiChat> GetByChatByIDUser(string IdUser1, string IdUser2)
        {
            return _TrangThaiChatRepository.GetMulti(x => 
            (x.IdUser1 == IdUser1 && x.IdUser2 == IdUser2) 
            || (x.IdUser1 == IdUser2 && x.IdUser2 == IdUser1), 
            new string[] { "AppUser1", "AppUser2" });
        }

        public TrangThaiChat GetChatByUserId1AndUserId2(string IdUser1, string IdUser2)
        {
            return _TrangThaiChatRepository.GetSingleByCondition(x => x.IdUser1 == IdUser1 && x.IdUser2 == IdUser2);
        }

        public List<TrangThaiChat> GetListTrangThaiChatByNhomChatId(string IdNhomChat)
        {

            return _TrangThaiChatRepository.GetMulti(x => x.IdUser2 == IdNhomChat && x.IdNhom).ToList();
        }
        public List<string> GetListTrangThaiChatByIdUser(string IdUser)
        {
            return _TrangThaiChatRepository.GetMulti(x => x.IdUser1 == IdUser && !x.TrangThai).Select(b => b.IdUser2).ToList();
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(TrangThaiChat TrangThaiChat)
        {
            _TrangThaiChatRepository.Update(TrangThaiChat);
        }

        public List<string> GetListUserIdByNhomChatId(string IdNhomChat)
        {
            return _TrangThaiChatRepository.GetMulti(x => x.IdUser2 == IdNhomChat && x.IdNhom)
                .Select(sl => sl.IdUser1).ToList();
        }
    }
}
