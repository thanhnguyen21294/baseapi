﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Data.View.Mobile;

namespace Service
{
    public interface IChatNhomAppService
    {
        ChatNhomApp Add(ChatNhomApp ChatNhomApp);

        void Update(ChatNhomApp ChatNhomApp);

        IEnumerable<ChatNhomApp> GetByIdDuAn(int idDA);

        IEnumerable<ChatNhomApp> GetAll();

        IEnumerable<object> GetByChatByIDUser(string IdUser1, int IdUser2, out int totalItems, int pageIndex, int pageSize);

        ChatNhomApp Delete(int id);

        void Save();
    }

    public class ChatNhomAppService : IChatNhomAppService
    {
        private IChatNhomAppRepository _ChatNhomAppRepository;
        private IUnitOfWork _unitOfWork;

        public ChatNhomAppService(IChatNhomAppRepository ChatNhomAppRepository, IUnitOfWork unitOfWork)
        {
            this._ChatNhomAppRepository = ChatNhomAppRepository;
            this._unitOfWork = unitOfWork;
        }
        public ChatNhomApp Add(ChatNhomApp ChatNhomApp)
        {
            return _ChatNhomAppRepository.Add(ChatNhomApp);
        }
        public IEnumerable<ChatNhomApp> GetByIdDuAn(int Id)
        {
            return _ChatNhomAppRepository.GetMulti(x => x.Id == Id, new string[] { "AppUsers" });
        }

        public ChatNhomApp Delete(int id)
        {
            return _ChatNhomAppRepository.Delete(id);
        }

        public IEnumerable<ChatNhomApp> GetAll()
        {
            return _ChatNhomAppRepository.GetAll();
        }

        public IEnumerable<object> GetByChatByIDUser(string IdUser1, int IdUser2, out int totalItems, int pageIndex, int pageSize)
        {
            var listMsg = _ChatNhomAppRepository.GetMulti(x =>
            x.IdUser2 == IdUser2, new string[] { "AppUser1", "AppUser2" }).OrderByDescending(x => x.CreatedDate)
            .Select(msg => new MessageViewModel(
                    msg.Id,
                    msg.IdUser1,
                    msg.IdUser2.ToString(),
                    msg.NoiDung,
                     msg.IsFile,
                    msg.CreatedDate,
                    new UserChatViewModel(msg.IdUser1, msg.AppUser1.Avatar, msg.AppUser1.FullName),
                     new UserChatViewModel(msg.IdUser2.ToString(), "", msg.AppUser2.TenNhomChat)
                    ));
            //.Select(sl => new
            //{
            //    Date = sl.CreatedDate.ToString().Substring(0, sl.CreatedDate.ToString().IndexOf(" ")).ToString(),
            //    Sl = sl
            //}).GroupBy(sl => sl.Date).Select(slD => new
            //{
            //    Date = slD.Key,
            //    Messages = slD.Select(msg => new MessageViewModel(
            //        msg.Sl.Id,
            //        msg.Sl.IdUser1,
            //        msg.Sl.IdUser2.ToString(),
            //        msg.Sl.NoiDung,
            //         msg.Sl.IsFile,
            //        msg.Sl.CreatedDate,
            //        new UserChatViewModel(msg.Sl.IdUser1, msg.Sl.AppUser1.Avatar, msg.Sl.AppUser1.FullName),
            //         new UserChatViewModel(msg.Sl.IdUser2.ToString(), "", msg.Sl.AppUser2.TenNhomChat)
            //        ))
            //});
            totalItems = listMsg.Count();
            return listMsg.Skip((pageIndex - 1) * pageSize).Take(pageSize).OrderBy(x => x.NgayTao);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ChatNhomApp ChatNhomApp)
        {
            _ChatNhomAppRepository.Update(ChatNhomApp);
        }
    }
}
