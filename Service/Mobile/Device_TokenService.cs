﻿using Data.Infrastructure;
using Data.Repositories.Mobile;
using Model.Models.Mobile;
using System.Collections.Generic;
using System.Linq;

namespace Service.Mobile
{
    public interface IDevice_TokenService
    {
        Device_Token Add(Device_Token deviceToken);
        Device_Token Delete(int Id);
        IEnumerable<Device_Token> GetAll();
        Device_Token GetByUserId(string userId);
        void Save();
    }

    public class Device_TokenService : IDevice_TokenService
    {
        private IDevice_TokenRepository _IDevice_TokenRepository;
        private IUnitOfWork _IUnitOfWork;

        public Device_TokenService(IDevice_TokenRepository device_TokenRepository, IUnitOfWork unitOfWork)
        {
            _IDevice_TokenRepository = device_TokenRepository;
            _IUnitOfWork = unitOfWork;
        }

        public Device_Token Add(Device_Token deviceToken)
        {
            return _IDevice_TokenRepository.Add(deviceToken);
        }

        public Device_Token Delete(int Id)
        {
            return _IDevice_TokenRepository.Delete(Id);
        }

        public IEnumerable<Device_Token> GetAll()
        {
            return _IDevice_TokenRepository.GetAll();
        }

        public Device_Token GetByUserId(string userId)
        {
            return _IDevice_TokenRepository.GetMulti(x => x.UserId == userId).LastOrDefault();
        }

        public void Save()
        {
            _IUnitOfWork.Commit();
        }
    }
}
