﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public interface ILoaiChiPhiService
    {
        LoaiChiPhi Add(LoaiChiPhi LoaiChiPhi);

        void Update(LoaiChiPhi LoaiChiPhi);

        LoaiChiPhi Delete(int id);

        IEnumerable<LoaiChiPhi> GetAll();

        IEnumerable<LoaiChiPhi> GetByFilter(int page, int pageSize, out int totalRow);

        LoaiChiPhi GetById(int id);

        void Save();
    }

    public class LoaiChiPhiService : ILoaiChiPhiService
    {
        private ILoaiChiPhiRepository _LoaiChiPhiRepository;
        private IUnitOfWork _unitOfWork;

        public LoaiChiPhiService(ILoaiChiPhiRepository LoaiChiPhiRepository, IUnitOfWork unitOfWork)
        {
            this._LoaiChiPhiRepository = LoaiChiPhiRepository;
            this._unitOfWork = unitOfWork;
        }
        public LoaiChiPhi Add(LoaiChiPhi LoaiChiPhi)
        {
            return _LoaiChiPhiRepository.Add(LoaiChiPhi);
        }

        public LoaiChiPhi Delete(int id)
        {
            return _LoaiChiPhiRepository.Delete(id);
        }

        public IEnumerable<LoaiChiPhi> GetAll()
        {
            return _LoaiChiPhiRepository.GetAll();
        }

        public IEnumerable<LoaiChiPhi> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _LoaiChiPhiRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLoaiChiPhi).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LoaiChiPhi GetById(int id)
        {
            return _LoaiChiPhiRepository.GetMulti(x => x.IdLoaiChiPhi == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LoaiChiPhi LoaiChiPhi)
        {
            _LoaiChiPhiRepository.Update(LoaiChiPhi);
        }
    }
}
