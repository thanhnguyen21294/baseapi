﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System.Linq;

namespace Service
{
    public interface IDeleteService
    {
        NguonVonGoiThauThanhToan DeleteNguonVonGoiThauThanhToan(int id);
        ThanhToanHopDong DeleteThanhToanHopDong(int id);
        ThucHienHopDong DeleteThucHienHopDong(int id);
        PhuLucHopDong DeletePhuLucHopDong(int id);
        NguonVonGoiThauTamUng DeleteNguonVonGoiThauTamUng(int id);
        NguonVonDuAnGoiThau DeleteNguonVonDuAnGoiThau(int id);
        TamUngHopDong DeleteTamUngHopDong(int id);
        TienDoChiTiet DeleteTienDoChiTiet(int id);
        TienDoThucHien DeleteTienDoThucHien(int id);
        QuyetToanHopDong DeleteQuyetToanHopDong(int id);
        HopDong DeleteHopDong(int id);
        NguonVonThanhToanChiPhiKhac DeleteNguonVonThanhToanChiPhiKhac(int id);
        ThanhToanChiPhiKhac DeleteThanhToanChiPhiKhac(int id);
        KetQuaDauThau DeleteKetQuaDauThau(int id);
        DanhMucNhaThau DeleteDanhMucNhaThau(int id);
        HoSoMoiThau DeleteHoSoMoiThau(int id);
        GoiThau DeleteGoiThau(int id);
        NguonVonKeHoachVon DeleteNguonVonKeHoachVon(int id);
        NguonVonDuAn DeleteNguonVonDuAn(int id);
        KeHoachVon DeleteKeHoachVon(int id);
        ChuTruongDauTu DeleteChuTruongDauTu(int id);
        KeHoachLuaChonNhaThau DeleteKeHoachLuaChonNhaThau(int id);
        NguonVonQuyetToanDuAn DeleteNguonVonQuyetToanDuAn(int id);
        QuyetToanDuAn DeleteQuyetToanDuAn(int id);
        ThamDinhLapQuanLyDauTu DeleteThamDinhLapQuanLyDauTu(int id);
        LapQuanLyDauTu DeleteLapQuanLyDauTu(int id);
        VanBanDuAn DeleteVanBanDuAn(int id);
        DuAn DeleteDuAn(int id);
        NguonVon DeleteNguonVon(int id);
        void Save();
    }
    public class DeleteService : IDeleteService
    {
        private IUnitOfWork _unitOfWork;
        private INguonVonGoiThauThanhToanRepository _nguonVonGoiThauThanhToanRepository;
        private IThanhToanHopDongRepository _thanhToanHopDongRepository;
        private IThucHienHopDongRepository _thucHienHopDongRepository;
        private IPhuLucHopDongRepository _phuLucHopDongRepository;
        private INguonVonGoiThauTamUngRepository _nguonVonGoiThauTamUngRepository;
        private INguonVonDuAnGoiThauRepository _nguonVonDuAnGoiThauRepository;
        private ITamUngHopDongRepository _tamUngHopDongRepository;
        private ITienDoChiTietRepository _tienDoChiTietRepository;
        private ITienDoThucHienRepository _tienDoThucHienRepository;
        private IQuyetToanHopDongRepository _quyetToanHopDongRepository;
        private IHopDongRepository _hopDongRepository;
        private INguonVonThanhToanChiPhiKhacRepository _nguonVonThanhToanChiPhiKhacRepository;
        private IThanhToanChiPhiKhacRepository _thanhToanChiPhiKhacRepository;
        private IKetQuaDauThauRepository _ketQuaDauThauRepository;
        private IDanhMucNhaThauRepository _danhMucNhaThauRepository;
        private IHoSoMoiThauRepository _hoSoMoiThauRepository;
        private IGoiThauRepository _goiThauRepository;
        private INguonVonKeHoachVonRepository _nguonVonKeHoachVonRepository;
        private INguonVonDuAnRepository _nguonVonDuAnRepository;
        private IKeHoachVonRepository _keHoachVonRepository;
        private IChuTruongDauTuRepository _chuTruongDauTuRepository;
        private IKeHoachLuaChonNhaThauRepository _keHoachLuaChonNhaThauRepository;
        private INguonVonQuyetToanDuAnRepository _nguonVonQuyetToanDuAnRepository;
        private IQuyetToanDuAnRepository _quyetToanDuAnRepository;
        private IThamDinhLapQuanLyDauTuRepository _thamDinhLapQuanLyDauTuRepository;
        private ILapQuanLyDauTuRepository _lapQuanLyDauTuRepository;
        private IVanBanDuAnRepository _vanBanDuAnRepository;
        private IDuAnRepository _duAnRepository;
        private INguonVonRepository _nguonVonRepository;

        public DeleteService(IUnitOfWork unitOfWork, INguonVonGoiThauThanhToanRepository nguonVonGoiThauThanhToanRepository,IThanhToanHopDongRepository thanhToanHopDongRepository, IThucHienHopDongRepository thucHienHopDongRepository,IPhuLucHopDongRepository phuLucHopDongRepository,INguonVonGoiThauTamUngRepository nguonVonGoiThauTamUngRepository,INguonVonDuAnGoiThauRepository nguonVonDuAnGoiThauRepository,ITamUngHopDongRepository tamUngHopDongRepository,ITienDoChiTietRepository tienDoChiTietRepository,ITienDoThucHienRepository tienDoThucHienRepository,IQuyetToanHopDongRepository quyetToanHopDongRepository,IHopDongRepository hopDongRepository,INguonVonThanhToanChiPhiKhacRepository nguonVonThanhToanChiPhiKhacRepository,IThanhToanChiPhiKhacRepository thanhToanChiPhiKhacRepository,IKetQuaDauThauRepository ketQuaDauThauRepository,IDanhMucNhaThauRepository danhMucNhaThauRepository,IHoSoMoiThauRepository hoSoMoiThauRepository,IGoiThauRepository goiThauRepository,INguonVonKeHoachVonRepository nguonVonKeHoachVonRepository,INguonVonDuAnRepository nguonVonDuAnRepository,IKeHoachVonRepository keHoachVonRepository,IChuTruongDauTuRepository chuTruongDauTuRepository,IKeHoachLuaChonNhaThauRepository keHoachLuaChonNhaThauRepository,INguonVonQuyetToanDuAnRepository nguonVonQuyetToanDuAnRepository,IQuyetToanDuAnRepository quyetToanDuAnRepository,IThamDinhLapQuanLyDauTuRepository thamDinhLapQuanLyDauTuRepository,ILapQuanLyDauTuRepository lapQuanLyDauTuRepository,IVanBanDuAnRepository vanBanDuAnRepository,IDuAnRepository duAnRepository, INguonVonRepository nguonVonRepository)
        {
            this._unitOfWork = unitOfWork;
            this._nguonVonGoiThauThanhToanRepository = nguonVonGoiThauThanhToanRepository;
            this._thanhToanHopDongRepository = thanhToanHopDongRepository;
            this._thucHienHopDongRepository = thucHienHopDongRepository;
            this._phuLucHopDongRepository = phuLucHopDongRepository;
            this._nguonVonGoiThauTamUngRepository = nguonVonGoiThauTamUngRepository;
            this._nguonVonDuAnGoiThauRepository = nguonVonDuAnGoiThauRepository;
            this._tamUngHopDongRepository = tamUngHopDongRepository;
            this._tienDoChiTietRepository = tienDoChiTietRepository;
            this._tienDoThucHienRepository = tienDoThucHienRepository;
            this._quyetToanHopDongRepository = quyetToanHopDongRepository;
            this._hopDongRepository = hopDongRepository;
            this._nguonVonThanhToanChiPhiKhacRepository = nguonVonThanhToanChiPhiKhacRepository;
            this._thanhToanChiPhiKhacRepository = thanhToanChiPhiKhacRepository;
            this._ketQuaDauThauRepository = ketQuaDauThauRepository;
            this._danhMucNhaThauRepository = danhMucNhaThauRepository;
            this._hoSoMoiThauRepository = hoSoMoiThauRepository;
            this._goiThauRepository = goiThauRepository;
            this._nguonVonKeHoachVonRepository = nguonVonKeHoachVonRepository;
            this._nguonVonDuAnRepository = nguonVonDuAnRepository;
            this._keHoachVonRepository = keHoachVonRepository;
            this._chuTruongDauTuRepository = chuTruongDauTuRepository;
            this._keHoachLuaChonNhaThauRepository = keHoachLuaChonNhaThauRepository;
            this._nguonVonQuyetToanDuAnRepository = nguonVonQuyetToanDuAnRepository;
            this._quyetToanDuAnRepository = quyetToanDuAnRepository;
            this._thamDinhLapQuanLyDauTuRepository = thamDinhLapQuanLyDauTuRepository;
            this._lapQuanLyDauTuRepository = lapQuanLyDauTuRepository;
            this._vanBanDuAnRepository = vanBanDuAnRepository;
            this._duAnRepository = duAnRepository;
            this._nguonVonRepository = nguonVonRepository;
        }

        public NguonVonGoiThauThanhToan DeleteNguonVonGoiThauThanhToan(int id)
        {
            return _nguonVonGoiThauThanhToanRepository.Delete(id);
        }

        public ThanhToanHopDong DeleteThanhToanHopDong(int id)
        {
            var query = _nguonVonGoiThauThanhToanRepository.GetAll().Where(x => x.IdThanhToanHopDong == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteNguonVonGoiThauThanhToan(item.IdNguonVonGoiThauThanhToan);
                }
            }
            return _thanhToanHopDongRepository.Delete(id);
        }

        public ThucHienHopDong DeleteThucHienHopDong(int id)
        {
            var query = _thanhToanHopDongRepository.GetAll().Where(x => x.IdThucHien == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteThanhToanHopDong(item.IdThanhToanHopDong);
                }
            }
            return _thucHienHopDongRepository.Delete(id);
        }

        public PhuLucHopDong DeletePhuLucHopDong(int id)
        {
            return _phuLucHopDongRepository.Delete(id);
        }

        public NguonVonGoiThauTamUng DeleteNguonVonGoiThauTamUng(int id)
        {
            return _nguonVonGoiThauTamUngRepository.Delete(id);
        }

        public NguonVonDuAnGoiThau DeleteNguonVonDuAnGoiThau(int id)
        {
            var query1 = _nguonVonGoiThauThanhToanRepository.GetAll().Where(x => x.IdNguonVonDuAnGoiThau == id);
            if (query1.Count() > 0)
            {
                foreach (var item in query1)
                {
                    this.DeleteNguonVonGoiThauThanhToan(item.IdNguonVonGoiThauThanhToan);
                }
            }
            var query2 = _nguonVonGoiThauTamUngRepository.GetAll().Where(y => y.IdNguonVonDuAnGoiThau == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteNguonVonGoiThauTamUng(item2.IdNguonVonGoiThauTamUng);
                }
            }
            return _nguonVonDuAnGoiThauRepository.Delete(id);
        }

        public TamUngHopDong DeleteTamUngHopDong(int id)
        {
            var query = _nguonVonGoiThauTamUngRepository.GetAll().Where(x => x.IdTamUng == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteNguonVonGoiThauTamUng(item.IdNguonVonGoiThauTamUng);
                }
            }
            return _tamUngHopDongRepository.Delete(id);
        }

        public TienDoChiTiet DeleteTienDoChiTiet(int id)
        {
            return _tienDoChiTietRepository.Delete(id);
        }

        public TienDoThucHien DeleteTienDoThucHien(int id)
        {
            var query = _tienDoChiTietRepository.GetAll().Where(x => x.IdTienDo == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteTienDoChiTiet(item.IdTienDoChiTiet);
                }
            }
            return _tienDoThucHienRepository.Delete(id);
        }

        public QuyetToanHopDong DeleteQuyetToanHopDong(int id)
        {
            return _quyetToanHopDongRepository.Delete(id);
        }

        public HopDong DeleteHopDong(int id)
        {
            var query1 = _phuLucHopDongRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeletePhuLucHopDong(item1.IdPhuLuc);
                }
            }
            var query2 = _thucHienHopDongRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteThucHienHopDong(item2.IdThucHien);
                }
            }
            var query3 = _tamUngHopDongRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query3.Count() > 0)
            {
                foreach (var item3 in query3)
                {
                    this.DeleteTamUngHopDong(item3.IdTamUng);
                }
            }
            var query4 = _thanhToanHopDongRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query4.Count() > 0)
            {
                foreach (var item4 in query4)
                {
                    this.DeleteThanhToanHopDong(item4.IdThanhToanHopDong);
                }
            }
            var query5 = _tienDoThucHienRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query5.Count() > 0)
            {
                foreach (var item5 in query5)
                {
                    this.DeleteTienDoThucHien(item5.IdTienDo);
                }
            }
            var query6 = _quyetToanHopDongRepository.GetAll().Where(x => x.IdHopDong == id);
            if (query6.Count() > 0)
            {
                foreach (var item6 in query6)
                {
                    this.DeleteQuyetToanHopDong(item6.IdQuyetToan);
                }
            }
            return _hopDongRepository.Delete(id);
        }

        public NguonVonThanhToanChiPhiKhac DeleteNguonVonThanhToanChiPhiKhac(int id)
        {
            return _nguonVonThanhToanChiPhiKhacRepository.Delete(id);
        }

        public ThanhToanChiPhiKhac DeleteThanhToanChiPhiKhac(int id)
        {
            var query = _nguonVonThanhToanChiPhiKhacRepository.GetAll().Where(x => x.IdThanhToanChiPhiKhac == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteNguonVonThanhToanChiPhiKhac(item.IdNguonVonThanhToanChiPhiKhac);
                }
            }
            return _thanhToanChiPhiKhacRepository.Delete(id);
        }

        public KetQuaDauThau DeleteKetQuaDauThau(int id)
        {
            return _ketQuaDauThauRepository.Delete(id);
        }

        public DanhMucNhaThau DeleteDanhMucNhaThau(int id)
        {
            var query1 = _thanhToanChiPhiKhacRepository.GetAll().Where(x => x.IdNhaThau == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeleteThanhToanChiPhiKhac(item1.IdThanhToanChiPhiKhac);
                }
            }
            var query2 = _hopDongRepository.GetAll().Where(x => x.IdNhaThau == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteHopDong(item2.IdHopDong);
                }
            }
            var query3 = _ketQuaDauThauRepository.GetAll().Where(x => x.IdNhaThau == id);
            if (query3.Count() > 0)
            {
                foreach (var item3 in query3)
                {
                    this.DeleteKetQuaDauThau(item3.IdKetQuaLuaChonNhaThau);
                }
            }
            return _danhMucNhaThauRepository.Delete(id);
        }

        public HoSoMoiThau DeleteHoSoMoiThau(int id)
        {
            return _hoSoMoiThauRepository.Delete(id);
        }

        public GoiThau DeleteGoiThau(int id)
        {
            var query1 = _hopDongRepository.GetAll().Where(x => x.IdGoiThau == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeleteHopDong(item1.IdHopDong);
                }
            }
            var query2 = _nguonVonDuAnGoiThauRepository.GetAll().Where(x => x.IdGoiThau == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteNguonVonDuAnGoiThau(item2.IdNguonVonDuAnGoiThau);
                }
            }
            var query3 = _ketQuaDauThauRepository.GetAll().Where(x => x.IdGoiThau == id);
            if (query3.Count() > 0)
            {
                foreach (var item3 in query3)
                {
                    this.DeleteKetQuaDauThau(item3.IdKetQuaLuaChonNhaThau);
                }
            }
            var query4 = _hoSoMoiThauRepository.GetAll().Where(x => x.IdGoiThau == id);
            if (query4.Count() > 0)
            {
                foreach (var item4 in query4)
                {
                    this.DeleteHoSoMoiThau(item4.IdHoSoMoiThau);
                }
            }
            return _goiThauRepository.Delete(id);
        }

        public NguonVonKeHoachVon DeleteNguonVonKeHoachVon(int id)
        {
            return _nguonVonKeHoachVonRepository.Delete(id);
        }

        public NguonVonDuAn DeleteNguonVonDuAn(int id)
        {
            var query1 = _nguonVonKeHoachVonRepository.GetAll().Where(x => x.IdNguonVonDuAn == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeleteNguonVonKeHoachVon(item1.IdNguonVonKeHoachVon);
                }
            }
            var query2 = _nguonVonDuAnGoiThauRepository.GetAll().Where(x => x.IdNguonVonDuAn == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteNguonVonDuAnGoiThau(item2.IdNguonVonDuAnGoiThau);
                }
            }
            return _nguonVonDuAnRepository.Delete(id);
        }

        public KeHoachVon DeleteKeHoachVon(int id)
        {
            var query = _nguonVonKeHoachVonRepository.GetAll().Where(x => x.IdKeHoachVon == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteNguonVonKeHoachVon(item.IdNguonVonKeHoachVon);
                }
            }
            return _keHoachVonRepository.Delete(id);
        }

        public ChuTruongDauTu DeleteChuTruongDauTu(int id)
        {
            return _chuTruongDauTuRepository.Delete(id);
        }

        public KeHoachLuaChonNhaThau DeleteKeHoachLuaChonNhaThau(int id)
        {
            var query = _goiThauRepository.GetAll().Where(x => x.IdKeHoachLuaChonNhaThau == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteGoiThau(item.IdGoiThau);
                }
            }
            return _keHoachLuaChonNhaThauRepository.Delete(id);
        }

        public NguonVonQuyetToanDuAn DeleteNguonVonQuyetToanDuAn(int id)
        {
            return _nguonVonQuyetToanDuAnRepository.Delete(id);
        }

        public QuyetToanDuAn DeleteQuyetToanDuAn(int id)
        {
            var query = _nguonVonQuyetToanDuAnRepository.GetAll().Where(x => x.IdQuyetToanDuAn == id);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    this.DeleteNguonVonQuyetToanDuAn(item.IdNguonVonQuyetToanDuAn);
                }
            }
            return _quyetToanDuAnRepository.Delete(id);
        }

        public ThamDinhLapQuanLyDauTu DeleteThamDinhLapQuanLyDauTu(int id)
        {
            return _thamDinhLapQuanLyDauTuRepository.Delete(id);
        }

        public LapQuanLyDauTu DeleteLapQuanLyDauTu(int id)
        {
            var query1 = _thamDinhLapQuanLyDauTuRepository.GetAll().Where(x => x.IdLapQuanLyDauTu == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeleteThamDinhLapQuanLyDauTu(item1.IdThamDinh);
                }
            }
            var query2 = _nguonVonDuAnRepository.GetAll().Where(x => x.IdLapQuanLyDauTu == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteNguonVonDuAn(item2.IdNguonVonDuAn);
                }
            }
            return _lapQuanLyDauTuRepository.Delete(id);
        }

        public VanBanDuAn DeleteVanBanDuAn(int id)
        {
            return _vanBanDuAnRepository.Delete(id);
        }

        public DuAn DeleteDuAn(int id)
        {
            var query1 = _nguonVonDuAnRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query1.Count() > 0)
            {
                foreach (var item1 in query1)
                {
                    this.DeleteNguonVonDuAn(item1.IdNguonVonDuAn);
                }
            }
            var query2 = _hopDongRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query2.Count() > 0)
            {
                foreach (var item2 in query2)
                {
                    this.DeleteHopDong(item2.IdHopDong);
                }
            }
            var query3 = _keHoachVonRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query3.Count() > 0)
            {
                foreach (var item3 in query3)
                {
                    this.DeleteKeHoachVon(item3.IdKeHoachVon);
                }
            }
            var query4 = _goiThauRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query4.Count() > 0)
            {
                foreach (var item4 in query4)
                {
                    this.DeleteGoiThau(item4.IdGoiThau);
                }
            }
            var query5 = _chuTruongDauTuRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query5.Count() > 0)
            {
                foreach (var item5 in query5)
                {
                    this.DeleteChuTruongDauTu(item5.IdChuTruongDauTu);
                }
            }
            var query6 = _danhMucNhaThauRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query6.Count() > 0)
            {
                foreach (var item6 in query6)
                {
                    this.DeleteDanhMucNhaThau(item6.IdNhaThau);
                }
            }
            var query7 = _keHoachLuaChonNhaThauRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query7.Count() > 0)
            {
                foreach (var item7 in query7)
                {
                    this.DeleteKeHoachLuaChonNhaThau(item7.IdKeHoachLuaChonNhaThau);
                }
            }
            var query8 = _quyetToanDuAnRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query8.Count() > 0)
            {
                foreach (var item8 in query8)
                {
                    this.DeleteQuyetToanDuAn(item8.IdQuyetToanDuAn);
                }
            }
            var query9 = _lapQuanLyDauTuRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query9.Count() > 0)
            {
                foreach (var item9 in query9)
                {
                    this.DeleteLapQuanLyDauTu(item9.IdLapQuanLyDauTu);
                }
            }
            var query10 = _vanBanDuAnRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query10.Count() > 0)
            {
                foreach (var item10 in query10)
                {
                    this.DeleteVanBanDuAn(item10.IdVanBan);
                }
            }
            var query11 = _tienDoThucHienRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query11.Count() > 0)
            {
                foreach (var item11 in query11)
                {
                    this.DeleteTienDoThucHien(item11.IdTienDo);
                }
            }
            var query12 = _thanhToanChiPhiKhacRepository.GetAll().Where(x => x.IdDuAn == id);
            if (query12.Count() > 0)
            {
                foreach (var item12 in query12)
                {
                    this.DeleteThanhToanChiPhiKhac(item12.IdThanhToanChiPhiKhac);
                }
            }
            return _duAnRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public NguonVon DeleteNguonVon(int id)
        {
            var query = _nguonVonDuAnRepository.GetAll().Where(x => x.IdNguonVon == id);
            if (query.Count() > 0)
            {
                foreach(var item in query)
                {
                    this.DeleteNguonVonDuAn(item.IdNguonVonDuAn);
                }
            }
            return _nguonVonRepository.Delete(id);
        }
    }
}