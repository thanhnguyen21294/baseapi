﻿using Common;
using Data.Infrastructure;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IAppRoleServices {

        //Dictionary<string, string> GetUserRole();
        IEnumerable<UserRole> GetUserRole();
    }
    public class AppRoleService : IAppRoleServices
    {
        public IAppRoleRepository _AppRoleRepository;
        private IUnitOfWork _unitOfWork;
        public AppRoleService(IAppRoleRepository appRoleRepository, IUnitOfWork unitOfWork)
        {
            _AppRoleRepository = appRoleRepository;
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<UserRole> GetUserRole()
        {
            return _AppRoleRepository.GetUserRole();
        }

        //public Dictionary<string,string> GetUserRole()
        //{
        //    return _AppRoleRepository.GetUserRole().ToDictionary(x => x.UserId, x => x.RoleName);
        //}
        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
