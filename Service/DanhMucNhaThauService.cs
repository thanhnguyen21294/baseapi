﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface IDanhMucNhaThauService
    {
        DanhMucNhaThau Add(DanhMucNhaThau danhMucNhaThau);

        void Update(DanhMucNhaThau danhMucNhaThau);

        DanhMucNhaThau Delete(int id);
        IEnumerable<DanhMucNhaThau> GetByListIDNhaThau(List<int> ids);
        IEnumerable<DanhMucNhaThau> GetAll();
        IEnumerable<DanhMucNhaThau> GetAll(int idDuAn);
        IEnumerable<DanhMucNhaThau> GetByFilter(int page, int pageSize, out int totalRow, string filter);
        IEnumerable<DanhMucNhaThau> GetAllForLocDuAn();
        DanhMucNhaThau GetById(int id);

        void Save();
    }



    public class DanhMucNhaThauService : IDanhMucNhaThauService
    {
        private IDanhMucNhaThauRepository _danhMucNhaThauRepository;
        private IUnitOfWork _unitOfWork;

        public DanhMucNhaThauService(IDanhMucNhaThauRepository danhMucNhaThauRepository, IUnitOfWork unitOfWork)
        {
            this._danhMucNhaThauRepository = danhMucNhaThauRepository;
            this._unitOfWork = unitOfWork;
        }
        public DanhMucNhaThau Add(DanhMucNhaThau danhMucNhaThau)
        {
            return _danhMucNhaThauRepository.Add(danhMucNhaThau);
        }

        public DanhMucNhaThau Delete(int id)
        {
            return _danhMucNhaThauRepository.Delete(id);
        }

        public IEnumerable<DanhMucNhaThau> GetAll()
        {
            return _danhMucNhaThauRepository.GetAll();
        }

        public IEnumerable<DanhMucNhaThau> GetAllForLocDuAn()
        {
            var dmnt = _danhMucNhaThauRepository.GetAll();;

            return dmnt;
        }

        public IEnumerable<DanhMucNhaThau> GetAll(int idDuAn)
        {
            return _danhMucNhaThauRepository.GetMulti(x => x.IdDuAn == idDuAn);
        }

        public IEnumerable<DanhMucNhaThau> GetByListIDNhaThau(List<int> ids)
        {
            return _danhMucNhaThauRepository.GetMulti(x => ids.Contains(x.IdNhaThau));
        }

        public IEnumerable<DanhMucNhaThau> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _danhMucNhaThauRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenNhaThau.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdNhaThau).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public DanhMucNhaThau GetById(int id)
        {
            return _danhMucNhaThauRepository.GetMulti(x => x.IdNhaThau == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DanhMucNhaThau danhMucNhaThau)
        {
            _danhMucNhaThauRepository.Update(danhMucNhaThau);
        }
    }
}
