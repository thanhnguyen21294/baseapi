﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public interface IPhongCongTacService
    {
        PhongCongTac Add(PhongCongTac iPhongCongTac);

        void Update(PhongCongTac iPhongCongTac);
        PhongCongTac Delete(int id);
        IEnumerable<PhongCongTac> GetAll();
        IEnumerable<PhongCongTac> GetByIdDuAn(int idDA);
        PhongCongTac GetById(int id);
        PhongCongTac GetByIdDuAnIDPhong(int id, int IDPhong);
        void Save();
    }
    public class PhongCongTacService : IPhongCongTacService
    {
        private IPhongCongTacRepository _PhongCongTacRepository;
        private IUnitOfWork _unitOfWork;

        public PhongCongTacService(IPhongCongTacRepository kehoachvonRepository, IUnitOfWork unitOfWork)
        {
            this._PhongCongTacRepository = kehoachvonRepository;
            this._unitOfWork = unitOfWork;
        }
        public PhongCongTac Add(PhongCongTac keHoachVon)
        {
            return _PhongCongTacRepository.Add(keHoachVon);
        }

        public PhongCongTac Delete(int id)
        {
            return _PhongCongTacRepository.Delete(id);
        }

        public IEnumerable<PhongCongTac> GetAll()
        {
            return _PhongCongTacRepository.GetAll();
        }

      
        public IEnumerable<PhongCongTac> GetByIdDuAn(int idDA)
        {
            var query = _PhongCongTacRepository.GetAll();
            query = query.Where(x => x.IdDuAn == idDA);
            return query;
        }

        public PhongCongTac GetByIdDuAnIDPhong(int idDA, int idPhong)
        {
            var query = _PhongCongTacRepository.GetMulti(x => x.IdDuAn == idDA && x.IdNhomDuAnTheoUser == idPhong).FirstOrDefault();
            return query;
        }

        public PhongCongTac GetById(int id)
        {
            return _PhongCongTacRepository.GetSingleByCondition(x => x.IdChuDauTu == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PhongCongTac updatePhongCongTac)
        {
            _PhongCongTacRepository.Update(updatePhongCongTac);
        }
    }
}
