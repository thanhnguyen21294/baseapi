﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IVanBanDuAnService
    {
        IEnumerable<VanBanDuAn> GetVBDAByTM(int idTM,string filter);
        IEnumerable<VanBanDuAn> GetByIdDuAn(int id);
        IEnumerable<VanBanDuAn> GetAllForHistory();        
        VanBanDuAn Add(VanBanDuAn vbda);
        void Update(VanBanDuAn vbda);
        void Delete(int idVBDA);
        void Save();
        VanBanDuAn GetById(int id);
        int TotalVBDAByTM(int idTM);
    }
    class VanBanDuAnService : IVanBanDuAnService
    {
        public IVanBanDuAnRepository _vbdarepository;
        public IUnitOfWork _unitOfWork;
        public VanBanDuAnService(IVanBanDuAnRepository vbdaRepository, IUnitOfWork unitOfWork)
        {
            _vbdarepository = vbdaRepository;
            _unitOfWork = unitOfWork;
        }
        public VanBanDuAn Add(VanBanDuAn vbda)
        {
          VanBanDuAn newVBDA= _vbdarepository.Add(vbda);
            return newVBDA;
        }
       

        public void Delete(int idVBDA)
        {
            _vbdarepository.Delete(idVBDA);
        }

        public IEnumerable<VanBanDuAn> GetVBDAByTM(int idTM,string filter)
        {
            IEnumerable<VanBanDuAn> vbdas = _vbdarepository.GetVBDAByTM(idTM,filter);
            return vbdas;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(VanBanDuAn vbda)
        {
            _vbdarepository.Update(vbda);
        }
        public VanBanDuAn GetById(int id)
        {
          return  _vbdarepository.GetSingleById(id);
        }

        public int TotalVBDAByTM(int idTM)
        {
            return _vbdarepository.totalVBDAByTM(idTM);
        }

        public IEnumerable<VanBanDuAn> GetByIdDuAn(int id)
        {
            return _vbdarepository.GetAll().Where(x => x.IdDuAn == id).OrderBy(y=>y.TenVanBan);
        }

        public IEnumerable<VanBanDuAn> GetAllForHistory()
        {
            return _vbdarepository.GetAll();
        }
    }
}
