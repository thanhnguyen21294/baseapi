﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Data.Repositories;
using Model.Models;

namespace Service
{
    public interface ILoaiCapCongTrinhService
    {
        LoaiCapCongTrinh Add(LoaiCapCongTrinh loaiCapCongTrinh);

        void Update(LoaiCapCongTrinh loaiCapCongTrinh);

        LoaiCapCongTrinh Delete(int id);

        IEnumerable<LoaiCapCongTrinh> GetAll();

        IEnumerable<LoaiCapCongTrinh> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        LoaiCapCongTrinh GetById(int id);

        void Save();
    }



    public class LoaiCapCongTrinhService : ILoaiCapCongTrinhService
    {
        private ILoaiCapCongTrinhRepository _loaiCapCongTrinhRepository;
        private IUnitOfWork _unitOfWork;

        public LoaiCapCongTrinhService(ILoaiCapCongTrinhRepository loaiCapCongTrinhRepository, IUnitOfWork unitOfWork)
        {
            this._loaiCapCongTrinhRepository = loaiCapCongTrinhRepository;
            this._unitOfWork = unitOfWork;
        }
        public LoaiCapCongTrinh Add(LoaiCapCongTrinh loaiCapCongTrinh)
        {
            return _loaiCapCongTrinhRepository.Add(loaiCapCongTrinh);
        }

        public LoaiCapCongTrinh Delete(int id)
        {
            return _loaiCapCongTrinhRepository.Delete(id);
        }

        public IEnumerable<LoaiCapCongTrinh> GetAll()
        {
            return _loaiCapCongTrinhRepository.GetAll();
        }

        public IEnumerable<LoaiCapCongTrinh> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _loaiCapCongTrinhRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenLoaiCapCongTrinh.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLoaiCapCongTrinh).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LoaiCapCongTrinh GetById(int id)
        {
            return _loaiCapCongTrinhRepository.GetMulti(x => x.IdLoaiCapCongTrinh == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LoaiCapCongTrinh loaiCapCongTrinh)
        {
            _loaiCapCongTrinhRepository.Update(loaiCapCongTrinh);
        }
    }
}
