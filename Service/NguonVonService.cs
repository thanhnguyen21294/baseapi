﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface INguonVonService
    {
        NguonVon Add(NguonVon nguonVon);

        void Update(NguonVon nguonVon);

        NguonVon Delete(int id);

        IEnumerable<NguonVon> GetAll();

        IEnumerable<NguonVon> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        NguonVon GetById(int id);

        IEnumerable<NguonVon> GetByIdQuyetToanDuAn(int id);

        void Save();
    }

    public class NguonVonService : INguonVonService
    {
        private INguonVonRepository _nguonVonRepository;
        private INguonVonDuAnService _nguonVonDuAnService;
        private INguonVonQuyetToanDuAnService _nguonVonQuyetToanDuAnService;
        private IUnitOfWork _unitOfWork;

        public NguonVonService(INguonVonRepository nguonVonRepository, INguonVonDuAnService nguonVonDuAnService, INguonVonQuyetToanDuAnService nguonVonQuyetToanDuAnService, IUnitOfWork unitOfWork)
        {
            this._nguonVonRepository = nguonVonRepository;
            this._nguonVonDuAnService = nguonVonDuAnService;
            this._nguonVonQuyetToanDuAnService = nguonVonQuyetToanDuAnService;
            this._unitOfWork = unitOfWork;
        }
        public NguonVon Add(NguonVon nguonVon)
        {
            return _nguonVonRepository.Add(nguonVon);
        }

        public NguonVon Delete(int id)
        {
            return _nguonVonRepository.Delete(id);
        }

        public IEnumerable<NguonVon> GetAll()
        {
            return _nguonVonRepository.GetAll();
        }

        private IEnumerable<NguonVon> GetChild(IEnumerable<NguonVon> nguonVons, NguonVon nguonVon)
        {
            return nguonVons.Where(x => x.IdNguonVonCha == nguonVon.IdNguonVon || x.IdNguonVon == nguonVon.IdNguonVon)
                .Union(nguonVons.Where(x => x.IdNguonVonCha == nguonVon.IdNguonVon).SelectMany(y => GetChild(nguonVons, y)));
        }

        private IEnumerable<NguonVon> FindAllParents(List<NguonVon> all_data, int? child)
        {
            var parent = all_data.FirstOrDefault(x => x.IdNguonVon == child);

            if (parent == null)
                return Enumerable.Empty<NguonVon>();

            return new[] { parent }.Concat(FindAllParents(all_data, parent.IdNguonVonCha));
        }

        public IEnumerable<NguonVon> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _nguonVonRepository.GetAll();
            List<NguonVon> search = new List<NguonVon>();
            if (filter != null)
            {
                var listSearch = query.Where(x => x.TenNguonVon.Contains(filter) || x.Ma.Contains(filter));
                foreach (var nguonVon in listSearch)
                {
                    var items = search.Where(x => x.IdNguonVon == nguonVon.IdNguonVon);
                    if (!items.Any())
                    {
                        search.Add(nguonVon);
                        search = search.Union(FindAllParents(query.ToList(), nguonVon.IdNguonVon)).ToList();
                    }
                }
                var parents = search.Where(x => x.IdNguonVonCha == null);
                totalRow = parents.Count();
                var resual = parents.OrderBy(x => x.IdNguonVon).Skip((page - 1) * pageSize).Take(pageSize);

                foreach (var duAn in resual)
                {
                    parents = parents.Union(GetChild(search, duAn));
                }
                return parents;

            }
            var parent = query.Where(y => y.IdNguonVonCha == null).OrderBy(x => x.IdNguonVon).Skip((page - 1) * pageSize).Take(pageSize);
            if (!parent.Any() && page > 1)
            {
                page = page - 1;
                parent = query.Where(y => y.IdNguonVonCha == null).OrderBy(x => x.IdNguonVon).Skip((page - 1) * pageSize).Take(pageSize);
            }
            totalRow = query.Count(x => x.IdNguonVonCha == null);
            foreach (var item in parent)
            {
                parent = parent.Union(GetChild(query, item));
            }
            return parent;
        }

        public NguonVon GetById(int id)
        {
            return _nguonVonRepository.GetMulti(x => x.IdNguonVon == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(NguonVon nguonVon)
        {
            _nguonVonRepository.Update(nguonVon);
        }

        public IEnumerable<NguonVon> GetByIdQuyetToanDuAn(int id)
        {
            List<NguonVon> nguonvons = new List<NguonVon>();
            var nguonvonquyettoanduan = _nguonVonQuyetToanDuAnService.GetAll().Where(x => x.IdQuyetToanDuAn == id);
            foreach(var item in nguonvonquyettoanduan)
            {
                var nguonvonduans = _nguonVonDuAnService.GetAll().Where(x => x.IdNguonVonDuAn == item.IdNguonVonDuAn);
                foreach(var item1 in nguonvonduans)
                {
                    var nguonvon = this.GetAll().Where(x => x.IdNguonVon == item1.IdNguonVon);
                    foreach(var item2 in nguonvon)
                    {
                        nguonvons.Add(item2);
                    }
                }
            }
            return nguonvons;
        }
    }
}
