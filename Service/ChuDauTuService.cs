﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IChuDauTuService
    {
        ChuDauTu Add(ChuDauTu chuDauTu);

        void Update(ChuDauTu chuDauTu);

        ChuDauTu Delete(int id);

        IEnumerable<ChuDauTu> GetAll();

        IEnumerable<ChuDauTu> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        ChuDauTu GetById(int id);

        void Save();
    }

    public class ChuDauTuService : IChuDauTuService
    {
        private IChuDauTuRepository _chuDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public ChuDauTuService(IChuDauTuRepository chuDauTuRepository, IUnitOfWork unitOfWork)
        {
            this._chuDauTuRepository = chuDauTuRepository;
            this._unitOfWork = unitOfWork;
        }
        public ChuDauTu Add(ChuDauTu chuDauTu)
        {
            return _chuDauTuRepository.Add(chuDauTu);
        }

        public ChuDauTu Delete(int id)
        {
            return _chuDauTuRepository.Delete(id);
        }

        public IEnumerable<ChuDauTu> GetAll()
        {
            return _chuDauTuRepository.GetAll();
        }

        public IEnumerable<ChuDauTu> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _chuDauTuRepository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.TenChuDauTu.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdChuDauTu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ChuDauTu GetById(int id)
        {
            return _chuDauTuRepository.GetMulti(x => x.IdChuDauTu == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ChuDauTu chuDauTu)
        {
            _chuDauTuRepository.Update(chuDauTu);
        }
    }
}
