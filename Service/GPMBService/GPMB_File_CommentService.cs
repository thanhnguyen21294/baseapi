﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.QLTD.GPMB;
using System.Collections.Generic;
using System.Linq;

namespace Service.GPMBService
{

    public interface IGPMB_File_CommentService
    {
        GPMP_File_Comment Add(GPMP_File_Comment Comments);

        void Update(int id);

        GPMP_File_Comment Delete(int id);

        IEnumerable<GPMP_File_Comment> GetAll();

        IEnumerable<GPMP_File_Comment> GetByIdGPMBFileComment(int idFile);

        GPMP_File_Comment GetById(int id);

        void Save();
    }

    public class GPMB_File_CommentService : IGPMB_File_CommentService
    {
        private IGPMB_File_CommentRepository _GPMB_File_CommentRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_File_CommentService(IGPMB_File_CommentRepository GPMB_File_CommentRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_File_CommentRepository = GPMB_File_CommentRepository;
            this._unitOfWork = unitOfWork;
        }
        public GPMP_File_Comment Add(GPMP_File_Comment Comments)
        {
            return _GPMB_File_CommentRepository.Add(Comments);
        }

        public GPMP_File_Comment Delete(int id)
        {
            return _GPMB_File_CommentRepository.Delete(id);
        }

        public IEnumerable<GPMP_File_Comment> GetAll()
        {
            return _GPMB_File_CommentRepository.GetAll(new[] { "GPMB_File" });
        }

        public IEnumerable<GPMP_File_Comment> GetByIdGPMBFileComment(int idFile)
        {
            return _GPMB_File_CommentRepository.GetAll(new string[] { "AppUser" }).Where(x => x.IdFile == idFile);
        }

        public GPMP_File_Comment GetById(int id)
        {
            return _GPMB_File_CommentRepository.GetMulti(x => x.Id == id, new string[] { "AppUser" }).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMP_File_Comment Comments)
        {
            _GPMB_File_CommentRepository.Update(Comments);
        }

        public void Update(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}
