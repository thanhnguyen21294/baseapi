﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.BCModel.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_CTCVService
    {
        GPMB_CTCV Add(GPMB_CTCV gpmb_CTCV);

        void Update(GPMB_CTCV gpmb_CTCV);

        GPMB_CTCV Delete(int id);

        IEnumerable<GPMB_CTCV> GetAll(int idKHCV);

        GPMB_CTCV GetByID(int id);

        IEnumerable<GPMB_CTCV> GetAllByIDKH(int idKH, string filter = null);

        GPMB_CTCV GetByIdKHCV(int idKHCV);

        void Save();

        IEnumerable<GPMB_CTCV> GetCongViecChoDuyet(string filter = null);

        IEnumerable<GPMB_KeHoachTienDo_BC> GetBaoCaoDangKiKeHoachTienDoChiTiet(string IDDuAns);
    }

    public class GPMB_CTCVService : IGPMB_CTCVService
    {
        private IGPMB_CTCVRepository _GPMB_CTCVRepository;
        private IGPMB_KeHoachRepository _GPMB_KeHoachRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_CTCVService(IGPMB_CTCVRepository GPMB_CTCVRepository, IGPMB_KeHoachRepository GPMB_KeHoachRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_CTCVRepository = GPMB_CTCVRepository;
            this._GPMB_KeHoachRepository = GPMB_KeHoachRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_CTCV Add(GPMB_CTCV gpmb_CTCV)
        {
            return _GPMB_CTCVRepository.Add(gpmb_CTCV);
        }

        public GPMB_CTCV Delete(int id)
        {
            return _GPMB_CTCVRepository.Delete(id);
        }

        public IEnumerable<GPMB_CTCV> GetAll(int idKHCV)
        {
            return _GPMB_CTCVRepository.GetGPMB_CTCV().Where(x => x.GPMB_KeHoachCongViec.IdKeHoachGPMB == idKHCV);
        }

        public IEnumerable<GPMB_CTCV> GetAllByIDKH(int idKH, string filter = null)
        {
            var query = _GPMB_CTCVRepository.GetMulti(x => x.GPMB_KeHoachCongViec.IdKeHoachGPMB == idKH, new[] { "GPMB_KeHoachCongViec", "GPMB_KeHoachCongViec.GPMB_CongViec" });
            if (filter != null)
            {
                query = query.Where(x => x.GPMB_KeHoachCongViec.GPMB_CongViec.TenCongViec.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public GPMB_CTCV GetByIdKHCV(int idKHCV)
        {
            return _GPMB_CTCVRepository.GetGPMB_CTCV().Where(x => x.IdKeHoachCongViecGPMB == idKHCV).SingleOrDefault();
        }
        public GPMB_CTCV GetByID(int id)
        {
            return _GPMB_CTCVRepository.GetSingleByCondition(x => x.IdChiTietCongViec == id, new[] { "GPMB_KeHoachCongViec", "GPMB_KeHoachCongViec.GPMB_CongViec", "GPMB_KeHoachCongViec.GPMB_KeHoach" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_CTCV gpmb_CTCV)
        {
            _GPMB_CTCVRepository.Update(gpmb_CTCV);
        }

        public IEnumerable<GPMB_KeHoachTienDo_BC> GetBaoCaoDangKiKeHoachTienDoChiTiet(string IDDuAns)
        {
            string[] sIdDuAn = IDDuAns.Split(',');

            IEnumerable<int> idskehoach;
            idskehoach = _GPMB_KeHoachRepository.GetMulti(x => x.TrangThai == 3 && sIdDuAn.Contains(x.IdDuAn.ToString())).Select(y => new
            {
                IdKeHoachGPMB = y.IdKeHoachGPMB,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn }).Select(x => x.OrderByDescending(y => y.IdKeHoachGPMB).Select(y => y.IdKeHoachGPMB).FirstOrDefault());
            var lst = _GPMB_CTCVRepository.GetMulti(x => idskehoach.Contains(x.GPMB_KeHoachCongViec.IdKeHoachGPMB), new[] { "GPMB_KeHoachCongViec", "GPMB_KeHoachCongViec.GPMB_KeHoach", "GPMB_KeHoachCongViec.GPMB_KeHoach.DuAn" })
                .Select(s => new {
                    IDDuAn = s.GPMB_KeHoachCongViec.GPMB_KeHoach.IdDuAn,
                    TenDuAn  = s.GPMB_KeHoachCongViec.GPMB_KeHoach.DuAn.TenDuAn,
                    IDCongViecGPMB = s.GPMB_KeHoachCongViec.IdCongViecGPMB,
                    VBGNV_SoVanBan = s.VBGNV_SoVanBan != null ? s.VBGNV_SoVanBan : "",
                    VBGNV_Ngay = s.VBGNV_Ngay != null ? s.VBGNV_Ngay.ToString() : "",
                    BBBGMG_Ngay = s.BBBGMG_Ngay != null ? s.BBBGMG_Ngay.ToString() : "",
                    QDTLHD_So = s.QDTLHD_So != null ? s.QDTLHD_So : "",
                    QDTLHD_Ngay = s.QDTLHD_Ngay != null ? s.QDTLHD_Ngay.ToString() : "",
                    QDTCT_So = s.QDTCT_So != null ? s.QDTCT_So : "",
                    QDTCT_Ngay = s.QDTCT_Ngay != null ? s.QDTCT_Ngay.ToString() : "",
                    QDPDDT_So = s.QDPDDT_So != null ? s.QDPDDT_So : "",
                    QDPDDT_NgayBanBanHanh = s.QDPDDT_NgayBanBanHanh != null ? s.QDPDDT_NgayBanBanHanh.ToString() : "",
                    HHD_Ngay = s.HHD_Ngay != null ? s.HHD_Ngay.ToString() : "",
                    HDU_Ngay = s.HDU_Ngay != null ? s.HDU_Ngay.ToString() : "",
                    QCTDHT_Chon = s.QCTDHT_Chon ? "Đã xong" : "Chưa xong",
                    TBTHD_SoDoiTuong = s.TBTHD_SoDoiTuong != null ? s.TBTHD_SoDoiTuong : "",
                    HDPTK_Chon = s.HDPTK_Chon ? "Đã họp" : "Chưa họp",
                    DTKKLBB_SoDoiTuong = s.DTKKLBB_SoDoiTuong,
                    CCKDBB_Chon = s.CCKDBB_Chon ? "Có cưỡng chế" : "Không cưỡng chế",
                    LHSGPMB_So = s.LHSGPMB_So != null ? s.LHSGPMB_So : "",
                    LTHDTT_So = s.LTHDTT_So != null ? s.LTHDTT_So : "",
                    SKKTDTPA_So = s.SKKTDTPA_So != null ? s.SKKTDTPA_So : "",
                    THDHTPA_So = s.THDHTPA_So != null ? s.THDHTPA_So : "",
                    QDPDPA_So = s.QDPDPA_So != null ? s.QDPDPA_So : "",
                    QDPDPA_TGT = s.QDPDPA_TGT != null ? Convert.ToDouble(s.QDPDPA_TGT.ToString()).ToString("N0", System.Globalization.CultureInfo.GetCultureInfo("vi")) : "",
                    CTTNBG_So = s.CTTNBG_So != null ? s.CTTNBG_So : "",
                    CTTNBG_TGT = s.CTTNBG_TGT != null ? Convert.ToDouble(s.CTTNBG_TGT.ToString()).ToString("N0", System.Globalization.CultureInfo.GetCultureInfo("vi")) : "",
                    CCTHD_Chon = s.CCTHD_Chon == true ? "Có cưỡng chế" : "Không cưỡng chế",
                    QTKP_So = s.QTKP_So != null ? s.QTKP_So : "",
                    QTKP_Ngay = s.QTKP_Ngay != null ? s.QTKP_Ngay.ToString() : "",
                    QTKP_GT = s.QTKP_GT != null ? Convert.ToDouble(s.QTKP_GT).ToString("N0", System.Globalization.CultureInfo.GetCultureInfo("vi")) : "",
                    QDGD_So = s.QDGD_So != null ? s.QDGD_So : "",
                    QDGD_Ngay = s.QDGD_Ngay != null ? s.QDGD_Ngay.ToString() : "",
                    GhiChu = s.GPMB_KeHoachCongViec.GPMB_KeHoach.GhiChu
                }).ToList();

            var lstGPMB = lst.GroupBy(x => x.IDDuAn)
                .Select(x => new GPMB_KeHoachTienDo_BC()
                {
                    TenDuAn = x.Select(y => y.TenDuAn).FirstOrDefault(),

                    CBTH_VanBanGiaoNhiemVu_SoVB = x.Where(y => y.IDCongViecGPMB == 1).Select(y => y.VBGNV_SoVanBan).FirstOrDefault(),
                    CBTH_VanBanGiaoNhiemVu_NgayQD = x.Where(y => y.IDCongViecGPMB == 1).Select(y => y.VBGNV_Ngay).FirstOrDefault(),

                    CBTH_BienBanBanGiaoMocGioi_NgayQD = x.Where(y => y.IDCongViecGPMB == 2).Select(y => y.BBBGMG_Ngay).FirstOrDefault(),

                    CBTH_QuyetDinhThanhLapHoiDong_SoVB = x.Where(y => y.IDCongViecGPMB == 3).Select(y => y.QDTLHD_So).FirstOrDefault(),
                    CBTH_QuyetDinhThanhLapHoiDong_NgayQD = x.Where(y => y.IDCongViecGPMB == 3).Select(y => y.QDTLHD_Ngay).FirstOrDefault(),
                    CBTH_QuyetDinhTCT_SoVB = x.Where(y => y.IDCongViecGPMB == 3).Select(y => y.QDTCT_So).FirstOrDefault(),
                    CBTH_QuyetDinhTCT_NgayQD = x.Where(y => y.IDCongViecGPMB == 3).Select(y => y.QDTCT_Ngay).FirstOrDefault(),

                    CBTH_QuyetDinhPheDuyetDuToan_SoVB = x.Where(y => y.IDCongViecGPMB == 4).Select(y => y.QDPDDT_So).FirstOrDefault(),
                    CBTH_QuyetDinhPheDuyetDuToan_NgayQD = x.Where(y => y.IDCongViecGPMB == 4).Select(y => y.QDPDDT_NgayBanBanHanh).FirstOrDefault(),

                    CBTH_HopHoiDong_HHD = x.Where(y => y.IDCongViecGPMB == 5).Select(y => y.HHD_Ngay).FirstOrDefault().ToString(),
                    CBTH_HopHoiDong_HDU = x.Where(y => y.IDCongViecGPMB == 5).Select(y => y.HDU_Ngay).FirstOrDefault(),

                    CBTH_TrichDoHienTrang_Chon = x.Where(y => y.IDCongViecGPMB == 6).Select(y => y.QCTDHT_Chon).FirstOrDefault(),

                    CBTH_ThongBaoThuHoiDat_SoVB = x.Where(y => y.IDCongViecGPMB == 7).Select(y => y.TBTHD_SoDoiTuong).FirstOrDefault(),

                    TH_HopDan_Chon = x.Where(y => y.IDCongViecGPMB == 8).Select(y => y.HDPTK_Chon).FirstOrDefault().ToString(),

                    TH_DieuTraKeKhai_SoVB = x.Where(y => y.IDCongViecGPMB == 9).Select(y => y.DTKKLBB_SoDoiTuong).FirstOrDefault(),
                    
                    TH_CuongCheKiemDem_Chon = x.Where(y => y.IDCongViecGPMB == 10).Select(y => y.CCKDBB_Chon).FirstOrDefault().ToString(),

                    TH_LapHoSoGPMB_SoVB = x.Where(y => y.IDCongViecGPMB == 12).Select(y => y.LHSGPMB_So).FirstOrDefault(),

                    TH_LapTrinhHoiDongThamTra_SoVB = x.Where(y => y.IDCongViecGPMB == 13).Select(y => y.LTHDTT_So).FirstOrDefault(),

                    TH_CongKhaiDuThao_SoVB = x.Where(y => y.IDCongViecGPMB == 14).Select(y => y.SKKTDTPA_So).FirstOrDefault(),

                    TH_TrinhHoiDongThamDinh_SoVB = x.Where(y => y.IDCongViecGPMB == 15).Select(y => y.THDHTPA_So).FirstOrDefault(),

                    TH_QuyetDinhThuHoiDat_SoVB = x.Where(y => y.IDCongViecGPMB == 16).Select(y => y.QDPDPA_So).FirstOrDefault(),
                    TH_QuyetDinhThuHoiDat_GiaTri = x.Where(y => y.IDCongViecGPMB == 16).Select(y => y.QDPDPA_TGT).FirstOrDefault().ToString(),

                    TH_ChiTraTien_SoVB = x.Where(y => y.IDCongViecGPMB == 17).Select(y => y.CTTNBG_So).FirstOrDefault(),
                    TH_ChiTraTien_GiaTri = x.Where(y => y.IDCongViecGPMB == 17).Select(y => y.CTTNBG_TGT).FirstOrDefault(),

                    TH_CuongCheThuHoiDat_Chon = x.Where(y => y.IDCongViecGPMB == 18).Select(y => y.CCTHD_Chon).FirstOrDefault().ToString(),

                    TH_QuyetToanKinhPhi_SoVB = x.Where(y => y.IDCongViecGPMB == 19).Select(y => y.QTKP_So).FirstOrDefault(),
                    TH_QuyetToanKinhPhi_NgayQD = x.Where(y => y.IDCongViecGPMB == 19).Select(y => y.QTKP_Ngay).FirstOrDefault(),
                    TH_QuyetToanKinhPhi_GiaTri = x.Where(y => y.IDCongViecGPMB == 19).Select(y => y.QTKP_GT).FirstOrDefault(),

                    TH_LapHoSoDeNghiTP_SoVB = x.Where(y => y.IDCongViecGPMB == 20).Select(y => y.QDGD_So).FirstOrDefault(),
                    TH_LapHoSoDeNghiTP_NgayQD = x.Where(y => y.IDCongViecGPMB == 20).Select(y => y.QDGD_Ngay).FirstOrDefault(),

                    GhiChu = x.Select(y => y.GhiChu).FirstOrDefault()
                });

            return lstGPMB.ToList();

        }

        public IEnumerable<GPMB_CTCV> GetCongViecChoDuyet(string filter = null)
        {
            var lst = _GPMB_CTCVRepository.
                GetMulti(x => x.HoanThanh == 1, new string[] {
                    "GPMB_KeHoachCongViec",
                    "GPMB_KeHoachCongViec.GPMB_CongViec",
                    "GPMB_KeHoachCongViec.GPMB_KeHoach",
                    "GPMB_KeHoachCongViec.GPMB_KeHoach.DuAn"
                });
            if (filter != null)
            {
                lst = lst.Where(x => x.GPMB_KeHoachCongViec.GPMB_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            return lst;
        }
    }
}
