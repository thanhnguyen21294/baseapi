﻿using Data.Infrastructure;
using Data.Repositories;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_KeHoachCongViecService
    {
        GPMB_KeHoachCongViec Add(GPMB_KeHoachCongViec gpmb_KeHoachCongViec);

        void Update(GPMB_KeHoachCongViec gpmb_KeHoachCongViec);

        GPMB_KeHoachCongViec Delete(int id);

        IEnumerable<GPMB_KeHoachCongViec> GetAll();

        IEnumerable<GPMB_KeHoachCongViec> GetAllID(int idKH);

        IEnumerable<GPMB_KeHoachCongViec> GetByFilter(int idKH, int page, int pageSize, out int totalRow, string filter);

        IEnumerable<GPMB_KeHoachCongViec> GetListByIdKeHoach(int idKH);

        GPMB_KeHoachCongViec GetById(int id);

        void DeleteByIdKeHoachGPMB(int idKH);

        void Save();
    }

    public class GPMB_KeHoachCongViecService : IGPMB_KeHoachCongViecService
    {
        private IGPMB_KeHoachCongViecRepository _GPMB_KeHoachCongViecRepository;
        private IDuAnRepository _DuAnRepository;

        private IGPMB_CTCVRepository _GPMB_CTCVRepository;

        private IGPMB_KeHoachRepository _IGPMB_KeHoachRepository;
        private IUnitOfWork _unitOfWork;
        private IGPMB_KeHoachService _GPMB_KeHoachService;

        public GPMB_KeHoachCongViecService(IGPMB_KeHoachCongViecRepository GPMB_KeHoachCongViecRepository,
            IDuAnRepository duAnRepository,
            IGPMB_CTCVRepository GPMB_CTCVRepository,
            IGPMB_KeHoachRepository IGPMB_KeHoachRepository,
            IUnitOfWork unitOfWork,
            IGPMB_KeHoachService gpmb_KeHoachService)
        {
            _GPMB_KeHoachCongViecRepository = GPMB_KeHoachCongViecRepository;
            _DuAnRepository = duAnRepository;
            _GPMB_CTCVRepository = GPMB_CTCVRepository;
            _IGPMB_KeHoachRepository = IGPMB_KeHoachRepository;

            this._unitOfWork = unitOfWork;
            this._GPMB_KeHoachService = gpmb_KeHoachService;
        }

        public GPMB_KeHoachCongViec Add(GPMB_KeHoachCongViec gpmb_KeHoachCongViec)
        {
            return _GPMB_KeHoachCongViecRepository.Add(gpmb_KeHoachCongViec);
        }

        public GPMB_KeHoachCongViec Delete(int id)
        {
            return _GPMB_KeHoachCongViecRepository.Delete(id);
        }

        public void DeleteByIdKeHoachGPMB(int idKH)
        {
            _GPMB_KeHoachCongViecRepository.DeleteMulti(x => x.IdKeHoachGPMB == idKH);
        }

        public IEnumerable<GPMB_KeHoachCongViec> GetAll()
        {
            return _GPMB_KeHoachCongViecRepository.GetAll(new[] { "GPMB_CongViec" });
        }

        public IEnumerable<GPMB_KeHoachCongViec> GetAllID(int idKH)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GPMB_KeHoachCongViec> GetByFilter(int idKeHoach, int page, int pageSize, out int totalRow, string filter)
        {
            throw new NotImplementedException();
        }

        public GPMB_KeHoachCongViec GetById(int id)
        {
            return _GPMB_KeHoachCongViecRepository.GetSingleByCondition(x => x.IdKeHoachCongViec == id, new[] { "GPMB_CongViec" });
        }

        public IEnumerable<GPMB_KeHoachCongViec> GetListByIdKeHoach(int idKH)
        {
            return _GPMB_KeHoachCongViecRepository.GetGPMB_KeHoachCongViec().Where(x => x.IdKeHoachGPMB == idKH);
        }

        private List<DateTime> listDateFromTo(DateTime startDate, DateTime endDate)
        {
            List<DateTime> reLstDate = new List<DateTime>();
            for (DateTime d = startDate; d <= endDate; d = d.AddDays(1))
            {
                reLstDate.Add(d);
            }
            return reLstDate;
        }
        private void getFullNameUser()
        {

        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KeHoachCongViec gpmb_KeHoachCongViec)
        {
            _GPMB_KeHoachCongViecRepository.Update(gpmb_KeHoachCongViec);
        }
    }
}
