﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.BCModel.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_CTCV_DotService
    {
        GPMB_CTCV_Dot Add(GPMB_CTCV_Dot GPMB_CTCV_Dot);

        void Update(GPMB_CTCV_Dot GPMB_CTCV_Dot);

        GPMB_CTCV_Dot Delete(int id);

        IEnumerable<GPMB_CTCV_Dot> GetAll(int idCVCT);

        GPMB_CTCV_Dot GetByID(int id);

        IEnumerable<GPMB_CTCV_Dot> GetAllByIDCVCT(int idCVCT, string filter = null);

        GPMB_CTCV_Dot GetById(int id);

        void Save();
    }

    public class GPMB_CTCV_DotService : IGPMB_CTCV_DotService
    {
        private IGPMB_CTCV_DotRepository _GPMB_CTCV_DotRepository;
        private IGPMB_KeHoachRepository _GPMB_KeHoachRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_CTCV_DotService(IGPMB_CTCV_DotRepository GPMB_CTCV_DotRepository, IGPMB_KeHoachRepository GPMB_KeHoachRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_CTCV_DotRepository = GPMB_CTCV_DotRepository;
            this._GPMB_KeHoachRepository = GPMB_KeHoachRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_CTCV_Dot Add(GPMB_CTCV_Dot GPMB_CTCV_Dot)
        {
            return _GPMB_CTCV_DotRepository.Add(GPMB_CTCV_Dot);
        }

        public GPMB_CTCV_Dot Delete(int id)
        {
            return _GPMB_CTCV_DotRepository.Delete(id);
        }

        public IEnumerable<GPMB_CTCV_Dot> GetAll(int idCVCT)
        {
            return _GPMB_CTCV_DotRepository.GetGPMB_CTCV_Dot().Where(x => x.IdChiTietCongViec == idCVCT);
        }

        public IEnumerable<GPMB_CTCV_Dot> GetAllByIDCVCT(int idCVCT, string filter = null)
        {
            var query = _GPMB_CTCV_DotRepository.GetMulti(x => x.IdChiTietCongViec == idCVCT, new[] { "GPMB_CTCV" });
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()));
            }
            return query;
        }

        public GPMB_CTCV_Dot GetById(int id)
        {
            return _GPMB_CTCV_DotRepository.GetGPMB_CTCV_Dot().Where(x => x.IdChiTietCongViec_Dot == id).SingleOrDefault();
        }
        public GPMB_CTCV_Dot GetByID(int id)
        {
            return _GPMB_CTCV_DotRepository.GetSingleByCondition(x => x.IdChiTietCongViec_Dot == id, new[] { "GPMB_CTCV" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_CTCV_Dot GPMB_CTCV_Dot)
        {
            _GPMB_CTCV_DotRepository.Update(GPMB_CTCV_Dot);
        }
    }
}
