﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_KhoKhanVuongMacService
    {
        GPMB_KhoKhanVuongMac Add(GPMB_KhoKhanVuongMac gpmb_KhoKhanVuongMac);

        void Update(GPMB_KhoKhanVuongMac gpmb_KhoKhanVuongMac);

        GPMB_KhoKhanVuongMac Delete(int id);

        IEnumerable<GPMB_KhoKhanVuongMac> GetByIdKeHoach(int idKH);

        GPMB_KhoKhanVuongMac GetById(int id);

        object GetKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser);
        void Save();
    }

    public class GPMB_KhoKhanVuongMacService : IGPMB_KhoKhanVuongMacService
    {
        private IGPMB_KhoKhanVuongMacRepository _GPMB_KhoKhanVuongMacRepository;
        private IGPMB_KeHoachRepository _GPMB_KeHoachRepository;
        private IUnitOfWork _unitOfWork;
        public GPMB_KhoKhanVuongMacService(IGPMB_KhoKhanVuongMacRepository GPMB_KhoKhanVuongMacRepository,
            IGPMB_KeHoachRepository GPMB_KeHoachRepository,
            IUnitOfWork unitOfWork)
        {
            this._GPMB_KhoKhanVuongMacRepository = GPMB_KhoKhanVuongMacRepository;
            _GPMB_KeHoachRepository = GPMB_KeHoachRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_KhoKhanVuongMac Add(GPMB_KhoKhanVuongMac gpmb_KhoKhanVuongMac)
        {
            return _GPMB_KhoKhanVuongMacRepository.Add(gpmb_KhoKhanVuongMac);
        }

        public GPMB_KhoKhanVuongMac Delete(int id)
        {
            return _GPMB_KhoKhanVuongMacRepository.Delete(id);
        }

        public IEnumerable<GPMB_KhoKhanVuongMac> GetByIdKeHoach(int idKH)
        {
            return _GPMB_KhoKhanVuongMacRepository.GetGPMB_KhoKhanVuongMac().Where(x => x.IdKeHoachGPMB == idKH);
        }

        public GPMB_KhoKhanVuongMac GetById(int id)
        {
            return _GPMB_KhoKhanVuongMacRepository.GetSingleByCondition(x => x.IdKhoKhanVuongMac == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KhoKhanVuongMac gpmb_KhoKhanVuongMac)
        {
            _GPMB_KhoKhanVuongMacRepository.Update(gpmb_KhoKhanVuongMac);
        }

        public object GetKhoKhanVuongMacDieuHanh(string userId, int idNhomDuAnTheoUser)
        {
            var khPD = _GPMB_KeHoachRepository.GetMulti(w => w.TinhTrang == 3).Select(x => new
            {
                IDKeHoach = x.IdKeHoachGPMB,
                IDDA = x.IdDuAn,
            });
            
            var grpKHPD = khPD.GroupBy(g => new { g.IDDA })
                .Select(x => new
                {
                    KH = x.Select(y => new
                    {
                        IDKH = y.IDKeHoach
                    }).OrderByDescending(a => a.IDKH).Select(a => a.IDKH).FirstOrDefault()
                }).Select(x => x.KH).ToList();

            var datas = _GPMB_KhoKhanVuongMacRepository.GetMulti(x => x.DaGiaiQuyet == false && grpKHPD.Contains(x.IdKeHoachGPMB), new string[] { "GPMB_KeHoach", "GPMB_KeHoach.DuAn", "GPMB_KeHoach.DuAn.DuAnUsers" })
                .Where(x => x.GPMB_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == userId).Count() > 0).GroupBy(x => new { x.GPMB_KeHoach.IdDuAn, x.GPMB_KeHoach.DuAn.TenDuAn }).Select(s => new
                {
                    IdDuAn = s.Key.IdDuAn.ToString(),
                    TenDuAn = s.Key.TenDuAn.ToString(),
                    grpGiaiDoan = s.GroupBy(g => new
                    {
                        g.IdKeHoachGPMB,
                    }).Select(y => new
                    {
                        IDKH = y.Key.IdKeHoachGPMB,
                        NoiDung = y.Select(z => z.GPMB_KeHoach.NoiDung).FirstOrDefault(),
                        total = y.Count()
                    }).ToList()
                }).ToList();

            return datas;
        }
    }
}
