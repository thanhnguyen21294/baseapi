﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.DieuHanhModel;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_KeHoachService
    {
        GPMB_KeHoach Add(GPMB_KeHoach gpmb_KeHoach);

        void Update(GPMB_KeHoach gpmb_KeHoach);

        GPMB_KeHoach Delete(int id);

        IEnumerable<GPMB_KeHoach> GetAll(int idDuAn, int? idNhomDuAnTheoUser, string filter);

        IEnumerable<GPMB_KeHoach> GetAll(string filter);

        IEnumerable<GPMB_KeHoach> GetAllForHistory();

        IEnumerable<GPMB_KeHoach> GetByFilter(int idDA, int? idNhomDuAnTheoUser, int page, int pageSize, string sort, out int totalRow, string filter);

        GPMB_KeHoach GetById(int id);

        IEnumerable<GPMB_KeHoach> GetAll();

        void Save();

        IEnumerable<BanLVPheDuyetKHTD> Get_KeHoachMoiNhat_DuoiMucPhoGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter);
    }

    public class GPMB_KeHoachService : IGPMB_KeHoachService
    {
        private IGPMB_KeHoachRepository _GPMB_KeHoachRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_KeHoachService(IGPMB_KeHoachRepository gpmb_KeHoachRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_KeHoachRepository = gpmb_KeHoachRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_KeHoach Add(GPMB_KeHoach gpmb_KeHoach)
        {
            return _GPMB_KeHoachRepository.Add(gpmb_KeHoach);
        }

        public GPMB_KeHoach Delete(int id)
        {
            return _GPMB_KeHoachRepository.Delete(id);
        }

        public IEnumerable<GPMB_KeHoach> GetAllForHistory()
        {
            return _GPMB_KeHoachRepository.GetAll(new[] { "DuAn" }).OrderByDescending(x => x.IdKeHoachGPMB);
        }

        public IEnumerable<GPMB_KeHoach> GetAll(int idDuAn, int? idNhomDuAnTheoUser, string filter)
        {
            var query = _GPMB_KeHoachRepository.GetGPMB_KeHoach();
            if (idNhomDuAnTheoUser != null)
            {
                query = query.Where(x => x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
            }
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.IdDuAn == idDuAn)
                    .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang);
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDuAn)
                    .OrderByDescending(o => o.TinhTrang).OrderByDescending(o => o.NgayTinhTrang);
            }
            return query;
        }

        public IEnumerable<GPMB_KeHoach> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _GPMB_KeHoachRepository.GetMulti(x => x.NoiDung.Contains(filter));
            }
            return _GPMB_KeHoachRepository.GetGPMB_KeHoach();
        }

        public IEnumerable<GPMB_KeHoach> GetAll()
        {
            return _GPMB_KeHoachRepository.GetGPMB_KeHoach();
        }

        public IEnumerable<GPMB_KeHoach> GetByFilter(int idDuAn, int? idNhomDuAnTheoUser, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _GPMB_KeHoachRepository.GetGPMB_KeHoach();
            if (filter != null)
            {
                //query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
                query = query.Where(x => x.NoiDung.Contains(filter) && x.IdDuAn == idDuAn);
            }
            else
            {
                //query = query.Where(x => x.IdDuAn == idDuAn && x.IdGiaiDoan == idGiaiDoan && x.IdNhomDuAnTheoUser == idNhomDuAnTheoUser);
                query = query.Where(x => x.IdDuAn == idDuAn);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdKeHoachGPMB).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public GPMB_KeHoach GetById(int id)
        {
            return _GPMB_KeHoachRepository.GetSingleByCondition(x => x.IdKeHoachGPMB == id, new string[] { "GPMB_KeHoachCongViecs", "GPMB_KeHoachCongViecs.GPMB_CongViec" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KeHoach gpmb_KeHoach)
        {
            _GPMB_KeHoachRepository.Update(gpmb_KeHoach);
        }

        public IEnumerable<BanLVPheDuyetKHTD> Get_KeHoachMoiNhat_DuoiMucPhoGiamDocPheDuyet_DieuHanh(int idNhomDuAnTheoUser, string idUser, int nTrangThai, string filter)
        {
            var query = _GPMB_KeHoachRepository.GetMulti(x => x.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0,
                new[] { "GPMB_KeHoachCongViecs", "DuAn", "DuAn.DuAnUsers" });

            if (filter != null)
            {
                query = query.Where(x => x.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()));
            }

            IEnumerable<int> idskehoach;

            idskehoach = _GPMB_KeHoachRepository.GetAll().Select(y => new
            {
                IdKeHoachGPMB = y.IdKeHoachGPMB,
                IdDuAn = y.IdDuAn
            }).GroupBy(x => new { x.IdDuAn }).Select(x => x.OrderByDescending(y => y.IdKeHoachGPMB).Select(y => y.IdKeHoachGPMB).FirstOrDefault());

            var lst = query.Where(x => idskehoach.Contains(x.IdKeHoachGPMB) && x.TrangThai < 3 && x.TrangThai == nTrangThai)
                .Select(x => new BanLVPheDuyetKHTD()
                {
                    IDKH = x.IdKeHoachGPMB,
                    KH = x.NoiDung,
                    IDDA = x.IdDuAn,
                    DuAn = x.DuAn.TenDuAn,
                    TinhTrang = x.TinhTrang,
                    TrangThai = x.TrangThai,
                    gpmb_kehoachcongviec = x.GPMB_KeHoachCongViecs
                });

            return lst;
        }
    }
}