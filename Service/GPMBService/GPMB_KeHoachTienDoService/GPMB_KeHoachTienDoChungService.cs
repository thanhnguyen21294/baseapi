﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_KeHoachTienDoChungService
    {
        GPMB_KeHoachTienDoChung Add(GPMB_KeHoachTienDoChung gpmb_KeHoachTienDoChung);

        void Update(GPMB_KeHoachTienDoChung gpmb_KeHoachTienDoChung);

        GPMB_KeHoachTienDoChung Delete(int id);

        GPMB_KeHoachTienDoChung GetByIdKeHoach(int idKH);

        GPMB_KeHoachTienDoChung GetById(int id);

        void Save();
    }

    public class QLTD_KeHoachTienDoChungService : IGPMB_KeHoachTienDoChungService
    {
        private IGPMB_KeHoachTienDoChungRepository _GPMB_KeHoachTienDoChungRepository;
        private IUnitOfWork _unitOfWork;
        public QLTD_KeHoachTienDoChungService(IGPMB_KeHoachTienDoChungRepository GPMB_KeHoachTienDoChungRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_KeHoachTienDoChungRepository = GPMB_KeHoachTienDoChungRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_KeHoachTienDoChung Add(GPMB_KeHoachTienDoChung gpmb_KeHoachTienDoChung)
        {
            return _GPMB_KeHoachTienDoChungRepository.Add(gpmb_KeHoachTienDoChung);
        }

        public GPMB_KeHoachTienDoChung Delete(int id)
        {
            return _GPMB_KeHoachTienDoChungRepository.Delete(id);
        }

        public GPMB_KeHoachTienDoChung GetByIdKeHoach(int idKH)
        {
            return _GPMB_KeHoachTienDoChungRepository.GetGPMB_KeHoachTienDoChung().Where(x => x.IdKeHoachGPMB == idKH).FirstOrDefault();
        }

        public GPMB_KeHoachTienDoChung GetById(int id)
        {
            return _GPMB_KeHoachTienDoChungRepository.GetSingleByCondition(x => x.IdKeHoachTienDoChung == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KeHoachTienDoChung gpmb_KeHoachTienDoChung)
        {
            _GPMB_KeHoachTienDoChungRepository.Update(gpmb_KeHoachTienDoChung);
        }
    }
}
