﻿using Data.Infrastructure;
using Data.Repositories.GPMB.GPMB_KeHoachTienDo;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService.GPMB_KeHoachTienDoService
{
    public interface IGPMB_CongViecService
    {
        GPMB_CongViec Add(GPMB_CongViec gpmb_CongViec);

        void Update(GPMB_CongViec gpmb_CongViec);

        GPMB_CongViec Delete(int id);

        IEnumerable<GPMB_CongViec> GetAll();

        GPMB_CongViec GetById(int id);

        void Save();
    }

    public class QLTD_CongViecService : IGPMB_CongViecService
    {
        private IGPMB_CongViecRepository _GPMB_CongViecRepository;
        private IUnitOfWork _unitOfWork;

        public QLTD_CongViecService(IGPMB_CongViecRepository GPMB_CongViecRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_CongViecRepository = GPMB_CongViecRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_CongViec Add(GPMB_CongViec gpmb_CongViec)
        {
            return _GPMB_CongViecRepository.Add(gpmb_CongViec);
        }

        public GPMB_CongViec Delete(int id)
        {
            return _GPMB_CongViecRepository.Delete(id);
        }

        public IEnumerable<GPMB_CongViec> GetAll()
        {
            return _GPMB_CongViecRepository.GetGPMB_CongViec()/*.OrderBy(o => o.Order)*/;
        }

        public GPMB_CongViec GetById(int id)
        {
            return _GPMB_CongViecRepository.GetMulti(x => x.IdCongViecGPMB == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_CongViec gpmb_CongViec)
        {
            _GPMB_CongViecRepository.Update(gpmb_CongViec);
        }
    }
}
