﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService
{
    public interface IGPMB_CommentService
    {
        GPMB_Comment Add(GPMB_Comment Comments);

        void Update(GPMB_Comment id);

        GPMB_Comment Delete(int id);

        IEnumerable<GPMB_Comment> GetAll();

        IEnumerable<GPMB_Comment> GetByIdDuAn(int idDuAn);

        GPMB_Comment GetById(int id);

        void Save();
    }

    public class GPMB_CommentService : IGPMB_CommentService
    {
        private IGPMB_CommentRepository _GPMB_CommentsRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_CommentService(IGPMB_CommentRepository GPMB_CommentsRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_CommentsRepository = GPMB_CommentsRepository;
            this._unitOfWork = unitOfWork;
        }
        public GPMB_Comment Add(GPMB_Comment Comments)
        {
            return _GPMB_CommentsRepository.Add(Comments);
        }

        public GPMB_Comment Delete(int id)
        {
            return _GPMB_CommentsRepository.Delete(id);
        }

        public IEnumerable<GPMB_Comment> GetAll()
        {
            return _GPMB_CommentsRepository.GetAll(new[] { "DuAns" });
        }

        public IEnumerable<GPMB_Comment> GetByIdDuAn(int idDuAn)
        {
            var query = _GPMB_CommentsRepository.GetAll(new string[] { "AppUsers" }).Where(x => x.IdDuAn == idDuAn);
            return query;
        }

        public GPMB_Comment GetById(int id)
        {
            return _GPMB_CommentsRepository.GetMulti(x => x.IdComment == id, new string[] { "AppUsers" }).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_Comment Comments)
        {
            _GPMB_CommentsRepository.Update(Comments);
        }
    }
}
