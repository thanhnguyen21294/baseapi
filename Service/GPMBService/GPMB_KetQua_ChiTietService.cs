﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService
{
    public interface IGPMB_KetQua_ChiTietService
    {
        GPMB_KetQua_ChiTiet Add(GPMB_KetQua_ChiTiet qltd_gpmb_tongQuanKetQua);

        void Update(GPMB_KetQua_ChiTiet qltd_gpmb_tongQuanKetQua);

        GPMB_KetQua_ChiTiet Delete(int id);

        IEnumerable<GPMB_KetQua_ChiTiet> GetAll(int idDuAn);

        GPMB_KetQua_ChiTiet GetById(int id);

        IEnumerable<GPMB_KetQua_ChiTiet> GetByFilter(int idGPMBKetQua, int page, int pageSize, out int totalRow);

        void Save();
    }

    public class GPMB_KetQua_ChiTietService : IGPMB_KetQua_ChiTietService
    {
        private IGPMB_KetQua_ChiTietRepository _GPMB_KetQua_ChiTietRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_KetQua_ChiTietService(IGPMB_KetQua_ChiTietRepository GPMB_KetQua_ChiTietRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_KetQua_ChiTietRepository = GPMB_KetQua_ChiTietRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_KetQua_ChiTiet Add(GPMB_KetQua_ChiTiet qltd_gpmb_tongQuanKetQua)
        {
            return _GPMB_KetQua_ChiTietRepository.Add(qltd_gpmb_tongQuanKetQua);
        }

        public GPMB_KetQua_ChiTiet Delete(int id)
        {
            return _GPMB_KetQua_ChiTietRepository.Delete(id);
        }

        public IEnumerable<GPMB_KetQua_ChiTiet> GetAll(int idGPMB_KetQua)
        {
            return _GPMB_KetQua_ChiTietRepository.GetGPMB_KetQua_ChiTiet().Where(x => x.IdGPMBKetQua == idGPMB_KetQua);
        }

        public GPMB_KetQua_ChiTiet GetById(int idGPMBKetQuaChiTiet)
        {
            return _GPMB_KetQua_ChiTietRepository.GetSingleById(idGPMBKetQuaChiTiet);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KetQua_ChiTiet qltd_gpmb_ketqua_chitiet)
        {
            _GPMB_KetQua_ChiTietRepository.Update(qltd_gpmb_ketqua_chitiet);
        }

        public IEnumerable<GPMB_KetQua_ChiTiet> GetByFilter(int idGPMBKetQua, int page, int pageSize, out int totalRow)
        {
            var query = _GPMB_KetQua_ChiTietRepository.GetGPMB_KetQua_ChiTiet().Where(x => x.IdGPMBKetQua == idGPMBKetQua);
            totalRow = query.Count();
            return query.OrderByDescending(x => x.CreatedDate).Skip((page - 1) * pageSize).Take(pageSize);
        }
    }
}
