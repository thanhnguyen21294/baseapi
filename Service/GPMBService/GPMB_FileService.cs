﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models;
using Model.Models.DieuHanhModel;
using Model.Models.QLTD.GPMB;
using System.Collections.Generic;
using System.Linq;

namespace Service.GPMBService
{
    public interface IGPMB_FileService
    {
        IEnumerable<GPMB_File> GetAll();
        IEnumerable<GPMB_File> GetAllAndSearch(string filter);
        Dictionary<string, object> GetMobileAllAndSearch(string filter, int item, int Numberpage, int page);
        int GetCountAllAndSearch(string filter, int item);
        IEnumerable<GPMB_File> GetListPaging(string filter, out int total, int pageIndex, int pageSize, bool isWeekChecked, bool isMonthChecked, bool isEmployee, int role);
        GPMB_File GetById(int id);
        void Save();
        GPMB_File Add(GPMB_File gpmb_File);
        void Update(GPMB_File gpmb_File);
        GPMB_File Delete(int id);
        IEnumerable<BanLVFilePheDuyet> Get_GPMB_File_ChuaPheDuyet(int nTrangThai, string filter);
    }

    public class GPMB_FileService : IGPMB_FileService
    {
        private IGPMB_FileRepository _GPMB_FileRespository;
        private IUnitOfWork _unitOfWork;

        public GPMB_FileService(IGPMB_FileRepository gpmb_FileRespository, IUnitOfWork unitOfWork)
        {
            _GPMB_FileRespository = gpmb_FileRespository;
            _unitOfWork = unitOfWork;
        }

        public GPMB_File Add(GPMB_File gpmb_File)
        {
            return _GPMB_FileRespository.Add(gpmb_File);
        }

        public void Update(GPMB_File gpmb_File)
        {
            _GPMB_FileRespository.Update(gpmb_File);
        }

        public GPMB_File Delete(int id)
        {
            return _GPMB_FileRespository.Delete(id);
        }

        public IEnumerable<GPMB_File> GetAll()
        {
            return _GPMB_FileRespository.GetAll(new string[] { "AppUser" });
        }

        public IEnumerable<GPMB_File> GetAllAndSearch(string filter)
        {
            var listGPMBFile = _GPMB_FileRespository.GetAll(new string[] { "AppUser" }).ToList();
            if (filter != null)
            {
                var listSearch = new List<GPMB_File>();
                var listParent = listGPMBFile.Where(x => x.NoiDung.Contains(filter) && x.ParentId == null).ToList();
                var listChild = listGPMBFile.Where(x => x.NoiDung.Contains(filter) && x.ParentId != null).ToList();
                var listChildAll = listGPMBFile.Where(x => x.ParentId != null).ToList();
                var listParentAll = listGPMBFile.Where(x => x.ParentId == null).ToList();
                foreach (var parent in listParent)
                {
                    foreach (var child in listChildAll)
                    {
                        if (child.ParentId == parent.Id && listChild.Where(x => x.Id == child.Id).SingleOrDefault() == null)
                        {
                            listSearch.Add(child);
                        }
                    }
                }
                foreach (var child in listChild)
                {
                    foreach (var parent in listParentAll)
                    {
                        if (child.ParentId == parent.Id && listParent.Where(x => x.Id == parent.Id).SingleOrDefault() == null)
                        {
                            listSearch.Add(parent);
                        }
                    }
                }
                listSearch = listSearch.Concat(listChild).Concat(listParent).ToList();
                return listSearch.OrderByDescending(x => x.NgayTinhTrang);
            }

            return listGPMBFile.OrderByDescending(x => x.NgayTinhTrang);
        }

        public IEnumerable<GPMB_File> GetListPaging(string filter, out int total, int pageIndex, int pageSize, bool isWeekChecked, bool isMonthChecked, bool isEmployee, int role)
        {
            var listGPMBFile = _GPMB_FileRespository.GetAll(new string[] { "AppUser" }).ToList();
            if (!isEmployee)
            {
                if (role == 1)
                {
                    listGPMBFile = listGPMBFile.Where(x => (x.TrangThai != -1 && x.TinhTrang == 1) || (x.TinhTrang == 3 && x.ParentId == null)).ToList();
                }
                if (role == 2)
                {
                    listGPMBFile = listGPMBFile.Where(x => (x.TrangThai != -1 && x.TinhTrang == 2) || (x.TinhTrang == 3 && x.ParentId == null)).ToList();
                }
            }
            if (filter != null)
            {
                var listSearch = new List<GPMB_File>();
                var listParentRemove = new List<GPMB_File>();
                var listParent = listGPMBFile.Where(x => x.NoiDung.Contains(filter) && x.ParentId == null).ToList();
                var listChild = listGPMBFile.Where(x => x.NoiDung.Contains(filter) && x.ParentId != null).ToList();
                var listChildAll = listGPMBFile.Where(x => x.ParentId != null).ToList();
                var listParentAll = listGPMBFile.Where(x => x.ParentId == null).ToList();
                foreach (var parent in listParent)
                {
                    var a = 0;
                    foreach (var child in listChildAll)
                    {
                        if (child.ParentId == parent.Id && child.TinhTrang == role)
                        {
                            a = a + 1;
                            listSearch.Add(child);
                        }
                    }
                    if (a == 0)
                    {
                        listParentRemove.Add(parent);
                    }
                }
                foreach (var child in listChild)
                {
                    foreach (var parent in listParentAll)
                    {
                        if (child.ParentId == parent.Id && listParent.Where(x => x.Id == parent.Id).SingleOrDefault() == null)
                        {
                            listSearch.Add(parent);
                        }
                    }
                }
                listGPMBFile = listSearch.Concat(listChild).Concat(listParent).Except(listParentRemove).ToList();
            }
            else
            {
                var listSearch = new List<GPMB_File>();
                var listParent = listGPMBFile.Where(x => x.ParentId == null).ToList();
                var listChild = listGPMBFile.Where(x => x.ParentId != null).ToList();
                var listChildAll = listGPMBFile.Where(x => x.ParentId != null).ToList();
                var listParentAll = listGPMBFile.Where(x => x.ParentId == null).ToList();
                foreach (var parent in listParent)
                {
                    foreach (var child in listChildAll)
                    {
                        if (child.ParentId == parent.Id && listChild.Where(x => x.Id == child.Id).SingleOrDefault() == null)
                        {
                            listSearch.Add(child);
                        }
                    }
                }
                foreach (var child in listChild)
                {
                    foreach (var parent in listParentAll)
                    {
                        if (child.ParentId == parent.Id && listParent.Where(x => x.Id == parent.Id).SingleOrDefault() == null)
                        {
                            listSearch.Add(parent);
                        }
                    }
                }
                listGPMBFile = listSearch.Concat(listChild).Concat(listParent).ToList();
            }
            if (isWeekChecked && !isMonthChecked)
            {
                listGPMBFile = listGPMBFile.Where(x => x.ParentId != null).ToList();
            }
            else if (!isWeekChecked && isMonthChecked)
            {
                listGPMBFile = listGPMBFile.Where(x => x.ParentId == null).ToList();
            }
            total = listGPMBFile.Count() / pageSize;
            if ((listGPMBFile.Count() % pageSize) > 0)
            {
                total++;
            }
            ////ListUser = listGPMBFile.GroupBy(x => x.AppUser.Id).Select(grp => grp.First().AppUser)
            ////    .Select(r => new AppUserName
            ////    {
            ////        Id = r.Id,
            ////        FullName = r.FullName
            ////    }).ToList();

            return listGPMBFile.OrderByDescending(x => x.NgayTinhTrang).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public GPMB_File GetById(int id)
        {
            return _GPMB_FileRespository.GetSingleByCondition(x => x.Id == id, new string[] { "AppUser" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<BanLVFilePheDuyet> Get_GPMB_File_ChuaPheDuyet(int nTrangThai, string filter)
        {
            var query = _GPMB_FileRespository.GetAll();
            if (filter != null)
            {
                query = query.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()));
            }

            var lst = query.Where(x => x.TrangThai < 3 && x.TrangThai == nTrangThai)
                .Select(x => new BanLVFilePheDuyet()
                {
                    IDFile = x.Id,
                    NoiDung = x.NoiDung,
                    TinhTrang = x.TinhTrang,
                    TrangThai = x.TrangThai
                });

            return lst;
        }
        // Duy
        public Dictionary<string, object> GetMobileAllAndSearch(string filter, int item, int Numberpage, int page)
        {
            var listGPMBFile = _GPMB_FileRespository.GetAll(new string[] { "AppUser" }).ToList();
            if (filter != null && filter != "")
            {
                var listSearchview = new List<GPMB_File>();
                var listSearch = new List<GPMB_File>();
                var listSearchview1 = new List<GPMB_File>();
                var listChaSearch = new List<GPMB_File>();
                var listChaNotSearch = new List<GPMB_File>();
                var listParentRemove = new List<GPMB_File>();
                var listParent = listGPMBFile.Where(x => x.ParentId == null && x.TinhTrang != item).ToList();
                var listChildAll = listGPMBFile.Where(x => x.ParentId != null && x.TinhTrang == item && x.TrangThai != -1).ToList();
                var listChild = listGPMBFile.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.ParentId != null && x.TinhTrang == item && x.TrangThai != -1).ToList();
                var listParentAllItem = listGPMBFile.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.ParentId == null && x.TinhTrang == item).ToList();
                var listParentAll = listGPMBFile.Where(x => x.NoiDung.ToLower().Contains(filter.ToLower()) && x.ParentId == null).ToList();
                foreach (var parent in listParentAll)
                {
                    var a = 0;
                    foreach (var child in listChildAll)
                    {
                        if (child.ParentId == parent.Id && child.TinhTrang == item)
                        {
                            a = a + 1;
                            listSearch.Add(child);
                        }
                    }
                    if (a != 0)
                    {
                        listParentRemove.Add(parent);
                    }
                }

                foreach (var parent in listParentAllItem)
                {
                    if (!listParentRemove.Contains(parent))
                    {
                        listChaSearch.Add(parent);
                    }
                }
                listChaSearch = listChaSearch.Concat(listParentRemove).ToList();
                foreach (var parent in listParent)
                {
                    var a = 0;
                    foreach (var child in listChild)
                    {
                        if (child.ParentId == parent.Id)
                        {
                            a = 1;
                        }
                    }
                    if (a == 1)
                    {
                        listChaSearch.Add(parent);
                    }
                }
                var results = (listChaSearch.OrderBy(x => x.Id)
              .Skip((page - 1) * Numberpage)
              .Take(Numberpage));
                foreach (var parent in results)
                {
                    var a = 0;
                    foreach (var child in listChildAll)
                    {
                        if (child.ParentId == parent.Id && child.TinhTrang == item)
                        {
                            if (a == 0)
                            {
                                parent.Listchild = true;
                                listSearchview1.Add(parent);
                                listChaNotSearch.Add(parent);
                                child.Listchild = false;
                                listSearchview1.Add(child);
                                a = a + 1;
                            }
                            else
                            {
                                child.Listchild = false;
                                listSearchview1.Add(child);
                                a = a + 1;
                            }
                        }
                    }
                    if (a == 0)
                    {
                        parent.Listchild = false;
                        listSearchview1.Add(parent);
                    }
                }

                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", listChaSearch.Count() % Numberpage > 0 ? ((listChaSearch.Count() / Numberpage) + 1) : (listChaSearch.Count() / Numberpage));
                dictionary2.Add("TotalItems", listChaSearch.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", listSearchview1);
                dictionary.Add("Page", dictionary2);
                return dictionary;
            }
            else
            {
                var listSearch = new List<GPMB_File>();
                var listChaSearch = new List<GPMB_File>();
                var listParent = listGPMBFile.Where(x => x.ParentId == null && x.TinhTrang != item).ToList();
                var listChild = listGPMBFile.Where(x => x.ParentId != null && x.TinhTrang == item && x.TrangThai != -1).ToList();
                var listParentAll = listGPMBFile.Where(x => x.ParentId == null && x.TinhTrang == item && x.TrangThai != -1).ToList();
                foreach (var parent in listParentAll)
                {
                    listChaSearch.Add(parent);
                }
                foreach (var parent in listParent)
                {
                    var a = 0;
                    foreach (var child in listChild)
                    {
                        if (child.ParentId == parent.Id)
                        {
                            a = 1;
                        }
                    }
                    if (a == 1)
                    {
                        listChaSearch.Add(parent);
                    }
                }
                var results = (listChaSearch.OrderBy(x => x.Id)
              .Skip((page - 1) * Numberpage)
              .Take(Numberpage));
                foreach (var parent in results)
                {
                    var a = 0;
                    foreach (var child in listChild)
                    {
                        if (child.ParentId == parent.Id)
                        {
                            if (a == 0)
                            {
                                parent.Listchild = true;
                                listSearch.Add(parent);
                            }
                            child.Listchild = false;
                            listSearch.Add(child);
                            a = a + 1;
                        }
                    }
                    if (a == 0)
                    {
                        parent.Listchild = false;
                        listSearch.Add(parent);
                    }
                }
                Dictionary<string, int> dictionary2 = new Dictionary<string, int>();
                dictionary2.Add("CurrentPage", page);
                dictionary2.Add("TotalItemPages", results.Count());
                dictionary2.Add("TotalPages", listChaSearch.Count() % Numberpage > 0 ? ((listChaSearch.Count() / Numberpage) + 1) : (listChaSearch.Count() / Numberpage));
                dictionary2.Add("TotalItems", listChaSearch.Count());

                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("Data", listSearch);
                dictionary.Add("Page", dictionary2);
                return dictionary;
            }
        }

        public int GetCountAllAndSearch(string filter, int item)
        {
            var listGPMBFile = _GPMB_FileRespository.GetAll(new string[] { "AppUser" }).ToList();
            var listChild = listGPMBFile.Where(x => x.TinhTrang == item).ToList();
            var Count = listChild.Count();
            return Count;
        }
    }

}
