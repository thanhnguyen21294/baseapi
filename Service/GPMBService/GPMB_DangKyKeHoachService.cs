﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService
{
    public interface IGPMB_DangKyKeHoachService
    {
        GPMB_DangKyKeHoach Add(GPMB_DangKyKeHoach gpmb_KetQua);

        void Update(GPMB_DangKyKeHoach gpmb_KetQua);

        GPMB_DangKyKeHoach Delete(int id);

        IEnumerable<GPMB_DangKyKeHoach> GetAll(int idDuAn);

        GPMB_DangKyKeHoach GetById(int id);

        void Save();
    }
    public class GPMB_DangKyKeHoachService : IGPMB_DangKyKeHoachService
    {
        private IGPMB_DangKyKeHoachRepository _GPMB_DangKyKeHoachRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_DangKyKeHoachService(IGPMB_DangKyKeHoachRepository gpmb_KetQuaRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_DangKyKeHoachRepository = gpmb_KetQuaRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_DangKyKeHoach Add(GPMB_DangKyKeHoach gpmb_KetQua)
        {
            return _GPMB_DangKyKeHoachRepository.Add(gpmb_KetQua);
        }

        public GPMB_DangKyKeHoach Delete(int id)
        {
            return _GPMB_DangKyKeHoachRepository.Delete(id);
        }

        public IEnumerable<GPMB_DangKyKeHoach> GetAll(int idDuAn)
        {
            return _GPMB_DangKyKeHoachRepository.GetGPMB_DangKyKeHoach().Where(x => x.IdDuAn == idDuAn);
        }

        public GPMB_DangKyKeHoach GetById(int idGPMBKetQua)
        {
            return _GPMB_DangKyKeHoachRepository.GetSingleById(idGPMBKetQua);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_DangKyKeHoach gpmb_KetQua)
        {
            _GPMB_DangKyKeHoachRepository.Update(gpmb_KetQua);
        }
    }
}
