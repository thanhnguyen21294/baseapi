﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService
{
    public interface IGPMB_TongQuanKetQuaService
    {
        GPMB_TongQuanKetQua Add(GPMB_TongQuanKetQua qltd_gpmb_tongQuanKetQua);

        void Update(GPMB_TongQuanKetQua qltd_gpmb_tongQuanKetQua);

        GPMB_TongQuanKetQua Delete(int id);

        IEnumerable<GPMB_TongQuanKetQua> GetAll(int idDuAn);
        IEnumerable<GPMB_TongQuanKetQua> GetAllForHistory();

        GPMB_TongQuanKetQua GetById(int id);

        void Save();

    }

    public class GPMB_TongQuanKetQuaService : IGPMB_TongQuanKetQuaService
    {
        private IGPMB_TongQuanKetQuaRepository _GPMB_TongQuanKetQuaRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_TongQuanKetQuaService(IGPMB_TongQuanKetQuaRepository qltd_CongViecRepository, IUnitOfWork unitOfWork)
        {
            this._GPMB_TongQuanKetQuaRepository = qltd_CongViecRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_TongQuanKetQua Add(GPMB_TongQuanKetQua qltd_gpmb_tongQuanKetQua)
        {
            return _GPMB_TongQuanKetQuaRepository.Add(qltd_gpmb_tongQuanKetQua);
        }

        public GPMB_TongQuanKetQua Delete(int id)
        {
            return _GPMB_TongQuanKetQuaRepository.Delete(id);
        }

        public IEnumerable<GPMB_TongQuanKetQua> GetAll(int idDuAn)
        {
            return _GPMB_TongQuanKetQuaRepository.GetGPMB_TongQuanKetQua().Where(x => x.IdDuAn == idDuAn);
        }

        public IEnumerable<GPMB_TongQuanKetQua> GetAllForHistory()
        {
            return _GPMB_TongQuanKetQuaRepository.GetAll(new[] { "DuAn" });
        }

        public GPMB_TongQuanKetQua GetById(int idGPMBKetQua)
        {
            return _GPMB_TongQuanKetQuaRepository.GetSingleById(idGPMBKetQua);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_TongQuanKetQua qltd_gpmb_tongQuanKetQua)
        {
            _GPMB_TongQuanKetQuaRepository.Update(qltd_gpmb_tongQuanKetQua);
        }
        
    }
}
