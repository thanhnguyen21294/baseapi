﻿using Data.Infrastructure;
using Data.Repositories.GPMB;
using Model.Models.BCModel;
using Model.Models.DieuHanhModel;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.GPMBService
{
    public interface IGPMB_KetQuaService
    {
        GPMB_KetQua Add(GPMB_KetQua gpmb_KetQua);

        void Update(GPMB_KetQua gpmb_KetQua);

        GPMB_KetQua Delete(int id);

        IEnumerable<GPMB_KetQua> GetAll(int IdGPMBTongQuanKetQua);

        //IEnumerable<GPMB_KetQua> GetAllHoanThanh(int idDuAn);

        GPMB_KetQua GetById(int id);

        void Save();

        IEnumerable<GPMB_BanLamViec> GetGPMB_BanLamViec(string filter);

        IEnumerable<BaoCaoGPMBTongHopKQ> GetBC_GPMB_TongHopKetQua(string IDDuAns);

        IEnumerable<BLV_Congviec_DangThucHien> GetBC_GPMB_CongViecDangThucHien(string idUser);

        IEnumerable<BLV_Congviec_DangThucHien> Page_GetBC_GPMB_CongViecDangThucHien(string idUser, string filter, Dictionary<string, string> dicUse);
    }

    public class GPMB_KetQuaService : IGPMB_KetQuaService
    {
        private IGPMB_KetQuaRepository _GPMB_KetQuaRepository;
        private IGPMB_KetQua_ChiTietRepository _GPMB_KetQua_ChiTietRepository;
        private IUnitOfWork _unitOfWork;

        public GPMB_KetQuaService(IGPMB_KetQuaRepository gpmb_KetQuaRepository,
            IGPMB_KetQua_ChiTietRepository GPMB_KetQua_ChiTietRepository,
            IUnitOfWork unitOfWork)
        {
            this._GPMB_KetQuaRepository = gpmb_KetQuaRepository;
            _GPMB_KetQua_ChiTietRepository = GPMB_KetQua_ChiTietRepository;
            this._unitOfWork = unitOfWork;
        }

        public GPMB_KetQua Add(GPMB_KetQua gpmb_KetQua)
        {
            return _GPMB_KetQuaRepository.Add(gpmb_KetQua);
        }

        public GPMB_KetQua Delete(int id)
        {
            return _GPMB_KetQuaRepository.Delete(id);
        }

        public IEnumerable<GPMB_KetQua> GetAll(int IdGPMBTongQuanKetQua)
        {
            return _GPMB_KetQuaRepository.GetGPMB_KetQua().Where(x => x.IDTongQuanKetQua == IdGPMBTongQuanKetQua);
        }

        //public IEnumerable<GPMB_KetQua> GetAllHoanThanh(int idDuAn)
        //{
        //    return _GPMB_KetQuaRepository.GetGPMB_KetQua().Where(x => /*x.IdDuAn == idDuAn &&*/ x.ChiTraTienNhanBanGiaoDat_DT != "" && x.ChiTraTienNhanBanGiaoDat_SoHo != 0);
        //}

        public GPMB_KetQua GetById(int idGPMBKetQua)
        {
            //return _GPMB_KeHoachRepository.GetSingleByCondition(x => x.IdKeHoachGPMB == id, new string[] { "GPMB_KeHoachCongViecs", "GPMB_KeHoachCongViecs.GPMB_CongViec" });
            //return _GPMB_KetQuaRepository.GetSingleById(idGPMBKetQua);
            return _GPMB_KetQuaRepository.GetSingleByCondition(x => x.IdGPMBKetQua == idGPMBKetQua, new string[] { "GPMB_TongQuanKetQua" });
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(GPMB_KetQua gpmb_KetQua)
        {
            _GPMB_KetQuaRepository.Update(gpmb_KetQua);
        }

        public IEnumerable<GPMB_BanLamViec> GetGPMB_BanLamViec(string filter)
        {
            var lstTongQuanKetQua = _GPMB_KetQuaRepository.GetAll(new[] { "GPMB_TongQuanKetQua.DuAn", "GPMB_TongQuanKetQua" }).ToList();

            if(filter != null)
            {
                lstTongQuanKetQua = lstTongQuanKetQua.Where(x => x.GPMB_TongQuanKetQua.DuAn.TenDuAn.ToLower().Contains(filter.ToLower())).ToList();
            }

            var lst = lstTongQuanKetQua.GroupBy(x => new { x.GPMB_TongQuanKetQua.IdDuAn })
                .Select(x => new GPMB_BanLamViec()
                {
                    IDDA = x.Key.IdDuAn,
                    TenDuAn = x.Select(y => y.GPMB_TongQuanKetQua.DuAn.TenDuAn).FirstOrDefault(),
                    TongQuan_TongDienTich = x.Select(y => y.GPMB_TongQuanKetQua.Tong_DT).FirstOrDefault(),
                    TongQuan_TongSoHo = x.Select(y => y.GPMB_TongQuanKetQua.Tong_SoHo).FirstOrDefault(),
                    DHT_DienTich = x.Select(y => y.DHT_DT).FirstOrDefault(),
                    DHT_SoHo = x.Select(y => y.DHT_SoHo).FirstOrDefault(),
                    KKVM_DienTich = x.Select(y => y.KKVM_DT).FirstOrDefault(),
                    KKVM_SoHo = x.Select(y => y.KKVM_SoHo).FirstOrDefault(),
                    KKVM_SoHo_DaPheChuaNhanTien = x.Select(y => y.KKVM_DaPheChuaNhanTien).FirstOrDefault(),
                    KKVM_SoHo_KhongHopTacDieuTra = x.Select(y => y.KKVM_KhongHopTac).FirstOrDefault(),
                    KKVM_SoHo_ChuaDieuTra = x.Select(y => y.KKVM_ChuaDieuTra).FirstOrDefault(),
                    GhiChu = x.Select(y => y.GhiChu).FirstOrDefault()
                });
            return lst;
        }

        public IEnumerable<BaoCaoGPMBTongHopKQ> GetBC_GPMB_TongHopKetQua(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lstTongQuanKetQua = _GPMB_KetQuaRepository.GetMulti(x => arrDuAn.Contains(x.GPMB_TongQuanKetQua.IdDuAn.ToString()) ,new[] { "GPMB_TongQuanKetQua.DuAn", "GPMB_TongQuanKetQua" }).ToList();

            var lst = lstTongQuanKetQua.GroupBy(x => new { x.GPMB_TongQuanKetQua.IdDuAn })
                .Select(x => new BaoCaoGPMBTongHopKQ()
                {
                    IDDA = x.Key.IdDuAn,
                    TenDuAn = x.Select(y => y.GPMB_TongQuanKetQua.DuAn.TenDuAn) == null ? "" : x.Select(y => y.GPMB_TongQuanKetQua.DuAn.TenDuAn).FirstOrDefault(),

                    TongQuan_TongDT = x.Select(y => y.GPMB_TongQuanKetQua.Tong_DT).FirstOrDefault(),
                    TongQuan_TongSH = x.Select(y => y.GPMB_TongQuanKetQua.Tong_SoHo).FirstOrDefault(),
                    TongQuan_NNCaThe_DT = x.Select(y => y.GPMB_TongQuanKetQua.DatNNCaTheTuSDOD_DT).FirstOrDefault(),
                    TongQuan_NNCaThe_SH = x.Select(y => y.GPMB_TongQuanKetQua.DatNNCaTheTuSDOD_SoHo).FirstOrDefault(),
                    TongQuan_NNCong_DT = x.Select(y => y.GPMB_TongQuanKetQua.DatNNCong_DT).FirstOrDefault(),
                    TongQuan_NNCong_SH = x.Select(y => y.GPMB_TongQuanKetQua.DatNNCong_SoHo).FirstOrDefault(),
                    TongQuan_DatKhac_DT = x.Select(y => y.GPMB_TongQuanKetQua.DatKhac_DT).FirstOrDefault(),
                    TongQuan_DatKhac_SH = x.Select(y => y.GPMB_TongQuanKetQua.DatKhac_SoHo).FirstOrDefault(),
                    TongQuan_MoMa = x.Select(y => y.GPMB_TongQuanKetQua.MoMa).FirstOrDefault(),

                    DaHT_DT = x.Select(y => y.DHT_DT).FirstOrDefault(),
                    DaHT_SH = x.Select(y => y.DHT_SoHo).FirstOrDefault(),
                    DaHT_TGT = x.Select(y => y.DHT_TongKinhPhi).FirstOrDefault(),
                    DaHT_NNCaThe_DT = x.Select(y => y.DHT_DatNNCaThe_DT).FirstOrDefault(),
                    DaHT_NNCaThe_SH = x.Select(y => y.DHT_DatNNCaThe_SoHo).FirstOrDefault(),
                    DaHT_NNCong_DT = x.Select(y => y.DHT_DatNNCong_DT).FirstOrDefault(),
                    DaHT_NNCong_SH = x.Select(y => y.DHT_DatNNCong_SoHo).FirstOrDefault(),
                    DaHT_DatKhac_DT = x.Select(y => y.DHT_DatKhac_DT).FirstOrDefault(),
                    DaHT_DatKhac_SH = x.Select(y => y.DHT_DatKhac_SoHo).FirstOrDefault(),
                    DaHT_MoMa = x.Select(y => y.DHT_MoMa).FirstOrDefault(),

                    CTHX_DT = x.Select(y => y.CTHX_TongDT).FirstOrDefault(),
                    CTHX_SH = x.Select(y => y.CTHX_TongSoHo).FirstOrDefault(),
                    CTHX_HTDTPA_DT = x.Select(y => y.CTHX_LapTrinhHoiDong_DuThao_DT).FirstOrDefault(),
                    CTHX_HTDTPA_SH = x.Select(y => y.CTHX_LapTrinhHoiDong_DuThao_SoHo).FirstOrDefault(),
                    CTHX_CKDTPA_DT = x.Select(y => y.CTHX_CongKhai_DuThao_DT).FirstOrDefault(),
                    CTHX_CKDTPA_SH = x.Select(y => y.CTHX_CongKhai_DuThao_SoHo).FirstOrDefault(),
                    CTHX_HTPACT_DT = x.Select(y => y.CTHX_TrinhHoiDong_ChinhThuc_DT).FirstOrDefault(),
                    CTHX_HTPACT_SH = x.Select(y => y.CTHX_TrinhHoiDong_ChinhThuc_SoHo).FirstOrDefault(),
                    CTHX_QDPDDA_DT = x.Select(y => y.CTHX_QuyetDinhThuHoi_DT).FirstOrDefault(),
                    CTHX_QDPDDA_SH = x.Select(y => y.CTHX_QuyetDinhThuHoi_SoHo).FirstOrDefault(),
                    CTHX_NBGD_DT = x.Select(y => y.CTHX_ChiTraTien_DT).FirstOrDefault(),
                    CTHX_NBGD_SH = x.Select(y => y.CTHX_ChiTraTien_SoHo).FirstOrDefault(),

                    KKVM_DT = x.Select(y => y.KKVM_DT).FirstOrDefault(),
                    KKVM_SH = x.Select(y => y.KKVM_SoHo).FirstOrDefault(),
                    KKVM_SH_CNT = x.Select(y => y.KKVM_DaPheChuaNhanTien).FirstOrDefault(),
                    KKVM_SH_KHTDT = x.Select(y => y.KKVM_KhongHopTac).FirstOrDefault(),
                    KKVM_SH_CDT = x.Select(y => y.KKVM_ChuaDieuTra).FirstOrDefault(),
                    GhiChu = x.Select(y => y.GhiChu).FirstOrDefault()
                });
            return lst;
        }

        public IEnumerable<BLV_Congviec_DangThucHien> GetBC_GPMB_CongViecDangThucHien(string idUser)
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstDaBam = _GPMB_KetQua_ChiTietRepository.GetAll().Where(x =>
            DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year,
            ((DateTime)x.CreatedDate).Month,
            ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdGPMBKetQua).ToList().Distinct();

            var a = _GPMB_KetQuaRepository.GetMulti(x =>
            (x.GPMB_TongQuanKetQua.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
            && ((x.GPMB_TongQuanKetQua.Tong_DT > (x.DHT_DT.HasValue ? x.DHT_DT : 0)) || (x.GPMB_TongQuanKetQua.Tong_SoHo > (x.DHT_SoHo.HasValue ? x.DHT_SoHo : 0)))
            && !lstDaBam.Contains(x.IdGPMBKetQua)
            ),
                new[] { "GPMB_TongQuanKetQua", "GPMB_TongQuanKetQua.DuAn", "GPMB_TongQuanKetQua.DuAn.DuAnUsers" });

            var b = a.GroupBy(x => new { x.GPMB_TongQuanKetQua.IdDuAn, x.GPMB_TongQuanKetQua.DuAn.TenDuAn }).Select(x => new BLV_Congviec_DangThucHien()
            {
                IDDA = x.Key.IdDuAn,
                TenDuAn = x.Key.TenDuAn,
                IDCV = x.Select(y => y.IdGPMBKetQua).FirstOrDefault()
            });
            return b;
        }

        // Duy
        public IEnumerable<BLV_Congviec_DangThucHien> Page_GetBC_GPMB_CongViecDangThucHien(string idUser, string filter, Dictionary<string, string> dicUser)
        {
            DateTime now = DateTime.Today;
            DateTime comDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            var lstDaBam = _GPMB_KetQua_ChiTietRepository.GetAll().Where(x =>
            DateTime.Compare(new DateTime(((DateTime)x.CreatedDate).Year,
            ((DateTime)x.CreatedDate).Month,
            ((DateTime)x.CreatedDate).Day, 0, 0, 0), comDate) == 0).Select(x => x.IdGPMBKetQua).ToList().Distinct();

            var a = _GPMB_KetQuaRepository.GetMulti(x =>
            (x.GPMB_TongQuanKetQua.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
            && ((x.GPMB_TongQuanKetQua.Tong_DT > (x.DHT_DT.HasValue ? x.DHT_DT : 0)) || (x.GPMB_TongQuanKetQua.Tong_SoHo > (x.DHT_SoHo.HasValue ? x.DHT_SoHo : 0)))
            && !lstDaBam.Contains(x.IdGPMBKetQua)
            ),
                new[] { "GPMB_TongQuanKetQua", "GPMB_TongQuanKetQua.DuAn", "GPMB_TongQuanKetQua.DuAn.DuAnUsers" });

            var b = a.GroupBy(x => new { x.GPMB_TongQuanKetQua.IdDuAn, x.GPMB_TongQuanKetQua.DuAn.TenDuAn, x.GPMB_TongQuanKetQua.DuAn.IdNhomDuAnTheoUser, x.GPMB_TongQuanKetQua.DuAn.DuAnUsers }).Select(x => new BLV_Congviec_DangThucHien()
            {
                IDDA = x.Key.IdDuAn,
                TenDuAn = x.Key.TenDuAn,
                IdNhomDuAnTheoUser = x.Key.IdNhomDuAnTheoUser,
                DuAnUsers = string.Join("; ", dicUser.Keys.Select(h => ((x.Key.DuAnUsers.ToList().
                    Select(y => y.UserId)).Where(a1 => a1 == h).FirstOrDefault()))
                    .Select(z => z != null ? dicUser[z] : null).Where(k => k != null)),
                IDCV = x.Select(y => y.IdGPMBKetQua).FirstOrDefault()
            });
            if (filter != null)
            {
                b = b.Where(x => x.TenDuAn.ToLower().Contains(filter.ToLower()));
            }
            return b;
        }
    }
}
