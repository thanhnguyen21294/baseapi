﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IThuMucVanBanDenService
    {
        ThuMucVanBanDen Add(ThuMucVanBanDen ThuMucVanBanDen);

        void Update(ThuMucVanBanDen ThuMucVanBanDen);

        ThuMucVanBanDen Delete(int id);

        IEnumerable<ThuMucVanBanDen> GetAll();

        IEnumerable<ThuMucVanBanDen> GetByFilter(int page, int pageSize, out int totalRow);

        ThuMucVanBanDen GetById(int id);

        void Save();
    }

    public class ThuMucVanBanDenService : IThuMucVanBanDenService
    {
        private IThuMucVanBanDenRepository _ThuMucVanBanDenRepository;
        private IUnitOfWork _unitOfWork;

        public ThuMucVanBanDenService(IThuMucVanBanDenRepository ThuMucVanBanDenRepository, IUnitOfWork unitOfWork)
        {
            this._ThuMucVanBanDenRepository = ThuMucVanBanDenRepository;
            this._unitOfWork = unitOfWork;
        }
        public ThuMucVanBanDen Add(ThuMucVanBanDen ThuMucVanBanDen)
        {
            return _ThuMucVanBanDenRepository.Add(ThuMucVanBanDen);
        }

        public ThuMucVanBanDen Delete(int id)
        {
            return _ThuMucVanBanDenRepository.Delete(id);
        }

        public IEnumerable<ThuMucVanBanDen> GetAll()
        {
            return _ThuMucVanBanDenRepository.GetAll();
        }

        public IEnumerable<ThuMucVanBanDen> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _ThuMucVanBanDenRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdThuMucVanBanDen).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ThuMucVanBanDen GetById(int id)
        {
            return _ThuMucVanBanDenRepository.GetMulti(x => x.IdThuMucVanBanDen == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ThuMucVanBanDen ThuMucVanBanDen)
        {
            _ThuMucVanBanDenRepository.Update(ThuMucVanBanDen);
        }
    }
}
