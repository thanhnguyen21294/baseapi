﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IVanBanDenService
    {
        VanBanDen Add(VanBanDen VanBanDen);

        void Update(VanBanDen VanBanDen);

        VanBanDen Delete(int id);

        IEnumerable<VanBanDen> GetAll();

        IEnumerable<VanBanDen> GetByFilter(int page, int pageSize, out int totalRow);

        VanBanDen GetById(int id);

        void Save();
        IEnumerable<VanBanDen> GetVBByTM(int idTM, string filter, string uID);
        IEnumerable<VanBanDen> GetVanBanCham(string uID);
        int TotalVBByTM(int idTM, string uID);
    }

    public class VanBanDenService : IVanBanDenService
    {
        private IVanBanDenRepository _VanBanDenRepository;
        private IThuMucVanBanDenRepository _ThuMucVanBanDenRepository;
        private IUnitOfWork _unitOfWork;

        public VanBanDenService(IVanBanDenRepository VanBanDenRepository, IThuMucVanBanDenRepository ThuMucVanBanDenRepository, IUnitOfWork unitOfWork)
        {
            this._VanBanDenRepository = VanBanDenRepository;
            _ThuMucVanBanDenRepository = ThuMucVanBanDenRepository;
            this._unitOfWork = unitOfWork;
        }
        public IEnumerable<VanBanDen> GetVBByTM(int idTM, string filter, string uId)
        {
            List<VanBanDen> lstVBD = new List<VanBanDen>();
            if (idTM != 0)
            {
                List<int> lst = new List<int>();
                GetAllIDThuMucCon(idTM, lst);
                if (filter != null)
                    lstVBD = _VanBanDenRepository.GetMulti(x => lst.Contains(x.IdThuMucVanBanDen) && x.TenVanBanDen.Contains(filter) && (x.VanBanDenUsers.Where(y => y.UserId == uId).Count() > 0),
                        new string[] { "VanBanDenUsers" }).ToList();

                else
                    lstVBD = _VanBanDenRepository.GetMulti(x => lst.Contains(x.IdThuMucVanBanDen) && (x.VanBanDenUsers.Where(y => y.UserId == uId).Count() > 0), new string[] { "VanBanDenUsers" }).ToList();
            }
            else
            {
                if (filter != null)
                    lstVBD = _VanBanDenRepository.GetMulti(x => x.TenVanBanDen.Contains(filter) && (x.VanBanDenUsers.Where(y => y.UserId == uId).Count() > 0),
                        new string[] { "VanBanDenUsers" }).ToList();

                else
                    lstVBD = _VanBanDenRepository.GetMulti(x => (x.VanBanDenUsers.Where(y => y.UserId == uId).Count() > 0), new string[] { "VanBanDenUsers" }).ToList();
            }
            return lstVBD;
        }
        public IEnumerable<VanBanDen> GetVanBanCham(string uId)
        {
            List<VanBanDen> lstVBD = new List<VanBanDen>();
            lstVBD = _VanBanDenRepository.GetMulti(x => x.VanBanDenUsers.Where(y => y.UserId == uId).Count() > 0, new string[] { "VanBanDenUsers" }).ToList();
            return lstVBD;
        }
        public int TotalVBByTM(int idTM, string uID)
        {
            return totalVBDAByTM(idTM, uID);
        }

        private int totalVBDAByTM(int idTM, string uID)
        {
            List<VanBanDen> lstVBDA = new List<VanBanDen>();
            List<int> lst = new List<int>();
            GetAllIDThuMucCon(idTM, lst);
            lstVBDA = _VanBanDenRepository.GetMulti(x => lst.Contains(x.IdThuMucVanBanDen) && (x.VanBanDenUsers.Where(y => y.UserId == uID).Count() > 0), new string[] { "VanBanDenUsers" }).ToList();
            return lstVBDA.Count;
        }

        private void GetAllIDThuMucCon(int idTM, List<int> lstAllIdOfChildrenTM)
        {
            if (lstAllIdOfChildrenTM.Count == 0)
            {
                lstAllIdOfChildrenTM.Add(idTM);
            }

            IEnumerable<int> lstIdOfChildrenTM = _ThuMucVanBanDenRepository.GetMulti(x => x.IdThuMucCha == idTM).Where(X => X.IdThuMucCha == idTM).Select(x => x.IdThuMucVanBanDen);
            lstAllIdOfChildrenTM.AddRange(lstIdOfChildrenTM);
            foreach (var item in lstIdOfChildrenTM)
            {
                GetAllIDThuMucCon(item, lstAllIdOfChildrenTM);
            }

        }
        public VanBanDen Add(VanBanDen VanBanDen)
        {
            return _VanBanDenRepository.Add(VanBanDen);
        }

        public VanBanDen Delete(int id)
        {
            return _VanBanDenRepository.Delete(id);
        }

        public IEnumerable<VanBanDen> GetAll()
        {
            return _VanBanDenRepository.GetAll();
        }

        public IEnumerable<VanBanDen> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _VanBanDenRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdVanBanDen).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public VanBanDen GetById(int id)
        {
            try
            {
                return _VanBanDenRepository.GetMulti(x => x.IdVanBanDen == id, new string[] { "VanBanDenUsers" }).SingleOrDefault();
            }
            catch(Exception)
            {
                return null;
            }
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(VanBanDen VanBanDen)
        {
            _VanBanDenRepository.Update(VanBanDen);
        }
    }
}
