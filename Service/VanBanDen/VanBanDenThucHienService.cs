﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IVanBanDenThucHienService
    {
        VanBanDenThucHien Add(VanBanDenThucHien VanBanDenThucHien);

        void Update(VanBanDenThucHien VanBanDenThucHien);

        VanBanDenThucHien Delete(int id);

        IEnumerable<VanBanDenThucHien> GetAll();

        IEnumerable<VanBanDenThucHien> GetByFilter(int page, int pageSize, out int totalRow);

        VanBanDenThucHien GetById(int id);
        IEnumerable<VanBanDenThucHien> GetByIdVanBan(int id);

        void Save();
    }

    public class VanBanDenThucHienService : IVanBanDenThucHienService
    {
        private IVanBanDenThucHienRepository _VanBanDenThucHienRepository;
        private IUnitOfWork _unitOfWork;

        public VanBanDenThucHienService(IVanBanDenThucHienRepository VanBanDenThucHienRepository, IUnitOfWork unitOfWork)
        {
            this._VanBanDenThucHienRepository = VanBanDenThucHienRepository;
            this._unitOfWork = unitOfWork;
        }
        public VanBanDenThucHien Add(VanBanDenThucHien VanBanDenThucHien)
        {
            return _VanBanDenThucHienRepository.Add(VanBanDenThucHien);
        }

        public VanBanDenThucHien Delete(int id)
        {
            return _VanBanDenThucHienRepository.Delete(id);
        }
        public IEnumerable<VanBanDenThucHien> GetByIdVanBan(int id)
        {
            return _VanBanDenThucHienRepository.GetMulti(x => x.IdVanBanDen == id).OrderByDescending(x => x.CreatedDate);
        }
        public IEnumerable<VanBanDenThucHien> GetAll()
        {
            return _VanBanDenThucHienRepository.GetAll();
        }

        public IEnumerable<VanBanDenThucHien> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _VanBanDenThucHienRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdVanBanDenThucHien).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public VanBanDenThucHien GetById(int id)
        {
            return _VanBanDenThucHienRepository.GetMulti(x => x.IdVanBanDenThucHien == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(VanBanDenThucHien VanBanDenThucHien)
        {
            _VanBanDenThucHienRepository.Update(VanBanDenThucHien);
        }
    }
}
