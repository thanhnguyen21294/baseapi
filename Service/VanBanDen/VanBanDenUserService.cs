﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IVanBanDenUserService
    {
        VanBanDenUser Add(VanBanDenUser VanBanDenUser);

        void Update(VanBanDenUser VanBanDenUser);

        VanBanDenUser Delete(int id);

        IEnumerable<VanBanDenUser> GetAll();

        IEnumerable<VanBanDenUser> GetByFilter(int page, int pageSize, out int totalRow);

        VanBanDenUser GetById(int id);
        void DeleteByIdVanBan(int idVanBanDen);
        void Save();
    }

    public class VanBanDenUserService : IVanBanDenUserService
    {
        private IVanBanDenUserRepository _VanBanDenUserRepository;
        private IUnitOfWork _unitOfWork;

        public VanBanDenUserService(IVanBanDenUserRepository VanBanDenUserRepository, IUnitOfWork unitOfWork)
        {
            this._VanBanDenUserRepository = VanBanDenUserRepository;
            this._unitOfWork = unitOfWork;
        }
        public VanBanDenUser Add(VanBanDenUser VanBanDenUser)
        {
            return _VanBanDenUserRepository.Add(VanBanDenUser);
        }

        public VanBanDenUser Delete(int id)
        {
            return _VanBanDenUserRepository.Delete(id);
        }

        public void DeleteByIdVanBan(int idVanBanDen)
        {
            _VanBanDenUserRepository.DeleteMulti(x => x.IdVanBanDen == idVanBanDen);
        }

        public IEnumerable<VanBanDenUser> GetAll()
        {
            return _VanBanDenUserRepository.GetAll();
        }

        public IEnumerable<VanBanDenUser> GetByFilter(int page, int pageSize, out int totalRow)
        {
            var query = _VanBanDenUserRepository.GetAll();
            totalRow = query.Count();
            return query.OrderBy(x => x.IdVanBanDenUser).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public VanBanDenUser GetById(int id)
        {
            return _VanBanDenUserRepository.GetMulti(x => x.IdVanBanDenUser == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(VanBanDenUser VanBanDenUser)
        {
            _VanBanDenUserRepository.Update(VanBanDenUser);
        }
    }
}
