﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface IChuTruongDauTuService
    {
        ChuTruongDauTu Add(ChuTruongDauTu iChuTruongDauTu);

        void Update(ChuTruongDauTu iChuTruongDauTu);

        ChuTruongDauTu Delete(int id);

        IEnumerable<ChuTruongDauTu> GetAll();

        IEnumerable<ChuTruongDauTu> GetAll(string filter);

        IEnumerable<ChuTruongDauTu> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter);

        ChuTruongDauTu GetById(int id);

        void Save();
    }
    public class ChuTruongDauTuService : IChuTruongDauTuService
    {
        private IChuTruongDauTuRepository _ChuTruongDauTuRepository;
        private IUnitOfWork _unitOfWork;

        public ChuTruongDauTuService(IChuTruongDauTuRepository chuTruongDauTuRepository, IUnitOfWork unitOfWork)
        {
            this._ChuTruongDauTuRepository = chuTruongDauTuRepository;
            this._unitOfWork = unitOfWork;
        }
        public ChuTruongDauTu Add(ChuTruongDauTu chuTruongDauTu)
        {
            return _ChuTruongDauTuRepository.Add(chuTruongDauTu);
        }

        public ChuTruongDauTu Delete(int id)
        {
            return _ChuTruongDauTuRepository.Delete(id);
        }

        public IEnumerable<ChuTruongDauTu> GetAll()
        {
            return _ChuTruongDauTuRepository.GetChuTruongDauTu();
        }

        public IEnumerable<ChuTruongDauTu> GetAll(string filter)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                return _ChuTruongDauTuRepository.GetMulti(x => x.SoVanBan.Contains(filter));
            }
            return _ChuTruongDauTuRepository.GetChuTruongDauTu();
        }

        public IEnumerable<ChuTruongDauTu> GetByFilter(int idDA, int page, int pageSize, string sort, out int totalRow, string filter)
        {
            var query = _ChuTruongDauTuRepository.GetChuTruongDauTu();
            if (filter != null)
            {
                query = query.Where(x => x.SoVanBan.Contains(filter) && x.IdDuAn == idDA);
            }
            else
            {
                query = query.Where(x => x.IdDuAn == idDA);
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdChuTruongDauTu).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public ChuTruongDauTu GetById(int id)
        {
            return _ChuTruongDauTuRepository.GetMulti(x => x.IdChuTruongDauTu == id).SingleOrDefault();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ChuTruongDauTu updateChuTruongDauTu)
        {
            _ChuTruongDauTuRepository.Update(updateChuTruongDauTu);
        }
    }
}
