﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Service
{
    public interface INguonVonGoiThauTamUngService
    {
        NguonVonGoiThauTamUng Add(NguonVonGoiThauTamUng nguonVonGoiThauTamUng);
        void DeleteByIdTamUngHopDong(int IdQTDA);
        IEnumerable<NguonVonGoiThauTamUng> GetAll();
        IEnumerable<NguonVonGoiThauTamUng> GetByTamUngHopDong(int id);
        void Save();
    }

    public class NguonVonGoiThauTamUngService : INguonVonGoiThauTamUngService
    {
        private INguonVonGoiThauTamUngRepository _NguonVonGoiThauTamUngRepository;
        private IUnitOfWork _unitOfWork;

        public NguonVonGoiThauTamUngService(INguonVonGoiThauTamUngRepository nguonVonGoiThauTamUngRepository, IUnitOfWork unitOfWork)
        {
            this._NguonVonGoiThauTamUngRepository = nguonVonGoiThauTamUngRepository;
            this._unitOfWork = unitOfWork;
        }

        public NguonVonGoiThauTamUng Add(NguonVonGoiThauTamUng nguonvongoithauTamUng)
        {
            return _NguonVonGoiThauTamUngRepository.Add(nguonvongoithauTamUng);
        }

        public void DeleteByIdTamUngHopDong(int idTUHD)
        {
            _NguonVonGoiThauTamUngRepository.DeleteMulti(x => x.IdTamUng == idTUHD);
        }

        public IEnumerable<NguonVonGoiThauTamUng> GetAll()
        {
            return _NguonVonGoiThauTamUngRepository.GetAll();
        }

        public IEnumerable<NguonVonGoiThauTamUng> GetByTamUngHopDong(int id)
        {
            return _NguonVonGoiThauTamUngRepository.GetAll().Where(x => x.IdTamUng == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
