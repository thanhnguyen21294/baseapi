﻿using System;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Core;
using System.Web;

namespace Service
{
    public interface IConvertFileToHTMLService
    {
        object ConvertFile(string urlFile, string dirFile);
        bool DeleteCurentFile(string path);
    }
    public class ConvertFileToHTMLService : IConvertFileToHTMLService
    {
        public object refTrue = true;
        public object refFalse = false;
        public object refMissing = Type.Missing;
        string fileName = "";
        public object ConvertFile(string urlFile, string dirFile)
        {
            string hdfUrl = "";
            string hdfFolder = "";
            string path = HttpContext.Current.Server.MapPath("/");

            string exten = Path.GetExtension(urlFile).ToLower();
            fileName = Path.GetFileNameWithoutExtension(urlFile);

            //Kiểm tra trùng tên file. Tạo tên file mới
            string newFileName = fileName + exten;
            //while (Directory.Exists(HttpContext.Current.Server.MapPath("~" + "/page/" + newFileName)))
            //{
            //    ex = "(" + index.ToString() + ")";
            //    index++;
            //    newFileName = fileName + ex + exten;
            //}
            hdfFolder = newFileName;
            string targetDirectory = path + "page\\" + newFileName;

            //DeleteFolder(path + "page");

            string[] extension = new string[14] { ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".jpeg", ".jpg", ".tiff", ".gif", ".png", ".bmp" };
            bool check = false;
            for (int i = 0; i < extension.Length; i++)
            {
                if (exten.Equals(extension[i]))
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                return hdfUrl;
            }
            //Tạo thư mục chứa file convert

            switch (exten)
            {
                case ".doc":
                case ".docx":
                    Directory.CreateDirectory(targetDirectory);
                    WordToHTML(urlFile, targetDirectory);
                    hdfUrl = "/page/" + newFileName + "/" + newFileName + ".htm";
                    break;
                case ".xls":
                case ".xlsx":
                    Directory.CreateDirectory(targetDirectory);
                    ExcelToHTML(urlFile, targetDirectory);
                    DirectoryInfo drInfo = new DirectoryInfo(targetDirectory);
                    FileInfo[] files = drInfo.GetFiles(); //lay cac files
                    if (files.Length > 0)
                    {
                        hdfUrl = "/page/" + newFileName + "/" + files[0].Name;
                    }
                    break;
                default:
                    hdfUrl = urlFile.Replace(path, "");
                    break;
            }

            return new
            {
                path = hdfUrl,
                filename = newFileName,
                ext = exten
            };
        }
        private void ExcelToHTML(string refSourceFileName, string refTargetDirectory)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("vi-VN");
            Excel._Application excel = new Excel.Application();

            Excel.Workbook workbook = null;

            excel.Visible = false;
            excel.DisplayAlerts = false;

            try
            {
                workbook = excel.Workbooks.Open(refSourceFileName.ToString(),
                    refMissing, refTrue, refMissing, refMissing,
                    refMissing, refMissing, refMissing, refMissing,
                    refMissing, refMissing, refMissing, refMissing,
                    refMissing, refMissing);

                object xlHtml = Excel.XlFileFormat.xlHtml;

                System.Collections.IEnumerator wsEnumerator = excel.ActiveWorkbook.Worksheets.GetEnumerator();

                string worksheetFileName = null;
                string fileName = Path.GetFileName(refSourceFileName.ToString());
                string webPath = refTargetDirectory.Replace(fileName, String.Empty);

                while (wsEnumerator.MoveNext())
                {
                    Excel.Worksheet wsCurrent = (Excel.Worksheet)wsEnumerator.Current;
                    worksheetFileName = Path.Combine(refTargetDirectory, wsCurrent.Name + ".htm");
                    wsCurrent.SaveAs(worksheetFileName, xlHtml, refMissing, refMissing, refMissing, refMissing, refMissing, refMissing, refMissing, refMissing);
                }
            }
            catch (System.Runtime.InteropServices.COMException)
            {

            }
            finally
            {
                excel.Application.Quit();
                excel.Quit();
                excel = null;
            }
        }
        private void WordToHTML(object refSourceFileName, object refTargetDirectory)
        {
            Word._Application word = new Word.Application();
            Word.Documents documents = word.Documents;

            try
            {
                string fileName = Path.GetFileName(refSourceFileName.ToString());
                Word.Document document = documents.OpenNoRepairDialog(ref refSourceFileName, ref refTrue,
                    ref refFalse, ref refFalse, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refTrue, ref refFalse, ref refMissing,
                    ref refMissing, ref refMissing);

                if (document == null)
                    throw new Exception("Could not read " + refSourceFileName);

                word.Visible = false;
                word.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;
                object wdFormatHTML = Word.WdSaveFormat.wdFormatHTML;
                word.Options.SavePropertiesPrompt = false;
                word.Options.SaveNormalPrompt = false;
                object newRefTargetDirectory = Path.Combine(refTargetDirectory.ToString(), fileName + ".htm");
                word.ActiveDocument.SaveAs(ref newRefTargetDirectory, ref wdFormatHTML,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing);
            }
            catch (System.Runtime.InteropServices.COMException)
            {

            }
            finally
            {
                word.Quit();
                word = null;
            }
        }

        private void PowerPointToPDF(string refSourceFileName, string refTargetDirectory)
        {
            PowerPoint.Application powerPoint = new PowerPoint.Application();
            PowerPoint.Presentation presentation = null;

            string fileName = null;
            string newRefTargetDirectory = null;

            try
            {
                fileName = Path.GetFileName(refSourceFileName.ToString());
                newRefTargetDirectory = Path.Combine(refTargetDirectory.ToString(), fileName + ".pdf");

                presentation = powerPoint.Presentations.Open(refSourceFileName, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);
                presentation.SaveAs(newRefTargetDirectory, PowerPoint.PpSaveAsFileType.ppSaveAsPDF, MsoTriState.msoCTrue);

                powerPoint.DisplayAlerts = PowerPoint.PpAlertLevel.ppAlertsNone;
            }
            catch (System.Runtime.InteropServices.COMException)
            {

            }
            finally
            {
                presentation.Close();
                powerPoint.Quit();

                presentation = null;
                powerPoint = null;
            }
        }

        private void JPGToHTML(object refSourceFileName, object refTargetDirectory)
        {
            Word._Application word = new Word.Application();
            Word.Documents documents = word.Documents;

            try
            {
                string fileName = Path.GetFileName(refSourceFileName.ToString());
                Word.Document document = documents.OpenNoRepairDialog(ref refSourceFileName, ref refTrue,
                    ref refFalse, ref refFalse, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refTrue, ref refFalse, ref refMissing,
                    ref refMissing, ref refMissing);

                if (document == null)
                    throw new Exception("Could not read " + refSourceFileName);

                word.Visible = false;
                word.DisplayAlerts = Word.WdAlertLevel.wdAlertsNone;
                object wdFormatHTML = Word.WdSaveFormat.wdFormatHTML;
                word.Options.SavePropertiesPrompt = false;
                word.Options.SaveNormalPrompt = false;
                object newRefTargetDirectory = Path.Combine(refTargetDirectory.ToString(), fileName + ".htm");
                word.ActiveDocument.SaveAs(ref newRefTargetDirectory, ref wdFormatHTML,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing, ref refMissing,
                    ref refMissing, ref refMissing);
            }
            catch (System.Runtime.InteropServices.COMException)
            {

            }
            finally
            {
                word.Quit();
                word = null;
            }
        }


        private void DeleteFolder(string path)
        {
            DirectoryInfo drInfo = new DirectoryInfo(path);
            DirectoryInfo[] folders = drInfo.GetDirectories(); // lay cac folder
            FileInfo[] files = drInfo.GetFiles(); //lay cac files

            // neu van con thu muc con thi phai xoa het cac thu muc con
            if (folders != null)
            {
                foreach (DirectoryInfo fol in folders)
                {
                    var createTime = fol.CreationTime;
                    //if (createTime.DayOfYear != DateTime.Today.DayOfYear)
                    var timespan = DateTime.Now - createTime;
                    var times = new TimeSpan(1, 0, 0);
                    if (timespan > times)
                    {
                        try
                        {
                            DeleteFolder(fol.FullName);  //xoa thu muc con va cac file trong thu muc con do
                            Directory.Delete(fol.FullName);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }

            //Neu van con file thi phai xoa het cac file
            if (files != null)
            {
                foreach (FileInfo f in files)
                {
                    var createTime = f.CreationTime;
                    //if (createTime.DayOfYear != DateTime.Today.DayOfYear)
                    var timespan = DateTime.Now - createTime;
                    var times = new TimeSpan(1, 0, 0);
                    if (timespan > times)
                    {
                        try
                        {
                            File.Delete(f.FullName);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
        }

        public bool DeleteCurentFile(string path)
        {
            try
            {
                string pathsource = HttpContext.Current.Server.MapPath("/");
                DeleteFolderCurent(pathsource + "page\\" + path);
                Directory.Delete(pathsource + "page\\" + path);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private static void DeleteFolderCurent(string path)
        {
            DirectoryInfo drInfo = new DirectoryInfo(path);
            DirectoryInfo[] folders = drInfo.GetDirectories(); // lay cac folder
            FileInfo[] files = drInfo.GetFiles(); //lay cac files
            // neu van con thu muc con thi phai xoa het cac thu muc con
            if (folders != null)
            {
                foreach (DirectoryInfo fol in folders)
                {
                    try
                    {
                        DeleteFolderCurent(fol.FullName);  //xoa thu muc con va cac file trong thu muc con do
                        Directory.Delete(fol.FullName);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            //Neu van con file thi phai xoa het cac file
            if (files != null)
            {
                foreach (FileInfo f in files)
                {
                    try
                    {
                        File.Delete(f.FullName);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
    }
    
}
