﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public interface ILinhVucNganhNgheService
    {
        LinhVucNganhNghe Add(LinhVucNganhNghe linhVucNganhNghe);

        void Update(LinhVucNganhNghe linhVucNganhNghe);

        LinhVucNganhNghe Delete(int id);

        IEnumerable<LinhVucNganhNghe> GetAll();

        IEnumerable<LinhVucNganhNghe> GetByFilter(int page, int pageSize, out int totalRow, string filter);

        LinhVucNganhNghe GetById(int id);

        IEnumerable<LinhVucNganhNghe> GetDuAnById(int id);

        void Save();
    }

    public class LinhVucNganhNgheService : ILinhVucNganhNgheService
    {
        private ILinhVucNganhNgheRepository _linhVucNganhNgheRepository;
        private IUnitOfWork _unitOfWork;

        public LinhVucNganhNgheService(ILinhVucNganhNgheRepository linhVucNganhNgheRepository, IUnitOfWork unitOfWork)
        {
            this._linhVucNganhNgheRepository = linhVucNganhNgheRepository;
            this._unitOfWork = unitOfWork;
        }
        public LinhVucNganhNghe Add(LinhVucNganhNghe linhVucNganhNghe)
        {
            return _linhVucNganhNgheRepository.Add(linhVucNganhNghe);
        }

        public LinhVucNganhNghe Delete(int id)
        {
            return _linhVucNganhNgheRepository.Delete(id);
        }

        public IEnumerable<LinhVucNganhNghe> GetAll()
        {
            return _linhVucNganhNgheRepository.GetAll();
        }

        public IEnumerable<LinhVucNganhNghe> GetByFilter(int page, int pageSize, out int totalRow, string filter)
        {
            var query = _linhVucNganhNgheRepository.GetAll(new[] { "DuAns" });
            if (filter != null)
            {
                query = query.Where(x => x.TenLinhVucNganhNghe.Contains(filter));
            }
            totalRow = query.Count();
            return query.OrderBy(x => x.IdLinhVucNganhNghe).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public LinhVucNganhNghe GetById(int id)
        {
            return _linhVucNganhNgheRepository.GetMulti(x => x.IdLinhVucNganhNghe == id).SingleOrDefault();
        }

        public IEnumerable<LinhVucNganhNghe> GetDuAnById(int id)
        {
            return _linhVucNganhNgheRepository.GetMulti(x => x.IdLinhVucNganhNghe == id, new[] { "DuAns" }).ToList();
            
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(LinhVucNganhNghe linhVucNganhNghe)
        {
            _linhVucNganhNgheRepository.Update(linhVucNganhNghe);
        }
    }
}
