namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionHistories",
                c => new
                    {
                        IdHistory = c.Int(nullable: false, identity: true),
                        AppUser = c.String(),
                        Action = c.String(),
                        Function = c.String(),
                        FunctionUrl = c.String(),
                        ParentFunction = c.String(),
                        ParentFunctionUrl = c.String(),
                        Content = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        FunctionID = c.String(),
                    })
                .PrimaryKey(t => t.IdHistory);
            
            CreateTable(
                "dbo.AppRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        MaRole = c.String(),
                        Description = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        AppUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.AppUser_Id)
                .Index(t => t.AppUser_Id);
            
            CreateTable(
                "dbo.AppUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        AppUser_Id = c.String(maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AppUsers", t => t.AppUser_Id)
                .ForeignKey("dbo.AppRoles", t => t.IdentityRole_Id)
                .Index(t => t.AppUser_Id)
                .Index(t => t.IdentityRole_Id);
            
            CreateTable(
                "dbo.BaoCaoTables",
                c => new
                    {
                        IdBaoCao = c.Int(nullable: false, identity: true),
                        MaBaoCao = c.String(),
                        Ten = c.String(nullable: false),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        PhanLoai = c.String(),
                        URL = c.String(),
                    })
                .PrimaryKey(t => t.IdBaoCao);
            
            CreateTable(
                "dbo.BaoLanhTHHDs",
                c => new
                    {
                        IdBaoLanh = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdHopDong = c.Int(nullable: false),
                        LoaiBaoLanh = c.Int(nullable: false),
                        SoBaoLanh = c.String(nullable: false, maxLength: 255),
                        NgayBaoLanh = c.DateTime(),
                        NgayHetHan = c.DateTime(),
                        SoTien = c.Double(),
                        GhiChu = c.String(),
                        TrangThaiGui = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdBaoLanh)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong, cascadeDelete: true)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdHopDong);
            
            CreateTable(
                "dbo.DuAns",
                c => new
                    {
                        IdDuAn = c.Int(nullable: false, identity: true),
                        IdNhomDuAnTheoUser = c.Int(),
                        Ma = c.String(nullable: false, maxLength: 50),
                        TenDuAn = c.String(),
                        IdDuAnCha = c.Int(),
                        IdChuDauTu = c.Int(),
                        ThoiGianThucHien = c.String(maxLength: 50),
                        IdLinhVucNganhNghe = c.Int(),
                        DiaDiem = c.String(),
                        KinhDo = c.Double(nullable: false),
                        ViDo = c.Double(nullable: false),
                        MucTieu = c.String(),
                        QuyMoNangLuc = c.String(),
                        IdNhomDuAn = c.Int(),
                        IdLoaiCapCongTrinh = c.Int(),
                        IdHinhThucDauTu = c.Int(),
                        IdGiaiDoanDuAn = c.Int(),
                        IdHinhThucQuanLy = c.Int(),
                        IdTinhTrangDuAn = c.Int(),
                        NgayKhoiCongKeHoach = c.DateTime(),
                        NgayKetThucKeHoach = c.DateTime(),
                        NgayKhoiCongThucTe = c.DateTime(),
                        NgayKetThucThucTe = c.DateTime(),
                        TongMucDauTu = c.Double(nullable: false),
                        TongMucDauTu_XL = c.Double(),
                        TongMucDauTu_TB = c.Double(),
                        TongMucDauTu_GPMB = c.Double(),
                        TongMucDauTu_TV = c.Double(),
                        TongMucDauTu_QLDA = c.Double(),
                        TongMucDauTu_K = c.Double(),
                        TongMucDauTu_DP = c.Double(),
                        TongDuToan = c.Double(),
                        TongDuToan_XL = c.Double(),
                        TongDuToan_TB = c.Double(),
                        TongDuToan_GPMB = c.Double(),
                        TongDuToan_TV = c.Double(),
                        TongDuToan_QLDA = c.Double(),
                        TongDuToan_K = c.Double(),
                        TongDuToan_DP = c.Double(),
                        TongKHV = c.Double(),
                        TongThanhToan = c.Double(),
                        Order = c.Int(),
                        OrderTemp = c.Int(),
                        GhiChu = c.String(),
                        GhiChuKeToan = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDuAn)
                .ForeignKey("dbo.ChuDauTus", t => t.IdChuDauTu)
                .ForeignKey("dbo.NhomDuAnTheoUsers", t => t.IdNhomDuAnTheoUser)
                .ForeignKey("dbo.GiaiDoanDuAns", t => t.IdGiaiDoanDuAn)
                .ForeignKey("dbo.HinhThucDauTus", t => t.IdHinhThucDauTu)
                .ForeignKey("dbo.HinhThucQuanLys", t => t.IdHinhThucQuanLy)
                .ForeignKey("dbo.LinhVucNganhNghes", t => t.IdLinhVucNganhNghe)
                .ForeignKey("dbo.LoaiCapCongTrinhs", t => t.IdLoaiCapCongTrinh)
                .ForeignKey("dbo.NhomDuAns", t => t.IdNhomDuAn)
                .ForeignKey("dbo.TinhTrangDuAns", t => t.IdTinhTrangDuAn)
                .Index(t => t.IdNhomDuAnTheoUser)
                .Index(t => t.IdChuDauTu)
                .Index(t => t.IdLinhVucNganhNghe)
                .Index(t => t.IdNhomDuAn)
                .Index(t => t.IdLoaiCapCongTrinh)
                .Index(t => t.IdHinhThucDauTu)
                .Index(t => t.IdGiaiDoanDuAn)
                .Index(t => t.IdHinhThucQuanLy)
                .Index(t => t.IdTinhTrangDuAn);
            
            CreateTable(
                "dbo.ChuDauTus",
                c => new
                    {
                        IdChuDauTu = c.Int(nullable: false, identity: true),
                        TenChuDauTu = c.String(nullable: false, maxLength: 255),
                        DiaChi = c.String(maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChuDauTu);
            
            CreateTable(
                "dbo.ChuTruongDauTus",
                c => new
                    {
                        IdChuTruongDauTu = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        SoVanBan = c.String(nullable: false),
                        IdCoQuanPheDuyet = c.Int(),
                        NgayPheDuyet = c.DateTime(),
                        NguoiPheDuyet = c.String(maxLength: 255),
                        TongMucDauTu = c.Double(),
                        KinhPhi = c.Double(),
                        LoaiDieuChinh = c.String(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChuTruongDauTu)
                .ForeignKey("dbo.CoQuanPheDuyets", t => t.IdCoQuanPheDuyet)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.CoQuanPheDuyets",
                c => new
                    {
                        IdCoQuanPheDuyet = c.Int(nullable: false, identity: true),
                        TenCoQuanPheDuyet = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.KeHoachLuaChonNhaThaus",
                c => new
                    {
                        IdKeHoachLuaChonNhaThau = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        IdDuAn = c.Int(nullable: false),
                        IdDieuChinhKeHoachLuaChonNhaThau = c.Int(),
                        NgayTrinh = c.DateTime(),
                        SoVanBanTrinh = c.String(maxLength: 255),
                        NgayNhanDuHoSo = c.DateTime(),
                        NgayThamDinh = c.DateTime(),
                        NgayPheDuyet = c.DateTime(),
                        SoPheDuyet = c.String(maxLength: 50),
                        IdCoQuanPheDuyet = c.Int(),
                        NguoiPheDuyet = c.String(maxLength: 50),
                        NgayDangTaiThongTin = c.DateTime(),
                        PhuongTienDangTai = c.String(maxLength: 50),
                        ToChucCaNhanGiamSat = c.String(maxLength: 50),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachLuaChonNhaThau)
                .ForeignKey("dbo.CoQuanPheDuyets", t => t.IdCoQuanPheDuyet)
                .ForeignKey("dbo.KeHoachLuaChonNhaThaus", t => t.IdDieuChinhKeHoachLuaChonNhaThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdDieuChinhKeHoachLuaChonNhaThau)
                .Index(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.GoiThaus",
                c => new
                    {
                        IdGoiThau = c.Int(nullable: false, identity: true),
                        TenGoiThau = c.String(nullable: false, maxLength: 255),
                        IdDuAn = c.Int(nullable: false),
                        IdKeHoachLuaChonNhaThau = c.Int(nullable: false),
                        GiaGoiThau = c.Double(),
                        LoaiGoiThau = c.Int(),
                        IdLinhVucDauThau = c.Int(),
                        IdHinhThucLuaChon = c.Int(),
                        IdPhuongThucDauThau = c.Int(),
                        HinhThucDauThau = c.Int(),
                        LoaiDauThau = c.Int(),
                        IdLoaiHopDong = c.Int(),
                        ThoiGianThucHienHopDong = c.String(maxLength: 50),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGoiThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.HinhThucLuaChonNhaThaus", t => t.IdHinhThucLuaChon)
                .ForeignKey("dbo.LoaiHopDongs", t => t.IdLoaiHopDong)
                .ForeignKey("dbo.KeHoachLuaChonNhaThaus", t => t.IdKeHoachLuaChonNhaThau)
                .ForeignKey("dbo.LinhVucDauThaus", t => t.IdLinhVucDauThau)
                .ForeignKey("dbo.PhuongThucDauThaus", t => t.IdPhuongThucDauThau)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdKeHoachLuaChonNhaThau)
                .Index(t => t.IdLinhVucDauThau)
                .Index(t => t.IdHinhThucLuaChon)
                .Index(t => t.IdPhuongThucDauThau)
                .Index(t => t.IdLoaiHopDong);
            
            CreateTable(
                "dbo.HinhThucLuaChonNhaThaus",
                c => new
                    {
                        IdHinhThucLuaChon = c.Int(nullable: false, identity: true),
                        TenHinhThucLuaChon = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHinhThucLuaChon);
            
            CreateTable(
                "dbo.HopDongs",
                c => new
                    {
                        IdHopDong = c.Int(nullable: false, identity: true),
                        TenHopDong = c.String(nullable: false, maxLength: 255),
                        IdDuAn = c.Int(nullable: false),
                        IdGoiThau = c.Int(nullable: false),
                        SoHopDong = c.String(maxLength: 50),
                        NgayKy = c.DateTime(),
                        ThoiGianThucHien = c.Int(),
                        IdLoaiHopDong = c.Int(),
                        GiaHopDong = c.Double(),
                        GiaHopDongDieuChinh = c.Double(),
                        NoiDungVanTat = c.String(),
                        IdNhaThau = c.Int(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        NgayHoanThanhDieuChinh = c.DateTime(),
                        NgayThanhLyHopDong = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHopDong)
                .ForeignKey("dbo.DanhMucNhaThaus", t => t.IdNhaThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.GoiThaus", t => t.IdGoiThau)
                .ForeignKey("dbo.LoaiHopDongs", t => t.IdLoaiHopDong)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdGoiThau)
                .Index(t => t.IdLoaiHopDong)
                .Index(t => t.IdNhaThau);
            
            CreateTable(
                "dbo.DanhMucNhaThaus",
                c => new
                    {
                        IdNhaThau = c.Int(nullable: false, identity: true),
                        TenNhaThau = c.String(nullable: false),
                        DiaChiNhaThau = c.String(),
                        MoTa = c.String(),
                        IdDuAn = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNhaThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.KetQuaDauThaus",
                c => new
                    {
                        IdKetQuaLuaChonNhaThau = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        IdDuAn = c.Int(nullable: false),
                        IdGoiThau = c.Int(nullable: false),
                        NgayLapBienBanMoThau = c.DateTime(),
                        NgayLapBaoCaoDanhGia = c.DateTime(),
                        NgayLapBienBanThuongThao = c.DateTime(),
                        NgayLapBaoCaoThamDinh = c.DateTime(),
                        NgayPheDuyet = c.DateTime(),
                        SoPheDuyet = c.String(maxLength: 50),
                        NgayDangTai = c.DateTime(),
                        SoDangTai = c.String(maxLength: 50),
                        PhuongTienDangTai = c.String(maxLength: 50),
                        NgayGuiThongBao = c.DateTime(),
                        GiaDuThau = c.Double(),
                        GiaTrungThau = c.Double(),
                        IdNhaThau = c.Int(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKetQuaLuaChonNhaThau)
                .ForeignKey("dbo.DanhMucNhaThaus", t => t.IdNhaThau)
                .ForeignKey("dbo.GoiThaus", t => t.IdGoiThau, cascadeDelete: true)
                .Index(t => t.IdGoiThau)
                .Index(t => t.IdNhaThau);
            
            CreateTable(
                "dbo.ThanhToanChiPhiKhacs",
                c => new
                    {
                        IdThanhToanChiPhiKhac = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        IdNhaThau = c.Int(),
                        LoaiThanhToan = c.String(maxLength: 50),
                        IdLoaiChiPhi = c.Int(),
                        TrangThaiGui = c.Int(nullable: false),
                        NgayThanhToan = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThanhToanChiPhiKhac)
                .ForeignKey("dbo.DanhMucNhaThaus", t => t.IdNhaThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn)
                .ForeignKey("dbo.LoaiChiPhis", t => t.IdLoaiChiPhi)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdNhaThau)
                .Index(t => t.IdLoaiChiPhi);
            
            CreateTable(
                "dbo.LoaiChiPhis",
                c => new
                    {
                        IdLoaiChiPhi = c.Int(nullable: false, identity: true),
                        IdLoaiChiPhiCha = c.Int(),
                        TenLoaiChiPhi = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        Order = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLoaiChiPhi);
            
            CreateTable(
                "dbo.NguonVonThanhToanCPKs",
                c => new
                    {
                        IdNguonVonThanhToanChiPhiKhac = c.Int(nullable: false, identity: true),
                        IdThanhToanChiPhiKhac = c.Int(nullable: false),
                        IdNguonVonDuAn = c.Int(nullable: false),
                        GiaTri = c.Double(nullable: false),
                        GiaTriTabmis = c.Double(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonThanhToanChiPhiKhac)
                .ForeignKey("dbo.NguonVonDuAns", t => t.IdNguonVonDuAn, cascadeDelete: true)
                .ForeignKey("dbo.ThanhToanChiPhiKhacs", t => t.IdThanhToanChiPhiKhac)
                .Index(t => t.IdThanhToanChiPhiKhac)
                .Index(t => t.IdNguonVonDuAn);
            
            CreateTable(
                "dbo.NguonVonDuAns",
                c => new
                    {
                        IdNguonVonDuAn = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdNguonVon = c.Int(nullable: false),
                        GiaTri = c.Double(),
                        GiaTriTrungHan = c.Double(),
                        IdLapQuanLyDauTu = c.Int(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonDuAn)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.LapQuanLyDauTus", t => t.IdLapQuanLyDauTu)
                .ForeignKey("dbo.NguonVons", t => t.IdNguonVon, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdNguonVon)
                .Index(t => t.IdLapQuanLyDauTu);
            
            CreateTable(
                "dbo.LapQuanLyDauTus",
                c => new
                    {
                        IdLapQuanLyDauTu = c.Int(nullable: false, identity: true),
                        LoaiQuanLyDauTu = c.Int(nullable: false),
                        IdDuAn = c.Int(nullable: false),
                        NoiDung = c.String(maxLength: 255),
                        NgayTrinh = c.DateTime(),
                        SoToTrinh = c.String(maxLength: 255),
                        GiaTriTrinh = c.Double(),
                        NgayPheDuyet = c.DateTime(),
                        SoQuyetDinh = c.String(maxLength: 255),
                        NguoiPheDuyet = c.String(maxLength: 255),
                        IdCoQuanPheDuyet = c.Int(),
                        LoaiDieuChinh = c.Int(),
                        TongGiaTri = c.Double(),
                        XayLap = c.Double(),
                        ThietBi = c.Double(),
                        GiaiPhongMatBang = c.Double(),
                        TuVan = c.Double(),
                        QuanLyDuAn = c.Double(),
                        Khac = c.Double(),
                        DuPhong = c.Double(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLapQuanLyDauTu)
                .ForeignKey("dbo.CoQuanPheDuyets", t => t.IdCoQuanPheDuyet)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.ThamDinhLapQuanLyDauTus",
                c => new
                    {
                        IdThamDinh = c.Int(nullable: false, identity: true),
                        IdLapQuanLyDauTu = c.Int(nullable: false),
                        NoiDung = c.String(maxLength: 255),
                        NgayNhanDuHoSo = c.DateTime(),
                        NgayPheDuyet = c.DateTime(),
                        ToChucNguoiThamDinh = c.String(maxLength: 50),
                        DanhGia = c.String(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThamDinh)
                .ForeignKey("dbo.LapQuanLyDauTus", t => t.IdLapQuanLyDauTu, cascadeDelete: true)
                .Index(t => t.IdLapQuanLyDauTu);
            
            CreateTable(
                "dbo.NguonVons",
                c => new
                    {
                        IdNguonVon = c.Int(nullable: false, identity: true),
                        Ma = c.String(nullable: false, maxLength: 50),
                        TenNguonVon = c.String(nullable: false, maxLength: 255),
                        Loai = c.String(maxLength: 50),
                        Nhom = c.String(maxLength: 50),
                        IdNguonVonCha = c.Int(),
                        Ghichu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVon)
                .ForeignKey("dbo.NguonVons", t => t.IdNguonVonCha)
                .Index(t => t.IdNguonVonCha);
            
            CreateTable(
                "dbo.NguonVonDuAnGoiThaus",
                c => new
                    {
                        IdNguonVonDuAnGoiThau = c.Int(nullable: false, identity: true),
                        IdGoiThau = c.Int(nullable: false),
                        IdNguonVonDuAn = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonDuAnGoiThau)
                .ForeignKey("dbo.GoiThaus", t => t.IdGoiThau)
                .ForeignKey("dbo.NguonVonDuAns", t => t.IdNguonVonDuAn, cascadeDelete: true)
                .Index(t => t.IdGoiThau)
                .Index(t => t.IdNguonVonDuAn);
            
            CreateTable(
                "dbo.NguonVonGoiThauTamUngs",
                c => new
                    {
                        IdNguonVonGoiThauTamUng = c.Int(nullable: false, identity: true),
                        IdNguonVonDuAnGoiThau = c.Int(nullable: false),
                        IdTamUng = c.Int(nullable: false),
                        GiaTri = c.Double(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonGoiThauTamUng)
                .ForeignKey("dbo.NguonVonDuAnGoiThaus", t => t.IdNguonVonDuAnGoiThau, cascadeDelete: true)
                .ForeignKey("dbo.TamUngHopDongs", t => t.IdTamUng)
                .Index(t => t.IdNguonVonDuAnGoiThau)
                .Index(t => t.IdTamUng);
            
            CreateTable(
                "dbo.TamUngHopDongs",
                c => new
                    {
                        IdTamUng = c.Int(nullable: false, identity: true),
                        IdHopDong = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        NgayTamUng = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTamUng)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong)
                .Index(t => t.IdHopDong);
            
            CreateTable(
                "dbo.NguonVonGoiThauThanhToans",
                c => new
                    {
                        IdNguonVonGoiThauThanhToan = c.Int(nullable: false, identity: true),
                        IdNguonVonDuAnGoiThau = c.Int(nullable: false),
                        IdThanhToanHopDong = c.Int(nullable: false),
                        GiaTri = c.Double(nullable: false),
                        TamUng = c.Double(nullable: false),
                        GiaTriTabmis = c.Double(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonGoiThauThanhToan)
                .ForeignKey("dbo.NguonVonDuAnGoiThaus", t => t.IdNguonVonDuAnGoiThau, cascadeDelete: true)
                .ForeignKey("dbo.ThanhToanHopDongs", t => t.IdThanhToanHopDong)
                .Index(t => t.IdNguonVonDuAnGoiThau)
                .Index(t => t.IdThanhToanHopDong);
            
            CreateTable(
                "dbo.ThanhToanHopDongs",
                c => new
                    {
                        IdThanhToanHopDong = c.Int(nullable: false, identity: true),
                        IdHopDong = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        IdThucHien = c.Int(nullable: false),
                        DeNghiThanhToan = c.Double(),
                        ThoiDiemThanhToan = c.DateTime(),
                        TrangThaiGui = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThanhToanHopDong)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong, cascadeDelete: true)
                .ForeignKey("dbo.ThucHienHopDongs", t => t.IdThucHien)
                .Index(t => t.IdHopDong)
                .Index(t => t.IdThucHien);
            
            CreateTable(
                "dbo.ThucHienHopDongs",
                c => new
                    {
                        IdThucHien = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(),
                        IdHopDong = c.Int(),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        ThoiDiemBaoCao = c.DateTime(),
                        UocDenNgay = c.DateTime(),
                        KhoiLuong = c.Double(),
                        UocThucHien = c.Double(),
                        NgayNghiemThu = c.DateTime(),
                        IdLoaiChiPhi = c.Int(),
                        GiaTriNghiemThu = c.Double(),
                        TrangThaiGui = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHien)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong)
                .ForeignKey("dbo.LoaiChiPhis", t => t.IdLoaiChiPhi)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdHopDong)
                .Index(t => t.IdLoaiChiPhi);
            
            CreateTable(
                "dbo.NguonVonKeHoachVons",
                c => new
                    {
                        IdNguonVonKeHoachVon = c.Int(nullable: false, identity: true),
                        IdKeHoachVon = c.Int(nullable: false),
                        IdNguonVonDuAn = c.Int(nullable: false),
                        GiaTri = c.Double(nullable: false),
                        GiaTriTabmis = c.Double(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonKeHoachVon)
                .ForeignKey("dbo.KeHoachVons", t => t.IdKeHoachVon)
                .ForeignKey("dbo.NguonVonDuAns", t => t.IdNguonVonDuAn, cascadeDelete: true)
                .Index(t => t.IdKeHoachVon)
                .Index(t => t.IdNguonVonDuAn);
            
            CreateTable(
                "dbo.KeHoachVons",
                c => new
                    {
                        IdKeHoachVon = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdKeHoachVonDieuChinh = c.Int(),
                        SoQuyetDinh = c.String(nullable: false, maxLength: 50),
                        IdCoQuanPheDuyet = c.Int(),
                        NgayPheDuyet = c.DateTime(),
                        NienDo = c.Int(),
                        TinhChatKeHoach = c.String(maxLength: 50),
                        TongGiaTri = c.Double(),
                        XayLap = c.Double(),
                        ThietBi = c.Double(),
                        GiaiPhongMatBang = c.Double(),
                        TuVan = c.Double(),
                        QuanLyDuAn = c.Double(),
                        Khac = c.Double(),
                        DuPhong = c.Double(),
                        GhiChu = c.String(),
                        DeXuatDieuChinh = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachVon)
                .ForeignKey("dbo.CoQuanPheDuyets", t => t.IdCoQuanPheDuyet)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.LoaiHopDongs",
                c => new
                    {
                        IdLoaiHopDong = c.Int(nullable: false, identity: true),
                        TenLoaiHopDong = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLoaiHopDong);
            
            CreateTable(
                "dbo.PhuLucHopDongs",
                c => new
                    {
                        IdPhuLuc = c.Int(nullable: false, identity: true),
                        IdHopDong = c.Int(nullable: false),
                        TenPhuLuc = c.String(nullable: false, maxLength: 255),
                        NgayKy = c.DateTime(),
                        GiaHopDongDieuChinh = c.Double(),
                        NgayHoanThanhDieuChinh = c.DateTime(),
                        NoiDung = c.String(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPhuLuc)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong, cascadeDelete: true)
                .Index(t => t.IdHopDong);
            
            CreateTable(
                "dbo.QuyetToanHopDongs",
                c => new
                    {
                        IdQuyetToan = c.Int(nullable: false, identity: true),
                        IdHopDong = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        NgayQuyetToan = c.DateTime(),
                        GiaTriQuyetToan = c.Double(),
                        TrangThaiGui = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdQuyetToan)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong, cascadeDelete: true)
                .Index(t => t.IdHopDong);
            
            CreateTable(
                "dbo.TienDoThucHiens",
                c => new
                    {
                        IdTienDo = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdGoiThau = c.Int(nullable: false),
                        IdHopDong = c.Int(nullable: false),
                        TenCongViec = c.String(maxLength: 255),
                        TuNgay = c.DateTime(),
                        DenNgay = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDo)
                .ForeignKey("dbo.HopDongs", t => t.IdHopDong, cascadeDelete: true)
                .Index(t => t.IdHopDong);
            
            CreateTable(
                "dbo.TienDoChiTiets",
                c => new
                    {
                        IdTienDoChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDo = c.Int(nullable: false),
                        NoiDung = c.String(),
                        NgayThucHien = c.DateTime(),
                        TonTaiVuongMac = c.String(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoChiTiet)
                .ForeignKey("dbo.TienDoThucHiens", t => t.IdTienDo)
                .Index(t => t.IdTienDo);
            
            CreateTable(
                "dbo.HoSoMoiThaus",
                c => new
                    {
                        IdHoSoMoiThau = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(),
                        IdDuAn = c.Int(),
                        IdGoiThau = c.Int(),
                        NgayTrinh = c.DateTime(),
                        SoVanBanTrinh = c.String(maxLength: 50),
                        NgayNhanDu = c.DateTime(),
                        NgayThamDinh = c.DateTime(),
                        NgayPheDuyet = c.DateTime(),
                        SoPheDuyet = c.String(maxLength: 50),
                        NgayDangTai = c.DateTime(),
                        SoDangTai = c.String(maxLength: 50),
                        PhuongTienDangTai = c.String(maxLength: 255),
                        NgayPhatHanh = c.DateTime(),
                        SoPhatHanh = c.String(maxLength: 50),
                        NgayHetHanHieuLuc = c.DateTime(),
                        NgayHetHanBaoLanh = c.DateTime(),
                        NgayLapBienBan = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHoSoMoiThau)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn)
                .ForeignKey("dbo.GoiThaus", t => t.IdGoiThau)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdGoiThau);
            
            CreateTable(
                "dbo.LinhVucDauThaus",
                c => new
                    {
                        IdLinhVucDauThau = c.Int(nullable: false, identity: true),
                        TenLinhVucDauThau = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLinhVucDauThau);
            
            CreateTable(
                "dbo.PhuongThucDauThaus",
                c => new
                    {
                        IdPhuongThucDauThau = c.Int(nullable: false, identity: true),
                        TenPhuongThucDauThau = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPhuongThucDauThau);
            
            CreateTable(
                "dbo.QuyetToanDuAns",
                c => new
                    {
                        IdQuyetToanDuAn = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        NgayKyBienBan = c.DateTime(),
                        NgayTrinh = c.DateTime(),
                        NgayNhanDuHoSo = c.DateTime(),
                        NgayThamTra = c.DateTime(),
                        NgayPheDuyetQuyetToan = c.DateTime(),
                        SoPheDuyet = c.String(maxLength: 50),
                        IdCoQuanPheDuyet = c.Int(),
                        TongGiaTri = c.Double(),
                        XayLap = c.Double(),
                        ThietBi = c.Double(),
                        GiaiPhongMatBang = c.Double(),
                        TuVan = c.Double(),
                        QuanLyDuAn = c.Double(),
                        Khac = c.Double(),
                        DuPhong = c.Double(),
                        GhiChu = c.String(),
                        TrangThaiGui = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdQuyetToanDuAn)
                .ForeignKey("dbo.CoQuanPheDuyets", t => t.IdCoQuanPheDuyet)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdCoQuanPheDuyet);
            
            CreateTable(
                "dbo.NguonVonQuyetToanDuAns",
                c => new
                    {
                        IdNguonVonQuyetToanDuAn = c.Int(nullable: false, identity: true),
                        IdQuyetToanDuAn = c.Int(nullable: false),
                        IdNguonVonDuAn = c.Int(nullable: false),
                        GiaTri = c.Double(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNguonVonQuyetToanDuAn)
                .ForeignKey("dbo.NguonVonDuAns", t => t.IdNguonVonDuAn, cascadeDelete: true)
                .ForeignKey("dbo.QuyetToanDuAns", t => t.IdQuyetToanDuAn)
                .Index(t => t.IdQuyetToanDuAn)
                .Index(t => t.IdNguonVonDuAn);
            
            CreateTable(
                "dbo.DuAnQuanLys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdQuanLyDA = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.QuanLyDAs", t => t.IdQuanLyDA, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdQuanLyDA);
            
            CreateTable(
                "dbo.QuanLyDAs",
                c => new
                    {
                        IdQuanLyDA = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdQuanLyDA);
            
            CreateTable(
                "dbo.DuAnUsers",
                c => new
                    {
                        IdDuanUser = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        HasRead = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDuanUser)
                .ForeignKey("dbo.AppUsers", t => t.UserId)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AppUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        IdNhomDuAnTheoUser = c.Int(),
                        FullName = c.String(maxLength: 256),
                        Address = c.String(maxLength: 256),
                        Avatar = c.String(),
                        BirthDay = c.DateTime(),
                        Status = c.Boolean(nullable: false),
                        Gender = c.Boolean(),
                        OnlineTime = c.DateTime(),
                        Online = c.Boolean(nullable: false),
                        Delete = c.Boolean(nullable: false),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NhomDuAnTheoUsers", t => t.IdNhomDuAnTheoUser)
                .Index(t => t.IdNhomDuAnTheoUser);
            
            CreateTable(
                "dbo.AppUserClaims",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        Id = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        AppUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.AppUsers", t => t.AppUser_Id)
                .Index(t => t.AppUser_Id);
            
            CreateTable(
                "dbo.AppUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        AppUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.AppUsers", t => t.AppUser_Id)
                .Index(t => t.AppUser_Id);
            
            CreateTable(
                "dbo.NhomDuAnTheoUsers",
                c => new
                    {
                        IdNhomDuAn = c.Int(nullable: false, identity: true),
                        TenNhomDuAn = c.String(maxLength: 500),
                        GhiChu = c.String(),
                        PhongChucNang = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNhomDuAn);
            
            CreateTable(
                "dbo.GiaiDoanDuAns",
                c => new
                    {
                        IdGiaiDoanDuAn = c.Int(nullable: false, identity: true),
                        TenGiaiDoanDuAn = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGiaiDoanDuAn);
            
            CreateTable(
                "dbo.HinhThucDauTus",
                c => new
                    {
                        IdHinhThucDauTu = c.Int(nullable: false, identity: true),
                        TenHinhThucDauTu = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHinhThucDauTu);
            
            CreateTable(
                "dbo.HinhThucQuanLys",
                c => new
                    {
                        IdHinhThucQuanLy = c.Int(nullable: false, identity: true),
                        TenHinhThucQuanLy = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHinhThucQuanLy);
            
            CreateTable(
                "dbo.LinhVucNganhNghes",
                c => new
                    {
                        IdLinhVucNganhNghe = c.Int(nullable: false, identity: true),
                        Ma = c.String(nullable: false, maxLength: 255),
                        TenLinhVucNganhNghe = c.String(nullable: false, maxLength: 255),
                        KhoiLinhVuc = c.String(maxLength: 255),
                        IdLienKetLinhVuc = c.Int(),
                        GhiChu = c.String(),
                        Order = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLinhVucNganhNghe)
                .ForeignKey("dbo.LienKetLinhVucs", t => t.IdLienKetLinhVuc)
                .Index(t => t.IdLienKetLinhVuc);
            
            CreateTable(
                "dbo.LienKetLinhVucs",
                c => new
                    {
                        IdLienKetLinhVuc = c.Int(nullable: false, identity: true),
                        TenLienKetLinhVuc = c.String(),
                        GhiChu = c.String(),
                        Order = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLienKetLinhVuc);
            
            CreateTable(
                "dbo.LoaiCapCongTrinhs",
                c => new
                    {
                        IdLoaiCapCongTrinh = c.Int(nullable: false, identity: true),
                        TenLoaiCapCongTrinh = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdLoaiCapCongTrinh);
            
            CreateTable(
                "dbo.NhomDuAns",
                c => new
                    {
                        IdNhomDuAn = c.Int(nullable: false, identity: true),
                        TenNhomDuAn = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdNhomDuAn);
            
            CreateTable(
                "dbo.QLTD_KeHoach",
                c => new
                    {
                        IdKeHoach = c.Int(nullable: false, identity: true),
                        IdNhomDuAnTheoUser = c.Int(),
                        IdKeHoachDC = c.Int(),
                        IdDuAn = c.Int(nullable: false),
                        IdGiaiDoan = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        NgayGiaoLapChuTruong = c.DateTime(),
                        PheDuyet = c.Boolean(nullable: false),
                        TrangThai = c.Int(nullable: false),
                        TinhTrang = c.Int(),
                        NgayTinhTrang = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoach)
                .ForeignKey("dbo.AppUsers", t => t.CreatedBy)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.NhomDuAnTheoUsers", t => t.IdNhomDuAnTheoUser)
                .ForeignKey("dbo.QLTD_GiaiDoan", t => t.IdGiaiDoan, cascadeDelete: true)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoachDC)
                .Index(t => t.IdNhomDuAnTheoUser)
                .Index(t => t.IdKeHoachDC)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdGiaiDoan)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.QLTD_GiaiDoan",
                c => new
                    {
                        IdGiaiDoan = c.Int(nullable: false, identity: true),
                        TenGiaiDoan = c.String(nullable: false, maxLength: 255),
                        NhomGiaiDoan = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGiaiDoan);
            
            CreateTable(
                "dbo.QLTD_KeHoachCongViec",
                c => new
                    {
                        IdKeHoachCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        TuNgay = c.DateTime(),
                        DenNgay = c.DateTime(),
                        LoaiCongViec = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec, cascadeDelete: true)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec);
            
            CreateTable(
                "dbo.QLTD_CongViec",
                c => new
                    {
                        IdCongViec = c.Int(nullable: false, identity: true),
                        IdGiaiDoan = c.Int(nullable: false),
                        TenCongViec = c.String(),
                        LoaiCongViec = c.Int(nullable: false),
                        TienDoThucHien = c.Boolean(nullable: false),
                        Order = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_GiaiDoan", t => t.IdGiaiDoan)
                .Index(t => t.IdGiaiDoan);
            
            CreateTable(
                "dbo.QLTD_KeHoachTienDoChung",
                c => new
                    {
                        IdKeHoachTienDoChung = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachTienDoChung)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .Index(t => t.IdKeHoach);
            
            CreateTable(
                "dbo.TinhTrangDuAns",
                c => new
                    {
                        IdTinhTrangDuAn = c.Int(nullable: false, identity: true),
                        TenTinhTrangDuAn = c.String(nullable: false, maxLength: 255),
                        IdTinhTrangDuAnCha = c.Int(),
                        ThuTu = c.Int(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTinhTrangDuAn)
                .ForeignKey("dbo.TinhTrangDuAns", t => t.IdTinhTrangDuAnCha)
                .Index(t => t.IdTinhTrangDuAnCha);
            
            CreateTable(
                "dbo.VanBanDuAns",
                c => new
                    {
                        IdVanBan = c.Int(nullable: false, identity: true),
                        SoVanBan = c.String(nullable: false, maxLength: 255),
                        TenVanBan = c.String(nullable: false, maxLength: 255),
                        FileName = c.String(maxLength: 255),
                        FileUrl = c.String(storeType: "ntext"),
                        NgayBanHanh = c.DateTime(),
                        CoQuanBanHanh = c.String(maxLength: 255),
                        MoTa = c.String(),
                        GhiChu = c.String(),
                        IdThuMuc = c.Int(nullable: false),
                        IdDuAn = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdVanBan)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.ThuMucs", t => t.IdThuMuc, cascadeDelete: true)
                .Index(t => t.IdThuMuc)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.ThuMucs",
                c => new
                    {
                        IdThuMuc = c.Int(nullable: false, identity: true),
                        TenThuMuc = c.String(nullable: false, maxLength: 255),
                        IdThuMucCha = c.Int(),
                        IdThuMucMacDinh = c.Int(),
                        IdDuAn = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThuMuc)
                .ForeignKey("dbo.ThuMucMacDinhs", t => t.IdThuMucMacDinh)
                .Index(t => t.IdThuMucMacDinh);
            
            CreateTable(
                "dbo.ThuMucMacDinhs",
                c => new
                    {
                        IdThuMucMacDinh = c.Int(nullable: false, identity: true),
                        TenThuMuc = c.String(nullable: false, maxLength: 255),
                        IdThuMucMacDinhCha = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThuMucMacDinh);
            
            CreateTable(
                "dbo.CamKetGiaiNgans",
                c => new
                    {
                        IdCamKetGiaiNgan = c.Int(nullable: false, identity: true),
                        NamCK = c.Int(nullable: false),
                        IdDuAn = c.Int(nullable: false),
                        Thang1 = c.Double(),
                        Thang2 = c.Double(),
                        Thang3 = c.Double(),
                        Thang4 = c.Double(),
                        Thang5 = c.Double(),
                        Thang6 = c.Double(),
                        Thang7 = c.Double(),
                        Thang8 = c.Double(),
                        Thang9 = c.Double(),
                        Thang10 = c.Double(),
                        Thang11 = c.Double(),
                        Thang12 = c.Double(),
                        Quy1 = c.Double(),
                        Quy2 = c.Double(),
                        Quy3 = c.Double(),
                        Quy4 = c.Double(),
                        TongCong = c.Double(),
                        GhiChu = c.String(),
                        TrangThaiGui = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdCamKetGiaiNgan)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.CapNhatKLNTs",
                c => new
                    {
                        IdFile = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        FileUrl = c.String(storeType: "ntext"),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdFile);
            
            CreateTable(
                "dbo.ChatApps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdUser1 = c.String(nullable: false, maxLength: 128),
                        IdUser2 = c.String(maxLength: 128),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        TrangThaiChatNhom = c.Boolean(nullable: false),
                        IsFile = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.IdUser1, cascadeDelete: true)
                .ForeignKey("dbo.AppUsers", t => t.IdUser2)
                .Index(t => t.IdUser1)
                .Index(t => t.IdUser2);
            
            CreateTable(
                "dbo.ChatNhomApps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdUser1 = c.String(maxLength: 128),
                        IdUser2 = c.Int(nullable: false),
                        NoiDung = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                        IsFile = c.Boolean(nullable: false),
                        TrangThaiChatNhom = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.IdUser1)
                .ForeignKey("dbo.NhomChatApps", t => t.IdUser2, cascadeDelete: true)
                .Index(t => t.IdUser1)
                .Index(t => t.IdUser2);
            
            CreateTable(
                "dbo.NhomChatApps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ListUser = c.String(),
                        TenNhomChat = c.String(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        IdComment = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdComment)
                .ForeignKey("dbo.AppUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DanhMucHoSoQuyetToans",
                c => new
                    {
                        IdDanhMucHoSoQuyetToan = c.Int(nullable: false, identity: true),
                        GiaiDoan = c.Int(),
                        IdParent = c.Int(),
                        TenDanhMucHoSoQuyetToan = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        Order = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDanhMucHoSoQuyetToan);
            
            CreateTable(
                "dbo.Device_Token",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Token = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DuAnHoSoQuyetToans",
                c => new
                    {
                        IdDuAnHoSoQuyetToan = c.Int(nullable: false, identity: true),
                        IdDanhMucHoSoQuyetToan = c.Int(nullable: false),
                        IdDuAn = c.Int(nullable: false),
                        KyThuat = c.Int(nullable: false),
                        KeToan = c.Int(nullable: false),
                        HoanThien = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDuAnHoSoQuyetToan)
                .ForeignKey("dbo.DanhMucHoSoQuyetToans", t => t.IdDanhMucHoSoQuyetToan, cascadeDelete: true)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDanhMucHoSoQuyetToan)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.Errors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        StackTrace = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Functions",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 50, unicode: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        URL = c.String(nullable: false, maxLength: 256),
                        DisplayOrder = c.Int(nullable: false),
                        ParentId = c.String(maxLength: 50, unicode: false),
                        ShowInMenu = c.Boolean(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Menu = c.Boolean(nullable: false),
                        IconCss = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Functions", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.GiaTriThanhToanTheoNams",
                c => new
                    {
                        IdGiaTriThanhToanTheoNam = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdKeHoachVon = c.Int(),
                        NienDo = c.Int(nullable: false),
                        GiaTriKHV = c.Double(),
                        GiaTriKHVTabmis = c.Double(),
                        GiaTriThanhToan = c.Double(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGiaTriThanhToanTheoNam)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.GPMB_Comment",
                c => new
                    {
                        IdComment = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdComment)
                .ForeignKey("dbo.AppUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GPMB_CongViec",
                c => new
                    {
                        IdCongViecGPMB = c.Int(nullable: false, identity: true),
                        TenCongViec = c.String(),
                        LoaiCongViec = c.Int(nullable: false),
                        NhieuDot = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdCongViecGPMB);
            
            CreateTable(
                "dbo.GPMB_CTCV",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoachCongViecGPMB = c.Int(nullable: false),
                        SoQuyetDinh = c.String(),
                        NgayPheDuyet = c.DateTime(),
                        VBGNV_SoVanBan = c.String(),
                        VBGNV_Ngay = c.DateTime(),
                        BBBGMG_Ngay = c.DateTime(),
                        QDTLHD_So = c.String(),
                        QDTLHD_Ngay = c.DateTime(),
                        QDTCT_So = c.String(),
                        QDTCT_Ngay = c.DateTime(),
                        QDPDDT_So = c.String(),
                        QDPDDT_NgayBanBanHanh = c.DateTime(),
                        HHD_Ngay = c.DateTime(),
                        HDU_Ngay = c.DateTime(),
                        QCTDHT_Chon = c.Boolean(nullable: false),
                        TBTHD_SoDoiTuong = c.String(),
                        HDPTK_Chon = c.Boolean(nullable: false),
                        DTKKLBB_SoDoiTuong = c.String(),
                        CCKDBB_Chon = c.Boolean(nullable: false),
                        LHSGPMB_So = c.String(),
                        LTHDTT_So = c.String(),
                        SKKTDTPA_So = c.String(),
                        THDHTPA_So = c.String(),
                        QDPDPA_So = c.String(),
                        QDPDPA_TGT = c.String(),
                        CTTNBG_So = c.String(),
                        CTTNBG_TGT = c.String(),
                        CCTHD_Chon = c.Boolean(nullable: false),
                        QTKP_So = c.String(),
                        QTKP_Ngay = c.DateTime(),
                        QTKP_GT = c.String(),
                        QDGD_So = c.String(),
                        QDGD_Ngay = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.GPMB_KeHoachCongViec", t => t.IdKeHoachCongViecGPMB, cascadeDelete: true)
                .Index(t => t.IdKeHoachCongViecGPMB);
            
            CreateTable(
                "dbo.GPMB_KeHoachCongViec",
                c => new
                    {
                        IdKeHoachCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoachGPMB = c.Int(nullable: false),
                        IdCongViecGPMB = c.Int(nullable: false),
                        TuNgay = c.DateTime(),
                        DenNgay = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachCongViec)
                .ForeignKey("dbo.GPMB_CongViec", t => t.IdCongViecGPMB, cascadeDelete: true)
                .ForeignKey("dbo.GPMB_KeHoach", t => t.IdKeHoachGPMB, cascadeDelete: true)
                .Index(t => t.IdKeHoachGPMB)
                .Index(t => t.IdCongViecGPMB);
            
            CreateTable(
                "dbo.GPMB_KeHoach",
                c => new
                    {
                        IdKeHoachGPMB = c.Int(nullable: false, identity: true),
                        IdNhomDuAnTheoUser = c.Int(),
                        IdDuAn = c.Int(nullable: false),
                        NoiDung = c.String(nullable: false, maxLength: 255),
                        PheDuyet = c.Boolean(nullable: false),
                        TrangThai = c.Int(nullable: false),
                        TinhTrang = c.Int(),
                        NgayTinhTrang = c.DateTime(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachGPMB)
                .ForeignKey("dbo.AppUsers", t => t.CreatedBy)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.NhomDuAnTheoUsers", t => t.IdNhomDuAnTheoUser)
                .Index(t => t.IdNhomDuAnTheoUser)
                .Index(t => t.IdDuAn)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.GPMB_CTCV_Dots",
                c => new
                    {
                        IdChiTietCongViec_Dot = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TBTHD_Ngay = c.DateTime(),
                        TBTHD_SoDoiTuong = c.String(),
                        HDPTK_Ngay = c.DateTime(),
                        DTKT_SoDoiTuong = c.String(),
                        CCKDBB_Chon = c.Boolean(nullable: false),
                        LHSGPMB_SoDoiTuong = c.String(),
                        LTHDTT_SoDoiTuong = c.String(),
                        CKKT_SoDoiTuong = c.String(),
                        THDTD_SoDoiTuong = c.String(),
                        QDPDPA_SoDoiTuong = c.String(),
                        QDPDPA_GiaTri = c.String(),
                        CTNBG_SoDoiTuong = c.String(),
                        CTNBG_GiaTri = c.String(),
                        CCTHD_Chon = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec_Dot)
                .ForeignKey("dbo.GPMB_CTCV", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.GPMB_DangKyKeHoach",
                c => new
                    {
                        IdDangKyKeHoach = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh = c.Double(),
                        QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh = c.Double(),
                        HopHD_HopDUXMoRong_NgayHoanThanh = c.Double(),
                        QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh = c.Double(),
                        TBThuHoiDat_NgayHoanThanh = c.Double(),
                        HopDanPhatToKhai_NgayHoanThanh = c.Double(),
                        DieuTraKeKhaiLapBBGPMB_NgayHoanThanh = c.Double(),
                        CuongCheKDBB_NgayHoanThanh = c.Double(),
                        LapHSGPMB_NgayHoanThanh = c.Double(),
                        LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh = c.Double(),
                        CongKhaiKTCongKhaiDTPA_NgayHoanThanh = c.Double(),
                        TrinhHDTDHoanThienPACT_NgayHoanThanh = c.Double(),
                        QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh = c.Double(),
                        ChiTraTien_NhanBGD_NgayHoanThanh = c.Double(),
                        CuongCheThuHoiDat_NgayHoanThanh = c.Double(),
                        QuyetToanKPTHGPMB_NgayHoanThanh = c.Double(),
                        LapHSDNTPGiaoDat_NgayHoanThanh = c.Double(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDangKyKeHoach)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.GPMB_File",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        NoiDung = c.String(maxLength: 255),
                        PheDuyet = c.Boolean(nullable: false),
                        IsBossCreated = c.Boolean(nullable: false),
                        TrangThai = c.Int(nullable: false),
                        TinhTrang = c.Int(),
                        NgayTinhTrang = c.DateTime(),
                        Mota = c.String(maxLength: 255),
                        FileURL = c.String(storeType: "ntext"),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.CreatedBy)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.GPMB_KeHoachTienDoChung",
                c => new
                    {
                        IdKeHoachTienDoChung = c.Int(nullable: false, identity: true),
                        IdKeHoachGPMB = c.Int(nullable: false),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKeHoachTienDoChung)
                .ForeignKey("dbo.GPMB_KeHoach", t => t.IdKeHoachGPMB, cascadeDelete: true)
                .Index(t => t.IdKeHoachGPMB);
            
            CreateTable(
                "dbo.GPMB_KetQua",
                c => new
                    {
                        IdGPMBKetQua = c.Int(nullable: false, identity: true),
                        DHT_DT = c.Double(),
                        DHT_SoHo = c.Double(),
                        DHT_TongKinhPhi = c.Double(),
                        DHT_DatNNCaThe_DT = c.Double(),
                        DHT_DatNNCaThe_SoHo = c.Double(),
                        DHT_DatNNCaThe_GiaTri = c.Double(),
                        DHT_DatNNCong_DT = c.Double(),
                        DHT_DatNNCong_SoHo = c.Double(),
                        DHT_DatNNCong_GiaTri = c.Double(),
                        DHT_DatKhac_DT = c.Double(),
                        DHT_DatKhac_SoHo = c.Double(),
                        DHT_DatKhac_GiaTri = c.Double(),
                        DHT_MoMa = c.Double(),
                        DHT_MoMa_GiaTri = c.Double(),
                        CTHX_TongDT = c.Double(),
                        CTHX_TongSoHo = c.Double(),
                        CTHX_LapTrinhHoiDong_DuThao_DT = c.Double(),
                        CTHX_LapTrinhHoiDong_DuThao_SoHo = c.Double(),
                        CTHX_CongKhai_DuThao_DT = c.Double(),
                        CTHX_CongKhai_DuThao_SoHo = c.Double(),
                        CTHX_TrinhHoiDong_ChinhThuc_DT = c.Double(),
                        CTHX_TrinhHoiDong_ChinhThuc_SoHo = c.Double(),
                        CTHX_QuyetDinhThuHoi_DT = c.Double(),
                        CTHX_QuyetDinhThuHoi_SoHo = c.Double(),
                        CTHX_ChiTraTien_DT = c.Double(),
                        CTHX_ChiTraTien_SoHo = c.Double(),
                        KKVM_DT = c.Double(),
                        KKVM_SoHo = c.Double(),
                        KKVM_DaPheChuaNhanTien = c.Double(),
                        KKVM_KhongHopTac = c.Double(),
                        KKVM_ChuaDieuTra = c.Double(),
                        GhiChu = c.String(),
                        IDTongQuanKetQua = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGPMBKetQua)
                .ForeignKey("dbo.GPMB_TongQuanKetQua", t => t.IDTongQuanKetQua, cascadeDelete: true)
                .Index(t => t.IDTongQuanKetQua);
            
            CreateTable(
                "dbo.GPMB_TongQuanKetQua",
                c => new
                    {
                        IdGPMBTongQuanKetQua = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        DatNNCaTheTuSDOD_DT = c.Double(),
                        DatNNCaTheTuSDOD_SoHo = c.Double(),
                        DatNNCong_DT = c.Double(),
                        DatNNCong_SoHo = c.Double(),
                        DatKhac_DT = c.Double(),
                        DatKhac_SoHo = c.Double(),
                        MoMa = c.Double(),
                        Tong_DT = c.Double(),
                        Tong_SoHo = c.Double(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGPMBTongQuanKetQua)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn);
            
            CreateTable(
                "dbo.GPMB_KetQua_ChiTiet",
                c => new
                    {
                        IdGPMBKetQuaChiTiet = c.Int(nullable: false, identity: true),
                        IdGPMBKetQua = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdGPMBKetQuaChiTiet)
                .ForeignKey("dbo.AppUsers", t => t.CreatedBy)
                .ForeignKey("dbo.GPMB_KetQua", t => t.IdGPMBKetQua, cascadeDelete: true)
                .Index(t => t.IdGPMBKetQua)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.GPMB_KhoKhanVuongMac",
                c => new
                    {
                        IdKhoKhanVuongMac = c.Int(nullable: false, identity: true),
                        IdKeHoachGPMB = c.Int(nullable: false),
                        NguyenNhanLyDo = c.String(),
                        GiaiPhapTrienKhai = c.String(),
                        DaGiaiQuyet = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKhoKhanVuongMac)
                .ForeignKey("dbo.GPMB_KeHoach", t => t.IdKeHoachGPMB, cascadeDelete: true)
                .Index(t => t.IdKeHoachGPMB);
            
            CreateTable(
                "dbo.GPMP_File_Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdFile = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.GPMB_File", t => t.IdFile, cascadeDelete: true)
                .Index(t => t.IdFile)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.HinhThucDauThaus",
                c => new
                    {
                        IdHinhThucDauThau = c.Int(nullable: false, identity: true),
                        TenHinhThucDauThau = c.String(nullable: false, maxLength: 255),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdHinhThucDauThau);
            
            CreateTable(
                "dbo.Notification_Group_Token",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceTokenId = c.Int(nullable: false),
                        NotificationGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Device_Token", t => t.DeviceTokenId, cascadeDelete: true)
                .ForeignKey("dbo.Notification_Group", t => t.NotificationGroupId, cascadeDelete: true)
                .Index(t => t.DeviceTokenId)
                .Index(t => t.NotificationGroupId);
            
            CreateTable(
                "dbo.Notification_Group",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Notification_Key_Name = c.String(),
                        Notification_Key = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Notification_User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        NotificationId = c.Int(nullable: false),
                        HasRead = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.UserId)
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.NotificationId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.CreatedBy)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.String(maxLength: 128),
                        FunctionId = c.String(maxLength: 50, unicode: false),
                        CanCreate = c.Boolean(nullable: false),
                        CanRead = c.Boolean(nullable: false),
                        CanUpdate = c.Boolean(nullable: false),
                        CanDelete = c.Boolean(nullable: false),
                        CanGet = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppRoles", t => t.RoleId)
                .ForeignKey("dbo.Functions", t => t.FunctionId)
                .Index(t => t.RoleId)
                .Index(t => t.FunctionId);
            
            CreateTable(
                "dbo.PhieuXuLyCongViecNoiBos",
                c => new
                    {
                        IdPhieuXuLyCongViecNoiBo = c.Int(nullable: false, identity: true),
                        TenCongViec = c.String(),
                        NoiDung = c.String(),
                        NgayNhan = c.DateTime(),
                        NgayChuyen = c.DateTime(),
                        IdDuAn = c.Int(),
                        TrangThai = c.Int(),
                        IDPhieuCha = c.Int(),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPhieuXuLyCongViecNoiBo)
                .ForeignKey("dbo.PhieuXuLyCongViecNoiBos", t => t.IDPhieuCha)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn)
                .Index(t => t.IdDuAn)
                .Index(t => t.IDPhieuCha);
            
            CreateTable(
                "dbo.PhieuXuLyCongViecNoiBoUsers",
                c => new
                    {
                        IdPhieuXuLyCongViecNoiBoUser = c.Int(nullable: false, identity: true),
                        IdPhieuXuLyCongViecNoiBo = c.Int(nullable: false),
                        UserXLCVNoiBoEnum = c.Int(nullable: false),
                        DaKy = c.Boolean(nullable: false),
                        UserId = c.String(maxLength: 128),
                        FullName = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPhieuXuLyCongViecNoiBoUser)
                .ForeignKey("dbo.AppUsers", t => t.UserId)
                .ForeignKey("dbo.PhieuXuLyCongViecNoiBos", t => t.IdPhieuXuLyCongViecNoiBo, cascadeDelete: true)
                .Index(t => t.IdPhieuXuLyCongViecNoiBo)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PhongCongTacs",
                c => new
                    {
                        IdChuDauTu = c.Int(nullable: false, identity: true),
                        IdDuAn = c.Int(nullable: false),
                        IdNhomDuAnTheoUser = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChuDauTu)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .ForeignKey("dbo.NhomDuAnTheoUsers", t => t.IdNhomDuAnTheoUser, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdNhomDuAnTheoUser);
            
            CreateTable(
                "dbo.QLTD_CTCV_ChuanBiDauTu",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        IdKeHoachCongViec = c.Int(nullable: false),
                        BCPATK_NoiDung = c.String(),
                        PDDT_SoQD = c.String(),
                        PDDT_NgayPQ = c.DateTime(),
                        PDDT_GiaTri = c.Double(),
                        PDKHLCNT_SoQD = c.String(),
                        PDKHLCNT_NgayPD = c.DateTime(),
                        PDKHLCNT_GiaTri = c.Double(),
                        KSHT_NoiDung = c.String(),
                        CGDD_NoiDung = c.String(),
                        SLHTKT_NoiDung = c.String(),
                        PDQH_KhongPheDuyet = c.Boolean(nullable: false),
                        PDQH_SoQD_QHCT = c.String(),
                        PDQH_NgayPD_QHCT = c.DateTime(),
                        PDQH_SoQD_QHTMB = c.String(),
                        PDQH_NgayPD_QHTMB = c.DateTime(),
                        KSDCDH_NoiDung = c.String(),
                        TTTDPCCC_NoiDung = c.String(),
                        TTK_NoiDung = c.String(),
                        LHSCBDT_KhoKhanVuongMac = c.String(),
                        LHSCBDT_KienNghiDeXuat = c.String(),
                        LHSCBDT_NgayHoanThanh = c.DateTime(),
                        LHSCBDT_SoQD = c.String(),
                        LHSCBDT_NgayLaySo = c.DateTime(),
                        THSCBDT_SoToTrinh = c.String(),
                        THSCBDT_NgayTrinh = c.DateTime(),
                        PDDA_BCKTKT_SoQD = c.String(),
                        PDDA_BCKTKT_NgayPD = c.DateTime(),
                        PDDA_BCKTKT_GiaTri = c.Double(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .ForeignKey("dbo.QLTD_KeHoachCongViec", t => t.IdKeHoachCongViec, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec)
                .Index(t => t.IdKeHoachCongViec);
            
            CreateTable(
                "dbo.QLTD_TDTH_ChuanBiDauTu",
                c => new
                    {
                        IdTienDoThucHien = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TenDonViThucHien = c.String(),
                        DonViThamDinh = c.String(),
                        DonViPhoiHop = c.String(),
                        CoQuanPheDuyet = c.String(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoThucHien)
                .ForeignKey("dbo.QLTD_CTCV_ChuanBiDauTu", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.QLTD_THCT_ChuanBiDauTu",
                c => new
                    {
                        IdThucHienChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDoThucHien = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHienChiTiet)
                .ForeignKey("dbo.QLTD_TDTH_ChuanBiDauTu", t => t.IdTienDoThucHien, cascadeDelete: true)
                .Index(t => t.IdTienDoThucHien);
            
            CreateTable(
                "dbo.QLTD_CTCV_ChuanBiThucHien",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        IdKeHoachCongViec = c.Int(nullable: false),
                        PDDT_SoQuyetDinh = c.String(),
                        PDDT_NgayPheDuyet = c.DateTime(),
                        PDDT_GiaTri = c.Double(),
                        PDKHLCNT_SoQuyetDinh = c.String(),
                        PDKHLCNT_NgayPheDuyet = c.DateTime(),
                        PDKHLCNT_GiaTri = c.Double(),
                        LHS_KhoKhanVuongMac = c.String(),
                        LHS_KienNghiDeXuat = c.String(),
                        LHS_NgayHoanThanh = c.DateTime(),
                        THS_SoToTrinh = c.String(),
                        LHS_NgayTrinh = c.DateTime(),
                        PDDA_SoQuyetDinh = c.String(),
                        PDDA_NgayPheDuyet = c.DateTime(),
                        PDDA_GiaTri = c.Double(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .ForeignKey("dbo.QLTD_KeHoachCongViec", t => t.IdKeHoachCongViec, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec)
                .Index(t => t.IdKeHoachCongViec);
            
            CreateTable(
                "dbo.QLTD_TDTH_ChuanBiThucHien",
                c => new
                    {
                        IdTienDoThucHien = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TenDonViThucHien = c.String(),
                        DonViThamDinh = c.String(),
                        DonViPhoiHop = c.String(),
                        CoQuanPheDuyet = c.String(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoThucHien)
                .ForeignKey("dbo.QLTD_CTCV_ChuanBiThucHien", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.QLTD_THCT_ChuanBiThucHien",
                c => new
                    {
                        IdThucHienChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDoThucHien = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHienChiTiet)
                .ForeignKey("dbo.QLTD_TDTH_ChuanBiThucHien", t => t.IdTienDoThucHien, cascadeDelete: true)
                .Index(t => t.IdTienDoThucHien);
            
            CreateTable(
                "dbo.QLTD_CTCV_ChuTruongDauTu",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        IdKeHoachCongViec = c.Int(nullable: false),
                        CTDD_CoSanDiaDiem = c.Boolean(nullable: false),
                        CTDD_SoVB = c.String(),
                        CTDD_NgayPD = c.DateTime(),
                        LCTDT_TienDoThucHien = c.String(),
                        LCTDT_KhoKhanVuongMac = c.String(),
                        LCTDT_KienNghiDeXuat = c.String(),
                        LCTDT_NgayHT = c.DateTime(),
                        THSPDDA_NgayTrinhThucTe = c.DateTime(),
                        PQCTDT_SoQD = c.String(),
                        PQCTDT_NgayLaySo = c.DateTime(),
                        PQCTDT_GiaTri = c.Double(),
                        PQCTDT_NgayPheDuyetTT = c.DateTime(),
                        PQCTDT_CoQuanPD = c.Int(),
                        BTGNV_SoToTrinh = c.String(),
                        BTGNV_SoBaoCao = c.String(),
                        BTGNV_NgayTrinh = c.DateTime(),
                        HGNV_SoToTrinh = c.String(),
                        HGNV_SoBaoCao = c.String(),
                        HGNV_NgayTrinh = c.DateTime(),
                        HGNV_SoVBYKien = c.String(),
                        HTPPDGNV_SoVanBan = c.String(),
                        HTPPDGNV_NgayNhan = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .ForeignKey("dbo.QLTD_KeHoachCongViec", t => t.IdKeHoachCongViec, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec)
                .Index(t => t.IdKeHoachCongViec);
            
            CreateTable(
                "dbo.QLTD_TDTH_ChuTruongDauTu",
                c => new
                    {
                        IdTienDoThucHien = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TenDonViThucHien = c.String(),
                        DonViThamDinh = c.String(),
                        DonViPhoiHop = c.String(),
                        CoQuanPheDuyet = c.String(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoThucHien)
                .ForeignKey("dbo.QLTD_CTCV_ChuTruongDauTu", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.QLTD_THCT_ChuTruongDauTu",
                c => new
                    {
                        IdThucHienChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDoThucHien = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHienChiTiet)
                .ForeignKey("dbo.QLTD_TDTH_ChuTruongDauTu", t => t.IdTienDoThucHien, cascadeDelete: true)
                .Index(t => t.IdTienDoThucHien);
            
            CreateTable(
                "dbo.QLTD_CTCV_QuyetToanDuAn",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        IdKeHoachCongViec = c.Int(nullable: false),
                        HSQLCL_DaHoanThanh = c.Boolean(nullable: false),
                        TTQT_DangThamTra = c.String(),
                        TTQT_SoThamTra = c.String(),
                        TTQT_NgayThamTra = c.DateTime(),
                        TTQT_GiaTri = c.Double(),
                        KTDL_KhongKiemToan = c.Boolean(nullable: false),
                        KTDL_DangKiemToan = c.String(),
                        KTDL_GiaTriSauKiemToan = c.Double(),
                        HTHSQT_GiaTriQuyetToan = c.Double(),
                        HTQT_SoQD = c.String(),
                        HTQT_NgayPD = c.DateTime(),
                        HTQT_GiaTri = c.Double(),
                        HTQT_CongNoThu = c.Double(),
                        HTQT_CongNoTra = c.Double(),
                        HTQT_BoTriTraCongNo = c.Double(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .ForeignKey("dbo.QLTD_KeHoachCongViec", t => t.IdKeHoachCongViec, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec)
                .Index(t => t.IdKeHoachCongViec);
            
            CreateTable(
                "dbo.QLTD_TDTH_QuyetToanDuAn",
                c => new
                    {
                        IdTienDoThucHien = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TenDonViThucHien = c.String(),
                        DonViThamDinh = c.String(),
                        DonViPhoiHop = c.String(),
                        CoQuanPheDuyet = c.String(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoThucHien)
                .ForeignKey("dbo.QLTD_CTCV_QuyetToanDuAn", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.QLTD_THCT_QuyetToanDuAn",
                c => new
                    {
                        IdThucHienChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDoThucHien = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHienChiTiet)
                .ForeignKey("dbo.QLTD_TDTH_QuyetToanDuAn", t => t.IdTienDoThucHien, cascadeDelete: true)
                .Index(t => t.IdTienDoThucHien);
            
            CreateTable(
                "dbo.QLTD_CTCV_ThucHienDuAn",
                c => new
                    {
                        IdChiTietCongViec = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        IdCongViec = c.Int(nullable: false),
                        IdKeHoachCongViec = c.Int(nullable: false),
                        GPMB_DamBaoTienDo = c.String(),
                        GPMB_KhoKhanVuongMac = c.String(),
                        GPMB_DeXuatGiaiPhap = c.String(),
                        CTHLCNT_DangLapTKBVTC = c.String(),
                        CTHLCNT_DangTrinhTKBVTC = c.String(),
                        CTHLCNT_SoQDKHLCNT = c.String(),
                        CTHLCNT_NgayPheDuyetKHLCNT = c.DateTime(),
                        CTHLCNT_SoNgayQDPD_KHLCNT = c.String(),
                        CTHLCNT_KhoKhan = c.String(),
                        DLCNT_SoQDKQLCNT = c.String(),
                        DLCNT_NgayPheDuyet = c.DateTime(),
                        DLCNT_GiaTri = c.Double(),
                        DLCNT_NgayPD_KQLCNT = c.DateTime(),
                        DTCXD_NgayBanGiaoMB = c.DateTime(),
                        DTCXD_ThoiGianKhoiCong = c.String(),
                        DTCXD_KhoKhanVuongMac = c.String(),
                        DTCXD_NgayBBNT_HoanThanhCT = c.DateTime(),
                        HTTC_NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        NgayHoanThanh = c.DateTime(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdChiTietCongViec)
                .ForeignKey("dbo.QLTD_CongViec", t => t.IdCongViec)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach)
                .ForeignKey("dbo.QLTD_KeHoachCongViec", t => t.IdKeHoachCongViec, cascadeDelete: true)
                .Index(t => t.IdKeHoach)
                .Index(t => t.IdCongViec)
                .Index(t => t.IdKeHoachCongViec);
            
            CreateTable(
                "dbo.QLTD_TDTH_ThucHienDuAn",
                c => new
                    {
                        IdTienDoThucHien = c.Int(nullable: false, identity: true),
                        IdChiTietCongViec = c.Int(nullable: false),
                        NoiDung = c.String(),
                        TenDonViThucHien = c.String(),
                        DonViThamDinh = c.String(),
                        DonViPhoiHop = c.String(),
                        CoQuanPheDuyet = c.String(),
                        NgayBatDau = c.DateTime(),
                        NgayHoanThanh = c.DateTime(),
                        HoanThanh = c.Int(nullable: false),
                        GhiChu = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdTienDoThucHien)
                .ForeignKey("dbo.QLTD_CTCV_ThucHienDuAn", t => t.IdChiTietCongViec, cascadeDelete: true)
                .Index(t => t.IdChiTietCongViec);
            
            CreateTable(
                "dbo.QLTD_THCT_ThucHienDuAn",
                c => new
                    {
                        IdThucHienChiTiet = c.Int(nullable: false, identity: true),
                        IdTienDoThucHien = c.Int(nullable: false),
                        NoiDung = c.String(),
                        ThayDoi = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThucHienChiTiet)
                .ForeignKey("dbo.QLTD_TDTH_ThucHienDuAn", t => t.IdTienDoThucHien, cascadeDelete: true)
                .Index(t => t.IdTienDoThucHien);
            
            CreateTable(
                "dbo.QLTD_KhoKhanVuongMac",
                c => new
                    {
                        IdKhoKhanVuongMac = c.Int(nullable: false, identity: true),
                        IdKeHoach = c.Int(nullable: false),
                        NguyenNhanLyDo = c.String(),
                        GiaiPhapTrienKhai = c.String(),
                        DaGiaiQuyet = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdKhoKhanVuongMac)
                .ForeignKey("dbo.QLTD_KeHoach", t => t.IdKeHoach, cascadeDelete: true)
                .Index(t => t.IdKeHoach);
            
            CreateTable(
                "dbo.SystemLogs",
                c => new
                    {
                        IdSystemLogs = c.Int(nullable: false, identity: true),
                        TenChucNang = c.String(),
                        ThaoTac = c.String(),
                        NguoiThaoTac = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdSystemLogs);
            
            CreateTable(
                "dbo.ThuMucVanBanDens",
                c => new
                    {
                        IdThuMucVanBanDen = c.Int(nullable: false, identity: true),
                        TenThuMucVanBanDen = c.String(nullable: false, maxLength: 255),
                        IdThuMucCha = c.Int(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdThuMucVanBanDen);
            
            CreateTable(
                "dbo.VanBanDens",
                c => new
                    {
                        IdVanBanDen = c.Int(nullable: false, identity: true),
                        SoVanBanDen = c.String(nullable: false, maxLength: 255),
                        TenVanBanDen = c.String(nullable: false, maxLength: 255),
                        FileName = c.String(maxLength: 255),
                        FileUrl = c.String(storeType: "ntext"),
                        NgayDen = c.DateTime(),
                        NgayNhan = c.DateTime(),
                        ThoiHanHoanThanh = c.DateTime(),
                        TrangThai = c.Int(),
                        TPYeuCau = c.String(),
                        DeXuatTPTH = c.String(),
                        YKienChiDaoGiamDoc = c.String(),
                        NoiGui = c.String(maxLength: 255),
                        MoTa = c.String(),
                        GhiChu = c.String(),
                        IdThuMucVanBanDen = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdVanBanDen)
                .ForeignKey("dbo.ThuMucVanBanDens", t => t.IdThuMucVanBanDen, cascadeDelete: true)
                .Index(t => t.IdThuMucVanBanDen);
            
            CreateTable(
                "dbo.VanBanDenThucHiens",
                c => new
                    {
                        IdVanBanDenThucHien = c.Int(nullable: false, identity: true),
                        IdVanBanDen = c.Int(nullable: false),
                        NoiDung = c.String(),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                        VanBanDen_IdVanBanDen = c.Int(),
                    })
                .PrimaryKey(t => t.IdVanBanDenThucHien)
                .ForeignKey("dbo.VanBanDens", t => t.IdVanBanDen, cascadeDelete: true)
                .ForeignKey("dbo.VanBanDens", t => t.VanBanDen_IdVanBanDen)
                .Index(t => t.IdVanBanDen)
                .Index(t => t.VanBanDen_IdVanBanDen);
            
            CreateTable(
                "dbo.VanBanDenUsers",
                c => new
                    {
                        IdVanBanDenUser = c.Int(nullable: false, identity: true),
                        IdVanBanDen = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        HasRead = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 256),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 256),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdVanBanDenUser)
                .ForeignKey("dbo.AppUsers", t => t.UserId)
                .ForeignKey("dbo.VanBanDens", t => t.IdVanBanDen, cascadeDelete: true)
                .Index(t => t.IdVanBanDen)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.TrangThaiChats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdUser1 = c.String(),
                        IdUser2 = c.String(),
                        IdNhom = c.Boolean(nullable: false),
                        TrangThai = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VonCapNhatKLNTs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        KLNTVonHuyen = c.String(),
                        TDGNVonHuyen = c.String(),
                        KLNTVonTP = c.String(),
                        TDGNVonTP = c.String(),
                        IdDuAn = c.Int(nullable: false),
                        MaDuAn = c.String(),
                        IdFile = c.Int(),
                        CreateDateTime = c.DateTime(nullable: false),
                        isVonHuyen = c.Boolean(nullable: false),
                        isVonTP = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CapNhatKLNTs", t => t.IdFile)
                .ForeignKey("dbo.DuAns", t => t.IdDuAn, cascadeDelete: true)
                .Index(t => t.IdDuAn)
                .Index(t => t.IdFile);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VonCapNhatKLNTs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.VonCapNhatKLNTs", "IdFile", "dbo.CapNhatKLNTs");
            DropForeignKey("dbo.VanBanDenUsers", "IdVanBanDen", "dbo.VanBanDens");
            DropForeignKey("dbo.VanBanDenUsers", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.VanBanDenThucHiens", "VanBanDen_IdVanBanDen", "dbo.VanBanDens");
            DropForeignKey("dbo.VanBanDenThucHiens", "IdVanBanDen", "dbo.VanBanDens");
            DropForeignKey("dbo.VanBanDens", "IdThuMucVanBanDen", "dbo.ThuMucVanBanDens");
            DropForeignKey("dbo.AppUserRoles", "IdentityRole_Id", "dbo.AppRoles");
            DropForeignKey("dbo.QLTD_KhoKhanVuongMac", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_THCT_ThucHienDuAn", "IdTienDoThucHien", "dbo.QLTD_TDTH_ThucHienDuAn");
            DropForeignKey("dbo.QLTD_TDTH_ThucHienDuAn", "IdChiTietCongViec", "dbo.QLTD_CTCV_ThucHienDuAn");
            DropForeignKey("dbo.QLTD_CTCV_ThucHienDuAn", "IdKeHoachCongViec", "dbo.QLTD_KeHoachCongViec");
            DropForeignKey("dbo.QLTD_CTCV_ThucHienDuAn", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_CTCV_ThucHienDuAn", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.QLTD_THCT_QuyetToanDuAn", "IdTienDoThucHien", "dbo.QLTD_TDTH_QuyetToanDuAn");
            DropForeignKey("dbo.QLTD_TDTH_QuyetToanDuAn", "IdChiTietCongViec", "dbo.QLTD_CTCV_QuyetToanDuAn");
            DropForeignKey("dbo.QLTD_CTCV_QuyetToanDuAn", "IdKeHoachCongViec", "dbo.QLTD_KeHoachCongViec");
            DropForeignKey("dbo.QLTD_CTCV_QuyetToanDuAn", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_CTCV_QuyetToanDuAn", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.QLTD_THCT_ChuTruongDauTu", "IdTienDoThucHien", "dbo.QLTD_TDTH_ChuTruongDauTu");
            DropForeignKey("dbo.QLTD_TDTH_ChuTruongDauTu", "IdChiTietCongViec", "dbo.QLTD_CTCV_ChuTruongDauTu");
            DropForeignKey("dbo.QLTD_CTCV_ChuTruongDauTu", "IdKeHoachCongViec", "dbo.QLTD_KeHoachCongViec");
            DropForeignKey("dbo.QLTD_CTCV_ChuTruongDauTu", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_CTCV_ChuTruongDauTu", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.QLTD_THCT_ChuanBiThucHien", "IdTienDoThucHien", "dbo.QLTD_TDTH_ChuanBiThucHien");
            DropForeignKey("dbo.QLTD_TDTH_ChuanBiThucHien", "IdChiTietCongViec", "dbo.QLTD_CTCV_ChuanBiThucHien");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiThucHien", "IdKeHoachCongViec", "dbo.QLTD_KeHoachCongViec");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiThucHien", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiThucHien", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.QLTD_THCT_ChuanBiDauTu", "IdTienDoThucHien", "dbo.QLTD_TDTH_ChuanBiDauTu");
            DropForeignKey("dbo.QLTD_TDTH_ChuanBiDauTu", "IdChiTietCongViec", "dbo.QLTD_CTCV_ChuanBiDauTu");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiDauTu", "IdKeHoachCongViec", "dbo.QLTD_KeHoachCongViec");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiDauTu", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_CTCV_ChuanBiDauTu", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.PhongCongTacs", "IdNhomDuAnTheoUser", "dbo.NhomDuAnTheoUsers");
            DropForeignKey("dbo.PhongCongTacs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.PhieuXuLyCongViecNoiBoUsers", "IdPhieuXuLyCongViecNoiBo", "dbo.PhieuXuLyCongViecNoiBos");
            DropForeignKey("dbo.PhieuXuLyCongViecNoiBoUsers", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.PhieuXuLyCongViecNoiBos", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.PhieuXuLyCongViecNoiBos", "IDPhieuCha", "dbo.PhieuXuLyCongViecNoiBos");
            DropForeignKey("dbo.Permissions", "FunctionId", "dbo.Functions");
            DropForeignKey("dbo.Permissions", "RoleId", "dbo.AppRoles");
            DropForeignKey("dbo.Notification_User", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.Notifications", "CreatedBy", "dbo.AppUsers");
            DropForeignKey("dbo.Notification_User", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.Notification_Group_Token", "NotificationGroupId", "dbo.Notification_Group");
            DropForeignKey("dbo.Notification_Group_Token", "DeviceTokenId", "dbo.Device_Token");
            DropForeignKey("dbo.GPMP_File_Comments", "IdFile", "dbo.GPMB_File");
            DropForeignKey("dbo.GPMP_File_Comments", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.GPMB_KhoKhanVuongMac", "IdKeHoachGPMB", "dbo.GPMB_KeHoach");
            DropForeignKey("dbo.GPMB_KetQua_ChiTiet", "IdGPMBKetQua", "dbo.GPMB_KetQua");
            DropForeignKey("dbo.GPMB_KetQua_ChiTiet", "CreatedBy", "dbo.AppUsers");
            DropForeignKey("dbo.GPMB_KetQua", "IDTongQuanKetQua", "dbo.GPMB_TongQuanKetQua");
            DropForeignKey("dbo.GPMB_TongQuanKetQua", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.GPMB_KeHoachTienDoChung", "IdKeHoachGPMB", "dbo.GPMB_KeHoach");
            DropForeignKey("dbo.GPMB_File", "CreatedBy", "dbo.AppUsers");
            DropForeignKey("dbo.GPMB_DangKyKeHoach", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.GPMB_CTCV_Dots", "IdChiTietCongViec", "dbo.GPMB_CTCV");
            DropForeignKey("dbo.GPMB_CTCV", "IdKeHoachCongViecGPMB", "dbo.GPMB_KeHoachCongViec");
            DropForeignKey("dbo.GPMB_KeHoach", "IdNhomDuAnTheoUser", "dbo.NhomDuAnTheoUsers");
            DropForeignKey("dbo.GPMB_KeHoachCongViec", "IdKeHoachGPMB", "dbo.GPMB_KeHoach");
            DropForeignKey("dbo.GPMB_KeHoach", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.GPMB_KeHoach", "CreatedBy", "dbo.AppUsers");
            DropForeignKey("dbo.GPMB_KeHoachCongViec", "IdCongViecGPMB", "dbo.GPMB_CongViec");
            DropForeignKey("dbo.GPMB_Comment", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.GPMB_Comment", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.GiaTriThanhToanTheoNams", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.Functions", "ParentId", "dbo.Functions");
            DropForeignKey("dbo.DuAnHoSoQuyetToans", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.DuAnHoSoQuyetToans", "IdDanhMucHoSoQuyetToan", "dbo.DanhMucHoSoQuyetToans");
            DropForeignKey("dbo.Device_Token", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.Comments", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.Comments", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.ChatNhomApps", "IdUser2", "dbo.NhomChatApps");
            DropForeignKey("dbo.ChatNhomApps", "IdUser1", "dbo.AppUsers");
            DropForeignKey("dbo.ChatApps", "IdUser2", "dbo.AppUsers");
            DropForeignKey("dbo.ChatApps", "IdUser1", "dbo.AppUsers");
            DropForeignKey("dbo.CamKetGiaiNgans", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.BaoLanhTHHDs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.VanBanDuAns", "IdThuMuc", "dbo.ThuMucs");
            DropForeignKey("dbo.ThuMucs", "IdThuMucMacDinh", "dbo.ThuMucMacDinhs");
            DropForeignKey("dbo.VanBanDuAns", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.TinhTrangDuAns", "IdTinhTrangDuAnCha", "dbo.TinhTrangDuAns");
            DropForeignKey("dbo.DuAns", "IdTinhTrangDuAn", "dbo.TinhTrangDuAns");
            DropForeignKey("dbo.QLTD_KeHoachTienDoChung", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_KeHoach", "IdKeHoachDC", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_KeHoachCongViec", "IdKeHoach", "dbo.QLTD_KeHoach");
            DropForeignKey("dbo.QLTD_KeHoachCongViec", "IdCongViec", "dbo.QLTD_CongViec");
            DropForeignKey("dbo.QLTD_CongViec", "IdGiaiDoan", "dbo.QLTD_GiaiDoan");
            DropForeignKey("dbo.QLTD_KeHoach", "IdGiaiDoan", "dbo.QLTD_GiaiDoan");
            DropForeignKey("dbo.QLTD_KeHoach", "IdNhomDuAnTheoUser", "dbo.NhomDuAnTheoUsers");
            DropForeignKey("dbo.QLTD_KeHoach", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.QLTD_KeHoach", "CreatedBy", "dbo.AppUsers");
            DropForeignKey("dbo.DuAns", "IdNhomDuAn", "dbo.NhomDuAns");
            DropForeignKey("dbo.DuAns", "IdLoaiCapCongTrinh", "dbo.LoaiCapCongTrinhs");
            DropForeignKey("dbo.LinhVucNganhNghes", "IdLienKetLinhVuc", "dbo.LienKetLinhVucs");
            DropForeignKey("dbo.DuAns", "IdLinhVucNganhNghe", "dbo.LinhVucNganhNghes");
            DropForeignKey("dbo.DuAns", "IdHinhThucQuanLy", "dbo.HinhThucQuanLys");
            DropForeignKey("dbo.DuAns", "IdHinhThucDauTu", "dbo.HinhThucDauTus");
            DropForeignKey("dbo.DuAns", "IdGiaiDoanDuAn", "dbo.GiaiDoanDuAns");
            DropForeignKey("dbo.DuAnUsers", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.DuAnUsers", "UserId", "dbo.AppUsers");
            DropForeignKey("dbo.AppUserRoles", "AppUser_Id", "dbo.AppUsers");
            DropForeignKey("dbo.AppUsers", "IdNhomDuAnTheoUser", "dbo.NhomDuAnTheoUsers");
            DropForeignKey("dbo.DuAns", "IdNhomDuAnTheoUser", "dbo.NhomDuAnTheoUsers");
            DropForeignKey("dbo.AppUserLogins", "AppUser_Id", "dbo.AppUsers");
            DropForeignKey("dbo.AppUserClaims", "AppUser_Id", "dbo.AppUsers");
            DropForeignKey("dbo.AppRoles", "AppUser_Id", "dbo.AppUsers");
            DropForeignKey("dbo.DuAnQuanLys", "IdQuanLyDA", "dbo.QuanLyDAs");
            DropForeignKey("dbo.DuAnQuanLys", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.ChuTruongDauTus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.NguonVonQuyetToanDuAns", "IdQuyetToanDuAn", "dbo.QuyetToanDuAns");
            DropForeignKey("dbo.NguonVonQuyetToanDuAns", "IdNguonVonDuAn", "dbo.NguonVonDuAns");
            DropForeignKey("dbo.QuyetToanDuAns", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.QuyetToanDuAns", "IdCoQuanPheDuyet", "dbo.CoQuanPheDuyets");
            DropForeignKey("dbo.GoiThaus", "IdPhuongThucDauThau", "dbo.PhuongThucDauThaus");
            DropForeignKey("dbo.GoiThaus", "IdLinhVucDauThau", "dbo.LinhVucDauThaus");
            DropForeignKey("dbo.GoiThaus", "IdKeHoachLuaChonNhaThau", "dbo.KeHoachLuaChonNhaThaus");
            DropForeignKey("dbo.HoSoMoiThaus", "IdGoiThau", "dbo.GoiThaus");
            DropForeignKey("dbo.HoSoMoiThaus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.TienDoChiTiets", "IdTienDo", "dbo.TienDoThucHiens");
            DropForeignKey("dbo.TienDoThucHiens", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.QuyetToanHopDongs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.PhuLucHopDongs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.HopDongs", "IdLoaiHopDong", "dbo.LoaiHopDongs");
            DropForeignKey("dbo.GoiThaus", "IdLoaiHopDong", "dbo.LoaiHopDongs");
            DropForeignKey("dbo.HopDongs", "IdGoiThau", "dbo.GoiThaus");
            DropForeignKey("dbo.HopDongs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.NguonVonThanhToanCPKs", "IdThanhToanChiPhiKhac", "dbo.ThanhToanChiPhiKhacs");
            DropForeignKey("dbo.NguonVonThanhToanCPKs", "IdNguonVonDuAn", "dbo.NguonVonDuAns");
            DropForeignKey("dbo.NguonVonKeHoachVons", "IdNguonVonDuAn", "dbo.NguonVonDuAns");
            DropForeignKey("dbo.NguonVonKeHoachVons", "IdKeHoachVon", "dbo.KeHoachVons");
            DropForeignKey("dbo.KeHoachVons", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.KeHoachVons", "IdCoQuanPheDuyet", "dbo.CoQuanPheDuyets");
            DropForeignKey("dbo.NguonVonGoiThauThanhToans", "IdThanhToanHopDong", "dbo.ThanhToanHopDongs");
            DropForeignKey("dbo.ThanhToanHopDongs", "IdThucHien", "dbo.ThucHienHopDongs");
            DropForeignKey("dbo.ThucHienHopDongs", "IdLoaiChiPhi", "dbo.LoaiChiPhis");
            DropForeignKey("dbo.ThucHienHopDongs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.ThucHienHopDongs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.ThanhToanHopDongs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.NguonVonGoiThauThanhToans", "IdNguonVonDuAnGoiThau", "dbo.NguonVonDuAnGoiThaus");
            DropForeignKey("dbo.NguonVonGoiThauTamUngs", "IdTamUng", "dbo.TamUngHopDongs");
            DropForeignKey("dbo.TamUngHopDongs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.NguonVonGoiThauTamUngs", "IdNguonVonDuAnGoiThau", "dbo.NguonVonDuAnGoiThaus");
            DropForeignKey("dbo.NguonVonDuAnGoiThaus", "IdNguonVonDuAn", "dbo.NguonVonDuAns");
            DropForeignKey("dbo.NguonVonDuAnGoiThaus", "IdGoiThau", "dbo.GoiThaus");
            DropForeignKey("dbo.NguonVonDuAns", "IdNguonVon", "dbo.NguonVons");
            DropForeignKey("dbo.NguonVons", "IdNguonVonCha", "dbo.NguonVons");
            DropForeignKey("dbo.ThamDinhLapQuanLyDauTus", "IdLapQuanLyDauTu", "dbo.LapQuanLyDauTus");
            DropForeignKey("dbo.NguonVonDuAns", "IdLapQuanLyDauTu", "dbo.LapQuanLyDauTus");
            DropForeignKey("dbo.LapQuanLyDauTus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.LapQuanLyDauTus", "IdCoQuanPheDuyet", "dbo.CoQuanPheDuyets");
            DropForeignKey("dbo.NguonVonDuAns", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.ThanhToanChiPhiKhacs", "IdLoaiChiPhi", "dbo.LoaiChiPhis");
            DropForeignKey("dbo.ThanhToanChiPhiKhacs", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.ThanhToanChiPhiKhacs", "IdNhaThau", "dbo.DanhMucNhaThaus");
            DropForeignKey("dbo.KetQuaDauThaus", "IdGoiThau", "dbo.GoiThaus");
            DropForeignKey("dbo.KetQuaDauThaus", "IdNhaThau", "dbo.DanhMucNhaThaus");
            DropForeignKey("dbo.HopDongs", "IdNhaThau", "dbo.DanhMucNhaThaus");
            DropForeignKey("dbo.DanhMucNhaThaus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.BaoLanhTHHDs", "IdHopDong", "dbo.HopDongs");
            DropForeignKey("dbo.GoiThaus", "IdHinhThucLuaChon", "dbo.HinhThucLuaChonNhaThaus");
            DropForeignKey("dbo.GoiThaus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.KeHoachLuaChonNhaThaus", "IdDuAn", "dbo.DuAns");
            DropForeignKey("dbo.KeHoachLuaChonNhaThaus", "IdDieuChinhKeHoachLuaChonNhaThau", "dbo.KeHoachLuaChonNhaThaus");
            DropForeignKey("dbo.KeHoachLuaChonNhaThaus", "IdCoQuanPheDuyet", "dbo.CoQuanPheDuyets");
            DropForeignKey("dbo.ChuTruongDauTus", "IdCoQuanPheDuyet", "dbo.CoQuanPheDuyets");
            DropForeignKey("dbo.DuAns", "IdChuDauTu", "dbo.ChuDauTus");
            DropIndex("dbo.VonCapNhatKLNTs", new[] { "IdFile" });
            DropIndex("dbo.VonCapNhatKLNTs", new[] { "IdDuAn" });
            DropIndex("dbo.VanBanDenUsers", new[] { "UserId" });
            DropIndex("dbo.VanBanDenUsers", new[] { "IdVanBanDen" });
            DropIndex("dbo.VanBanDenThucHiens", new[] { "VanBanDen_IdVanBanDen" });
            DropIndex("dbo.VanBanDenThucHiens", new[] { "IdVanBanDen" });
            DropIndex("dbo.VanBanDens", new[] { "IdThuMucVanBanDen" });
            DropIndex("dbo.QLTD_KhoKhanVuongMac", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_THCT_ThucHienDuAn", new[] { "IdTienDoThucHien" });
            DropIndex("dbo.QLTD_TDTH_ThucHienDuAn", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.QLTD_CTCV_ThucHienDuAn", new[] { "IdKeHoachCongViec" });
            DropIndex("dbo.QLTD_CTCV_ThucHienDuAn", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_CTCV_ThucHienDuAn", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_THCT_QuyetToanDuAn", new[] { "IdTienDoThucHien" });
            DropIndex("dbo.QLTD_TDTH_QuyetToanDuAn", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.QLTD_CTCV_QuyetToanDuAn", new[] { "IdKeHoachCongViec" });
            DropIndex("dbo.QLTD_CTCV_QuyetToanDuAn", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_CTCV_QuyetToanDuAn", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_THCT_ChuTruongDauTu", new[] { "IdTienDoThucHien" });
            DropIndex("dbo.QLTD_TDTH_ChuTruongDauTu", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuTruongDauTu", new[] { "IdKeHoachCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuTruongDauTu", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuTruongDauTu", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_THCT_ChuanBiThucHien", new[] { "IdTienDoThucHien" });
            DropIndex("dbo.QLTD_TDTH_ChuanBiThucHien", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiThucHien", new[] { "IdKeHoachCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiThucHien", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiThucHien", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_THCT_ChuanBiDauTu", new[] { "IdTienDoThucHien" });
            DropIndex("dbo.QLTD_TDTH_ChuanBiDauTu", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiDauTu", new[] { "IdKeHoachCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiDauTu", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_CTCV_ChuanBiDauTu", new[] { "IdKeHoach" });
            DropIndex("dbo.PhongCongTacs", new[] { "IdNhomDuAnTheoUser" });
            DropIndex("dbo.PhongCongTacs", new[] { "IdDuAn" });
            DropIndex("dbo.PhieuXuLyCongViecNoiBoUsers", new[] { "UserId" });
            DropIndex("dbo.PhieuXuLyCongViecNoiBoUsers", new[] { "IdPhieuXuLyCongViecNoiBo" });
            DropIndex("dbo.PhieuXuLyCongViecNoiBos", new[] { "IDPhieuCha" });
            DropIndex("dbo.PhieuXuLyCongViecNoiBos", new[] { "IdDuAn" });
            DropIndex("dbo.Permissions", new[] { "FunctionId" });
            DropIndex("dbo.Permissions", new[] { "RoleId" });
            DropIndex("dbo.Notifications", new[] { "CreatedBy" });
            DropIndex("dbo.Notification_User", new[] { "NotificationId" });
            DropIndex("dbo.Notification_User", new[] { "UserId" });
            DropIndex("dbo.Notification_Group_Token", new[] { "NotificationGroupId" });
            DropIndex("dbo.Notification_Group_Token", new[] { "DeviceTokenId" });
            DropIndex("dbo.GPMP_File_Comments", new[] { "UserId" });
            DropIndex("dbo.GPMP_File_Comments", new[] { "IdFile" });
            DropIndex("dbo.GPMB_KhoKhanVuongMac", new[] { "IdKeHoachGPMB" });
            DropIndex("dbo.GPMB_KetQua_ChiTiet", new[] { "CreatedBy" });
            DropIndex("dbo.GPMB_KetQua_ChiTiet", new[] { "IdGPMBKetQua" });
            DropIndex("dbo.GPMB_TongQuanKetQua", new[] { "IdDuAn" });
            DropIndex("dbo.GPMB_KetQua", new[] { "IDTongQuanKetQua" });
            DropIndex("dbo.GPMB_KeHoachTienDoChung", new[] { "IdKeHoachGPMB" });
            DropIndex("dbo.GPMB_File", new[] { "CreatedBy" });
            DropIndex("dbo.GPMB_DangKyKeHoach", new[] { "IdDuAn" });
            DropIndex("dbo.GPMB_CTCV_Dots", new[] { "IdChiTietCongViec" });
            DropIndex("dbo.GPMB_KeHoach", new[] { "CreatedBy" });
            DropIndex("dbo.GPMB_KeHoach", new[] { "IdDuAn" });
            DropIndex("dbo.GPMB_KeHoach", new[] { "IdNhomDuAnTheoUser" });
            DropIndex("dbo.GPMB_KeHoachCongViec", new[] { "IdCongViecGPMB" });
            DropIndex("dbo.GPMB_KeHoachCongViec", new[] { "IdKeHoachGPMB" });
            DropIndex("dbo.GPMB_CTCV", new[] { "IdKeHoachCongViecGPMB" });
            DropIndex("dbo.GPMB_Comment", new[] { "UserId" });
            DropIndex("dbo.GPMB_Comment", new[] { "IdDuAn" });
            DropIndex("dbo.GiaTriThanhToanTheoNams", new[] { "IdDuAn" });
            DropIndex("dbo.Functions", new[] { "ParentId" });
            DropIndex("dbo.DuAnHoSoQuyetToans", new[] { "IdDuAn" });
            DropIndex("dbo.DuAnHoSoQuyetToans", new[] { "IdDanhMucHoSoQuyetToan" });
            DropIndex("dbo.Device_Token", new[] { "UserId" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropIndex("dbo.Comments", new[] { "IdDuAn" });
            DropIndex("dbo.ChatNhomApps", new[] { "IdUser2" });
            DropIndex("dbo.ChatNhomApps", new[] { "IdUser1" });
            DropIndex("dbo.ChatApps", new[] { "IdUser2" });
            DropIndex("dbo.ChatApps", new[] { "IdUser1" });
            DropIndex("dbo.CamKetGiaiNgans", new[] { "IdDuAn" });
            DropIndex("dbo.ThuMucs", new[] { "IdThuMucMacDinh" });
            DropIndex("dbo.VanBanDuAns", new[] { "IdDuAn" });
            DropIndex("dbo.VanBanDuAns", new[] { "IdThuMuc" });
            DropIndex("dbo.TinhTrangDuAns", new[] { "IdTinhTrangDuAnCha" });
            DropIndex("dbo.QLTD_KeHoachTienDoChung", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_CongViec", new[] { "IdGiaiDoan" });
            DropIndex("dbo.QLTD_KeHoachCongViec", new[] { "IdCongViec" });
            DropIndex("dbo.QLTD_KeHoachCongViec", new[] { "IdKeHoach" });
            DropIndex("dbo.QLTD_KeHoach", new[] { "CreatedBy" });
            DropIndex("dbo.QLTD_KeHoach", new[] { "IdGiaiDoan" });
            DropIndex("dbo.QLTD_KeHoach", new[] { "IdDuAn" });
            DropIndex("dbo.QLTD_KeHoach", new[] { "IdKeHoachDC" });
            DropIndex("dbo.QLTD_KeHoach", new[] { "IdNhomDuAnTheoUser" });
            DropIndex("dbo.LinhVucNganhNghes", new[] { "IdLienKetLinhVuc" });
            DropIndex("dbo.AppUserLogins", new[] { "AppUser_Id" });
            DropIndex("dbo.AppUserClaims", new[] { "AppUser_Id" });
            DropIndex("dbo.AppUsers", new[] { "IdNhomDuAnTheoUser" });
            DropIndex("dbo.DuAnUsers", new[] { "UserId" });
            DropIndex("dbo.DuAnUsers", new[] { "IdDuAn" });
            DropIndex("dbo.DuAnQuanLys", new[] { "IdQuanLyDA" });
            DropIndex("dbo.DuAnQuanLys", new[] { "IdDuAn" });
            DropIndex("dbo.NguonVonQuyetToanDuAns", new[] { "IdNguonVonDuAn" });
            DropIndex("dbo.NguonVonQuyetToanDuAns", new[] { "IdQuyetToanDuAn" });
            DropIndex("dbo.QuyetToanDuAns", new[] { "IdCoQuanPheDuyet" });
            DropIndex("dbo.QuyetToanDuAns", new[] { "IdDuAn" });
            DropIndex("dbo.HoSoMoiThaus", new[] { "IdGoiThau" });
            DropIndex("dbo.HoSoMoiThaus", new[] { "IdDuAn" });
            DropIndex("dbo.TienDoChiTiets", new[] { "IdTienDo" });
            DropIndex("dbo.TienDoThucHiens", new[] { "IdHopDong" });
            DropIndex("dbo.QuyetToanHopDongs", new[] { "IdHopDong" });
            DropIndex("dbo.PhuLucHopDongs", new[] { "IdHopDong" });
            DropIndex("dbo.KeHoachVons", new[] { "IdCoQuanPheDuyet" });
            DropIndex("dbo.KeHoachVons", new[] { "IdDuAn" });
            DropIndex("dbo.NguonVonKeHoachVons", new[] { "IdNguonVonDuAn" });
            DropIndex("dbo.NguonVonKeHoachVons", new[] { "IdKeHoachVon" });
            DropIndex("dbo.ThucHienHopDongs", new[] { "IdLoaiChiPhi" });
            DropIndex("dbo.ThucHienHopDongs", new[] { "IdHopDong" });
            DropIndex("dbo.ThucHienHopDongs", new[] { "IdDuAn" });
            DropIndex("dbo.ThanhToanHopDongs", new[] { "IdThucHien" });
            DropIndex("dbo.ThanhToanHopDongs", new[] { "IdHopDong" });
            DropIndex("dbo.NguonVonGoiThauThanhToans", new[] { "IdThanhToanHopDong" });
            DropIndex("dbo.NguonVonGoiThauThanhToans", new[] { "IdNguonVonDuAnGoiThau" });
            DropIndex("dbo.TamUngHopDongs", new[] { "IdHopDong" });
            DropIndex("dbo.NguonVonGoiThauTamUngs", new[] { "IdTamUng" });
            DropIndex("dbo.NguonVonGoiThauTamUngs", new[] { "IdNguonVonDuAnGoiThau" });
            DropIndex("dbo.NguonVonDuAnGoiThaus", new[] { "IdNguonVonDuAn" });
            DropIndex("dbo.NguonVonDuAnGoiThaus", new[] { "IdGoiThau" });
            DropIndex("dbo.NguonVons", new[] { "IdNguonVonCha" });
            DropIndex("dbo.ThamDinhLapQuanLyDauTus", new[] { "IdLapQuanLyDauTu" });
            DropIndex("dbo.LapQuanLyDauTus", new[] { "IdCoQuanPheDuyet" });
            DropIndex("dbo.LapQuanLyDauTus", new[] { "IdDuAn" });
            DropIndex("dbo.NguonVonDuAns", new[] { "IdLapQuanLyDauTu" });
            DropIndex("dbo.NguonVonDuAns", new[] { "IdNguonVon" });
            DropIndex("dbo.NguonVonDuAns", new[] { "IdDuAn" });
            DropIndex("dbo.NguonVonThanhToanCPKs", new[] { "IdNguonVonDuAn" });
            DropIndex("dbo.NguonVonThanhToanCPKs", new[] { "IdThanhToanChiPhiKhac" });
            DropIndex("dbo.ThanhToanChiPhiKhacs", new[] { "IdLoaiChiPhi" });
            DropIndex("dbo.ThanhToanChiPhiKhacs", new[] { "IdNhaThau" });
            DropIndex("dbo.ThanhToanChiPhiKhacs", new[] { "IdDuAn" });
            DropIndex("dbo.KetQuaDauThaus", new[] { "IdNhaThau" });
            DropIndex("dbo.KetQuaDauThaus", new[] { "IdGoiThau" });
            DropIndex("dbo.DanhMucNhaThaus", new[] { "IdDuAn" });
            DropIndex("dbo.HopDongs", new[] { "IdNhaThau" });
            DropIndex("dbo.HopDongs", new[] { "IdLoaiHopDong" });
            DropIndex("dbo.HopDongs", new[] { "IdGoiThau" });
            DropIndex("dbo.HopDongs", new[] { "IdDuAn" });
            DropIndex("dbo.GoiThaus", new[] { "IdLoaiHopDong" });
            DropIndex("dbo.GoiThaus", new[] { "IdPhuongThucDauThau" });
            DropIndex("dbo.GoiThaus", new[] { "IdHinhThucLuaChon" });
            DropIndex("dbo.GoiThaus", new[] { "IdLinhVucDauThau" });
            DropIndex("dbo.GoiThaus", new[] { "IdKeHoachLuaChonNhaThau" });
            DropIndex("dbo.GoiThaus", new[] { "IdDuAn" });
            DropIndex("dbo.KeHoachLuaChonNhaThaus", new[] { "IdCoQuanPheDuyet" });
            DropIndex("dbo.KeHoachLuaChonNhaThaus", new[] { "IdDieuChinhKeHoachLuaChonNhaThau" });
            DropIndex("dbo.KeHoachLuaChonNhaThaus", new[] { "IdDuAn" });
            DropIndex("dbo.ChuTruongDauTus", new[] { "IdCoQuanPheDuyet" });
            DropIndex("dbo.ChuTruongDauTus", new[] { "IdDuAn" });
            DropIndex("dbo.DuAns", new[] { "IdTinhTrangDuAn" });
            DropIndex("dbo.DuAns", new[] { "IdHinhThucQuanLy" });
            DropIndex("dbo.DuAns", new[] { "IdGiaiDoanDuAn" });
            DropIndex("dbo.DuAns", new[] { "IdHinhThucDauTu" });
            DropIndex("dbo.DuAns", new[] { "IdLoaiCapCongTrinh" });
            DropIndex("dbo.DuAns", new[] { "IdNhomDuAn" });
            DropIndex("dbo.DuAns", new[] { "IdLinhVucNganhNghe" });
            DropIndex("dbo.DuAns", new[] { "IdChuDauTu" });
            DropIndex("dbo.DuAns", new[] { "IdNhomDuAnTheoUser" });
            DropIndex("dbo.BaoLanhTHHDs", new[] { "IdHopDong" });
            DropIndex("dbo.BaoLanhTHHDs", new[] { "IdDuAn" });
            DropIndex("dbo.AppUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.AppUserRoles", new[] { "AppUser_Id" });
            DropIndex("dbo.AppRoles", new[] { "AppUser_Id" });
            DropTable("dbo.VonCapNhatKLNTs");
            DropTable("dbo.TrangThaiChats");
            DropTable("dbo.VanBanDenUsers");
            DropTable("dbo.VanBanDenThucHiens");
            DropTable("dbo.VanBanDens");
            DropTable("dbo.ThuMucVanBanDens");
            DropTable("dbo.SystemLogs");
            DropTable("dbo.QLTD_KhoKhanVuongMac");
            DropTable("dbo.QLTD_THCT_ThucHienDuAn");
            DropTable("dbo.QLTD_TDTH_ThucHienDuAn");
            DropTable("dbo.QLTD_CTCV_ThucHienDuAn");
            DropTable("dbo.QLTD_THCT_QuyetToanDuAn");
            DropTable("dbo.QLTD_TDTH_QuyetToanDuAn");
            DropTable("dbo.QLTD_CTCV_QuyetToanDuAn");
            DropTable("dbo.QLTD_THCT_ChuTruongDauTu");
            DropTable("dbo.QLTD_TDTH_ChuTruongDauTu");
            DropTable("dbo.QLTD_CTCV_ChuTruongDauTu");
            DropTable("dbo.QLTD_THCT_ChuanBiThucHien");
            DropTable("dbo.QLTD_TDTH_ChuanBiThucHien");
            DropTable("dbo.QLTD_CTCV_ChuanBiThucHien");
            DropTable("dbo.QLTD_THCT_ChuanBiDauTu");
            DropTable("dbo.QLTD_TDTH_ChuanBiDauTu");
            DropTable("dbo.QLTD_CTCV_ChuanBiDauTu");
            DropTable("dbo.PhongCongTacs");
            DropTable("dbo.PhieuXuLyCongViecNoiBoUsers");
            DropTable("dbo.PhieuXuLyCongViecNoiBos");
            DropTable("dbo.Permissions");
            DropTable("dbo.Notifications");
            DropTable("dbo.Notification_User");
            DropTable("dbo.Notification_Group");
            DropTable("dbo.Notification_Group_Token");
            DropTable("dbo.HinhThucDauThaus");
            DropTable("dbo.GPMP_File_Comments");
            DropTable("dbo.GPMB_KhoKhanVuongMac");
            DropTable("dbo.GPMB_KetQua_ChiTiet");
            DropTable("dbo.GPMB_TongQuanKetQua");
            DropTable("dbo.GPMB_KetQua");
            DropTable("dbo.GPMB_KeHoachTienDoChung");
            DropTable("dbo.GPMB_File");
            DropTable("dbo.GPMB_DangKyKeHoach");
            DropTable("dbo.GPMB_CTCV_Dots");
            DropTable("dbo.GPMB_KeHoach");
            DropTable("dbo.GPMB_KeHoachCongViec");
            DropTable("dbo.GPMB_CTCV");
            DropTable("dbo.GPMB_CongViec");
            DropTable("dbo.GPMB_Comment");
            DropTable("dbo.GiaTriThanhToanTheoNams");
            DropTable("dbo.Functions");
            DropTable("dbo.Errors");
            DropTable("dbo.DuAnHoSoQuyetToans");
            DropTable("dbo.Device_Token");
            DropTable("dbo.DanhMucHoSoQuyetToans");
            DropTable("dbo.Comments");
            DropTable("dbo.NhomChatApps");
            DropTable("dbo.ChatNhomApps");
            DropTable("dbo.ChatApps");
            DropTable("dbo.CapNhatKLNTs");
            DropTable("dbo.CamKetGiaiNgans");
            DropTable("dbo.ThuMucMacDinhs");
            DropTable("dbo.ThuMucs");
            DropTable("dbo.VanBanDuAns");
            DropTable("dbo.TinhTrangDuAns");
            DropTable("dbo.QLTD_KeHoachTienDoChung");
            DropTable("dbo.QLTD_CongViec");
            DropTable("dbo.QLTD_KeHoachCongViec");
            DropTable("dbo.QLTD_GiaiDoan");
            DropTable("dbo.QLTD_KeHoach");
            DropTable("dbo.NhomDuAns");
            DropTable("dbo.LoaiCapCongTrinhs");
            DropTable("dbo.LienKetLinhVucs");
            DropTable("dbo.LinhVucNganhNghes");
            DropTable("dbo.HinhThucQuanLys");
            DropTable("dbo.HinhThucDauTus");
            DropTable("dbo.GiaiDoanDuAns");
            DropTable("dbo.NhomDuAnTheoUsers");
            DropTable("dbo.AppUserLogins");
            DropTable("dbo.AppUserClaims");
            DropTable("dbo.AppUsers");
            DropTable("dbo.DuAnUsers");
            DropTable("dbo.QuanLyDAs");
            DropTable("dbo.DuAnQuanLys");
            DropTable("dbo.NguonVonQuyetToanDuAns");
            DropTable("dbo.QuyetToanDuAns");
            DropTable("dbo.PhuongThucDauThaus");
            DropTable("dbo.LinhVucDauThaus");
            DropTable("dbo.HoSoMoiThaus");
            DropTable("dbo.TienDoChiTiets");
            DropTable("dbo.TienDoThucHiens");
            DropTable("dbo.QuyetToanHopDongs");
            DropTable("dbo.PhuLucHopDongs");
            DropTable("dbo.LoaiHopDongs");
            DropTable("dbo.KeHoachVons");
            DropTable("dbo.NguonVonKeHoachVons");
            DropTable("dbo.ThucHienHopDongs");
            DropTable("dbo.ThanhToanHopDongs");
            DropTable("dbo.NguonVonGoiThauThanhToans");
            DropTable("dbo.TamUngHopDongs");
            DropTable("dbo.NguonVonGoiThauTamUngs");
            DropTable("dbo.NguonVonDuAnGoiThaus");
            DropTable("dbo.NguonVons");
            DropTable("dbo.ThamDinhLapQuanLyDauTus");
            DropTable("dbo.LapQuanLyDauTus");
            DropTable("dbo.NguonVonDuAns");
            DropTable("dbo.NguonVonThanhToanCPKs");
            DropTable("dbo.LoaiChiPhis");
            DropTable("dbo.ThanhToanChiPhiKhacs");
            DropTable("dbo.KetQuaDauThaus");
            DropTable("dbo.DanhMucNhaThaus");
            DropTable("dbo.HopDongs");
            DropTable("dbo.HinhThucLuaChonNhaThaus");
            DropTable("dbo.GoiThaus");
            DropTable("dbo.KeHoachLuaChonNhaThaus");
            DropTable("dbo.CoQuanPheDuyets");
            DropTable("dbo.ChuTruongDauTus");
            DropTable("dbo.ChuDauTus");
            DropTable("dbo.DuAns");
            DropTable("dbo.BaoLanhTHHDs");
            DropTable("dbo.BaoCaoTables");
            DropTable("dbo.AppUserRoles");
            DropTable("dbo.AppRoles");
            DropTable("dbo.ActionHistories");
        }
    }
}
