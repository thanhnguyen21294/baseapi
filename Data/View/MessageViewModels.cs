﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebAPI.Data.View.Mobile
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public string IdUser1 { get; set; }
        public string IdUser2 { get; set; }
        public string NoiDung { get; set; }
        public bool IsFile { set; get; }
        public DateTime? NgayTao { set; get; }
        public UserChatViewModel AppUser1 { get; set; }
        public UserChatViewModel AppUser2 { get; set; }

        public MessageViewModel(int id, string idUser1, string idUser2, string noiDung, bool isFile, DateTime? ngayTao, UserChatViewModel appUser1, UserChatViewModel appUser2)
        {
            Id = id;
            IdUser1 = idUser1;
            IdUser2 = idUser2;
            NoiDung = noiDung;
            NgayTao = ngayTao;
            AppUser1 = appUser1;
            AppUser2 = appUser2;
            IsFile = isFile;
        }
    }

    public class UserChatViewModel
    {
        public string IdUser { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }

        public UserChatViewModel(string idUser, string avatar, string fullName)
        {
            IdUser = idUser;
            Avatar = avatar;
            FullName = fullName;
        }
    }
}