﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ITienDoChiTietRepository : IRepository<TienDoChiTiet>
    {
    }

    public class TienDoChiTietRepository : RepositoryBase<TienDoChiTiet>, ITienDoChiTietRepository
    {
        public TienDoChiTietRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
