﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface ITienDoThucHienRepository : IRepository<TienDoThucHien>
    {
        IEnumerable<HopDong> GetThucHienHopDongs(int idDuAn, string filter);
    }

    public class TienDoThucHienRepository : RepositoryBase<TienDoThucHien>, ITienDoThucHienRepository
    {
        public TienDoThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
        public IEnumerable<HopDong> GetThucHienHopDongs(int idDuAn, string filter)
        {
            IEnumerable<HopDong> query;
            if (filter == null)
            {
                query = from hopdong in DbContext.HopDongs.Include("TienDoThucHiens")
                        where hopdong.TienDoThucHiens.Count > 0 && hopdong.IdDuAn == idDuAn
                        select hopdong;
            }
            else
            {
                query = from hopdong in DbContext.HopDongs.IncludeFilter(x =>
                        x.TienDoThucHiens.Where(y => y.TenCongViec.Contains(filter)))
                        where hopdong.TienDoThucHiens.Count > 0 &&
                              hopdong.IdDuAn == idDuAn &&
                              hopdong.TienDoThucHiens.Any(a => a.TenCongViec.Contains(filter))
                        select hopdong;
            }
            return query;
        }
    }
}
