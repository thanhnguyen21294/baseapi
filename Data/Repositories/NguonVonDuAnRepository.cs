﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonDuAnRepository : IRepository<NguonVonDuAn>
    {
    }

    public class NguonVonDuAnRepository : RepositoryBase<NguonVonDuAn>, INguonVonDuAnRepository
    {
        public NguonVonDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
