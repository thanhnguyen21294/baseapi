﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IThuMucRepository:IRepository<ThuMuc>    
    {
       IEnumerable<ThuMuc> GetTMsByDA(int idDA);
       void AddThuMucWhenDuAnCreated(int idDA);
    }
    public class ThuMucRepository : RepositoryBase<ThuMuc>, IThuMucRepository
    {
        public ThuMucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       

        public IEnumerable<ThuMuc> GetTMsByDA(int idDA)
        {
         
          IEnumerable<ThuMuc> lstTMByDA=  DbContext.ThuMucs.Where(x=>x.IdDuAn==idDA);
         
            return lstTMByDA;
        }
        public void AddThuMucWhenDuAnCreated(int idDA)
        {
            List<ThuMucMacDinh> tmmds = DbContext.ThuMucMacDinhs.ToList();
      
            var query = tmmds.Where(x => x.IdThuMucMacDinhCha == null);
           
            foreach(var item in query)
            {
                Add(item, tmmds, idDA, null);
            }
        }
        public void Add(ThuMucMacDinh tmmd,List<ThuMucMacDinh> tmmds,int idDA,int ?idTMCha)
        {
            ThuMuc tm = new ThuMuc();
            tm.IdDuAn = idDA;
            tm.IdThuMucMacDinh = tmmd.IdThuMucMacDinh;
            tm.IdThuMucCha = idTMCha;
            tm.TenThuMuc = tmmd.TenThuMuc;
            tm.Status = true;
            Add(tm);
            DbContext.SaveChanges();
            var lstTMMDChild = tmmds.Where(x => x.IdThuMucMacDinhCha == tmmd.IdThuMucMacDinh).ToList();
         
            foreach(var item in lstTMMDChild)
            {
                Add(item, tmmds, idDA, tm.IdThuMuc);
            }
            
        }

       
    }
}
