﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IHinhThucLuaChonNhaThauRepository: IRepository<HinhThucLuaChonNhaThau>
    {
    }
    public class HinhThucLuaChonNhaThauRepository : RepositoryBase<HinhThucLuaChonNhaThau>, IHinhThucLuaChonNhaThauRepository
    {
        public HinhThucLuaChonNhaThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
