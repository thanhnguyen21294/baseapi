﻿using Data.Infrastructure;
using Model.Models;
using System.Collections.Generic;
using Z.EntityFramework.Plus;
using System.Linq;
using System.Data.Entity;

namespace Data.Repositories
{
    public interface IPhieuXuLyCongViecNoiBoRepository : IRepository<PhieuXuLyCongViecNoiBo>
    {
        IEnumerable<PhieuXuLyCongViecNoiBo> GetListTree();
    }

    public class PhieuXuLyCongViecNoiBoRepository : RepositoryBase<PhieuXuLyCongViecNoiBo>, IPhieuXuLyCongViecNoiBoRepository
    {
        public PhieuXuLyCongViecNoiBoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<PhieuXuLyCongViecNoiBo> GetListTree()
        {

            var query = (from f in DbContext.PhieuXuLyCongViecNoiBos where f.IDPhieuCha == null select f)
                .Include("DuAn")
                .Include("PhieuXuLyCongViecNoiBoUsers")
                .IncludeOptimized(x => x.Childrens.Where(z => z.TrangThai == 0))
                .IncludeOptimized(x => x.Childrens.Select(y => y.Childrens));
            return query;
        }
    }
}
