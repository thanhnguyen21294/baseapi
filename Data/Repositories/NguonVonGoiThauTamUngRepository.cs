﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonGoiThauTamUngRepository : IRepository<NguonVonGoiThauTamUng>
    {
    }
    public class NguonVonGoiThauTamUngRepository : RepositoryBase<NguonVonGoiThauTamUng>, INguonVonGoiThauTamUngRepository
    {
        public NguonVonGoiThauTamUngRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
