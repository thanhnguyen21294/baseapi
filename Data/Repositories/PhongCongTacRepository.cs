﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IPhongCongTacRepository : IRepository<PhongCongTac>
    {
        IEnumerable<PhongCongTac> GetPhongCongTac();
    }
    public class PhongCongTacRepository : RepositoryBase<PhongCongTac>, IPhongCongTacRepository
    {
        public PhongCongTacRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<PhongCongTac> GetPhongCongTac()
        {
            var query = from kh in DbContext.PhongCongTacs
                        select kh;
            return query;
        }
    }
}
