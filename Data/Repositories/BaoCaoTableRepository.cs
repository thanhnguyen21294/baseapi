﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IBaoCaoTableRepository : IRepository<BaoCaoTable>
    {
    }
    public class BaoCaoTableRepository : RepositoryBase<BaoCaoTable>, IBaoCaoTableRepository
    {
        public BaoCaoTableRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
