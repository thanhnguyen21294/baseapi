﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_CommentRepository : IRepository<GPMB_Comment>
    {
    }

    public class GPMB_CommentRepository : RepositoryBase<GPMB_Comment>, IGPMB_CommentRepository
    {
        public GPMB_CommentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }

}
