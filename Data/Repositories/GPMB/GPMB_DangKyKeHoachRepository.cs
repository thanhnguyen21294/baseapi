﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_DangKyKeHoachRepository : IRepository<GPMB_DangKyKeHoach>
    {
        IEnumerable<GPMB_DangKyKeHoach> GetGPMB_DangKyKeHoach();
    }
    public class GPMB_DangKyKeHoachRepository : RepositoryBase<GPMB_DangKyKeHoach>, IGPMB_DangKyKeHoachRepository
    {
        public GPMB_DangKyKeHoachRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_DangKyKeHoach> GetGPMB_DangKyKeHoach()
        {
            var query = from kh in DbContext.GPMB_DangKyKeHoach
                        select kh;
            return query;
        }
    }
}
