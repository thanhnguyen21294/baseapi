﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_CTCV_DotRepository : IRepository<GPMB_CTCV_Dot>
    {
        IEnumerable<GPMB_CTCV_Dot> GetGPMB_CTCV_Dot();
    }

    public class GPMB_CTCV_DotRepository : RepositoryBase<GPMB_CTCV_Dot>, IGPMB_CTCV_DotRepository
    {
        public GPMB_CTCV_DotRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_CTCV_Dot> GetGPMB_CTCV_Dot()
        {
            var query = from kh in DbContext.GPMB_CTCV_Dots
                        select kh;
            return query;
        }
    }
}
