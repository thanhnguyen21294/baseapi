﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_CongViecRepository : IRepository<GPMB_CongViec>
    {
        IEnumerable<GPMB_CongViec> GetGPMB_CongViec();
    }

    public class GPMB_CongViecRepository : RepositoryBase<GPMB_CongViec>, IGPMB_CongViecRepository
    {
        public GPMB_CongViecRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_CongViec> GetGPMB_CongViec()
        {
            var query = from kh in DbContext.GPMB_CongViec
                        select kh;
            return query;
        }
    }
}
