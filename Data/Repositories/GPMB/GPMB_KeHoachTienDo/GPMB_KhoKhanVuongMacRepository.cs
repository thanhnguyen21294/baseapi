﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_KhoKhanVuongMacRepository : IRepository<GPMB_KhoKhanVuongMac>
    {
        IEnumerable<GPMB_KhoKhanVuongMac> GetGPMB_KhoKhanVuongMac();
    }

    public class GPMB_KhoKhanVuongMacRepository : RepositoryBase<GPMB_KhoKhanVuongMac>, IGPMB_KhoKhanVuongMacRepository
    {
        public GPMB_KhoKhanVuongMacRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KhoKhanVuongMac> GetGPMB_KhoKhanVuongMac()
        {
            var query = from kh in DbContext.GPMB_KhoKhanVuongMac
                        select kh;
            return query;
        }
    }
}
