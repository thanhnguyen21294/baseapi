﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_CTCVRepository : IRepository<GPMB_CTCV>
    {
        IEnumerable<GPMB_CTCV> GetGPMB_CTCV();
    }

    public class GPMB_CTCVRepository : RepositoryBase<GPMB_CTCV>, IGPMB_CTCVRepository
    {
        public GPMB_CTCVRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_CTCV> GetGPMB_CTCV()
        {
            var query = from kh in DbContext.GPMB_CTCV
                        select kh;
            return query;
        }
    }
}
