﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_KeHoachCongViecRepository : IRepository<GPMB_KeHoachCongViec>
    {
        IEnumerable<GPMB_KeHoachCongViec> GetGPMB_KeHoachCongViec();
    }

    public class GPMB_KeHoachCongViecRepository : RepositoryBase<GPMB_KeHoachCongViec>, IGPMB_KeHoachCongViecRepository
    {
        public GPMB_KeHoachCongViecRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KeHoachCongViec> GetGPMB_KeHoachCongViec()
        {
            var query = from kh in DbContext.GPMB_KeHoachCongViec.Include("GPMB_CongViec")
                        select kh;
            return query;
        }
    }
}
