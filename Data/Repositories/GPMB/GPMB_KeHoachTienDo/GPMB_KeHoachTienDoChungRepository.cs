﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_KeHoachTienDoChungRepository : IRepository<GPMB_KeHoachTienDoChung>
    {
        IEnumerable<GPMB_KeHoachTienDoChung> GetGPMB_KeHoachTienDoChung();
    }

    public class GPMB_KeHoachTienDoChungRepository : RepositoryBase<GPMB_KeHoachTienDoChung>, IGPMB_KeHoachTienDoChungRepository
    {
        public GPMB_KeHoachTienDoChungRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KeHoachTienDoChung> GetGPMB_KeHoachTienDoChung()
        {
            var query = from kh in DbContext.GPMB_KeHoachTienDoChung
                        select kh;
            return query;
        }
    }
}
