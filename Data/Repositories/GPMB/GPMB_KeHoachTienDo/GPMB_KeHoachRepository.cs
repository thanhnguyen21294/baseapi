﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB.GPMB_KeHoachTienDo
{
    public interface IGPMB_KeHoachRepository : IRepository<GPMB_KeHoach>
    {
        IEnumerable<GPMB_KeHoach> GetGPMB_KeHoach();
    }

    public class GPMB_KeHoachRepository : RepositoryBase<GPMB_KeHoach>, IGPMB_KeHoachRepository
    {
        public GPMB_KeHoachRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KeHoach> GetGPMB_KeHoach()
        {
            var query = from kh in DbContext.GPMB_KeHoach.Include("AppUsers")
                        select kh;
            return query;
        }
    }
}
