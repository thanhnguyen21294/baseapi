﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_TongQuanKetQuaRepository : IRepository<GPMB_TongQuanKetQua>
    {
        IEnumerable<GPMB_TongQuanKetQua> GetGPMB_TongQuanKetQua();
    }
    public class GPMB_TongQuanKetQuaRepository : RepositoryBase<GPMB_TongQuanKetQua>, IGPMB_TongQuanKetQuaRepository
    {
        public GPMB_TongQuanKetQuaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_TongQuanKetQua> GetGPMB_TongQuanKetQua()
        {
            var query = from kh in DbContext.GPMB_TongQuanKetQua
                        select kh;
            return query;
        }
    }
}
