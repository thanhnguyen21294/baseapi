﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_FileRepository : IRepository<GPMB_File>
    {
    }

    public class GPMB_FileRepository : RepositoryBase<GPMB_File>, IGPMB_FileRepository
    {
        public GPMB_FileRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
