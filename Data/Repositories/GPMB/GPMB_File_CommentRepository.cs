﻿using Data.Infrastructure;
using Model.Models.QLTD.GPMB;

namespace Data.Repositories.GPMB
{

    public interface IGPMB_File_CommentRepository : IRepository<GPMP_File_Comment>
    {
    }
    public class GPMB_File_CommentRepository : RepositoryBase<GPMP_File_Comment>, IGPMB_File_CommentRepository
    {
        public GPMB_File_CommentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
