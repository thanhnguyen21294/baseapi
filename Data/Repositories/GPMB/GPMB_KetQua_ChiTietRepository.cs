﻿using Data.Infrastructure;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_KetQua_ChiTietRepository : IRepository<GPMB_KetQua_ChiTiet>
    {
        IEnumerable<GPMB_KetQua_ChiTiet> GetGPMB_KetQua_ChiTiet();
    }

    public class GPMB_KetQua_ChiTietRepository : RepositoryBase<GPMB_KetQua_ChiTiet>, IGPMB_KetQua_ChiTietRepository
    {
        public GPMB_KetQua_ChiTietRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KetQua_ChiTiet> GetGPMB_KetQua_ChiTiet()
        {
            var query = from kh in DbContext.GPMB_KetQua_ChiTiet.Include("AppUsers")
                        select kh;
            return query;
        }
    }
}
