﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.GPMB
{
    public interface IGPMB_KetQuaRepository : IRepository<GPMB_KetQua>
    {
        IEnumerable<GPMB_KetQua> GetGPMB_KetQua();
    }
    public class GPMB_KetQuaRepository : RepositoryBase<GPMB_KetQua>, IGPMB_KetQuaRepository
    {
        public GPMB_KetQuaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GPMB_KetQua> GetGPMB_KetQua()
        {
            var query = from kh in DbContext.GPMB_KetQua
                        select kh;
            return query;
        }
    }
}
