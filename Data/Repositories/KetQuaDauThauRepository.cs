﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IKetQuaDauThauRepository : IRepository<KetQuaDauThau>
    {
        IEnumerable<GoiThau> GetKetQuaByGoiThaus(int idDuAn, string filter);
    }

    public class KetQuaDauThauRepository : RepositoryBase<KetQuaDauThau>, IKetQuaDauThauRepository
    {
        public KetQuaDauThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GoiThau> GetKetQuaByGoiThaus(int idDuAn, string filter)
        {
            IEnumerable<GoiThau> query;
            if (filter == null)
            {
                query = from goiThau in DbContext.GoiThaus.Include("KetQuaDauThaus").Include("KetQuaDauThaus.DanhMucNhaThau")
                    where goiThau.KetQuaDauThaus.Count > 0 && goiThau.IdDuAn == idDuAn
                    select goiThau;
            }
            else
            {
                query = from goiThau in DbContext.GoiThaus.IncludeFilter(x =>
                        x.KetQuaDauThaus.Where(y => y.NoiDung.Contains(filter))).Include("KetQuaDauThaus.DanhMucNhaThau")
                    where goiThau.KetQuaDauThaus.Count > 0 &&
                          goiThau.IdDuAn == idDuAn &&
                          goiThau.KetQuaDauThaus.Any(a => a.NoiDung.Contains(filter))
                    select goiThau;
            }
            return query;
        }
    }
}
