﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IPhuLucHopDongRepository : IRepository<PhuLucHopDong>
    {
    }

    public class PhuLucHopDongRepository : RepositoryBase<PhuLucHopDong>, IPhuLucHopDongRepository
    {
        public PhuLucHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
