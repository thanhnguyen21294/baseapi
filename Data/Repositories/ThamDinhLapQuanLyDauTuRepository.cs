﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public  interface IThamDinhLapQuanLyDauTuRepository: IRepository<ThamDinhLapQuanLyDauTu>
    {
        IEnumerable<ThamDinhLapQuanLyDauTu> GetByIdLapQuanLyDauTu(int id);
    }
   public class ThamDinhLapQuanLyDauTuRepository : RepositoryBase<ThamDinhLapQuanLyDauTu>,IThamDinhLapQuanLyDauTuRepository
    {
        public ThamDinhLapQuanLyDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ThamDinhLapQuanLyDauTu> GetByIdLapQuanLyDauTu(int id)
        {
          IEnumerable<ThamDinhLapQuanLyDauTu> lst =  DbContext.ThamDinhLapQuanLyDauTus.Where(x => x.IdLapQuanLyDauTu == id);
            return lst;
        }
    }
}
