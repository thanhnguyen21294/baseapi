﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IGoiThauRepository : IRepository<GoiThau>
    {
    }

    public class GoiThauRepository : RepositoryBase<GoiThau>, IGoiThauRepository
    {
        public GoiThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
