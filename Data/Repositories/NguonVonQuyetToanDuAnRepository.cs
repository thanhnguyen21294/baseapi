﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonQuyetToanDuAnRepository : IRepository<NguonVonQuyetToanDuAn>
    {
    }

    public class NguonVonQuyetToanDuAnRepository : RepositoryBase<NguonVonQuyetToanDuAn>, INguonVonQuyetToanDuAnRepository
    {
        public NguonVonQuyetToanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}

