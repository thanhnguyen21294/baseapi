﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IKeHoachVonRepository : IRepository<KeHoachVon>
    {
        IEnumerable<KeHoachVon> GetKeHoachVon();
    }
    public class KeHoachVonRepository : RepositoryBase<KeHoachVon>, IKeHoachVonRepository
    {
        public KeHoachVonRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<KeHoachVon> GetKeHoachVon()
        {
            var query = from kh in DbContext.KeHoachVons
                        select kh;
            return query;
        }
        public IEnumerable<KeHoachVon> GetKeHoachVonDieuChinhById(int idKeHoachVon)
        {
          return   DbContext.KeHoachVons.Where(x => x.IdKeHoachVonDieuChinh == idKeHoachVon);
        }
    }
}
