﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IThuMucMacDinhRepository : IRepository<ThuMucMacDinh>

    {
       IEnumerable<ThuMucMacDinh>   GetByFilter(string filter);

    }
    public class ThuMucMacDinhRepository : RepositoryBase<ThuMucMacDinh>, IThuMucMacDinhRepository
    {
        public ThuMucMacDinhRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<ThuMucMacDinh> GetByFilter(string filter)
        {
            IEnumerable<ThuMucMacDinh> tmmds = null;
            if(filter!=null)
            {
                tmmds= DbContext.ThuMucMacDinhs.Where(x => x.TenThuMuc.Contains(filter));
            }
            else
            {
                tmmds = DbContext.ThuMucMacDinhs;
            }
            return tmmds;
        }
    }
}
