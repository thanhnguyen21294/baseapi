﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;
using Model.Models.Common;

namespace Data.Repositories
{
    public interface IDuAnRepository : IRepository<DuAn>
    {
        IEnumerable<DuAn> GetAllKHV();
        IEnumerable<DuAn> GetAllTienDoDauThau();
        IEnumerable<DuAn> GetAllTienDoThucHienHopDong();
        IEnumerable<DuAn> GetAllDuAnCham();
        IEnumerable<DuAn> GetAllNguonVonDuAn();

        //phan quyen theo user
        IEnumerable<DuAnView> GetDuAnTheoUser(int? IdNhomDuAnTheoUser);
       

        //SignalR DuAn
        //IQueryable<DuAn> GetAllUnread(string userId);
    }
    public class DuAnRepository : RepositoryBase<DuAn>, IDuAnRepository
    {
        public DuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
        public IEnumerable<DuAn> GetAllKHV()
        {
            IEnumerable<DuAn> lst = DbContext.DuAns.Include("HopDongs.ThanhToanHopDongs.NguonVonGoiThauThanhToans")
                .Include("HopDongs.TamUngHopDongs.NguonVonGoiThauTamUngs");
                //chưa sửa load chậm do include NguonVonDuAns
                //.Include("NguonVonDuAns.NguonVon")
                //.Include("LapQuanLyDauTus")
                //.Include("KeHoachVons")
                //.Include("NhomDuAn");
           

            return lst;
        }
        public IEnumerable<DuAn> GetAllTienDoDauThau()
        {


            var lst = DbContext.DuAns

                .Include("GoiThaus.HopDongs")
                .Include("LapQuanLyDauTus")
                .Include("GoiThaus.KetQuaDauThaus")
                .Include("GoiThaus.HoSoMoiThaus")
                .Include("KeHoachLuaChonNhaThaus");

            return lst;
        }

        public IEnumerable<DuAn> GetAllTienDoThucHienHopDong()
        {
            var query = DbContext.DuAns.Include("HopDongs.TienDoThucHiens.TienDoChiTiets");
            return query;
        }
        public IEnumerable<DuAn> GetAllDuAnCham()
        {
            var lst = DbContext.DuAns.Include("QuyetToanDuAns")
                .Include("NhomDuAn");
            return lst;
        }
        public void Get(int idDuan)
        {

            var q1 = DbContext.KeHoachVons.Where(x => x.IdKeHoachVonDieuChinh != null && x.IdDuAn == idDuan).GroupBy(x => x.IdKeHoachVonDieuChinh).Select(x => new
            {
                IdKeHoachVonDieuChinh = x.Key,
                x.ToList().OrderByDescending(a => a.NgayPheDuyet).First().IdKeHoachVon,
            });

            var q2 = from khv1 in DbContext.KeHoachVons where !(from x in q1 select x.IdKeHoachVonDieuChinh).Contains(khv1.IdKeHoachVon)
                     select new
                     {
                         IdKeHoachVonDieuChinh = khv1.IdKeHoachVonDieuChinh,
                         IdKeHoachVon = khv1.IdKeHoachVon,
                     };
            var query = q1.Union(q2);



            var gt_nvs = from khvByDA in query
                         join nvKHV in DbContext.NguonVonKeHoachVons
                                 on khvByDA.IdKeHoachVon equals nvKHV.IdKeHoachVon

                         join nvDA in DbContext.NguonVonDuAns
                                on nvKHV.IdNguonVonDuAn equals nvDA.IdNguonVonDuAn
                         join nv in DbContext.NguonVons
                                on nvDA.IdNguonVon equals nv.IdNguonVon
                         select new
                         {
                             nvKHV.GiaTri,
                             nv.TenNguonVon,
                         };



            var query1 = DbContext.KeHoachVons.Include("NguonVonKeHoachVons.NguonVonDuAn.NguonVon").Where(x => x.IdDuAn == idDuan)
             .Select(x => new
             {
                 GiaTri_NguonVon = x.NguonVonKeHoachVons.Select(a => new
                 {
                     a.GiaTri,
                     a.NguonVonDuAn.NguonVon.TenNguonVon,
                 })


             });



        }
        public IEnumerable<DuAn> GetAllNguonVonDuAn()
        {
            return DbContext.DuAns.Include("NguonVonDuAns");
        }


        // phan quyen du an theo user
        public IEnumerable<DuAnView> GetDuAnTheoUser(int? IdNhomDuAnTheoUser)
        {

            List<DuAnView> listDuAnViews = new List<DuAnView>();
            IEnumerable<DuAn> duAn = new List<DuAn>();
            List<DuAn> duAns;

            if (IdNhomDuAnTheoUser != null)
            {
                var nhom = (from n in DbContext.NhomDuAnTheoUsers
                            where n.IdNhomDuAn == IdNhomDuAnTheoUser
                            select n).FirstOrDefault();

                duAn = from da in DbContext.DuAns
                        where da.IdNhomDuAnTheoUser == IdNhomDuAnTheoUser
                        select da;

                duAns = duAn.GroupBy(x => x.IdDuAn).Select(y => y.FirstOrDefault()).ToList();
            }
            else
            {
                duAns = (from da in DbContext.DuAns
                         select da).ToList();

            }

            foreach (var item in duAns)
            {
                listDuAnViews.Add(new DuAnView
                {
                    IdDuAn = item.IdDuAn,
                    TenDuAn = item.TenDuAn,
                    Ma = item.Ma,
                    NhomDuAn = item.NhomDuAn.TenNhomDuAn,
                    GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
                    ThoiGianThucHien = item.ThoiGianThucHien
                });
            }

            return listDuAnViews;
            
        }

        //public IQueryable<DuAn> GetAllUnread(string userId)
        //{

        //    var query = from x in DbContext.DuAns
        //                join y in DbContext.DuAnUsers
        //                on x.IdDuAn equals y.IdDuAn
        //                into xy
        //                from y in xy.DefaultIfEmpty()
        //                where ((y.HasRead == null || y.HasRead == false) && (y.UserId == null || y.UserId == userId))
        //                select x;

        //    return query;
        //}
    }
}
