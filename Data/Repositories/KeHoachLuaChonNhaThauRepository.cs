﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IKeHoachLuaChonNhaThauRepository : IRepository<KeHoachLuaChonNhaThau>
    {

    }
    public class KeHoachLuaChonNhaThauRepository : RepositoryBase<KeHoachLuaChonNhaThau>, IKeHoachLuaChonNhaThauRepository
    {
        public KeHoachLuaChonNhaThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
