﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IHinhThucDauTuRepository: IRepository<HinhThucDauTu>
    {
    }

    public class HinhThucDauTuRepository : RepositoryBase<HinhThucDauTu>, IHinhThucDauTuRepository
    {
        public HinhThucDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
