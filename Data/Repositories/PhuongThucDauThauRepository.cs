﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IPhuongThucDauThauRepository: IRepository<PhuongThucDauThau>
    {
    }

    public class PhuongThucDauThauRepository : RepositoryBase<PhuongThucDauThau>, IPhuongThucDauThauRepository
    {
        public PhuongThucDauThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
