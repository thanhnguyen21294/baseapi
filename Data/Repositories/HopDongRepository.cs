﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IHopDongRepository : IRepository<HopDong>
    {
        IEnumerable<GoiThau> GetByFilter(int idDuAn, string filter);
    }

    public class HopDongRepository : RepositoryBase<HopDong>, IHopDongRepository
    {
        public HopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GoiThau> GetByFilter(int idDuAn, string filter)
        {
            IEnumerable<GoiThau> query;
            if (filter != null)
            {
                query = from gt in DbContext.GoiThaus.IncludeFilter(x =>
                        x.HopDongs.Where(y => y.TenHopDong.Contains(filter)))
                    where gt.IdDuAn == idDuAn && gt.HopDongs.Count > 0 && gt.HopDongs.Any(a => a.TenHopDong.Contains(filter))
                    select gt;
            }
            else
            {
                query = from gt in DbContext.GoiThaus.Include("HopDongs")
                    where gt.IdDuAn == idDuAn && gt.HopDongs.Count > 0
                    select gt;
            }
            return query;
        }
    }
}
