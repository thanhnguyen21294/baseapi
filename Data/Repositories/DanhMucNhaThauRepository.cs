﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IDanhMucNhaThauRepository : IRepository<DanhMucNhaThau>
    {
    }

    public class DanhMucNhaThauRepository : RepositoryBase<DanhMucNhaThau>, IDanhMucNhaThauRepository
    {
        public DanhMucNhaThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
