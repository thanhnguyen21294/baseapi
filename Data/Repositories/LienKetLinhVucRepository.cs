﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface ILienKetLinhVucRepository : IRepository<LienKetLinhVuc>
    {

    }
    public class LienKetLinhVucRepository : RepositoryBase<LienKetLinhVuc>, ILienKetLinhVucRepository
    {
        public LienKetLinhVucRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
