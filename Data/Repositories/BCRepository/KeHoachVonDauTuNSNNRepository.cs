﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Z.EntityFramework.Plus;

namespace Data.Repositories.BCRepository
{
    public interface IKeHoachVonDauTuNSNNRepository : IRepository<KeHoachVonDauTuNSNNRepository>
    {
        List<object> GetThongTinDuAnCha(string IDDuAns);
        List<object> GetThongTinDuAnCon(string IDDuAns);
        
        List<object> GetKHVNNSNN(int iNam);
        List<object> GetKHVNDCNSNN(int iNam);

        List<object> GetLuyKeKeHoachVonNSNN(int iNam);
        List<object> GetLuyKeKeHoachVonDieuChinhNSNN(int iNam);

        Dictionary<int, List<object>> GetDicKLTH(int iNam);

        List<object> GetUocThanhToanChiPhiKhacs(int iNam);
        List<object> GetUocThanhToanHopDong(int iNam);

        Dictionary<int, double> GetDicKHGN(int iNam);

    }

    public class KeHoachVonDauTuNSNNRepository : RepositoryBase<KeHoachVonDauTuNSNNRepository>, IKeHoachVonDauTuNSNNRepository
    {
        public KeHoachVonDauTuNSNNRepository(IDbFactory dbFactory) : base(dbFactory)
        { }

        public List<object> GetThongTinDuAnCha(string IDDuAns)
        {
            List<object> lstDuAnCha;

            string[] arrDuAn = IDDuAns.Split(',');

            //Thông tin chung dự án cha
            var lDuAnCha = from itemDA in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                           where itemDA.IdTinhTrangDuAn != null && itemDA.IdDuAnCha == null
                           && arrDuAn.Contains(itemDA.IdDuAn.ToString())
                           select new
                           {
                               IDDA = itemDA.IdDuAn,
                               TenDuAn = itemDA.TenDuAn == null ? "" : itemDA.TenDuAn,
                               DiaDiemXayDung = itemDA.DiaDiem == null ? "" : itemDA.DiaDiem,
                               NangLucThietKe = itemDA.QuyMoNangLuc == null ? "" : itemDA.QuyMoNangLuc,
                               ThoiGianKhoiCongHoanThanh = itemDA.ThoiGianThucHien == null ? "" : itemDA.ThoiGianThucHien,
                               TinhTrangDuAn = itemDA.TinhTrangDuAn.TenTinhTrangDuAn,
                               TinhTrangDuAnID = itemDA.IdTinhTrangDuAn
                           };

            lstDuAnCha = (from item in lDuAnCha
                          group item by new { item.TinhTrangDuAnID, item.TinhTrangDuAn }
                         into item1
                          select new
                          {
                              IDTinhTrangDuAn = item1.Key.TinhTrangDuAnID,
                              TenTinhTrangDuAn = item1.Key.TinhTrangDuAn,
                              grpDuAnCha = item1.Select(x => new
                              {
                                  IDDA = x.IDDA,
                                  TenDuAn = x.TenDuAn,
                                  DiaDiemXayDung = x.DiaDiemXayDung,
                                  NangLucThietKe = x.NangLucThietKe,
                                  ThoiGianKhoiCongHoanThanh = x.ThoiGianKhoiCongHoanThanh
                              }).ToList<object>()
                          }).OrderBy(x => x.IDTinhTrangDuAn).ToList<object>();
            return lstDuAnCha;
        }

        public List<object> GetThongTinDuAnCon(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            //Thong tin chung du an con
            var lstDuAnCon = (from itemDA in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                              where itemDA.IdDuAnCha != null && arrDuAn.Contains(itemDA.IdDuAnCha.ToString())
                              select new
                              {
                                  IDDA = itemDA.IdDuAn,
                                  IDDACha = itemDA.IdDuAnCha,
                                  TenDuAn = itemDA.TenDuAn == null ? "" : itemDA.TenDuAn,
                                  DiaDiemXayDung = itemDA.DiaDiem == null ? "" : itemDA.DiaDiem,
                                  NangLucThietKe = itemDA.QuyMoNangLuc == null ? "" : itemDA.QuyMoNangLuc,
                                  ThoiGianKhoiCongHoanThanh = itemDA.ThoiGianThucHien == null ? "" : itemDA.ThoiGianThucHien,
                              }).ToList<object>();
            List<object> l1 = lstDuAnCon.ToList<object>();
            return lstDuAnCon;
        }

        public List<object> GetKHVNNSNN(int iNam)
        {
            List<object> KHV = (from item in DbContext.KeHoachVons
                                where item.IdKeHoachVonDieuChinh == null && item.NienDo == iNam
                                group item by new { item.IdDuAn, item.IdKeHoachVon, item.NienDo }
                                into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHV = item1.Key.IdKeHoachVon,
                                    NienDo = item1.Key.NienDo,
                                    GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Select(y => (double?)y.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Select(y => (double?)y.GiaTri)).FirstOrDefault().Sum()
                                }).ToList<object>();

            return KHV;
            
        }

        public List<object> GetKHVNDCNSNN(int iNam)
        {
            List<object> KHVDC = (from item in DbContext.KeHoachVons
                                  where item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam
                                  group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                                  into item1
                                  select new
                                  {
                                      IDDA = item1.Key.IdDuAn,
                                      IDKHVDC = item1.Key.IdKeHoachVonDieuChinh,
                                      NienDo = item1.Key.NienDo,
                                      GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Select(y => (double?)y.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Select(y => (double?)y.GiaTri)).FirstOrDefault().Sum()
                                  }).ToList<object>();

            return KHVDC;
        }
        
        public List<object> GetLuyKeKeHoachVonNSNN(int iNam)
        {
            var KHV = (from item in DbContext.KeHoachVons
                       where item.IdKeHoachVonDieuChinh == null && item.NienDo <= iNam
                       group item by new { item.IdDuAn, item.IdKeHoachVon, item.NienDo }
                     into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           IDKHV = item1.Key.IdKeHoachVon,
                           NienDo = item1.Key.NienDo,
                           LuyKeKHVDenN_1_Tinh = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           LuyKeKHVDenN_1_Huyen = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           LuyKeKHVDenN_1_Xa = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           LuyKeKHVDenN_1_NSTW = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.Ma == "NSTW") || (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "NSTW"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.Ma == "NSTW") || (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "NSTW"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           LuyKeKHVDenN_1TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           LuyKeKHVDenN_1_Khac = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách địa phương") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách tỉnh") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách huyện") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách địa phương") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách tỉnh") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách huyện") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                       }).ToList<object>();

            return KHV;
        }

        public List<object> GetLuyKeKeHoachVonDieuChinhNSNN(int iNam)
        {
            var KHVDC = (from item in DbContext.KeHoachVons
                         where item.IdKeHoachVonDieuChinh != null && item.NienDo <= iNam
                         group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                         into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             IDKHVDC = item1.Key.IdKeHoachVonDieuChinh,
                             NienDo = item1.Key.NienDo,
                             LuyKeKHVDenN_1_Tinh = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             LuyKeKHVDenN_1_Huyen = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             LuyKeKHVDenN_1_Xa = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             LuyKeKHVDenN_1_NSTW = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.Ma == "NSTW") || (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "NSTW"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.Ma == "NSTW") || (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "NSTW"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             LuyKeKHVDenN_1TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             LuyKeKHVDenN_1_Khac = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách địa phương") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách tỉnh") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách huyện") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách địa phương") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách tỉnh") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách huyện") && (y.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách xã"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                         }).ToList<object>();

            return KHVDC;

        }
        
        public Dictionary<int, List<object>> GetDicKLTH(int iNam)
        {
            Dictionary<int, List<object>> dicKLTH = new Dictionary<int, List<object>>();

            dicKLTH = (from item in
                           (from da in DbContext.DuAns.Include(a => a.HopDongs)
                            select new
                            {
                                IDDA = da.IdDuAn,
                                KhoiLuongThucHienDenHetNamN_1 = da.HopDongs.Where(x => x.ThucHienHopDongs.Any(xx => xx.ThoiDiemBaoCao.Value.Year <= iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year <= iNam - 1).Sum(yyy => (double?)yyy.KhoiLuong)) == null ? 0 : da.HopDongs.Where(x => x.ThucHienHopDongs.Any(xx => xx.ThoiDiemBaoCao.Value.Year <= iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year <= iNam - 1).Sum(yyy => (double?)yyy.KhoiLuong)),
                                KhoiLuongThucHienCaNamN_1 = da.HopDongs.Where(x => x.ThucHienHopDongs.Any(xx => xx.ThoiDiemBaoCao.Value.Year == iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam - 1).Sum(yyy => (double?)yyy.KhoiLuong)) == null ? 0 : da.HopDongs.Where(x => x.ThucHienHopDongs.Any(xx => xx.ThoiDiemBaoCao.Value.Year == iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam - 1).Sum(yyy => (double?)yyy.KhoiLuong)),
                            })
                       group item by new { item.IDDA }
                       into item1
                       select new
                       {
                           IDDA = item1.Key.IDDA,
                           grpKLTH = item1.Select(x => new
                           {
                               KLTHDenHetNam = x.KhoiLuongThucHienDenHetNamN_1,
                               KLTHCaNam = x.KhoiLuongThucHienCaNamN_1
                           }).ToList<object>()
                       }).ToDictionary(x => x.IDDA, x => x.grpKLTH);

            return dicKLTH;
        }

        public List<object> GetUocThanhToanChiPhiKhacs(int iNam)
        {
            List<object> lstThanhToanCPK;

            lstThanhToanCPK = (from item in
                                   (from ttcpk in DbContext.ThanhToanChiPhiKhacs.Include(a => a.NguonVonThanhToanChiPhiKhacs)
                                    where (ttcpk.NgayThanhToan.Value.Year <= iNam - 1 || (ttcpk.NgayThanhToan.Value.Year == iNam && ttcpk.NgayThanhToan.Value.Month == 1))
                                    select new
                                    {
                                        IDDA = ttcpk.IdDuAn,

                                        GiaTriNguonTinh = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonHuyen = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonXa = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonNSTW = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước"))).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonTPCP = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonKhac = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => ((x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(x => (double?)x.GiaTri)
                                    })
                               group item by new { item.IDDA }
                               into item1
                               select new
                               {
                                   IDDA = item1.Key.IDDA,

                                   GiaTriNguonTinh = item1.Sum(x => x.GiaTriNguonTinh),

                                   GiaTriNguonHuyen = item1.Sum(x => x.GiaTriNguonHuyen),

                                   GiaTriNguonXa = item1.Sum(x => x.GiaTriNguonXa),

                                   GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),

                                   GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),

                                   GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac)
                               }).ToList<object>();

            return lstThanhToanCPK;
        }

        public List<object> GetUocThanhToanHopDong(int iNam)
        {
            List<object> lstThanhToanHD;

            lstThanhToanHD = (from item in
                                  (from hd in DbContext.HopDongs.Include("ThanhToanHopDongs.NguonVonGoiThauThanhToans.NguonVonDuAnGoiThaus.NguonVonDuAns.NguonVons")
                                   select new
                                   {
                                       IDDA = hd.IdDuAn,

                                       GiaTriNguonTinh = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonHuyen = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách huyện") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonXa = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách xã") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonNSTW = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương") && (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách nhà nước"))).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonTPCP = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonKhac = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year <= iNam - 1) || (x.ThoiDiemThanhToan.Value.Year == iNam && x.ThoiDiemThanhToan.Value.Month == 1)) && (x.NguonVonGoiThauThanhToans.Any(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố")))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => ((y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách thành phố") || (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri))
                                   })
                              group item by new { item.IDDA }
                             into item1
                              select new
                              {
                                  IDDA = item1.Key.IDDA,
                                  GiaTriNguonTinh = item1.Sum(x => x.GiaTriNguonTinh),
                                  GiaTriNguonHuyen = item1.Sum(x => x.GiaTriNguonHuyen),
                                  GiaTriNguonXa = item1.Sum(x => x.GiaTriNguonXa),
                                  GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),
                                  GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),
                                  GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac)
                              }).ToList<object>();
            return lstThanhToanHD;
        }

        public Dictionary<int, double> GetDicKHGN(int iNam)
        {
            Dictionary<int, double> DicKHGNN_1;

            //Giải ngân = Thanh toán chi phí khác + Thanh toán hợp đồng

            //Thanh toán chi phí khác
            var lstThanhToanCPKsN_1 = from item in
                                    (from ttcpk in DbContext.ThanhToanChiPhiKhacs
                                     where ttcpk.NgayThanhToan.Value.Year == iNam - 1
                                     select new
                                     {
                                         IDDA = ttcpk.IdDuAn,
                                         GiaTriCPKN_1 = ttcpk.NguonVonThanhToanChiPhiKhacs.Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Sum(x => (double?)x.GiaTri)
                                     })
                                      group item by new { item.IDDA }
                                      into item1
                                      select new
                                      {
                                          IDDA = item1.Key.IDDA,
                                          GiaTriCPKN_1 = item1.Sum(x => x.GiaTriCPKN_1)
                                      };
            List<object> l1 = lstThanhToanCPKsN_1.ToList<object>();

            //Thanh toán hợp đồng
            var lstThanhToanHDN_1 = from da in DbContext.DuAns.Include(a => a.HopDongs)
                                    select new
                                    {
                                        IDDA = da.IdDuAn,
                                        GiaTriTTHDN_1 = da.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam - 1)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam - 1).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri))) == null ? 0 : da.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam - 1)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam - 1).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri)))
                                    };
            List<object> l2 = lstThanhToanHDN_1.ToList<object>();

            DicKHGNN_1 = (from itemTTCPK in lstThanhToanCPKsN_1
                          join itemTTHD in lstThanhToanHDN_1
                        on itemTTCPK.IDDA equals itemTTHD.IDDA
                        select new
                        {
                            IDDA = itemTTCPK.IDDA,
                            GiaTriGNN_2 = (double)itemTTCPK.GiaTriCPKN_1 + (double)itemTTHD.GiaTriTTHDN_1
                        }).ToDictionary(x => x.IDDA, x => x.GiaTriGNN_2);


            return DicKHGNN_1;
        }

        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }
    }
}


