﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Z.EntityFramework.Plus;
using System.Globalization;

namespace Data.Repositories.BCRepository
{
    public interface ITinhHinhThucHienODAVonXDCBRepository : IRepository<TinhHinhThucHienODAVonXDCB>
    {
        List<object> GetDuAnCha(string IDDuAns);
        List<object> GetDuAnCon(string IDDuAns);

        Dictionary<int, string> GetTMDTLDADTSoQDBanDau();
        Dictionary<int, string> GetTMDTLTDTSoQDBanDau();

        Dictionary<int, double> GetTMDTLDADTTongGiaTriBanDau();
        Dictionary<int, double> GetTMDTLTDTTongGiaTriBanDau();


        Dictionary<int, string> GetTMDTLDADTSoQDSuaDoi();
        Dictionary<int, string> GetTMDTLTDTSoQDSuaDoi();

        Dictionary<int, double> GetTMDTLDADTTongGiaTriSuaDoi();
        Dictionary<int, double> GetTMDTLTDTTongGiaTriSuaDoi();

        List<object> GetLuyKeVonThanhToanCPKs(int iNam);
        List<object> GetLuyKeVonThanhToanHD(int iNam);

        List<object> GetDicKHVN_ODA(int iNam);
        List<object> GetDicKHVNDC_ODA(int iNam);

        List<object> GetUocThanhToanCPKs(int iNam);
        List<object> GetUocThanhToanHD(int iNam);

        List<object> GetThanhToanCPKsNam(int iNam);
        List<object> GetThanhToanHDNam(int iNam);

    }

    public class TinhHinhThucHienODAVonXDCBRepository : RepositoryBase<TinhHinhThucHienODAVonXDCB>, ITinhHinhThucHienODAVonXDCBRepository
    {
        public TinhHinhThucHienODAVonXDCBRepository(IDbFactory dbFactory) : base(dbFactory)
        { }

        public List<object> GetDuAnCha(string IDDuAns)
        {
            List<object> lstDuAnCha;

            string[] arrDuAn = IDDuAns.Split(',');

            //Thông tin chung dự án cha
            var lDuAnCha = from itemDA in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                           where itemDA.IdLinhVucNganhNghe != null && itemDA.IdTinhTrangDuAn != null && itemDA.IdDuAnCha == null
                           && arrDuAn.Contains(itemDA.IdDuAn.ToString())
                           select new
                           {
                               IDDA = itemDA.IdDuAn,
                               TenDuAn = itemDA.TenDuAn == null ? "" : itemDA.TenDuAn,
                               TinhTrangDuAn = itemDA.TinhTrangDuAn.TenTinhTrangDuAn,
                               TinhTrangDuAnID = itemDA.IdTinhTrangDuAn,
                               LinhVucNganhNghe = itemDA.LinhVucNganhNghe.TenLinhVucNganhNghe,
                               LinhVucNganhNgheID = itemDA.IdLinhVucNganhNghe
                           };

            lstDuAnCha = (from item in lDuAnCha
                          group item by new { item.LinhVucNganhNgheID, item.LinhVucNganhNghe }
                          into item1
                          select new
                          {
                              IDLinhVucNganhNghe = item1.Key.LinhVucNganhNgheID,
                              LinhVucNganhNghe = item1.Key.LinhVucNganhNghe,
                              grpTinhTrangDuAn = item1.GroupBy(x => new { x.TinhTrangDuAnID, x.TinhTrangDuAn }).Select(x => new
                              {
                                  IDTinhTrangDuAn = x.Key.TinhTrangDuAnID,
                                  TinhTrangDuAn = x.Key.TinhTrangDuAn,
                                  grpDuAnCha = item1.Where(xx => xx.TinhTrangDuAnID == x.Key.TinhTrangDuAnID).Select(y => new
                                  {
                                      IDDA = y.IDDA,
                                      TenDuAn = y.TenDuAn,
                                  }).ToList<object>()
                              }).ToList<object>()
                          }).OrderByDescending(x => x.IDLinhVucNganhNghe).ToList<object>();

            return lstDuAnCha;
        }

        public List<object> GetDuAnCon(string IDDuAns)
        {

            string[] arrDuAn = IDDuAns.Split(',');

            //Thong tin chung du an con
            List<object> lstDuAnCon = (from itemDA in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                                       where itemDA.IdDuAnCha != null && arrDuAn.Contains(itemDA.IdDuAnCha.ToString())
                                       select new
                                       {
                                           IDDA = itemDA.IdDuAn,
                                           IDDACha = itemDA.IdDuAnCha,
                                           TenDuAn = itemDA.TenDuAn == null ? "" : itemDA.TenDuAn,
                                       }).ToList<object>();
            return lstDuAnCon;
        }

        public Dictionary<int, string> GetTMDTLDADTSoQDBanDau()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 1
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  SoQD = item.SoQuyetDinh,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             SoQD = item1.OrderBy(x => x.NgayPD).Select(x => x.SoQD).FirstOrDefault(),
                             NgayPD = item1.OrderBy(x => x.NgayPD).Select(x => x.NgayPD).FirstOrDefault()
                         }).ToList<object>();

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oDT in query)
            {
                string szSo_NgayPD = "";

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                string szSQD = (string)GetValueObject(oDT, "SoQD");
                string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                szSo_NgayPD = szSQD + "," + szNPD + ";";

                dicRe.Add(IDDA, szSo_NgayPD);
            }

            return dicRe;
        }

        public Dictionary<int, string> GetTMDTLTDTSoQDBanDau()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 2
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  SoQD = item.SoQuyetDinh,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             SoQD = item1.OrderBy(x => x.NgayPD).Select(x => x.SoQD).FirstOrDefault(),
                             NgayPD = item1.OrderBy(x => x.NgayPD).Select(x => x.NgayPD).FirstOrDefault()
                         }).ToList<object>();

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oDT in query)
            {
                string szSo_NgayPD = "";

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                string szSQD = (string)GetValueObject(oDT, "SoQD");
                string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                szSo_NgayPD = szSQD + "," + szNPD + ";";

                dicRe.Add(IDDA, szSo_NgayPD);
            }

            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTriBanDau()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 1
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  TongGiaTri = item.TongGiaTri == null ? 0 : item.TongGiaTri,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             TongGiaTri = item1.OrderBy(x => x.NgayPD).Select(x => x.TongGiaTri).FirstOrDefault(),

                         }).ToList<object>();

            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oDT in query)
            {
                double dTongGiaTri = 0;

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                double TongGiaTri = (double)GetValueObject(oDT, "TongGiaTri");

                dTongGiaTri = TongGiaTri;

                dicRe.Add(IDDA, dTongGiaTri);
            }

            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLTDTTongGiaTriBanDau()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 2
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  TongGiaTri = item.TongGiaTri == null ? 0 : item.TongGiaTri,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             TongGiaTri = item1.OrderBy(x => x.NgayPD).Select(x => x.TongGiaTri).FirstOrDefault(),

                         }).ToList<object>();

            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oDT in query)
            {
                double dTongGiaTri = 0;

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                double TongGiaTri = (double)GetValueObject(oDT, "TongGiaTri");

                dTongGiaTri = TongGiaTri;

                dicRe.Add(IDDA, dTongGiaTri);
            }

            return dicRe;
        }

        public Dictionary<int, string> GetTMDTLDADTSoQDSuaDoi()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 1
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  SoQD = item.SoQuyetDinh,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             SoQD = item1.OrderByDescending(x => x.NgayPD).Select(x => x.SoQD).FirstOrDefault(),
                             NgayPD = item1.OrderByDescending(x => x.NgayPD).Select(x => x.NgayPD).FirstOrDefault()
                         }).ToList<object>();

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oDT in query)
            {
                string szSo_NgayPD = "";

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                string szSQD = (string)GetValueObject(oDT, "SoQD");
                string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                szSo_NgayPD = szSQD + "," + szNPD + ";";

                dicRe.Add(IDDA, szSo_NgayPD);
            }

            return dicRe;
        }

        public Dictionary<int, string> GetTMDTLTDTSoQDSuaDoi()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 2
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  SoQD = item.SoQuyetDinh,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             SoQD = item1.OrderByDescending(x => x.NgayPD).Select(x => x.SoQD).FirstOrDefault(),
                             NgayPD = item1.OrderByDescending(x => x.NgayPD).Select(x => x.NgayPD).FirstOrDefault()
                         }).ToList<object>();

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oDT in query)
            {
                string szSo_NgayPD = "";

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                string szSQD = (string)GetValueObject(oDT, "SoQD");
                string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                szSo_NgayPD = szSQD + "," + szNPD + ";";

                dicRe.Add(IDDA, szSo_NgayPD);
            }

            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTriSuaDoi()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 1
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  TongGiaTri = item.TongGiaTri == null ? 0 : item.TongGiaTri,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             TongGiaTri = item1.OrderByDescending(x => x.NgayPD).Select(x => x.TongGiaTri).FirstOrDefault(),

                         }).ToList<object>();

            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oDT in query)
            {
                double dTongGiaTri = 0;

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                double TongGiaTri = (double)GetValueObject(oDT, "TongGiaTri");

                dTongGiaTri = TongGiaTri;

                dicRe.Add(IDDA, dTongGiaTri);
            }

            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLTDTTongGiaTriSuaDoi()
        {
            var query = (from item in
                             (from item in DbContext.LapQuanLyDauTus
                              where item.LoaiQuanLyDauTu == 2
                              select new
                              {
                                  IDDA = item.IdDuAn,
                                  TongGiaTri = item.TongGiaTri == null ? 0 : item.TongGiaTri,
                                  NgayPD = item.NgayPheDuyet
                              })
                         group item by new { item.IDDA }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IDDA,
                             TongGiaTri = item1.OrderByDescending(x => x.NgayPD).Select(x => x.TongGiaTri).FirstOrDefault(),

                         }).ToList<object>();

            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oDT in query)
            {
                double dTongGiaTri = 0;

                int IDDA = (int)GetValueObject(oDT, "IDDA");

                double TongGiaTri = (double)GetValueObject(oDT, "TongGiaTri");

                dTongGiaTri = TongGiaTri;

                dicRe.Add(IDDA, dTongGiaTri);
            }

            return dicRe;
        }

        public List<object> GetLuyKeVonThanhToanCPKs(int iNam)
        {
            List<object> lstThanhToanCPK;

            lstThanhToanCPK = (from item in
                                   (from ttcpk in DbContext.ThanhToanChiPhiKhacs.Include(a => a.NguonVonThanhToanChiPhiKhacs)
                                    where (ttcpk.NgayThanhToan.Value.Year <= iNam - 2)
                                    select new
                                    {
                                        IDDA = ttcpk.IdDuAn,

                                        GiaTriNguonNSTW = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonTPCP = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonKhac = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriVonVayNuocNgoai = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri),
                                    })
                               group item by new { item.IDDA }
                               into item1
                               select new
                               {
                                   IDDA = item1.Key.IDDA,

                                   GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),

                                   GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),

                                   GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),

                                   GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),

                               }).ToList<object>();

            return lstThanhToanCPK;
        }

        public List<object> GetLuyKeVonThanhToanHD(int iNam)
        {
            List<object> lstThanhToanHD;

            lstThanhToanHD = (from item in
                                  (from hd in DbContext.HopDongs.Include("ThanhToanHopDongs.NguonVonGoiThauThanhToans.NguonVonDuAnGoiThaus.NguonVonDuAns.NguonVons")
                                   select new
                                   {
                                       IDDA = hd.IdDuAn,

                                       GiaTriNguonNSTW = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonTPCP = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonKhac = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriVonVayNuocNgoai = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year <= iNam - 2) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri))
                                   })
                              group item by new { item.IDDA }
                             into item1
                              select new
                              {
                                  IDDA = item1.Key.IDDA,
                                  GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),
                                  GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),
                                  GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),
                                  GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),
                              }).ToList<object>();
            return lstThanhToanHD;
        }

        public List<object> GetDicKHVN_ODA(int iNam)
        {
            List<object> KHV = (from item in DbContext.KeHoachVons
                                where item.IdKeHoachVonDieuChinh == null && item.NienDo == iNam
                                group item by new { item.IdDuAn, item.IdKeHoachVon, item.NienDo }
                                into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHV = item1.Key.IdKeHoachVon,
                                    NienDo = item1.Key.NienDo,
                                    KeHoachVonNam_NSTW = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "NGANSACHTRUNGUONG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "NGANSACHTRUNGUONG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "Vốn trái phiếu chính phủ đối ứng")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_Khac = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONDOIUNG" && y.NguonVonDuAn.NguonVon.Ma != "NGANSACHTRUNGUONG" && y.NguonVonDuAn.NguonVon.Ma != "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONDOIUNG" && y.NguonVonDuAn.NguonVon.Ma != "NGANSACHTRUNGUONG" && y.NguonVonDuAn.NguonVon.Ma != "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_NuocNgoai = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONNUOCNGOAI" || y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONNUOCNGOAI")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONNUOCNGOAI" || y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONNUOCNGOAI")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                                }).ToList<object>();

            return KHV;
        }

        public List<object> GetDicKHVNDC_ODA(int iNam)
        {
            List<object> KHVDC = (from item in DbContext.KeHoachVons
                                where item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam
                                group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                                into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHVDC = item1.Key.IdKeHoachVonDieuChinh,
                                    NienDo = item1.Key.NienDo,
                                    KeHoachVonNam_NSTW = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "NGANSACHTRUNGUONG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "NGANSACHTRUNGUONG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_Khac = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONDOIUNG" && y.NguonVonDuAn.NguonVon.Ma != "NGANSACHTRUNGUONG" && y.NguonVonDuAn.NguonVon.Ma != "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONDOIUNG" && y.NguonVonDuAn.NguonVon.Ma != "NGANSACHTRUNGUONG" && y.NguonVonDuAn.NguonVon.Ma != "VONTPCPDOIUNG")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                                    KeHoachVonNam_NuocNgoai = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONNUOCNGOAI" || y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONNUOCNGOAI")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => (y.NguonVonDuAn.NguonVon.Ma == "VONNUOCNGOAI" || y.NguonVonDuAn.NguonVon.NguonVonCha.Ma == "VONNUOCNGOAI")).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                                }).ToList<object>();

            return KHVDC;

        }

        public List<object> GetUocThanhToanCPKs(int iNam)
        {
            List<object> lstThanhToanCPK;

            lstThanhToanCPK = (from item in
                                   (from ttcpk in DbContext.ThanhToanChiPhiKhacs.Include(a => a.NguonVonThanhToanChiPhiKhacs)
                                    where ((ttcpk.NgayThanhToan.Value.Year >= iNam - 1) || ((ttcpk.NgayThanhToan.Value.Year == iNam) && (ttcpk.NgayThanhToan.Value.Month == 1)))
                                    select new
                                    {
                                        IDDA = ttcpk.IdDuAn,

                                        GiaTriNguonNSTW = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonTPCP = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonKhac = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriVonVayNuocNgoai = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri),
                                    })
                               group item by new { item.IDDA }
                               into item1
                               select new
                               {
                                   IDDA = item1.Key.IDDA,

                                   GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),

                                   GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),

                                   GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),

                                   GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),

                               }).ToList<object>();

            return lstThanhToanCPK;
        }

        public List<object> GetUocThanhToanHD(int iNam)
        {
            List<object> lstThanhToanHD;

            lstThanhToanHD = (from item in
                                  (from hd in DbContext.HopDongs.Include("ThanhToanHopDongs.NguonVonGoiThauThanhToans.NguonVonDuAnGoiThaus.NguonVonDuAns.NguonVons")
                                   select new
                                   {
                                       IDDA = hd.IdDuAn,

                                       GiaTriNguonNSTW = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonTPCP = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonKhac = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriVonVayNuocNgoai = hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => (((x.ThoiDiemThanhToan.Value.Year >= iNam - 1) || ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.ThoiDiemThanhToan.Value.Month == 1))) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri))
                                   })
                              group item by new { item.IDDA }
                             into item1
                              select new
                              {
                                  IDDA = item1.Key.IDDA,
                                  GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),
                                  GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),
                                  GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),
                                  GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),
                              }).ToList<object>();
            return lstThanhToanHD;
        }

        public List<object> GetThanhToanCPKsNam(int iNam)
        {
            List<object> lstThanhToanCPK;

            lstThanhToanCPK = (from item in
                                   (from ttcpk in DbContext.ThanhToanChiPhiKhacs.Include(a => a.NguonVonThanhToanChiPhiKhacs)
                                    where (ttcpk.NgayThanhToan.Value.Year == iNam)
                                    select new
                                    {
                                        IDDA = ttcpk.IdDuAn,

                                        GiaTriNguonNSTW = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonTPCP = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng" && x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriNguonKhac = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương" && x.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng")).Sum(x => (double?)x.GiaTri),

                                        GiaTriVonVayNuocNgoai = ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri) == null ? 0 : ttcpk.NguonVonThanhToanChiPhiKhacs.Where(x => (x.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài" || x.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")).Sum(x => (double?)x.GiaTri),
                                    })
                               group item by new { item.IDDA }
                               into item1
                               select new
                               {
                                   IDDA = item1.Key.IDDA,

                                   GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),

                                   GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),

                                   GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),

                                   GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),

                               }).ToList<object>();

            return lstThanhToanCPK;
        }

        public List<object> GetThanhToanHDNam(int iNam)
        {
            List<object> lstThanhToanHD;

            lstThanhToanHD = (from item in
                                  (from hd in DbContext.HopDongs.Include("ThanhToanHopDongs.NguonVonGoiThauThanhToans.NguonVonDuAnGoiThaus.NguonVonDuAns.NguonVons")
                                   select new
                                   {
                                       IDDA = hd.IdDuAn,

                                       GiaTriNguonNSTW = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonTPCP = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ đối ứng")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriNguonKhac = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Vốn trái phiếu chính phủ đối ứng" && y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon != "Ngân sách trung ương")).Sum(z => (double?)z.GiaTri)),

                                       GiaTriVonVayNuocNgoai = hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri)) == null ? 0 : hd.ThanhToanHopDongs.Where(x => ((x.ThoiDiemThanhToan.Value.Year == iNam) && (x.NguonVonGoiThauThanhToans.Any(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài"))))).Sum(x => x.NguonVonGoiThauThanhToans.Where(y => (y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài" || y.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vay nước ngoài")).Sum(z => (double?)z.GiaTri))
                                   })
                              group item by new { item.IDDA }
                             into item1
                              select new
                              {
                                  IDDA = item1.Key.IDDA,
                                  GiaTriNguonNSTW = item1.Sum(x => x.GiaTriNguonNSTW),
                                  GiaTriNguonTPCP = item1.Sum(x => x.GiaTriNguonTPCP),
                                  GiaTriNguonKhac = item1.Sum(x => x.GiaTriNguonKhac),
                                  GiaTriVonVayNuocNgoai = item1.Sum(x => x.GiaTriVonVayNuocNgoai),
                              }).ToList<object>();
            return lstThanhToanHD;
        }


        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }
    }
}



