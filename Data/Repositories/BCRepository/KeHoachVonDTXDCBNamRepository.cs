﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Z.EntityFramework.Plus;
using System.Globalization;

namespace Data.Repositories.BCRepository
{
    public interface IKeHoachVonDTXDCBNamRepository : IRepository<KeHoachVonDTXDCBNam>
    {
        List<object> GetBCVonDauTuXDCBNam(string IDDuAns);
        List<object> GetDuAnCon(string IDDuAns);

        Dictionary<int, string> GetTMDTLDADTSoQD();
        Dictionary<int, string> GetTMDTLTDTSoQD();

        Dictionary<int, double> GetTMDTLDADTTongGiaTri();
        Dictionary<int, double> GetTMDTLTDTTongGiaTri();
        
        List<object> GetLstKhoiLuongThucHien(int iNam);

        List<object> GetKeHoachVonNamDieuChinh(int iNam);

        List<object> GetKeHoachVonNamXDCB(int iNam);

        List<object> GetThanhToanChiPhiKhacN_1(int iNam);

        List<object> GetThanhToanHopDongN_1(int iNam);

        List<object> GetThanhToanChiPhiKhacN_2(int iNam);

        List<object> GetThanhToanHopDongN_2(int iNam);
    }

    public class KeHoachVonDTXDCBNamRepository : RepositoryBase<KeHoachVonDTXDCBNam>, IKeHoachVonDTXDCBNamRepository
    {
        public KeHoachVonDTXDCBNamRepository(IDbFactory dbFactory) : base(dbFactory)
        { }

        public List<object> GetBCVonDauTuXDCBNam(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lBaoCao = from item in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                          where item.IdChuDauTu != null && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString())
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDChuDauTu = item.IdChuDauTu,
                              ChuDauTu = item.ChuDauTu.TenChuDauTu,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianKhoiCongHoanThanh = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien
                          };

            var lstBaoCao = (from item in lBaoCao
                             group item by new { item.IDChuDauTu, item.ChuDauTu }
                            into item1
                             select new
                             {
                                 IDChuDauTu = item1.Key.IDChuDauTu,
                                 TenChuDauTu = item1.Key.ChuDauTu,
                                 grpTTDA = item1.GroupBy(x => new { x.IDTinhTrangDuAn, x.TinhTrangDuAn }).Select(x => new
                                 {
                                     IDTinhTrangDuAn = x.Key.IDTinhTrangDuAn,
                                     TenTinhTrangDuAn = x.Key.TinhTrangDuAn,
                                     grpLVNN = item1.Where(xx => xx.IDTinhTrangDuAn == x.Key.IDTinhTrangDuAn).GroupBy(y => new { y.IDLinhVucNganhNghe, y.LinhVucNganhNghe }).Select(y => new
                                     {
                                         IDLinhVucNganhNghe = y.Key.IDLinhVucNganhNghe,
                                         TenLinhVucNganhNghe = y.Key.LinhVucNganhNghe,
                                         grpDuAn = item1.Where(yy => yy.IDLinhVucNganhNghe == y.Key.IDLinhVucNganhNghe).Select(z => new
                                         {
                                             IDDA = z.IDDA,
                                             TenDuAn = z.TenDuAn,
                                             DiaDiemXayDung = z.DiaDiemXayDung,
                                             NangLucThietKe = z.NangLucThietKe,
                                             ThoiGianKhoiCongHoanThanh = z.ThoiGianKhoiCongHoanThanh,
                                         }).ToList<object>()
                                     }).ToList<object>()
                                 }).ToList<object>()
                             }).ToList<object>();
            return lstBaoCao;
        }

        public List<object> GetDuAnCon(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lDuAnCon = (from item in DbContext.DuAns.Include(a => a.LapQuanLyDauTus)
                            where item.IdDuAnCha != null && arrDuAn.Contains(item.IdDuAnCha.ToString())
                            select new
                            {
                                IDDA = item.IdDuAn,
                                IDDACha = item.IdDuAnCha,
                                TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                                DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                                NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                                ThoiGianKhoiCongHoanThanh = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                                LoaiDieuChinh = item.LapQuanLyDauTus.Select(x => x.LoaiDieuChinh).FirstOrDefault().ToString() == null ? "" : item.LapQuanLyDauTus.Select(x => x.LoaiDieuChinh).FirstOrDefault().ToString()
                            }).ToList<object>();
            return lDuAnCon;
        }

        public Dictionary<int, string> GetTMDTLDADTSoQD()
        {
            var query = (from item in DbContext.LapQuanLyDauTus.OrderByDescending(x => x.NgayPheDuyet)
                         where item.LoaiQuanLyDauTu == 1
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             grpQDDT = item1/*.OrderBy(x => x.NgayPheDuyet)*/.Select(x => new
                             {
                                 SoQD = x.SoQuyetDinh == null ? "" : x.SoQuyetDinh,
                                 NgayPD = x.NgayPheDuyet,
                                 LoaiDieuChinh = x.LoaiDieuChinh == null ? 0 : x.LoaiDieuChinh
                             }).ToList<object>()
                         }).ToList<object>();

            string szSo_NgayPD = "";

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oQDDT in query)
            {
                int IDDA = (int)GetValueObject(oQDDT, "IDDA");

                List<object> lstDT = (List<object>)GetValueObject(oQDDT, "grpQDDT");
                string sSo_NgayPD = "";
                foreach (var oDT in lstDT)
                {

                    int LoaiDieuChinh = (int)GetValueObject(oDT, "LoaiDieuChinh");

                    string szSQD = (string)GetValueObject(oDT, "SoQD");
                    string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                    string So_NgayPD = szSQD + "," + szNPD + ";";

                    if (LoaiDieuChinh == 0)
                    {
                        sSo_NgayPD = So_NgayPD;
                    }
                    if (LoaiDieuChinh == 1)
                    {
                        sSo_NgayPD += So_NgayPD;
                    }

                    if (LoaiDieuChinh == 2)
                    {
                        sSo_NgayPD = So_NgayPD;
                    }
                }
                szSo_NgayPD = sSo_NgayPD;
                dicRe.Add(IDDA, szSo_NgayPD);

            }
            return dicRe;
        }

        public Dictionary<int, string> GetTMDTLTDTSoQD()
        {
            var query = (from item in DbContext.LapQuanLyDauTus.OrderByDescending(x => x.NgayPheDuyet)
                         where item.LoaiQuanLyDauTu == 2
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             grpQDDT = item1/*.OrderBy(x => x.NgayPheDuyet)*/.Select(x => new
                             {
                                 SoQD = x.SoQuyetDinh == null ? "" : x.SoQuyetDinh,
                                 NgayPD = x.NgayPheDuyet,
                                 LoaiDieuChinh = x.LoaiDieuChinh == null ? 0 : x.LoaiDieuChinh
                             }).ToList<object>()
                         }).ToList<object>();

            string szSo_NgayPD = "";

            Dictionary<int, string> dicRe = new Dictionary<int, string>();

            foreach (var oQDDT in query)
            {
                int IDDA = (int)GetValueObject(oQDDT, "IDDA");
                List<object> lstDT = (List<object>)GetValueObject(oQDDT, "grpQDDT");

                string sSo_NgayPD = "";

                foreach (var oDT in lstDT)
                {

                    int LoaiDieuChinh = (int)GetValueObject(oDT, "LoaiDieuChinh");

                    string szSQD = (string)GetValueObject(oDT, "SoQD");
                    string szNPD = ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy") == "01/01/0001" ? "" : ((DateTime?)GetValueObject(oDT, "NgayPD")).GetValueOrDefault().ToString("dd/MM/yyyy");

                    string So_NgayPD = szSQD + "," + szNPD + ";";

                    if (LoaiDieuChinh == 0)
                    {
                        sSo_NgayPD = So_NgayPD;

                    }
                    if (LoaiDieuChinh == 1)
                    {
                        sSo_NgayPD += So_NgayPD;

                    }

                    if (LoaiDieuChinh == 2)
                    {
                        sSo_NgayPD = So_NgayPD;
                    }
                }
                szSo_NgayPD = sSo_NgayPD;
                dicRe.Add(IDDA, szSo_NgayPD);
            }
            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLDADTTongGiaTri()
        {
            var query = (from item in DbContext.LapQuanLyDauTus.OrderByDescending(x => x.NgayPheDuyet)
                         where item.LoaiQuanLyDauTu == 1
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             grpQDDT = item1/*.OrderBy(x => x.NgayPheDuyet)*/.Select(x => new
                             {
                                 TongGiaTri = x.TongGiaTri == null ? 0 : x.TongGiaTri,
                                 LoaiDieuChinh = x.LoaiDieuChinh == null ? 0 : x.LoaiDieuChinh
                             }).ToList<object>()
                         }).ToList<object>();

            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oQDDT in query)
            {
                int IDDA = (int)GetValueObject(oQDDT, "IDDA");

                double dTongGiaTri = 0;

                List<object> lstDT = (List<object>)GetValueObject(oQDDT, "grpQDDT");
                foreach (var oDT in lstDT)
                {
                    int LoaiDieuChinh = (int)GetValueObject(oDT, "LoaiDieuChinh");

                    double dTGT = (double)GetValueObject(oDT, "TongGiaTri");

                    dTongGiaTri = dTGT;
                }
                dicRe.Add(IDDA, dTongGiaTri);
            }
            return dicRe;
        }

        public Dictionary<int, double> GetTMDTLTDTTongGiaTri()
        {
            var query = (from item in DbContext.LapQuanLyDauTus.OrderByDescending(x => x.NgayPheDuyet)
                         where item.LoaiQuanLyDauTu == 2
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             grpQDDT = item1/*.OrderBy(x => x.NgayPheDuyet)*/.Select(x => new
                             {
                                 TongGiaTri = x.TongGiaTri == null ? 0 : x.TongGiaTri,
                                 LoaiDieuChinh = x.LoaiDieuChinh == null ? 0 : x.LoaiDieuChinh
                             }).ToList<object>()
                         }).ToList<object>();



            Dictionary<int, double> dicRe = new Dictionary<int, double>();

            foreach (var oQDDT in query)
            {
                int IDDA = (int)GetValueObject(oQDDT, "IDDA");

                List<object> lstDT = (List<object>)GetValueObject(oQDDT, "grpQDDT");

                double dTongGiaTri = 0;

                foreach (var oDT in lstDT)
                {
                    int LoaiDieuChinh = (int)GetValueObject(oDT, "LoaiDieuChinh");

                    double dTGT = (double)GetValueObject(oDT, "TongGiaTri");

                    dTongGiaTri = dTGT;
                }
                dicRe.Add(IDDA, dTongGiaTri);
            }
            return dicRe;
        }
        
        public List<object> GetLstKhoiLuongThucHien(int iNam)
        {
            List<object> lstKhoiLuongThucHien;

            lstKhoiLuongThucHien = (from item in DbContext.DuAns
                                    group item by new { item.IdDuAn }
                                    into item1
                                    select new
                                    {
                                        IDDA = item1.Key.IdDuAn,

                                        KLTHDenNgay = item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year <= iNam - 2)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year <= iNam - 2).Sum(z => (double?)z.KhoiLuong))) == null ? 0 : item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year <= iNam - 2)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year <= iNam - 2).Sum(z => (double?)z.KhoiLuong))),

                                        KLTHCaNam = item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year == iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam - 1).Sum(z => (double?)z.KhoiLuong))) == null ? 0 : item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year == iNam - 1)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam - 1).Sum(z => (double?)z.KhoiLuong)))
                                    }).ToList<object>();

            return lstKhoiLuongThucHien;
        }

        public List<object> GetKeHoachVonNamDieuChinh(int iNam)
        {
            var KHVDC = (from item in DbContext.KeHoachVons
                         where item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam
                         group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                         into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             IDKHVDC = item1.Key.IdKeHoachVonDieuChinh,
                             NienDo = item1.Key.NienDo,
                             KeHoachVonNam_NSTP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             KeHoachVonNam_SDDDD = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             KeHoachVonNam_NSTT = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             KeHoachVonNam_NST = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             KeHoachVonNam_ODA = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                             KeHoachVonNam_TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                         }).ToList<object>();

            return KHVDC;
        }

        public List<object> GetKeHoachVonNamXDCB(int iNam)
        {
            var KHV = (from item in DbContext.KeHoachVons
                       where item.IdKeHoachVonDieuChinh == null && item.NienDo == iNam
                       group item by new { item.IdDuAn, item.IdKeHoachVon, item.NienDo }
                       into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           IDKHV = item1.Key.IdKeHoachVon,
                           NienDo = item1.Key.NienDo,
                           KeHoachVonNam_NSTP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           KeHoachVonNam_SDDDD = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           KeHoachVonNam_NSTT = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung").Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           KeHoachVonNam_NST = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           KeHoachVonNam_ODA = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum(),
                           KeHoachVonNam_TPCP = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.NguonVonKeHoachVons.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Select(z => (double?)z.GiaTri)).FirstOrDefault().Sum()
                       }).ToList<object>();

            return KHV;
        }

        public List<object> GetThanhToanChiPhiKhacN_2(int iNam)
        {
            var lstThanhToanCPKsN_2 = (from ttcpk in DbContext.ThanhToanChiPhiKhacs
                                       where ttcpk.NgayThanhToan.Value.Year <= iNam - 2
                                       group ttcpk by new { ttcpk.IdDuAn }
                                      into ttcpk1
                                       select new
                                       {
                                           IDDA = ttcpk1.Key.IdDuAn,
                                           GiaTriCPKN_2 = ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri)) == null ? 0 : ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri))
                                       }).ToList<object>();

            return lstThanhToanCPKsN_2;
        }

        public List<object> GetThanhToanHopDongN_2(int iNam)
        {
            var lstThanhToanHDN_2 = (from tthd in DbContext.DuAns
                                     select new
                                     {
                                         IDDA = tthd.IdDuAn,
                                         GiaTriTTHDN_2 = tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year <= iNam - 2)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year <= iNam - 2).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri))) == null ? 0 : tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year <= iNam - 2)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year <= iNam - 2).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri)))
                                     }).ToList<object>();
            return lstThanhToanHDN_2;
        }
        
        public List<object> GetThanhToanChiPhiKhacN_1(int iNam)
        {
            //Thanh toán chi phí khác
            List<object> lstThanhToanCPKN_1;

            lstThanhToanCPKN_1 = (from item in DbContext.ThanhToanChiPhiKhacs
                                  where item.NgayThanhToan.Value.Year == iNam - 1
                                  group item by new { item.IdDuAn }
                                  into ttcpk
                                  select new
                                  {
                                      IDDA = ttcpk.Key.IdDuAn,

                                      GiaTriNguonSDD = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri)),

                                      GiaTriNguonSDDDD = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri)),

                                      GiaTriNguonTinh = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri)),

                                      GiaTriNguonTT = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri)),

                                      GiaTriNguonODA = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Sum(z => (double?)z.GiaTri)),

                                      GiaTriNguonTPCP = ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri)) == null ? 0 : ttcpk.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Where(y => ((y.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (y.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri))

                                  }).ToList<object>();
            
            return lstThanhToanCPKN_1;
        }

        public List<object> GetThanhToanHopDongN_1(int iNam)
        {
            //Thanh toán hợp đồng

            List<object> lstThanhToanHDN_1;

            lstThanhToanHDN_1 = (from da in DbContext.HopDongs
                                 group da by new { da.IdDuAn }
                                 into da1
                                 select new
                                 {
                                     IDDA = da1.Key.IdDuAn,

                                     GiaTriNguonSDD = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu từ SDĐ") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách huyện"))).Sum(z => (double?)z.GiaTri))),

                                     GiaTriNguonSDDDD = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Nguồn thu tiền SDĐ dôi dư") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách xã"))).Sum(z => (double?)z.GiaTri))),

                                     GiaTriNguonTinh = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tỉnh") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách tỉnh"))).Sum(z => (double?)z.GiaTri))),

                                     GiaTriNguonTT = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Ngân sách tập trung") && (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Ngân sách thành phố"))).Sum(z => (double?)z.GiaTri))),

                                     GiaTriNguonODA = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn ODA") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn đối ứng") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vay nước ngoài"))).Sum(z => (double?)z.GiaTri))),

                                     GiaTriNguonTPCP = da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri))) == null ? 0 : da1.Sum(x => x.ThanhToanHopDongs.Where(y => ((y.ThoiDiemThanhToan.Value.Year == iNam - 1) && (y.NguonVonGoiThauThanhToans.Any(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ")))))).Sum(y => y.NguonVonGoiThauThanhToans.Where(yy => ((yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.TenNguonVon == "Vốn trái phiếu chính phủ") || (yy.NguonVonDuAnGoiThau.NguonVonDuAn.NguonVon.NguonVonCha.TenNguonVon == "Vốn trái phiếu chính phủ"))).Sum(z => (double?)z.GiaTri)))
                                 }).ToList<object>();
            
            return lstThanhToanHDN_1;
        }

        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }
    }
}