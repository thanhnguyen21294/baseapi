﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.BCModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Z.EntityFramework.Plus;
using System.Globalization;
using Data.Repositories.QLTDRepository;
using Model.Models.BCModel.BaocaoTiendo;

namespace Data.Repositories.BCRepository
{
    public interface IQLTD_TTDA_BCRepository : IRepository<QLTD_TTDA__BC>
    {
        #region Báo cáo Tiến đọ kế hoạch xây dựng cơ bản
        List<object> GetListDACbiDTu(string IDDuAns);
        List<object> GetListDA(string IDDuAns);
        List<object> GetDuAnCon(string IDDuAns);

        #region Chủ trương đầu tư
        List<object> GetList_CTDT_TGHTCT(string IDDuAns);
        List<object> GetList_CTDT_NGNV(string IDDuAns);
        List<object> GetList_CTDT_CTDD(string IDDuAns);
        List<object> GetList_CTDT_LCTDT(string IDDuAns);
        List<object> GetList_CTDT_NTHSPD(string IDDuAns);
        List<object> GetList_CTDT_PDCTDT(string IDDuAns);
        List<object> GetList_CTDT_KK(string IDDuAns);
        List<object> GetList_CTDT_GP(string IDDuAns);
        List<object> GetList_CTDT_TDC(string IDDuAns);
        #endregion

        #region Chuẩn bị đầu tư
        BC_UyBan_PL1_Them GetDACbiDTu(int IDDuAn);
        List<object> GetList_CBDT_BCPATK(string IDDuAns);
        List<object> GetList_CBDT_PDT_CBDT(string IDDuAns);
        List<object> GetList_CBDT_PKHKCNT(string IDDuAns);
        List<object> GetList_CBDT_KSHT_CGDD_HTKT(string IDDuAns);
        List<object> GetList_CBDT_PDQH(string IDDuAns);
        List<object> GetList_CBDT_KSDC(string IDDuAns);
        List<object> GetList_CBDT_HTPDDA(string IDDuAns);
        List<object> GetList_CBDT_TTDPDDA(string IDDuAns);
        List<object> GetList_CBDT_PCCC_TTK(string IDDuAns);
        List<object> GetList_CBDT_DLHS(string IDDuAns);
        //List<object> GetList_CBDT_KKGP(string IDDuAns);
        List<object> GetList_CBDT_KK(string IDDuAns);
        List<object> GetList_CBDT_GP(string IDDuAns);
        List<object> GetList_CBDT_TDC(string IDDuAns);
        #endregion

        #region Chuẩn bị thực hiện
        //Chuẩn bị thực hiện
        List<object> GetList_CBTH_PDT(string IDDuAns);
        List<object> GetList_CBTH_PKHLCNT(string IDDuAns);
        List<object> GetList_CBTH_HTPD_THBV(string IDDuAns);
        List<object> GetList_CBTH_TTD_TKBV(string IDDuAns);
        List<object> GetList_CBTH_DLHS(string IDDuAns);
        //List<object> GetList_CBTH_KKGP(string IDDuAns);
        List<object> GetList_CBTH_KK(string IDDuAns);
        List<object> GetList_CBTH_GP(string IDDuAns);
        List<object> GetList_CBTH_TDC(string IDDuAns);
        #endregion

        #region Thực hiện
        //Thực hiện 
        List<object> GetList_THDT_HTTC(string IDDuAns);
        List<object> GetList_THDT_DTCXD(string IDDuAns);
        List<object> GetList_THDT_DLCNT(string IDDuAns);
        List<object> GetList_THDT_CTHLCNT(string IDDuAns);
        List<object> GetList_THDT_GPMB(string IDDuAns);
        //List<object> GetList_THDT_KKGP(string IDDuAns);
        List<object> GetList_TH_KK(string IDDuAns);
        List<object> GetList_TH_GP(string IDDuAns);
        List<object> GetList_TH_TDC(string IDDuAns);
        #endregion

        #region Quyết toán
        List<object> GetList_QT_HTQT(string IDDuAns);
        List<object> GetList_QT_HTHS_QLCL(string IDDuAns);
        List<object> GetList_QT_HTHS_HSQT(string IDDuAns);
        List<object> GetList_QT_HTHS_KTDL(string IDDuAns);
        List<object> GetList_QT_HTHS_TTQT(string IDDuAns);
        //List<object> GetList_QT_KKGP(string IDDuAns);
        List<object> GetList_QT_KKGP(string IDDuAns);
        List<object> GetList_QT_TDC(string IDDuAns);
        #endregion

        #region Kế hoạch vốn
        List<object> GetList_KHV_BD(string IDDuAns, int iNam);
        List<object> GetList_KHV_SDC(string IDDuAns, int iNam);
        List<object> GetList_KHV_TTCPK(string IDDuAns, int iNam);
        List<object> GetList_KHV_TTHD(string IDDuAns, int iNam);
        #endregion

        #endregion

        #region Báo cáo Tiến độ dự án chuẩn bị đầu tư --- Báo cáo ủy ban PL1
        #region Cây dự án
        List<object> GetListTD_DA_CBDT_CDA(string IDDuAns);
        List<object> GetListTD_DA_DAC(string IDDuAns);
        #endregion

        #region Kế hoạch vốn
        List<object> GetList_DA_CBDT_KHV(string IDDuAns, int iNam);
        List<object> GetList_DA_CBDT_KHVDC(string IDDuAns, int iNam);
        List<object> GetList_DA_KHV_DXDC(string IDDuAns, int iNam);
        #endregion

        #region Giải ngân
        List<object> GetList_DA_CBDT_TTHD(string IDDuAns, int iNam);
        List<object> GetList_DA_CBDT_TTCPK(string IDDuAns, int iNam);
        #endregion

        #region Hoàn thành trình duyệt
        List<object> GetList_DA_CBDT_TDDA(string IDDuAns);
        #endregion

        #region Hoàn thành lập chủ trương đầu tư
        List<object> GetList_DA_CBDT_HT_LCTDT(string IDDuAns);
        #endregion
        #endregion

        #region Tiến độ thực hiện
        #region Tiến độ thực hiện chủ trương đầu tư
        List<object> GetList_TDTH_LCTDT(string IDDuAns);
        #endregion
        #region Tiến độ thực hiện chuẩn bị đâu tư
        List<object> GetList_TDTH_CBDT(string IDDuAns);
        #endregion
        #region Tiến độ Chuẩn bị thực hiện
        List<object> GetList_TDTH_CBTH(string IDDuAns);
        #endregion
        #region Tiến độ Thực hiện - DTCXD
        List<object> GetList_TDTH_TH_DTCXD(string IDDuAns);
        #endregion
        #region Tiến độ thực hiện - GPMB
        List<object> GetList_TDTH_TH_GPMB(string IDDuAns);
        #endregion
        #endregion

        #region Báo cáo tiến độ thực hiện dự án --- Báo cáo ủy ban PL2
        #region Cây dự án
        List<object> GetListTD_DA_TH_CDA(string IDDuAns);
        #endregion
        #region Khối lượng thực hiện
        List<object> GetLstKhoiLuongThucHien(string IDDuAns, int iNam);
        #endregion
        #endregion

        #region Cây dự án
        #region Quyết toán dự án
        List<object> GetList_QTDA_CDA(string IDDuAns);
        #endregion
        #region Thực hiện dự án
        List<object> GetList_CDA_THDT(string IDDuAns);
        #endregion
        #endregion

        #region Điều hành
        #region Tiến độ dự án

        #region Chủ trương đầu tư   
        List<object> GetList_DH_TDDA_CTDT_CDA(string idUser, string filter);
        #endregion
        #region Chuẩn bị đầu tư
        List<object> GetList_DH_TDDA_CBDT_CDA(string idUser, string filter);
        #endregion
        #region Chuẩn bị thực hiện
        List<object> GetList_DH_TDDA_CBTH_CDA(string idUser, string filter);
        #endregion
        #region Thực hiện
        List<object> GetList_DH_TDDA_TH_CDA(string idUser, string filter);
        #endregion
        #region Quyết toán
        List<object> GetList_DH_TDDA_QT_CDA(string idUser, string filter);
        #endregion


        #endregion
        #region Lấy kế hoạch mới nhất
        List<object> GetList_DH_TDDA_KHMN(string idUser);
        #endregion
        #endregion

        #region Kế hoạch mới nhất, trạng thái hoàn thành dưới mức 4
        List<object> KHMN_DGDPD(string idUser, string filter);
        List<object> KHMN(string idUser);
        #endregion

        #region Công việc chưa hoàn thành - Điều hành
        #region Chủ trương đầu tư
        List<object> GetLst_CV_CHT_CTDT(string idUser);
        #endregion
        #region Chuẩn bị đầu tư
        List<object> GetLst_CV_CHT_CBDT(string idUser);
        #endregion
        #region Chuẩn bị thực hiện
        List<object> GetLst_CV_CHT_CBTH(string idUser);
        #endregion
        #region Thực hiện
        List<object> GetLst_CV_CHT_TH(string idUser);

        List<object> GetLst_CVChuyenQT(string idUser);
        #endregion
        #region Quyết toán
        List<object> GetLst_CV_CHT_QT(string idUser);
        #endregion
        #endregion

        
    }

    public class QLTD_TTDA_BCRepository : RepositoryBase<QLTD_TTDA__BC>, IQLTD_TTDA_BCRepository
    {
        private readonly IQLTD_CTCV_ChuTruongDauTuRepository _iQLTD_CTCV_ChuTruongDauTuRepository;
        private readonly IQLTD_KeHoachRepository QLTD_Kehoach_Repository;
        private readonly IQLTD_KeHoachTienDoChungRepository QLTD_Kehoach_Tiendochung_Repository;
        public QLTD_TTDA_BCRepository(IDbFactory dbFactory, 
            IQLTD_CTCV_ChuTruongDauTuRepository iQLTD_CTCV_ChuTruongDauTuRepository,
            IQLTD_KeHoachRepository qltd_Kehoach_Repository,
            IQLTD_KeHoachTienDoChungRepository qltd_Kehoach_Tiendochung_Repository) : base(dbFactory)
        {
            _iQLTD_CTCV_ChuTruongDauTuRepository = iQLTD_CTCV_ChuTruongDauTuRepository;
            QLTD_Kehoach_Repository = qltd_Kehoach_Repository;
            QLTD_Kehoach_Tiendochung_Repository = qltd_Kehoach_Tiendochung_Repository;
        }
        #region Báo cáo Tiến độ kế hoạch xây dựng cơ bản
        public BC_UyBan_PL1_Them GetDACbiDTu(int IDDuAn)
        {
            var baoCao = from duAn in DbContext.DuAns
                         join keHoach in DbContext.QLTD_KeHoachs on duAn.IdDuAn equals keHoach.IdDuAn
                         join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                         where duAn.IdDuAn == IDDuAn
                         select new
                         {
                             duAn.TongMucDauTu,
                             ctcv.PDDA_BCKTKT_SoQD,
                             ctcv.PDDA_BCKTKT_NgayPD,
                             ctcv.PDDA_BCKTKT_GiaTri
                         };
            //BC_UyBan_PL1_Them bc = baoCao.FirstOrDefault<BC_UyBan_PL1_Them>();
            return null;
        }
        public List<object> GetListDACbiDTu(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            List<object> lstBaoCao;
            
            var lBaoCao = from item in DbContext.DuAns
                          join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                          join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                          where item.IdGiaiDoanDuAn == 1 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString()) && ctcv.PDDA_BCKTKT_GiaTri > 0
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              OrderLinhVuc = item.LinhVucNganhNghe.Order,
                              IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
                              GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
                              OrderDuAn = item.Order,
                              OrderTemp = item.OrderTemp,
                              DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              MaSoDuAn = item.Ma == null ? "" : item.Ma,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              //TMDT = item.TongMucDauTu
                              TMDT = ctcv.PDDA_BCKTKT_GiaTri,
                              PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD,
                              PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD,
                              TMDT_ChuaPD = item.TongMucDauTu
                          };

            lstBaoCao = (from item in lBaoCao
                         group item by new { item.IDLinhVucNganhNghe, item.LinhVucNganhNghe, item.OrderLinhVuc }
                           into item1
                         select new
                         {
                             IDLinhVucNganhNghe = item1.Key.IDLinhVucNganhNghe,
                             TenLinhVucNganhNghe = item1.Key.LinhVucNganhNghe,
                             OrderLinhVuc = item1.Key.OrderLinhVuc,
                             grpGDDT = item1.GroupBy(x => new { x.IDGiaiDoanDuAn, x.GiaiDoanDuAn }).Select(x => new
                             {
                                 IDGiaiDoanDuAn = x.Key.IDGiaiDoanDuAn,
                                 TenGiaiDoanDuAn = x.Key.GiaiDoanDuAn,
                                 grpTTDA = item1.Where(xx => xx.IDGiaiDoanDuAn == x.Key.IDGiaiDoanDuAn).GroupBy(y => new { y.IDTinhTrangDuAn, y.TinhTrangDuAn }).Select(y => new
                                 {
                                     IDTinhTrangDuAn = y.Key.IDTinhTrangDuAn,
                                     TinhTrangDuAn = y.Key.TinhTrangDuAn,
                                     grpDuAn = item1.Where(yy => yy.IDTinhTrangDuAn == y.Key.IDTinhTrangDuAn).Select(z => new
                                     {
                                         IDDA = z.IDDA,
                                         TenDuAn = z.TenDuAn,
                                         NhomDuAn = z.NhomDuAn,
                                         DiaDiemXayDung = z.DiaDiemXayDung,
                                         NangLucThietKe = z.NangLucThietKe,
                                         ThoiGianThucHien = z.ThoiGianThucHien,
                                         MaSoDuAn = z.MaSoDuAn,
                                         TenChuDauTu = z.TenChuDauTu,
                                         TMDT = z.TMDT,
                                         PDDA_BCKTKT_SoQD = z.PDDA_BCKTKT_SoQD,
                                         PDDA_BCKTKT_NgayPD = z.PDDA_BCKTKT_NgayPD,
                                         TMDT_ChuaPD = z.TMDT_ChuaPD,
                                         OrderDuAn = z.OrderDuAn,
                                         OrderTemp = z.OrderTemp
                                     }).Distinct().OrderBy(z => new { z.OrderTemp, z.OrderDuAn }).ToList<object>()
                                 }).ToList<object>()
                             }).ToList<object>()
                         }).OrderBy(x => x.OrderLinhVuc).ToList<object>();
            return lstBaoCao.Distinct().ToList();
        }

        public List<object> GetListDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            List<object> lstBaoCao;

            var lBaoCao = from item in DbContext.DuAns
                          join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                          join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                          where item.IdGiaiDoanDuAn != null && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null && item.IdGiaiDoanDuAn > 1
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString()) && ctcv.PDDA_BCKTKT_GiaTri > 0
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
                              GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
                              DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              MaSoDuAn = item.Ma == null ? "" : item.Ma,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              TMDT = ctcv.PDDA_BCKTKT_GiaTri,
                              PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD,
                              PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD,
                              TMDT_ChuaPD = item.TongMucDauTu
                          };

            lstBaoCao = (from item in lBaoCao
                         group item by new { item.IDGiaiDoanDuAn, item.GiaiDoanDuAn }
                           into item1
                         select new
                         {
                             IDGiaiDoanDuAn = item1.Key.IDGiaiDoanDuAn,
                             GiaiDoanDuAn = item1.Key.GiaiDoanDuAn,
                             grpLVNN = item1.GroupBy(x => new { x.IDLinhVucNganhNghe, x.LinhVucNganhNghe }).Select(x => new
                             {
                                 IDLinhVucNganhNghe = x.Key.IDLinhVucNganhNghe,
                                 LinhVucNganhNghe = x.Key.LinhVucNganhNghe,
                                 grpTTDA = item1.Where(xx => xx.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).GroupBy(y => new { y.IDTinhTrangDuAn, y.TinhTrangDuAn }).Select(y => new
                                 {
                                     IDTinhTrangDuAn = y.Key.IDTinhTrangDuAn,
                                     TinhTrangDuAn = y.Key.TinhTrangDuAn,
                                     grpDuAn = item1.Where(yy => yy.IDTinhTrangDuAn == y.Key.IDTinhTrangDuAn && yy.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).Select(z => new
                                     {
                                         IDDA = z.IDDA,
                                         TenDuAn = z.TenDuAn,
                                         NhomDuAn = z.NhomDuAn,
                                         DiaDiemXayDung = z.DiaDiemXayDung,
                                         NangLucThietKe = z.NangLucThietKe,
                                         ThoiGianThucHien = z.ThoiGianThucHien,
                                         MaSoDuAn = z.MaSoDuAn,
                                         TenChuDauTu = z.TenChuDauTu,
                                         TMDT = z.TMDT,
                                         PDDA_BCKTKT_SoQD = z.PDDA_BCKTKT_SoQD,
                                         PDDA_BCKTKT_NgayPD = z.PDDA_BCKTKT_NgayPD,
                                         TMDT_ChuaPD = z.TMDT_ChuaPD,
                                     }).ToList<object>()
                                 }).ToList<object>()
                             }).ToList<object>()
                         }).ToList<object>();

            return lstBaoCao.Distinct().ToList();
        }

        public List<object> GetDuAnCon(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lDuAnCon = (from item in DbContext.DuAns
                            join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                            join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                            where item.IdDuAnCha != null && arrDuAn.Contains(item.IdDuAnCha.ToString())
                            select new
                            {
                                IDDA = item.IdDuAn,
                                IDDACha = item.IdDuAnCha,
                                TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                                NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
                                DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                                NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                                ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                                MaSoDuAn = item.Ma == null ? "" : item.Ma,
                                TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                                PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD == null ? "" : ctcv.PDDA_BCKTKT_SoQD,
                                PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD == null ? null : ctcv.PDDA_BCKTKT_NgayPD,
                                TMDT_ChuaPD = item.TongMucDauTu == 0 ? 0 : item.TongMucDauTu
                            }).ToList<object>();
            return lDuAnCon;
        }

        #region Chủ trương đầu tư
        public List<object> GetList_CTDT_TGHTCT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_KeHoachCongViecs
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           ThoiGian = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.DenNgay.Value.Year).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_NGNV(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_KeHoachs
                       where arrDuAn.Contains(item.IdDuAn.ToString())
                       group item by new { item.IdDuAn }
                                  into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           NgayGiaoNVu = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NgayGiaoLapChuTruong).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NgayGiaoLapChuTruong).FirstOrDefault().ToString()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_CTDT_CTDD(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                              into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           CoSanDiaDiem = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_CoSanDiaDiem).FirstOrDefault(),
                           SoQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString(),
                           NgayQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString(),
                       }).ToList<object>();

            return lst;

        }

        public List<object> GetList_CTDT_LCTDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                              into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           KhoKhanVuongMac = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString(),
                           KienNghi = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString(),
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_NTHSPD(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                              into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           NgayTrinhHoSoPD = item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString(),
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_PDCTDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                              into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoVBPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString(),
                           NgayPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString(),
                           GiaTri = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault(),
                           NgayPheDuyetThucTe = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString()
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_KK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpKKVM_LCTDT = item1.Select(x => new
                           {
                               KKVM_LCTDT = x.NguyenNhanLyDo,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpKKVM_LCTDT = (List<object>)GetValueObject(item1, "grpKKVM_LCTDT")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_GP(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpGP_LCTDT = item1.Select(x => new
                           {
                               GP_LCTDT = x.GiaiPhapTrienKhai,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpGP_LCTDT = (List<object>)GetValueObject(item1, "grpGP_LCTDT")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CTDT_TDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_KeHoachTienDoChung
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault(),
                            NoiDung = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NoiDung).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           NoiDung = (string)GetValueObject(item1, "NoiDung")
                       }).ToList<object>();

            return lst;
        }
        

        //public List<object> GetTT_LCTDT(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
        //               where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //               group item by new { item.QLTD_KeHoach.IdDuAn }
        //                      into item1
        //               select new
        //               {
        //                   IDDA = item1.Key.IdDuAn,
        //                   CoSanDiaDiem = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_CoSanDiaDiem).FirstOrDefault(),

        //                   SoQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString(),
        //                   NgayQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString(),
        //                   TienDoThucHien = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_TienDoThucHien).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_TienDoThucHien).FirstOrDefault().ToString(),
        //                   KhoKhanVuongMac = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString(),
        //                   KienNghi = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString(),
        //                   NgayTrinhHoSoPD = item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString(),
        //                   SoVBPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString(),
        //                   GiaTri = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault(),
        //                   NgayPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString(),
        //                   NgayPheDuyetThucTe = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString()
        //               }).ToList<object>();

        //    //var lstTT_LCTDT = (from item in
        //    //                      (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
        //    //                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //    //                       group item by new { item.QLTD_KeHoach.IdDuAn }
        //    //                  into item1
        //    //                       select new
        //    //                       {
        //    //                           IDDA = item1.Key.IdDuAn,
        //    //                           CoSanDiaDiem = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_CoSanDiaDiem).FirstOrDefault(),

        //    //                           SoQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_SoVB).FirstOrDefault().ToString(),
        //    //                           NgayQDChapThuan = item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 10).OrderByDescending(x => x.IdKeHoach).Select(x => x.CTDD_NgayPD).FirstOrDefault().ToString(),
        //    //                           TienDoThucHien = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_TienDoThucHien).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_TienDoThucHien).FirstOrDefault().ToString(),
        //    //                           KhoKhanVuongMac = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString(),
        //    //                           KienNghi = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString(),
        //    //                           NgayTrinhHoSoPD = item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 13).OrderByDescending(x => x.IdKeHoach).Select(x => x.THSPDDA_NgayTrinhThucTe).FirstOrDefault().ToString(),
        //    //                           SoVBPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_SoQD).FirstOrDefault().ToString(),
        //    //                           GiaTri = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PQCTDT_GiaTri).FirstOrDefault(),
        //    //                           NgayPheDuyet = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayLaySo).FirstOrDefault().ToString(),
        //    //                           NgayPheDuyetThucTe = item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 14).OrderByDescending(x => x.IdKeHoach).Select(x => x.PQCTDT_NgayPheDuyetTT).FirstOrDefault().ToString()
        //    //                       })
        //    //                   group item by new { item.IDDA }
        //    //                  into item1
        //    //                   select new
        //    //                   {
        //    //                       IDDA = item1.Key.IDDA,
        //    //                       grpTT = item1.Select(x => new
        //    //                       {
        //    //                           CoSanDiaDiem = x.CoSanDiaDiem,
        //    //                           SoQDChapThuan = x.SoQDChapThuan,
        //    //                           NgayQDChapThuan = x.NgayQDChapThuan,
        //    //                           TienDoThucHien = x.TienDoThucHien,
        //    //                           KhoKhanVuongMac = x.KhoKhanVuongMac,
        //    //                           KienNghi = x.KienNghi,
        //    //                           GiaTri = x.GiaTri,
        //    //                           NgayTrinhHoSoPD = x.NgayTrinhHoSoPD,
        //    //                           SoVBPheDuyet = x.SoVBPheDuyet,
        //    //                           NgayPheDuyet = x.NgayPheDuyet,
        //    //                           NgayPheDuyetThucTe = x.NgayPheDuyetThucTe
        //    //                       }).ToList<object>()
        //    //                   }).ToDictionary(x => x.IDDA, x => x.grpTT);
        //    return lst;
        //}



        #endregion

        #region Chuẩn bị đầu tư
        public List<object> GetList_CBDT_BCPATK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           BaoCaoPAnTKe = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 15).Select(x => x.BCPATK_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 15).Select(x => x.BCPATK_NoiDung).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_PDT_CBDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoPheDToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_SoQD).FirstOrDefault(),
                           NgayPheDToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_NgayPQ).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_NgayPQ).FirstOrDefault().ToString(),
                           GiaTriPheDToan = item1.Where(x => x.IdCongViec == 16).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 16).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_PKHKCNT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoPheKH = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 17).Select(x => x.PDKHLCNT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 17).Select(x => x.PDKHLCNT_SoQD).FirstOrDefault(),
                           NgayPheKH = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 17).Select(x => x.PDKHLCNT_NgayPD).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 17).Select(x => x.PDKHLCNT_NgayPD).FirstOrDefault().ToString(),
                           GiaTriPheKH = item1.Where(x => x.IdCongViec == 17).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDKHLCNT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 17).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDKHLCNT_GiaTri).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_KSHT_CGDD_HTKT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           KhaoSatHienTrang = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 18).Select(x => x.KSHT_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 18).Select(x => x.KSHT_NoiDung).FirstOrDefault(),
                           ChiGioiDuongDo = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 19).Select(x => x.CGDD_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 19).Select(x => x.CGDD_NoiDung).FirstOrDefault(),
                           SoLieuHTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 20).Select(x => x.SLHTKT_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 20).Select(x => x.SLHTKT_NoiDung).FirstOrDefault(),
                       }).ToList<object>();

            return lst;
        }
        public List<object> GetList_CBDT_PDQH(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           KhongPheDuyet = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_KhongPheDuyet).FirstOrDefault(),
                           SoPheDuyetQHCT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHCT).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHCT).FirstOrDefault(),
                           NgayPheDuyetQHCT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHCT).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHCT).FirstOrDefault().ToString(),
                           SoPheDuyetQHTMB = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHTMB).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHTMB).FirstOrDefault(),
                           NgayPheDuyetQHTMB = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHTMB).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHTMB).FirstOrDefault().ToString()
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_KSDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           KhaoSatDiaChat = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 22).Select(x => x.KSDCDH_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 22).Select(x => x.KSDCDH_NoiDung).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_HTPDDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_SoQD).FirstOrDefault(),
                           NgayPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_NgayPD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_NgayPD).FirstOrDefault().ToString(),
                           GiaTriPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => (double?)x.PDDA_BCKTKT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => (double?)x.PDDA_BCKTKT_GiaTri).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_TTDPDDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoToTrinhHoSoBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_SoToTrinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_SoToTrinh).FirstOrDefault(),
                           NgayTrinhHoSoBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_NgayTrinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_NgayTrinh).FirstOrDefault().ToString(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_PCCC_TTK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           PCCC = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 23).Select(x => x.TTTDPCCC_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 23).Select(x => x.TTTDPCCC_NoiDung).FirstOrDefault(),
                           TTK = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 24).Select(x => x.TTK_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 24).Select(x => x.TTK_NoiDung).FirstOrDefault(),
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_DLHS(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           LHSCBDT_SoQD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_SoQD).FirstOrDefault(),
                           LHSCBDT_NgayLaySo = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayLaySo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayLaySo).FirstOrDefault().ToString(),
                           LHSCBDT_NgayHoanThanh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayHoanThanh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayHoanThanh).FirstOrDefault().ToString(),
                           LHSCBDT_KhoKhanVuongMac = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KhoKhanVuongMac).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KhoKhanVuongMac).FirstOrDefault(),
                           LHSCBDT_KienNghiDeXuat = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KienNghiDeXuat).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KienNghiDeXuat).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_KK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString()) && (item.QLTD_KeHoach.DuAn.IdGiaiDoanDuAn == 2 || item.QLTD_KeHoach.DuAn.IdGiaiDoanDuAn == 3)
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpKKVM_CBDT = item1.Select(x => new
                           {
                               KKVM_CBDT = x.NguyenNhanLyDo,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpKKVM_CBDT = (List<object>)GetValueObject(item1, "grpKKVM_CBDT")
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_GP(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString()) && (item.QLTD_KeHoach.DuAn.IdGiaiDoanDuAn == 2 || item.QLTD_KeHoach.DuAn.IdGiaiDoanDuAn == 3)
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpGP_CBDT = item1.Select(x => new
                           {
                               GP_CBDT = x.GiaiPhapTrienKhai,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpGP_CBDT = (List<object>)GetValueObject(item1, "grpGP_CBDT")
                       }).ToList<object>();
            return lst;
        }
        public List<object> GetList_CBDT_TDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_KeHoachTienDoChung
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault(),
                            NoiDung = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NoiDung).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           NoiDung = (string)GetValueObject(item1, "NoiDung")
                       }).ToList<object>();

            return lst;
        }

        //public List<object> GetList_CBDT_KKGP(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var l_IDKH_CBDT = from item in DbContext.QLTD_CTCV_ChuanBiDauTus
        //                      where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //                      group item by new { item.QLTD_KeHoach.IdDuAn }
        //                     into item1
        //                      select new
        //                      {
        //                          IDDA = item1.Key.IdDuAn,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString()
        //                      };

        //    var l_IDKH_KKVM = from item in DbContext.QLTD_KhoKhanVuongMac
        //                      where item.DaGiaiQuyet == false
        //                      group item by new { item.IdKhoKhanVuongMac }
        //                      into item1
        //                      select new
        //                      {
        //                          IDKhoKhanVuongMac = item1.Key.IdKhoKhanVuongMac,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString(),
        //                          NguyenNhan = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault(),
        //                          GiaiPhap = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault()
        //                      };

        //    var lst = (from item in l_IDKH_CBDT
        //               join item1 in l_IDKH_KKVM
        //               on item.IDKeHoach equals item1.IDKeHoach
        //               select new
        //               {
        //                   IDDA = item.IDDA,
        //                   NguyenNhan = item1.NguyenNhan,
        //                   GiaiPhap = item1.GiaiPhap
        //               }).ToList<object>();
        //    return lst;
        //}
        //public List<object> GetListTT_CBiDTu(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var lstCbiDTu = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
        //                     where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //                     group item by new { item.QLTD_KeHoach.IdDuAn }
        //            into item1
        //                     select new
        //                     {
        //                         IDDA = item1.Key.IdDuAn,
        //                         BaoCaoPAnTKe = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 15).Select(x => x.BCPATK_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 15).Select(x => x.BCPATK_NoiDung).FirstOrDefault(),
        //                         SoPheDToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_SoQD).FirstOrDefault(),
        //                         NgayPheDToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_NgayPQ.Value.Day + "/" + x.PDDT_NgayPQ.Value.Month + "/" + x.PDDT_NgayPQ.Value.Year).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 16).Select(x => x.PDDT_NgayPQ.Value.Day + "/" + x.PDDT_NgayPQ.Value.Month + "/" + x.PDDT_NgayPQ.Value.Year).FirstOrDefault().ToString(),
        //                         GiaTriPheDToan = item1.Where(x => x.IdCongViec == 16).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault() == null ? 0 : item1.Where(x => x.IdCongViec == 16).OrderByDescending(x => x.IdKeHoach).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault(),

        //                         KhaoSatHienTrang = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 18).Select(x => x.KSHT_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 18).Select(x => x.KSHT_NoiDung).FirstOrDefault(),
        //                         ChiGioiDuongDo = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 19).Select(x => x.CGDD_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 19).Select(x => x.CGDD_NoiDung).FirstOrDefault(),
        //                         SoLieuHTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 20).Select(x => x.SLHTKT_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 20).Select(x => x.SLHTKT_NoiDung).FirstOrDefault(),
        //                         KhongPheDuyet = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_KhongPheDuyet).FirstOrDefault(),
        //                         SoPheDuyetQHCT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHCT).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHCT).FirstOrDefault(),
        //                         NgayPheDuyetQHCT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHCT.Value.Day + "/" + x.PDQH_NgayPD_QHCT.Value.Month + "/" + x.PDQH_NgayPD_QHCT.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHCT.Value.Day + "/" + x.PDQH_NgayPD_QHCT.Value.Month + "/" + x.PDQH_NgayPD_QHCT.Value.Year).FirstOrDefault(),
        //                         SoPheDuyetQHTMB = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHTMB).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_SoQD_QHTMB).FirstOrDefault(),
        //                         NgayPheDuyetQHTMB = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHTMB.Value.Day + "/" + x.PDQH_NgayPD_QHTMB.Value.Month + "/" + x.PDQH_NgayPD_QHTMB.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 21).Select(x => x.PDQH_NgayPD_QHTMB.Value.Day + "/" + x.PDQH_NgayPD_QHTMB.Value.Month + "/" + x.PDQH_NgayPD_QHTMB.Value.Year).FirstOrDefault(),
        //                         KhaoSatDiaChat = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 22).Select(x => x.KSDCDH_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 22).Select(x => x.KSDCDH_NoiDung).FirstOrDefault(),
        //                         SoPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_SoQD).FirstOrDefault(),
        //                         NgayPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_NgayPD.Value.Day + "/" + x.PDDA_BCKTKT_NgayPD.Value.Month + "/" + x.PDDA_BCKTKT_NgayPD.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => x.PDDA_BCKTKT_NgayPD.Value.Day + "/" + x.PDDA_BCKTKT_NgayPD.Value.Month + "/" + x.PDDA_BCKTKT_NgayPD.Value.Year).FirstOrDefault(),
        //                         GiaTriPheDuyetBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => (double?)x.PDDA_BCKTKT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 27).Select(x => (double?)x.PDDA_BCKTKT_GiaTri).FirstOrDefault(),
        //                         SoToTrinhHoSoBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_SoToTrinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_SoToTrinh).FirstOrDefault(),
        //                         NgayTrinhHoSoBCKTKT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_NgayTrinh.Value.Day + "/" + x.THSCBDT_NgayTrinh.Value.Month + "/" + x.THSCBDT_NgayTrinh.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 26).Select(x => x.THSCBDT_NgayTrinh.Value.Day + "/" + x.THSCBDT_NgayTrinh.Value.Month + "/" + x.THSCBDT_NgayTrinh.Value.Year).FirstOrDefault(),
        //                         PCCC = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 23).Select(x => x.TTTDPCCC_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 23).Select(x => x.TTTDPCCC_NoiDung).FirstOrDefault(),
        //                         TTK = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 24).Select(x => x.TTK_NoiDung).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 24).Select(x => x.TTK_NoiDung).FirstOrDefault(),
        //                         LHSCBDT_SoQD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_SoQD).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_SoQD).FirstOrDefault(),
        //                         LHSCBDT_NgayLaySo = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayLaySo.Value.Day + "/" + x.LHSCBDT_NgayLaySo.Value.Month + "/" + x.LHSCBDT_NgayLaySo.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayLaySo.Value.Day + "/" + x.LHSCBDT_NgayLaySo.Value.Month + "/" + x.LHSCBDT_NgayLaySo.Value.Year).FirstOrDefault(),
        //                         LHSCBDT_NgayHoanThanh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayHoanThanh.Value.Day + "/" + x.LHSCBDT_NgayHoanThanh.Value.Month + "/" + x.LHSCBDT_NgayHoanThanh.Value.Year).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_NgayHoanThanh.Value.Day + "/" + x.LHSCBDT_NgayHoanThanh.Value.Month + "/" + x.LHSCBDT_NgayHoanThanh.Value.Year).FirstOrDefault(),
        //                         LHSCBDT_KhoKhanVuongMac = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KhoKhanVuongMac).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KhoKhanVuongMac).FirstOrDefault(),
        //                         LHSCBDT_KienNghiDeXuat = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KienNghiDeXuat).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 25).Select(x => x.LHSCBDT_KienNghiDeXuat).FirstOrDefault()
        //                     }).ToList<object>();

        //    return lstCbiDTu;
        //}
        #endregion

        #region Chuẩn bị thực hiện
        public List<object> GetList_CBTH_PDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lstPheDuToanCBTHDT = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                                      where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                                      group item by new { item.QLTD_KeHoach.IdDuAn }
                                     into item1
                                      select new
                                      {
                                          IDDA = item1.Key.IdDuAn,
                                          PDDT_SoQuyetDinh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => x.PDDT_SoQuyetDinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => x.PDDT_SoQuyetDinh).FirstOrDefault(),
                                          PDDT_NgayPheDuyet = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => x.PDDT_NgayPheDuyet).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => x.PDDT_NgayPheDuyet).FirstOrDefault().ToString(),
                                          PDDT_GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 29).Select(x => (double?)x.PDDT_GiaTri).FirstOrDefault()
                                      }).ToList<object>();
            return lstPheDuToanCBTHDT;
        }

        public List<object> GetList_CBTH_PKHLCNT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lstPheKHLCNT = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                                where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                                group item by new { item.QLTD_KeHoach.IdDuAn }
                                     into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    PDKHLCNT_SoQuyetDinh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => x.PDKHLCNT_SoQuyetDinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => x.PDKHLCNT_SoQuyetDinh).FirstOrDefault(),
                                    PDKHLCNT_NgayPheDuyet = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => x.PDKHLCNT_NgayPheDuyet).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => x.PDKHLCNT_NgayPheDuyet).FirstOrDefault().ToString(),
                                    PDKHLCNT_GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => (double?)x.PDKHLCNT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 30).Select(x => (double?)x.PDKHLCNT_GiaTri).FirstOrDefault()
                                }).ToList<object>();
            return lstPheKHLCNT;
        }

        public List<object> GetList_CBTH_HTPD_THBV(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst_HTPD_THBV = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                                 where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                                 group item by new { item.QLTD_KeHoach.IdDuAn }
                                     into item1
                                 select new
                                 {
                                     IDDA = item1.Key.IdDuAn,
                                     PDDA_SoQuyetDinh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => x.PDDA_SoQuyetDinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => x.PDDA_SoQuyetDinh).FirstOrDefault(),
                                     PDDA_NgayPheDuyet = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => x.PDDA_NgayPheDuyet).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => x.PDDA_NgayPheDuyet).FirstOrDefault().ToString(),
                                     PDDA_GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => (double?)x.PDDA_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 33).Select(x => (double?)x.PDDA_GiaTri).FirstOrDefault()
                                 }).ToList<object>();
            return lst_HTPD_THBV;
        }

        public List<object> GetList_CBTH_TTD_TKBV(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst_TTD_TKBV = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                                where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                                group item by new { item.QLTD_KeHoach.IdDuAn }
                                     into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    THS_SoToTrinh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 32).Select(x => x.THS_SoToTrinh).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 32).Select(x => x.THS_SoToTrinh).FirstOrDefault(),
                                    LHS_NgayTrinh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 32).Select(x => x.LHS_NgayTrinh).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 32).Select(x => x.LHS_NgayTrinh).FirstOrDefault().ToString(),
                                }).ToList<object>();
            return lst_TTD_TKBV;
        }

        public List<object> GetList_CBTH_DLHS(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst_TTD_TKBV = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                                where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                                group item by new { item.QLTD_KeHoach.IdDuAn }
                                     into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    LHSCBDT_KhoKhanVuongMac = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 31).Select(x => x.LHS_KhoKhanVuongMac).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 31).Select(x => x.LHS_KhoKhanVuongMac).FirstOrDefault(),
                                    LHSCBDT_KienNghiDeXuat = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 31).Select(x => x.LHS_KienNghiDeXuat).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 31).Select(x => x.LHS_KienNghiDeXuat).FirstOrDefault(),
                                }).ToList<object>();
            return lst_TTD_TKBV;

        }

        public List<object> GetList_CBTH_KK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpKKVM_CBTH = item1.Select(x => new
                           {
                               KKVM_CBTH = x.NguyenNhanLyDo,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpKKVM_CBTH = (List<object>)GetValueObject(item1, "grpKKVM_CBTH")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CBTH_GP(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpGP_CBTH = item1.Select(x => new
                           {
                               GP_CBTH = x.GiaiPhapTrienKhai,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpGP_CBTH = (List<object>)GetValueObject(item1, "grpGP_CBTH")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_CBTH_TDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_KeHoachTienDoChung
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault(),
                            NoiDung = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NoiDung).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           NoiDung = (string)GetValueObject(item1, "NoiDung")
                       }).ToList<object>();

            return lst;
        }

        //public List<object> GetList_CBTH_KKGP(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var l_IDKH_CBDT = from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
        //                      where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //                      group item by new { item.QLTD_KeHoach.IdDuAn }
        //                     into item1
        //                      select new
        //                      {
        //                          IDDA = item1.Key.IdDuAn,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString()
        //                      };

        //    var l_IDKH_KKVM = from item in DbContext.QLTD_KhoKhanVuongMac
        //                      where item.DaGiaiQuyet == false
        //                      group item by new { item.IdKhoKhanVuongMac }
        //                      into item1
        //                      select new
        //                      {
        //                          IDKhoKhanVuongMac = item1.Key.IdKhoKhanVuongMac,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString(),
        //                          NguyenNhan = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault(),
        //                          GiaiPhap = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault()
        //                      };

        //    var lst = (from item in l_IDKH_CBDT
        //               join item1 in l_IDKH_KKVM
        //               on item.IDKeHoach equals item1.IDKeHoach
        //               select new
        //               {
        //                   IDDA = item.IDDA,
        //                   NguyenNhan = item1.NguyenNhan,
        //                   GiaiPhap = item1.GiaiPhap
        //               }).ToList<object>();
        //    return lst;
        //}
        #endregion

        #region Thực hiện
        public List<object> GetList_THDT_HTTC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           NgayHoanThanh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 38).Select(x => x.HTTC_NgayHoanThanh).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 38).Select(x => x.HTTC_NgayHoanThanh).FirstOrDefault().ToString()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_THDT_DTCXD(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           NgayBanGiao = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_NgayBanGiaoMB).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_NgayBanGiaoMB).FirstOrDefault().ToString(),
                           ThoiGianKhoiCongHoanThanh = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_ThoiGianKhoiCong).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_ThoiGianKhoiCong).FirstOrDefault(),
                           KhoKhanVuongMac = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_KhoKhanVuongMac).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 37).Select(x => x.DTCXD_KhoKhanVuongMac).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_THDT_DLCNT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoQDPD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => x.DLCNT_SoQDKQLCNT).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => x.DLCNT_SoQDKQLCNT).FirstOrDefault(),
                           NgayQDPD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => x.DLCNT_NgayPheDuyet).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => x.DLCNT_NgayPheDuyet).FirstOrDefault().ToString(),
                           GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => (double?)x.DLCNT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 36).Select(x => (double?)x.DLCNT_GiaTri).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_THDT_CTHLCNT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoQDKH = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_SoQDKHLCNT).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_SoQDKHLCNT).FirstOrDefault(),
                           NgayQDKH = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_NgayPheDuyetKHLCNT).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_NgayPheDuyetKHLCNT).FirstOrDefault().ToString(),
                           NgayTrinhKH = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_SoNgayQDPD_KHLCNT).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_SoNgayQDPD_KHLCNT).FirstOrDefault().ToString(),
                           NgayTrinhBVTC = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_DangTrinhTKBVTC).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_DangTrinhTKBVTC).FirstOrDefault().ToString(),
                           DangLapTK = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_DangLapTKBVTC).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_DangLapTKBVTC).FirstOrDefault().ToString(),
                           KhoKhan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_KhoKhan).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 35).Select(x => x.CTHLCNT_KhoKhan).FirstOrDefault().ToString()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_THDT_GPMB(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           DamBaoTienDo = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_DamBaoTienDo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_DamBaoTienDo).FirstOrDefault(),
                           KhoKhanVuongMac = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_KhoKhanVuongMac).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_KhoKhanVuongMac).FirstOrDefault(),
                           DeXuatGiaiPhap = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_DeXuatGiaiPhap).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 34).Select(x => x.GPMB_DeXuatGiaiPhap).FirstOrDefault(),
                       }).ToList<object>();

            return lst;
        }

        //public List<object> GetList_THDT_KKGP(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var l_IDKH_CBDT = from item in DbContext.QLTD_CTCV_ThucHienDuAns
        //                      where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //                      group item by new { item.QLTD_KeHoach.IdDuAn }
        //                     into item1
        //                      select new
        //                      {
        //                          IDDA = item1.Key.IdDuAn,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString()
        //                      };

        //    var l_IDKH_KKVM = from item in DbContext.QLTD_KhoKhanVuongMac
        //                      where item.DaGiaiQuyet == false
        //                      group item by new { item.IdKhoKhanVuongMac }
        //                      into item1
        //                      select new
        //                      {
        //                          IDKhoKhanVuongMac = item1.Key.IdKhoKhanVuongMac,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString(),
        //                          NguyenNhan = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault(),
        //                          GiaiPhap = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault()
        //                      };

        //    var lst = (from item in l_IDKH_CBDT
        //               join item1 in l_IDKH_KKVM
        //               on item.IDKeHoach equals item1.IDKeHoach
        //               select new
        //               {
        //                   IDDA = item.IDDA,
        //                   NguyenNhan = item1.NguyenNhan,
        //                   GiaiPhap = item1.GiaiPhap
        //               }).ToList<object>();
        //    return lst;
        //}
        public List<object> GetList_TH_KK(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpKKVM_TH = item1.Select(x => new
                           {
                               KKVM_TH = x.NguyenNhanLyDo,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpKKVM_TH = (List<object>)GetValueObject(item1, "grpKKVM_TH")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_TH_GP(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpGP_TH = item1.Select(x => new
                           {
                               GP_TH = x.GiaiPhapTrienKhai,
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpGP_TH = (List<object>)GetValueObject(item1, "grpGP_TH")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_TH_TDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_KeHoachTienDoChung
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault(),
                            NoiDung = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NoiDung).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           NoiDung = (string)GetValueObject(item1, "NoiDung")
                       }).ToList<object>();

            return lst;
        }
        #endregion

        #region Quyết toán
        public List<object> GetList_QT_HTQT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           SoQD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => x.HTQT_SoQD).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => x.HTQT_SoQD).FirstOrDefault(),
                           NgayQD = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => x.HTQT_NgayPD).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => x.HTQT_NgayPD).FirstOrDefault().ToString(),
                           GiaTriQT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_GiaTri).FirstOrDefault(),
                           CongNoThu = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_CongNoThu).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_CongNoThu).FirstOrDefault(),
                           CongNoTra = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_CongNoTra).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_CongNoTra).FirstOrDefault(),
                           BoTriVonTra = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_BoTriTraCongNo).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 43).Select(x => (double?)x.HTQT_BoTriTraCongNo).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_QT_HTHS_QLCL(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           HoSoQLCL = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 39).Select(x => x.HSQLCL_DaHoanThanh).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_QT_HTHS_HSQT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 42).Select(x => (double?)x.HTHSQT_GiaTriQuyetToan).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 42).Select(x => (double?)x.HTHSQT_GiaTriQuyetToan).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_QT_HTHS_KTDL(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           KhongKiemToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 41).Select(x => x.KTDL_KhongKiemToan).FirstOrDefault(),
                           DangKiemToan = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 41).Select(x => x.KTDL_DangKiemToan).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 41).Select(x => x.KTDL_DangKiemToan).FirstOrDefault(),
                           GiaTriQT = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 41).Select(x => (double?)x.KTDL_GiaTriSauKiemToan).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 41).Select(x => (double?)x.KTDL_GiaTriSauKiemToan).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        public List<object> GetList_QT_HTHS_TTQT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           DangThamTra = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_DangThamTra).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_DangThamTra).FirstOrDefault(),
                           SoThamTra = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_SoThamTra).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_SoThamTra).FirstOrDefault().ToString(),
                           NgayThamTra = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_NgayThamTra).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => x.TTQT_NgayThamTra).FirstOrDefault().ToString(),
                           GiaTri = item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => (double?)x.TTQT_GiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.IdKeHoach).Where(x => x.IdCongViec == 40).Select(x => (double?)x.TTQT_GiaTri).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }

        //public List<object> GetList_QT_KKGP(string IDDuAns)
        //{
        //    string[] arrDuAn = IDDuAns.Split(',');

        //    var l_IDKH_CBDT = from item in DbContext.QLTD_CTCV_QuyetToanDuAns
        //                      where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
        //                      group item by new { item.QLTD_KeHoach.IdDuAn }
        //                     into item1
        //                      select new
        //                      {
        //                          IDDA = item1.Key.IdDuAn,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString()
        //                      };

        //    var l_IDKH_KKVM = from item in DbContext.QLTD_KhoKhanVuongMac
        //                      where item.DaGiaiQuyet == false
        //                      group item by new { item.IdKhoKhanVuongMac }
        //                      into item1
        //                      select new
        //                      {
        //                          IDKhoKhanVuongMac = item1.Key.IdKhoKhanVuongMac,
        //                          IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault().ToString(),
        //                          NguyenNhan = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NguyenNhanLyDo).FirstOrDefault(),
        //                          GiaiPhap = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault() == null ? "" : item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.GiaiPhapTrienKhai).FirstOrDefault()
        //                      };

        //    var lst = (from item in l_IDKH_CBDT
        //               join item1 in l_IDKH_KKVM
        //               on item.IDKeHoach equals item1.IDKeHoach
        //               select new
        //               {
        //                   IDDA = item.IDDA,
        //                   NguyenNhan = item1.NguyenNhan,
        //                   GiaiPhap = item1.GiaiPhap
        //               }).ToList<object>();
        //    return lst;
        //}

        public List<object> GetList_QT_KKGP(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = from item in DbContext.QLTD_KhoKhanVuongMac
                       where item.DaGiaiQuyet == false
                       group item by new { item.IdKeHoach }
                       into item1
                       select new
                       {
                           IDKH = item1.Key.IdKeHoach,
                           grpKKVM_QT = item1.Select(x => new
                           {
                               KKVM_QT = x.NguyenNhanLyDo,
                               GP_QT = x.GiaiPhapTrienKhai
                           }).ToList<object>()
                       };

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpKKVM_QT = (List<object>)GetValueObject(item1, "grpKKVM_QT")
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_QT_TDC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_KeHoachTienDoChung
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                        into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault(),
                            NoiDung = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.NoiDung).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           NoiDung = (string)GetValueObject(item1, "NoiDung")
                       }).ToList<object>();

            return lst;
        }
        #endregion

        #region Kế hoạch vốn
        public List<object> GetList_KHV_BD(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var lst = (from item in DbContext.KeHoachVons
                       where arrDuAn.Contains(item.IdDuAn.ToString()) && item.IdKeHoachVonDieuChinh == null && item.NienDo == iNam
                       group item by new { item.IdDuAn, item.IdKeHoachVon, item.NienDo }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_KHV_SDC(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var lst = (from item in DbContext.KeHoachVons
                       where arrDuAn.Contains(item.IdDuAn.ToString()) && item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam
                       group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_KHV_TTCPK(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var lst = (from ttcpk in DbContext.ThanhToanChiPhiKhacs
                       where arrDuAn.Contains(ttcpk.IdDuAn.ToString()) && ttcpk.NgayThanhToan.Value.Year == iNam
                       group ttcpk by new { ttcpk.IdDuAn }
                        into ttcpk1
                       select new
                       {
                           IDDA = ttcpk1.Key.IdDuAn,
                           GiaTri = ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri)) == null ? 0 : ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri))
                       }).ToList<object>();
            return lst;
        }

        public List<object> GetList_KHV_TTHD(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var lst = (from tthd in DbContext.DuAns
                       select new
                       {
                           IDDA = tthd.IdDuAn,
                           GiaTri = tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri))) == null ? 0 : tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri)))
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #endregion

        #region Báo cáo tiến độ dự án chuẩn bị đầu tư  --- Báo cáo ủy ban Phụ lục 1
        #region Cây dự án
        
        public List<object> GetListTD_DA_CBDT_CDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lBaoCao = from item in DbContext.DuAns
                          join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                          join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                          where item.IdGiaiDoanDuAn == 1 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString()) && ctcv.PDDA_BCKTKT_GiaTri > 0
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              OrderLinhVuc = item.LinhVucNganhNghe.Order,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              OrderDuAn = item.Order,
                              OrderTemp = item.OrderTemp,
                              TMDT = ctcv.PDDA_BCKTKT_GiaTri,
                              PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD,
                              PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD,
                              TMDT_ChuaPD = item.TongMucDauTu,
                              DiaDiem = item.DiaDiem
                          };

            var lst = (from item in lBaoCao
                       group item by new { item.IDLinhVucNganhNghe, item.LinhVucNganhNghe, item.OrderLinhVuc }
                      into item1
                       select new
                       {
                           IDLinhVucNganhNghe = item1.Key.IDLinhVucNganhNghe,
                           LinhVucNganhNghe = item1.Key.LinhVucNganhNghe,
                           OrderLinhVuc = item1.Key.OrderLinhVuc,
                           grpTTDA = item1.Where(x => x.IDLinhVucNganhNghe == item1.Key.IDLinhVucNganhNghe).GroupBy(x => new { x.IDTinhTrangDuAn, x.TinhTrangDuAn }).Select(x => new
                           {
                               IDTinhTrangDuAn = x.Key.IDTinhTrangDuAn,
                               TinhTrangDuAn = x.Key.TinhTrangDuAn,
                               grpDuAn = item1.Where(xx => xx.IDTinhTrangDuAn == x.Key.IDTinhTrangDuAn).Select(z => new
                               {
                                   IDDA = z.IDDA,
                                   TenDuAn = z.TenDuAn,
                                   ThoiGianThucHien = z.ThoiGianThucHien,
                                   TenChuDauTu = z.TenChuDauTu,
                                   DiaDiem = z.DiaDiem,
                                   TMDT = z.TMDT,
                                   PDDA_BCKTKT_SoQD = z.PDDA_BCKTKT_SoQD,
                                   PDDA_BCKTKT_NgayPD = z.PDDA_BCKTKT_NgayPD,
                                   TMDT_ChuaPD = z.TMDT_ChuaPD,
                                   Order = z.OrderDuAn,
                                   OrderTemp = z.OrderTemp
                               }).Distinct().OrderBy(z => new { z.OrderTemp, z.Order }).ToList<object>()
                           }).ToList<object>()
                       }).OrderBy(x => x.OrderLinhVuc).ToList<object>();
            return lst.Distinct().ToList();
        }

        public List<object> GetListTD_DA_DAC(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lDuAnCon = (from item in DbContext.DuAns
                            where item.IdDuAnCha != null && arrDuAn.Contains(item.IdDuAnCha.ToString())
                            select new
                            {
                                IDDA = item.IdDuAn,
                                IDDACha = item.IdDuAnCha,
                                TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                                NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                                ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                                TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu
                            }).ToList<object>();
            return lDuAnCon;
        }
        #endregion

        #region Kế hoạch vốn
        public List<object> GetList_DA_CBDT_KHV(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            List<object> lst = (from item in DbContext.KeHoachVons
                                where arrDuAn.Contains(item.IdDuAn.ToString()) && item.IdKeHoachVonDieuChinh == null && item.NienDo == iNam
                                group item by new { item.IdDuAn, item.NienDo }
                                into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHV = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.IdKeHoachVon).FirstOrDefault(),
                                    GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
                                }).ToList<object>();

            return lst;
        }

        public List<object> GetList_DA_CBDT_KHVDC(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            List<object> lst = (from item in DbContext.KeHoachVons
                                where arrDuAn.Contains(item.IdDuAn.ToString()) && item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam && item.DeXuatDieuChinh == false
                                group item by new { item.IdDuAn, item.NienDo }
                                  into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHVDC = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => x.IdKeHoachVonDieuChinh).FirstOrDefault(),
                                    GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault() == null ? 0 : item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
                                }).ToList<object>();

            return lst;
        }

        public List<object> GetList_DA_KHV_DXDC(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            List<object> lst = (from item in DbContext.KeHoachVons
                                where arrDuAn.Contains(item.IdDuAn.ToString()) && item.IdKeHoachVonDieuChinh != null && item.NienDo == iNam && item.DeXuatDieuChinh == true
                                group item by new { item.IdDuAn, item.IdKeHoachVonDieuChinh, item.NienDo }
                                  into item1
                                select new
                                {
                                    IDDA = item1.Key.IdDuAn,
                                    IDKHVDC = item1.Key.IdKeHoachVonDieuChinh,
                                    GiaTri = item1.OrderByDescending(x => x.NgayPheDuyet).Select(x => (double?)x.TongGiaTri).FirstOrDefault()
                                }).ToList<object>();

            return lst;
        }
        #endregion

        #region Giải ngân
        public List<object> GetList_DA_CBDT_TTHD(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lstThanhToanHD = (from tthd in DbContext.DuAns
                                  where arrDuAn.Contains(tthd.IdDuAn.ToString())
                                  select new
                                  {
                                      IDDA = tthd.IdDuAn,
                                      GiaTriTTHD = tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri))) == null ? 0 : tthd.HopDongs.Where(x => x.ThanhToanHopDongs.Any(xx => xx.ThoiDiemThanhToan.Value.Year == iNam)).Sum(x => x.ThanhToanHopDongs.Where(y => y.ThoiDiemThanhToan.Value.Year == iNam).Sum(xx => xx.NguonVonGoiThauThanhToans.Sum(xxx => (double?)xxx.GiaTri)))
                                  }).ToList<object>();
            return lstThanhToanHD;
        }
        public List<object> GetList_DA_CBDT_TTCPK(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lstThanhToanCPK = (from ttcpk in DbContext.ThanhToanChiPhiKhacs
                                   where arrDuAn.Contains(ttcpk.IdDuAn.ToString()) && ttcpk.NgayThanhToan.Value.Year == iNam
                                   group ttcpk by new { ttcpk.IdDuAn }
                                      into ttcpk1
                                   select new
                                   {
                                       IDDA = ttcpk1.Key.IdDuAn,
                                       GiaTriCPK = ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri)) == null ? 0 : ttcpk1.Sum(x => x.NguonVonThanhToanChiPhiKhacs.Sum(y => (double?)y.GiaTri))
                                   }).ToList<object>();

            return lstThanhToanCPK;
        }
        #endregion

        #region Hoàn thành trình duyệt
        public List<object> GetList_DA_CBDT_TDDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           TrangThaiTrinh = item1.Where(x => x.IdCongViec == 27).OrderByDescending(x => x.IdKeHoach).Select(x => x.HoanThanh).FirstOrDefault()
                       }).ToList<object>();

            return lst;
        }
        #endregion

        #region Hoàn thành Lập chủ trương đầu tư
        public List<object> GetList_DA_CBDT_HT_LCTDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                       group item by new { item.QLTD_KeHoach.IdDuAn }
                      into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           TrangThaiTrinh = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.HoanThanh).FirstOrDefault(),
                           KhoKhanVuongMac = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KhoKhanVuongMac).FirstOrDefault().ToString(),
                           KienNghi = item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString() == null ? "" : item1.Where(x => x.IdCongViec == 11).OrderByDescending(x => x.IdKeHoach).Select(x => x.LCTDT_KienNghiDeXuat).FirstOrDefault().ToString(),
                       }).ToList<object>();

            return lst;
        }
        #endregion

        #endregion

        #region Tiến độ thực hiện
        #region Tiến độ lập chủ trương đầu tư
        public List<object> GetList_TDTH_LCTDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_TienDoThucHien.OrderByDescending(x => x.QLTD_CTCV_ChuTruongDauTu.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_CTCV_ChuTruongDauTu.QLTD_KeHoach.IdDuAn, item.QLTD_CTCV_ChuTruongDauTu.IdKeHoach }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.Key.IdKeHoach,
                            grpTD_LCTDT = item1.Select(x => new
                            {
                                TD_LCTDT = x.NoiDung
                            }).ToList<object>()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus.OrderByDescending(x => x.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpTD_LCTDT = (List<object>)GetValueObject(item, "grpTD_LCTDT")
                       }).ToList<object>();
            return lst;
        }
        #endregion

        #region Tiến độ chuẩn bị đầu tư
        public List<object> GetList_TDTH_CBDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_TDTH_ChuanBiDauTu.OrderByDescending(x => x.QLTD_CTCV_ChuanBiDauTu.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_CTCV_ChuanBiDauTu.QLTD_KeHoach.IdDuAn, item.QLTD_CTCV_ChuanBiDauTu.IdKeHoach }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.Key.IdKeHoach,
                            grpTD_CBDT = item1.Select(x => new
                            {
                                TD_CBDT = x.NoiDung
                            }).ToList<object>()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus.OrderByDescending(x => x.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpTD_CBDT = (List<object>)GetValueObject(item, "grpTD_CBDT")
                       }).ToList<object>();
            return lst;
        }
        #endregion

        #region Tiến độ chuẩn bị thực hiện
        public List<object> GetList_TDTH_CBTH(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_TDTH_ChuanBiThucHien.OrderByDescending(x => x.QLTD_CTCV_ChuanBiThucHien.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_CTCV_ChuanBiThucHien.QLTD_KeHoach.IdDuAn, item.QLTD_CTCV_ChuanBiThucHien.IdKeHoach }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.Key.IdKeHoach,
                            grpTD_CBTH = item1.Select(x => new
                            {
                                TD_CBTH = x.NoiDung
                            }).ToList<object>()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens.OrderByDescending(x => x.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpTD_CBTH = (List<object>)GetValueObject(item, "grpTD_CBTH")
                       }).ToList<object>();
            return lst;
        }
        #endregion

        #region Tiến độ thực hiện - DTCXD
        public List<object> GetList_TDTH_TH_DTCXD(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_TDTH_ThucHienDuAn.OrderByDescending(x => x.QLTD_CTCV_ThucHienDuAn.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn.ToString()) && item.QLTD_CTCV_ThucHienDuAn.IdCongViec == 37
                        group item by new { item.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn, item.QLTD_CTCV_ThucHienDuAn.IdKeHoach }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.Key.IdKeHoach,
                            grpTD_TH = item1.Select(x => new
                            {
                                TD_TH = x.NoiDung
                            }).ToList<object>()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_CTCV_ThucHienDuAns.OrderByDescending(x => x.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpTD_TH = (List<object>)GetValueObject(item, "grpTD_TH")
                       }).ToList<object>();
            return lst;
        }
        #endregion

        #region Tiến độ thực hiện - GPMB
        public List<object> GetList_TDTH_TH_GPMB(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst1 = (from item in DbContext.QLTD_TDTH_ThucHienDuAn.OrderByDescending(x => x.QLTD_CTCV_ThucHienDuAn.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn.ToString()) && item.QLTD_CTCV_ThucHienDuAn.IdCongViec == 34
                        group item by new { item.QLTD_CTCV_ThucHienDuAn.QLTD_KeHoach.IdDuAn, item.QLTD_CTCV_ThucHienDuAn.IdKeHoach }
                      into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.Key.IdKeHoach,
                            grpTD_TH = item1.Select(x => new
                            {
                                TD_TH = x.NoiDung
                            }).ToList<object>()
                        }).ToList<object>();

            var lst2 = (from item in DbContext.QLTD_CTCV_ThucHienDuAns.OrderByDescending(x => x.IdKeHoach)
                        where arrDuAn.Contains(item.QLTD_KeHoach.IdDuAn.ToString())
                        group item by new { item.QLTD_KeHoach.IdDuAn }
                       into item1
                        select new
                        {
                            IDDA = item1.Key.IdDuAn,
                            IDKH = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                        }).ToList<object>();

            var lst = (from item in lst1
                       join item1 in lst2
                       on (int)GetValueObject(item, "IDKH") equals (int)GetValueObject(item1, "IDKH")
                       select new
                       {
                           IDDA = (int)GetValueObject(item, "IDDA"),
                           grpTD_TH = (List<object>)GetValueObject(item, "grpTD_TH")
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #endregion

        #region Báo cáo tiến độ thực hiện dự án --- Báo cáo ủy ban Phụ lục 2
        #region Cây dự án
        public List<object> GetListTD_DA_TH_CDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lBaoCao = from item in DbContext.DuAns
                          where item.IdGiaiDoanDuAn > 1 && item.IdGiaiDoanDuAn < 4 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString())
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              OrderLinhVuc = item.LinhVucNganhNghe.Order,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              OrderDuAn = item.Order,
                              OrderTemp = item.OrderTemp
                          };

            var lst = (from item in lBaoCao
                       group item by new { item.IDLinhVucNganhNghe, item.LinhVucNganhNghe, item.OrderLinhVuc }
                      into item1
                       select new
                       {
                           IDLinhVucNganhNghe = item1.Key.IDLinhVucNganhNghe,
                           LinhVucNganhNghe = item1.Key.LinhVucNganhNghe,
                           OrderLinhVuc = item1.Key.OrderLinhVuc,
                           grpTTDA = item1.Where(x => x.IDLinhVucNganhNghe == item1.Key.IDLinhVucNganhNghe).GroupBy(x => new { x.IDTinhTrangDuAn, x.TinhTrangDuAn }).Select(x => new
                           {
                               IDTinhTrangDuAn = x.Key.IDTinhTrangDuAn,
                               TinhTrangDuAn = x.Key.TinhTrangDuAn,
                               grpDuAn = item1.Where(xx => xx.IDTinhTrangDuAn == x.Key.IDTinhTrangDuAn).Select(z => new
                               {
                                   IDDA = z.IDDA,
                                   TenDuAn = z.TenDuAn,
                                   ThoiGianThucHien = z.ThoiGianThucHien,
                                   NangLucThietKe = z.NangLucThietKe,
                                   TenChuDauTu = z.TenChuDauTu,
                                   Order = z.OrderDuAn,
                                   OrderTemp = z.OrderTemp
                               }).OrderBy(z => new { z.OrderTemp, z.Order }).ToList<object>()
                           }).ToList<object>()
                       }).OrderBy(x => x.OrderLinhVuc).ToList<object>();
            return lst;
        }
        #endregion

        #region Khối lượng thực hiện
        public List<object> GetLstKhoiLuongThucHien(string IDDuAns, int iNam)
        {
            string[] arrDuAn = IDDuAns.Split(',');

            var lst = (from item in DbContext.DuAns
                       where arrDuAn.Contains(item.IdDuAn.ToString())
                       group item by new { item.IdDuAn }
                                    into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,

                           KLTH = item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year == iNam)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam).Sum(z => (double?)z.KhoiLuong))) == null ? 0 : item1.Sum(x => x.HopDongs.Where(y => y.ThucHienHopDongs.Any(yyy => yyy.ThoiDiemBaoCao.Value.Year == iNam)).Sum(y => y.ThucHienHopDongs.Where(yy => yy.ThoiDiemBaoCao.Value.Year == iNam).Sum(z => (double?)z.KhoiLuong)))
                       }).ToList<object>();

            return lst;
        }

        #endregion
        #endregion

        #region Cây dự án
        #region Quyết toán dự án
        public List<object> GetList_QTDA_CDA(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            List<object> lstBaoCao;

            var lBaoCao = from item in DbContext.DuAns
                          join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                          into gr1
                          from keHoach in gr1.DefaultIfEmpty()
                          //join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                          join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on (keHoach == null ? 0 : keHoach.IdKeHoach) equals ctcv.IdKeHoach
                          into gr2
                          from ctcv in gr2.DefaultIfEmpty()
                          where item.IdGiaiDoanDuAn == 4 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString())// && ctcv.PDDA_BCKTKT_GiaTri > 0
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              OrderLinhVuc = item.LinhVucNganhNghe.Order,
                              IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
                              GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
                              OrderDuAn = item.Order,
                              OrderTemp = item.OrderTemp,
                              DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              MaSoDuAn = item.Ma == null ? "" : item.Ma,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              TMDT = ctcv.PDDA_BCKTKT_GiaTri,
                              PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD,
                              PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD,
                              TMDT_ChuaPD = item.TongMucDauTu
                          };

            //var a = lBaoCao.ToList<object>();

            lstBaoCao = (from item in lBaoCao
                         group item by new { item.IDGiaiDoanDuAn, item.GiaiDoanDuAn }
                           into item1
                         select new
                         {
                             IDGiaiDoanDuAn = item1.Key.IDGiaiDoanDuAn,
                             GiaiDoanDuAn = item1.Key.GiaiDoanDuAn,
                             grpLVNN = item1.GroupBy(x => new { x.IDLinhVucNganhNghe, x.LinhVucNganhNghe, x.OrderLinhVuc }).Select(x => new
                             {
                                 IDLinhVucNganhNghe = x.Key.IDLinhVucNganhNghe,
                                 TenLinhVucNganhNghe = x.Key.LinhVucNganhNghe,
                                 OrderLinhVuc = x.Key.OrderLinhVuc,
                                 grpTTDA = item1.Where(xx => xx.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).GroupBy(y => new { y.IDTinhTrangDuAn, y.TinhTrangDuAn }).Select(y => new
                                 {
                                     IDTinhTrangDuAn = y.Key.IDTinhTrangDuAn,
                                     TinhTrangDuAn = y.Key.TinhTrangDuAn,
                                     grpDuAn = item1.Where(yy => yy.IDTinhTrangDuAn == y.Key.IDTinhTrangDuAn && yy.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).Select(z => new
                                     {
                                         IDDA = z.IDDA,
                                         TenDuAn = z.TenDuAn,
                                         NhomDuAn = z.NhomDuAn,
                                         DiaDiemXayDung = z.DiaDiemXayDung,
                                         NangLucThietKe = z.NangLucThietKe,
                                         ThoiGianThucHien = z.ThoiGianThucHien,
                                         MaSoDuAn = z.MaSoDuAn,
                                         TenChuDauTu = z.TenChuDauTu,
                                         TMDT = z.TMDT,
                                         PDDA_BCKTKT_SoQD = z.PDDA_BCKTKT_SoQD,
                                         PDDA_BCKTKT_NgayPD = z.PDDA_BCKTKT_NgayPD,
                                         TMDT_ChuaPD = z.TMDT_ChuaPD,
                                         OrderDuAn = z.OrderDuAn,
                                         OrderTemp = z.OrderTemp
                                     }).Distinct().OrderBy(z => new { z.OrderTemp, z.OrderDuAn }).ToList<object>()
                                 }).ToList<object>()
                             }).OrderBy(x => x.OrderLinhVuc).ToList<object>()
                         }).ToList<object>();

            return lstBaoCao.Distinct().ToList();
        }
        #endregion
        #region Thực hiện đầu tư
        public List<object> GetList_CDA_THDT(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            List<object> lstBaoCao;

            var lBaoCao = from item in DbContext.DuAns
                          join keHoach in DbContext.QLTD_KeHoachs on item.IdDuAn equals keHoach.IdDuAn
                          join ctcv in DbContext.QLTD_CTCV_ChuanBiDauTus on keHoach.IdKeHoach equals ctcv.IdKeHoach
                          where item.IdGiaiDoanDuAn < 4 && item.IdGiaiDoanDuAn > 1 && item.IdTinhTrangDuAn != null && item.IdLinhVucNganhNghe != null
                          && item.IdDuAnCha == null && arrDuAn.Contains(item.IdDuAn.ToString()) && ctcv.PDDA_BCKTKT_GiaTri > 0
                          select new
                          {
                              IDDA = item.IdDuAn,
                              IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
                              LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
                              OrderLinhVuc = item.LinhVucNganhNghe.Order,
                              IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
                              GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
                              IDTinhTrangDuAn = item.IdTinhTrangDuAn,
                              TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
                              TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
                              NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
                              OrderDuAn = item.Order,
                              OrderTemp = item.OrderTemp,
                              DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
                              NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
                              ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
                              MaSoDuAn = item.Ma == null ? "" : item.Ma,
                              TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu,
                              TMDT = ctcv.PDDA_BCKTKT_GiaTri,
                              PDDA_BCKTKT_SoQD = ctcv.PDDA_BCKTKT_SoQD,
                              PDDA_BCKTKT_NgayPD = ctcv.PDDA_BCKTKT_NgayPD,
                              TMDT_ChuaPD = item.TongMucDauTu
                          };

            var a = lBaoCao.ToList<object>();

            lstBaoCao = (from item in lBaoCao
                         group item by new { item.IDGiaiDoanDuAn, item.GiaiDoanDuAn }
                           into item1
                         select new
                         {
                             IDGiaiDoanDuAn = item1.Key.IDGiaiDoanDuAn,
                             GiaiDoanDuAn = item1.Key.GiaiDoanDuAn,
                             grpLVNN = item1.GroupBy(x => new { x.IDLinhVucNganhNghe, x.LinhVucNganhNghe, x.OrderLinhVuc }).Select(x => new
                             {
                                 IDLinhVucNganhNghe = x.Key.IDLinhVucNganhNghe,
                                 TenLinhVucNganhNghe = x.Key.LinhVucNganhNghe,
                                 OrderLinhVuc = x.Key.OrderLinhVuc,
                                 grpTTDA = item1.Where(xx => xx.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).GroupBy(y => new { y.IDTinhTrangDuAn, y.TinhTrangDuAn }).Select(y => new
                                 {
                                     IDTinhTrangDuAn = y.Key.IDTinhTrangDuAn,
                                     TinhTrangDuAn = y.Key.TinhTrangDuAn,
                                     grpDuAn = item1.Where(yy => yy.IDTinhTrangDuAn == y.Key.IDTinhTrangDuAn && yy.IDLinhVucNganhNghe == x.Key.IDLinhVucNganhNghe).Select(z => new
                                     {
                                         IDDA = z.IDDA,
                                         TenDuAn = z.TenDuAn,
                                         NhomDuAn = z.NhomDuAn,
                                         DiaDiemXayDung = z.DiaDiemXayDung,
                                         NangLucThietKe = z.NangLucThietKe,
                                         ThoiGianThucHien = z.ThoiGianThucHien,
                                         MaSoDuAn = z.MaSoDuAn,
                                         TenChuDauTu = z.TenChuDauTu,
                                         TMDT = z.TMDT,
                                         PDDA_BCKTKT_SoQD = z.PDDA_BCKTKT_SoQD,
                                         PDDA_BCKTKT_NgayPD = z.PDDA_BCKTKT_NgayPD,
                                         TMDT_ChuaPD = z.TMDT_ChuaPD,
                                         OrderDuAn = z.OrderDuAn,
                                         OrderTemp = z.OrderTemp
                                     }).Distinct().OrderBy(z => new { z.OrderTemp, z.OrderDuAn }).ToList<object>()
                                 }).ToList<object>()
                             }).OrderBy(x => x.OrderLinhVuc).ToList<object>()
                         }).ToList<object>();

            return lstBaoCao.Distinct().ToList();
        }
        #endregion
        #endregion

        #region  Dung chung
        public object GetValueObject(object obj, string property)
        {
            System.Type typeMethod = obj.GetType();
            var objMethod = obj;
            typeMethod = obj.GetType();
            var objValue = typeMethod.GetProperty(property).GetValue(objMethod, null);
            return objValue;
        }

        #endregion

        #region Điều hành
        #region Chủ trương đầu tư
        public List<object> GetList_DH_TDDA_CTDT_CDA(string idUser, string filter)
        {
            var now = DateTime.Now.Date;
            List<object> lst = new List<object>();

            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where item.QLTD_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()) 
                       && item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 2
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 2
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            return lst;


        }
        #endregion
        #region Chuẩn bị đầu tư
        public List<object> GetList_DH_TDDA_CBDT_CDA(string idUser, string filter)
        {
            var now = DateTime.Now.Date;
            List<object> lst = new List<object>();
            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where item.QLTD_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()) && 
                       item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 3
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where item.QLTD_KeHoach.DuAn.DuAnUsers
                       .Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null 
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 3
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            return lst;
        }
        #endregion
        #region Chuẩn bị thực hiện
        public List<object> GetList_DH_TDDA_CBTH_CDA(string idUser, string filter)
        {
            var now = DateTime.Now.Date;
            List<object> lst = new List<object>();
            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                       where item.QLTD_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()) 
                       && item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null 
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 4
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null 
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 4
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            return lst;

        }
        #endregion
        #region Thực hiện
        public List<object> GetList_DH_TDDA_TH_CDA(string idUser, string filter)
        {
            var now = DateTime.Now.Date;
            List<object> lst = new List<object>();

            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where item.QLTD_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower())
                       && item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null 
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 5
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null 
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 5
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }

            return lst;

        }
        #endregion
        #region Quyết toán
        public List<object> GetList_DH_TDDA_QT_CDA(string idUser, string filter)
        {
            var now = DateTime.Now.Date;
            List<object> lst = new List<object>();

            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where item.QLTD_KeHoach.DuAn.TenDuAn.ToLower().Contains(filter.ToLower()) 
                       && item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 6
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 
                       && item.QLTD_KeHoach.DuAn.IdLinhVucNganhNghe != null
                       && item.QLTD_KeHoach.DuAn.IdTinhTrangDuAn != null 
                       && item.QLTD_KeHoach.QLTD_GiaiDoan.IdGiaiDoan == 6
                       && item.HoanThanh == 0
                       && ((now > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh == null) || (item.NgayHoanThanh > item.QLTD_KeHoachCongViec.DenNgay && item.NgayHoanThanh != null))
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           TuNgay = item.QLTD_KeHoachCongViec.TuNgay,
                           DenNgay = item.QLTD_KeHoachCongViec.DenNgay,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId)
                       }).ToList<object>();
            }
            return lst;
        }
        #endregion

        #region Kế hoạch mới nhất
        public List<object> GetList_DH_TDDA_KHMN(string idUser)
        {
            var lst = (from item in DbContext.QLTD_KeHoachs
                       where item.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
                       group item by new { item.IdDuAn, item.IdGiaiDoan }
                     into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           IDGiaiDoan = item1.Key.IdGiaiDoan,
                           IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #endregion

        #region Kế hoạch mới nhất, trạng thái hoàn thành dưới mức 4
        public List<object> KHMN_DGDPD(string idUser, string filter)
        {
            List<object> lst = new List<object>();
            if (filter != null)
            {
                lst = (from item in DbContext.QLTD_KeHoachs
                       where item.TinhTrang < 4 && item.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
                       && item.DuAn.TenDuAn.ToLower().Contains(filter.ToLower())
                       select new
                       {
                           IDGiaiDoan = item.QLTD_GiaiDoan.IdGiaiDoan,
                           IDKH = item.IdKeHoach,
                           KH = item.NoiDung,
                           IDDA = item.IdDuAn,
                           DA = item.DuAn.TenDuAn,
                           TinhTrang = item.TinhTrang,
                           TrangThai = item.TrangThai
                       }).ToList<object>();
            }
            else
            {
                lst = (from item in DbContext.QLTD_KeHoachs
                       where item.TinhTrang < 4 && item.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
                       select new
                       {
                           IDGiaiDoan = item.QLTD_GiaiDoan.IdGiaiDoan,
                           IDKH = item.IdKeHoach,
                           KH = item.NoiDung,
                           IDDA = item.IdDuAn,
                           DA = item.DuAn.TenDuAn,
                           TinhTrang = item.TinhTrang,
                           TrangThai = item.TrangThai
                       }).ToList<object>();
            }
            return lst;
        }

        public List<object> KHMN(string idUser)
        {
            var lst = (from item in DbContext.QLTD_KeHoachs
                       where item.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0
                       group item by new { item.IdDuAn }
                     into item1
                       select new
                       {
                           IDDA = item1.Key.IdDuAn,
                           IDKeHoach = item1.OrderByDescending(x => x.IdKeHoach).Select(x => x.IdKeHoach).FirstOrDefault()
                       }).ToList<object>();
            return lst;
        }

        #endregion

        #region Công việc chưa hoàn thành - điều hành
        #region Chủ trương đầu tư
        public List<object> GetLst_CV_CHT_CTDT(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_ChuTruongDauTus
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId).Distinct()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #region Chuẩn bị đầu tư
        public List<object> GetLst_CV_CHT_CBDT(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiDauTus
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId).Distinct()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #region Chuẩn bị thực hiện
        public List<object> GetLst_CV_CHT_CBTH(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_ChuanBiThucHiens
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId).Distinct()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #region Thực hiện
        public List<object> GetLst_CV_CHT_TH(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId).Distinct()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        public List<object> GetLst_CVChuyenQT(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_ThucHienDuAns
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1 && item.IdCongViec == 44
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec
                       }).ToList<object>();
            return lst;
        }
        
        #region Quyết toán
        public List<object> GetLst_CV_CHT_QT(string idUser)
        {
            var lst = (from item in DbContext.QLTD_CTCV_QuyetToanDuAns
                       where item.QLTD_KeHoach.DuAn.DuAnUsers.Where(y => y.UserId == idUser).Count() > 0 && item.HoanThanh == 1
                       select new
                       {
                           IDGiaiDoan = item.QLTD_KeHoach.IdGiaiDoan,
                           GiaiDoan = item.QLTD_KeHoach.QLTD_GiaiDoan.TenGiaiDoan,
                           IDDA = item.QLTD_KeHoach.IdDuAn,
                           TenDuAn = item.QLTD_KeHoach.DuAn.TenDuAn,
                           IdNhomDuAnTheoUser = item.QLTD_KeHoach.DuAn.IdNhomDuAnTheoUser,
                           OrderDuAn = item.QLTD_KeHoach.DuAn.Order,
                           IDKH = item.IdKeHoach,
                           IDKHCV = item.QLTD_KeHoachCongViec.IdKeHoachCongViec,
                           CV = item.QLTD_KeHoachCongViec.QLTD_CongViec.TenCongViec,
                           HoanThanh = item.HoanThanh,
                           IDCTCV = item.IdChiTietCongViec,
                           DuAnUsers = item.QLTD_KeHoach.DuAn.DuAnUsers.Select(u => u.UserId).Distinct()
                       }).ToList<object>();
            return lst;
        }
        #endregion
        #endregion


    }
}
