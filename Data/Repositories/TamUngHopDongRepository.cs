﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface ITamUngHopDongRepository : IRepository<TamUngHopDong>
    {
        IEnumerable<HopDong> GetTamUngHopDongs(int idDuAn, string filter);
    }

    public class TamUngHopDongRepository : RepositoryBase<TamUngHopDong>, ITamUngHopDongRepository
    {
        public TamUngHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<HopDong> GetTamUngHopDongs(int idDuAn, string filter)
        {
            IEnumerable<HopDong> query;
            if (filter == null)
            {
                query = from hopdong in DbContext.HopDongs.Include("TamUngHopDongs")
                        where hopdong.TamUngHopDongs.Count > 0 && hopdong.IdDuAn == idDuAn
                        select hopdong;
            }
            else
            {
                query = from hopdong in DbContext.HopDongs.IncludeFilter(x =>
                        x.TamUngHopDongs.Where(y => y.NoiDung.Contains(filter)))
                        where hopdong.TamUngHopDongs.Count > 0 &&
                              hopdong.IdDuAn == idDuAn &&
                              hopdong.TamUngHopDongs.Any(a => a.NoiDung.Contains(filter))
                        select hopdong;
            }
            return query;
        }
    }
}
