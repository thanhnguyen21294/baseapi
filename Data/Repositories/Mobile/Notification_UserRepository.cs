﻿using Data.Infrastructure;
using Model.Models.Mobile;

namespace Data.Repositories.Mobile
{
    public interface INotification_UserRepository : IRepository<Notification_User>
    {
    }

    public class Notification_UserRepository : RepositoryBase<Notification_User>, INotification_UserRepository
    {
        public Notification_UserRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }
}