﻿using Data.Infrastructure;
using Model.Models.Mobile;

namespace Data.Repositories.Mobile
{
    public interface INotification_GroupRepository : IRepository<Notification_Group>
    {
       
    }

    public class Notification_GroupRepository : RepositoryBase<Notification_Group>, INotification_GroupRepository
    {
        public Notification_GroupRepository(IDbFactory dbFactory): base(dbFactory)
        {

        }
    }
}
