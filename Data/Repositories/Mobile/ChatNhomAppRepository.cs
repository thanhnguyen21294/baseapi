﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IChatNhomAppRepository : IRepository<ChatNhomApp>
    {
        IEnumerable<ChatNhomApp> GetChatNhomApp();
       
    }

    public class ChatNhomAppRepository : RepositoryBase<ChatNhomApp>, IChatNhomAppRepository
    {
        public ChatNhomAppRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ChatNhomApp> GetChatNhomApp()
        {
            var query = from kh in DbContext.ChatNhomApps
                        select kh;
            return query;
        }
    
    }
}