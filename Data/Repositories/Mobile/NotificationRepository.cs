﻿using Data.Infrastructure;
using Model.Models.Mobile;

namespace Data.Repositories.Mobile
{
    public interface INotificationRepository: IRepository<Notification>
    {

    }

    public class NotificationRepository : RepositoryBase<Notification>,  INotificationRepository
    {
        public NotificationRepository(IDbFactory dbFactory): base(dbFactory)
        {

        }
    }
}
