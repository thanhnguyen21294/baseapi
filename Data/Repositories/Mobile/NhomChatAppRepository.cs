﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INhomChatAppRepository : IRepository<NhomChatApp>
    {
        IEnumerable<NhomChatApp> GetNhomChatApp();
       
    }

    public class NhomChatAppRepository : RepositoryBase<NhomChatApp>, INhomChatAppRepository
    {
        public NhomChatAppRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<NhomChatApp> GetNhomChatApp()
        {
            var query = from kh in DbContext.NhomChatApps
                        select kh;
            return query;
        }
    
    }
}