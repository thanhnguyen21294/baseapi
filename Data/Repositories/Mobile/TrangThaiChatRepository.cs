﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ITrangThaiChatRepository : IRepository<TrangThaiChat>
    {
        IEnumerable<TrangThaiChat> GetTrangThaiChat();
       
    }

    public class TrangThaiChatRepository : RepositoryBase<TrangThaiChat>, ITrangThaiChatRepository
    {
        public TrangThaiChatRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<TrangThaiChat> GetTrangThaiChat()
        {
            var query = from kh in DbContext.TrangThaiChats
                        select kh;
            return query;
        }
    }
}