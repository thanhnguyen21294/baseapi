﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IChatAppRepository : IRepository<ChatApp>
    {
        IEnumerable<ChatApp> GetChatApp();
       
    }

    public class ChatAppRepository : RepositoryBase<ChatApp>, IChatAppRepository
    {
        public ChatAppRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ChatApp> GetChatApp()
        {
            var query = from kh in DbContext.ChatApps
                        select kh;
            return query;
        }
    
    }
}