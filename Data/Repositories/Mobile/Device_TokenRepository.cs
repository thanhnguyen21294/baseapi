﻿using Data.Infrastructure;
using Model.Models.Mobile;

namespace Data.Repositories.Mobile
{
    public interface IDevice_TokenRepository : IRepository<Device_Token>
    {
    }

    public class Device_TokenRepository : RepositoryBase<Device_Token>, IDevice_TokenRepository
    {
        public Device_TokenRepository(IDbFactory dbFactory) : base(dbFactory) { }
    }
}
