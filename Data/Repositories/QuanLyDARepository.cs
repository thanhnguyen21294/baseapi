﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IQuanLyDARepository : IRepository<QuanLyDA>
    {
    }

    public class QuanLyDARepository : RepositoryBase<QuanLyDA>, IQuanLyDARepository
    {
        public QuanLyDARepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
