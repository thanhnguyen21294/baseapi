﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IChuDauTuRepository: IRepository<ChuDauTu>
    {
        IEnumerable<ChuTruongDauTu> GetChuTruongDauTu();
    }

    public class ChuDauTuRepository : RepositoryBase<ChuDauTu>, IChuDauTuRepository
    {
        public ChuDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<ChuTruongDauTu> GetChuTruongDauTu()
        {
            var query = from kh in DbContext.ChuTruongDauTus.Include("CoQuanPheDuyet")
                        select kh;
            return query;
        }
    }
}
