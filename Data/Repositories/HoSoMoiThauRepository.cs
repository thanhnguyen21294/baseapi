﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;
using System.Data.Entity;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IHoSoMoiThauRepository : IRepository<HoSoMoiThau>
    {
        IEnumerable<GoiThau> GetGoiThaus(int idDuAn, string filter);
    }

    public class HoSoMoiThauRepository : RepositoryBase<HoSoMoiThau>, IHoSoMoiThauRepository
    {
        public HoSoMoiThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<GoiThau> GetGoiThaus(int idDuAn, string filter)
        {
            IEnumerable<GoiThau> query;
            if (filter != null)
            {
                query = from goiThau in DbContext.GoiThaus.IncludeFilter(x =>
                        x.HoSoMoiThaus.Where(y=>y.NoiDung.Contains(filter)))
                    where goiThau.IdDuAn == idDuAn && goiThau.HoSoMoiThaus.Count > 0 && goiThau.HoSoMoiThaus.Any(a=>a.NoiDung.Contains(filter))
                    select goiThau;
            }
            else
            {
                query = from goiThau in DbContext.GoiThaus.Include("HoSoMoiThaus")
                    where goiThau.IdDuAn == idDuAn && goiThau.HoSoMoiThaus.Count > 0
                    //join hoSoMoiThau in queryHoSo on goiThau.IdGoiThau equals hoSoMoiThau.IdGoiThau
                    select goiThau;
            }
            return query;
        }
    }
}
