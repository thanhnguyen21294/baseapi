﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IPhieuXuLyCongViecNoiBoUserRepository : IRepository<PhieuXuLyCongViecNoiBoUser>
    {
    }

    public class PhieuXuLyCongViecNoiBoUserRepository : RepositoryBase<PhieuXuLyCongViecNoiBoUser>, IPhieuXuLyCongViecNoiBoUserRepository
    {
        public PhieuXuLyCongViecNoiBoUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
