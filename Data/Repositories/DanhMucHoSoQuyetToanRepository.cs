﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IDanhMucHoSoQuyetToanRepository : IRepository<DanhMucHoSoQuyetToan>
    {
    }
    public class DanhMucHoSoQuyetToanRepository : RepositoryBase<DanhMucHoSoQuyetToan>, IDanhMucHoSoQuyetToanRepository
    {
        public DanhMucHoSoQuyetToanRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
