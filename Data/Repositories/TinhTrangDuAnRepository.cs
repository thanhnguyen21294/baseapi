﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ITinhTrangDuAnRepository: IRepository<TinhTrangDuAn>
    {
    }

    public class TinhTrangDuAnRepository : RepositoryBase<TinhTrangDuAn>, ITinhTrangDuAnRepository
    {
        public TinhTrangDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
