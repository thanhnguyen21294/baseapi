﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface ICamKetGiaiNganRepository : IRepository<CamKetGiaiNgan>
    {
    }

    public class CamKetGiaiNganRepository : RepositoryBase<CamKetGiaiNgan>, ICamKetGiaiNganRepository
    {
        public CamKetGiaiNganRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
