﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IQuyetToanHopDongRepository : IRepository<QuyetToanHopDong>
    {
        IEnumerable<HopDong> GetQuyetToanHopDongs(int idDuAn, string filter);
    }

    public class QuyetToanHopDongRepository : RepositoryBase<QuyetToanHopDong>, IQuyetToanHopDongRepository
    {
        public QuyetToanHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<HopDong> GetQuyetToanHopDongs(int idDuAn, string filter)
        {
            IEnumerable<HopDong> query;
            if (filter == null)
            {
                query = from hopdong in DbContext.HopDongs.Include("QuyetToanHopDongs")
                        where hopdong.QuyetToanHopDongs.Count > 0 && hopdong.IdDuAn == idDuAn
                        select hopdong;
            }
            else
            {
                query = from hopdong in DbContext.HopDongs.IncludeFilter(x =>
                        x.QuyetToanHopDongs.Where(y => y.NoiDung.Contains(filter)))
                        where hopdong.QuyetToanHopDongs.Count > 0 &&
                              hopdong.IdDuAn == idDuAn &&
                              hopdong.QuyetToanHopDongs.Any(a => a.NoiDung.Contains(filter))
                        select hopdong;
            }
            return query;
        }
    }
}
