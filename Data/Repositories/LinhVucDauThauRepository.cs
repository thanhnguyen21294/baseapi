﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ILinhVucDauThauRepository: IRepository<LinhVucDauThau>
    {
    }
    public class LinhVucDauThauRepository : RepositoryBase<LinhVucDauThau>, ILinhVucDauThauRepository
    {
        public LinhVucDauThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
