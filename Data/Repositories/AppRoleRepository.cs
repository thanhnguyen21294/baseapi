﻿using Common;
using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IAppRoleRepository : IRepository<AppRole>
    {
        IEnumerable<UserRole> GetUserRole();
        IEnumerable<UserRole> GetUserRoleNhanVien();
    }

    public class AppRoleRepository : RepositoryBase<AppRole>, IAppRoleRepository
    {
        public AppRoleRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
        public IEnumerable<UserRole> GetUserRole()
        {
            var role = (from r in DbContext.AppRoles
                        join ur in DbContext.UserRoles on r.Id equals ur.RoleId
                        select new UserRole
                        {
                            RoleId = ur.RoleId,
                            UserId = ur.UserId,
                            RoleName = r.Name
                        });
            return role;
        }

        public IEnumerable<UserRole> GetUserRoleNhanVien()
        {
            var role = (from r in DbContext.AppRoles
                        join ur in DbContext.UserRoles on r.Id equals ur.RoleId
                        join u in DbContext.Users on ur.UserId equals u.Id
                        where r.Name == "Nhân viên"
                        select new UserRole
                        {
                            UserId = ur.UserId,
                            FullName = u.FullName
                        });
            return role;
        }
    }
}
