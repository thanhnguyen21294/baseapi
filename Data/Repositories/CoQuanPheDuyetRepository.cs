﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ICoQuanPheDuyetRepository: IRepository<CoQuanPheDuyet>
    {
    }

    public class CoQuanPheDuyetRepository : RepositoryBase<CoQuanPheDuyet>, ICoQuanPheDuyetRepository
    {
        public CoQuanPheDuyetRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
