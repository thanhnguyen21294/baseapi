﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonKeHoachVonRepository : IRepository<NguonVonKeHoachVon>
    {
    }

    public class NguonVonKeHoachVonRepository : RepositoryBase<NguonVonKeHoachVon>, INguonVonKeHoachVonRepository
    {
        public NguonVonKeHoachVonRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
