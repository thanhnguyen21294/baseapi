﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface INguonVonDuAnGoiThauRepository : IRepository<NguonVonDuAnGoiThau>
    {
    }
    public class NguonVonDuAnGoiThauRepository : RepositoryBase<NguonVonDuAnGoiThau>, INguonVonDuAnGoiThauRepository
    {
        public NguonVonDuAnGoiThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
