﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INhomDuAnRepository: IRepository<NhomDuAn>
    {
    }
    public class NhomDuAnRepository : RepositoryBase<NhomDuAn>, INhomDuAnRepository
    {
        public NhomDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
