﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IQuyetToanDuAnRepository : IRepository<QuyetToanDuAn>
    {
        
    }

    public class QuyetToanDuAnRepository : RepositoryBase<QuyetToanDuAn>, IQuyetToanDuAnRepository
    {
        public QuyetToanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        
    }
}
