﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Data.Repositories
{
    public interface IVanBanDuAnRepository : IRepository<VanBanDuAn>
    {
        List<VanBanDuAn> GetVBDAByTM(int idTM,string filter);
        int totalVBDAByTM(int idTM);
    }
    public class VanBanDuAnRepository : RepositoryBase<VanBanDuAn>, IVanBanDuAnRepository
    {
     
     

        public VanBanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
         

        }

        public List<VanBanDuAn> GetVBDAByTM(int idTM, string filter)
        {
            
          
            List<VanBanDuAn> lstVBDA = new List<VanBanDuAn>();
            List<int> lst = new List<int>();
           
            GetAllIDThuMucCon(idTM, lst);
            if (filter != null)
                lstVBDA = DbContext.VanBanDuAns.Where(x => lst.Contains(x.IdThuMuc) && x.TenVanBan.Contains(filter)).ToList();
           
            else
                lstVBDA = DbContext.VanBanDuAns.Where(x => lst.Contains(x.IdThuMuc)).ToList();
            return lstVBDA;
        }
        public int totalVBDAByTM(int idTM)
        {
            List<VanBanDuAn> lstVBDA = new List<VanBanDuAn>();
            List<int> lst = new List<int>();
           
            GetAllIDThuMucCon(idTM, lst);
            lstVBDA = DbContext.VanBanDuAns.Where(x => lst.Contains(x.IdThuMuc)).ToList();
            return lstVBDA.Count;
        }
  
        
        public void  GetAllIDThuMucCon(int idTM,List<int> lstAllIdOfChildrenTM)
        {
            if(lstAllIdOfChildrenTM.Count==0)
            {
                lstAllIdOfChildrenTM.Add(idTM);
            }

            IEnumerable<int> lstIdOfChildrenTM=  DbContext.ThuMucs.Where(x => x.IdThuMucCha == idTM).Where(X => X.IdThuMucCha == idTM).Select(x => x.IdThuMuc);
            lstAllIdOfChildrenTM.AddRange(lstIdOfChildrenTM);
            foreach (var item in lstIdOfChildrenTM)
            {
                GetAllIDThuMucCon(item,lstAllIdOfChildrenTM);
            }

        }
    }  
}
