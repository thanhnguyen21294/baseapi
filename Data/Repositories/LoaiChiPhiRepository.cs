﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface ILoaiChiPhiRepository : IRepository<LoaiChiPhi>
    {
    }

    public class LoaiChiPhiRepository : RepositoryBase<LoaiChiPhi>, ILoaiChiPhiRepository
    {
        public LoaiChiPhiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
