﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ILapQuanLyDauTuRepository : IRepository<LapQuanLyDauTu>
    {
        IEnumerable<LapQuanLyDauTu> GetLapQuanLyDauTu();
    }

    public class LapQuanLyDauTuRepository : RepositoryBase<LapQuanLyDauTu>, ILapQuanLyDauTuRepository
    {
        public LapQuanLyDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<LapQuanLyDauTu> GetLapQuanLyDauTu()
        {
            var query = from kh in DbContext.LapQuanLyDauTus
                        .Include("CoQuanPheDuyet")
                        .Include("NguonVonDuAns")
                        select kh;
            return query;
        }
    }
}
