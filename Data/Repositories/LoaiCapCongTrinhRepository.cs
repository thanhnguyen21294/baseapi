﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface ILoaiCapCongTrinhRepository : IRepository<LoaiCapCongTrinh>
    {
    }

    public class LoaiCapCongTrinhRepository : RepositoryBase<LoaiCapCongTrinh>, ILoaiCapCongTrinhRepository
    {
        public LoaiCapCongTrinhRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
