﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IThucHienHopDongRepository : IRepository<ThucHienHopDong>
    {
        IEnumerable<HopDong> GetThucHienHopDongs(int idDuAn, string filter);
    }

    public class ThucHienHopDongRepository : RepositoryBase<ThucHienHopDong>, IThucHienHopDongRepository
    {
        public ThucHienHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<HopDong> GetThucHienHopDongs(int idDuAn, string filter)
        {
            IEnumerable<HopDong> query;
            if (filter == null)
            {
                query = from hopdong in DbContext.HopDongs.Include("ThucHienHopDongs").Include("ThucHienHopDongs.LoaiChiPhi")
                        where hopdong.ThucHienHopDongs.Count > 0 && hopdong.IdDuAn == idDuAn
                        select hopdong;
            }
            else
            {
                query = from hopdong in DbContext.HopDongs.IncludeFilter(x =>
                        x.ThucHienHopDongs.Where(y => y.NoiDung.Contains(filter)))
                        where hopdong.ThucHienHopDongs.Count > 0 &&
                              hopdong.IdDuAn == idDuAn &&
                              hopdong.ThucHienHopDongs.Any(a => a.NoiDung.Contains(filter))
                        select hopdong;
            }
            return query;
        }
    }
}
