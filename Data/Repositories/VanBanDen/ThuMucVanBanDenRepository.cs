﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IThuMucVanBanDenRepository : IRepository<ThuMucVanBanDen>
    {
    }

    public class ThuMucVanBanDenRepository : RepositoryBase<ThuMucVanBanDen>, IThuMucVanBanDenRepository
    {
        public ThuMucVanBanDenRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
