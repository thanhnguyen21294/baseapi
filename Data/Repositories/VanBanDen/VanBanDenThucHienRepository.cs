﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IVanBanDenThucHienRepository : IRepository<VanBanDenThucHien>
    {
    }

    public class VanBanDenThucHienRepository : RepositoryBase<VanBanDenThucHien>, IVanBanDenThucHienRepository
    {
        public VanBanDenThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
