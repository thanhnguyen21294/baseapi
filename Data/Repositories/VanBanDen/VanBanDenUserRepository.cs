﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IVanBanDenUserRepository : IRepository<VanBanDenUser>
    {
    }

    public class VanBanDenUserRepository : RepositoryBase<VanBanDenUser>, IVanBanDenUserRepository
    {
        public VanBanDenUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
