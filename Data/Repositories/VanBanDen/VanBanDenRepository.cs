﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IVanBanDenRepository : IRepository<VanBanDen>
    {
    }

    public class VanBanDenRepository : RepositoryBase<VanBanDen>, IVanBanDenRepository
    {
        public VanBanDenRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
