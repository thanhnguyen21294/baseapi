﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IDuAnUserRepository : IRepository<DuAnUser>
    {
        IEnumerable<DuAnUser> GetDuAnUser();
    }

    public class DuAnUserRepository : RepositoryBase<DuAnUser>, IDuAnUserRepository
    {
        public DuAnUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<DuAnUser> GetDuAnUser()
        {
            var query = from kh in DbContext.DuAnUsers
                        select kh;
            return query;
        }
    }
}