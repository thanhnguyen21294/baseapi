﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonRepository: IRepository<NguonVon>
    {
    }

    public class NguonVonRepository : RepositoryBase<NguonVon>, INguonVonRepository
    {
        public NguonVonRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
