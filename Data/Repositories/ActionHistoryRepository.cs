﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IActionHistoryRepository : IRepository<ActionHistory>
    {
    }

    public class ActionHistoryRepository : RepositoryBase<ActionHistory>, IActionHistoryRepository
    {
        public ActionHistoryRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}