﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface INhomDuAnTheoUserRepository : IRepository<NhomDuAnTheoUser>
    {

    }

    public class NhomDuAnTheoUserRepository : RepositoryBase<NhomDuAnTheoUser>, INhomDuAnTheoUserRepository
    {
        public NhomDuAnTheoUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}