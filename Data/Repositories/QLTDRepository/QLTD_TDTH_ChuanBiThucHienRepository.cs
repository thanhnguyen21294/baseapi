﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_TDTH_ChuanBiThucHienRepository : IRepository<QLTD_TDTH_ChuanBiThucHien>
    {
        IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetQLTD_TDTH_ChuanBiThucHien();
    }
    public class QLTD_TDTH_ChuanBiThucHienRepository : RepositoryBase<QLTD_TDTH_ChuanBiThucHien>, IQLTD_TDTH_ChuanBiThucHienRepository
    {
        public QLTD_TDTH_ChuanBiThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_TDTH_ChuanBiThucHien> GetQLTD_TDTH_ChuanBiThucHien()
        {
            var query = from kh in DbContext.QLTD_TDTH_ChuanBiThucHien
                        select kh;
            return query;
        }
    }
}
