﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CongViecRepository : IRepository<QLTD_CongViec>
    {
        IEnumerable<QLTD_CongViec> GetQLTD_CongViec();
    }
    public class QLTD_CongViecRepository : RepositoryBase<QLTD_CongViec>, IQLTD_CongViecRepository
    {
        public QLTD_CongViecRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CongViec> GetQLTD_CongViec()
        {
            var query = from kh in DbContext.QLTD_CongViecs
                        select kh;
            return query;
        }
    }
}
