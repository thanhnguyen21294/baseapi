﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_TienDoThucHienRepository : IRepository<QLTD_TDTH_ChuTruongDauTu>
    {
        IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetQLTD_TienDoThucHien();
    }
    public class QLTD_TienDoThucHienRepository : RepositoryBase<QLTD_TDTH_ChuTruongDauTu>, IQLTD_TienDoThucHienRepository
    {
        public QLTD_TienDoThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_TDTH_ChuTruongDauTu> GetQLTD_TienDoThucHien()
        {
            var query = from kh in DbContext.QLTD_TienDoThucHien
                        select kh;
            return query;
        }
    }
}
