﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CTCV_ThucHienDuAnRepository : IRepository<QLTD_CTCV_ThucHienDuAn>
    {
        IEnumerable<QLTD_CTCV_ThucHienDuAn> GetQLTD_CTCV_ThucHienDuAn();
    }
    public class QLTD_CTCV_ThucHienDuAnRepository : RepositoryBase<QLTD_CTCV_ThucHienDuAn>, IQLTD_CTCV_ThucHienDuAnRepository
    {
        public QLTD_CTCV_ThucHienDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CTCV_ThucHienDuAn> GetQLTD_CTCV_ThucHienDuAn()
        {
            var query = from kh in DbContext.QLTD_CTCV_ThucHienDuAns
                        select kh;
            return query;
        }
    }
}
