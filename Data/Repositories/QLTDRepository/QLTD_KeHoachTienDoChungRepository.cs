﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_KeHoachTienDoChungRepository : IRepository<QLTD_KeHoachTienDoChung>
    {
        IEnumerable<QLTD_KeHoachTienDoChung> GetQLTD_KeHoachTienDoChung();
    }
    public class QLTD_KeHoachTienDoChungRepository : RepositoryBase<QLTD_KeHoachTienDoChung>, IQLTD_KeHoachTienDoChungRepository
    {
        public QLTD_KeHoachTienDoChungRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_KeHoachTienDoChung> GetQLTD_KeHoachTienDoChung()
        {
            var query = from kh in DbContext.QLTD_KeHoachTienDoChung
                        select kh;
            return query;
        }
    }
}
