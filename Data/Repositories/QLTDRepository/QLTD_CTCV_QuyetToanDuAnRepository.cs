﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CTCV_QuyetToanDuAnRepository : IRepository<QLTD_CTCV_QuyetToanDuAn>
    {
        IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetQLTD_CTCV_QuyetToanDuAn();
    }
    public class QLTD_CTCV_QuyetToanDuAnRepository : RepositoryBase<QLTD_CTCV_QuyetToanDuAn>, IQLTD_CTCV_QuyetToanDuAnRepository
    {
        public QLTD_CTCV_QuyetToanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CTCV_QuyetToanDuAn> GetQLTD_CTCV_QuyetToanDuAn()
        {
            var query = from kh in DbContext.QLTD_CTCV_QuyetToanDuAns
                        select kh;
            return query;
        }
    }
}
