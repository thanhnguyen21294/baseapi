﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_KhoKhanVuongMacRepository : IRepository<QLTD_KhoKhanVuongMac>
    {
        IEnumerable<QLTD_KhoKhanVuongMac> GetQLTD_KhoKhanVuongMac();
    }
    public class QLTD_KhoKhanVuongMacRepository : RepositoryBase<QLTD_KhoKhanVuongMac>, IQLTD_KhoKhanVuongMacRepository
    {
        public QLTD_KhoKhanVuongMacRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_KhoKhanVuongMac> GetQLTD_KhoKhanVuongMac()
        {
            var query = from kh in DbContext.QLTD_KhoKhanVuongMac
                        select kh;
            return query;
        }
    }
}
