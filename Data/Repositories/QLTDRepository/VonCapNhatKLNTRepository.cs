﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IVonCapNhatKLNTRepository : IRepository<VonCapNhatKLNT>
    {
        IEnumerable<VonCapNhatKLNT> GetVonCapNhatKLNT();

        List<object> GetGiaiNganVon(string IDDuAns);

        List<object> GetKLTH(string IDDuAns);
    }

    public class VonCapNhatKLNTRepository : RepositoryBase<VonCapNhatKLNT>, IVonCapNhatKLNTRepository
    {
        public VonCapNhatKLNTRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<VonCapNhatKLNT> GetVonCapNhatKLNT()
        {
            var query = from capnhat in DbContext.VonCapNhatKLNTs
                        select capnhat;
            return query;
        }

        public List<object> GetGiaiNganVon(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var query = (from item in DbContext.VonCapNhatKLNTs
                         where (arrDuAn.Contains(item.IdDuAn.ToString()))
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             GiaiNganVonHuyen = item1.OrderByDescending(x => x.IdFile).Select(x => x.TDGNVonHuyen).FirstOrDefault() == "" ? "0" : item1.OrderByDescending(x => x.IdFile).Select(x => x.TDGNVonHuyen).FirstOrDefault(),
                             GiaiNganVonTP = item1.OrderByDescending(x => x.IdFile).Select(x => x.TDGNVonTP).FirstOrDefault() == "" ? "0" : item1.OrderByDescending(x => x.IdFile).Select(x => x.TDGNVonTP).FirstOrDefault()
                         }).ToList<object>();
            return query;
        }

        public List<object> GetKLTH(string IDDuAns)
        {
            string[] arrDuAn = IDDuAns.Split(',');
            var query = (from item in DbContext.VonCapNhatKLNTs
                         where (arrDuAn.Contains(item.IdDuAn.ToString()))
                         group item by new { item.IdDuAn }
                        into item1
                         select new
                         {
                             IDDA = item1.Key.IdDuAn,
                             KLTHVonHuyen = item1.OrderByDescending(x => x.IdFile).Select(x => x.KLNTVonHuyen).FirstOrDefault() == "" ? "0" : item1.OrderByDescending(x => x.IdFile).Select(x => x.KLNTVonHuyen).FirstOrDefault(),
                             KLTHVonTP = item1.OrderByDescending(x => x.IdFile).Select(x => x.KLNTVonTP).FirstOrDefault() == "" ? "0" : item1.OrderByDescending(x => x.IdFile).Select(x => x.KLNTVonTP).FirstOrDefault()
                         }).ToList<object>();
            return query;
        }

    }
}
