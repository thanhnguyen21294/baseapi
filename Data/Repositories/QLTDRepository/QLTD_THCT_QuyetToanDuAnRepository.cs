﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_THCT_QuyetToanDuAnRepository : IRepository<QLTD_THCT_QuyetToanDuAn>
    {
        IEnumerable<QLTD_THCT_QuyetToanDuAn> GetQLTD_THCT_QuyetToanDuAn();
    }
    public class QLTD_THCT_QuyetToanDuAnRepository : RepositoryBase<QLTD_THCT_QuyetToanDuAn>, IQLTD_THCT_QuyetToanDuAnRepository
    {
        public QLTD_THCT_QuyetToanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_THCT_QuyetToanDuAn> GetQLTD_THCT_QuyetToanDuAn()
        {
            var query = from kh in DbContext.QLTD_THCT_QuyetToanDuAn
                        select kh;
            return query;
        }
    }
}
