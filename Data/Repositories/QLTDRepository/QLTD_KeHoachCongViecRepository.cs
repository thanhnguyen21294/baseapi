﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_KeHoachCongViecRepository : IRepository<QLTD_KeHoachCongViec>
    {
        IEnumerable<QLTD_KeHoachCongViec> GetQLTD_KeHoachCongViec();
    }
    public class QLTD_KeHoachCongViecRepository : RepositoryBase<QLTD_KeHoachCongViec>, IQLTD_KeHoachCongViecRepository
    {
        public QLTD_KeHoachCongViecRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_KeHoachCongViec> GetQLTD_KeHoachCongViec()
        {
            var query = from kh in DbContext.QLTD_KeHoachCongViecs.Include("QLTD_CongViec")
                        select kh;
            return query;
        }
    }
}
