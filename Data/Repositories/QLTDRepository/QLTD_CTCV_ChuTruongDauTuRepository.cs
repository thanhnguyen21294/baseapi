﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CTCV_ChuTruongDauTuRepository : IRepository<QLTD_CTCV_ChuTruongDauTu>
    {
        IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetQLTD_CTCV_ChuTruongDauTu();
    }
    public class QLTD_CTCV_ChuTruongDauTuRepository : RepositoryBase<QLTD_CTCV_ChuTruongDauTu>, IQLTD_CTCV_ChuTruongDauTuRepository
    {
        public QLTD_CTCV_ChuTruongDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CTCV_ChuTruongDauTu> GetQLTD_CTCV_ChuTruongDauTu()
        {
            var query = from kh in DbContext.QLTD_CTCV_ChuTruongDauTus
                        select kh;
            return query;
        }
    }
}
