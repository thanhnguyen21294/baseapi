﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_THCT_ChuTruongDauTuRepository : IRepository<QLTD_THCT_ChuTruongDauTu>
    {
        IEnumerable<QLTD_THCT_ChuTruongDauTu> GetQLTD_THCT_ChuTruongDauTu();
    }
    public class QLTD_THCT_ChuTruongDauTuRepository : RepositoryBase<QLTD_THCT_ChuTruongDauTu>, IQLTD_THCT_ChuTruongDauTuRepository
    {
        public QLTD_THCT_ChuTruongDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_THCT_ChuTruongDauTu> GetQLTD_THCT_ChuTruongDauTu()
        {
            var query = from kh in DbContext.QLTD_THCT_ChuTruongDauTu
                        select kh;
            return query;
        }
    }
}
