﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_TDTH_ThucHienDuAnRepository : IRepository<QLTD_TDTH_ThucHienDuAn>
    {
        IEnumerable<QLTD_TDTH_ThucHienDuAn> GetQLTD_TDTH_ThucHienDuAn();
    }
    public class QLTD_TDTH_ThucHienDuAnRepository : RepositoryBase<QLTD_TDTH_ThucHienDuAn>, IQLTD_TDTH_ThucHienDuAnRepository
    {
        public QLTD_TDTH_ThucHienDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_TDTH_ThucHienDuAn> GetQLTD_TDTH_ThucHienDuAn()
        {
            var query = from kh in DbContext.QLTD_TDTH_ThucHienDuAn
                        select kh;
            return query;
        }
    }
}
