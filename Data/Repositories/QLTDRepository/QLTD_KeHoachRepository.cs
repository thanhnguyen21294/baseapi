﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_KeHoachRepository : IRepository<QLTD_KeHoach>
    {
        IEnumerable<QLTD_KeHoach> GetQLTD_KeHoach();
    }
    public class QLTD_KeHoachRepository : RepositoryBase<QLTD_KeHoach>, IQLTD_KeHoachRepository
    {
        public QLTD_KeHoachRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_KeHoach> GetQLTD_KeHoach()
        {
            var query = from kh in DbContext.QLTD_KeHoachs.Include("AppUsers")
                        select kh;
            return query;
        }
    }
}
