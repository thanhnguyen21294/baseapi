﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_TDTH_QuyetToanDuAnRepository : IRepository<QLTD_TDTH_QuyetToanDuAn>
    {
        IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetQLTD_TDTH_QuyetToanDuAn();
    }
    public class QLTD_TDTH_QuyetToanDuAnRepository : RepositoryBase<QLTD_TDTH_QuyetToanDuAn>, IQLTD_TDTH_QuyetToanDuAnRepository
    {
        public QLTD_TDTH_QuyetToanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<QLTD_TDTH_QuyetToanDuAn> GetQLTD_TDTH_QuyetToanDuAn()
        {
            var query = from kh in DbContext.QLTD_TDTH_QuyetToanDuAn
                        select kh;
            return query;
        }
    }
}
