﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_THCT_ChuanBiThucHienRepository : IRepository<QLTD_THCT_ChuanBiThucHien>
    {
        IEnumerable<QLTD_THCT_ChuanBiThucHien> GetQLTD_THCT_ChuanBiThucHien();
    }
    public class QLTD_THCT_ChuanBiThucHienRepository : RepositoryBase<QLTD_THCT_ChuanBiThucHien>, IQLTD_THCT_ChuanBiThucHienRepository
    {
        public QLTD_THCT_ChuanBiThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_THCT_ChuanBiThucHien> GetQLTD_THCT_ChuanBiThucHien()
        {
            var query = from kh in DbContext.QLTD_THCT_ChuanBiThucHien
                        select kh;
            return query;
        }
    }
}
