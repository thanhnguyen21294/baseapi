﻿using Data.Infrastructure;
using Model.Models;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_TDTH_ChuanBiDauTuRepository : IRepository<QLTD_TDTH_ChuanBiDauTu>
    {
        IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetQLTD_TDTH_ChuanBiDauTu();
    }
    public class QLTD_TDTH_ChuanBiDauTuRepository : RepositoryBase<QLTD_TDTH_ChuanBiDauTu>, IQLTD_TDTH_ChuanBiDauTuRepository
    {
        public QLTD_TDTH_ChuanBiDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_TDTH_ChuanBiDauTu> GetQLTD_TDTH_ChuanBiDauTu()
        {
            var query = from kh in DbContext.QLTD_TDTH_ChuanBiDauTu
                        select kh;
            return query;
        }
    }
}
