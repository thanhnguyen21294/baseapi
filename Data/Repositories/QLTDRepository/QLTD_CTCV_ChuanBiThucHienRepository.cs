﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CTCV_ChuanBiThucHienRepository : IRepository<QLTD_CTCV_ChuanBiThucHien>
    {
        IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetQLTD_CTCV_ChuanBiThucHien();
    }
    public class QLTD_CTCV_ChuanBiThucHienRepository : RepositoryBase<QLTD_CTCV_ChuanBiThucHien>, IQLTD_CTCV_ChuanBiThucHienRepository
    {
        public QLTD_CTCV_ChuanBiThucHienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CTCV_ChuanBiThucHien> GetQLTD_CTCV_ChuanBiThucHien()
        {
            var query = from kh in DbContext.QLTD_CTCV_ChuanBiThucHiens
                        select kh;
            return query;
        }
    }
}
