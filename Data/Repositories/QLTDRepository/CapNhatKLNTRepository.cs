﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface ICapNhatKLNTRepository : IRepository<CapNhatKLNT>
    {
        IEnumerable<CapNhatKLNT> GetCapNhatKLNT();
    }

    public class CapNhatKLNTRepository : RepositoryBase<CapNhatKLNT>, ICapNhatKLNTRepository
    {
        public CapNhatKLNTRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<CapNhatKLNT> GetCapNhatKLNT()
        {
            var query = from capnhat in DbContext.CapNhatKLNTs
                        select capnhat;
            return query;
        }
    }
}
