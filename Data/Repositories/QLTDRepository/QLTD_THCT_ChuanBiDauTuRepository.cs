﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_THCT_ChuanBiDauTuRepository : IRepository<QLTD_THCT_ChuanBiDauTu>
    {
        IEnumerable<QLTD_THCT_ChuanBiDauTu> GetQLTD_THCT_ChuanBiDauTu();
    }
    public class QLTD_THCT_ChuanBiDauTuRepository : RepositoryBase<QLTD_THCT_ChuanBiDauTu>, IQLTD_THCT_ChuanBiDauTuRepository
    {
        public QLTD_THCT_ChuanBiDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_THCT_ChuanBiDauTu> GetQLTD_THCT_ChuanBiDauTu()
        {
            var query = from kh in DbContext.QLTD_THCT_ChuanBiDauTu
                        select kh;
            return query;
        }
    }
}
