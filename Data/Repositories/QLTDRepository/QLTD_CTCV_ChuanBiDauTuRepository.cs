﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_CTCV_ChuanBiDauTuRepository : IRepository<QLTD_CTCV_ChuanBiDauTu>
    {
        IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetQLTD_CTCV_ChuanBiDauTu();
    }
    public class QLTD_CTCV_ChuanBiDauTuRepository : RepositoryBase<QLTD_CTCV_ChuanBiDauTu>, IQLTD_CTCV_ChuanBiDauTuRepository
    {
        public QLTD_CTCV_ChuanBiDauTuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_CTCV_ChuanBiDauTu> GetQLTD_CTCV_ChuanBiDauTu()
        {
            var query = from kh in DbContext.QLTD_CTCV_ChuanBiDauTus
                        select kh;
            return query;
        }
    }
}
