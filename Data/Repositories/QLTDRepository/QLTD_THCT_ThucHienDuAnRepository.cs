﻿using Data.Infrastructure;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories.QLTDRepository
{
    public interface IQLTD_THCT_ThucHienDuAnRepository : IRepository<QLTD_THCT_ThucHienDuAn>
    {
        IEnumerable<QLTD_THCT_ThucHienDuAn> GetQLTD_THCT_ThucHienDuAn();
    }
    public class QLTD_THCT_ThucHienDuAnRepository : RepositoryBase<QLTD_THCT_ThucHienDuAn>, IQLTD_THCT_ThucHienDuAnRepository
    {
        public QLTD_THCT_ThucHienDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<QLTD_THCT_ThucHienDuAn> GetQLTD_THCT_ThucHienDuAn()
        {
            var query = from kh in DbContext.QLTD_THCT_ThucHienDuAn
                        select kh;
            return query;
        }
    }
}
