﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface INguonVonGoiThauThanhToanRepository : IRepository<NguonVonGoiThauThanhToan>
    {
    }
    public class NguonVonGoiThauThanhToanRepository : RepositoryBase<NguonVonGoiThauThanhToan>, INguonVonGoiThauThanhToanRepository
    {
        public NguonVonGoiThauThanhToanRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
