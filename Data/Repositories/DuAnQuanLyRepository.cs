﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IDuAnQuanLyRepository : IRepository<DuAnQuanLy>
    {
    }

    public class DuAnQuanLyRepository : RepositoryBase<DuAnQuanLy>, IDuAnQuanLyRepository
    {
        public DuAnQuanLyRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
