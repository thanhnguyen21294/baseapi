﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ILoaiHopDongRepository: IRepository<LoaiHopDong>
    {
    }
    public class LoaiHopDongRepository : RepositoryBase<LoaiHopDong>, ILoaiHopDongRepository
    {
        public LoaiHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
