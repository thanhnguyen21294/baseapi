﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Infrastructure;
using Model.Models;
using Z.EntityFramework.Plus;

namespace Data.Repositories
{
    public interface IThanhToanHopDongRepository : IRepository<ThanhToanHopDong>
    {
        IEnumerable<HopDong> GetThanhToanHopDongs(int idDuAn, string filter);
    }

    public class ThanhToanHopDongRepository : RepositoryBase<ThanhToanHopDong>, IThanhToanHopDongRepository
    {
        public ThanhToanHopDongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<HopDong> GetThanhToanHopDongs(int idDuAn, string filter)
        {
            IEnumerable<HopDong> query;
            if (filter == null)
            {
                query = from hopdong in DbContext.HopDongs.Include("ThanhToanHopDongs")
                        where hopdong.ThanhToanHopDongs.Count > 0 && hopdong.IdDuAn == idDuAn
                        select hopdong;
            }
            else
            {
                query = from hopdong in DbContext.HopDongs.IncludeFilter(x =>
                        x.ThanhToanHopDongs.Where(y => y.NoiDung.Contains(filter)))
                        where hopdong.ThanhToanHopDongs.Count > 0 &&
                              hopdong.IdDuAn == idDuAn &&
                              hopdong.ThanhToanHopDongs.Any(a => a.NoiDung.Contains(filter))
                        select hopdong;
            }
            return query;
        }
    }
}
