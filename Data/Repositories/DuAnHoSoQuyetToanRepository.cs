﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IDuAnHoSoQuyetToanRepository : IRepository<DuAnHoSoQuyetToan>
    {
    }
    public class DuAnHoSoQuyetToanRepository : RepositoryBase<DuAnHoSoQuyetToan>, IDuAnHoSoQuyetToanRepository
    {
        public DuAnHoSoQuyetToanRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
