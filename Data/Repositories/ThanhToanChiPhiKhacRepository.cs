﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IThanhToanChiPhiKhacRepository : IRepository<ThanhToanChiPhiKhac>
    {
        IEnumerable<ThanhToanChiPhiKhac> GetThanhToan_NguonVonThanhToanCPK();
    }

    public class ThanhToanChiPhiKhacRepository : RepositoryBase<ThanhToanChiPhiKhac>, IThanhToanChiPhiKhacRepository
    {
        public ThanhToanChiPhiKhacRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public IEnumerable<ThanhToanChiPhiKhac> GetThanhToan_NguonVonThanhToanCPK()
        {
            var query = DbContext.ThanhToanChiPhiKhacs.Include("NguonVonThanhToanChiPhiKhacs");
            return query;
        }
      
    }
}
