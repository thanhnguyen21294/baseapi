﻿using Common;
using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IAppUserRepository : IRepository<AppUser>
    {
        
        IEnumerable<AppUser> GetAllUser();
        IEnumerable<AppUser> GetAllUserOther14();
    }

    public class AppUserRepository : RepositoryBase<AppUser>, IAppUserRepository
    {
        public AppUserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<AppUser> GetAllUser()
        {
            var users = from r in DbContext.Users
                        select r;

            return users;
        }

        public IEnumerable<AppUser> GetAllUserOther14()
        {
            var users = from r in DbContext.Users
                        where r.IdNhomDuAnTheoUser != null && r.IdNhomDuAnTheoUser != 14
                        select r;

            return users;
        }
    }
}
