﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IHinhThucDauThauRepository: IRepository<HinhThucDauThau>
    {
    }
    public class HinhThucDauThauRepository : RepositoryBase<HinhThucDauThau>, IHinhThucDauThauRepository
    {
        public HinhThucDauThauRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
