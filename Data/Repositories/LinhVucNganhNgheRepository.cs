﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface ILinhVucNganhNgheRepository: IRepository<LinhVucNganhNghe>
    {
    }
    public class LinhVucNganhNgheRepository : RepositoryBase<LinhVucNganhNghe>, ILinhVucNganhNgheRepository
    {
        public LinhVucNganhNgheRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
