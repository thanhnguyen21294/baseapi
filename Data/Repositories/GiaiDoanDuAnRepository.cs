﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public interface IGiaiDoanDuAnRepository: IRepository<GiaiDoanDuAn>
    {
    }
    public class GiaiDoanDuAnRepository : RepositoryBase<GiaiDoanDuAn>, IGiaiDoanDuAnRepository
    {
        public GiaiDoanDuAnRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
