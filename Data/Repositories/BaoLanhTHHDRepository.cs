﻿using Data.Infrastructure;
using Model.Models;

namespace Data.Repositories
{
    public interface IBaoLanhTHHDRepository : IRepository<BaoLanhTHHD>
    {
    }

    public class BaoLanhTHHDRepository : RepositoryBase<BaoLanhTHHD>, IBaoLanhTHHDRepository
    {
        public BaoLanhTHHDRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }


    }
}
