﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Model.Models;
using Model.Models.QLTD;
using Model.Models.QLTD.GPMB;
using Model.Models.Mobile;
using Model.Models.Common;

namespace Data
{
    public class DataContext : IdentityDbContext<AppUser>
    {
        public DataContext() : base("DataConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<DataContext>(null);
            Database.CommandTimeout = 1800;
        }

        
        public DbSet<Function> Functions { set; get; }
        public DbSet<Permission> Permissions { set; get; }
        public DbSet<AppRole> AppRoles { set; get; }
        //public DbSet<AppUser> AppUsers { set; get; }
        public DbSet<IdentityUserRole> UserRoles { set; get; }
        public DbSet<Error> Errors { set; get; }

        public DbSet<ChuDauTu> ChuDauTus { set; get; }
        public DbSet<ChuTruongDauTu> ChuTruongDauTus { set; get; }
        public DbSet<CoQuanPheDuyet> CoQuanPheDuyets { set; get; }
        public DbSet<DanhMucNhaThau> DanhMucNhaThaus { set; get; }
        public DbSet<DuAn> DuAns { set; get; }
        public DbSet<DuAnUser> DuAnUsers { set; get; }
        public DbSet<GiaiDoanDuAn> GiaiDoanDuAns { set; get; }
        public DbSet<DanhMucHoSoQuyetToan> DanhMucHoSoQuyetToans { set; get; }
        public DbSet<DuAnHoSoQuyetToan> DuAnHoSoQuyetToans { set; get; }
        public DbSet<GoiThau> GoiThaus { set; get; }
        public DbSet<HinhThucDauThau> HinhThucDauThaus { set; get; }
        public DbSet<HinhThucDauTu> HinhThucDauTus { set; get; }
        public DbSet<HinhThucLuaChonNhaThau> HinhThucLuaChonNhaThaus { set; get; }
        public DbSet<HinhThucQuanLy> HinhThucQuanLys { set; get; }
        public DbSet<HopDong> HopDongs { set; get; }
        public DbSet<HoSoMoiThau> HoSoMoiThaus { set; get; }
        public DbSet<LoaiChiPhi> LoaiChiPhis { set; get; }
        public DbSet<KeHoachLuaChonNhaThau> KeHoachLuaChonNhaThaus { set; get; }
        public DbSet<KeHoachVon> KeHoachVons { set; get; }
        public DbSet<KetQuaDauThau> KetQuaDauThaus { set; get; }
        public DbSet<LapQuanLyDauTu> LapQuanLyDauTus { set; get; }
        public DbSet<LinhVucDauThau> LinhVucDauThaus { set; get; }
        public DbSet<LinhVucNganhNghe> LinhVucNganhNghes { set; get; }
        public DbSet<LoaiCapCongTrinh> LoaiCapCongTrinhs { set; get; }
        public DbSet<LoaiHopDong> LoaiHopDongs { set; get; }
        public DbSet<NguonVon> NguonVons { set; get; }
        public DbSet<NguonVonDuAn> NguonVonDuAns { set; get; }
        public DbSet<NguonVonDuAnGoiThau> NguonVonDuAnGoiThaus { set; get; }
        public DbSet<NguonVonGoiThauTamUng> NguonVonGoiThauTamUngs { set; get; }
        public DbSet<NguonVonGoiThauThanhToan> NguonVonGoiThauThanhToans { set; get; }
        public DbSet<NguonVonKeHoachVon> NguonVonKeHoachVons { set; get; }
        public DbSet<NguonVonQuyetToanDuAn> NguonVonQuyetToanDuAns { set; get; }
        public DbSet<NguonVonThanhToanChiPhiKhac> NguonVonThanhToanChiPhiKhacs { set; get; }
        public DbSet<NhomDuAn> NhomDuAns { set; get; }
        public DbSet<PhuLucHopDong> PhuLucHopDongs { set; get; }
        public DbSet<PhuongThucDauThau> PhuongThucDauThaus { set; get; }
        public DbSet<QuyetToanDuAn> QuyetToanDuAns { set; get; }
        public DbSet<QuyetToanHopDong> QuyetToanHopDongs { set; get; }
        public DbSet<LienKetLinhVuc> LienKetLinhVucs { set; get; }
        public DbSet<TamUngHopDong> TamUngHopDongs { set; get; }
        public DbSet<ThamDinhLapQuanLyDauTu> ThamDinhLapQuanLyDauTus { set; get; }
        public DbSet<ThanhToanChiPhiKhac> ThanhToanChiPhiKhacs { set; get; }
        public DbSet<ThanhToanHopDong> ThanhToanHopDongs { set; get; }
        public DbSet<ThucHienHopDong> ThucHienHopDongs { set; get; }
        public DbSet<ThuMucMacDinh> ThuMucMacDinhs { set; get; }
        public DbSet<ThuMuc> ThuMucs { set; get; }
        public DbSet<TienDoThucHien> TienDoThucHiens { set; get; }
        public DbSet<TienDoChiTiet> TienDoChiTiets { set; get; }
        public DbSet<TinhTrangDuAn> TinhTrangDuAns { set; get; }
        public DbSet<VanBanDuAn> VanBanDuAns { set; get; }
        public DbSet<NhomDuAnTheoUser> NhomDuAnTheoUsers { set; get; }


        public DbSet<QLTD_GiaiDoan> QLTD_GiaiDoans { set; get; }
        public DbSet<QLTD_KeHoach> QLTD_KeHoachs { set; get; }
        public DbSet<QLTD_CongViec> QLTD_CongViecs { set; get; }
        public DbSet<QLTD_KeHoachCongViec> QLTD_KeHoachCongViecs { set; get; }
        public DbSet<QLTD_TDTH_ChuTruongDauTu> QLTD_TienDoThucHien { set; get; }
        public DbSet<QLTD_TDTH_ChuanBiDauTu> QLTD_TDTH_ChuanBiDauTu { set; get; }
        public DbSet<QLTD_TDTH_ChuanBiThucHien> QLTD_TDTH_ChuanBiThucHien { set; get; }
        public DbSet<QLTD_TDTH_ThucHienDuAn> QLTD_TDTH_ThucHienDuAn { set; get; }
        public DbSet<QLTD_TDTH_QuyetToanDuAn> QLTD_TDTH_QuyetToanDuAn { set; get; }
        public DbSet<QLTD_CTCV_ChuTruongDauTu> QLTD_CTCV_ChuTruongDauTus { set; get; }
        public DbSet<QLTD_CTCV_ChuanBiDauTu> QLTD_CTCV_ChuanBiDauTus { set; get; }
        public DbSet<QLTD_CTCV_ChuanBiThucHien> QLTD_CTCV_ChuanBiThucHiens { set; get; }
        public DbSet<QLTD_CTCV_ThucHienDuAn> QLTD_CTCV_ThucHienDuAns { set; get; }
        public DbSet<QLTD_CTCV_QuyetToanDuAn> QLTD_CTCV_QuyetToanDuAns { set; get; }
        public DbSet<QLTD_KhoKhanVuongMac> QLTD_KhoKhanVuongMac { set; get; }
        public DbSet<QLTD_THCT_ChuTruongDauTu> QLTD_THCT_ChuTruongDauTu { set; get; }
        public DbSet<QLTD_THCT_ChuanBiDauTu> QLTD_THCT_ChuanBiDauTu { set; get; }
        public DbSet<QLTD_THCT_ChuanBiThucHien> QLTD_THCT_ChuanBiThucHien { set; get; }
        public DbSet<QLTD_THCT_ThucHienDuAn> QLTD_THCT_ThucHienDuAn { set; get; }
        public DbSet<QLTD_THCT_QuyetToanDuAn> QLTD_THCT_QuyetToanDuAn { set; get; }

        public DbSet<GPMB_KetQua> GPMB_KetQua { set; get; }
        public DbSet<GPMB_TongQuanKetQua> GPMB_TongQuanKetQua { set; get; }
        public DbSet<GPMB_DangKyKeHoach> GPMB_DangKyKeHoach { set; get; }
        public DbSet<GPMB_File> GPMB_File { set; get; }
        public DbSet<GPMP_File_Comment> GPMP_File_Comments { set; get; }
        public DbSet<QLTD_KeHoachTienDoChung> QLTD_KeHoachTienDoChung { set; get; }

        public DbSet<GPMB_KetQua_ChiTiet> GPMB_KetQua_ChiTiet { set; get; }

        public DbSet<GPMB_KeHoach> GPMB_KeHoach { get; set; }
        public DbSet<GPMB_KeHoachCongViec> GPMB_KeHoachCongViec { get; set; }
        public DbSet<GPMB_CongViec> GPMB_CongViec { get; set; }
        public DbSet<GPMB_CTCV> GPMB_CTCV { get; set; }
        public DbSet<GPMB_KhoKhanVuongMac> GPMB_KhoKhanVuongMac { get; set; }
        public DbSet<GPMB_KeHoachTienDoChung> GPMB_KeHoachTienDoChung { get; set; }

        public DbSet<PhongCongTac> PhongCongTacs { set; get; }

        public DbSet<Comment> Comments { set; get; }
        public DbSet<GPMB_Comment> GPMB_Comments { get; set; }

        public DbSet<CapNhatKLNT> CapNhatKLNTs { get; set; }
        public DbSet<VonCapNhatKLNT> VonCapNhatKLNTs { get; set; }

        public DbSet<BaoCaoTable> BaoCaoTable { get; set; }
        public DbSet<ActionHistory> ActionHistories { get; set; }
        public DbSet<SystemLogs> SystemLogs { get; set; }

        public DbSet<GPMB_CTCV_Dot> GPMB_CTCV_Dots { get; set; }

        public DbSet<QuanLyDA> QuanLyDAs { get; set; }
        public DbSet<DuAnQuanLy> DuAnQuanLys { get; set; }

        public DbSet<ChatApp> ChatApps { get; set; }
        public DbSet<NhomChatApp> NhomChatApps { get; set; }
        public DbSet<ChatNhomApp> ChatNhomApps { get; set; }
        public DbSet<TrangThaiChat> TrangThaiChats { get; set; }
        public DbSet<CamKetGiaiNgan> CamKetGiaiNgans { set; get; }

        public DbSet<ThuMucVanBanDen> ThuMucVanBanDens { set; get; }
        public DbSet<VanBanDen> VanBanDens { set; get; }
        public DbSet<VanBanDenThucHien> VanBanDenThucHiens { set; get; }
        public DbSet<VanBanDenUser> VanBanDenUsers { set; get; }

        public DbSet<BaoLanhTHHD> BaoLanhTHHDs { set; get; }
        public DbSet<GiaTriThanhToanTheoNam> GiaTriThanhToanTheoNams { set; get; }

        public DbSet<PhieuXuLyCongViecNoiBo> PhieuXuLyCongViecNoiBos { set; get; }
        public DbSet<PhieuXuLyCongViecNoiBoUser> PhieuXuLyCongViecNoiBoUsers { set; get; }

        #region Table mobile
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Device_Token> Device_Tokens { get; set; }
        public DbSet<Notification_User> Notification_Users { get; set; }
        public DbSet<Notification_Group> Notification_Groups { get; set; }
        public DbSet<Notification_Group_Token> Notification_Group_Tokens { get; set; }
        #endregion Table mobile

        public static DataContext Create()
        {
            return new DataContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasKey(r => r.Id).ToTable("AppRoles");
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("AppUserRoles");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("AppUserLogins");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("AppUserClaims");

            //builder.Entity<NguonVonKeHoachVon>()
            //    .HasRequired(x => x.KeHoachVon)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);
            //builder.Entity<NguonVonDuAn>()
            //    .HasRequired(x=>x.DuAn)
            //    .WithMany()
            //    .WillCascadeOnDelete(true);
            //builder.Entity<NguonVonDuAnGoiThau>()
            //    .HasRequired(x => x.NguonVonDuAn)
            //    .WithMany(a => a.NguonVonDuAnGoiThaus)
            //    .HasForeignKey(y => y.IdNguonVonDuAn)
            //    .WillCascadeOnDelete(false);
            builder.Entity<ChatNhomApp>()
            .HasRequired(x => x.AppUser2)
            .WithMany()
            .HasForeignKey(y => y.IdUser2)
            .WillCascadeOnDelete(true);

            //builder.Entity<BaoLanhTHHD>()
            //.HasRequired(x => x.HopDong)
            //.WithMany()
            //.WillCascadeOnDelete(true);

            builder.Entity<BaoLanhTHHD>()
            .HasRequired(x => x.DuAn)
            .WithMany()
            .WillCascadeOnDelete(false);
            builder.Entity<VanBanDenThucHien>()
           .HasRequired(x => x.VanBanDen)
           .WithMany()
           .WillCascadeOnDelete(true);

            builder.Entity<ChatApp>()
             .HasRequired(x => x.AppUser1)
             .WithMany()
             .HasForeignKey(y => y.IdUser1)
             .WillCascadeOnDelete(true);

            builder.Entity<VonCapNhatKLNT>()
               .HasRequired(x => x.DuAn)
               .WithMany()
               .HasForeignKey(y => y.IdDuAn)
               .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_QuyetToanDuAn>()
               .HasRequired(x => x.QLTD_CongViec)
               .WithMany()
               .HasForeignKey(y => y.IdCongViec)
               .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_QuyetToanDuAn>()
                .HasRequired(x => x.QLTD_KeHoachCongViec)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoachCongViec)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_QuyetToanDuAn>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ThucHienDuAn>()
               .HasRequired(x => x.QLTD_CongViec)
               .WithMany()
               .HasForeignKey(y => y.IdCongViec)
               .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ThucHienDuAn>()
                .HasRequired(x => x.QLTD_KeHoachCongViec)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoachCongViec)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_ThucHienDuAn>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuanBiThucHien>()
               .HasRequired(x => x.QLTD_CongViec)
               .WithMany()
               .HasForeignKey(y => y.IdCongViec)
               .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuanBiThucHien>()
                .HasRequired(x => x.QLTD_KeHoachCongViec)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoachCongViec)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_ChuanBiThucHien>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuanBiDauTu>()
               .HasRequired(x => x.QLTD_CongViec)
               .WithMany()
               .HasForeignKey(y => y.IdCongViec)
               .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuanBiDauTu>()
                .HasRequired(x => x.QLTD_KeHoachCongViec)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoachCongViec)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_ChuanBiDauTu>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuTruongDauTu>()
               .HasRequired(x => x.QLTD_CongViec)
               .WithMany()
               .HasForeignKey(y => y.IdCongViec)
               .WillCascadeOnDelete(false);
            builder.Entity<QLTD_CTCV_ChuTruongDauTu>()
                .HasRequired(x => x.QLTD_KeHoachCongViec)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoachCongViec)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CTCV_ChuTruongDauTu>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany()
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_KeHoach>()
                .HasRequired(x => x.QLTD_GiaiDoan)
                .WithMany()
                .HasForeignKey(y => y.IdGiaiDoan)
                .WillCascadeOnDelete(true);
            builder.Entity<QLTD_CongViec>()
                .HasRequired(x => x.QLTD_GiaiDoan)
                .WithMany()
                .HasForeignKey(y => y.IdGiaiDoan)
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonDuAnGoiThau>()
                .HasRequired(x => x.NguonVonDuAn)
                .WithMany(a => a.NguonVonDuAnGoiThaus)
                .HasForeignKey(y => y.IdNguonVonDuAn)
                .WillCascadeOnDelete(true);
            builder.Entity<NguonVonDuAnGoiThau>()
                .HasRequired(x => x.GoiThau)
                .WithMany(a => a.NguonVonDuAnGoiThaus)
                .HasForeignKey(y => y.IdGoiThau)
                .WillCascadeOnDelete(false);
            builder.Entity<ThanhToanChiPhiKhac>()
                .HasRequired(x => x.DuAn)
                .WithMany()
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonGoiThauThanhToan>()
                .HasRequired(x => x.ThanhToanHopDong)
                .WithMany(a => a.NguonVonGoiThauThanhToans)
                .HasForeignKey(y => y.IdThanhToanHopDong)
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonGoiThauTamUng>()
                .HasRequired(x => x.TamUngHopDong)
                .WithMany(a => a.NguonVonGoiThauTamUngs)
                .HasForeignKey(y => y.IdTamUng)
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonThanhToanChiPhiKhac>()
                .HasRequired(x => x.ThanhToanChiPhiKhac)
                .WithMany(a => a.NguonVonThanhToanChiPhiKhacs)
                .HasForeignKey(y => y.IdThanhToanChiPhiKhac)
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonQuyetToanDuAn>()
                .HasRequired(x => x.QuyetToanDuAn)
                .WithMany(a => a.NguonVonQuyetToanDuAns)
                .HasForeignKey(y => y.IdQuyetToanDuAn)
                .WillCascadeOnDelete(false);
            builder.Entity<NguonVonKeHoachVon>()
                .HasRequired(x => x.KeHoachVon)
                .WithMany(a => a.NguonVonKeHoachVons)
                .HasForeignKey(y => y.IdKeHoachVon)
                .WillCascadeOnDelete(false);
            builder.Entity<TamUngHopDong>()
               .HasRequired(x => x.HopDong)
               .WithMany(a => a.TamUngHopDongs)
               .HasForeignKey(y => y.IdHopDong)
               .WillCascadeOnDelete(false);
            builder.Entity<ThanhToanHopDong>()
               .HasRequired(x => x.ThucHienHopDong)
               .WithMany(a => a.ThanhToanHopDongs)
               .HasForeignKey(y => y.IdThucHien)
               .WillCascadeOnDelete(false);
            builder.Entity<TienDoChiTiet>()
                .HasRequired(x => x.TienDoThucHien)
                .WithMany(a => a.TienDoChiTiets)
                .HasForeignKey(y => y.IdTienDo)
                .WillCascadeOnDelete(false);
            builder.Entity<GoiThau>()
                .HasRequired(x => x.KeHoachLuaChonNhaThau)
                .WithMany(a=>a.GoiThaus)
                .HasForeignKey(y => y.IdKeHoachLuaChonNhaThau)
                .WillCascadeOnDelete(false);
            builder.Entity<HopDong>()
                .HasRequired(x => x.GoiThau)
                .WithMany(a => a.HopDongs)
                .HasForeignKey(y => y.IdGoiThau)
                .WillCascadeOnDelete(false);
            builder.Entity<QLTD_KeHoachTienDoChung>()
                .HasRequired(x => x.QLTD_KeHoach)
                .WithMany(a => a.QLTD_KeHoachTienDoChungs)
                .HasForeignKey(y => y.IdKeHoach)
                .WillCascadeOnDelete(false);
            //builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
