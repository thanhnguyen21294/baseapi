﻿using System;

namespace Model.Abstract
{
    public class IAuditable
    {
        private DateTime? CreatedDate { set; get; }
        private string CreatedBy { set; get; }
        private DateTime? UpdatedDate { set; get; }
        private string UpdatedBy { set; get; }

        private bool Satus { set; get; }
    }
}