﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("VanBanDens")]
    public class VanBanDen : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdVanBanDen { set; get; }
        
        [MaxLength(255)]
        [Required]
        public string SoVanBanDen { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenVanBanDen { set; get; }
        [MaxLength(255)]
        public string FileName { set; get; }
        [Column(TypeName = "ntext")]
        public string FileUrl { set; get; }
        public DateTime? NgayDen { set; get; }
        public DateTime? NgayNhan { set; get; }

        public DateTime? ThoiHanHoanThanh { set; get; }

        public int? TrangThai { get; set; }
        public string TPYeuCau { get; set; }
        public string DeXuatTPTH  { get; set; }
        public string YKienChiDaoGiamDoc { get; set; }
        [MaxLength(255)]
        public string NoiGui { set; get; }
        public string MoTa { set; get; }
        public string GhiChu { set; get; }
        public int IdThuMucVanBanDen { set; get; }
        [ForeignKey("IdThuMucVanBanDen")]
        public virtual ThuMucVanBanDen ThuMucVanBanDen { set; get; }
        public virtual ICollection<VanBanDenUser> VanBanDenUsers { set; get; }
        public virtual ICollection<VanBanDenThucHien> VanBanDenThucHiens { set; get; }
    }

}
