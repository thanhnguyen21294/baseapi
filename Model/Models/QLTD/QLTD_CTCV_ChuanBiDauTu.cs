﻿using Model.Abstract;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_CTCV_ChuanBiDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        [Required]
        public int IdCongViec { get; set; }

        [Required]
        public int IdKeHoachCongViec { get; set; }

        public string BCPATK_NoiDung { set; get; }

        public string PDDT_SoQD { set; get; }
        public DateTime? PDDT_NgayPQ { set; get; }
        public double? PDDT_GiaTri { set; get; }

        public string PDKHLCNT_SoQD { set; get; }
        public DateTime? PDKHLCNT_NgayPD { set; get; }
        public double? PDKHLCNT_GiaTri { set; get; }

        public string KSHT_NoiDung { set; get; }

        public string CGDD_NoiDung { set; get; }

        public string SLHTKT_NoiDung { set; get; }

        public bool PDQH_KhongPheDuyet { set; get; }
        public string PDQH_SoQD_QHCT { set; get; }
        public DateTime? PDQH_NgayPD_QHCT { set; get; }
        public string PDQH_SoQD_QHTMB { set; get; }
        public DateTime? PDQH_NgayPD_QHTMB { set; get; }

        public string KSDCDH_NoiDung { set; get; }

        public string TTTDPCCC_NoiDung { set; get; }

        public string TTK_NoiDung { set; get; }

        public string LHSCBDT_KhoKhanVuongMac { set; get; }
        public string LHSCBDT_KienNghiDeXuat { set; get; }
        public DateTime? LHSCBDT_NgayHoanThanh { set; get; }
        public string LHSCBDT_SoQD { set; get; }
        public DateTime? LHSCBDT_NgayLaySo { set; get; }

        public string THSCBDT_SoToTrinh { set; get; }
        public DateTime? THSCBDT_NgayTrinh { set; get; }

        public string PDDA_BCKTKT_SoQD { set; get; }
        public DateTime? PDDA_BCKTKT_NgayPD { set; get; }
        public double? PDDA_BCKTKT_GiaTri { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }
        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdKeHoachCongViec")]
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }

        public virtual ICollection<QLTD_TDTH_ChuanBiDauTu> QLTD_TDTH_ChuanBiDauTu { get; set; }
    }
}

