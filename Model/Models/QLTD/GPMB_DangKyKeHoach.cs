﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class GPMB_DangKyKeHoach : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDangKyKeHoach { set; get; }

        [Required]
        public int IdDuAn { get; set; }

        public double? QuyetDinhTLHD_TCT_PCNV_NgayHoanThanh { get; set; }
        public double? QuyetDinhPDDT_CongTacGPMB_NgayHoanThanh { get; set; }
        public double? HopHD_HopDUXMoRong_NgayHoanThanh { get; set; }
        public double? QuyChuTrichDoHTDD_LapKHThuHoiDat_NgayHoanThanh { get; set; }
        public double? TBThuHoiDat_NgayHoanThanh { get; set; }

        public double? HopDanPhatToKhai_NgayHoanThanh { get; set; }
        public double? DieuTraKeKhaiLapBBGPMB_NgayHoanThanh { get; set; }
        public double? CuongCheKDBB_NgayHoanThanh { get; set; }
        public double? LapHSGPMB_NgayHoanThanh { get; set; }
        public double? LapTrinhHDTT_HoanThienDTPA_NgayHoanThanh { get; set; }
        public double? CongKhaiKTCongKhaiDTPA_NgayHoanThanh { get; set; }
        public double? TrinhHDTDHoanThienPACT_NgayHoanThanh { get; set; }
        public double? QDThuHoiDat_QDPheDuyetPA_NgayHoanThanh { get; set; }
        public double? ChiTraTien_NhanBGD_NgayHoanThanh { get; set; }
        public double? CuongCheThuHoiDat_NgayHoanThanh { get; set; }

        public double? QuyetToanKPTHGPMB_NgayHoanThanh { get; set; }
        public double? LapHSDNTPGiaoDat_NgayHoanThanh { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
    }
}
