﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_TienDoThucHien : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTienDoThucHien { set; get; }

        [Required]
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { set; get; }

        public string TenDonViThucHien { set; get; }

        public string DonViThamDinh { set; get; }

        public string DonViPhoiHop { set; get; }

        public string CoQuanPheDuyet { set; get; }

        public string GhiChu { set; get; }

        [ForeignKey("IdChiTietCongViec")]
        public virtual QLTD_CTCV_ChuTruongDauTu QLTD_CTCV_ChuTruongDauTu { get; set; }
    }
}