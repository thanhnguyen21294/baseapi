﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class GPMB_KetQua_ChiTiet : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGPMBKetQuaChiTiet { set; get; }

        public int IdGPMBKetQua { get; set; }

        public string NoiDung { set; get; }

        public string ThayDoi { get; set; }

        [ForeignKey("IdGPMBKetQua")]
        public virtual GPMB_KetQua GPMB_KetQua { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual AppUser AppUsers { get; set; }
    }
}
