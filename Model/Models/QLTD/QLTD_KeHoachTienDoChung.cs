﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Model.Abstract;

namespace Model.Models.QLTD
{
    public class QLTD_KeHoachTienDoChung : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachTienDoChung { set; get; }

        [Required]
        public int IdKeHoach { set; get; }

        public string NoiDung { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
    }
}
