﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class CapNhatKLNT : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdFile { set; get; }

        [MaxLength(255)]
        public string FileName { set; get; }

        [Column(TypeName = "ntext")]
        public string FileUrl { set; get; }


    }
}
