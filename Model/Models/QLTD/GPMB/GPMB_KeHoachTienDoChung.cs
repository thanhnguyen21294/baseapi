﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_KeHoachTienDoChung : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachTienDoChung { set; get; }

        [Required]
        public int IdKeHoachGPMB { set; get; }

        public string NoiDung { get; set; }

        [ForeignKey("IdKeHoachGPMB")]
        public virtual GPMB_KeHoach GPMB_KeHoach { get; set; }
    }
}
