﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    [Table("GPMB_CTCV_Dots")]
    public class GPMB_CTCV_Dot : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec_Dot { set; get; }

        [Required]
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { get; set; }
        public DateTime? TBTHD_Ngay { get; set; }
        public string TBTHD_SoDoiTuong { get; set; }

        public DateTime? HDPTK_Ngay { get; set; }

        public string DTKT_SoDoiTuong { get; set; }

        public bool CCKDBB_Chon { get; set; }

        public string LHSGPMB_SoDoiTuong { get; set; }

        public string LTHDTT_SoDoiTuong { get; set; }

        public string CKKT_SoDoiTuong { get; set; }

        public string THDTD_SoDoiTuong { get; set; }

        public string QDPDPA_SoDoiTuong { get; set; }
        public string QDPDPA_GiaTri { get; set; }

        public string CTNBG_SoDoiTuong { get; set; }
        public string CTNBG_GiaTri { get; set; }

        public bool CCTHD_Chon { get; set; }

        [ForeignKey("IdChiTietCongViec")]
        public virtual GPMB_CTCV GPMB_CTCV { get; set; }
    }
}
