﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_CTCV : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoachCongViecGPMB { get; set; }

        public string SoQuyetDinh { get; set; }
        public DateTime? NgayPheDuyet { get; set; }

        public string VBGNV_SoVanBan { get; set; }
        public DateTime? VBGNV_Ngay { get; set; }

        public DateTime? BBBGMG_Ngay { get; set; }

        public string QDTLHD_So { get; set; }
        public DateTime? QDTLHD_Ngay { get; set; }
        public string QDTCT_So { get; set; }
        public DateTime? QDTCT_Ngay { get; set; }

        public string QDPDDT_So { get; set; }
        public DateTime? QDPDDT_NgayBanBanHanh { get; set; }

        public DateTime? HHD_Ngay { get; set; }
        public DateTime? HDU_Ngay { get; set; }

        public bool QCTDHT_Chon { get; set; }

        public string TBTHD_SoDoiTuong { get; set; }

        public bool HDPTK_Chon { get; set; }

        public string DTKKLBB_SoDoiTuong { get; set; }

        public bool CCKDBB_Chon { get; set; }

        public string LHSGPMB_So { get; set; }

        public string LTHDTT_So { get; set; }

        public string SKKTDTPA_So { get; set; }

        public string THDHTPA_So { get; set; }

        public string QDPDPA_So { get; set; }
        public string QDPDPA_TGT { get; set; }

        public string CTTNBG_So { get; set; }
        public string CTTNBG_TGT { get; set; }

        public bool CCTHD_Chon { get; set; }

        public string QTKP_So { get; set; }
        public DateTime? QTKP_Ngay { get; set; }
        public string QTKP_GT { get; set; }

        public string QDGD_So { get; set; }
        public DateTime? QDGD_Ngay { get; set; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }

        [ForeignKey("IdKeHoachCongViecGPMB")]
        public virtual GPMB_KeHoachCongViec GPMB_KeHoachCongViec { get; set; }
    }
}
