﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_Comment : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdComment { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        [Required]
        public string UserId { get; set; }

        public string NoiDung { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAns { get; set; }
    }
}
