﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.QLTD.GPMB
{
    [Table("GPMP_File_Comments")]
    public class GPMP_File_Comment : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int IdFile { get; set; }

        [Required]
        public string UserId { get; set; }

        public string NoiDung { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUser { get; set; }

        [ForeignKey("IdFile")]
        public virtual GPMB_File GPMB_File { get; set; }
    }
}
