﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_BanLamViec
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public double? TongQuan_TongDienTich { get; set; }
        public double? TongQuan_TongSoHo { get; set; }
        public double? DHT_DienTich { get; set; }
        public double? DHT_SoHo { get; set; }
        public double? KKVM_DienTich { get; set; }
        public double? KKVM_SoHo { get; set; }
        public double? KKVM_SoHo_DaPheChuaNhanTien { get; set; }
        public double? KKVM_SoHo_KhongHopTacDieuTra { get; set; }
        public double? KKVM_SoHo_ChuaDieuTra { get; set; }
        public string GhiChu { get; set; }
    }
}
