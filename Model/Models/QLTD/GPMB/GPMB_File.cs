﻿using Model.Abstract;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Model.Models.QLTD.GPMB
{
    public class GPMB_File : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public int? ParentId { set; get; }

        [MaxLength(255)]
        public string NoiDung { set; get; }

        public bool PheDuyet { get; set; }
        [DefaultValue(false)]
        public bool IsBossCreated { get; set; }

        public int TrangThai { get; set; }

        public int? TinhTrang { get; set; }

        public DateTime? NgayTinhTrang { get; set; }

        [MaxLength(255)]
        public string Mota { set; get; }

        [Column(TypeName = "ntext")]
        public string FileURL { set; get; }

        [ForeignKey("CreatedBy")]
        public virtual AppUser AppUser { get; set; }

        [NotMapped]
        public bool Listchild { get; set; }
    }
}
