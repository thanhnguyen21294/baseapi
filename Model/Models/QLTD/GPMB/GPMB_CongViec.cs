﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_CongViec : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCongViecGPMB { set; get; }

        public string TenCongViec { set; get; }

        public int LoaiCongViec { get; set; }

        public bool NhieuDot { get; set; }
        //public int? Order { set; get; }

        //[ForeignKey("IdGiaiDoan")]
        //public virtual QLTD_GiaiDoan QLTD_GiaiDoan { get; set; }
    }
}
