﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_KhoKhanVuongMac : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKhoKhanVuongMac { set; get; }

        [Required]
        public int IdKeHoachGPMB { get; set; }

        public string NguyenNhanLyDo { set; get; }

        public string GiaiPhapTrienKhai { set; get; }

        public bool DaGiaiQuyet { set; get; }

        [ForeignKey("IdKeHoachGPMB")]
        public virtual GPMB_KeHoach GPMB_KeHoach { get; set; }
    }
}
