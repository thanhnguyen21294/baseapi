﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_KeHoachCongViec : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachCongViec { set; get; }

        [Required]
        public int IdKeHoachGPMB { set; get; }

        [Required]
        public int IdCongViecGPMB { set; get; }

        [ForeignKey("IdKeHoachGPMB")]
        public virtual GPMB_KeHoach GPMB_KeHoach { get; set; }

        [ForeignKey("IdCongViecGPMB")]
        public virtual GPMB_CongViec GPMB_CongViec { get; set; }

        public DateTime? TuNgay { get; set; }

        public DateTime? DenNgay { get; set; }

        //public int LoaiCongViec { get; set; }
    }
}
