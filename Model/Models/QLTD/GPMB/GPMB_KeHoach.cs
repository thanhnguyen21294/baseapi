﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD.GPMB
{
    public class GPMB_KeHoach : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachGPMB { set; get; }

        public int? IdNhomDuAnTheoUser { set; get; }

        [Required]
        public int IdDuAn { get; set; }

        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }

        public bool PheDuyet { get; set; }

        public int TrangThai { get; set; }

        public int? TinhTrang { get; set; }

        public DateTime? NgayTinhTrang { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { set; get; }

        public virtual ICollection<GPMB_KeHoachCongViec> GPMB_KeHoachCongViecs { get; set; }
    }
}
