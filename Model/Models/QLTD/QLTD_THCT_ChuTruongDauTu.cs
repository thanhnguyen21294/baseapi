﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.QLTD
{
    public class QLTD_THCT_ChuTruongDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThucHienChiTiet { set; get; }

        [Required]
        public int IdTienDoThucHien { get; set; }

        public string NoiDung { set; get; }

        public bool ThayDoi { set; get; }

        [ForeignKey("IdTienDoThucHien")]
        public virtual QLTD_TDTH_ChuTruongDauTu QLTD_TDTH_ChuTruongDauTu { get; set; }
    }
}
