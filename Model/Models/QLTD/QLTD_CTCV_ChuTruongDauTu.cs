﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Model.Abstract;

namespace Model.Models
{
    public class QLTD_CTCV_ChuTruongDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        [Required]
        public int IdCongViec { get; set; }

        [Required]
        public int IdKeHoachCongViec { get; set; }

        public bool CTDD_CoSanDiaDiem { set; get; }
        public string CTDD_SoVB { set; get; }
        public DateTime? CTDD_NgayPD { set; get; }

        public string LCTDT_TienDoThucHien { set; get; }
        public string LCTDT_KhoKhanVuongMac { set; get; }
        public string LCTDT_KienNghiDeXuat { set; get; }
        public DateTime? LCTDT_NgayHT { set; get; }

        public DateTime? THSPDDA_NgayTrinhThucTe { set; get; }

        public string PQCTDT_SoQD { set; get; }
        public DateTime? PQCTDT_NgayLaySo { set; get; }
        public double? PQCTDT_GiaTri { set; get; }
        public DateTime? PQCTDT_NgayPheDuyetTT { set; get; }
        public int? PQCTDT_CoQuanPD { set; get; }

        public string BTGNV_SoToTrinh { set; get; }
        public string BTGNV_SoBaoCao { set; get; }
        public DateTime? BTGNV_NgayTrinh { set; get; }

        public string HGNV_SoToTrinh { set; get; }
        public string HGNV_SoBaoCao { set; get; }
        public DateTime? HGNV_NgayTrinh { set; get; }
        public string HGNV_SoVBYKien { set; get; }

        public string HTPPDGNV_SoVanBan { set; get; }
        public DateTime? HTPPDGNV_NgayNhan { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }
        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdKeHoachCongViec")]
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }

        public virtual ICollection<QLTD_TDTH_ChuTruongDauTu> QLTD_TDTH_ChuTruongDauTu { get; set; }
    }
}
