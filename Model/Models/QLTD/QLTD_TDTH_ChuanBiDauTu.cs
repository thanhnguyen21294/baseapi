﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.QLTD
{
    public class QLTD_TDTH_ChuanBiDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTienDoThucHien { set; get; }

        [Required]
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { set; get; }

        public string TenDonViThucHien { set; get; }

        public string DonViThamDinh { set; get; }

        public string DonViPhoiHop { set; get; }

        public string CoQuanPheDuyet { set; get; }

        public DateTime? NgayBatDau { set; get; }

        public DateTime? NgayHoanThanh { set; get; }
        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public string GhiChu { set; get; }

        [ForeignKey("IdChiTietCongViec")]
        public virtual QLTD_CTCV_ChuanBiDauTu QLTD_CTCV_ChuanBiDauTu { get; set; }

        public virtual ICollection<QLTD_THCT_ChuanBiDauTu> QLTD_THCT_ChuanBiDauTu { get; set; }
    }
}
