﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_CongViec : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCongViec { set; get; }

        [Required]
        public int IdGiaiDoan { get; set; }

        public string TenCongViec { set; get; }

        public int LoaiCongViec { get; set; }

        public bool TienDoThucHien { set; get; }

        public int? Order { set; get; }

        [ForeignKey("IdGiaiDoan")]
        public virtual QLTD_GiaiDoan QLTD_GiaiDoan { get; set; }
    }
}
