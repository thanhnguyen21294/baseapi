﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class QLTD_THCT_ChuanBiDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThucHienChiTiet { set; get; }

        [Required]
        public int IdTienDoThucHien { get; set; }

        public string NoiDung { set; get; }

        public bool ThayDoi { set; get; }

        [ForeignKey("IdTienDoThucHien")]
        public virtual QLTD_TDTH_ChuanBiDauTu QLTD_TDTH_ChuanBiDauTu { get; set; }
    }
}
