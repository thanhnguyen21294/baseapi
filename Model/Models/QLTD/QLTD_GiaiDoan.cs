﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Model.Abstract;

namespace Model.Models
{
    public class QLTD_GiaiDoan : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGiaiDoan { set; get; }

        [MaxLength(255)]
        [Required]
        public string TenGiaiDoan { set; get; }

        public int NhomGiaiDoan { set; get; }
    }
}
