﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_KeHoachCongViec : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachCongViec { set; get; }

        [Required]
        public int IdKeHoach { set; get; }

        [Required]
        public int IdCongViec { set; get; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        public DateTime? TuNgay { get; set; }

        public DateTime? DenNgay { get; set; }

        public int LoaiCongViec { get; set; }
    }
}
