﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class GPMB_TongQuanKetQua : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGPMBTongQuanKetQua { set; get; }

        [Required]
        public int IdDuAn { get; set; }

        public double? DatNNCaTheTuSDOD_DT { set; get; }
        public double? DatNNCaTheTuSDOD_SoHo { set; get; }
        //public double? DatNNCaTheTuSDOD_GiaTri { set; get; }

        public double? DatNNCong_DT { set; get; }
        public double? DatNNCong_SoHo { set; get; }
        //public double? DatNNCong_GiaTri { set; get; }

        public double? DatKhac_DT { set; get; }
        public double? DatKhac_SoHo { set; get; }
        //public double? DatKhac_GiaTri { set; get; }

        public double? MoMa { set; get; }
        //public double? MoMa_GiaTri { set; get; }

        public double? Tong_DT { set; get; }
        public double? Tong_SoHo { set; get; }
        //public double? Tong_GiaTri { set; get; }

        public string GhiChu { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

        public virtual IEnumerable<GPMB_KetQua> GPMB_KetQua { get; set; }
    }
}
