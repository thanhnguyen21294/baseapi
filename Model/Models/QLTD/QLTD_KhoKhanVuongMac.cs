﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_KhoKhanVuongMac :Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKhoKhanVuongMac { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        public string NguyenNhanLyDo { set; get; }

        public string GiaiPhapTrienKhai { set; get; }

        public bool DaGiaiQuyet { set; get; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }
    }
}
