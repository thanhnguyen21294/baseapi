﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class VonCapNhatKLNT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string KLNTVonHuyen { get; set; }
        public string TDGNVonHuyen { get; set; }
        public string KLNTVonTP { get; set; }
        public string TDGNVonTP { get; set; }

        public int? IdDuAn { get; set; }

        public string MaDuAn { get; set; }

        public int? IdFile { get; set; }

        public DateTime CreateDateTime { get; set; }
        public bool isVonHuyen {get;set;}
        public bool isVonTP { get; set; }

        [ForeignKey("IdDuAn")]
        public DuAn DuAn { get; set; }

        [ForeignKey("IdFile")]
        public CapNhatKLNT CapNhatKLNT { get; set; }
    }
}
