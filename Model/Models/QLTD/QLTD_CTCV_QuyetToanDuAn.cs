﻿using Model.Abstract;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_CTCV_QuyetToanDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        [Required]
        public int IdCongViec { get; set; }

        [Required]
        public int IdKeHoachCongViec { get; set; }

        public bool HSQLCL_DaHoanThanh { set; get; }

        public string TTQT_DangThamTra { set; get; }
        public string TTQT_SoThamTra { set; get; }
        public DateTime? TTQT_NgayThamTra { set; get; }
        public double? TTQT_GiaTri { set; get; }

        public bool KTDL_KhongKiemToan { set; get; }
        public string KTDL_DangKiemToan { set; get; }
        public double? KTDL_GiaTriSauKiemToan { set; get; }

        public double? HTHSQT_GiaTriQuyetToan { set; get; }

        public string HTQT_SoQD { set; get; }
        public DateTime? HTQT_NgayPD { set; get; }
        public double? HTQT_GiaTri { set; get; }
        public double? HTQT_CongNoThu { set; get; }
        public double? HTQT_CongNoTra { set; get; }
        public double? HTQT_BoTriTraCongNo { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }
        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdKeHoachCongViec")]
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }

        public virtual ICollection<QLTD_TDTH_QuyetToanDuAn> QLTD_TDTH_QuyetToanDuAn { get; set; }
    }
}

