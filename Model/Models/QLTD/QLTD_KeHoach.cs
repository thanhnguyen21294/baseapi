﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Model.Abstract;
using Model.Models.QLTD;

namespace Model.Models
{
    public class QLTD_KeHoach : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoach { set; get; }

        public int? IdNhomDuAnTheoUser { set; get; }

        public int? IdKeHoachDC { set; get; }

        [Required]
        public int IdDuAn { get; set; }

        [Required]
        public int IdGiaiDoan { get; set; }

        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }

        public DateTime? NgayGiaoLapChuTruong { get; set; }

        public bool PheDuyet { get; set; }

        public int TrangThai { get; set; }

        public int? TinhTrang { get; set; }

        public DateTime? NgayTinhTrang { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("IdKeHoachDC")]
        public virtual QLTD_KeHoach QLTD_KeHoachDC { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

        [ForeignKey("IdGiaiDoan")]
        public virtual QLTD_GiaiDoan QLTD_GiaiDoan { get; set; }

        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { set; get; }

        public virtual ICollection<QLTD_KeHoachCongViec> QLTD_KeHoachCongViecs { get; set; }
        public virtual ICollection<QLTD_KeHoachTienDoChung> QLTD_KeHoachTienDoChungs { get; set; }
    }
}
