﻿using Model.Abstract;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_CTCV_ThucHienDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        [Required]
        public int IdCongViec { get; set; }

        [Required]
        public int IdKeHoachCongViec { get; set; }

        public string GPMB_DamBaoTienDo { set; get; }
        public string GPMB_KhoKhanVuongMac { set; get; }
        public string GPMB_DeXuatGiaiPhap { set; get; }

        public string CTHLCNT_DangLapTKBVTC { set; get; }
        public string CTHLCNT_DangTrinhTKBVTC { set; get; }
        public string CTHLCNT_SoQDKHLCNT { set; get; }
        public DateTime? CTHLCNT_NgayPheDuyetKHLCNT { set; get; }
        public string CTHLCNT_SoNgayQDPD_KHLCNT { set; get; }
        public string CTHLCNT_KhoKhan { set; get; }

        public string DLCNT_SoQDKQLCNT { set; get; }
        public DateTime? DLCNT_NgayPheDuyet { set; get; }
        public double? DLCNT_GiaTri { set; get; }
        public DateTime? DLCNT_NgayPD_KQLCNT { get; set; }

        public DateTime? DTCXD_NgayBanGiaoMB { set; get; }
        public string DTCXD_ThoiGianKhoiCong { set; get; }
        public string DTCXD_KhoKhanVuongMac { set; get; }
        public DateTime? DTCXD_NgayBBNT_HoanThanhCT { set; get; }

        public DateTime? HTTC_NgayHoanThanh { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }
        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdKeHoachCongViec")]
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }

        public virtual ICollection<QLTD_TDTH_ThucHienDuAn> QLTD_TDTH_ThucHienDuAn { get; set; }
    }
}