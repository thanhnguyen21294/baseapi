﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.QLTD
{
    public class GPMB_KetQua : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGPMBKetQua { set; get; }

        public double? DHT_DT { set; get; }
        public double? DHT_SoHo { get; set; }
        public double? DHT_TongKinhPhi { get; set; }

        public double? DHT_DatNNCaThe_DT { get; set; }
        public double? DHT_DatNNCaThe_SoHo { get; set; }
        public double? DHT_DatNNCaThe_GiaTri { get; set; }

        public double? DHT_DatNNCong_DT { get; set; }
        public double? DHT_DatNNCong_SoHo { get; set; }
        public double? DHT_DatNNCong_GiaTri { get; set; }

        public double? DHT_DatKhac_DT { get; set; }
        public double? DHT_DatKhac_SoHo { get; set; }
        public double? DHT_DatKhac_GiaTri { get; set; }

        public double? DHT_MoMa { get; set; }
        public double? DHT_MoMa_GiaTri { get; set; }


        public double? CTHX_TongDT { get; set; }
        public double? CTHX_TongSoHo { get; set; }

        public double? CTHX_LapTrinhHoiDong_DuThao_DT { get; set; }
        public double? CTHX_LapTrinhHoiDong_DuThao_SoHo { get; set; }

        public double? CTHX_CongKhai_DuThao_DT { get; set; }
        public double? CTHX_CongKhai_DuThao_SoHo { get; set; }

        public double? CTHX_TrinhHoiDong_ChinhThuc_DT { get; set; }
        public double? CTHX_TrinhHoiDong_ChinhThuc_SoHo { get; set; }

        public double? CTHX_QuyetDinhThuHoi_DT { get; set; }
        public double? CTHX_QuyetDinhThuHoi_SoHo { get; set; }

        public double? CTHX_ChiTraTien_DT { get; set; }
        public double? CTHX_ChiTraTien_SoHo { get; set; }

        public double? KKVM_DT { get; set; }
        public double? KKVM_SoHo { get; set; }
        public double? KKVM_DaPheChuaNhanTien { get; set; }
        public double? KKVM_KhongHopTac { get; set; }
        public double? KKVM_ChuaDieuTra { get; set; }

        public string GhiChu { get; set; }



        public int IDTongQuanKetQua { get; set; }

        [ForeignKey("IDTongQuanKetQua")]
        public virtual GPMB_TongQuanKetQua GPMB_TongQuanKetQua { get; set; }
        
        public virtual IEnumerable<GPMB_KetQua_ChiTiet> GPMB_KetQua_ChiTiet { get; set; }
    }
}
