﻿using Model.Abstract;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_CTCV_ChuanBiThucHien : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChiTietCongViec { set; get; }

        [Required]
        public int IdKeHoach { get; set; }

        [Required]
        public int IdCongViec { get; set; }

        [Required]
        public int IdKeHoachCongViec { get; set; }

        public string PDDT_SoQuyetDinh { set; get; }
        public DateTime? PDDT_NgayPheDuyet { set; get; }
        public double? PDDT_GiaTri { set; get; }

        public string PDKHLCNT_SoQuyetDinh { set; get; }
        public DateTime? PDKHLCNT_NgayPheDuyet { set; get; }
        public double? PDKHLCNT_GiaTri { set; get; }

        public string LHS_KhoKhanVuongMac { set; get; }
        public string LHS_KienNghiDeXuat { set; get; }
        public DateTime? LHS_NgayHoanThanh { set; get; }

        public string THS_SoToTrinh { set; get; }
        public DateTime? LHS_NgayTrinh { set; get; }

        public string PDDA_SoQuyetDinh { set; get; }
        public DateTime? PDDA_NgayPheDuyet { set; get; }
        public double? PDDA_GiaTri { set; get; }

        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt
        public DateTime? NgayHoanThanh { set; get; }
        [ForeignKey("IdCongViec")]
        public virtual QLTD_CongViec QLTD_CongViec { get; set; }

        [ForeignKey("IdKeHoach")]
        public virtual QLTD_KeHoach QLTD_KeHoach { get; set; }

        [ForeignKey("IdKeHoachCongViec")]
        public virtual QLTD_KeHoachCongViec QLTD_KeHoachCongViec { get; set; }

        public virtual ICollection<QLTD_TDTH_ChuanBiThucHien> QLTD_TDTH_ChuanBiThucHien { get; set; }
    }
}