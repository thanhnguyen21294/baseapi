﻿using Model.Abstract;
using Model.Models.QLTD;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    public class QLTD_TDTH_ChuTruongDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTienDoThucHien { set; get; }

        [Required]
        public int IdChiTietCongViec { get; set; }

        public string NoiDung { set; get; }

        public string TenDonViThucHien { set; get; }

        public string DonViThamDinh { set; get; }

        public string DonViPhoiHop { set; get; }

        public string CoQuanPheDuyet { set; get; }

        public DateTime? NgayBatDau { set; get; }

        public DateTime? NgayHoanThanh { set; get; }
        public int HoanThanh { set; get; }// 0-Chưa hoàn thành; 1-Hoàn thành; 2-Phê duyệt

        public string GhiChu { set; get; }

        [ForeignKey("IdChiTietCongViec")]
        public virtual QLTD_CTCV_ChuTruongDauTu QLTD_CTCV_ChuTruongDauTu { get; set; }

        public virtual ICollection<QLTD_THCT_ChuTruongDauTu> QLTD_THCT_ChuTruongDauTu { get; set; }
    }
}