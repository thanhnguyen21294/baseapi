﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("NguonVonQuyetToanDuAns")]
    public class NguonVonQuyetToanDuAn: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonQuyetToanDuAn { get; set; }

        public int IdQuyetToanDuAn { get; set; }

        public int IdNguonVonDuAn { get; set; }

        public double GiaTri { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdQuyetToanDuAn")]
        public virtual QuyetToanDuAn QuyetToanDuAn { get; set; }
        [ForeignKey("IdNguonVonDuAn")]
        public virtual NguonVonDuAn NguonVonDuAn { get; set; }
    }
}
