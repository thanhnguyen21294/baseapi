﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVonDuAnGoiThaus")]
    public class NguonVonDuAnGoiThau : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonDuAnGoiThau { get; set; }

        [Required]
        public int IdGoiThau { get; set; }

        [Required]
        public int IdNguonVonDuAn { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdGoiThau")]
        public virtual GoiThau GoiThau { get; set; }
        [ForeignKey("IdNguonVonDuAn")]
        public virtual NguonVonDuAn NguonVonDuAn { get; set; }
        public virtual ICollection<NguonVonGoiThauThanhToan> NguonVonGoiThauThanhToans { get; set; }
        public virtual ICollection<NguonVonGoiThauTamUng> NguonVonGoiThauTamUngs { get; set; }
    }
}
