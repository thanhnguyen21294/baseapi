﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("QuyetToanHopDongs")]
    public class QuyetToanHopDong : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQuyetToan { set; get; }
        public int IdHopDong { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public DateTime? NgayQuyetToan { set; get; }
        public double? GiaTriQuyetToan { set; get; }
        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }
        public string GhiChu { set; get; }
        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }
    }
}
