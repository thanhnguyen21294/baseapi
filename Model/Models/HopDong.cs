﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("HopDongs")]
    public class HopDong: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHopDong { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenHopDong { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public int IdGoiThau { get; set; }

        [MaxLength(50)]
        public string SoHopDong { get; set; }

        public DateTime? NgayKy { get; set; }

        public int? ThoiGianThucHien { get; set; }

        public int? IdLoaiHopDong { get; set; }

        public double? GiaHopDong { get; set; }

        public double? GiaHopDongDieuChinh { get; set; }

        public string NoiDungVanTat { get; set; }

        public int? IdNhaThau { get; set; }

        public DateTime? NgayBatDau { get; set; }

        public DateTime? NgayHoanThanh { get; set; }

        public DateTime? NgayHoanThanhDieuChinh { get; set; }

        public DateTime? NgayThanhLyHopDong { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdNhaThau")]
        public virtual DanhMucNhaThau DanhMucNhaThau { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IdGoiThau")]
        public virtual GoiThau GoiThau { get; set; }
        [ForeignKey("IdLoaiHopDong")]
        public virtual LoaiHopDong LoaiHopDong { get; set; }
        public virtual ICollection<PhuLucHopDong> PhuLucHopDongs { get; set; }
        public virtual ICollection<ThucHienHopDong> ThucHienHopDongs { get; set; }
        public virtual ICollection<TamUngHopDong> TamUngHopDongs { get; set; }
        public virtual ICollection<ThanhToanHopDong> ThanhToanHopDongs { get; set; }
        public virtual ICollection<TienDoThucHien> TienDoThucHiens { get; set; }
        public virtual ICollection<QuyetToanHopDong> QuyetToanHopDongs { get; set; }
        public virtual ICollection<BaoLanhTHHD> BaoLanhTHHDs { get; set; }
    }
}
