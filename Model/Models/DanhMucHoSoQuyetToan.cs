﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("DanhMucHoSoQuyetToans")]
    public class DanhMucHoSoQuyetToan : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDanhMucHoSoQuyetToan { get; set; }
        public int? GiaiDoan { get; set; }
        public int? IdParent { get; set; }
        [Required]
        [MaxLength(255)]
        public string TenDanhMucHoSoQuyetToan { get; set; }
        public string GhiChu { get; set; }

        public int? Order { get; set; }
    }
}
