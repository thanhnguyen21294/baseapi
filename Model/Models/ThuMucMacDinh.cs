﻿using Model.Abstract;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("ThuMucMacDinhs")]
    public class ThuMucMacDinh : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThuMucMacDinh { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenThuMuc { set; get; }
        public int?  IdThuMucMacDinhCha { get; set; }
        public ICollection<ThuMuc> ThuMucs { get; set; }
    }
}
