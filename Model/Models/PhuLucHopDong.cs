﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("PhuLucHopDongs")]
    public class PhuLucHopDong : Auditable
    {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int IdPhuLuc { set; get; }

    public int IdHopDong { set; get; }

    [MaxLength(255)]
    [Required]
    public string TenPhuLuc { set; get; }
    public DateTime? NgayKy { set; get; }
    public double? GiaHopDongDieuChinh { set; get; }
    public DateTime? NgayHoanThanhDieuChinh { set; get; }
    public string NoiDung { set; get; }
    public string GhiChu { set; get; }
    [ForeignKey("IdHopDong")]
    public virtual HopDong HopDong { set; get; }
    }
}
