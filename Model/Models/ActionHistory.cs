﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("ActionHistories")]
    public class ActionHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHistory { set; get; }

        public string AppUser { get; set; }

        public string Action { get; set; }

        public string Function { get; set; }

        public string FunctionUrl { get; set; }

        public string ParentFunction { get; set; }

        public string ParentFunctionUrl { get; set; }

        public string Content { get; set; }

        public DateTime CreateDate { get; set; }

        public string FunctionID { get; set; }
    }
}