﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("TinhTrangDuAns")]
    public class TinhTrangDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTinhTrangDuAn { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenTinhTrangDuAn { set; get; }
        public int? IdTinhTrangDuAnCha { get; set; }
        public int? ThuTu { get; set; }
        public string GhiChu { set; get; }
        public virtual ICollection<DuAn> DuAns { set; get; }
        [ForeignKey("IdTinhTrangDuAnCha")]
        public virtual TinhTrangDuAn TinhTrangDuAnCha { set; get; }
    }
}
