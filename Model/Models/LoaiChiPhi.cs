﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("LoaiChiPhis")]
    public class LoaiChiPhi : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLoaiChiPhi { get; set; }
        public int? IdLoaiChiPhiCha { get; set; }
        [Required]
        [MaxLength(255)]
        public string TenLoaiChiPhi { get; set; }

        public string GhiChu { get; set; }

        public int? Order { get; set; }
    }
}
