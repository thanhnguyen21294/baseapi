﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("PhongCongTacs")]
    public class PhongCongTac : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChuDauTu { set; get; }

        public int IdDuAn { get; set; }

        public int IdNhomDuAnTheoUser { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { get; set; }
    }
}
