﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("PhieuXuLyCongViecNoiBos")]
    public class PhieuXuLyCongViecNoiBo : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPhieuXuLyCongViecNoiBo { set; get; }
        public string TenCongViec { get; set; }
        public string NoiDung { get; set; }
        public DateTime? NgayNhan { set; get; }
        public DateTime? NgayChuyen { set; get; }
        public int? IdDuAn { get; set; }
        public int? TrangThai { get; set; }
        public int? IDPhieuCha { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IDPhieuCha")]
        public virtual PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBoCha { get; set; }
        public string GhiChu { set; get; }
        public virtual ICollection<PhieuXuLyCongViecNoiBoUser> PhieuXuLyCongViecNoiBoUsers { get; set; }

        public virtual ICollection<PhieuXuLyCongViecNoiBo> Childrens { set; get; }
    }
}
