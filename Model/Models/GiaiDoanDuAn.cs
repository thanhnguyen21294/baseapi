﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("GiaiDoanDuAns")]
    public class GiaiDoanDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGiaiDoanDuAn { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenGiaiDoanDuAn { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<DuAn> DuAns { get; set; }
    }
}
