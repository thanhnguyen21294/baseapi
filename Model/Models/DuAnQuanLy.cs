﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("DuAnQuanLys")]
    public class DuAnQuanLy : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public int IdDuAn { set; get; }

        public int IdQuanLyDA { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

        [ForeignKey("IdQuanLyDA")]
        public virtual QuanLyDA QuanLyDA { get; set; }
    }
}

