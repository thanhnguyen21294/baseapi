﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("VanBanDenThucHiens")]
    public class VanBanDenThucHien : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdVanBanDenThucHien { set; get; }
        public int IdVanBanDen { set; get; }
        public string NoiDung { set; get; }
        [ForeignKey("IdVanBanDen")]
        public virtual VanBanDen VanBanDen { set; get; }
        
    }

}
