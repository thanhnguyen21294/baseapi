﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("LapQuanLyDauTus")]
    public class LapQuanLyDauTu: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLapQuanLyDauTu { get; set; }
        public int LoaiQuanLyDauTu { get; set; }
        public int IdDuAn { get; set; }

        [MaxLength(255)]
        public string NoiDung { get; set; }

        public DateTime? NgayTrinh { get; set; }

        [MaxLength(255)]
        public string SoToTrinh { get; set; }

        public double? GiaTriTrinh { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        [MaxLength(255)]
        public string SoQuyetDinh { get; set; }

        [MaxLength(255)]
        public string NguoiPheDuyet { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        public int? LoaiDieuChinh { get; set; }

        public double? TongGiaTri { get; set; }

        public double? XayLap { get; set; }

        public double? ThietBi { get; set; }

        public double? GiaiPhongMatBang { get; set; }

        public double? TuVan { get; set; }

        public double? QuanLyDuAn { get; set; }

        public double? Khac { get; set; }

        public double? DuPhong { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("IdCoQuanPheDuyet")]
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        public virtual ICollection<ThamDinhLapQuanLyDauTu> ThamDinhLapQuanLyDauTus { get; set; }
        public virtual ICollection<NguonVonDuAn> NguonVonDuAns { get; set; }

    }
}
