﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("SystemLogs")]
    public class SystemLogs : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdSystemLogs { set; get; }

        public string TenChucNang { set; get; }
        public string ThaoTac { set; get; }
        public string NguoiThaoTac { set; get; }
    }
}
