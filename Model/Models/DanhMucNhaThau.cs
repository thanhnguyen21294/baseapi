﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("DanhMucNhaThaus")]
    public class DanhMucNhaThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNhaThau { get; set; }

        [Required]
        public string TenNhaThau { get; set; }

        public string DiaChiNhaThau { get; set; }

        public string MoTa { get; set; }

        public int IdDuAn { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        public virtual ICollection<ThanhToanChiPhiKhac> ThanhToanChiPhiKhacs { get; set; }
        public virtual ICollection<HopDong> HopDongs { get; set; }
        public virtual ICollection<KetQuaDauThau> KetQuaDauThaus { get; set; }
    }
}
