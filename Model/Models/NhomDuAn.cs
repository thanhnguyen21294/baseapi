﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("NhomDuAns")]
    public class NhomDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNhomDuAn { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenNhomDuAn { set; get; }
        public string GhiChu { set; get; }
        public virtual ICollection<DuAn> DuAns { set; get; }
    }
}
