﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVonDuAns")]
    public class NguonVonDuAn: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonDuAn { get; set; }

        public int IdDuAn { get; set; }

        public int IdNguonVon { get; set; }

        public double? GiaTri { get; set; }
        public double? GiaTriTrungHan { get; set; }

        public int? IdLapQuanLyDauTu { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IdNguonVon")]
        public virtual NguonVon NguonVon { get; set; }
        [ForeignKey("IdLapQuanLyDauTu")]
        public virtual LapQuanLyDauTu LapQuanLyDauTu { get; set; }
        //public virtual ICollection<NguonVonQuyetToanDuAn> NguonVonQuyetToanDuAns { get; set; }
        //public virtual ICollection<NguonVonThanhToanChiPhiKhac> NguonVonThanhToanChiPhiKhacs { get; set; }
        public virtual ICollection<NguonVonKeHoachVon> NguonVonKeHoachVons { get; set; }
        public virtual ICollection<NguonVonDuAnGoiThau> NguonVonDuAnGoiThaus { get; set; }
    }
}
