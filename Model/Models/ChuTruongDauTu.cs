﻿using Model.Abstract;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("ChuTruongDauTus")]
    public class ChuTruongDauTu: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChuTruongDauTu { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        [Required]
        public string SoVanBan { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        [MaxLength(255)]
        public string NguoiPheDuyet { get; set; }

        public double? TongMucDauTu { get; set; }

        public double? KinhPhi { get; set; }

        public string LoaiDieuChinh { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IdCoQuanPheDuyet")]
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
    }
}
