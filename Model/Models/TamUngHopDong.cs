﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("TamUngHopDongs")]
    public class TamUngHopDong : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTamUng { set; get; }
        public int IdHopDong { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public DateTime? NgayTamUng { set; get; }
        public string GhiChu { set; get; }
        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }
        public virtual ICollection<NguonVonGoiThauTamUng> NguonVonGoiThauTamUngs { set; get; }
    }
}
