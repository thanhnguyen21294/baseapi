﻿using Model.Abstract;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("KetQuaDauThaus")]
    public class KetQuaDauThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKetQuaLuaChonNhaThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string NoiDung { get; set; }
        public int IdDuAn { get; set; }
        public int IdGoiThau { get; set; }

        public DateTime? NgayLapBienBanMoThau { get; set; }

        public DateTime? NgayLapBaoCaoDanhGia { get; set; }

        public DateTime? NgayLapBienBanThuongThao { get; set; }

        public DateTime? NgayLapBaoCaoThamDinh { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public DateTime? NgayDangTai { get; set; }

        [MaxLength(50)]
        public string SoDangTai { get; set; }

        [MaxLength(50)]
        public string PhuongTienDangTai { get; set; }

        public DateTime? NgayGuiThongBao { get; set; }

        public double? GiaDuThau { get; set; }

        public double? GiaTrungThau { get; set; }

        public int? IdNhaThau { get; set; }

        public string GhiChu { get; set; }

        [ForeignKey("IdNhaThau")]
        public virtual DanhMucNhaThau DanhMucNhaThau { get; set; }
        [ForeignKey("IdGoiThau")]
        public virtual GoiThau GoiThau { get; set; }

    }
}
