﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("HinhThucLuaChonNhaThaus")]
    public class HinhThucLuaChonNhaThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHinhThucLuaChon { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenHinhThucLuaChon { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<GoiThau> GoiThaus { get; set; }
    }
}
