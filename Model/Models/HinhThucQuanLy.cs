﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("HinhThucQuanLys")]
    public class HinhThucQuanLy: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHinhThucQuanLy { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenHinhThucQuanLy { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<DuAn> DuAns { get; set; }
    }
}
