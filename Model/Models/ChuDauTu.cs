﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("ChuDauTus")]
    public class ChuDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChuDauTu { set; get; }

        [Required]
        [MaxLength(255)]        
        public string TenChuDauTu { set; get; }

        [MaxLength(255)]
        public string DiaChi { set; get; }

        public string GhiChu { set; get; }

        public virtual ICollection<DuAn> DuAns { set; get; }
    }
}
