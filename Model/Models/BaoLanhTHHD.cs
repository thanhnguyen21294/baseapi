﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("BaoLanhTHHDs")]
    public class BaoLanhTHHD : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdBaoLanh { set; get; }

        [Required]
        public int IdDuAn { set; get; }

        [Required]
        public int IdHopDong { set; get; }

        public int LoaiBaoLanh { set; get; }

        [Required]
        [MaxLength(255)]
        public string SoBaoLanh { set; get; }

        public DateTime? NgayBaoLanh { set; get; }

        public DateTime? NgayHetHan { set; get; }

        public double? SoTien { get; set; }

        public string GhiChu { set; get; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }


        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { set; get; }

        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }

        
    }
}