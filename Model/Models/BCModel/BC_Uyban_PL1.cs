﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BC_UyBan_PL1_Them
    {
        public double TongMucDauTu { get; set; }
        public string PDDA_BCKTKT_SoQD { get; set; }
        public DateTime? PDDA_BCKTKT_NgayPD { get; set; }
        public double? PDDA_BCKTKT_GiaTri { get; set; }
    }
    public class BC_Uyban_PL1_LVNN
    {
        public int? IDLVNN { get; set; }
        public string TenLVNN { get; set; }
        public int? Order { get; set; }
        public IEnumerable<BC_Uyban_PL1_TTDA> grpTTDA { get; set; }
    }

    public class BC_Uyban_PL1_TTDA
    {
        public int? IDTTDA { get; set; }
        public string TenTTDA { get; set; }
        public IEnumerable<BC_Uyban_PL1_DA_Cha> grpDACha { get; set; }
    }

    public class BC_Uyban_PL1_DA_Cha
    {
        public int IDDACha { get; set; }
        public string TenDACha { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string TenChuDauTu { get; set; }
        public IEnumerable<BC_Uyban_PL1_DA_Con> grpDACon { get; set; }
    }

    public class BC_Uyban_PL1_DA_Con
    {
        public int IDDA { get; set; }
        public string TenDACon { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string TenChuDauTu { get; set; }
    }

    public class BC_Uyban_PL1_KHV
    {
        public int IDDA { get; set; }
        public double? KHV { get; set; }
    }

    public class BC_Uyban_PL1_GN
    {
        public int? IDDA { get; set; }
        public string GTGN { get; set; }
    }

    public class BC_Uyban_PL1_PDDA
    {
        public int IDDA { get; set; }
    }

    public class BC_Uyban_PL1_KHTDC
    {
        public int IDDA { get; set; }
        public string NoiDung { get; set; }
    }

    public class BC_Uyban_PL1_KHTDC_New
    {
        public int IDKH { get; set; }
        public int IDDA { get; set; }
        public string NoiDung { get; set; }
    }

    public class BC_Uyban_PL1_KKGP
    {
        public int IDDA { get; set; }
        public IEnumerable<string> KK { get; set; }
        public IEnumerable<string> GP { get; set; }
    }
    public class BC_Uyban_PL1_KKGP_New
    {
        public int IDKH { get; set; }
        public int IDDA { get; set; }
        public IEnumerable<string> KK { get; set; }
        public IEnumerable<string> GP { get; set; }
    }

    public class BC_TienDo_KK_QT
    {
        public int IDDA { get; set; }
        public IEnumerable<BC_TienDo_KK_QT_NoiDung> grpKKVM { get; set; }
    }

    public class BC_TienDo_KK_QT_NoiDung
    {
        public string KKVM { get; set; }
    }


}
