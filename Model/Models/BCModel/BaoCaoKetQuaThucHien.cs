﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BaoCaoKetQuaThucHien
    {
        public int IDDuAn { get; set; }
        public string TenDA { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public List<KH> grpKH { get; set; }
    }

    public class Users
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class KH
    {
        public int IDKH { get; set; }
        public string TenKH { get; set; }
        public List<CV> grpCV { get; set; }
    }

    public class CV
    {
        public string TenCV { get; set; }
        public DateTime NgayHoanThanh { get; set; }
    }
}
