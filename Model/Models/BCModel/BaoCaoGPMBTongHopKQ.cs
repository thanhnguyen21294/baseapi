﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BaoCaoGPMBTongHopKQ
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }

        public double? TongQuan_TongDT { get; set; }
        public double? TongQuan_TongSH { get; set; }
        public double? TongQuan_NNCaThe_DT { get; set; }
        public double? TongQuan_NNCaThe_SH { get; set; }
        public double? TongQuan_NNCong_DT { get; set; }
        public double? TongQuan_NNCong_SH { get; set; }
        public double? TongQuan_DatKhac_DT { get; set; }
        public double? TongQuan_DatKhac_SH { get; set; }
        public double? TongQuan_MoMa { get; set; }

        public double? DaHT_DT { get; set; }
        public double? DaHT_SH { get; set; }
        public double? DaHT_TGT { get; set; }
        public double? DaHT_NNCaThe_DT { get; set; }
        public double? DaHT_NNCaThe_SH { get; set; }
        public double? DaHT_NNCong_DT { get; set; }
        public double? DaHT_NNCong_SH { get; set; }
        public double? DaHT_DatKhac_DT { get; set; }
        public double? DaHT_DatKhac_SH { get; set; }
        public double? DaHT_MoMa { get; set; }

        public double? CTHX_DT { get; set; }
        public double? CTHX_SH { get; set; }
        public double? CTHX_HTDTPA_DT { get; set; }
        public double? CTHX_HTDTPA_SH { get; set; }
        public double? CTHX_CKDTPA_DT { get; set; }
        public double? CTHX_CKDTPA_SH { get; set; }
        public double? CTHX_HTPACT_DT { get; set; }
        public double? CTHX_HTPACT_SH { get; set; }
        public double? CTHX_QDPDDA_DT { get; set; }
        public double? CTHX_QDPDDA_SH { get; set; }
        public double? CTHX_NBGD_DT { get; set; }
        public double? CTHX_NBGD_SH { get; set; }

        public double? KKVM_DT { get; set; }
        public double? KKVM_SH { get; set; }
        public double? KKVM_SH_CNT { get; set; }
        public double? KKVM_SH_KHTDT { get; set; }
        public double? KKVM_SH_CDT { get; set; }

        public string GhiChu { get; set; }
    }
}
