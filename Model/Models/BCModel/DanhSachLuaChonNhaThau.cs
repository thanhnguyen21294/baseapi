﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class DanhSachLuaChonNhaThau
    {
        public int? IDLinhVuc { get; set; }
        public string TenLinhVuc { get; set; }
        public IEnumerable<GiaiDoanLCNT> grpGiaiDoan { get; set; }
    }

    //public class GiaiDoanLCNT
    //{
    //    public int? IDGiaiDoan { get; set; }
    //    public string TenGiaiDoan { get; set; }
    //    public IEnumerable<GoiThauLuaChonNhaThau> grpGoiThau { get; set; }
    //}

    public class GiaiDoanLCNT
    {
        public int? IDGiaiDoan { get; set; }
        public string TenGiaiDoan { get; set; }
        public IEnumerable<DuAnLuaChonNhaThau> grpDuAn { get; set; }
    }

    //public class GoiThauLuaChonNhaThau
    //{
    //    public int IDDA { get; set; }
    //    public string TenDuAn { get; set; }
    //    public IEnumerable<Users> Users { get; set; }
    //    public string NhaThauThucHien { get; set; }
    //    public string TuVanGiamSat { get; set; }
    //    public string TuNgay { get; set; }
    //    public string DenNgay { get; set; }
    //    public int? LoaiGoiThau { get; set; }
    //}

    public class DuAnLuaChonNhaThau
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public int? OrderDuAn { get; set; }
        public int? OrderTemp { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<GoiThauLuaChonNhaThau> grpGoiThau { get; set; }
    }

    public class GoiThauLuaChonNhaThau
    {
        public string NhaThauThucHien { get; set; }
        public string TuVanGiamSat { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public int? LoaiGoiThau { get; set; }
    }

}
