﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BC_LocDuAn_PhanQuyen_PhongCT
    {
        public int? IDPhong { get; set; }
        public IEnumerable<BC_LocDuAn_PhanQuyen_DuAn_Cha> grpDuAnCha { get; set; }
    }

    public class BC_LocDuAn_PhanQuyen_DuAn_Cha
    {
        public int IDDA { get; set; }
        public string TenDuAnCha { get; set; }
        public string MaDuAnCha { get; set; }
        public string TenNhomDuAnCha { get; set; }
        public string TenLVNNCha { get; set; }
        public string TenGDCha { get; set; }
        public string ThoiGianThucHienCha { get; set; }
        public string QuanLyCha { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<BC_LocDuAn_PhanQuyen_DuAn> grpDuAnCon { get; set; }
    }

    public class BC_LocDuAn_PhanQuyen_DuAn
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public string MaDuAn { get; set; }
        public string TenNhomDuAn { get; set; }
        public string TenLVNN { get; set; }
        public string TenGD { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string QuanLy { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }
}
