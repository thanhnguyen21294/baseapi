﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel.GPMB_KeHoachTienDo
{
    public class GPMB_KeHoachTienDo_BC
    {
        public string TenDuAn { get; set; }

        public string CBTH_VanBanGiaoNhiemVu_SoVB { get; set; }
        public string CBTH_VanBanGiaoNhiemVu_NgayQD { get; set; }

        public string CBTH_BienBanBanGiaoMocGioi_NgayQD { get; set; }

        public string CBTH_QuyetDinhThanhLapHoiDong_SoVB { get; set; }
        public string CBTH_QuyetDinhThanhLapHoiDong_NgayQD { get; set; }
        public string CBTH_QuyetDinhTCT_SoVB { get; set; }
        public string CBTH_QuyetDinhTCT_NgayQD { get; set; }

        public string CBTH_QuyetDinhPheDuyetDuToan_SoVB { get; set; }
        public string CBTH_QuyetDinhPheDuyetDuToan_NgayQD { get; set; }

        public string CBTH_HopHoiDong_HHD { get; set; }
        public string CBTH_HopHoiDong_HDU { get; set; }

        public string CBTH_TrichDoHienTrang_Chon { get; set; }

        public string CBTH_ThongBaoThuHoiDat_SoVB { get; set; }
        
        public string TH_HopDan_Chon { get; set; }
        
        public string TH_DieuTraKeKhai_SoVB { get; set; }
        
        public string TH_CuongCheKiemDem_Chon { get; set; }
        
        public string TH_LapHoSoGPMB_SoVB { get; set; }
        
        public string TH_LapTrinhHoiDongThamTra_SoVB { get; set; }
        
        public string TH_CongKhaiDuThao_SoVB { get; set; }

        public string TH_TrinhHoiDongThamDinh_SoVB { get; set; }

        public string TH_QuyetDinhThuHoiDat_SoVB { get; set; }
        public string TH_QuyetDinhThuHoiDat_GiaTri { get; set; }

        public string TH_ChiTraTien_SoVB { get; set; }
        public string TH_ChiTraTien_GiaTri { get; set; }

        public string TH_CuongCheThuHoiDat_Chon { get; set; }

        public string TH_QuyetToanKinhPhi_SoVB { get; set; }
        public string TH_QuyetToanKinhPhi_NgayQD { get; set; }
        public string TH_QuyetToanKinhPhi_GiaTri { get; set; }

        public string TH_LapHoSoDeNghiTP_SoVB { get; set; }
        public string TH_LapHoSoDeNghiTP_NgayQD { get; set; }

        public string GhiChu { get; set; }
    }
}
