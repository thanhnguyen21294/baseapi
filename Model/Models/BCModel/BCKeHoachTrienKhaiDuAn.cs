﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BCKeHoachTrienKhaiDuAn
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public string TenPhong { get; set; }
        public int? OrderTemp { get; set; }
        public int? Order { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<BCNhomGiaiDoanTrienKhaiDA> grpNhomGiaiDoan { get; set; }
    }

    public class BCNhomGiaiDoanTrienKhaiDA
    {
        public string TenNhomGiaiDoan { get; set; }
        public IEnumerable<BCGiaiDoanTrienKhaiDA> grpGiaiDoan { get; set; }
    }

    public class BCGiaiDoanTrienKhaiDA
    {
        public int IDGiaiDoan { get; set; }
        public string TenGiaiDoan { get; set; }
        public IEnumerable<BCCongViecTrienKhaiDA> grpCongViec { get; set; }
    }

    public class BCCongViecTrienKhaiDA
    {
        public string TenCongViec { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public int IDCV { get; set; }
        public int IDKH { get; set; }
        public int HoanThanh { get; set; }
    }

    public class BC_TrienKhaiDA
    {
        public int IDKH { get; set; }
        public int IDDA { get; set; }
        public int IDNhomGiaiDoan { get; set; }
        public string TenNhomGiaiDoan { get; set; }
        public string TenDuAn { get; set; }
        public int IDGiaiDoan { get; set; }
        public string TenGiaiDoan { get; set; }
        public string TenCongViec { get; set; }
        public DateTime? Tungay { get; set; }
        public DateTime? DenNgay { get; set; }
    }
    
    public class BC_ThucHien_TrienKhaiDA
    {
        public string NoiDung1 { get; set; }
        public string NoiDung2 { get; set; }
        public string NoiDung3 { get; set; }
        public string NoiDung4 { get; set; }
        public string NoiDung5 { get; set; }
        public string NoiDung6 { get; set; }
        public string NoiDung7 { get; set; }
        public string NoiDung8 { get; set; }
        public string NoiDung9 { get; set; }
        public string NoiDung10 { get; set; }
    }
}
