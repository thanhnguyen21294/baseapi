﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class KetQuaHoanThanhNhungCham
    {
        public int IDDuAn { get; set; }
        public string TenDA { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public List<KHHoanThanhNhungCham> grpKH { get; set; }
    }

    public class KHHoanThanhNhungCham
    {
        public int IDKH { get; set; }
        public string TenKH { get; set; }
        public List<CVHoanThanhNhungCham> grpCV { get; set; }
    }

    public class CVHoanThanhNhungCham
    {
        public string TenCV { get; set; }
        public DateTime NgayHoanThanhKH { get; set; }
        public DateTime NgayHoanThanh { get; set; }
    }


}
