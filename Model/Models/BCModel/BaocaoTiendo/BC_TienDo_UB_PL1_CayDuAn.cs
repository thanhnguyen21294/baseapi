﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel.BaocaoTiendo
{
    public class BC_TienDo_UB_PL1_CayDuAn
    {
        public int? IDLVNN { get; set; }
        public int? Order { get; set; }
        public string TenLVNN { get; set; }
        public string DiaDiem { get; set; }
        public IEnumerable<BC_TienDo_UB_PL1_TTDA> grpTTDA { get; set; }
    }

    public class BC_TienDo_UB_PL1_TTDA
    {
        public int? IDTTDA { get; set; }
        public string TenTTDA { get; set; }
        public string DiaDiem { get; set; }
        public IEnumerable<BC_TienDo_UB_PL1_DACha> grpDACha { get; set; }
    }

    public class BC_TienDo_UB_PL1_DACha
    {
        public int IDDACha { get; set; }
        public string TenDACha { get; set; }
        public string DiaDiem { get; set; }
        public string ThoiGianThucHienCha { get; set; }
        public string TenChuDauTuCha { get; set; }
        public string TenPhongCha { get; set; }
        public int? OrderDACha { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<BC_TienDo_UB_PL1_DACon> grpDACon { get; set; }
    }

    public class BC_TienDo_UB_PL1_DACon
    {
        public int IDDA { get; set; }
        public string TenDA { get; set; }
        public string DiaDiem { get; set; }
        public int? OrderDA { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string TenChuDauTu { get; set; }
        public string TenPhong { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }

    public class BC_TienDo_KHV
    {
        public int IDDA { get; set; }
        public double? dGiaTri { get; set; }
    }

    public class BC_TienDo_KHV_New
    {
        public int IDDA { get; set; }
        public double? dGiaTri { get; set; }
        public double? dGiaTriHuyen { get; set; }
        public double? dGiaTriTP { get; set; }
    }

    public class BC_TienDo_GN
    {
        public string IDDA { get; set; }
        public string dGiaTriGN { get; set; }
    }

    public class BC_ListID
    {
        public int IDDA { get; set; }
    }
}
