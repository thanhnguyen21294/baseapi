﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel.BaocaoTiendo
{
    public class BC_TienDo_LCTDT_ThongTin_LCTDT
    {
        public int IDDA { get; set; }
        public bool ChapThuanDD_CoSanDiaDiem { get; set; }
        public string ChapThuanDD_SoVB { get; set; }
        public DateTime? ChapThuanDD_NgayPD { get; set; }
        public IEnumerable<string> LCTDT_TienDoThucHien { get; set; }
        public string LCTDT_KKVM { get; set; }
        public string LCTDT_KNDX { get; set; }
        public DateTime? NgayTrinhHS { get; set; }
        public string PDCTDT_SoVB { get; set; }
        public DateTime? PDCTDT_NgayPD { get; set; }
        public double? PDCTDT_TongMucDauTu { get; set; }
        public DateTime? PDCTDT_NgayPDTT { get; set; }
    }

    public class BC_TienDo_LCTDT_ThoiGianHoanThanh
    {
        public int IDDA { get; set; }
        public int ThoiGianHoanThanh_Nam { get; set; }
    }

    public class BC_TienDo_LCTDT_NgayGiaoNhiemVu
    {
        public int IDDA { get; set; }
        public DateTime? NgayGiaoNVu_LCTDT { get; set; }
    }

    public class BC_TienDo_LCTDT_TDC
    {
        public int IDDA { get; set; }
        public string NoiDung { get; set; }
    }

    public class BC_TienDo_LCTDT_KK_GP
    {
        public int IDDA { get; set; }
        public IEnumerable<string> KK { get; set; }
        public IEnumerable<string> GP { get; set; }
    }
}
