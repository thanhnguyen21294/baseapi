﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel.BaocaoTiendo
{
    public class BC_TienDo_Phong_CanBo_QuanLy
    {
        public int IDDA { get; set; }
        public string TenPhong { get; set; }
        public IEnumerable<Users> Users { get; set; }
    }

}
