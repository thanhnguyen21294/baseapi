﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class KetQuaKhongNhapLieu
    {
        public int IDDuAn { get; set; }
        public string TenDA { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public List<KHKhongNhapLieu> grpKH { get; set; }
    }

    public class KHKhongNhapLieu
    {
        public int IDKH { get; set; }
        public string TenKH { get; set; }
        public DateTime TuNgay { get; set; }
        public DateTime NgayHT { get; set; }
        public List<DateTime> lstDate { get; set; }
        public List<TDTH> grpTDTH { get; set; }
    }

    public class TDTH
    {
        public int IDTienDoThucHien { get; set; }
        public string NoiDung { get; set; }
        public List<DateTime> grpNgayBam { get; set; }
    }
}
