﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class KetQuaChuaHoanThanh
    {
        public int IDDuAn { get; set; }
        public string TenDA { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public List<KHChuaHoanThanh> grpKH { get; set; }
    }

    public class KHChuaHoanThanh
    {
        public int IDKH { get; set; }
        public string TenKH { get; set; }
        public List<CVChuaHoanThanh> grpCV { get; set; }
    }

    public class CVChuaHoanThanh
    {
        public string TenCV { get; set; }
        public string DenNgay { get; set; }
    }
}
