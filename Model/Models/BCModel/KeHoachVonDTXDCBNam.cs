﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class KeHoachVonDTXDCBNam
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public int? IDChuDauTu { get; set; }
        public string TenChuDauTu { get; set; }
        public int? IDTinhTrangDuAn { get; set; }
        public string TenTinhTrangDuAn { get; set; }
        public int? IDLinhVucNganhNghe { get; set; }
        public string TenLinhVucNganhNghe { get; set; }
        public string DiaDiemXayDung { get; set; }
        public string NangLucThietKe { get; set; }
        public string QuyetDinhDauTu { get; set; }
        public double? TongMucDauTuCuoiCung { get; set; }
        public string ThoiGianKhoiCongHoanThanh { get; set; }

        public double? KhoiLuongThucHienDenNgay { get; set; }
        public double? KhoiLuongThucHienCaNam { get; set; }

        public double? KeHoachVonNam_1NSTP { get; set; }
        public double? KeHoachVonNam_1SDDDD { get; set; }
        public double? KeHoachVonNam_1NSTT { get; set; }
        public double? KeHoachVonNam_1NST { get; set; }
        public double? KeHoachVonNam_1ODA { get; set; }
        public double? KeHoachVonNam_1TPCP { get; set; }

        public double? GiaiNganDenNgayNamN_2 { get; set; }
        public double? GiaiNganNamN_1NSTP { get; set; }
        public double? GiaiNganNamN_1SDDDD { get; set; }
        public double? GiaiNganNamN_1NSTT { get; set; }
        public double? GiaiNganNamN_1NST { get; set; }
        public double? GiaiNganNamN_1ODA { get; set; }
        public double? GiaiNganNamN_1TPCP { get; set; }

        public double? KeHoachVonNamNSTP { get; set; }
        public double? KeHoachVonNamSDDDD { get; set; }
        public double? KeHoachVonNamNSTT { get; set; }
        public double? KeHoachVonNamNST { get; set; }
        public double? KeHoachVonNamODA { get; set; }
        public double? KeHoachVonNamTPCP { get; set; }
        
        public string GhiChu { get; set; }
    }

    public class KeHoachVonDTXDCBNamCon
    {
        public int? IDDACha { get; set; }
        public string TenDuAnCon { get; set; }
        public int IDDACon { get; set; }

        public string DiaDiemXayDungCon { get; set; }
        public string NangLucThietKeCon { get; set; }
        public string QuyetDinhDauTuCon { get; set; }
        public double? TongMucDauTuCuoiCungCon { get; set; }
        public string ThoiGianKhoiCongHoanThanhCon { get; set; }

        public double? KhoiLuongThucHienDenNgayCon { get; set; }
        public double? KhoiLuongThucHienCaNamCon { get; set; }

        public double? KeHoachVonNam_1NSTPCon { get; set; }
        public double? KeHoachVonNam_1SDDDDCon { get; set; }
        public double? KeHoachVonNam_1NSTTCon { get; set; }
        public double? KeHoachVonNam_1NSTCon { get; set; }
        public double? KeHoachVonNam_1ODACon { get; set; }
        public double? KeHoachVonNam_1TPCPCon { get; set; }

        public double? GiaiNganDenNgayNamN_2Con { get; set; }
        public double? GiaiNganNamN_1NSTPCon { get; set; }
        public double? GiaiNganNamN_1SDDDDCon { get; set; }
        public double? GiaiNganNamN_1NSTTCon { get; set; }
        public double? GiaiNganNamN_1NSTCon { get; set; }
        public double? GiaiNganNamN_1ODACon { get; set; }
        public double? GiaiNganNamN_1TPCPCon { get; set; }

        public double? KeHoachVonNamNSTPCon { get; set; }
        public double? KeHoachVonNamSDDDDCon { get; set; }
        public double? KeHoachVonNamNSTTCon { get; set; }
        public double? KeHoachVonNamNSTCon { get; set; }
        public double? KeHoachVonNamODACon { get; set; }
        public double? KeHoachVonNamTPCPCon { get; set; }
    }
        
}
