﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BC_KhoKhanVuongMac
    {
        public int? IDPhong { get; set; }
        public IEnumerable<BC_KhoKhanVuongMac_DuAn> grpDA { get; set; }
    }

    public class BC_KhoKhanVuongMac_DuAn
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public IEnumerable<BC_KhoKhanVuongMac_GiaiDoan> grpGiaiDoan { get; set; }
    }

    public class BC_KhoKhanVuongMac_GiaiDoan
    {
        public int IDGD { get; set; }
        public string TenGiaiDoan { get; set; }
        public IEnumerable<BC_KhoKhanVuongMac_Kehoach> grpKH { get; set; }
    }

    public class BC_KhoKhanVuongMac_Kehoach
    {
        public int IDKH { get; set; }
    }

    //public class KKGiaiDoan
    //{
    //    public int IDKH { get; set; }
    //    public string TenGiaiDoan { get; set; }
    //    public int total { get; set; }
    //    public List<QLTD_KhoKhanVuongMac> lstKK { get; set; }
    //}
}
