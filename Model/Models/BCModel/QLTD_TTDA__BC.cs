﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class QLTD_TTDA__BC
    {
        public int IDDA { get; set; }
        public int? IDLinhVucNganhNghe { get; set; }
        public string LinhVucNganhNghe { get; set; }

        public int? IDGiaiDoanDuAn { get; set; }
        public string GiaiDoanDuAn { get; set; }

        public int? IDTinhTrangDuAn { get; set; }
        public string TinhTrangDuAn { get; set; }

        public string TenDuAn { get; set; }
        public string NhomDuAn { get; set; }
        public string DiaDiemXayDung { get; set; }
        public string NangLucThietKe { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string MaSoDuAn { get; set; }
        public string TenChuDauTu { get; set; }


        //IDDA = item.IdDuAn,
        //                          IDLinhVucNganhNghe = item.IdLinhVucNganhNghe,
        //                          LinhVucNganhNghe = item.LinhVucNganhNghe.TenLinhVucNganhNghe,
        //                          IDGiaiDoanDuAn = item.IdGiaiDoanDuAn,
        //                          GiaiDoanDuAn = item.GiaiDoanDuAn.TenGiaiDoanDuAn,
        //                          IDTinhTrangDuAn = item.IdTinhTrangDuAn,
        //                          TinhTrangDuAn = item.TinhTrangDuAn.TenTinhTrangDuAn,
        //                          TenDuAn = item.TenDuAn == null ? "" : item.TenDuAn,
        //                          NhomDuAn = item.NhomDuAn.TenNhomDuAn == null ? "" : item.NhomDuAn.TenNhomDuAn,
        //                          DiaDiemXayDung = item.DiaDiem == null ? "" : item.DiaDiem,
        //                          NangLucThietKe = item.QuyMoNangLuc == null ? "" : item.QuyMoNangLuc,
        //                          ThoiGianThucHien = item.ThoiGianThucHien == null ? "" : item.ThoiGianThucHien,
        //                          MaSoDuAn = item.Ma == null ? "" : item.Ma,
        //                          TenChuDauTu = item.ChuDauTu.TenChuDauTu == null ? "" : item.ChuDauTu.TenChuDauTu
    }
}
