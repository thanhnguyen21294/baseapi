﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.BCModel
{
    public class BC_ThucHien_KeHoachTrienKhaiDuAn_NguonVon
    {
        public string TenNV { get; set; }
        public int STT { get; set; }
        public IEnumerable<BC_ThucHien_KeHoachTrienKhaiDuAn_DuAn> grpDuAn { get; set; }
    }
    
    public class BC_ThucHien_KeHoachTrienKhaiDuAn_DuAn
    {
        public int? IDDA { get; set; }
        public string TenDuAn { get; set; }
        public string TenPhong { get; set; }
        public IEnumerable<Users> Users { get; set; }
        public string GiaTriNV { get; set; }
    }

    public class BC_ThucHien_KeHoachTrienKhaiDuAn_TDTH
    {
        public string ND { get; set; }
    }

    public class BC_KeHoachTrienKhaiDuAn_TDGN
    {
        public int? IDDA { get; set; }
        public string MaDuAn { get; set; }
        public string TDGNHuyen { get; set; }
        public string TDGNTP { get; set; }
    }
}
