﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("HoSoMoiThaus")]
    public class HoSoMoiThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHoSoMoiThau { get; set; }

        public string NoiDung { get; set; }

        public int? IdDuAn { get; set; }

        public int? IdGoiThau { get; set; }

        public DateTime? NgayTrinh { get; set; }

        [MaxLength(50)]
        public string SoVanBanTrinh { get; set; }

        public DateTime? NgayNhanDu { get; set; }

        public DateTime? NgayThamDinh { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public DateTime? NgayDangTai { get; set; }

        [MaxLength(50)]
        public string SoDangTai { get; set; }

        [MaxLength(255)]
        public string PhuongTienDangTai { get; set; }

        public DateTime? NgayPhatHanh { get; set; }

        [MaxLength(50)]
        public string SoPhatHanh { get; set; }

        public DateTime? NgayHetHanHieuLuc { get; set; }

        public DateTime? NgayHetHanBaoLanh { get; set; }

        public DateTime? NgayLapBienBan { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdGoiThau")]
        public virtual GoiThau GoiThau { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { set; get; }
    }
}
