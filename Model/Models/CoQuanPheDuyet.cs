﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("CoQuanPheDuyets")]
    public class CoQuanPheDuyet : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCoQuanPheDuyet { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenCoQuanPheDuyet { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<LapQuanLyDauTu> LapQuanLyDauTus { get; set; }
        public virtual ICollection<ChuTruongDauTu> ChuTruongDauTus { get; set; }
        public virtual ICollection<QuyetToanDuAn> QuyetToanDuAns { get; set; }
        public virtual ICollection<KeHoachVon> KeHoachVons { get; set; }
        public virtual ICollection<KeHoachLuaChonNhaThau> KeHoachLuaChonNhaThaus { get; set; }
    }
}
