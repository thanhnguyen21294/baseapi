﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("QuanLyDAs")]
    public class QuanLyDA : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQuanLyDA { set; get; }

        [Required]
        public string NoiDung { set; get; }

        public string GhiChu { set; get; }
    }
}
