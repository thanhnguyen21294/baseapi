﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("ThanhToanHopDongs")]
    public class ThanhToanHopDong : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThanhToanHopDong { set; get; }
        public int IdHopDong { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public int? IdThucHien { set; get; }
        public double? DeNghiThanhToan { set; get; }
        public DateTime? ThoiDiemThanhToan { set; get; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }

        public string GhiChu { set; get; }
        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }
        [ForeignKey("IdThucHien")]
        public virtual ThucHienHopDong ThucHienHopDong { set; get; }
        public virtual ICollection<NguonVonGoiThauThanhToan> NguonVonGoiThauThanhToans { set; get; }
    }
}
