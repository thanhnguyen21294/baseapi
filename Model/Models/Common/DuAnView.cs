﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Model.Models.Common
{
    public class DuAnView
    {
        public int IdDuAn { set; get; }
        
        [MaxLength(50)]
        public string Ma { set; get; }

        [MaxLength(255)]
        public string TenDuAn { set; get; }

        public string NhomDuAn { set; get; }

        public string GiaiDoanDuAn { set; get; }

        public string ThoiGianThucHien { set; get; }
        

    }
}
