﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("AppUsers")]
    public class AppUser : IdentityUser
    {
        public int? IdNhomDuAnTheoUser { set; get; }
        [MaxLength(256)]
        public string FullName { set; get; }

        [MaxLength(256)]
        public string Address { set; get; }

        public string Avatar { get; set; }

        public DateTime? BirthDay { set; get; }

        public bool Status { get; set; }

        public bool? Gender { get; set; }

        public DateTime? OnlineTime { set; get; }
        public bool Online { set; get; }

        public bool Delete { set; get; }

        [NotMapped]
        public Boolean TrangThai { set; get; }

        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { set; get; }

        public virtual ICollection<AppRole> AppRoles { set; get; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<AppUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}