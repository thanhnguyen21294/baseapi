﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVonGoiThauThanhToans")]
    public class NguonVonGoiThauThanhToan: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonGoiThauThanhToan { get; set; }

        [Required]
        public int IdNguonVonDuAnGoiThau { get; set; }

        [Required]
        public int IdThanhToanHopDong { get; set; }

        public double GiaTri { get; set; }

        public double TamUng { get; set; }

        public double? GiaTriTabmis { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdNguonVonDuAnGoiThau")]
        public virtual NguonVonDuAnGoiThau NguonVonDuAnGoiThau { get; set; }
        [ForeignKey("IdThanhToanHopDong")]
        public virtual ThanhToanHopDong ThanhToanHopDong { get; set; }

    }
}
