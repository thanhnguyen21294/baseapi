﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("CamKetGiaiNgans")]
    public class CamKetGiaiNgan : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCamKetGiaiNgan { get; set; }

        public int NamCK { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public double? Thang1 { get; set; }
        public double? Thang2 { get; set; }
        public double? Thang3 { get; set; }
        public double? Thang4 { get; set; }
        public double? Thang5 { get; set; }
        public double? Thang6 { get; set; }
        public double? Thang7 { get; set; }
        public double? Thang8 { get; set; }
        public double? Thang9 { get; set; }
        public double? Thang10 { get; set; }
        public double? Thang11 { get; set; }
        public double? Thang12 { get; set; }
        public double? Quy1 { get; set; }
        public double? Quy2 { get; set; }
        public double? Quy3 { get; set; }
        public double? Quy4 { get; set; }
        public double? TongCong { get; set; }
        public string GhiChu { get; set; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }

       
    }
}
