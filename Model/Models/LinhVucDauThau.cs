﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("LinhVucDauThaus")]
    public class LinhVucDauThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLinhVucDauThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenLinhVucDauThau { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<GoiThau> GoiThaus { get; set; }
    }
}
