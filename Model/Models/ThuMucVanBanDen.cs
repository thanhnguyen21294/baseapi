﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("ThuMucVanBanDens")]
    public class ThuMucVanBanDen : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThuMucVanBanDen { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenThuMucVanBanDen { set; get; }
        public int? IdThuMucCha { set; get; }
        public ICollection<VanBanDen> VanBanDens { get; set; }
        [NotMapped]
        public int SoLuongVB { get; set; }
    }
}
