﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    public class PhanHoi
    {
        //public string chuoi { get; set; }
        public List<Double> phanTram { get; set; }
        public List<String> tenDuAn { get; set; }
        public PhanHoi()
        {
            phanTram = new List<Double>();
            tenDuAn = new List<String>();
        }
    }
}
