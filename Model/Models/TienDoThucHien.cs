﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("TienDoThucHiens")]
    public class TienDoThucHien : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTienDo { set; get; }
        public int IdDuAn { set; get; }
        public int IdGoiThau { set; get; }
        public int IdHopDong { set; get; }
        [MaxLength(255)]
        public string TenCongViec { set; get; }
        public DateTime? TuNgay { set; get; }
        public DateTime? DenNgay { set; get; }
        public string GhiChu { set; get; }
        //[ForeignKey("IdDuAn")]
        //public virtual DuAn DuAn { set; get; }
        //[ForeignKey("IdGoiThau")]
        //public virtual GoiThau GoiThau { set; get; }
        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }
        public virtual ICollection<TienDoChiTiet> TienDoChiTiets { set; get; }
    }
}
