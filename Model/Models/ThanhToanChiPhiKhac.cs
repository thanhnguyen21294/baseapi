﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("ThanhToanChiPhiKhacs")]
    public class ThanhToanChiPhiKhac : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThanhToanChiPhiKhac { set; get; }
        public int IdDuAn { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public int? IdNhaThau { set; get; }
        [MaxLength(50)]
        public string LoaiThanhToan { set; get; }

        public int? IdLoaiChiPhi { get; set; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }

        public DateTime? NgayThanhToan { set; get; }
        public string GhiChu { set; get; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { set; get; }
        [ForeignKey("IdNhaThau")]
        public virtual DanhMucNhaThau DanhMucNhaThau { set; get; }

        [ForeignKey("IdLoaiChiPhi")]
        public virtual LoaiChiPhi LoaiChiPhi { set; get; }
        public virtual ICollection<NguonVonThanhToanChiPhiKhac> NguonVonThanhToanChiPhiKhacs { set; get; }

    }
}
