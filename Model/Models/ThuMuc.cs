﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("ThuMucs")]
    public class ThuMuc : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThuMuc { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenThuMuc { set; get; }
        public int? IdThuMucCha { set; get; }
        
        public int? IdThuMucMacDinh { get; set; }
        [ForeignKey("IdThuMucMacDinh")]
        public virtual ThuMucMacDinh ThuMucMacDinh { set; get; }
       
        public ICollection<VanBanDuAn> VanBanDuAns { get; set; }
        public int IdDuAn { get; set; }

    }
}
