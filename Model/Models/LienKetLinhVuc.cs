﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("LienKetLinhVucs")]
    public class LienKetLinhVuc : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLienKetLinhVuc { get; set; }
        public string TenLienKetLinhVuc { get; set; }

        public string GhiChu { get; set; }

        public int? Order { get; set; }
    }
}
