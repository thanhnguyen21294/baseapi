﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVons")]
    public class NguonVon: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVon { get; set; }

        [Required]
        [MaxLength(50)]
        public string Ma { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenNguonVon { get; set; }

        [MaxLength(50)]
        public string Loai { get; set; }

        [MaxLength(50)]
        public string Nhom { get; set; }

        public int? IdNguonVonCha { get; set; }

        public string Ghichu { get; set; }

        [ForeignKey("IdNguonVonCha")]
        public virtual NguonVon NguonVonCha { set; get; }
        //public virtual ICollection<NguonVonDuAn> NguonVonDuAns { get; set; }
    }
}
