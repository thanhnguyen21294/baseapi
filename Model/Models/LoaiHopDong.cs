﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("LoaiHopDongs")]
    public class LoaiHopDong: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLoaiHopDong { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenLoaiHopDong { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<HopDong> HopDongs { get; set; }
        public virtual ICollection<GoiThau> GoiThaus { set; get; }
    }
}
