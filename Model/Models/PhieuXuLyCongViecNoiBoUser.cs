﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("PhieuXuLyCongViecNoiBoUsers")]
    public class PhieuXuLyCongViecNoiBoUser : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPhieuXuLyCongViecNoiBoUser { set; get; }
        public int IdPhieuXuLyCongViecNoiBo { set; get; }
        public int UserXLCVNoiBoEnum { get; set; }
        public bool DaKy { get; set; }
        public string UserId { get; set; }

        public string FullName { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdPhieuXuLyCongViecNoiBo")]
        public virtual PhieuXuLyCongViecNoiBo PhieuXuLyCongViecNoiBo { get; set; }
    }
}
