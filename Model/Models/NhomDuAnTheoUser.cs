﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Abstract;

namespace Model.Models
{
    [Table("NhomDuAnTheoUsers")]
    public class NhomDuAnTheoUser : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNhomDuAn { set; get; }
        [MaxLength(500)]
        public string TenNhomDuAn { set; get; }
        public string GhiChu { set; get; }
        public bool PhongChucNang { set; get; }
        //public virtual ICollection<AppUser> AppUsers { set; get; }
        public virtual ICollection<DuAn> DuAns { set; get; }

    }
}
