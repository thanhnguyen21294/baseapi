﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("KeHoachLuaChonNhaThaus")]
    public class KeHoachLuaChonNhaThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachLuaChonNhaThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string NoiDung { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public int? IdDieuChinhKeHoachLuaChonNhaThau { get; set; }

        public DateTime? NgayTrinh { get; set; }

        [MaxLength(255)]
        public string SoVanBanTrinh { get; set; }

        public DateTime? NgayNhanDuHoSo { get; set; }

        public DateTime? NgayThamDinh { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        [MaxLength(50)]
        public string SoPheDuyet { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        [MaxLength(50)]
        public string NguoiPheDuyet { get; set; }

        public DateTime? NgayDangTaiThongTin { get; set; }

        [MaxLength(50)]
        public string PhuongTienDangTai { get; set; }

        [MaxLength(50)]
        public string ToChucCaNhanGiamSat { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdCoQuanPheDuyet")]
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IdDieuChinhKeHoachLuaChonNhaThau")]
        public virtual KeHoachLuaChonNhaThau DieuChinhKeHoachLuaChonNhaThau { set; get; }
        public virtual ICollection<GoiThau> GoiThaus { get; set; }

    }
}
