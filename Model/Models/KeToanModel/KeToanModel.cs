﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.KeToanModel
{
    public class KeToanModel
    {
        public int IdDuAn { get; set; }
        public PheDuyetInVewModel CTDT { get; set; }
        public PheDuyetInVewModel CBDT { get; set; }
        public PheDuyetInVewModel CBTH { get; set; }
    }
    public class PheDuyetInVewModel{
        public string SoQD { get; set; }
        public double? GiaTri { get; set; }
    }
}
