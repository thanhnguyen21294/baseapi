﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("PhuongThucDauThaus")]
    public class PhuongThucDauThau : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPhuongThucDauThau { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenPhuongThucDauThau { set; get; }
        public string GhiChu { set; get; }
        public virtual ICollection<GoiThau> GoiThaus { set; get; }
    }
}
