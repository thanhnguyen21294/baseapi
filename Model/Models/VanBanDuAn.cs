﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("VanBanDuAns")]
    public class VanBanDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdVanBan { set; get; }
        
        [MaxLength(255)]
        [Required]
        public string SoVanBan { set; get; }
        [MaxLength(255)]
        [Required]
        public string TenVanBan { set; get; }
        [MaxLength(255)]
        public string FileName { set; get; }
        [Column(TypeName = "ntext")]
        public string FileUrl { set; get; }
        public DateTime? NgayBanHanh { set; get; }
        [MaxLength(255)]
        public string CoQuanBanHanh { set; get; }
        public string MoTa { set; get; }
        public string GhiChu { set; get; }
        public int IdThuMuc { set; get; }
        [ForeignKey("IdThuMuc")]
        public virtual ThuMuc ThuMuc { set; get; }
        public int IdDuAn { set; get; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
    }

}
