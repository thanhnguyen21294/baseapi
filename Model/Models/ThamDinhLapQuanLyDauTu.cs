﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("ThamDinhLapQuanLyDauTus")]
    public class ThamDinhLapQuanLyDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThamDinh { set; get; }
        public int IdLapQuanLyDauTu { set; get; }
        [MaxLength(255)]
        public string NoiDung { get; set; }
        public DateTime? NgayNhanDuHoSo { set; get; }
        public DateTime? NgayPheDuyet { set; get; }
        [MaxLength(50)]
        public string ToChucNguoiThamDinh { set; get; }
        public string DanhGia { set; get; }
        public string GhiChu { set; get; }
        [ForeignKey("IdLapQuanLyDauTu")]
        public virtual LapQuanLyDauTu LapQuanLyDauTu { set; get; }
    }
}
