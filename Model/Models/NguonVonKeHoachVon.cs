﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("NguonVonKeHoachVons")]
    public class NguonVonKeHoachVon: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonKeHoachVon { get; set; }

        public int IdKeHoachVon { get; set; }
  
        public int IdNguonVonDuAn { get; set; }
        public double GiaTri { get; set; }
        public double GiaTriTabmis { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdKeHoachVon")]
        public virtual KeHoachVon KeHoachVon { get; set; }
        [ForeignKey("IdNguonVonDuAn")]
        public virtual NguonVonDuAn NguonVonDuAn { get; set; }
    }
}
