﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("HinhThucDauTus")]
    public class HinhThucDauTu : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHinhThucDauTu { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenHinhThucDauTu { get; set; }

        public string GhiChu { get; set; }

        public virtual ICollection<DuAn> DuAns { get; set; }
    }
}
