﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Model.Abstract;

namespace Model.Models
{
    [Table("DuAns")]
    public class DuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDuAn { set; get; }

        public int? IdNhomDuAnTheoUser { set; get; }

        [MaxLength(50)]
        [Required]
        public string Ma { set; get; }

        public string TenDuAn { set; get; }

        public int? IdDuAnCha { get; set; }

        public int? IdChuDauTu { set; get; }

        [MaxLength(50)]
        public string ThoiGianThucHien { get; set; }

        public int? IdLinhVucNganhNghe { get; set; }

        public string DiaDiem { get; set; }
        public double KinhDo { set; get; }
        public double ViDo { set; get; }

        public string MucTieu { get; set; }

        public string QuyMoNangLuc { get; set; }

        public int? IdNhomDuAn { get; set; }

        public int? IdLoaiCapCongTrinh { get; set; }

        public int? IdHinhThucDauTu { get; set; }

        public int? IdGiaiDoanDuAn { get; set; }

        public int? IdHinhThucQuanLy { get; set; }

        public int? IdTinhTrangDuAn { get; set; }

        public DateTime? NgayKhoiCongKeHoach { get; set; }

        public DateTime? NgayKetThucKeHoach { get; set; }

        public DateTime? NgayKhoiCongThucTe { get; set; }

        public DateTime? NgayKetThucThucTe { get; set; }

        public double TongMucDauTu { get; set; }
        public double? TongMucDauTu_XL { get; set; }
        public double? TongMucDauTu_TB { get; set; }
        public double? TongMucDauTu_GPMB { get; set; }
        public double? TongMucDauTu_TV { get; set; }
        public double? TongMucDauTu_QLDA { get; set; }
        public double? TongMucDauTu_K { get; set; }
        public double? TongMucDauTu_DP { get; set; }

        public double? TongDuToan { get; set; }
        public double? TongDuToan_XL { get; set; }
        public double? TongDuToan_TB { get; set; }
        public double? TongDuToan_GPMB { get; set; }
        public double? TongDuToan_TV { get; set; }
        public double? TongDuToan_QLDA { get; set; }
        public double? TongDuToan_K { get; set; }
        public double? TongDuToan_DP { get; set; }

        public double? TongKHV { get; set; }
        public double? TongThanhToan { get; set; }

        public int? Order { get; set; }

        public int? OrderTemp { get; set; }

        public string GhiChu { get; set; }

        public string GhiChuKeToan { get; set; }

        [ForeignKey("IdChuDauTu")]
        public virtual ChuDauTu ChuDauTu { set; get; }
        [ForeignKey("IdLinhVucNganhNghe")]
        public virtual LinhVucNganhNghe LinhVucNganhNghe { get; set; }
        [ForeignKey("IdHinhThucDauTu")]
        public virtual HinhThucDauTu HinhThucDauTu { get; set; }
        [ForeignKey("IdNhomDuAn")]
        public virtual NhomDuAn NhomDuAn { get; set; }
        [ForeignKey("IdHinhThucQuanLy")]
        public virtual HinhThucQuanLy HinhThucQuanLy { get; set; }
        [ForeignKey("IdTinhTrangDuAn")]
        public virtual TinhTrangDuAn TinhTrangDuAn { get; set; }
        [ForeignKey("IdLoaiCapCongTrinh")]
        public virtual LoaiCapCongTrinh LoaiCapCongTrinh { get; set; }
        [ForeignKey("IdGiaiDoanDuAn")]
        public virtual GiaiDoanDuAn GiaiDoanDuAn { get; set; }
        [ForeignKey("IdNhomDuAnTheoUser")]
        public virtual NhomDuAnTheoUser NhomDuAnTheoUser { set; get; }
        public virtual ICollection<NguonVonDuAn> NguonVonDuAns { get; set; }
        public virtual ICollection<HopDong> HopDongs { get; set; }
        public virtual ICollection<KeHoachVon> KeHoachVons { get; set; }
        public virtual ICollection<GoiThau> GoiThaus { get; set; }
        public virtual ICollection<ChuTruongDauTu> ChuTruongDauTus { get; set; }
        public virtual ICollection<DanhMucNhaThau> DanhMucNhaThaus { get; set; }
        public virtual ICollection<KeHoachLuaChonNhaThau> KeHoachLuaChonNhaThaus { get; set; }
        public virtual ICollection<QuyetToanDuAn> QuyetToanDuAns { get; set; }
        public virtual ICollection<LapQuanLyDauTu> LapQuanLyDauTus { get; set; }
        public virtual ICollection<VanBanDuAn> VanBanDuAns { get; set; }
        public virtual ICollection<DuAnUser> DuAnUsers { get; set; }
        public virtual ICollection<DuAnQuanLy> DuAnQuanLys { get; set; }
        public virtual ICollection<QLTD_KeHoach> QLTD_KeHoachs { get; set; }
        //public virtual ICollection<TienDoThucHien> TienDoThucHiens { set; get; }
        //public virtual ICollection<ThanhToanChiPhiKhac> ThanhToanChiPhiKhacs { set; get; }

        //SignalR
        //public DuAn()
        //{
        //    DuAnUsers = new List<DuAnUser>();
        //}

        //[StringLength(128)]
        //public string UserId { set; get; }

        //[ForeignKey("UserId")]
        //public virtual AppUser AppUser { get; set; }

    }
}
