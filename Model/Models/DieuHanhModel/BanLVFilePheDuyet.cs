﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.DieuHanhModel
{
    public class BanLVFilePheDuyet
    {
        public int IDFile { get; set; }
        public string NoiDung { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
    }

    public class BanLVDkyKHoachParent
    {
        public int? ParentID { get; set; }
        public string NoiDungParent { get; set; }
        public IEnumerable<BanLVDKyKHoachChild> grp { get; set; }
    }

    public class BanLVDKyKHoachChild
    {
        public int IDFile { get; set; }
        public string NoiDung { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
    }
}
