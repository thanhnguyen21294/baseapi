﻿using Model.Models.QLTD.GPMB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.DieuHanhModel
{
    public class BanLVPheDuyetKHTD
    {
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int IDDA { get; set; }
        public string DuAn { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public ICollection<GPMB_KeHoachCongViec> gpmb_kehoachcongviec { get; set; }
    }

    public class DuAnKHMoiNhat
    {
        public int IDDA { get; set; }
        public string DA { get; set; }
        public IEnumerable<KHPDMoiNhat> grpKHPD { get; set; }
    }

    public class KHPDMoiNhat
    {
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public ICollection<GPMB_KeHoachCongViec> gpmb_kehoachcongviec { get; set; }
    }
}
