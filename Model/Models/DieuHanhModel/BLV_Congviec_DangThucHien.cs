﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.DieuHanhModel
{
    public class BLV_Congviec_DangThucHien
    {
        public int IDDA { get; set; }
        public string TenDuAn { get; set; }
        public int IDCV { get; set; }
        public int? IdNhomDuAnTheoUser { get; set; }
        public string DuAnUsers { get; set; }
        //public IEnumerable<BLV_Congviec_DangThucHien_Cviec> grpCV { get; set; }
    }

    //public class BLV_Congviec_DangThucHien_Cviec
    //{
    //    public int IDCV { get; set; }
    //    public string CViec { get; set; }
    //}
}
