﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.DieuHanhModel
{
    public class BanLVKeHoachMoiNhat
    {
        public int IDGiaiDoan { get; set; }
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int IDDA { get; set; }
        public string DuAn { get; set; }
        public int? TinhTrang { get; set; }
        public int? IdNhomDuAnTheoUser { get; set; }
        public int TrangThai { get; set; }
        public string DuAnUsers { get; set; }
        public ICollection<QLTD_KeHoachCongViec> qltd_kehoachcongviec {get;set;}
    }

    public class GiaiDoanKeHoachMoiNhat
    {
        public int IDGiaiDoan { get; set; }
        public int IDDA { get; set; }
        public string DA { get; set; }
        public string DuAnUsers { get; set; }
        public int? IdNhomDuAnTheoUser { get; set; }
        public IEnumerable<KHMoiNhat> grpKH { get; set; }
    }
    public class KHMoiNhat
    {
        public int IDGiaiDoan { get; set; }
        public int IDKH { get; set; }
        public string KH { get; set; }
        public int? TinhTrang { get; set; }
        public int TrangThai { get; set; }
        public ICollection<QLTD_KeHoachCongViec> qltd_kehoachcongviec { get; set; }
    }
}
