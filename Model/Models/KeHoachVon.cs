﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("KeHoachVons")]
    public class KeHoachVon: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKeHoachVon { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public int? IdKeHoachVonDieuChinh { get; set; }

        [Required]
        [MaxLength(50)]
        public string SoQuyetDinh { get; set; }

        public int? IdCoQuanPheDuyet { get; set; }

        public DateTime? NgayPheDuyet { get; set; }

        public int? NienDo { get; set; }

        [MaxLength(50)]
        public string TinhChatKeHoach { get; set; }

        public double? TongGiaTri { get; set; }

        public double? XayLap { get; set; }

        public double? ThietBi { get; set; }

        public double? GiaiPhongMatBang { get; set; }

        public double? TuVan { get; set; }

        public double? QuanLyDuAn { get; set; }

        public double? Khac { get; set; }

        public double? DuPhong { get; set; }

        public string GhiChu { get; set; }

        public bool DeXuatDieuChinh { get; set; }

        [ForeignKey("IdCoQuanPheDuyet")]
        public virtual CoQuanPheDuyet CoQuanPheDuyet { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        public virtual ICollection<NguonVonKeHoachVon> NguonVonKeHoachVons { get; set; }
    }
}
