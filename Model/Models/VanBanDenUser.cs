﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("VanBanDenUsers")]
    public class VanBanDenUser : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdVanBanDenUser { get; set; }

        public int IdVanBanDen { get; set; }

        public string UserId { get; set; }

        public bool HasRead { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdVanBanDen")]
        public virtual VanBanDen VanBanDens { get; set; }

    }
}