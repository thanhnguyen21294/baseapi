﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("LinhVucNganhNghes")]
    public class LinhVucNganhNghe: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLinhVucNganhNghe { get; set; }

        [Required]
        [MaxLength(255)]
        public string Ma { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenLinhVucNganhNghe { get; set; }

        [MaxLength(255)]
        public string KhoiLinhVuc { get; set; }

        public int? IdLienKetLinhVuc { set; get; }
        
        public string GhiChu { get; set; }

        public int? Order { get; set; }

        public virtual ICollection<DuAn> DuAns { get; set; }

        [ForeignKey("IdLienKetLinhVuc")]
        public virtual LienKetLinhVuc LienKetLinhVuc { set; get; }
    }
}
