﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("TienDoChiTiets")]
    public class TienDoChiTiet : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTienDoChiTiet { set; get; }
        public int IdTienDo { set; get; }
        public string NoiDung { set; get; }
        public DateTime? NgayThucHien { set; get; }
        public string TonTaiVuongMac { set; get; }
        public string GhiChu { set; get; }
        [ForeignKey("IdTienDo")]
        public virtual TienDoThucHien TienDoThucHien { set; get; }
    }
}
