﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("GoiThaus")]
    public class GoiThau: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGoiThau { get; set; }

        [Required]
        [MaxLength(255)]
        public string TenGoiThau { get; set; }

        public int IdDuAn { get; set; }

        public int? IdKeHoachLuaChonNhaThau { get; set; }

        public double? GiaGoiThau { get; set; }

        public int? LoaiGoiThau { get; set; }

        public int? IdLinhVucDauThau { get; set; }

        public int? IdHinhThucLuaChon { get; set; }

        public int? IdPhuongThucDauThau { get; set; }

        public int? HinhThucDauThau { get; set; }

        public int? LoaiDauThau { get; set; }

        public int? IdLoaiHopDong { get; set; }

        [MaxLength(50)]
        public string ThoiGianThucHienHopDong { get; set; }

        public string GhiChu { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
        [ForeignKey("IdLinhVucDauThau")]
        public virtual LinhVucDauThau LinhVucDauThau { get; set; }
        [ForeignKey("IdPhuongThucDauThau")]
        public virtual PhuongThucDauThau PhuongThucDauThau { get; set; }
        [ForeignKey("IdHinhThucLuaChon")]
        public virtual HinhThucLuaChonNhaThau HinhThucLuaChonNhaThau { get; set; }
        [ForeignKey("IdKeHoachLuaChonNhaThau")]
        public virtual KeHoachLuaChonNhaThau KeHoachLuaChonNhaThau { get; set; }
        [ForeignKey("IdLoaiHopDong")]
        public virtual LoaiHopDong LoaiHopDong { set; get; }
        public virtual ICollection<HopDong> HopDongs { get; set; }
        public virtual ICollection<NguonVonDuAnGoiThau> NguonVonDuAnGoiThaus { get; set; }
        public virtual ICollection<KetQuaDauThau> KetQuaDauThaus { get; set; }
        public virtual ICollection<HoSoMoiThau> HoSoMoiThaus { get; set; }
        //public virtual ICollection<TienDoThucHien> TienDoThucHiens { set; get; }

    }
}
