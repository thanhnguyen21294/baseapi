﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVonGoiThauTamUngs")]
    public class NguonVonGoiThauTamUng: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonGoiThauTamUng { get; set; }

        public int IdNguonVonDuAnGoiThau { get; set; }
        public int IdTamUng { get; set; }
        public double GiaTri { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdNguonVonDuAnGoiThau")]
        public virtual NguonVonDuAnGoiThau NguonVonDuAnGoiThau { get; set; }
        [ForeignKey("IdTamUng")]
        public virtual TamUngHopDong TamUngHopDong { get; set; }
    }
}
