﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("NguonVonThanhToanCPKs")]
    public class NguonVonThanhToanChiPhiKhac: Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNguonVonThanhToanChiPhiKhac { get; set; }
        public int IdThanhToanChiPhiKhac { get; set; }

        public int IdNguonVonDuAn { get; set; }

        public double GiaTri { get; set; }

        public double GiaTriTabmis { get; set; }

        public string GhiChu { get; set; }
        [ForeignKey("IdThanhToanChiPhiKhac")]
        public virtual ThanhToanChiPhiKhac ThanhToanChiPhiKhac { get; set; }
        [ForeignKey("IdNguonVonDuAn")]
        public virtual NguonVonDuAn NguonVonDuAn { get; set; }
    }
}
