﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("GiaTriThanhToanTheoNams")]
    public class GiaTriThanhToanTheoNam : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGiaTriThanhToanTheoNam { get; set; }

        [Required]
        public int IdDuAn { get; set; }

        public int? IdKeHoachVon { get; set; }

        public int NienDo { get; set; }
        [DefaultValue(0)]
        public double? GiaTriKHV { get; set; }
        [DefaultValue(0)]
        public double? GiaTriKHVTabmis { get; set; }

        [DefaultValue(0)]
        public double? GiaTriThanhToan { get; set; }
        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { get; set; }
    }
}
