﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models
{
    [Table("DuAnHoSoQuyetToans")]
    public class DuAnHoSoQuyetToan : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDuAnHoSoQuyetToan { get; set; }
        public int IdDanhMucHoSoQuyetToan { get; set; }
        public int IdDuAn { get; set; }

        public int KyThuat { get; set; }
        public int KeToan { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn Duan { get; set; }

        [ForeignKey("IdDanhMucHoSoQuyetToan")]
        public virtual DanhMucHoSoQuyetToan DanhMucHoSoQuyetToan { get; set; }
        public DateTime? HoanThien { get; set; }
    }
}
