﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("BaoCaoTables")]
    public class BaoCaoTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdBaoCao { get; set; }

        public string MaBaoCao { get; set; }

        [Required]
        public string Ten { get; set; }

        public string NoiDung { get; set; }

        public DateTime? CreatedDate { set; get; }

        [MaxLength(256)]
        public string CreatedBy { set; get; }

        public string PhanLoai { get; set; }

        public string URL { get; set; }
        
    }
}
