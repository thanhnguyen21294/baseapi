﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    //SignalR DuAnUser

    [Table("DuAnUsers")]
    public class DuAnUser : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDuanUser { get; set; }

        public int IdDuAn { get; set; }

        public string UserId { get; set; }

        public bool HasRead { get; set; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUsers { get; set; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAns { get; set; }

    }
}