﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models
{
    [Table("HinhThucDauThaus")]
    public class HinhThucDauThau : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHinhThucDauThau { get; set; }

        [MaxLength(255)]
        [Required]
        public string TenHinhThucDauThau { get; set; }

        public string GhiChu { get; set; }
    }
}
