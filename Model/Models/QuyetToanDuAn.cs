﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Abstract;

namespace Model.Models
{
    [Table("QuyetToanDuAns")]
    public class QuyetToanDuAn : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQuyetToanDuAn { set; get; }
        public int IdDuAn { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public DateTime? NgayKyBienBan { set; get; }
        public DateTime? NgayTrinh { set; get; }
        public DateTime? NgayNhanDuHoSo { set; get; }
        public DateTime? NgayThamTra { set; get; }
        public DateTime? NgayPheDuyetQuyetToan { set; get; }
        [MaxLength(50)]
        public string SoPheDuyet { set; get; }
        public int? IdCoQuanPheDuyet { set; get; }
        public double? TongGiaTri { set; get; }
        public double? XayLap { set; get; }
        public double? ThietBi { set; get; }
        public double? GiaiPhongMatBang { set; get; }
        public double? TuVan { set; get; }
        public double? QuanLyDuAn { set; get; }
        public double? Khac { set; get; }
        public double? DuPhong { set; get; }
        public string GhiChu { set; get; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { set; get; }
        [ForeignKey("IdCoQuanPheDuyet")]
        public virtual CoQuanPheDuyet CoQuanPheDuyet { set; get; }
        public virtual ICollection<NguonVonQuyetToanDuAn> NguonVonQuyetToanDuAns { set; get; }
    }
}
