﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Mobile
{
    public class Notification_Group : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string Name { set; get; }

        public string Description { set; get; }

        public string Notification_Key_Name { set; get; }

        public string Notification_Key { set; get; }
    }
}
