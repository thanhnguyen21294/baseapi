﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.Common
{
    [Table("ChatApps")]
    public class ChatApp : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [MaxLength(50)]
        public string IdUser1 { set; get; }
        [MaxLength(50)]
        public string IdUser2 { set; get; }

        public string NoiDung { set; get; }

        public DateTime NgayTao { set; get; }

        public Boolean TrangThaiChatNhom { set; get; }

        public bool IsFile { set; get; }

        [NotMapped]
        public List<string> ListThongTinNhom { get; set; }

        [ForeignKey("IdUser1")]
        public virtual AppUser AppUser1 { get; set; }

        [ForeignKey("IdUser2")]
        public virtual AppUser AppUser2 { get; set; }
        [NotMapped]
        public string TenNguoiGui { get; set; }

        [NotMapped]
        public Boolean TrangThai { set; get; }
    }
}
