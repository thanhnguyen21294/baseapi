﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Mobile
{
    public class Notification_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string UserId { set; get; }

        public int NotificationId { set; get; }

        [DefaultValue("false")]
        public bool HasRead { set; get; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUser { set; get; }

        [ForeignKey("NotificationId")]
        public virtual Notification Notification { set; get; }
    }
}
