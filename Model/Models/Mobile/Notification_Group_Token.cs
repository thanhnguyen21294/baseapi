﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Mobile
{
    public class Notification_Group_Token
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public int DeviceTokenId { set; get; }
        public int NotificationGroupId { set; get; }

        [ForeignKey("DeviceTokenId")]
        public virtual Device_Token Device_Token { set; get; }

        [ForeignKey("NotificationGroupId")]
        public virtual Notification_Group Notification_Group { set; get; }
    }
}
