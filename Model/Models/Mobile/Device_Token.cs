﻿using Model.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Mobile
{
    public class Device_Token : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string UserId { set; get; }

        public string Token { set; get; }

        [ForeignKey("UserId")]
        public virtual AppUser AppUser { set; get; }
    }
}
