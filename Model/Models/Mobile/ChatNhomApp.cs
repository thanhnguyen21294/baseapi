﻿using Model.Abstract;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.Common
{
    [Table("ChatNhomApps")]
    public class ChatNhomApp : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        [MaxLength(50)]
        public string IdUser1 { set; get; }

        public int IdUser2 { set; get; }

        public string NoiDung { set; get; }

        public DateTime NgayTao { set; get; }

        public bool IsFile { set; get; }

        public Boolean TrangThaiChatNhom { set; get; }

        [NotMapped]
        public List<string> ListThongTinNhom { get; set; }

        [ForeignKey("IdUser1")]
        public virtual AppUser AppUser1 { get; set; }

        [ForeignKey("IdUser2")]
        public virtual NhomChatApp AppUser2 { get; set; }

        [NotMapped]
        public string TenNguoiGui { get; set; }
    }
    public class NoiDungChatNhom : Auditable
    {
        public int Id { get; set; }
        public string IdUser1 { get; set; }
        public string IdUser2 { get; set; }
        public string NoiDung { get; set; }
        public bool IsFile { set; get; }
        public DateTime? NgayTao { set; get; }
        public object AppUser1 { get; set; }
        public object AppUser2 { get; set; }
    }
}
