﻿using Model.Abstract;
using Model.Models.Mobile;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Common
{
    [Table("TrangThaiChats")]
    public class TrangThaiChat
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string IdUser1 { set; get; }

        public string IdUser2 { set; get; }

        public bool IdNhom { set; get; }

        public bool TrangThai { set; get; }

        //public TrangThaiChat(string idUser1, string idUser2, bool idNhom, bool trangThai)
        //{
        //    IdUser1 = idUser1;
        //    IdUser2 = idUser2;
        //    IdNhom = idNhom;
        //    TrangThai = trangThai;
        //}
    }
}
