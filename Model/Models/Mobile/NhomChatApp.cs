﻿using Model.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Models.Mobile
{
    [Table("NhomChatApps")]
    public class NhomChatApp : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public string ListUser { set; get; }

        public string TenNhomChat { set; get; }

        public string GhiChu { set; get; }

        [NotMapped]
        public Boolean TrangThai { set; get; }

    }
}
