﻿using Model.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Models.Mobile
{
    public class Notification : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        [ForeignKey("CreatedBy")]
        public virtual AppUser AppUser { set; get; }
    }
}
