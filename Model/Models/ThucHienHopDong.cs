﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Model.Abstract;

namespace Model.Models
{
    [Table("ThucHienHopDongs")]
    public class ThucHienHopDong : Auditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdThucHien { set; get; }
        public int? IdDuAn { set; get; }
        public int? IdHopDong { set; get; }
        [MaxLength(255)]
        [Required]
        public string NoiDung { set; get; }
        public DateTime? ThoiDiemBaoCao { set; get; }
        public DateTime? UocDenNgay { set; get; }
        public double? KhoiLuong { set; get; }
        public double? UocThucHien { set; get; }
        public DateTime? NgayNghiemThu { set; get; }
        public int? IdLoaiChiPhi { set; get; }
        public double? GiaTriNghiemThu { set; get; }

        [DefaultValue(0)]
        public int TrangThaiGui { set; get; }

        public string GhiChu { set; get; }
        [ForeignKey("IdHopDong")]
        public virtual HopDong HopDong { set; get; }

        [ForeignKey("IdDuAn")]
        public virtual DuAn DuAn { set; get; }

        [ForeignKey("IdLoaiChiPhi")]
        public virtual LoaiChiPhi LoaiChiPhi { set; get; }
        public virtual ICollection<ThanhToanHopDong> ThanhToanHopDongs { set; get; }
    }
}
