﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class PhieuForMobiles
    {
        public int? IdDuAn { get; set; }
        public string TenDuAn { get; set; }
        public IEnumerable<PhieuMinis> Phieus  { get;set;}
    }

    public class PhieuMinis
    {
        public int IdPhieuXuLyCongViecNoiBo { get; set; }
        public string TenCongViec { get; set; }
        public string NoiDung { get; set; }
        public DateTime? NgayChuyen { get; set; }
        public DateTime? NgayNhan { get; set; }
        public IEnumerable<PhieuMinis> Childrends { get; set; }
    }
}
