﻿using System;

namespace Common
{
    public class CaiDatXoaLichSu
    {
        public int KieuXoa { get; set; }

        public int ChuKy { get; set; }

        public int Thang { get; set; }

        public int Thu { get; set; }

        public int Ngay { get; set; }

        public int Gio { get; set; }

        public int Phut { get; set; }

        public DateTime? NgayXoa { get; set; }
    }
}