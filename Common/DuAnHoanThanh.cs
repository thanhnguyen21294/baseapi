﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DuAnHoanThanh
    {
        public int IdDuAn { set; get; }
        public string TenDuAn { set; get; }
        public int IdKeHoach { set; get; }
        public DateTime? NgayKeHoach { set; get; }
        public DateTime? NgayHoanThanh { set; get; }
        public int SoSanh { set; get; }
    }
}
