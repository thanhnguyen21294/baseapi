﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DuAnTMDT
    {
        public int IdDuAn { set; get; }
        public string TenDuAn { set; get; }
        public int IdKeHoach { set; get; }
        public DateTime? NgayKeHoach { set; get; }
        public string SoQD { set; get; }
        public double? TMDTPD { set; get; }
        public double? TMDTDA { set; get; }
    }
}
